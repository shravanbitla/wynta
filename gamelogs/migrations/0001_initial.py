# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ChatLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('table_id', models.IntegerField(null=True, blank=True)),
                ('game_id', models.IntegerField(null=True, blank=True)),
                ('playername', models.CharField(max_length=50, null=True, blank=True)),
                ('message', models.CharField(max_length=255, null=True, blank=True)),
                ('chat_datetime', models.DateTimeField(null=True, blank=True)),
                ('gametype', models.CharField(max_length=20, null=True, blank=True)),
                ('chipstype', models.CharField(max_length=50, null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='GameLogs',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('gameid', models.IntegerField(null=True, blank=True)),
                ('subgameid', models.IntegerField(null=True, blank=True)),
                ('startdate', models.DateTimeField(null=True, blank=True)),
                ('enddate', models.DateTimeField(null=True, blank=True)),
                ('no_players', models.IntegerField(null=True, blank=True)),
                ('prize_amount', models.DecimalField(null=True, max_digits=10, decimal_places=2, blank=True)),
                ('rake', models.DecimalField(null=True, max_digits=10, decimal_places=2, blank=True)),
                ('winnings', models.DecimalField(null=True, max_digits=10, decimal_places=2, blank=True)),
                ('winner', models.CharField(max_length=50, null=True, blank=True)),
                ('status', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='GamePlayers',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('subgameid', models.IntegerField(null=True, blank=True)),
                ('playername', models.CharField(max_length=50, null=True, blank=True)),
                ('toss_position', models.IntegerField(null=True, blank=True)),
                ('toss_card', models.CharField(max_length=3, null=True, blank=True)),
                ('player_status', models.CharField(max_length=20, null=True, blank=True)),
                ('score_amount', models.DecimalField(null=True, max_digits=10, decimal_places=2, blank=True)),
                ('game', models.ForeignKey(blank=True, to='gamelogs.GameLogs', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='PicksDiscard',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('turns', models.CharField(max_length=10, null=True, blank=True)),
                ('playername', models.CharField(max_length=50, null=True, blank=True)),
                ('status', models.CharField(max_length=20, null=True, blank=True)),
                ('action', models.CharField(max_length=50, null=True, blank=True)),
                ('picked_card', models.CharField(max_length=3, null=True, blank=True)),
                ('picked_from', models.CharField(max_length=2, null=True, blank=True)),
                ('internrt_connection', models.BooleanField(default=False)),
                ('autoplay', models.BooleanField(default=False)),
                ('game', models.ForeignKey(to='gamelogs.GameLogs')),
            ],
        ),
        migrations.CreateModel(
            name='PlayerPools',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('playername', models.CharField(max_length=50, null=True, blank=True)),
                ('toss_position', models.IntegerField(null=True, blank=True)),
                ('toss_card', models.CharField(max_length=3, null=True, blank=True)),
                ('player_status', models.CharField(max_length=50, null=True, blank=True)),
                ('table_start_score', models.DecimalField(null=True, max_digits=10, decimal_places=2, blank=True)),
                ('result', models.CharField(max_length=50, null=True, blank=True)),
                ('count', models.DecimalField(null=True, max_digits=10, decimal_places=2, blank=True)),
                ('mode', models.CharField(max_length=10, null=True, blank=True)),
                ('score', models.DecimalField(null=True, max_digits=10, decimal_places=2, blank=True)),
                ('end_of_round', models.CharField(max_length=20, null=True, blank=True)),
                ('dropped_after_auto_play', models.BooleanField(default=False)),
                ('game', models.ForeignKey(blank=True, to='gamelogs.GameLogs', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='PlayerStrikes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('playername', models.CharField(max_length=50, null=True, blank=True)),
                ('toss_position', models.IntegerField(null=True, blank=True)),
                ('toss_card', models.CharField(max_length=3, null=True, blank=True)),
                ('player_status', models.CharField(max_length=50, null=True, blank=True)),
                ('amount_on_table', models.DecimalField(null=True, max_digits=10, decimal_places=2, blank=True)),
                ('result', models.CharField(max_length=50, null=True, blank=True)),
                ('count', models.DecimalField(null=True, max_digits=10, decimal_places=2, blank=True)),
                ('deducted', models.DecimalField(null=True, max_digits=10, decimal_places=2, blank=True)),
                ('amount_after_result', models.DecimalField(null=True, max_digits=10, decimal_places=2, blank=True)),
                ('mode', models.CharField(max_length=10, null=True, blank=True)),
                ('end_of_round', models.CharField(max_length=20, null=True, blank=True)),
                ('dropped_after_auto_play', models.BooleanField(default=False)),
                ('game', models.ForeignKey(to='gamelogs.GameLogs')),
            ],
        ),
        migrations.CreateModel(
            name='TableLogs',
            fields=[
                ('tableid', models.IntegerField(null=True, blank=True)),
                ('gameid', models.AutoField(serialize=False, primary_key=True)),
                ('startdate', models.DateTimeField(null=True, blank=True)),
                ('enddate', models.DateTimeField(null=True, blank=True)),
                ('gametype', models.CharField(max_length=50, null=True, blank=True)),
                ('bet', models.CharField(max_length=10, null=True, blank=True)),
                ('min_player', models.IntegerField(null=True, blank=True)),
                ('max_player', models.IntegerField(null=True, blank=True)),
                ('status', models.BooleanField(default=False)),
                ('mode_type', models.CharField(max_length=50, null=True, blank=True)),
                ('fraud', models.BooleanField(default=False)),
            ],
        ),
        migrations.AddField(
            model_name='picksdiscard',
            name='table',
            field=models.ForeignKey(to='gamelogs.TableLogs'),
        ),
        migrations.AddField(
            model_name='gameplayers',
            name='table',
            field=models.ForeignKey(blank=True, to='gamelogs.TableLogs', null=True),
        ),
        migrations.AddField(
            model_name='gamelogs',
            name='table',
            field=models.ForeignKey(blank=True, to='gamelogs.TableLogs', null=True),
        ),
    ]
