class LogRouter(object):
    """
    A router to control all database operations on models in the
    log applications.
    """
    def db_for_read(self, model, **hints):
        """
        Attempts to read log models go to rummylogs.
        """
        if model._meta.app_label == 'gamelogs':
            return 'rummy'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write log models go to rummylogs
        """
        if model._meta.app_label == 'gamelogs':
            return 'rummy'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations in all models
        """
        return True

    def allow_syncdb(self, db, model):
        """
        All non-log models end up in this pool.
        """
        if db == 'rummylogs':
            return model._meta.app_label == 'gamelogs'
        elif model._meta.app_label == 'gamelogs':
            return False
        return None