from django.shortcuts import render,redirect
from gamelogs.forms import *
from django.contrib.admin.views.decorators import staff_member_required
from gamelogs.models import *
from datetime import datetime, timedelta
from userapp.function import *
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.views.generic.base import TemplateView
from django.utils.decorators import method_decorator
import json 

from userapp.utils import get_fromdate_todate_for_django_query_filter



@check_ip_required
@staff_member_required
def home_gamelogs(request):
	""" it is the home url for Gameslogs section """
	return render(request, 'gamelogs/gamelogs_home.html', locals() )

@check_ip_required
@staff_member_required
def home(request):
	if request.method == 'POST':
		form = SearchTableLogsForm( request.POST )
		if form.is_valid():
			# get all search fields
			gametype = form.cleaned_data['game_type']
			date = form.cleaned_data['date']
			mode = form.cleaned_data['mode']
			max_players = form.cleaned_data['max_players']
			query = TableLogs.objects
			# Hit query only if fields dont specify ALL
			if gametype is not 'ALL':
				query = query.filter(gametype = gametype)
			if mode is not 'ALL':
				query = query.filter(mode_type = mode)
			if max_players != 'ALL':
				query = query.filter(max_player = max_players)
			# Date specific Queries
			end_date = datetime.today()
			if date == 'Today':
				query = query.filter(enddate = end_date)
			elif date == 'Yesterday':
				query = query.filter(enddate = end_date - timedelta(1))
			elif date == 'Week Till Date':
				start_date = end_date - timedelta(7)
				query = query.filter(enddate__range = (start_date, end_date) )
			elif date == 'Month Till Date':
				start_date = end_date - timedelta(30)
				query = query.filter(enddate__range = (start_date, end_date) )
			else:
				# Custom datetime now check time fields here
				pass
			tablelogs = list(query)
			return render(request, 'gamelogs/gamelogs.html', locals() )
		else:
			return render(request, 'gamelogs/gamelogs.html', locals() )
	form = SearchTableLogsForm()
	return render(request, 'gamelogs/gamelogs.html', locals() )

def show_table(request, table_id):
	table = TableLogs.objects.get( pk = table_id )
	gamelog_list = table.gamelogs_set.all()
	return render(request, 'gamelogs/tableround.html', locals() )

def show_chat(request, game_id):
	game = GameLogs.objects.get( pk = game_id )
	chat_list = game.chatlog_set.all()
	return render(request, 'gamelogs/chat.html', locals() )

def show_players( request, game_id):
	game = GameLogs.objects.get( pk = game_id )
	if game.subgameid == 0:
		# subgameid tels if game is strikes or pool
		player_list = game.playerstrikes_set.all()
		strike = True
		return render(request, 'gamelogs/player.html', locals() )
	else:
		player_list = game.playerpools_set.all()
		return render(request, 'gamelogs/player.html', locals() )

def show_picks_discards( request, game_id):
	game = GameLogs.objects.get( pk = game_id )
	pd_list = game.picksdiscard_set.all()
	return render(request, 'gamelogs/pick_discard.html', locals() )


@check_ip_required
@staff_member_required
def display_chat_logs(request):
	resp = {}
	from_date, to_date = None, None
	if request.method == 'POST':
		resp_type = request.POST.get('restype', None)
		form = ChatLogsForm(request.POST)
		if form.is_valid():
			# get all search fields
			gametype = form.cleaned_data['gametype']
			chipstype = form.cleaned_data['chipstype']
			table_id = form.cleaned_data['table_id']
			game_id = form.cleaned_data['game_id']
			date_filter = form.cleaned_data['date']
			from_date = request.POST.get('datepicker1')
			to_date = request.POST.get('datepicker2')
			dates_dict = get_fromdate_todate_for_django_query_filter(date_filter,from_date,  to_date)
			from_date = dates_dict.get('fromdate')
			to_date = dates_dict.get('todate')
			chatlogs_list = ChatLog.objects.filter(chat_datetime__lte=to_date,
										   chat_datetime__gte=from_date).order_by('-id','-chat_datetime')
			# Hit query only if fields dont specify ALL
			if gametype != 'ALL':
				chatlogs_list = chatlogs_list.filter(gametype=gametype)
			if chipstype != 'ALL':
				chatlogs_list = chatlogs_list.filter(chipstype=chipstype)
			if table_id not in ('', None):
				chatlogs_list = chatlogs_list.filter(table_id=table_id)
			if game_id not in ('', None):
				chatlogs_list = chatlogs_list.filter(game_id=game_id)
		else:
			resp = {'message':"Please select valid details",'form':form }
			if resp_type == "json":
				resp = {'message':"Please try after some time",'status':'error' }
				return HttpResponse(json.dumps(resp))
			return render(request, 'gamelogs/gamelogs_chatlog.html', resp )
	else:
		resp_type = request.GET.get('restype', None)
		tableid = request.GET.get('tableid', None)
		gameid = request.GET.get('gameid', None)
		
		form = ChatLogsForm()
		dates_dict = get_fromdate_todate_for_django_query_filter(None, None, None)
		if gameid and tableid:
			chatlogs_list = ChatLog.objects.filter(table_id=tableid, game_id=gameid).order_by('-id','-chat_datetime')
			resp = {'chatlogs':chatlogs_list, 'count':len(chatlogs_list)}
		else:
			chatlogs_list = ChatLog.objects.filter(chat_datetime__lte=dates_dict.get('todate'),
											   chat_datetime__gte=dates_dict.get('fromdate')).order_by('-id','-chat_datetime')
			from_date = dates_dict.get('fromdate').strftime('%d/%m/%Y')
			to_date = dates_dict.get('todate').strftime('%d/%m/%Y')
			resp = {'chatlogs':chatlogs_list, 'count':len(chatlogs_list), 'fromdate':from_date,'todate':to_date,
				'form':form}
	if resp_type == "json":
		resp['status'] = "Success"
		from django.core import serializers

		# assuming obj is a model instance
		l = serializers.serialize('json', resp['chatlogs'])
		resp['chatlogs'] = l
		return HttpResponse(json.dumps(resp))
	resp = {'chatlogs':chatlogs_list, 'form':form, 'count':len(chatlogs_list), 'fromdate':from_date,'todate':to_date}
	return render(request, 'gamelogs/gamelogs_chatlog.html', resp)


def reload_chat_logs(request):
	from userapp.models import Limit
	c_logs_count = 500
	limit_obj = Limit.objects.get(id=1)
	reload_seconds = limit_obj.chatpage_reload_seconds
	todays_date = datetime.now().date()
	chatlogs_list = ChatLog.objects.filter(chat_datetime__day=todays_date.day,
							chat_datetime__month=todays_date.month,
							chat_datetime__year=todays_date.year).order_by('-id','-chat_datetime')[0:c_logs_count]
	resp = {'chatlogs':chatlogs_list, 'count':len(chatlogs_list), 'reload_seconds':reload_seconds}
	return render(request, 'gamelogs/chatlogs_reload.html', resp)