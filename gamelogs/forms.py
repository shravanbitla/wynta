from django import forms

GAMEVARIANT= (('ALL','ALL'),
            ('BEST_OF_2', 'BEST_OF_2'),
            ('BEST_OF_3', 'BEST_OF_3'),
            ('BEST_OF_5', 'BEST_OF_5'),
            ('BEST_OF_6', 'BEST_OF_6'),
            ('101_POOL', '101_POOL'),
            ('201_POOL', '201_POOL'),
            ('PR_JOKER', 'PR_JOKER'),
            ('PR_NOJOKER', 'PR_NOJOKER'),)

DATE = (
		('Today', 'Today'),            
        ('Yesterday', 'Yesterday'),
		('WeekTildate', 'WeekTildate'),
		('MonthTilldate', 'MonthTilldate'),
		('Custom', 'Custom'),
	)

MAX_PLAYERS = (
			('ALL', 'ALL'),
			(2, '2 Players'),
			(6, '6 Players'),
		)

MODE = (
		('ALL', 'ALL'),
        ('CASH_CASH', 'CASH_CASH'),
		('FUNCHIPS_FUNCHIPS', 'FUNCHIPS_FUNCHIPS'),
		('FUNCHIPS_CASH', 'FUNCHIPS_CASH'),
		
	)

BONUSCREDIT = (('CASH', 'CASH'),
               ('BONUS', 'BONUS'),
               ('FUNCHIPS', 'FUNCHIPS'),
               ('LOYALTYPOINTS', 'LOYALTYPOINTS'),
               ('SOCIALPOINTS', 'SOCIALPOINTS'),)

class SearchTableLogsForm(forms.Form):
    game_type 		= forms.ChoiceField(choices=GAMEVARIANT, required = True)
    date 			= forms.ChoiceField(choices=DATE, required = True)
    mode 			= forms.ChoiceField(choices=MODE, required = True)
    max_players 	= forms.ChoiceField(choices=MAX_PLAYERS, required = True)
    # Optional Fields
    time_from		= forms.TimeField(required = False)
    time_to 		= forms.TimeField(required = False)
    search 			= forms.CharField(required = False)
    
class ChatLogsForm(forms.Form):
    gametype        = forms.ChoiceField(choices=GAMEVARIANT, required = False)
    chipstype       = forms.ChoiceField(choices=MODE, required = False)
    table_id         = forms.IntegerField(required=False)
    game_id         = forms.IntegerField(required=False)
    date             = forms.ChoiceField(choices=DATE, required = False)
    
    
    

