from django.db import models

class TableLogs(models.Model):
	tableid 		= models.IntegerField(blank=True, null=True)
	gameid 			= models.AutoField(primary_key=True)
	startdate 		= models.DateTimeField(blank=True, null=True)
	enddate 		= models.DateTimeField(blank=True, null=True)
	gametype 		= models.CharField(max_length=50,blank=True, null=True)
	bet 			= models.CharField(max_length=10,blank=True, null=True)
	min_player 		= models.IntegerField(blank=True, null=True)
	max_player 		= models.IntegerField(blank=True, null=True)
	status 			= models.BooleanField(default=False,blank=True) #0 = Progress , 1 = completed
	mode_type 		= models.CharField(max_length=50,blank=True, null=True)
	fraud 			= models.BooleanField(default=False,blank=True) # 0 = Fraud , 1 = No Fraud

class GameLogs(models.Model):
	table 			= models.ForeignKey(TableLogs,blank=True, null=True)
	gameid			= models.IntegerField(blank=True, null=True)
	subgameid 		= models.IntegerField(blank=True, null=True)
	startdate 		= models.DateTimeField(blank=True,null=True)
	enddate 		= models.DateTimeField(blank=True, null=True)
	no_players 		= models.IntegerField(blank=True, null=True)
	prize_amount 	= models.DecimalField(max_digits=10, decimal_places=2,blank=True, null=True)
	rake 			= models.DecimalField(max_digits=10, decimal_places=2,blank=True, null=True)
	winnings 		= models.DecimalField(max_digits=10, decimal_places=2,blank=True, null=True)
	winner 			= models.CharField(max_length=50,blank=True, null=True)
	status 			= models.BooleanField(default =False,blank=True)

	def __unicode__(self):
		return str(self.gameid)

class GamePlayers(models.Model):
	table 			= models.ForeignKey(TableLogs,blank=True, null=True)
	game 			= models.ForeignKey(GameLogs,blank=True, null=True)
	subgameid 		= models.IntegerField(blank=True, null=True)
	playername 		= models.CharField(max_length=50,blank=True, null=True)
	toss_position 	= models.IntegerField(blank=True, null=True)
	toss_card 		= models.CharField(max_length=3,blank=True, null=True)
	player_status  	= models.CharField(max_length=20,blank=True, null=True)
	score_amount 	= models.DecimalField(max_digits=10, decimal_places=2,blank=True, null=True)

	def __unicode__(self):
		return self.playername

class PicksDiscard(models.Model):
	table 			= models.ForeignKey(TableLogs)
	game 			= models.ForeignKey(GameLogs)
	turns 			= models.CharField(max_length=10,blank=True, null=True)
	playername 		= models.CharField(max_length=50,blank=True, null=True)
	status 			= models.CharField(max_length=20,blank=True, null=True)
	action 			= models.CharField(max_length=50,blank=True, null=True)
	picked_card 	= models.CharField(max_length=3,blank=True, null=True)
	picked_from 	= models.CharField(max_length=2,blank=True, null=True) #CD/OD
	internrt_connection = models.BooleanField(default=False,blank=True) #0 = Connected 1 = Disconnected
	autoplay 		= models.BooleanField(default=False,blank=True) # 0 = No 1 = Yes

"""
class ChatLog(models.Model):
	table   		= models.ForeignKey(TableLogs,blank=True, null=True)
	game 			= models.ForeignKey(GameLogs,blank=True, null=True)
	playername 		= models.CharField(max_length=50,blank=True, null=True)
	message 		= models.CharField(max_length=255,blank=True, null=True)
	chat_datetime 	= models.DateTimeField(blank=True, null=True)
"""

class ChatLog(models.Model):
	table_id   		= models.IntegerField(blank=True, null=True)
	game_id 		= models.IntegerField(blank=True, null=True)
	playername 		= models.CharField(max_length=50,blank=True, null=True)
	message 		= models.CharField(max_length=255,blank=True, null=True)
	chat_datetime 	= models.DateTimeField(blank=True, null=True)
	gametype        = models.CharField(max_length=20,blank=True, null=True)
	chipstype       = models.CharField(max_length=50,blank=True, null=True) 

class PlayerStrikes(models.Model):
	game 			= models.ForeignKey(GameLogs)
	playername 		= models.CharField(max_length=50,blank=True, null=True)
	toss_position 	= models.IntegerField(blank=True, null=True)
	toss_card 		= models.CharField(max_length=3,blank=True, null=True)
	player_status 	= models.CharField(max_length=50,blank=True, null=True)
	amount_on_table = models.DecimalField(max_digits=10, decimal_places=2,blank=True, null=True)
	result 			= models.CharField(max_length=50,blank=True, null=True)
	count 			= models.DecimalField(max_digits=10, decimal_places=2,blank=True, null=True)
	deducted 		= models.DecimalField(max_digits=10, decimal_places=2,blank=True, null=True)
	amount_after_result = models.DecimalField(max_digits=10, decimal_places=2,blank=True, null=True)
	mode 			= models.CharField(max_length=10,blank=True, null=True)
	end_of_round 	= models.CharField(max_length=20,blank=True, null=True) # 0 = Exists 1 = left
	dropped_after_auto_play = models.BooleanField(default=False,blank=True)  # 0 = Yes 1 = No

class PlayerPools(models.Model):
	game 			= models.ForeignKey(GameLogs,blank=True, null=True)
	playername 		= models.CharField(max_length=50,blank=True, null=True)
	toss_position 	= models.IntegerField(blank=True, null=True)
	toss_card 		= models.CharField(max_length=3,blank=True, null=True)
	player_status 	= models.CharField(max_length=50,blank=True, null=True)
	table_start_score = models.DecimalField(max_digits=10, decimal_places=2,blank=True, null=True)
	result 			= models.CharField(max_length=50,blank=True, null=True)
	count 			= models.DecimalField(max_digits=10, decimal_places=2,blank=True, null=True)
	mode 			= models.CharField(max_length=10,blank=True, null=True)
	score 			= models.DecimalField(max_digits=10, decimal_places=2,blank=True, null=True)
	end_of_round 	= models.CharField(max_length=20,blank=True, null=True) # 0 = Exists 1 = left
	dropped_after_auto_play = models.BooleanField(default=False,blank=True)  # 0 = Yes 1 = No




