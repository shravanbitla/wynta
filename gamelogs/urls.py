from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
	url(r'^tablelogs/(?P<table_id>[0-9]+)/$', 'gamelogs.views.show_table', name = 'show_table'),
	url(r'^chatlogs/(?P<game_id>[0-9]+)/$', 'gamelogs.views.show_chat', name = 'show_chat'),
	url(r'^players/(?P<game_id>[0-9]+)/$', 'gamelogs.views.show_players', name = 'show_players'),
	url(r'^picks/discards/(?P<game_id>[0-9]+)/$', 'gamelogs.views.show_picks_discards', name = 'show_picks_discards'),
	url(r'^display_chat_logs/$', 'gamelogs.views.display_chat_logs', name = 'gamelogs_display_chat_logs'),
	url(r'^reload_chat_logs/$', 'gamelogs.views.reload_chat_logs', name = 'gamelogs_reload_chat_logs'),
	url(r'gamelogs_home/$', 'gamelogs.views.home_gamelogs',name="home_gamelogs"),
	url(r'^gamelogs/$', 'gamelogs.views.home', name = 'gamelogshome'),
	)