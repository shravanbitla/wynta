try: import simplejson as json
except ImportError: import json
from decimal import Decimal
from datetime import datetime, date

class PrettyFloat(float):
    def __repr__(self):
        return '%.1f' % self

def pretty_floats(obj):
    if isinstance(obj, long):
        return PrettyFloat(obj)
    if isinstance(obj, float):
        return PrettyFloat(obj)
    elif isinstance(obj, dict):
        return dict((k, pretty_floats(v)) for k, v in obj.items())
    elif isinstance(obj, (list, tuple)):
        return map(pretty_floats, obj)             
    elif isinstance(obj, Decimal):
        return PrettyFloat(obj)  
    elif isinstance(obj, datetime):
        if obj.year < 1900:
            return "NA"
        else:
            return obj.strftime('%d-%m-%Y')
    elif isinstance(obj, date):
        try:
            obj = obj.strftime('%d-%m-%Y')
        except:
            obj = str(obj)                    
    return obj

def dumps(obj, indent=None):
    return json.dumps(pretty_floats(obj), indent=indent)
    
def loads(inp):
    return json.loads(inp)
