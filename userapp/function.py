""" setting data
"""
from datetime import datetime, timedelta
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response
from django.template import RequestContext

from django.conf import settings
import logging
LOGGER = logging.getLogger(__name__)
from django.http import HttpResponseRedirect
from django.core.validators import EmailValidator
KMP = "%H:%M:%S"
ONE = datetime.strptime("00:00:00", KMP).time()
TWO = datetime.strptime("02:59:59", KMP).time()
THREE = datetime.strptime("03:00:00", KMP).time()
FOUR = datetime.strptime("05:59:59", KMP).time()
FIVE = datetime.strptime("06:00:00", KMP).time()
SIX = datetime.strptime("08:59:59", KMP).time()
SEVEN = datetime.strptime("09:00:00", KMP).time()
EIGHT = datetime.strptime("11:59:59", KMP).time()
NINE = datetime.strptime("12:00:00", KMP).time()
TEN = datetime.strptime("14:59:59", KMP).time()
ELEVEN = datetime.strptime("15:00:00", KMP).time()
TWELVE = datetime.strptime("17:59:59", KMP).time()
THIRTEEN = datetime.strptime("18:00:00", KMP).time()
FOURTEEN = datetime.strptime("20:59:59", KMP).time()
FIFTEEN = datetime.strptime("21:00:00", KMP).time()
SIXTEEN = datetime.strptime("23:59:59", KMP).time()


def updatelevel(obj):
    """
     function to update level if player is getting
     loyalty points
    """
    from userapp.models import Limit
    limit = Limit.objects.get(id=1)
    if obj.totalloyaltypoints >= limit.limitforloyaltylevel2:
        obj.playerlevel = 2
    if obj.totalloyaltypoints >= limit.limitforloyaltylevel3:
        obj.playerlevel = 3
    if obj.totalloyaltypoints >= limit.limitforloyaltylevel4:
        obj.playerlevel = 4
    if obj.totalloyaltypoints >= limit.limitforloyaltylevel5:
        obj.playerlevel = 5
    obj.save()


def is_valid_email(email):
    """
     check email format
    """
    if EmailValidator(email):  # abc@xyz.com
        return True
    return False


def changedateformat(olddate):
    """ python date to javascript date format """
    return str(datetime(olddate.split('/')[0],
                        olddate.split('/')[1],
                        olddate.split('/')[2]))
    

def reconcileentry(**kwargs):
    """ reconcile report filling """
    from userapp.models import  Reconcilelogs
    Reconcilelogs.\
                  objects.create(playerid=kwargs.get('obj'),
                   timestamp=datetime.now(),
                   transaction_type=kwargs.get('direction'),
                   amounttype=kwargs.get('amounttype'),
                   amount=kwargs.get('amount'),
                   toplayerbalance=(kwargs.get('chips')+kwargs.get('inplay')),
                   toinplay=kwargs.get('inplay'),
                   tonetbalance=kwargs.get('chips'),
                   status=kwargs.get('status'),
                   orderid=kwargs.get('orderid'),
                   ipaddress=kwargs.get('ipaddress'))
    


def verifydate(date):
    """ to convert format of data to python format"""
    if date:
        fwmonth, fwday, fwyear = date.split('/')
        fwmonth, fwday, fwyear = int(fwmonth), int(fwday), int(fwyear) 
        from datetime import date
        date = date(fwyear, fwmonth, fwday)
        return date
    else:
        date = '9999-12-31'
        return date


def verifydatesearch(i_date):
    """ to convert format of data to python format"""
    if i_date and i_date not in [None,"None",0]:
        fwmonth, fwday, fwyear = i_date.split('/')
        fwmonth, fwday, fwyear = int(fwmonth), int(fwday), int(fwyear) 
        from datetime import date
        date = date(fwyear, fwmonth, fwday)
        return date

    else:
        from datetime import date
        date = date(9998, 12, 31)
        return date


def datereturn(date):
    """ convert date to html format"""
    if date:
        year, month, day = date.split('-')
        date = [month, day, year]
        date = '/'.join(date)
        return date
    else:
        date = "31-12-9999"
        return date


def myuser_login_required(func1):
    """ decorator for checking player is logged into website or not """
    def wrap(request, *args, **kwargs):
        """check the session if id key not exist,redirect to home """
        if 'id' not in request.session.keys():
            return HttpResponseRedirect('/')
            #return HttpResponseRedirect(reverse("presignin"))
        return func1(request, *args, **kwargs)
    wrap.__doc__ = func1.__doc__
    wrap.__name__ = func1.__name__
    return wrap


def check_ip_required(func1):
    """check ip address of each admin login """
    
    def wrap(request, *args, **kwargs):
        """ process function"""
        from userapp.models import UserProfile, Failedlogintry
        from django.http import HttpResponse
        from django.contrib.auth.views import logout
        from django.contrib.auth.models import User
        from userapp.models import Playerinfo
        loginip = str(request.META['REMOTE_ADDR'])
        iplist = []
        playeriplist = []

        try:
            userprofileobj = UserProfile.objects.filter(ip__contains=loginip)
            for i in userprofileobj:
                iplist.append(str(i.ip))
            if loginip not in iplist:
                LOGGER.info("Failed IP=%s", loginip)
                try:
                    user = User.objects.get(username=request.user)
                except User.DoesNotExist:
                    user = None
                sessionid = request.session.get('id', None)
                obj = None
                if sessionid:
                    try:
                        obj = Playerinfo.objects.get(id=sessionid)
                    except:
                        pass
                Failedlogintry.objects.create(user=user,ip=request.META['REMOTE_ADDR'],
                                              playerid=obj)
                
                return render_to_response('404.html',
                                          locals(),
                                          context_instance=RequestContext(request))#HttpResponseRedirect("/")
            user = None
            try:
                user = User.objects.get(username=request.user)
            except User.DoesNotExist:
                pass
                #here session flag code
            if user:
                userprofileobj = UserProfile.objects.filter(user=user)
                for i in userprofileobj:
                    playeriplist.append(str(i.ip))
                if len(playeriplist) is 0:
                    logout(request)
                    Failedlogintry.objects.create(user=user,
                                              ip=request.META['REMOTE_ADDR'])
                    return HttpResponse("Please Ask to add Your IP address.")
                if loginip not in playeriplist:
                    logout(request)
                    Failedlogintry.objects.create(user=user,
                                              ip=request.META['REMOTE_ADDR'])
                    return HttpResponse("Please Ask to Edit/Add IP Address.")
        except Exception as e:
            print e
            return HttpResponse("You Don't have access to login. Please Ask support to Edit/Add IP Address")
        return func1(request, *args, **kwargs)

    wrap.__doc__ = func1.__doc__
    wrap.__name__ = func1.__name__
    return wrap

def paginate_lis(page, list_len, is_next=False, is_prev=False, number_of_items = 100):
    if not page or page < 0:
        page = 0
    else:
        page = int(page) - 1
    next_page = False
    
    pages_to_display = 5
    no_of_pages= list_len/number_of_items
    if list_len%number_of_items > 0:
        no_of_pages = no_of_pages + 1
    
    start_page = (page/pages_to_display)*pages_to_display
    end_page = start_page + pages_to_display
    
    
    if is_next:
        start_page = start_page + pages_to_display
        end_page = start_page + pages_to_display
        if end_page > no_of_pages:
            end_page = no_of_pages
        page = start_page
    if is_prev:
        start_page = ((page/pages_to_display)-1)*pages_to_display
        if start_page < 0:
            start_page = 0
        end_page = start_page + pages_to_display
        page = start_page
    if no_of_pages > end_page :
        next_page = True
    #start_record = page * number_of_items
    #azeem
    start_record = page * number_of_items
    if page != 0:
        start_record = start_record - 1
    #azeem
    if start_record == 0:
        end_record = (1 * number_of_items)
    else:
        end_record = (start_record + number_of_items) + 1
        start_record = start_record + 1
    if end_page > no_of_pages:
            end_page = no_of_pages
    #print start_record, end_record, start_page, end_page, next_page
    return start_record, end_record, start_page, end_page, next_page, page+1

def get_client_ip(request):
    try:
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[-1].strip()
        else:
            ip = request.META.get('REMOTE_ADDR')
        return ip
    except Exception as e:
        print e
        return
