"""models.py"""
from decimal import Decimal
import urllib
from datetime import datetime, timedelta
from django.db.models import Q
from django.db import transaction
from django.db.models.signals import pre_save, post_save, post_delete, pre_delete
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.http import HttpResponse, HttpResponseRedirect
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator, MaxLengthValidator
from userapp.mails import *
from userapp.function import * 
import requests
from json_field import JSONField
from django.core.cache import cache
from django.utils import timezone

MESSAGETRIGGEREVENT = (('REGISTRATION', 'REGISTRATION'),
                       ('EMAIL_CHANGE', 'EMAIL_CHANGE'),
                       ("PASSWORD_RESET", "PASSWORD_RESET"),
                       ("REFER_SUB_AFFILIATE", "REFER_SUB_AFFILIATE"),
                       ("NEW_AFFILIATE", "NEW_AFFILIATE"),
                       ("AFFILIATE_SIGN_UP_APPROVAL", "AFFILIATE_SIGN_UP_APPROVAL"),
                       ("AFFILIATE_SIGN_UP_REJECTED", "AFFILIATE_SIGN_UP_REJECTED"),
                       ("CAMPAIGN_STATUS_CHANGE", "CAMPAIGN_STATUS_CHANGE"),
                       ("CAMPAIGN_PAYOUT_CHANGE", "CAMPAIGN_PAYOUT_CHANGE"),
                       ("NEW_ADVERTISER", "NEW_ADVERTISER"),
                       ("ADVERTISER_SIGN_UP_APPROVAL", "ADVERTISER_SIGN_UP_APPROVAL"),
                       ("ADDUSER", "ADDUSER"),
                       ("INVITE_AFFILIATE", "INVITE_AFFILIATE"),
                       ("Jumpman_Brand_Setup", "Jumpman_Brand_Setup"),
                       ("Jumpman_Client_Email", "Jumpman_Client_Email"),
                       )
class EnumField(models.Field):
    """
    custom Enum Field
    """

    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 104
        super(EnumField, self).__init__(*args, **kwargs)
        if not self.choices:
            raise AttributeError('EnumField requires `choices` attribute.')

    def db_type(self, connection):
        return "enum(%s)" % ','.join("'%s'" % k for (k, _) in self.choices)


class SET(models.Field):

    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 104
        super(SET, self).__init__(*args, **kwargs)
        if not self.choices:
            raise AttributeError('set requires `choices` attribute.')

    def db_type(self, connection):
        return "set(%s)" % ','.join("'%s'" % k for (k, _) in self.choices)


class Device_info(models.Model):
    id = models.AutoField(primary_key=True)
    device_type = models.CharField(max_length=30, null=True, blank=True)
    client_type = models.CharField(max_length=30, null=True, blank=True)
    operating_system = models.CharField(max_length=30, null=True, blank=True)
    
    @property
    def get_channel(self):
        return "Web" if self.device_type in [None, "None","Web"] else self.device_type
    
    @property
    def get_subchannel(self):
        return "Browser" if self.client_type in ['None', "WebUI",None] else self.client_type

    def __unicode__(self):
        """ return Playerinfo obj"""
        return str(self.device_type)


class Network(models.Model):
    JOB = (
        ("POST_BACK", "POST_BACK"),
        ("API_REPORT", "API_REPORT"),
        ("UPLOAD", "UPLOAD"),
        ("FTP", "FTP"),
    )
    networkid = models.IntegerField(primary_key=True)
    networkname = models.CharField(max_length=50)
    connect_type = models.CharField(max_length=50, choices=JOB)
    def __str__(self):
        return str(self.networkname)

class NetworkAndSiteMap(models.Model):
    networkid = models.ForeignKey('userapp.Network', db_column="networkid")
    siteid = models.ForeignKey(Site, db_column="siteid")
    logo = models.FileField(upload_to="sites/", null=True, blank=True)

class Country(models.Model):
    """ table to store country name. i created this gable because Ip bloacking based on country and
    state
    """
    countryname = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return str(self.countryname)

class State(models.Model):
    """" table to store state name of any player
    use to ip blocking based on place"""
    country = models.ForeignKey(Country)
    statename = models.CharField(max_length=50)

    def __str__(self):
        return str(self.statename)

class AffiliateProgramme(models.Model):
    """
    It will be a collection of Affiliate Programe.
    """
    SSL_STATE = (("NO","NO"),
            ("STARTED","STARTED"),
            ("FINISHED","FINISHED"),
            ("EXPIRED", "EXPIRED"),)
    name = models.CharField(max_length=100, unique=True)
    domain = models.CharField(max_length=100, null=True, blank=True)
    wynta_domain = models.CharField(max_length=200, null=True, blank=True)
    logo = models.FileField(upload_to="affprog/", null=True, blank=True)
    bgcolor = models.CharField(max_length=30, null=True, blank=True)
    bgtext = models.CharField(max_length=30, null=True, blank=True)
    buttonimage = models.TextField(null=True, blank=True)
    buttoncolor = models.TextField(null=True, blank=True)
    buttontext = models.CharField(max_length=30, null=True, blank=True)
    tablecolor = models.CharField(max_length=30, null=True, blank=True)
    tabletext = models.CharField(max_length=30, null=True, blank=True)
    networkid = models.ManyToManyField(Network, db_column='networkid', null=True, blank=True)
    ssl_state = models.CharField(max_length=30, choices=SSL_STATE, default='NO')
    # order = models.IntegerField(null=True, blank=True)
    accessible = models.BooleanField(default=True)

    def __str__(self):
        """Return subject"""
        return str(self.name)

    def __unicode__(self):
        """ return Playerinfo obj"""
        return str(self.name)

    def combined_fields(self):
        if self.wynta_domain:
            return '<a href="http://%s" target="_blank">Login</a>'%(self.wynta_domain + '/admin/sdignin?aid='+str(self.id))
        else:
            return '<a href="//%s" target="_blank">Login</a>'%(self.domain + '/admin/sdignin?aid='+str(self.id))


class MessageTemplate(models.Model):
    """
    To Manage Email templates in wynta
    """
    subject = models.CharField(max_length=100)
    content = models.TextField()

    def __str__(self):
        """Return subject"""
        return str(self.subject)

    def __unicode__(self):
        """ return Playerinfo obj"""
        return str(self.subject)

class CurrencyConverted(models.Model):
    """
    It will currency all email templates at predefined conditioned
    """
    eurtogbp = models.FloatField(null=True, blank=True)
    gbptoeur = models.FloatField(null=True, blank=True)
    date = models.DateField()

    def __str__(self):
        """Return subject"""
        return str(self.date)
    def __unicode__(self):
        """ return Playerinfo obj"""
        return str(self.date)

class MessageTrigger(models.Model):
    """
    It will trigger all email templates at predefined conditioned
    """
    affiliateprogramme = models.ForeignKey(AffiliateProgramme, db_column="affiliateprogramme", null=True, blank=True)
    templates = models.ForeignKey(MessageTemplate)
    triggercondition = EnumField(choices=MESSAGETRIGGEREVENT)
    status = models.BooleanField(default=False)

    def __str__(self):
        """Return subject"""
        return str(self.triggercondition)
    def __unicode__(self):
        """ return Playerinfo obj"""
        return str(self.triggercondition)

class AccountManager(models.Model):
    STATUS = (("Trial","Trial"),
            ("Aborted","Aborted"),
            ("Basic","Basic"),
            ("Advanced", "Advanced"),
            ("Premium","Premium"),
            ("Custom", "Custom"),)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    # user = models.ForeignKey('auth.user')
    affiliateprogramme = models.ForeignKey(AffiliateProgramme, db_column="affiliateprogramme")
    timestamp = models.DateTimeField(default=timezone.now)
    email = models.EmailField(unique=True)
    amcactive = models.BooleanField(default=True)
    pmcactive = models.BooleanField(default=False)
    status = models.CharField(max_length=30, choices=STATUS, default='Trial')
    siteid = models.ManyToManyField(Site, null=True,blank=True)
    rev_percent_cut = models.FloatField(default=0.00)

    def __unicode__(self):
        """ return Playerinfo obj"""
        return str(self.user.username)

class SiteOrder(models.Model):
    accountmanager_id = models.ForeignKey(AccountManager, db_column="accountmanager_id", null=True, blank=True)
    site_id = models.OneToOneField(Site, db_column='site_id', null=True,blank=True)
    order_id = models.IntegerField(null=True,blank=True)
    

class SuperAdmin(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    # user = models.ForeignKey('auth.user')
    timestamp = models.DateTimeField(default=timezone.now)
    email = models.EmailField(unique=True)

    def __unicode__(self):
        """ return Playerinfo obj"""
        return str(self.user.username)

class NetworkAffiliateProgrammeMapping(models.Model):
    networkid = models.ForeignKey("userapp.Network", db_column='networkid', null=True, blank=True)
    programme = models.ForeignKey(AffiliateProgramme, db_column="programmeid")
    accmanager = models.ForeignKey(AccountManager, db_column="accmanager", null=True, blank=True)
    siteid = models.ForeignKey(Site, db_column="siteid", null=True, blank=True)
    username = models.CharField(max_length=30, null=True, blank=True)
    password = models.CharField(max_length=30, null=True, blank=True)
    retrive = models.BooleanField(default=False)

class PartnerUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    # user = models.ForeignKey('auth.user')
    programme = models.ForeignKey(AffiliateProgramme, db_column="programmeid")
    accmanager = models.ForeignKey(AccountManager, db_column="accmanager", null=True, blank=True)
    timestamp = models.DateTimeField(default=timezone.now)
    email = models.EmailField(unique=True)
    options = JSONField(null=True, blank=True)
    role = models.CharField(max_length=50, null=True, blank=True)

    def __unicode__(self):
        """ return Playerinfo obj"""
        return str(self.user.username)
        
class Notifications(models.Model):
    accmanager = models.ForeignKey(AccountManager, db_column="accmanager", null=True, blank=True)
    affinfo = models.ForeignKey("affiliate.Affiliateinfo", db_column="affinfo", null=True, blank=True)
    notificationtype = models.CharField(max_length=50, null=True, blank=True)
    title =models.CharField(max_length=250, null=True, blank=True)
    content = models.CharField(max_length=5000, null=True, blank=True)
    redirecturl = models.CharField(max_length=250, null=True, blank=True)
    lastupdated = models.CharField(max_length=50, null=True, blank=True)
    lastseen = models.CharField(max_length=50, null=True, blank=True)
    category = models.CharField(max_length=50, null=True, blank=True)


class RegistrationPageEdit(models.Model):
    accmanager = models.ForeignKey(AccountManager, db_column='accmanager', null=True, blank=True)
    options = JSONField(null=True, blank=True)


class Emailclient(models.Model):
    accmanager = models.ForeignKey(AccountManager, db_column="accmanager", null=True, blank=True)
    username = models.CharField(max_length=30, null=True, blank=True)
    password = models.CharField(max_length=30, null=True, blank=True)
    emailclient = models.CharField(max_length=30, null=True, blank=True)
    domain = models.CharField(max_length=30, null=True, blank=True)
    apikey = models.CharField(max_length=500, null=True, blank=True)
    partuser = models.ForeignKey(PartnerUser, db_column="partuser", null=True, blank=True)

class Onelogin(models.Model):
    account = JSONField(null=True, blank=True)
    ipaddress = models.CharField(max_length=250, null=True, blank=True)
    timestamp = models.DateTimeField(default=timezone.now)