""" admin.py """
import time
import datetime
import urllib
from django.contrib import admin
from django.db import transaction
from django.conf import settings
from django.contrib.sites.models import Site

#azeem
from userapp.models import *

import logging
from django.contrib.auth.models import  User
logger = logging.getLogger(__name__)





# def utc_to_local_datetime( utc_datetime ):
#     """ convert utc to local timezone """
#     delta = utc_datetime - EPOCH_DATETIME
#     utc_epoch = SECONDS_PER_DAY * delta.days + delta.seconds
#     time_struct = time.localtime( utc_epoch )
#     dt_args = time_struct[:6] + (delta.microseconds,)
#     return datetime.datetime( *dt_args )


# def toUTC(t):
#     """ convert local timezone to UTC"""
#     struct_time= time.strptime(t,"%Y-%m-%d %H:%M:%S")
#     sec=time.mktime(struct_time)
#     struct_time=time.gmtime(sec)
#     mysql_time=time.strftime("%Y-%m-%d %H:%M:%S",struct_time)
#     return mysql_time


class CountryAdmin(admin.ModelAdmin):
    """
    class to activate country admin class
    """
    list_display = ['countryname']
admin.site.register(Country , CountryAdmin)  # comment this line on live server

class NetworkAdmin(admin.ModelAdmin):
    """
    class to activate Network admin class
    """
    list_display = ['networkname']

admin.site.register(Network , NetworkAdmin)  # comment this line on live server

class AffiliateProgrammeAdmin(admin.ModelAdmin):
    """
    class to activate AffiliateProgramme admin class
    """
    list_display = [ 'name', 'domain','redirect']
    def redirect(self, obj):
        return '<a href="http://%s/admin/sdignin/?aid=%s" target="_blank">Login</>'%(obj.wynta_domain, obj.id)

    redirect.short_description = 'Admin Login'
    redirect.allow_tags = True

admin.site.register(AffiliateProgramme , AffiliateProgrammeAdmin)  # comment this line on live server

class NetworkSiteMapAdmin(admin.ModelAdmin):
    """
    class to activate Network admin class
    """
    list_display = ['get_networkname', 'get_sitename']
    
    def get_sitename(self, obj):
        return obj.siteid.name

    def get_networkname(self, obj):
        return obj.networkid.networkname

admin.site.register(NetworkAndSiteMap , NetworkSiteMapAdmin)  # comment this line on live server

class StateAdmin(admin.ModelAdmin):
    """ this call will provide a template from which we can block any state
    player coming from those state can not register.
    """
    list_display = ['country', 'statename']
admin.site.register(State, StateAdmin)

class NetworkAffiliateProgrammeMappingAdmin(admin.ModelAdmin):
    list_display = ['networkid', 'programme']

admin.site.register(NetworkAffiliateProgrammeMapping, NetworkAffiliateProgrammeMappingAdmin)

class SuperAdminAdmin(admin.ModelAdmin):
    search_fields = ['user__username','user__id']
    list_display = [ 'user', 'timestamp']

admin.site.register(SuperAdmin, SuperAdminAdmin)

class PartnerUserAdmin(admin.ModelAdmin):
    search_fields = ['user__username','user__id']
    list_display = [ 'user', 'timestamp']

admin.site.register(PartnerUser, PartnerUserAdmin)

class AccountManagerAdmin(admin.ModelAdmin):
    search_fields = ['user__username','user__id']
    list_display = [ 'user', 'timestamp']
    filter_horizontal = ('siteid',)

admin.site.register(AccountManager, AccountManagerAdmin)

class MessagesTemplateAdmin(admin.ModelAdmin):
    """class to activate template for messsage template """
    list_display = ['subject', 'id']

    class Media:
        """ to call tinymce javascript"""
        js = ("/static_files/js/tiny_mce/tiny_mce.js", '/static_files/js/textareas.js')

admin.site.register(MessageTemplate, MessagesTemplateAdmin)


class MessageTriggerAdmin(admin.ModelAdmin):
    """ EMAIL TRIGGERS CREATION """
    list_display = [ 'affiliateprogramme', 'triggercondition',"template_subject", 'status']


    def template_subject(self, obj):
        return obj.templates.subject

admin.site.register(MessageTrigger, MessageTriggerAdmin)


class SiteOrderAdmin(admin.ModelAdmin):
    """ EMAIL TRIGGERS CREATION """
    list_display = [ 'get_sitename', 'get_networkname',"order_id"]

    def get_sitename(self, obj):
        return obj.site_id.name

    def get_networkname(self, obj):
        return obj.accountmanager_id.affiliateprogramme.name

admin.site.register(SiteOrder, SiteOrderAdmin)
# #azeem
# class SMSTemplateAdmin(admin.ModelAdmin):
#     """class to activate template for messsage template """
#     list_display = ['content','subject']
# admin.site.register(SMSTemplate, SMSTemplateAdmin)


# class SMSTriggerAdmin(admin.ModelAdmin):
#     """ EMAIL TRIGGERS CREATION """
#     list_display = ['networkid',"site" ,'triggercondition', 'template_subject','status']
#     fieldsets = [
#         ('Network Selection' , {'fields': ['networkid']}),
#         ('Site Selections', {'fields': ['siteid']}),
#         ('SMS Management', {'fields': ['templates','triggercondition','status']}),
#     ]
#     filter_horizontal = ('siteid',)
#     def site(self, obj):
#         return "\n- ".join([p.name for p in obj.siteid.all()])
    
#     def template_subject(self, obj):
#         return obj.templates.subject

# admin.site.register(SMSTrigger, SMSTriggerAdmin)

# #

# class SMSSenderIDAdmin(admin.ModelAdmin):
#     list_display = [ 'site', 'sendername','accounttype']
#     """
#     def has_add_permission(self, request, obj=None):
#         return False

#     def has_delete_permission(self, request, obj=None):
#         return False
#     """
# admin.site.register(SMSSenderID, SMSSenderIDAdmin)

# class AccountManagerAdmin(admin.ModelAdmin):
#     search_fields = ['user__username','user__id', 'siteid__id']
#     filter_horizontal = ('networkid','siteid',)
#     exclude = ['operator',]
#     list_display = [ 'user', 'timestamp','site_names', 'add_players']

#     def add_players(self, obj):

#         return '<a href="/egcs/account-manager/?id=%s" target="_blank">Update Players</>' %obj.id
#     add_players.short_description = 'Add Players'
#     add_players.allow_tags = True


#     def site_names(self, obj):
#         return ' - '.join([a.name for a in obj.siteid.all()])
#     site_names.short_description = "Sites"
#     def save_model(self, request, obj, form, change):
#         """ save egcs admin """
#         obj.operator = request.user
#         obj.save()
# admin.site.register(AccountManager, AccountManagerAdmin)