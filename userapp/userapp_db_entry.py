from userapp.models import *
from django_user_agents.utils import get_user_agent
from userapp.utils import error_log


def get_channels_list():
    all_devices = Device_info.objects.all()
    channel_list  = set( [i.device_type for i in all_devices if i.device_type not in ['None']])
    channel_list = list(channel_list)
    channel_list.insert(0, 'All')
    return channel_list

def get_selected_channel_devices_info_objs(channel):
    if channel and channel.lower() == "web":
        channel_list = ["web", "None"]
    else:
        channel_list = [channel]
    df_list  = Device_info.objects.filter(device_type__in=channel_list)
    return df_list

def get_device_info_obj(channel, subchannel=None):
    if not subchannel:
        subchannel = "WebUI"
    dif = Device_info.objects.filter(device_type=channel, 
                                   client_type=subchannel)
    if dif:
        dif = dif[0]
    else:
        dif = Device_info.objects.filter(device_type="Web", 
                               client_type="WebUI")
    return dif


class RequestAgentDetails(object):
    def __init__(self, request):
        self.request = request
        try:
            user_agent = get_user_agent(request)
        except Exception as e:
            error_log()
            user_agent = None
        self.device_type = "WEB"
        self.browser_type = None
        self.channel = "Web"
        self.subchannel = "WebUI" 
        self.brand = None
        self.device_family = None 
        self.browser_family = None 
        self.os_family = None
        if user_agent:
            self.browser_type = '%s(%s)'%(user_agent.browser.family, user_agent.browser.version_string)
            if user_agent.is_mobile:
                self.channel = "Mobile"
                self.device_type = "MOBILE"
            elif user_agent.is_tablet:
                self.channel = "Tablet"
                self.device_type = "TABLET"
            if hasattr(user_agent.device, 'brand'):
                self.brand = user_agent.device.brand
            if hasattr(user_agent.device, 'family'):
                self.device_family = user_agent.device.family
            if hasattr(user_agent.browser, 'family'):
                self.browser_family = user_agent.browser.family
            if hasattr(user_agent.os, 'family'):
                self.os_family = user_agent.os.family

class UniqueLoginDeviceDetailsObject(object):
    def __init__(self, loginon_obj, user_agent):
        self.playerobj = loginon_obj.playerid
        self.loginon_obj = loginon_obj
        self.user_agent = user_agent
        self.device_info_obj = get_device_info_obj(self.user_agent.channel, self.user_agent.subchannel)
        fresh_login = self.check_fresh_login()
        if fresh_login:
            self.loginon_obj.fresh_login = True
        else:
            self.loginon_obj.fresh_login = False
        self.loginon_obj.device_info_id = self.device_info_obj
        self.loginon_obj.brand = self.user_agent.brand
        self.loginon_obj.device_family = self.user_agent.device_family
        
        self.loginon_obj.os_family = self.user_agent.os_family
        self.loginon_obj.save()

    def check_fresh_login(self):
        uldd_count = Loginon.objects.filter(playerid=self.playerobj,
                                            logintime=self.loginon_obj.logintime,
                                            device_info_id=self.device_info_obj,
                                            loginok=True).count()

        if uldd_count > 1:
            return False
        else:
            return True


class OrderDeviceDetailsObject(object):
    def __init__(self, order_obj, user_agent):
        self.playerobj = order_obj.playerid
        self.order_obj = order_obj
        self.user_agent = user_agent
        self.odd = OrderDeviceDetails()
        self.odd.orderid = self.order_obj
        self.odd.device_info_id = get_device_info_obj(self.user_agent.channel, self.user_agent.subchannel)
        self.odd.brand = self.user_agent.brand
        self.odd.device_family = self.user_agent.device_family
        self.odd.browser_family = self.user_agent.browser_family 
        self.odd.os_family = self.user_agent.os_family
        self.odd.save()