from django.db import models
from django.contrib.sites.models import Site
from userapp.models import Playerinfo

class Network(models.Model):
    networkid = models.IntegerField(primary_key=True)
    networkname = models.CharField(max_length=50)
    def __str__(self):
        return str(self.networkname)

class NetworkAndSiteMap(models.Model):
    id = models.IntegerField(primary_key=True)
    networkid = models.ForeignKey(Network, db_column="networkid")
    siteid = models.ForeignKey(Site, db_column="siteid")

class PlayerAndSiteMapping(models.Model):
    id = models.IntegerField(primary_key=True,auto=True)
    playerid = models.ForeignKey(Playerinfo,db_column="playerid")
    siteid = models.ForeignKey(Site, db_column="siteid")
    networkid = models.ForeignKey(Network, db_column="networkid") 