from datetime import date, timedelta, datetime
from calendar import monthrange
from django.db.models import Min
from json2 import json
from django_user_agents.utils import get_user_agent


import logging, traceback, sys

error_logger = logging.getLogger('error_log')

def error_log():
    exc_type, exc_value, exc_traceback = sys.exc_info()
    traces = traceback.extract_tb(exc_traceback)
    stack_trace = [str(exc_value)]
    for trace in traces:
        stack_trace.append(' '.join(str(i) for i in trace))
    error_logger.error('\n\t'.join(stack_trace))
    return True

from django.core.cache import cache

def get_network_site_map_object(siteid):
    "this function is responsible for return networkandsitemap object from cache if its available "
    from userapp.models import Network, NetworkAndSiteMap
    from django.conf import settings
    cache_key = 'network_%s_site_%s' % (settings.NETWORK_ID, settings.SITE_ID)
    net_site_map = cache.get(cache_key)
    if not net_site_map:
        net_site_map = NetworkAndSiteMap.objects.get(networkid=settings.NETWORK_ID, siteid=settings.SITE_ID)
        cache.set(cache_key, net_site_map, 3600)
    return net_site_map

def get_network_sites_list():
    from userapp.models import Network, NetworkAndSiteMap
    ntw_sites_list = []
    networks = Network.objects.all()
    for i in networks:
        temp = dict()
        temp['name'] = i.networkname
        network_site_map_list = NetworkAndSiteMap.objects.filter(networkid=i).select_related('siteid')
        temp['sites'] = []
        for j in network_site_map_list:
            temp['sites'].append(j.siteid.domain)
        ntw_sites_list.append(temp)
    networks_list = [i.networkname for i in networks]
    return (networks_list, ntw_sites_list)

def get_sms_message_sender_id(siteid):
    sender_id = "TAJRMY"
    if siteid == 1:
        sender_id = "TAJRMY"
    elif siteid == 2:
        sender_id = "INDRMY"
    return sender_id

def get_python_date(input_date, date_format="mm/dd/yy"):
    """ to convert format of data to python format"""
    if input_date:
        if date_format == "dd/mm/yy":
            fwday, fwmonth,  fwyear = input_date.split('/')
            fwday, fwmonth, fwyear = int(fwday), int(fwmonth), int(fwyear)
        else:
            fwmonth, fwday, fwyear = input_date.split('/')
            fwmonth, fwday, fwyear = int(fwmonth), int(fwday), int(fwyear) 
        from datetime import date
        input_date = date(fwyear, fwmonth, fwday)
        return input_date
    else:
        date = '9999-12-31'
        return date

def get_first_day(dt, d_years=0, d_months=0):
    # d_years, d_months are "deltas" to apply to dt
    y, m = dt.year + d_years, dt.month + d_months
    a, m = divmod(m-1, 12)
    return date(y+a, m+1, 1)

def get_last_day(dt):
    return get_first_day(dt, 0, 1) + timedelta(-1)


def get_fromdate_todate(input_type, input_from_date, input_to_date, date_format="mm/dd/yy"):
    from datetime import time
    if input_type:
        custom = input_type
    else:
        custom = "Today"
    fromdate, todate = None, None
    if custom == 'None' or custom == 'Today':
        fromdate = datetime.now().date()
        todate = datetime.now().date() + timedelta(days=1)
    if custom == "Yesterday":
        yesterday = datetime.now().date() - timedelta(days=1)
        fromdate = yesterday
        todate = datetime.now().date() + timedelta(days=1)
    if custom == "WeekTildate":
        weekday = datetime.now().weekday()
        fromdate = datetime.now().date() - timedelta(days=weekday)
        todate = datetime.now().date()
    if custom == "Last 15 Days":
        weekday = 15
        fromdate = datetime.now().date() - timedelta(days=weekday)
        todate = datetime.now().date()
    if custom == "MonthTilldate":
        d = date.today()
        fromdate = get_first_day(d)
        todate = get_last_day(d)
        
    if custom == 'Custom':
        Fromdate = input_from_date
        Todate = input_to_date
        fromdate = get_python_date(Fromdate, date_format)
        todate = get_python_date(Todate, date_format)
        if fromdate == todate:
            todate = fromdate + timedelta(days=1)
        elif todate ==  datetime.now().date():
            todate = todate + timedelta(days=1)
        #fromdate = fromdate - timedelta(days=1)
    if not fromdate and not todate:
        fromdate = datetime.now().date()
        todate = datetime.now().date() + timedelta(days=1)
    resp = {'fromdate':fromdate, 'todate':todate}
    #resp = {'fromdate':date(2014, 10, 16), 'todate':date(2014, 10, 21)}
    return resp

def get_fromdate_todate_for_raw_query(input_type, input_from_date, input_to_date, date_format="mm/dd/yy"):
    from datetime import time
    if input_type:
        custom = input_type
    else:
        custom = "Today"
    fromdate, todate = None, None
    to_day_date = datetime.now().date()
    if custom == 'None' or custom == 'Today':
        fromdate = to_day_date
        todate = to_day_date
    if custom == "Yesterday":
        yesterday = to_day_date - timedelta(days=1)
        fromdate = yesterday
        todate = yesterday
    if custom == "WeekTildate":
        weekday = datetime.now().weekday()
        fromdate = to_day_date - timedelta(days=weekday)
        todate = to_day_date
    if custom == "Last 15 Days":
        weekday = 15
        fromdate = to_day_date - timedelta(days=weekday)
        todate = to_day_date
    
    if custom == "Last 30 Days":
        weekday = 30
        fromdate = to_day_date - timedelta(days=weekday)
        fromdate = datetime.combine(fromdate, time(0,0,0))
        todate = datetime.combine(to_day_date, time(23,59,59))
    
    if custom == "Last 45 Days":
        weekday = 45
        fromdate = to_day_date - timedelta(days=weekday)
        fromdate = datetime.combine(fromdate, time(0,0,0))
        todate = datetime.combine(to_day_date, time(23,59,59))

    if custom == "MonthTilldate":
        d = date.today()
        fromdate = get_first_day(d)
        todate = get_last_day(d)
        
    if custom == 'Custom':
        Fromdate = input_from_date
        Todate = input_to_date
        fromdate = get_python_date(Fromdate, date_format)
        todate = get_python_date(Todate, date_format)
    if not fromdate and not todate:
        fromdate = to_day_date
        todate = to_day_date
    resp = {'fromdate':fromdate, 'todate':todate}
    return resp

def get_fromdate_todate_for_django_query_filter(input_type, input_from_date, input_to_date, date_format="mm/dd/yy"):
    from datetime import time
    if input_type:
        custom = input_type
    else:
        custom = "Today"
    from_date_time, to_date_time = None, None
    today_date = datetime.now().date()
    
    if custom == 'None' or custom == 'Today':
        from_date_time = datetime.combine(today_date, time(0,0,0))
        to_date_time = datetime.combine(today_date, time(23,59,59))
    if custom == "Yesterday":
        yesterday = today_date - timedelta(days=1)
        from_date_time = datetime.combine(yesterday, time(0,0,0))
        to_date_time = datetime.combine(yesterday, time(23,59,59))
    if custom == "WeekTildate":
        weekday = datetime.now().weekday()
        fromdate = today_date - timedelta(days=weekday)
        from_date_time = datetime.combine(fromdate, time(0,0,0))
        to_date_time = datetime.combine(today_date, time(23,59,59))
    if custom == "Last 15 Days":
        weekday = 15
        fromdate = today_date - timedelta(days=weekday)
        from_date_time = datetime.combine(fromdate, time(0,0,0))
        to_date_time = datetime.combine(today_date, time(23,59,59))
    
    if custom == "Last 30 Days":
        weekday = 30
        fromdate = today_date - timedelta(days=weekday)
        from_date_time = datetime.combine(fromdate, time(0,0,0))
        to_date_time = datetime.combine(today_date, time(23,59,59))
    
    if custom == "Last 45 Days":
        weekday = 45
        fromdate = today_date - timedelta(days=weekday)
        from_date_time = datetime.combine(fromdate, time(0,0,0))
        to_date_time = datetime.combine(today_date, time(23,59,59))
        
    if (not to_date_time and not from_date_time) or custom == "MonthTilldate":
        d = date.today()
        fromdate = get_first_day(d)
        todate = get_last_day(d)
        from_date_time = datetime.combine(fromdate, time(0,0,0))
        to_date_time = datetime.combine(todate, time(23,59,59))
    
    if custom == "TillYear":
        d = date.today()
        from_date_time = datetime.combine(date(d.year, 1,1), time(0,0,0))
        to_date_time = datetime.combine(d, time(23,59,59))

    if custom == 'Custom':
        Fromdate = input_from_date
        Todate = input_to_date
        if not Fromdate or not Todate:
            return {'error_message': "Please select custom dates."}
        fromdate = get_python_date(Fromdate, date_format)
        todate = get_python_date(Todate, date_format)
        from_date_time = datetime.combine(fromdate, time(0,0,0))
        to_date_time = datetime.combine(todate, time(23,59,59))

    # if not to_date_time and not from_date_time:
    #     from_date_time = datetime.combine(today_date, time(0,0,0))
    #     to_date_time = datetime.combine(today_date, time(23,59,59))

    resp = {'fromdate':from_date_time, 'todate':to_date_time}
    return resp


def make_date_str_format(input_date, input_time, from_str=True):
    if from_str:
        time_str = input_time.replace(" ","")+":00"
    else:
        time_str = input_time.replace(" ","")+":59"
        
    date_string = "%s-%s-%s %s" % (input_date.year,input_date.month, input_date.day, time_str)
    return date_string


def custom_dates_validation(filtertype, from_date, to_date):
    """
    function is responsible for validating custom date selection in egcs
    """
    try:
        if filtertype.lower() == 'custom': 
            from_date = from_date.split("/")
            to_date = to_date.split("/")
            if from_date =="" or to_date == "":
                return False 
            if len(from_date) != 3 or len(to_date) != 3:
                return False
    except:
        return False
    return True

def get_site_player_first_register_timestamp():
    from userapp.models import Playerinfo
    from datetime import time
    min_register_date = Playerinfo.objects.all().aggregate(Min('registeredon'))
    if min_register_date.get('registeredon_min'):
        from_date = min_register_date.get('registeredon_min').date()
    else:
        from_date = date(2012,1,1)
    to_date_time =  datetime.combine(datetime.now().date() + timedelta(days=1), time(0,0,0))
    from_date_time = datetime.combine(from_date, time(0,0,0))
    return {'fromdate':from_date_time, 'todate':to_date_time}

def get_date_time_combined(dateto_combine, time_to_combine):
    return datetime.combine(dateto_combine, time_to_combine)


def get_paginate_dimensions(page, req_start_page, nexting_page, prevpage, total_count, number_of_items):
    from userapp.function import paginate_lis
    start_record, end_record, start_page, end_page, next_page, cur_page = 0,0,0,0,0,0
    
    if page or not req_start_page:
        start_record, end_record, start_page, end_page, next_page, cur_page = paginate_lis(page, total_count, is_next=False,
                                                                                            number_of_items=number_of_items)
    #if not req_start_page:
    #    start_record, end_record, start_page, end_page, next_page, cur_page = paginate_lis(page, total_count, is_next=False, 
    #                                                                                       number_of_items=number_of_items)
    if nexting_page:
        start_page = req_start_page
        start_record, end_record, start_page, end_page, next_page, cur_page = paginate_lis(int(start_page)+1, total_count, is_next=True, 
                                                                                           number_of_items=number_of_items)
    elif prevpage:
        start_page = req_start_page
        start_record, end_record, start_page, end_page, next_page, cur_page = paginate_lis(int(start_page), total_count, is_prev=True, 
                                                                                           number_of_items=number_of_items)
    
    page_count = range(start_page+1, end_page+1)
    return (start_record, end_record, start_page, end_page, next_page, cur_page, page_count)

from django.utils.encoding import smart_str
import hashlib

def get_hexdigest(algorithm, salt, raw_password):
    """
    Returns a string of the hexdigest of the given plaintext password and salt
    using the given algorithm ('md5', 'sha1' or 'crypt').
    """
    raw_password, salt = smart_str(raw_password), smart_str(salt)
    if algorithm == 'crypt':
        try:
            import crypt
        except ImportError:
            raise ValueError('"crypt" password algorithm not supported in this environment')
        return crypt.crypt(raw_password, salt)
    if algorithm == 'md5':
        return hashlib.md5(salt + raw_password).hexdigest()
    elif algorithm == 'sha1':
        return hashlib.sha1(salt + raw_password).hexdigest()
    raise ValueError("Got unknown password algorithm type in password.")


# def get_default_network_and_site():
#     from django.conf import settings
#     from userapp.models import Network, Site
#     try:
#         nework_name = Network.objects.get(networkid=settings.NETWORK_ID).networkname
#         #site_name = [Site.objects.get(id=settings.SITE_ID).domain]
#         site_name = [i.domain for i in Site.objects.all()]
#     except Exception as e:
#         print e
#         raise Exception("ERROR IN GETTING DEFAULT NETWORK AND SITE.")
#     return (nework_name, site_name)


class MyEncoder(json.JSONEncoder):
        """
        usage: print json.dumps(obj, cls = MyEncoder)
        """
        def default(self, obj):
            import json
            import datetime
            from decimal import Decimal
            from time import mktime
            if isinstance(obj, datetime.datetime):
                return obj.strftime("%d-%m-%Y %H:%M:%S")
            if isinstance(obj, Decimal):
                return str(obj)
            return json.JSONEncoder.default(self, obj)

def monthdelta(d1, d2):
    delta = 0
    while True:
        mdays = monthrange(d1.year, d1.month)[1]
        d1 += timedelta(days=mdays)
        if d1 <= d2:
            delta += 1
        else:
            break
    return delta

def get_chips_type(chip_type):
    from userapp.models import AMOUNT_TYPES_DICT
    if chip_type:
        chip_type = "%s_%s"%(chip_type.upper(), chip_type.upper())
    amount_code = "11"
    for k,v in AMOUNT_TYPES_DICT.items():
        if v == chip_type:
            amount_code = k
    return amount_code



def get_default_network_and_site_ids():
    from django.conf import settings
    from userapp.models import Network, Site
    try:
        nework_name = Network.objects.get(networkid=settings.NETWORK_ID).networkname
        #site_name = [Site.objects.get(id=settings.SITE_ID).domain]
        site_ids = [i.id for i in Site.objects.all()]
    except Exception as e:
        print e
        raise Exception("ERROR IN GETTING DEFAULT NETWORK AND SITE.")
    return (nework_name, site_ids)

def get_default_network_and_site():
    from django.conf import settings
    from userapp.models import Network, Site
    try:
        nework_name = Network.objects.get(networkid=settings.NETWORK_ID).networkname
        #site_name = [Site.objects.get(id=settings.SITE_ID).domain]
        site_name = [i.domain for i in Site.objects.all()]
    except Exception as e:
        print e
        raise Exception("ERROR IN GETTING DEFAULT NETWORK AND SITE.")
    return (nework_name, site_name)