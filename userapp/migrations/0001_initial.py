# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import userapp.models


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('countryname', models.CharField(unique=True, max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Device_info',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('device_type', models.CharField(max_length=30, null=True, blank=True)),
                ('client_type', models.CharField(max_length=30, null=True, blank=True)),
                ('operating_system', models.CharField(max_length=30, null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='MessageTemplate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('subject', models.CharField(max_length=b'100')),
                ('content', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='MessageTrigger',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tiggercondition', userapp.models.EnumField(max_length=104, choices=[(b'REGISTRATION', b'REGISTRATION'), (b'EMAIL_CHANGE', b'EMAIL_CHANGE'), (b'PASSWORD_RESET', b'PASSWORD_RESET'), (b'AFFILIATE_PASSWORD_RESET', b'AFFILIATE_PASSWORD_RESSET'), (b'REFER_SUB_AFFILIATE', b'REFER_SUB_AFFILIATE')])),
                ('status', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Network',
            fields=[
                ('networkid', models.IntegerField(serialize=False, primary_key=True)),
                ('networkname', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='NetworkAndSiteMap',
            fields=[
                ('id', models.IntegerField(serialize=False, primary_key=True)),
                ('networkid', models.ForeignKey(to='userapp.Network', db_column=b'networkid')),
                ('siteid', models.ForeignKey(to='sites.Site', db_column=b'siteid')),
            ],
        ),
        migrations.CreateModel(
            name='State',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('statename', models.CharField(max_length=50)),
                ('country', models.ForeignKey(to='userapp.Country')),
            ],
        ),
        migrations.AddField(
            model_name='messagetrigger',
            name='networkid',
            field=models.ForeignKey(to='userapp.Network', db_column=b'networkid'),
        ),
        migrations.AddField(
            model_name='messagetrigger',
            name='siteid',
            field=models.ManyToManyField(to='sites.Site'),
        ),
        migrations.AddField(
            model_name='messagetrigger',
            name='templates',
            field=models.ForeignKey(to='userapp.MessageTemplate'),
        ),
    ]
