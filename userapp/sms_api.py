from userapp.models import SMSTemplate, SMSTrigger, Playerinfo, PlayerAndSiteMapping, SMSSenderID
from django.contrib.sites.models import Site
from egcs.models import GenerateOTPlogs, MobileVerificationlogs
import string
from random import choice
import urllib
from userapp.utils import error_log
from django.template import Template, Context

class SendSMSWrapper(object):
    
    APIURL = "http://api.textlocal.in/send/?"
    APIKEY = "GZcWQqXIJNA-drBINx3Q0fNoBb0RP2YD1DSQnMmd7S"
    
    def __init__(self, player_obj, trigger=None, mobile_number=None, message=None):
        self.sms_type = SMSSenderID.TRANSACTION
        self.player_obj = player_obj
        self.mobile_number = mobile_number
        if self.mobile_number is None:
            self.mobile_number = self.player_obj.mobile
        self.trigger = trigger
        self.message = message
        self.sender_id = None
        self.token = None
        self.senderidobj = None
        self.set_sender_id()
        self.generate_token()
        self.first_name = player_obj.firstname.title()
        self.orderid = None
        self.order_amount = None

    def generate_token(self):
        self.token = ''.join([choice(string.digits) for i in range(6)])
    
    def set_orderid_and_amout(self, order_obj):
        self.orderid = str(order_obj.id)
        self.order_amount = str(int(order_obj.ordermoney))
    
    def is_valid_mobile_number(self):
        valid = False
        if self.mobile_number:
            try:
                int(self.mobile_number)
                if len(self.mobile_number) == 10:
                    valid = True
            except ValueError:
                pass
        return valid
    

    def sms_count_entry(self):
        from userapp.models import EventType, PlayerAndSiteMapping, SMSCount
        try:
            eventtype_obj = EventType.objects.get(eventname=self.trigger)
        except EventType.DoesNotExist:
            eventtype_obj = None
        try:
            ps = PlayerAndSiteMapping.objects.get(playerid=self.player_obj)
            sms_count = SMSCount(playerid=ps, eventtype=eventtype_obj, smssenderid=self.senderidobj)
            sms_count.save()
        except PlayerAndSiteMapping.DoesNotExist:
            raise Exception("Site not exist for player: %s"% self.player_obj.id)
        except Exception as e:
            print e
            error_log()

    def set_sender_id(self):
        ps = PlayerAndSiteMapping.objects.get(playerid=self.player_obj)
        senderid_obj = SMSSenderID.objects.filter(site=ps.siteid, accounttype=self.sms_type)
        if senderid_obj:
            self.senderidobj = senderid_obj[0]
            self.sender_id = senderid_obj[0].sendername
        else:
            #for Now default TR senderName
            self.senderidobj = None
            self.sender_id = "TAJRMY"
    
    def send_sms(self):
        """ 
        Send sms for given Mobile Number
        return boolen
        """
        sent_flag = True
        
        if not self.is_valid_mobile_number():
            return False
        try:
            data =  urllib.urlencode({"apiKey":SendSMSWrapper.APIKEY, 
                                    'numbers': "91%s"% (self.mobile_number),
                            'message' : self.message,'sender': self.sender_id,}) #"test":"true"})
    
            data = data.encode('utf-8')
            f = urllib.urlopen(SendSMSWrapper.APIURL, data)
            import json
            resp = json.loads(f.read())
            print resp
            if resp.get("errors"):
                sent_flag = False
            else:
                self.sms_count_entry()
        except Exception as e:
            error_log()
            sent_flag = False
        return sent_flag

    def sms_kyc(self, file_type):
        otp_sent = ""
        error_occ = 0
        try:
            sms_trigger_list = SMSTrigger.objects.filter(triggercondition=self.trigger,
                                                         siteid=self.player_obj.siteid,
                                                         networkid=self.player_obj.networkid,
                                                         status=True)
            for trig in sms_trigger_list:
                sms_template = SMSTemplate.objects.get(id=trig.templates_id)
                params = {"firstname":self.first_name,
                    'file_type': file_type, 'site_name': self.player_obj.siteid.name}
                template = Template(sms_template.content)
                context = Context(params)
                content = template.render(context)
                self.message = content
                self.send_sms()
        except Exception as e:
            print e



    def sms_triggers(self):
        otp_sent = ""
        error_occ = 0
        sms_trigger_list = SMSTrigger.objects.filter(triggercondition=self.trigger, 
                                                     siteid=self.player_obj.siteid, 
                                                     networkid=self.player_obj.networkid, 
                                                     status=True)
        for trig in sms_trigger_list:
            sms_template = SMSTemplate.objects.get(id=trig.templates_id)
            params = {"token":self.token,"firstname":self.first_name,
                      "amount":self.order_amount, "orderid":self.orderid}
            template = Template(sms_template.content)
            context = Context(params)
            content = template.render(context)
            self.message = content
            if self.send_sms():
                otp_sent = "OTP has been sent to your registered mobile number."
                error_occ = 0
            else:
                otp_sent = "There is some problem with mobile sms sending api."
                error_occ = 1
        return {'response': otp_sent,'error_occ': error_occ}
     
    def send_otp_sms(self):
        """
        This API responsible for sending OTP to player registered Mobile number.
        input: Playerobj
        output: dict
        """
        otp_sent = ""
        error_occ = 0
        #azeem
        try:
            otp = GenerateOTPlogs.objects.get(fk_playerinfo=self.player_obj)
            otp.token=self.token 
            otp.save()
        except GenerateOTPlogs.DoesNotExist:
            GenerateOTPlogs.objects.create(fk_playerinfo=self.player_obj, token=self.token)
        except GenerateOTPlogs.MultipleObjectsReturned:
            GenerateOTPlogs.objects.filter(fk_playerinfo=self.player_obj).delete()
            GenerateOTPlogs.objects.create(fk_playerinfo=self.player_obj, token=self.token)
        sms_api_resp_dict = self.sms_triggers()
        if self.player_obj.otp_to_mail:
            try:
                from userapp.mails import sendOTPmail
                sendOTPmail(self.player_obj, self.token)
            except Exception as e:
                print e
        return sms_api_resp_dict
    
    def send_verfication_sms(self):
        """
        This API responsible for sending Verification Code to player registered Mobile number.
        input: Playerobj
        output: dict
        for now we treat trigger type as REGISTRATION
        """
        response = ""
        mv_logs = MobileVerificationlogs.objects.filter(fk_playerinfo=self.player_obj).order_by('-id')
        if len(mv_logs) > 3:
            mv_logs.delete()
            MobileVerificationlogs.objects.create(fk_playerinfo=self.player_obj, token=self.token)
        elif len(mv_logs) == 3:
            mv_logs[0].token = self.token
            mv_logs[0].save()
        else:
            MobileVerificationlogs.objects.create(fk_playerinfo=self.player_obj, token=self.token)
        sms_api_resp_dict = self.sms_triggers()
        return sms_api_resp_dict

if __name__ == "__main__":
    pass
    #import django
    #django.setup()
    #pl = Playerinfo.objects.get(username="wildbunny")
    #ss = SendSMSWrapper(pl, trigger="REGISTRATION", mobile_number="919553362610", message=None)
    #ss.sms_triggers()