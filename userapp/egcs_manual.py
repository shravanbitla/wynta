from django.contrib.admin.views.decorators import staff_member_required
from userapp.models import *
from egcs.models import *
from egcs.views import block_state_gamecantplay
from userapp.mails import *
from userapp.function import *
from userapp.bonus import *
from userapp.forms import SupportForm
from userapp.utils import get_network_site_map_object, get_sms_message_sender_id, error_log

import csv
import sys
import urllib
import hashlib
import string
import base64
import itertools
import smtplib
import re
import json
import time
import random
import traceback
from django.shortcuts import render_to_response
from django.template import RequestContext, Context, Template
from django.http import HttpResponse, HttpRequest, \
                        HttpResponseRedirect, HttpResponseBadRequest, Http404
from random import choice
#from django.contrib.auth.models import get_hexdigest
from userapp.utils import get_hexdigest
from django.core.urlresolvers import reverse
from django.core.mail import EmailMessage
from django.utils import decorators
from django.template.loader import render_to_string
from django.contrib.sites.models import Site
from django.core.cache import cache
from django.conf import settings
from datetime import timedelta, date, datetime
from datetime import time as dttime
from django.contrib import sessions
from django.views.generic.base import TemplateView
from django.utils.decorators import method_decorator
from django.db import connection
from decimal import Decimal
from django.core.exceptions import ObjectDoesNotExist

"""
https://www.tajrummynetwork.com/add_players_manual/
https://www.tajrummynetwork.com/update_email_and_password/
https://www.tajrummynetwork.com/update_user_details/
https://www.tajrummynetwork.com/update_player_realchips/
"""

#forms.py
from django import forms
#
class UploadFileForm(forms.Form):
    file  = forms.FileField()

def enc_password(password):
    '''
    Encrypt password
    '''
    import random
    algo = 'sha1'
    salt = get_hexdigest(algo, str(random.random()), str(random.random()))[:5]
    hsh = get_hexdigest(algo, salt, password)
    password = '%s$%s$%s' % (algo, salt, hsh)
    return password


@check_ip_required
@staff_member_required
def update_user_details(request):
    form = UploadFileForm()
    error = []
    if request.method == "GET":
        print "asdada"
        return render_to_response('Tajrummy/update_user_details.html',
                              locals(),
                              context_instance=RequestContext(request))
    else:
        form = UploadFileForm(request.POST, request.FILES)
        reader = []
        if form.is_valid():
            reader = csv.reader(form.cleaned_data['file'])
        else:
            error = ["form not valid"]
            return render_to_response('Tajrummy/update_user_details.html',
                              locals(),
                              context_instance=RequestContext(request))
        updated_list = []
        try:
            for str_list in reader:
                try:
                    username = str_list[0].replace(" ","")
                    playerlevel = str_list[1].replace(" ","")
                    cash = str_list[2].replace(" ","")
                    comment = str_list[3]
                    funchips = str_list[4].replace(" ","")
                    direction = str_list[5].replace(" ","")
                    loyaltypoints = str_list[6].replace(" ", "")
                    amount_type = str_list[7].replace(" ","")
                    pl_obj = Playerinfo.objects.get(username=username)
                    pl_obj.playerlevel = playerlevel
                    ipaddress = get_client_ip(request)
                    
                    direction = direction.upper()
                    amount_type = amount_type.upper()
                    #FUNCHIPS
                    amounttype = "FUNCHIPS"
                    if direction == "WITHDRAWL":
                        reconcile_direction = "MANUAL_ADMIN_WITHDRAW"
                        if pl_obj.funchips > Decimal(funchips):
                            pl_obj.funchips = pl_obj.funchips - Decimal(funchips)
                        else:
                            pl_obj.funchips = 0
                        if pl_obj.loyaltypoints > Decimal(loyaltypoints):
                            pl_obj.loyaltypoints = pl_obj.loyaltypoints - Decimal(loyaltypoints)
                        else:
                            pl_obj.loyaltypoints = 0
                    else:
                        pl_obj.funchips = pl_obj.funchips + Decimal(funchips)
                        pl_obj.loyaltypoints = pl_obj.loyaltypoints + Decimal(loyaltypoints)
                        reconcile_direction = "MANUAL_ADMIN_ADD"
                    playerbalance = pl_obj.funchips
                    playerbalance_loy = pl_obj.loyaltypoints
                    inplay = pl_obj.funinplay
                    loy_inplay = pl_obj.loyaltyinplay
                    pl_obj.save()
                    engine_player_fun_update = "-"+str(Decimal(funchips)) if direction == "WITHDRAWL" else str(Decimal(funchips))
                    engine_player_loyal_update = "-"+str(Decimal(loyaltypoints)) if direction == "WITHDRAWL" else str(Decimal(loyaltypoints))
                    try:
                        params = urllib.urlencode({'command': 'balance_update', 'chipstype': 'funchips', 'amount':engine_player_fun_update ,
                                                   'userid':str(pl_obj.id)})
                        f = urllib.urlopen("http://127.0.0.1:4089/?%s" % params)
                    except Exception as e:
                        print e, "MANULA-FUN EGCS"
                        error.append(e)
                    
                    try:
                        params = urllib.urlencode({'command': 'balance_update', 'chipstype': 'loyaltychips', 'amount':engine_player_loyal_update ,
                                                   'userid':str(pl_obj.id)})
                        f = urllib.urlopen("http://127.0.0.1:4089/?%s" % params)
                    except Exception as e:
                        print e, "MANULA-FUN EGCS"
                        error.append(e)

                    t = Transaction_order.objects.create(playerid=pl_obj,
                                         transactiontype=reconcile_direction,
                                         amounttype=amounttype,
                                         amount=funchips)
                    reconcileentry(obj=pl_obj, status="ADD", amount=funchips, direction=reconcile_direction,
                                   amounttype=amounttype, chips=playerbalance, inplay=inplay, ipaddress = ipaddress)
                    amounttype = "LOYALTYPOINTS"
                    Transaction_order.objects.create(playerid=pl_obj,
                                         transactiontype=reconcile_direction,
                                         amounttype=amounttype,
                                         amount=loyaltypoints)
                    reconcileentry(obj=pl_obj, status="ADD", amount=loyaltypoints, direction=reconcile_direction,
                                   amounttype=amounttype, chips=playerbalance_loy, inplay=loy_inplay, ipaddress = ipaddress)
                    #CASH ADD
                    modeofdeposit = 'MANUAL_ADMIN'
                    comment = comment
                    message = "USER UPDATE MESSAGE"
                    reference ="USER UPDATE REFERENCE" 
                    timestamp = datetime.now()
                    amount = Decimal(cash)
                    order = Order.objects.create(playerid=pl_obj, ordertype='REAL_ADJUSTMENT', operator=request.user, comment=comment,
                                                 modeofdeposit=modeofdeposit, message=message, direction=direction,
                                                 approvedby=reference, timestamp=timestamp, ipaddress=ipaddress)
                    device_info_id = Device_info.objects.all()[0]
                    OrderDeviceDetails.objects.create(orderid=order, device_info_id=device_info_id)
                    if direction == "WITHDRAWL":
                        uptype = PlayerBalancesUpdatesStack.DEBIT
                        if amount_type == "D":
                            if pl_obj.depositamount >= amount:
                                pl_obj.realchips -= amount
                                pl_obj.depositamount -= amount
                            else:
                                error.append("Depsoit Amount is less For player :"% pl_obj.username)
                                continue
                                
                        else:
                            if pl_obj.withdrawableamount >= amount:
                                pl_obj.realchips -= amount
                                pl_obj.withdrawableamount -= amount
                            else:
                                error.append("Withdrawal Amount is less For Player :"% pl_obj.username)
                                continue 
                        pl_obj.save()
                        order.orderstate = "FINISHED"
                        order.ordermoney = amount
                        order.save()
                        player_balance = PlayerBalancesUpdatesStack(updated_from="W",
                                                       playerid=pl_obj,
                                                       update_amount=amount,
                                                       chips_type=PlayerBalancesUpdatesStack.CASH,
                                                       update_type=uptype,
                                                       chips_inplay=pl_obj.realinplay,
                                                       updated_balance=pl_obj.realchips,
                                                       depositamount=pl_obj.depositamount,
                                                       withdrawableamount=pl_obj.withdrawableamount
                                                                    )
                        player_balance.save()
                        #if pl_obj.flash_loggedin is True:
                        
                        eng_command = EngineCommand('balance_update')
                        if amount_type == 'D':
                            eng_command.balance_update(pl_obj.id, -order.ordermoney, EngineCommand.DEPOSIT_CHIPS_TYPE)
                        else:
                            eng_command.balance_update(pl_obj.id, -order.ordermoney, EngineCommand.WITHDRAWAL_CHIPS_TYPE)

                    if direction == "DEPOSIT":

                        uptype = PlayerBalancesUpdatesStack.CREDIT
                        
                        if amount_type == "D":
                            pl_obj.depositamount += amount
                            pl_obj.realchips += amount           
                        else:
                            pl_obj.withdrawableamount += amount
                            pl_obj.realchips += amount
            
                        pl_obj.save()
                        order.orderstate = "FINISHED"
                        order.ordermoney = amount
                        order.save()
                        player_balance = PlayerBalancesUpdatesStack(updated_from="W",
                                                       playerid=pl_obj,
                                                       update_amount=amount,
                                                        chips_type=PlayerBalancesUpdatesStack.CASH,
                                                        update_type=uptype,
                                                        chips_inplay=pl_obj.realinplay,
                                                       updated_balance=pl_obj.realchips,
                                                       depositamount=pl_obj.depositamount,
                                                       withdrawableamount=pl_obj.withdrawableamount)
                        player_balance.save()
                        #if pl_obj.flash_loggedin is True:
                        
                        eng_command = EngineCommand('balance_update')
                        if amount_type == 'D':
                            eng_command.balance_update(pl_obj.id, order.ordermoney, EngineCommand.DEPOSIT_CHIPS_TYPE)
                        else:
                            eng_command.balance_update(pl_obj.id, order.ordermoney, EngineCommand.WITHDRAWAL_CHIPS_TYPE)

                    pl_obj.save()
                    order.amount = pl_obj.realchips
                    order.save()
                    Transaction_order.objects.create(playerid=pl_obj,
                                                     transactiontype="REAL_ADJUSTMENT",
                                                     amounttype='CASH', amount=amount)
                    if direction == 'DEPOSIT':
                        amount = "+"+str(amount)
                    if direction == 'WITHDRAWL':
                        amount = "-"+str(amount)
                    reconcileentry(orderid=str(order.id), obj=pl_obj, status="FINISHED", amount=amount, direction="REAL_ADJUSTMENT",
                                   amounttype="CASH", chips=pl_obj.realchips, inplay=pl_obj.realinplay, ipaddress = ipaddress)
                    
                    updated_list.append(username)
                except Exception as e:
                    print e, "USER_UPDATE_LIST"
                    error.append(e)
        except Exception as e:
            print e, "USER_UPDATE_LIST", str_list
            error.append(e)
        player_objs =  Playerinfo.objects.filter(username__in=updated_list)
        return render_to_response('Tajrummy/update_user_details.html',
                              locals(),
                              context_instance=RequestContext(request))

@check_ip_required
@staff_member_required
def add_palyers_manual(request):
    form = UploadFileForm()
    if request.method == "GET":
        return render_to_response('Tajrummy/add_manual_player.html',
                              locals(),
                              context_instance=RequestContext(request))
    error = []
    if request.method == "POST":
        
        form = UploadFileForm(request.POST, request.FILES)
        reader = []
        if form.is_valid():
            reader = csv.reader(form.cleaned_data['file'])
        else:
            error = ["form not valid"]
            return render_to_response('Tajrummy/add_manual_player.html',
                              locals(),
                              context_instance=RequestContext(request))
        updated_list = []
        try:
            for str_list in reader:
                try:
                    username = str_list[0].replace(" ","")
                    password = str_list[1].replace(" ","")
                    email = str_list[2].replace(" ","")
                    Birthday = str_list[3].replace(" ","")
                    Birthday_list = Birthday.split("-")
                    if len(Birthday_list) != 3:
                        Birthday_list = Birthday.split("/")
                    Birthday = "/".join(Birthday_list)
                    Gender = str_list[4].replace(" ","")
                    statename = str_list[5].replace(" ","")
                    #Mobile = str_list[6].replace(" ","")
                    ipaddress = str_list[6].replace(" ","")
                    normal_account = str_list[7].replace(" ","")
                    from userapp.rummyauth import Signup
                    a = Signup()
                    response = a.post({'Username': username, 'Password':password ,
                                                   'Email':email,'Birthday':Birthday,'gender':Gender,
                                                   'Stateid':statename,'Mobile':'',
                                                   'TermsAndConditions':'ON','manual_ip_address_69':ipaddress,
                                                   'manual_add_player_69':20000,
                                                   'normal_account':normal_account})
                    
                    print response
                    if response.status_code == 200:
                        updated_list.append(username)
                    else:
                        try:
                            message = json.loads(response.content).get("message", None)
                            message = "%s - %s" %(message, username)
                        except:
                            message = "User Not added : %s" %username
                        error.append(message)
                except Exception as e:
                    print "USERADDERROR", e
                    error.append(e)
        except Exception as e:
            print e, "USER_ADD_LIST", str_list
            error.append(e)
        player_objs =  Playerinfo.objects.filter(username__in=updated_list)
    return render_to_response('Tajrummy/add_manual_player.html',
                              locals(),
                              context_instance=RequestContext(request))

@check_ip_required
@staff_member_required
def update_city_state(request):
    form = UploadFileForm()
    error = []
    if request.method == "GET":
        return render_to_response('Tajrummy/update_city_state.html',
                              locals(),
                              context_instance=RequestContext(request))
    else:
        form = UploadFileForm(request.POST, request.FILES)
        reader = []
        if form.is_valid():
            reader = csv.reader(form.cleaned_data['file'])
        else:
            error = ["form not valid"]
            return render_to_response('Tajrummy/update_city_state.html',
                              locals(),
                              context_instance=RequestContext(request))
        updated_list = []
        try:
            for str_list in reader:
                try:
                    username = str_list[0].replace(" ","")
                    city = str_list[1]
                    state = str_list[2]
                    pl_obj = Playerinfo.objects.get(username=username)
                    pl_obj.city = city
                    pl_obj.state = State.objects.get(statename=state) if state else None
                    pl_obj.save()
                    block_state_gamecantplay(pl_obj)
                    updated_list.append(username)
                except Exception as e:
                    error.append(e)
        except Exception as e:
            print e, "USER_ADD_LIST", str_list
            error.append(e)
        player_objs =  Playerinfo.objects.filter(username__in=updated_list)
    return render_to_response('Tajrummy/update_city_state.html',
                              locals(),
                              context_instance=RequestContext(request))

@check_ip_required
@staff_member_required
def update_email_and_password(request):
    form = UploadFileForm()
    error = []
    if request.method == "GET":
        return render_to_response('Tajrummy/update_email_and_password.html',
                              locals(),
                              context_instance=RequestContext(request))
    else:
        form = UploadFileForm(request.POST, request.FILES)
        reader = []
        if form.is_valid():
            reader = csv.reader(form.cleaned_data['file'])
        else:
            error = ["form not valid"]
            return render_to_response('Tajrummy/update_email_and_password.html',
                              locals(),
                              context_instance=RequestContext(request))
        updated_list = []
        try:
            for str_list in reader:
                try:
                    username = str_list[0].replace(" ","")
                    new_password = str_list[1].replace(" ","")
                    new_email = str_list[2].replace(" ","")
                    normal_account = str_list[3].replace(" ","")
                    password = enc_password(new_password)
                    email = new_email
                    pl_obj = Playerinfo.objects.get(username=username)
                    pl_obj.password = password
                    pl_obj.email = email
                    pl_obj.normal_account = int(normal_account)
                    pl_obj.save()
                    updated_list.append(username)
                except Exception as e:
                    error.append(e)
        except Exception as e:
            print e, "USER_ADD_LIST", str_list
            error.append(e)
        player_objs =  Playerinfo.objects.filter(username__in=updated_list)
    return render_to_response('Tajrummy/update_email_and_password.html',
                              locals(),
                              context_instance=RequestContext(request))

@check_ip_required
@staff_member_required
def update_player_realchips(request):
    form = UploadFileForm()
    error = []
    if request.method == "GET":
        return render_to_response('Tajrummy/update_player_realchips.html',
                              locals(),
                              context_instance=RequestContext(request))
    else:
        form = UploadFileForm(request.POST, request.FILES)
        reader = []
        if form.is_valid():
            reader = csv.reader(form.cleaned_data['file'])
        else:
            error = ["form not valid"]
            return render_to_response('Tajrummy/update_player_realchips.html',
                              locals(),
                              context_instance=RequestContext(request))
        updated_list = []
        try:
            for str_list in reader:
                try:
                    username = str_list[0].replace(" ","")
                    cash = str_list[1].replace(" ","")
                    comment = str_list[2].replace(" ","")
                    direction = str_list[3].replace(" ","")
                    normal_account = str_list[4].replace(" ","")
                    amount_type = str_list[5].replace(" ","")
                    pl_obj = Playerinfo.objects.get(username=username)
                    pl_obj.normal_account = int(normal_account)
                    ipaddress = get_client_ip(request)

                    modeofdeposit = 'MANUAL_ADMIN'
                    direction = direction.upper()
                    amount_type = amount_type.upper()
                    comment = comment
                    message = "USER UPDATE MESSAGE"
                    reference ="USER UPDATE REFERENCE"
                    timestamp = datetime.now()
                    amount = Decimal(cash)
                    order = Order.objects.create(playerid=pl_obj, ordertype='REAL_ADJUSTMENT', operator=request.user, comment=comment,
                                                 modeofdeposit=modeofdeposit, message=message, direction=direction,
                                                 approvedby=reference, timestamp=timestamp, ipaddress=ipaddress,
                                                 )
                    device_info_id = Device_info.objects.all()[0]
                    OrderDeviceDetails.objects.create(orderid=order, device_info_id=device_info_id)
                    if direction == "WITHDRAWL":
                        uptype = PlayerBalancesUpdatesStack.DEBIT
                        if amount_type == "D":
                            if pl_obj.depositamount >= amount:
                                pl_obj.realchips -= amount
                                pl_obj.depositamount -= amount
                            else:
                                error.append("Depsoit Amount is less For player :"% pl_obj.username)
                                continue
                        else:
                            if pl_obj.withdrawableamount >= amount:
                                pl_obj.realchips -= amount
                                pl_obj.withdrawableamount -= amount
                            else:
                                error.append("Withdrawal Amount is less For Player :"% pl_obj.username)
                                continue 
                        pl_obj.save()
                        order.orderstate = "FINISHED"
                        order.ordermoney = amount
                        order.save()
                        player_balance = PlayerBalancesUpdatesStack(updated_from="W",
                                                       playerid=pl_obj,
                                                       update_amount=amount,
                                                                    chips_type=PlayerBalancesUpdatesStack.CASH,
                                                                    update_type=uptype,
                                                                    chips_inplay=pl_obj.realinplay,
                                                       updated_balance=pl_obj.realchips,
                                                       depositamount=pl_obj.depositamount,
                                                       withdrawableamount=pl_obj.withdrawableamount)
                        player_balance.save()
                        eng_command = EngineCommand('balance_update')
                        if amount_type == 'D':
                            eng_command.balance_update(pl_obj.id, -order.ordermoney, EngineCommand.DEPOSIT_CHIPS_TYPE)
                        else:
                            eng_command.balance_update(pl_obj.id, -order.ordermoney, EngineCommand.WITHDRAWAL_CHIPS_TYPE)

                    if direction == "DEPOSIT":
                        uptype = PlayerBalancesUpdatesStack.CREDIT
                        if amount_type == "D":
                            pl_obj.depositamount += amount
                            pl_obj.realchips += amount           
                        else:
                            pl_obj.withdrawableamount += amount
                            pl_obj.realchips += amount
            
                        pl_obj.save()
                        order.orderstate = "FINISHED"
                        order.ordermoney = amount
                        order.save()
                        player_balance = PlayerBalancesUpdatesStack(updated_from="W",
                                                       playerid=pl_obj,
                                                       update_amount=amount,
                                                    chips_type=PlayerBalancesUpdatesStack.CASH,
                                                    update_type=uptype,
                                                    chips_inplay=pl_obj.realinplay,
                                                       updated_balance=pl_obj.realchips,
                                                       depositamount=pl_obj.depositamount,
                                                       withdrawableamount=pl_obj.withdrawableamount)
                        player_balance.save()
                        #if pl_obj.flash_loggedin is True:
                        
                        eng_command = EngineCommand('balance_update')
                        if amount_type == 'D':
                            eng_command.balance_update(pl_obj.id, order.ordermoney, EngineCommand.DEPOSIT_CHIPS_TYPE)
                        else:
                            eng_command.balance_update(pl_obj.id, order.ordermoney, EngineCommand.WITHDRAWAL_CHIPS_TYPE)

                    pl_obj.save()
                    order.amount = pl_obj.realchips
                    order.save()
                    Transaction_order.objects.create(playerid=pl_obj,
                                                     transactiontype="REAL_ADJUSTMENT",
                                                     amounttype='CASH', amount=amount)
                    if direction == 'DEPOSIT':
                        amount = "+"+str(amount)
                    if direction == 'WITHDRAWL':
                        amount = "-"+str(amount)
                    reconcileentry(orderid=str(order.id), obj=pl_obj, status="FINISHED", amount=amount, direction="REAL_ADJUSTMENT",
                                   amounttype="CASH", chips=pl_obj.realchips, inplay=pl_obj.realinplay, ipaddress = ipaddress)
                    updated_list.append(username)
                except Exception as e:
                    error.append(e)
        except Exception as e:
            print e, "USER_ADD_LIST", str_list
            error.append(e)
        player_objs =  Playerinfo.objects.filter(username__in=updated_list)
    return render_to_response('Tajrummy/update_player_realchips.html',
                              locals(),
                              context_instance=RequestContext(request))


@check_ip_required
@staff_member_required
def traffic_mgmt(request):
    form = UploadFileForm()
    error = []
    if request.method == "GET":
        return render_to_response('Tajrummy/traffic_mgmt.html',
                              locals(),
                              context_instance=RequestContext(request))
    else:
        form = UploadFileForm(request.POST, request.FILES)
        reader = []
        if form.is_valid():
            reader = csv.reader(form.cleaned_data['file'])
        else:
            error = ["form not valid"]
            return render_to_response('Tajrummy/traffic_mgmt.html',
                              locals(),
                              context_instance=RequestContext(request))
        updated_list = []
        gameids_list = []
        try:
            for str_list in reader:
                try:
                    gsid = str_list[0].replace(" ","")
                    gamename = str_list[1].replace(" ","")
                    bet = str_list[2].replace(" ","")
                    min_limit = str_list[3].replace(" ","")
                    max_limit = str_list[4].replace(" ","")
                    multiplication_factor = str_list[5].replace(" ","")
                    gsobj = Gamesetting.objects.get(id=gsid)
                    gsobj.random_min_limit = min_limit
                    gsobj.random_max_limit = max_limit
                    gsobj.multiplication_factor = multiplication_factor
                    gsobj.save()
                    gameids_list.append(gsid)
                    updated_list.append([gsid, gamename, min_limit, max_limit, multiplication_factor])
                except Exception as e:
                    error.append([e,"%s-%s-%s"%(gsid, gamename, bet)])
            
           
            try:
                gameids_list_str = ",".join(gameids_list)
                streamid = Stream.objects.filter(game__in=gameids_list)[0:1]
                if streamid:
                    streamid = str(streamid[0].id)
                else:
                    streamid = 1
                scheduleidobj = Schedule.objects.filter(stream__id=streamid)
                if not scheduleidobj:
                    scheduleidobj = Schedule.objects.all()[0:1]
                schedule_name = str(scheduleidobj[0].schedulename)
                params = urllib.urlencode({'command': 'bulk_settings_change',
                                                       'changein': 'game',
                                                       'streamid': streamid,
                                                       'scheduleid': schedule_name,
                                                       'type':'edit',
                                                       'gamesettingid': gameids_list_str})
                enginepath = urllib.urlopen("http://127.0.0.1:4089/?%s" % params)
            except Exception as e:
                print e,"FFFFFFFFFFFFFFFF"
                error.append([e, "%s-%s-%s"%(gsid,gamename,bet)])            
        except Exception as e:
            print e, "USER_ADD_LIST", str_list
            error.append([e, "%s-%s-%s"%(gsid,gamename,bet)])
    return render_to_response('Tajrummy/traffic_mgmt.html',
                              locals(),
                              context_instance=RequestContext(request))