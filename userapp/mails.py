from django.template import RequestContext
from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse
from random import choice
import string
from .models import *
from affiliate.models import *
# from django.contrib.sites.models import get_current_site
from django.template import Template, Context
from django.core.mail import EmailMessage
from django.contrib.sites.models import Site
import logging
logger = logging.getLogger(__name__)
import smtplib
import string
import random
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

def add_message_logs(aff_obj, trigger, from_email, to_email, status='U', account_manager=None, user_obj=None):
    from affiliate.models import Messagelogs
    msg_logs = Messagelogs.objects.create(affiliate=aff_obj,
                     from_email=from_email, to_email=to_email, status=status, affstatus=status, user=user_obj)
    if trigger:
        msg_logs.messagetrigger = trigger
    if account_manager:
        msg_logs.account_manager = account_manager
    msg_logs.save()
    return msg_logs


def maxi_emails(to_email, subject, content, cc=[]):
    email = 'manager@maxiaffiliates.com'
    passwd = 'marvingay3'
    server = smtplib.SMTP('smtp.gmail.com:587')
    server.ehlo()
    server.starttls()
    server.ehlo()
    server.login(email, passwd)
    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    msg['From'] = email
    msg['To'] = ", ".join(to_email)
    msg['Cc'] = ", ".join(cc)
    content = MIMEText(content, 'html')
    msg.attach(content)
    dd = server.sendmail(email, to_email, msg.as_string())

class SMSCount_DB(object):
    def __init__(self, player_obj, eventtype):
        from userapp.models import EventType,PlayerAndSiteMapping, SMSCount
        try:
            eventtype_obj = EventType.objects.get(eventname=eventtype)
        except EventType.DoesNotExist:
            eventtype_obj = None
        ps = PlayerAndSiteMapping.objects.get(playerid=player_obj)
        sms_count = SMSCount(playerid=ps, eventtype=eventtype_obj)
        sms_count.save()

class MailCount_DB(object):
    def __init__(self, player_obj, eventtype):
        from userapp.models import EventType, PlayerAndSiteMapping, MailCount
        try:
            eventtype_obj = EventType.objects.get(eventname=eventtype)
        except EventType.DoesNotExist:
            eventtype_obj = None
        ps = PlayerAndSiteMapping.objects.get(playerid=player_obj)
        mail_count = MailCount(playerid=ps, eventtype=eventtype_obj)
        mail_count.save()


def custom_affiliate_email(aff_obj,partuser_obj, am_obj, emails_list, from_email, content, subject):
    aff_prog = am_obj.affiliateprogramme.name
    d = content
    # if aff_prog == 'Maxi Affiliates':
    #     from_email = 'manager@maxiaffiliates.com'
    #     to_email = emails_list
    #     maxi_emails(to_email, subject, d, to_email)
    # else:
    #     to_email = emails_list
    #     msg = EmailMessage(subject, d, """%s""" % from_email, to_email,
    #         headers = {'Reply-To': from_email})
    #     msg.content_subtype = "html"
    #     #msg.send()
    # log_obj = add_message_logs(aff_obj, None, from_email, to_email[0], 
    #             'U', am_obj)
    # log_obj.content = d
    # log_obj.subject = subject
    # log_obj.save()
    headers = {'Reply-To': from_email}
    check_EmailClient(am_obj, None, subject,d, """%s""" % from_email, [from_email], emails_list, headers ,aff_obj, None,None, None)

def custom_admin_email(am_obj,partuser_obj, emails_list, from_email, content, subject):
    from affiliate.models import Affiliateinfo
    aff_prog = am_obj.affiliateprogramme.name
    d = content
    # if aff_prog == 'Maxi Affiliates':
    #     from_email = 'manager@maxiaffiliates.com'
    #     to_email = emails_list
    #     maxi_emails(to_email, subject, d, to_email)
    # else:
    #     to_email = emails_list
    #     msg = EmailMessage(subject, d, """%s""" % from_email, to_email,
    #         headers = {'Reply-To': from_email})
    #     msg.content_subtype = "html"
    #     #msg.send()
    # for aff_obj in Affiliateinfo.objects.filter(email__in=to_email):
    #     log_obj = add_message_logs(aff_obj, None, from_email, aff_obj.email, 
    #                 'U', am_obj)
    #     log_obj.content = d
    #     log_obj.subject = subject
    #     log_obj.save()
    headers = {'Reply-To': from_email}
    print('emails_list>>>>>>>>>>>>',emails_list)
    aff_list = Affiliateinfo.objects.filter(email__in=emails_list)
    check_EmailClient(am_obj, partuser_obj, subject,d, """%s""" % from_email, [from_email], emails_list, headers ,None, None,aff_list,None )

def registrationmail(obj, ap_obj):
    """ signup mail """
    from userapp.models import MessageTrigger, MessageTemplate, PartnerUser
    messagetrigger = MessageTrigger.objects.filter( 
                                triggercondition='REGISTRATION', status=True)
    if not messagetrigger:
        logger.critical("Please activate a Template in Wynta Admin to signup mail")
    if messagetrigger:
        trigger = messagetrigger[0]
        messagetemplate = MessageTemplate.objects.get(id=trigger.templates_id)
        subject = 'You have a new affiliate on '+ obj.account_manager.affiliateprogramme.name
        content = messagetemplate.content
        email = obj.email
        username = obj.username
        apm_name = obj.account_manager.user.email
        aff_prog = obj.account_manager.affiliateprogramme.name
        # emails_list = [i.user.email for i in PartnerUser.objects.filter(accmanager=obj.account_manager)]
        a = Template(content)
        b = Context(locals())
        d = a.render(b)
        check_EmailClient(obj.account_manager, None, subject, d, """%s""" % 'support@wynta.com', [], [obj.account_manager.user.email], None, obj,trigger,None, None)
        # if aff_prog == 'Maxi Affiliates':
        #     from_email = 'manager@maxiaffiliates.com'
        #     to_email = [obj.account_manager.user.email]
        #     maxi_emails([obj.account_manager.user.email], subject, d, emails_list)
        # else:
        #     from_email = obj.account_manager.user.email
        #     to_email = [obj.account_manager.user.email]
        #     msg = EmailMessage(subject, d, """%s""" % 'support@wynta.com', [obj.account_manager.user.email], emails_list)
        #     msg.content_subtype = "html"
        #     msg.send()
        # log_obj = add_message_logs(obj, trigger, from_email, ', '.join(to_email), 'U', obj.account_manager)
        # log_obj.content = d
        # log_obj.subject = subject
        # log_obj.save()
        # MailCount_DB(obj, "REGISTRATION")

def statuschangemail(obj, partuser_obj, ap_obj, status):
    """ signup mail """
    from userapp.models import MessageTrigger, MessageTemplate
    messagetrigger = MessageTrigger.objects.filter(
                                triggercondition=status, status=True)
    if not messagetrigger:
        logger.critical("Please activate a Template in Wynta Admin to Status mail")
    if messagetrigger:
        trigger = messagetrigger[0]
        messagetemplate = MessageTemplate.objects.get(id=trigger.templates_id)
        subject = obj.account_manager.affiliateprogramme.name + ': ' + str(messagetemplate.subject)
        content = messagetemplate.content
        username = obj.username
        aff_prog = obj.account_manager.affiliateprogramme.name
        a = Template(content)
        b = Context(locals())
        d = a.render(b)
        print('hree in status changes>>>>>>>>>>>>>>>>')
        try:
            # if aff_prog == 'Maxi Affiliates':
            #     from_email = 'manager@maxiaffiliates.com'
            #     to_email = [obj.email]
            #     maxi_emails([obj.email], subject, d)
            # else:
            #     from_email = obj.account_manager.user.email
            #     to_email = [obj.email]
            #     msg = EmailMessage(subject, d, """%s""" % obj.account_manager.user.email, [obj.email],
            #         headers = {'Reply-To': obj.account_manager.user.email})
            #     msg.content_subtype = "html"
            #     #msg.send()
            # log_obj = add_message_logs(obj, trigger, from_email, ', '.join(to_email), 'U', obj.account_manager)
            # log_obj.content = d
            # log_obj.subject = subject
            # log_obj.save()
            headers = {'Reply-To': obj.account_manager.user.email}
            check_EmailClient(obj.account_manager, partuser_obj, subject, d,  """%s""" % obj.account_manager.user.email, [obj.account_manager.user.email], [obj.email], headers,obj,trigger, None, None)
        except Exception as e:
            print "error in change maxi emails "+ str(e), aff_prog


def addusermail(obj, ap_obj, status, passwd):
    """ signup mail """
    from userapp.models import MessageTrigger, MessageTemplate
    messagetrigger = MessageTrigger.objects.filter(
                                triggercondition=status, status=True)
    if not messagetrigger:
        logger.critical("Please activate a Template in Wynta Admin to signup mail")
    if messagetrigger:
        trigger = messagetrigger[0]
        messagetemplate = MessageTemplate.objects.get(id=trigger.templates_id)
        subject = ap_obj.affiliateprogramme.name + ': ' + str(messagetemplate.subject)
        content = messagetemplate.content
        username = obj.username
        passwd = passwd
        aff_prog = ap_obj.affiliateprogramme.name
        a = Template(content)
        b = Context(locals())
        d = a.render(b)
        print('insde the add user email>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
        # if aff_prog == 'Maxi Affiliates':
        #     from_email = 'manager@maxiaffiliates.com'
        #     to_email = [obj.email]
        #     maxi_emails([obj.email], subject, d)
        # else:
        #     from_email = ap_obj.user.email
        #     to_email = [obj.email]
        #     msg = EmailMessage(subject, d, """%s""" % ap_obj.user.email, [obj.email])
        #     msg.content_subtype = "html"
        #     #msg.send()
        # log_obj = add_message_logs(obj, trigger, from_email, ', '.join(to_email), 'U', obj.account_manager)
        # log_obj.content = d
        # log_obj.subject = subject
        # log_obj.save()
        check_EmailClient(ap_obj, None, subject, d, """%s""" % ap_obj.user.email,[ap_obj.user.email], [obj.email], None,None,trigger, None,obj)



def affiliateprogramregister(ap_obj, status):
    """ signup mail """
    from userapp.models import MessageTrigger, MessageTemplate
    messagetrigger = MessageTrigger.objects.filter(
                                triggercondition=status, status=True)
    if not messagetrigger:
        logger.critical("Please activate a Template in Wynta Admin to signup mail")
    if messagetrigger:
        trigger = messagetrigger[0]
        messagetemplate = MessageTemplate.objects.get(id=trigger.templates_id)
        subject = "You're all set up on Wynta - " + ap_obj.affiliateprogramme.name
        content = messagetemplate.content
        aff_prog = ap_obj.affiliateprogramme.name
        email = ap_obj.user.email
        domain = str(ap_obj.affiliateprogramme.domain)
        a = Template(content)
        b = Context(locals())
        d = a.render(b)
        msg = EmailMessage(subject, d, """%s""" % 'support@wynta.com', [ap_obj.user.email])
        msg.content_subtype = "html"
        msg.send()

def affiliateinvite(ap_obj, obj, status):
    from userapp.models import MessageTrigger, MessageTemplate
    messagetrigger = MessageTrigger.objects.filter(
                                triggercondition=status, status=True)
    if not messagetrigger:
        logger.critical("Please activate a Template in Wynta Admin to signup mail")
    if messagetrigger:
        trigger = messagetrigger[0]
        messagetemplate = MessageTemplate.objects.get(id=trigger.templates_id)
        subject = ap_obj.affiliateprogramme.name + ': ' + str(messagetemplate.subject)
        content = messagetemplate.content
        username = obj.username
        if 'http' in ap_obj.affiliateprogramme.domain.lower():
            invite_url = ap_obj.affiliateprogramme.domain + '/affiliate/register/'
        else:
            invite_url = 'http://' + ap_obj.affiliateprogramme.domain + '/affiliate/register/'
        accept_invite = invite_url.replace('/affiliate/register/', '/admin/acceptaffiliateprog/')
        aff_prog = ap_obj.affiliateprogramme.name
        a = Template(content)
        b = Context(locals())
        d = a.render(b)
        if aff_prog == 'Maxi Affiliates':
            from_email = 'manager@maxiaffiliates.com'
            to_email = [obj.email]
            maxi_emails([obj.email], subject, d)
        else:
            from_email = ap_obj.user.email
            to_email = [obj.email]
            msg = EmailMessage(subject, d, """%s""" % ap_obj.user.email, [obj.email])
            msg.content_subtype = "html"
            msg.send()
        log_obj = add_message_logs(obj, trigger, from_email, ', '.join(to_email), 'U', obj.account_manager)
        log_obj.content = d
        log_obj.subject = subject
        log_obj.save()


def send_ap_jumpman_mail(ap_obj, status, network, brand, location, to_SP=False):
    from userapp.models import MessageTrigger, MessageTemplate
    messagetrigger = MessageTrigger.objects.filter(
                                triggercondition=status, status=True)
    if not messagetrigger:
        logger.critical("Please activate a Template in Wynta Admin to signup mail")
    if messagetrigger:
        trigger = messagetrigger[0]
        messagetemplate = MessageTemplate.objects.get(id=trigger.templates_id)
        subject = 'Action Required: Setting up '+ brand+ ' on ' + ap_obj.affiliateprogramme.name + 'at Wynta.'
        content = messagetemplate.content
        aff_prog = ap_obj.affiliateprogramme.name
        brand = brand
        network = network
        location = location.replace('/sftp', '')
        a = Template(content)
        b = Context(locals())
        d = a.render(b)
        if to_SP:
            msg = EmailMessage(subject, d, """%s""" % 'support@wynta.com', [
                'david@wearejumpman.com'
                ])
        else:
            msg = EmailMessage(subject, d, """%s""" % 'support@wynta.com', [ap_obj.user.email])
        msg.content_subtype = "html"
        msg.send()


def check_EmailClient(acc_manager, partuser_obj, subject, text_content,from_email, cc = None, to = None, headers=None, aff_obj=None,trigger=None,aff_list=None, user_obj=None):
    # pid = request.session.get('partnerid')
    # context_dict = {}
    # if not pid:
    #     return HttpResponseRedirect(reverse('partner_login'),)
    # from admins.views import get_u_details
    # acc_manager, partuser_obj, user = get_u_details(request)
    from userapp.models import Emailclient
    try:
        if partuser_obj:
            email_client_obj = Emailclient.objects.filter(partuser=partuser_obj[0])
        else:
            email_client_obj = Emailclient.objects.filter(accmanager=acc_manager)
        if email_client_obj:
            if email_client_obj[0].username and email_client_obj[0].password and email_client_obj[0].emailclient == 'gsuite':
                if aff_list:
                    for email in to:
                        server = smtplib.SMTP('smtp.gmail.com:587')
                        server.ehlo()
                        server.starttls()
                        server.ehlo()
                        server.login(email_client_obj[0].username, email_client_obj[0].password)
                        msg = MIMEMultipart('alternative')
                        msg['Subject'] = subject
                        msg['From'] = from_email  
                        msg['To'] =  email
                        if cc:
                            msg['Cc'] = ", ".join(cc)
                            print('message in cc', msg['Cc'])
                        content = MIMEText(text_content, 'html')
                        msg.attach(content)
                        dd = server.sendmail(from_email, [email], msg.as_string())
                else:
                    server = smtplib.SMTP('smtp.gmail.com:587')
                    server.ehlo()
                    server.starttls()
                    server.ehlo()
                    server.login(email_client_obj[0].username, email_client_obj[0].password)
                    msg = MIMEMultipart('alternative')
                    msg['Subject'] = subject
                    msg['From'] = from_email
                    msg['To'] = ", ".join(to)
                    if cc:
                        msg['Cc'] = ", ".join(cc)
                    content = MIMEText(text_content, 'html')
                    msg.attach(content)
                    dd = server.sendmail(from_email, to, msg.as_string())
            if email_client_obj[0].domain and email_client_obj[0].apikey and email_client_obj[0].emailclient == 'mailgun':
                send_email_mailgun(email_client_obj[0].domain, email_client_obj[0].apikey, from_email, to, subject, text_content, headers['Reply-To'] if headers else None)
            if aff_list and not aff_obj:
                for aff_obj in aff_list:
                    log_obj = add_message_logs(aff_obj, None, from_email, aff_obj.email, 
                                'U', acc_manager, user_obj)
                    log_obj.content = text_content
                    log_obj.subject = subject
                    log_obj.save()
            else:
                log_obj = add_message_logs(aff_obj, trigger, from_email, ', '.join(to), 'U', acc_manager, user_obj)
                log_obj.content = text_content
                log_obj.subject = subject
                log_obj.save()
        else:                
            msg = EmailMessage(subject, text_content, from_email, to, headers=headers)
            msg.content_subtype = "html"
            msg.send()
            log_obj = add_message_logs(aff_obj, trigger, from_email, ', '.join(to), 'U', acc_manager, user_obj)
            log_obj.content = text_content
            log_obj.subject = subject
            log_obj.save()
    except Exception as e:
        import traceback
        print('exception in check_EmailClient', e)
        traceback.print_exc()


def send_email_mailgun(domain, apikey, from_email, to, subject, text, headers):
    import requests
    import pdb
    pdb.set_trace()
    email_inputs = {"from": from_email,
                  "to": to,
                  "subject": subject,
                  "html": text,
                  "h:Reply-To": from_email,
                  }
    try:
        resp = requests.post(
            domain,
            auth=("api", apikey),
            data=email_inputs, verify=False)
    except Exception as e:
        print("ERROR in EMAIL SEND", self.email_inputs)
