import urllib
import logging

error_logger = logging.getLogger('error_log')

class EngineCommand(object):
    DEPOSIT_CHIPS_TYPE = "depositamount"
    WITHDRAWAL_CHIPS_TYPE = "withdrawableamount"
    FUNCHIPS_TYPE = "funchips"
    LOYALTYPOINTS_TYPE = "loyaltychips"
    
    def __init__(self, command_type):
        self.engine_api_url = "http://127.0.0.1:4089/"
        self.command_type = command_type
        self.api_parameters = dict(command_type=command_type)
    
    def get_error_msg_message(self):
        msg = ""
        for k in self.api_parameters:
            msg = "%s=%s|"%(k, self.api_parameters[k])
        return msg
    
    def convert_query_parmas_to_string(self):
        for k, v in self.api_parameters.iteritems():
            self.api_parameters[k] = str(v)

    def call_api(self):
        try:
            self.convert_query_parmas_to_string()
            query_params = urllib.urlencode(self.api_parameters)
            f = urllib.urlopen("%s?%s" % (self.engine_api_url, query_params))
            f.read()
        except Exception as e:
            error_logger.error(self.get_error_msg_message())
    
    def balance_update(self, userid, amount, chips_type, first_deposit=False):
        """
         {'command': 'balance_update', 'chipstype': 'realchips', 'amount': '-100', 'userid':1111}
        """
        command = "balance_update"
        self.api_parameters = dict(command=command, chipstype=chips_type,
                           amount=amount, userid=userid)
        if first_deposit:
            self.api_parameters['depositable'] = 1
        self.call_api()
    
    def chat_status(self, userid, status):
        command = "chat_status"
        self.api_parameters = dict(command=command, userid=userid, status=status)
        self.call_api()
        
    def block_player(self, userid):
        command = "block"
        self.api_parameters = dict(command=command, userid=userid)
        self.call_api()
    
    def disable_player(self, userid):
        command = "disable"
        self.api_parameters = dict(command=command, userid=userid)
        self.call_api()