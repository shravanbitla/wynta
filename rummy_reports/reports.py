from userapp.models import *
from affiliate.models import *
from dateutil.rrule import WEEKLY, SU, rrule, DAILY
from datetime import datetime, timedelta
from userapp.utils import monthdelta
from django.db.models import Q, Max, Sum, Count
from userapp.userapp_db_entry import get_selected_channel_devices_info_objs


class AllStatistics(object):
    
    def __init__(self, from_date, to_date, networkid=None, siteids_list=None, channel=None ,
                 affiliateid=None, campaignid=None):
        self.networkid = networkid
        self.siteid_list = siteids_list
        self.from_date = from_date
        self.to_date = to_date
        self.channel = channel
        if self.channel:
            dif = get_selected_channel_devices_info_objs(channel)
            #dif = Device_info.objects.filter(device_type=channel)
            self.device_info_ids = [int(i.id) for i in dif]
        self.affiliateid = affiliateid
        self.campaignid = campaignid
        
    
    def get_unique_deposit_accounts(self):
        deposits = OrderDeviceDetails.objects.filter(orderid__timestamp__gte=self.from_date,
                                            orderid__timestamp__lte=self.to_date,
                                            orderid__orderstate="FINISHED",
                                            orderid__direction="DEPOSIT")
        if self.networkid and self.siteid_list:
            deposits = deposits.filter(orderid__playerid__playerandsitemapping__siteid__id__in=self.siteid_list,
                                            orderid__playerid__playerandsitemapping__networkid__networkid__in=self.networkid)
        if self.channel:
            deposits = deposits.filter(device_info_id__in=self.device_info_ids)

        if self.affiliateid:
            deposits = deposits.filter(orderid__playerid__affiliate=self.affiliateid)
        deposits = deposits.exclude(orderid__ordertype='REAL_ADJUSTMENT').values_list('orderid__playerid', flat=True).distinct().count()
        return deposits

  
    def get_total_deposits_sum(self, count=False):   
        
        deposits = OrderDeviceDetails.objects.filter(orderid__timestamp__gte=self.from_date,
                                            orderid__timestamp__lte=self.to_date,
                                            orderid__orderstate="FINISHED",
                                            orderid__direction="DEPOSIT")
        if self.networkid and self.siteid_list:
            deposits = deposits.filter(orderid__playerid__playerandsitemapping__siteid__id__in=self.siteid_list,
                                            orderid__playerid__playerandsitemapping__networkid__networkid__in=self.networkid)
        if self.channel:
            deposits = deposits.filter(device_info_id__in=self.device_info_ids)
        
        if self.affiliateid:
            deposits = deposits.filter(orderid__playerid__affiliate=self.affiliateid)

        if not count:
            deposits = deposits.exclude(orderid__ordertype='REAL_ADJUSTMENT').aggregate(Sum('orderid__ordermoney')).values()
            deposits = deposits[0] if deposits and deposits[0] else 0
        else:
            deposits = deposits.exclude(orderid__ordertype='REAL_ADJUSTMENT').count()
        return deposits
    
    def get_total_withdrawals_sum(self, count=False):
        
        withdrawals = OrderDeviceDetails.objects.filter(Q(orderid__direction="WITHDRAWL") & Q(orderid__orderstate="FINISHED") &
                                    Q(orderid__timestamp__gte=self.from_date) &
                                    Q(orderid__timestamp__lte=self.to_date))
                                    
                                    
        if self.networkid and self.siteid_list:
            withdrawals = withdrawals.filter(orderid__playerid__playerandsitemapping__networkid__networkid__in=self.networkid,
                                       orderid__playerid__playerandsitemapping__siteid__id__in=self.siteid_list)       
        if self.channel:
            withdrawals = withdrawals.filter(device_info_id__in=self.device_info_ids)
        
        if self.affiliateid:
            withdrawals = withdrawals.filter(orderid__playerid__affiliate=self.affiliateid)

        if not count:
            withdrawals = withdrawals.exclude(orderid__ordertype='REAL_ADJUSTMENT').aggregate(Sum('orderid__ordermoney')).values()
            withdrawals = withdrawals[0] if withdrawals and withdrawals[0] else 0
        else:
            withdrawals = withdrawals.exclude(orderid__ordertype='REAL_ADJUSTMENT').count()
        return withdrawals
    
    def get_declined_withdrawal_sum(self, count=False):
        declined_withdrawals = OrderDeviceDetails.objects.filter(Q(orderid__direction="WITHDRAWL") & 
                                                                 Q(orderid__orderstate="DECLINED") &
                                        Q(orderid__timestamp__gte=self.from_date) &
                                        Q(orderid__timestamp__lte=self.to_date)& Q(orderid__operator__isnull=False))

        
        if self.networkid and self.siteid_list:
            declined_withdrawals = declined_withdrawals.filter(orderid__playerid__playerandsitemapping__networkid__networkid__in=self.networkid,
                                       orderid__playerid__playerandsitemapping__siteid__id__in=self.siteid_list)
        
        if self.channel:
            declined_withdrawals = declined_withdrawals.filter(device_info_id__in=self.device_info_ids)
        if not count:
            declined_withdrawals = declined_withdrawals.exclude(orderid__ordertype='REAL_ADJUSTMENT').\
                                       aggregate(Sum('orderid__ordermoney')).values()
            declined_withdrawals = declined_withdrawals[0] if declined_withdrawals and declined_withdrawals[0] else 0
        else:
            declined_withdrawals = declined_withdrawals.exclude(orderid__ordertype='REAL_ADJUSTMENT').count()
        return declined_withdrawals
    
    def get_reversed_withdrawal_sum(self, count=False):
        reversed_withdrawals = OrderDeviceDetails.objects.filter(Q(orderid__direction="WITHDRAWL") & 
                                                                 Q(orderid__orderstate="REVERSED") &
                                        Q(orderid__timestamp__gte=self.from_date) &
                                        Q(orderid__timestamp__lte=self.to_date) & Q(orderid__operator__isnull=True) )

        if self.networkid and self.siteid_list:
            reversed_withdrawals = reversed_withdrawals.filter(orderid__playerid__playerandsitemapping__networkid__networkid__in=self.networkid,
                                       orderid__playerid__playerandsitemapping__siteid__id__in=self.siteid_list)
        
        if self.channel:
            reversed_withdrawals = reversed_withdrawals.filter(device_info_id__in=self.device_info_ids)
        if not count:
            reversed_withdrawals = reversed_withdrawals.exclude(orderid__ordertype='REAL_ADJUSTMENT').\
                                       aggregate(Sum('orderid__ordermoney')).values()
            reversed_withdrawals = reversed_withdrawals[0] if reversed_withdrawals and reversed_withdrawals[0] else 0
        else:
            reversed_withdrawals = reversed_withdrawals.exclude(orderid__ordertype='REAL_ADJUSTMENT').count()
        return reversed_withdrawals
    
    def get_pending_withdrawals_sum(self, count=False):
        pending_withdrawals = OrderDeviceDetails.objects.filter(Q(orderid__direction="WITHDRAWL") 
                                                    & Q(orderid__orderstate__in=["PROGRESS", "INPROCESS"]))
        
        if self.networkid and self.siteid_list:
            pending_withdrawals = pending_withdrawals.\
                                    filter(orderid__playerid__playerandsitemapping__networkid__networkid__in=self.networkid,
                                       orderid__playerid__playerandsitemapping__siteid__id__in=self.siteid_list)
        
        if self.channel:
            pending_withdrawals = pending_withdrawals.filter(device_info_id__in=self.device_info_ids)
        if not count:
            pending_withdrawals = pending_withdrawals.exclude(orderid__ordertype='REAL_ADJUSTMENT').\
                                       aggregate(Sum('orderid__ordermoney')).values()
            pending_withdrawals = pending_withdrawals[0] if pending_withdrawals and pending_withdrawals[0] else 0
        else:
            pending_withdrawals = pending_withdrawals.exclude(orderid__ordertype='REAL_ADJUSTMENT').count()
        return pending_withdrawals

    
    def get_total_email_sent(self):
        mail_count_query = MailCount.objects.filter(timestamp__gte=self.from_date,
                                timestamp__lte=self.to_date)
        if self.networkid and self.siteid_list:
            mail_count_query = mail_count_query.filter(playerid__siteid__id__in=self.siteid_list,
                                         playerid__networkid__networkid__in=self.networkid)
        return mail_count_query.count()
        
        
        
    def get_total_sms_sent(self):
        sms_count_query = SMSCount.objects.filter(timestamp__gte=self.from_date,
                                timestamp__lte=self.to_date)
        if self.networkid and self.siteid_list:
            sms_count_query = sms_count_query.filter(playerid__siteid__id__in=self.siteid_list,
                                         playerid__networkid__networkid__in=self.networkid)
        return sms_count_query.count()
    
 
    def get_total_cash_bonus_expired(self):
        bonus_cash_credit = "CASH"
        status_list = "EXPIRED"
        bonus_cash = Bonuschunks.objects.filter(Q(bonuslogid__timestamp__gte=self.from_date) &
                                           Q(bonuslogid__timestamp__lte=self.to_date),
                                           bonuscredit=bonus_cash_credit,status=status_list)
        
        if self.networkid and self.siteid_list:
            bonus_cash = bonus_cash.filter(bonuslogid__playerid__playerandsitemapping__siteid__id__in=self.siteid_list,
                                           bonuslogid__playerid__playerandsitemapping__networkid__networkid__in=self.networkid)
        bonus_cash = bonus_cash.aggregate(Sum('chunkamount')).values()
        bonus_cash = bonus_cash[0] if bonus_cash and  bonus_cash[0] else 0
        return bonus_cash

    def get_total_cash_bonus_forfeited(self):
        bonus_cash_credit = "CASH"
        status_list = "FORFEITED"
        
        bonus_cash = Bonuschunks.objects.filter(Q(bonuslogid__timestamp__gte=self.from_date) &
                                           Q(bonuslogid__timestamp__lte=self.to_date),
                                           bonuscredit=bonus_cash_credit, status=status_list)
        
        if self.networkid and self.siteid_list:
            bonus_cash = bonus_cash.filter(bonuslogid__playerid__playerandsitemapping__siteid__id__in=self.siteid_list,
                                           bonuslogid__playerid__playerandsitemapping__networkid__networkid__in=self.networkid)
        bonus_cash = bonus_cash.aggregate(Sum('chunkamount')).values()
        bonus_cash = bonus_cash[0] if bonus_cash and  bonus_cash[0] else 0
        return bonus_cash


    def get_total_cash_bonus_released(self):
        bonus_cash_credit = "CASH"
        status = "PROCESSED"
        
        bonus_cash = Bonuschunks.objects.filter(Q(bonuslogid__timestamp__gte=self.from_date) &
                                           Q(bonuslogid__timestamp__lte=self.to_date),
                                           bonuscredit=bonus_cash_credit, status=status)
        
        if self.networkid and self.siteid_list:
            bonus_cash = bonus_cash.filter(bonuslogid__playerid__playerandsitemapping__siteid__id__in=self.siteid_list,
                                           bonuslogid__playerid__playerandsitemapping__networkid__networkid__in=self.networkid)
        bonus_cash = bonus_cash.aggregate(Sum('chunkamount')).values()
        bonus_cash = bonus_cash[0] if bonus_cash and  bonus_cash[0] else 0
        return bonus_cash

    def get_social_bonus_sum(self):
        bonus_social_credit = ['BONUS','FUNCHIPS', 'LOYALTYPOINTS','SOCIALPOINTS']
        status_list = ["PROCESSED"]
        
        bonus_social = Bonuschunks.objects.filter(Q(bonuslogid__timestamp__gte=self.from_date) &
                                           Q(bonuslogid__timestamp__lte=self.to_date),
                                           bonuscredit__in=bonus_social_credit, status__in=status_list)
        
        if self.networkid and self.siteid_list:
            bonus_social = bonus_social.filter(bonuslogid__playerid__playerandsitemapping__siteid__id__in=self.siteid_list,
                                           bonuslogid__playerid__playerandsitemapping__networkid__networkid__in=self.networkid)
        bonus_social = bonus_social.aggregate(Sum('chunkamount')).values()
        bonus_social = bonus_social[0] if bonus_social and  bonus_social[0] else 0
        return bonus_social
    
    def get_rake_sum(self):
        
        rake_amount = Playerrakelog.objects.filter(Q(timestamp__gte=self.from_date) &
                                      Q(timestamp__lte=self.to_date),
                                      playerid__deposits__gte=1,
                                      amouttype="11")
        if self.networkid and self.siteid_list:
            rake_amount = rake_amount.filter(playerid__playerandsitemapping__networkid__networkid__in=self.networkid,
                                             playerid__playerandsitemapping__siteid__id__in=self.siteid_list)
        if self.channel:
            rake_amount = rake_amount.filter(device_info_id__in=self.device_info_ids)
        
        if self.affiliateid:
            rake_amount = rake_amount.filter(playerid__affiliate=self.affiliateid)
        rake_amount = rake_amount.aggregate(Sum('rakeamount')).values()
        #print rake_amount.query
        rake_amount = rake_amount[0] if rake_amount and rake_amount[0] else 0
        return rake_amount

    
    def get_registrations_count(self):
        reg_list = Playerinfo1.objects.filter(playerid__registeredon__lte=self.to_date,
                                           playerid__registeredon__gte=self.from_date)
        
        if self.networkid and self.siteid_list:
            reg_list = reg_list.filter(playerid__playerandsitemapping__siteid__id__in=self.siteid_list,
                                           playerid__playerandsitemapping__networkid__networkid__in=self.networkid)
        if self.channel:
            reg_list = reg_list.filter(device_info_id__in=self.device_info_ids)
        
        if self.affiliateid:
            reg_list = reg_list.filter(playerid__affiliate=self.affiliateid)
        
        reg_list = reg_list.aggregate(Count('id')).values()
        reg_list = reg_list[0] if reg_list and reg_list[0] else 0
        return reg_list
    
    def get_total_login_count(self):
        logintoday = Loginon.objects.filter(Q(logintime__gte=self.from_date.date()) &
                                        Q(logintime__lte=self.to_date.date()) &
                                         Q(loginok=True))
        
        if self.networkid and self.siteid_list:
            logintoday = logintoday.filter(playerid__playerandsitemapping__siteid__id__in=self.siteid_list,
                                           playerid__playerandsitemapping__networkid__networkid__in=self.networkid)
        if self.channel:
            logintoday = logintoday.filter(device_info_id__in=self.device_info_ids)
        
        if self.affiliateid:
            logintoday = logintoday.filter(affiliateid=self.affiliateid)
        
        print logintoday.query
        logintoday = logintoday.count()
        return logintoday
    
    def get_free_games_played(self):
        
        free_games_played = Playerrakelog.objects.filter(timestamp__gte=self.from_date, timestamp__lte=self.to_date,
                                         amouttype="22", fkgame_id__isnull=False, tournament_id__isnull=True)
        
        if self.networkid and self.siteid_list:
            free_games_played = free_games_played.filter(playerid__playerandsitemapping__siteid__id__in=self.siteid_list,
                                           playerid__playerandsitemapping__networkid__networkid__in=self.networkid)
        
        if self.channel:
            free_games_played = free_games_played.filter(device_info_id__in=self.device_info_ids)
        #TODO Device
        free_games_played = free_games_played.values('fkgame_id').annotate(count=Count('fkgame_id', distinct=True))
        return len(free_games_played)
        
    
    def get_cash_games_played(self):

        cash_games_played = Playerrakelog.objects.filter(timestamp__gte=self.from_date, 
                                                              timestamp__lte=self.to_date,
                                         amouttype="11",fkgame_id__isnull=False, tournament_id__isnull=True)
        if self.networkid and self.siteid_list:
            cash_games_played = cash_games_played.filter(playerid__playerandsitemapping__siteid__id__in=self.siteid_list,
                                           playerid__playerandsitemapping__networkid__networkid__in=self.networkid)
        
        if self.channel:
            cash_games_played = cash_games_played.filter(device_info_id__in=self.device_info_ids)
        #TODO Device
        cash_games_played = cash_games_played.values('fkgame_id').annotate(count=Count('fkgame_id', distinct=True))
        return len(cash_games_played)
        
    def get_fresh_logins_count(self):
        unique_logins = Loginon.objects.filter(logintime__gte=self.from_date.date(),
                                                logintime__lte=self.to_date.date(),
                                                fresh_login=True)
        if self.networkid and self.siteid_list:
            unique_logins = unique_logins.filter(playerid__playerandsitemapping__siteid__id__in=self.siteid_list,
                                           playerid__playerandsitemapping__networkid__networkid__in=self.networkid)
    
        if self.channel:
            unique_logins = unique_logins.filter(device_info_id__in=self.device_info_ids)
        unique_logins = unique_logins.count()
        return unique_logins
    
    def get_uniquelogins_count(self):
        unique_logins = Loginon.objects.\
                                        filter(Q(logintime__gte=self.from_date.date())&
                                                Q(logintime__lte=self.to_date.date())&
                                                Q(uniquelogin=True)&Q( loginok=True))
        if self.networkid and self.siteid_list:
            unique_logins = unique_logins.filter(Q(playerid__playerandsitemapping__siteid__id__in=self.siteid_list)&
                                        Q(playerid__playerandsitemapping__networkid__networkid__in=self.networkid))
    
        if self.channel:
            unique_logins = unique_logins.filter(Q(device_info_id__in=self.device_info_ids))
        
        if self.affiliateid:
            unique_logins = unique_logins.filter(( Q(affiliateid=self.affiliateid) | Q(playerid__affiliate=self.affiliateid)))
        unique_logins = unique_logins.count()
        return unique_logins
    
    def get_locked_accounts_count(self):
        
        lockedcount = Locklogs.objects.filter( Q(expired=False) & Q(timestamp__gte=self.from_date)
                                         & Q(timestamp__lte=self.to_date))
        
        if self.networkid and self.siteid_list:
            lockedcount = lockedcount.filter(playerid__playerandsitemapping__siteid__id__in=self.siteid_list,
                                           playerid__playerandsitemapping__networkid__networkid__in=self.networkid)
        lockedcount = lockedcount.values('playerid').distinct().count()
        return lockedcount
    
    def get_tourneys_played(self):
        """
        cash_played = Reconcilelogs.objects.filter(transaction_type="TOURNAMENT_WIN",gametype="tournament",
                                                    timestamp__gte=self.from_date,
                                                    timestamp__lte=self.to_date)
        if self.networkid and self.siteid_list:
            cash_played = cash_played.filter(playerid__playerandsitemapping__siteid__id__in=self.siteid_list,
                                           playerid__playerandsitemapping__networkid__networkid__in=self.networkid)
        #TODO Device
        cash_played = cash_played.values('tableid').distinct().count()
        """
        t_played = Playerrakelog.objects.filter(Q(fkgame_id__isnull=True)|Q(fkgame_id=0),
                                                timestamp__gte=self.from_date, timestamp__lte=self.to_date,
                                         tournament_id__isnull=False,
                                         gametype="TOURNEY")
        
        if self.networkid and self.siteid_list:
            t_played = t_played.filter(playerid__playerandsitemapping__siteid__id__in=self.siteid_list,
                                           playerid__playerandsitemapping__networkid__networkid__in=self.networkid)
        if self.channel:
            t_played = t_played.filter(device_info_id__in=self.device_info_ids)
        #TODO Device
        t_played = t_played.values('tournament_id').annotate(count=Count('tournament_id', distinct=True))
        return len(t_played)

    
    def get_cash_tourneys_played(self):
        cash_played = Reconcilelogs.objects.filter(transaction_type="TOURNAMENT_WIN",gametype="tournament",
                                                    amounttype="CASH", timestamp__gte=self.from_date,
                                                    timestamp__lte=self.to_date)
        if self.networkid and self.siteid_list:
            cash_played = cash_played.filter(playerid__playerandsitemapping__siteid__id__in=self.siteid_list,
                                           playerid__playerandsitemapping__networkid__networkid__in=self.networkid)
        #if self.channel:
        #    cash_played = cash_played.filter(device_info_id__in=self.device_info_ids)
        
        #TODO Device
        cash_played = cash_played.values('tableid').distinct().count()
        return cash_played
    
    def get_loyalty_tourneys_played(self):
        cash_played = Reconcilelogs.objects.filter(transaction_type="TOURNAMENT_WIN",gametype="tournament",
                                                    amounttype="LOYALTYPOINTS", timestamp__gte=self.from_date,
                                                    timestamp__lte=self.to_date)
        if self.networkid and self.siteid_list:
            cash_played = cash_played.filter(playerid__playerandsitemapping__siteid__id__in=self.siteid_list,
                                           playerid__playerandsitemapping__networkid__networkid__in=self.networkid)
        
        #if self.channel:
        #    cash_played = cash_played.filter(device_info_id__in=self.device_info_ids)
        
        #TODO Device
        cash_played = cash_played.values('tableid').distinct().count()
        return cash_played
    
    def get_free_tourneys_played(self):
        cash_played = Reconcilelogs.objects.filter(transaction_type="TOURNAMENT_WIN",gametype="tournament",
                                                    amounttype="FUNCHIPS", timestamp__gte=self.from_date,
                                                    timestamp__lte=self.to_date)
        if self.networkid and self.siteid_list:
            cash_played = cash_played.filter(playerid__playerandsitemapping__siteid__id__in=self.siteid_list,
                                           playerid__playerandsitemapping__networkid__networkid__in=self.networkid)
        
        #if self.channel:
        #    cash_played = cash_played.filter(device_info_id__in=self.device_info_ids)
        
        #TODO Device
        cash_played = cash_played.values('tableid').distinct().count()
        return cash_played
    
    def get_loyalty_points_earned(self):
        loyal = LoyaltyStats.objects.filter(timestamp__gte=self.from_date,
                                    timestamp__lte=self.to_date,
                                    updatetype='E')
        if self.networkid and self.siteid_list:
            loyal = loyal.filter(playerid__playerandsitemapping__siteid__id__in=self.siteid_list,
                                           playerid__playerandsitemapping__networkid__networkid__in=self.networkid)
        if self.channel:
            loyal = loyal.filter(device_info_id__in=self.device_info_ids)
        
        loyal = loyal.aggregate(Sum('update_amount')).values()
        loyal = loyal[0] if loyal and loyal[0] else 0
        return loyal
    
    def get_loyalty_points_churned(self):
        loyal = LoyaltyStats.objects.filter(timestamp__gte=self.from_date,
                                    timestamp__lte=self.to_date,
                                    updatetype='CH')
        if self.networkid and self.siteid_list:
            loyal = loyal.filter(playerid__playerandsitemapping__siteid__id__in=self.siteid_list,
                                           playerid__playerandsitemapping__networkid__networkid__in=self.networkid)
        if self.channel:
            loyal = loyal.filter(device_info_id__in=self.device_info_ids)
        
        loyal = loyal.aggregate(Sum('update_amount')).values()
        loyal = loyal[0] if loyal and loyal[0] else 0
        return loyal
    
    def get_loyalty_points_converted(self):
        loyal = LoyaltyStats.objects.filter(timestamp__gte=self.from_date,
                                    timestamp__lte=self.to_date,
                                    updatetype='CN')
        if self.networkid and self.siteid_list:
            loyal = loyal.filter(playerid__playerandsitemapping__siteid__id__in=self.siteid_list,
                                           playerid__playerandsitemapping__networkid__networkid__in=self.networkid)
        if self.channel:
            loyal = loyal.filter(device_info_id__in=self.device_info_ids)
        
        loyal = loyal.aggregate(Sum('update_amount')).values()
        loyal = loyal[0] if loyal and loyal[0] else 0
        return loyal
    
    def get_smart_correction(self):
        al_log = AI_Logs.objects.filter(time_stamp__gte=self.from_date,
                                    time_stamp__lte=self.to_date,ai_type="SC")
        if self.networkid and self.siteid_list:
            al_log = al_log.filter(playerid__playerandsitemapping__siteid__id__in=self.siteid_list,
                                           playerid__playerandsitemapping__networkid__networkid__in=self.networkid)
        if self.channel:
            al_log = al_log.filter(device_info_id__in=self.device_info_ids)
        
        al_log = al_log.aggregate(Count('id')).values()
        al_log = al_log[0] if al_log and al_log[0] else 0
        return al_log

    
    def get_ai_used(self):
        al_log = AI_Logs.objects.filter(time_stamp__gte=self.from_date,
                                    time_stamp__lte=self.to_date,ai_type="AI")
        if self.networkid and self.siteid_list:
            al_log = al_log.filter(playerid__playerandsitemapping__siteid__id__in=self.siteid_list,
                                           playerid__playerandsitemapping__networkid__networkid__in=self.networkid)
        if self.channel:
            al_log = al_log.filter(device_info_id__in=self.device_info_ids)
        
        al_log = al_log.aggregate(Count('id')).values()
        al_log = al_log[0] if al_log and al_log[0] else 0
        return al_log
    
    def get_extend_auto_play_user(self):
        al_log = AI_Logs.objects.filter(time_stamp__gte=self.from_date,
                                    time_stamp__lte=self.to_date,ai_type="AP")
        if self.networkid and self.siteid_list:
            al_log = al_log.filter(playerid__playerandsitemapping__siteid__id__in=self.siteid_list,
                                           playerid__playerandsitemapping__networkid__networkid__in=self.networkid)
        if self.channel:
            al_log = al_log.filter(device_info_id__in=self.device_info_ids)
        
        al_log = al_log.aggregate(Count('id')).values()
        al_log = al_log[0] if al_log and al_log[0] else 0
        return al_log

    def get_bonus_codes_used(self):
        deposits = OrderDeviceDetails.objects.filter(Q(orderid__bonuscode__isnull=False) & 
                                                     ~Q(orderid__bonuscode__in=['','None']),
                                                     orderid__timestamp__gte=self.from_date,
                                            orderid__timestamp__lte=self.to_date,
                                            orderid__orderstate="FINISHED",
                                            orderid__direction="DEPOSIT",
                                            )
        if self.networkid and self.siteid_list:
            deposits = deposits.filter(orderid__playerid__playerandsitemapping__siteid__id__in=self.siteid_list,
                                            orderid__playerid__playerandsitemapping__networkid__networkid__in=self.networkid)
        
        if self.affiliateid:
            deposits = deposits.filter(playerid__affiliate= self.affiliateid)
        if self.campaignid:
            deposits = deposits.filter(playerid__campaign=self.campaignid)
        if self.channel:
            deposits = deposits.filter(device_info_id__in=self.device_info_ids)
        deposits = deposits.exclude(orderid__ordertype='REAL_ADJUSTMENT').count()
        return deposits
    
    def get_deposits_tried(self):
        deposits = OrderDeviceDetails.objects.filter(orderid__timestamp__gte=self.from_date,
                                            orderid__timestamp__lte=self.to_date,
                                            orderid__orderstate="CREATED",
                                            orderid__direction="DEPOSIT")
        if self.networkid and self.siteid_list:
            deposits = deposits.filter(orderid__playerid__playerandsitemapping__siteid__id__in=self.siteid_list,
                                            orderid__playerid__playerandsitemapping__networkid__networkid__in=self.networkid)
        if self.channel:
            deposits = deposits.filter(device_info_id__in=self.device_info_ids)
        deposits = deposits.exclude(orderid__ordertype='REAL_ADJUSTMENT').count()
        return deposits
    
    def get_transactions_declined(self):
        deposits = OrderDeviceDetails.objects.filter(orderid__timestamp__gte=self.from_date,
                                            orderid__timestamp__lte=self.to_date,
                                            orderid__orderstate="FAILED")
        if self.networkid and self.siteid_list:
            deposits = deposits.filter(orderid__playerid__playerandsitemapping__siteid__id__in=self.siteid_list,
                                            orderid__playerid__playerandsitemapping__networkid__networkid__in=self.networkid)
        if self.channel:
            deposits = deposits.filter(device_info_id__in=self.device_info_ids)
        deposits = deposits.exclude(orderid__ordertype='REAL_ADJUSTMENT').count()
        
        withdrawals = OrderDeviceDetails.objects.filter(orderid__timestamp__gte=self.from_date,
                                            orderid__timestamp__lte=self.to_date,
                                            orderid__orderstate="DECLINED", orderid__operator__isnull=False)
        if self.networkid and self.siteid_list:
            withdrawals = withdrawals.filter(orderid__playerid__playerandsitemapping__siteid__id__in=self.siteid_list,
                                            orderid__playerid__playerandsitemapping__networkid__networkid__in=self.networkid)
        if self.channel:
            withdrawals = withdrawals.filter(device_info_id__in=self.device_info_ids)
        withdrawals = withdrawals.exclude(orderid__ordertype='REAL_ADJUSTMENT').count()
        
        return deposits+withdrawals
    
    def get_withdraw_tried(self):
        return 0
    
    def get_affiliate_acquisitions_count(self):
        count = 0
        if self.affiliateid:
            acqs = OrderDeviceDetails.objects.filter(Q(orderid__direction='DEPOSIT') & Q(orderid__firstdeposit=True) & 
                                        Q(orderid__orderstate='FINISHED')& Q(orderid__playerid__affiliate=self.affiliateid) &
                                        Q(orderid__timestamp__gte=self.from_date) &
                                           Q(orderid__timestamp__lte=self.to_date))
            if self.channel:
                acqs = acqs.filter(device_info_id__in=self.device_info_ids)
            count = acqs.exclude(orderid__ordertype='REAL_ADJUSTMENT').count()
        return count
    
    def get_affiliate_hits_count(self):
        count = 0
        if self.affiliateid:
            hits = Hitcount.objects.filter(Q(fk_affiliateinfo=self.affiliateid)&
                                     Q(timestamp__gte=self.from_date) &
                                           Q(timestamp__lte=self.to_date))
            if self.channel:
                hits = hits.filter(device_info_id__in=self.device_info_ids)
            hits = hits.aggregate(sum=Sum('hits'))
            if hits and hits.get('sum'):
                count = hits.get('sum')
        return count


def reports_by_device(from_date, to_date, networkids=None, siteids=None, affiliateid=None, campaignid=None):
    if networkids and siteids:
        reg_list = Playerinfo1.objects.values('device_info_id').annotate(count=Count('id')).\
                                    filter(playerid__playerandsitemapping__siteid__id__in=siteids,
                                           playerid__playerandsitemapping__networkid__networkid__in=networkids,
                                           playerid__registeredon__lte=to_date,
                                           playerid__registeredon__gte=from_date)
    
    else:
        reg_list = Playerinfo1.objects.values('device_info_id').annotate(count=Count('id')).\
                                    filter(playerid__registeredon__lte=to_date,
                                           playerid__registeredon__gte=from_date)
    if affiliateid:
        reg_list = reg_list.filter(playerid__affiliate=affiliateid)
    if campaignid:
        reg_list = reg_list.filter(playerid__campaign=campaignid)
    
    if networkids and siteids:
        fresh_logins = Loginon.objects.values('device_info_id').annotate(count=Count('id')).\
                                            filter(logintime__gte=from_date,
                                                logintime__lte=to_date,
                                                playerid__playerandsitemapping__siteid__id__in=siteids,
                          playerid__playerandsitemapping__networkid__networkid__in=networkids,
                          loginok=True, fresh_login=True)
    else:
        fresh_logins = Loginon.objects.values('device_info_id').annotate(count=Count('id')).\
                                        filter(logintime__gte=from_date,
                                            logintime__lte=to_date,
                                            loginok=True, fresh_login=True)
    if affiliateid:
        fresh_logins = fresh_logins.filter(affiliateid=affiliateid)
    if campaignid:
        fresh_logins = fresh_logins.filter(campaignid=campaignid)

    if networkids and siteids:
        unique_logins = Loginon.objects.values('device_info_id').annotate(count=Count('id')).\
                                            filter(logintime__gte=from_date,
                                                logintime__lte=to_date,
                                                loginok=True, uniquelogin=True,
                                                playerid__playerandsitemapping__siteid__id__in=siteids,
                                                playerid__playerandsitemapping__networkid__networkid__in=networkids)
    else:
        unique_logins = Loginon.objects.values('device_info_id').annotate(count=Count('id')).\
                                        filter(logintime__gte=from_date,
                                            logintime__lte=to_date,
                                            loginok=True, uniquelogin=True)
    if affiliateid:
        unique_logins = unique_logins.filter(affiliateid=affiliateid)
    if campaignid:
        unique_logins = unique_logins.filter(campaignid=campaignid)
    
    if networkids and siteids:
        orders = OrderDeviceDetails.objects.values('device_info_id').annotate(count=Count('id')).\
                                        filter(orderid__timestamp__gte=from_date,
                                            orderid__timestamp__lte=to_date,
                                            orderid__playerid__playerandsitemapping__siteid__id__in=siteids,
                          orderid__playerid__playerandsitemapping__networkid__networkid__in=networkids,
                          orderid__orderstate="FINISHED",orderid__direction="DEPOSIT").\
                          exclude(orderid__ordertype='REAL_ADJUSTMENT')
    else:
        orders = OrderDeviceDetails.objects.values('device_info_id').annotate(count=Count('id')).\
                                        filter(orderid__timestamp__gte=from_date,
                                            orderid__timestamp__lte=to_date,
                                            orderid__orderstate="FINISHED",
                                            orderid__direction="DEPOSIT").\
                                            exclude(orderid__ordertype='REAL_ADJUSTMENT')
    if affiliateid:
        orders = orders.filter(orderid__playerid__affiliate=affiliateid)
    if campaignid:
        orders = orders.filter(orderid__playerid__campaign=campaignid)
    
    if networkids and siteids:
        orders_amount_list = OrderDeviceDetails.objects.values('device_info_id').\
                                    annotate(sum_amount=Sum('orderid__ordermoney')).\
                                        filter(orderid__timestamp__gte=from_date,
                                            orderid__timestamp__lte=to_date,
                                            orderid__playerid__playerandsitemapping__siteid__id__in=siteids,
                          orderid__playerid__playerandsitemapping__networkid__networkid__in=networkids,
                          orderid__orderstate="FINISHED",orderid__direction="DEPOSIT").\
                          exclude(orderid__ordertype='REAL_ADJUSTMENT')
    
    else:
        orders_amount_list = OrderDeviceDetails.objects.values('device_info_id').\
                                    annotate(sum_amount=Sum('orderid__ordermoney')).\
                                    filter(orderid__timestamp__gte=from_date,
                                        orderid__timestamp__lte=to_date,
                                        orderid__orderstate="FINISHED",orderid__direction="DEPOSIT").\
                                        exclude(orderid__ordertype='REAL_ADJUSTMENT')
    if affiliateid:
        orders_amount_list = orders_amount_list.filter(orderid__playerid__affiliate=affiliateid)
    if campaignid:
        orders_amount_list = orders_amount_list.filter(orderid__playerid__campaign=campaignid)
    
    data_list = [{'channel':"Web","subchannel":"WebUI","fl":0, "ul":0,"fgp":0,"cgp":0,"orders":0},
                 {'channel':"Mobile","subchannel":"WebUI", "fl":0,"ul":0,"fgp":0,"cgp":0,"orders":0},
                 {'channel':"Mobile","subchannel":"IOS","fl":0, "ul":0,"fgp":0,"cgp":0,"orders":0},
                 {'channel':"Mobile","subchannel":"Android", "fl":0,"ul":0,"fgp":0,"cgp":0,"orders":0},
                 {'channel':"Tablet","subchannel":"WebUI","fl":0,"ul":0,"fgp":0,"cgp":0,"orders":0},
                 {'channel':"Tablet","subchannel":"IOS","fl":0,"ul":0,"fgp":0,"cgp":0,"orders":0},
                 {'channel':"Tablet","subchannel":"Android","fl":0,"ul":0,"fgp":0,"cgp":0,"orders":0},
                 {'channel':"TV","subchannel":"TV","fl":0,"ul":0,"fgp":0,"cgp":0,"orders":0}]
    
    ul_dt = {"web_webui":0, "mobile_webui":0,"mobile_android":0, 
             "mobile_ios":0, 
             "tablet_ios":0, "tablet_webui":0, "tablet_android":0, "tv_tv":0,
             "other_other":0}
    
    fresh_logins_dt = {"web_webui":0, "mobile_webui":0,"mobile_android":0, 
             "mobile_ios":0, 
             "tablet_ios":0, "tablet_webui":0, "tablet_android":0, "tv_tv":0,
             "other_other":0}
    
    ol_dt = {"web_webui":0, "mobile_webui":0,"mobile_android":0, 
             "mobile_ios":0, 
             "tablet_ios":0, "tablet_webui":0, "tablet_android":0, "tv_tv":0,
             "other_other":0}
    
    ol_sum_dt = {"web_webui":0, "mobile_webui":0,"mobile_android":0, 
         "mobile_ios":0, 
         "tablet_ios":0, "tablet_webui":0, "tablet_android":0, "tv_tv":0,
         "other_other":0}
    reg_dict = {"web_webui":0, "mobile_webui":0,"mobile_android":0, 
         "mobile_ios":0, 
         "tablet_ios":0, "tablet_webui":0, "tablet_android":0, "tv_tv":0,
         "other_other":0}
    
    free_games_dict = {"web_webui":0, "mobile_webui":0,"mobile_android":0, 
         "mobile_ios":0, 
         "tablet_ios":0, "tablet_webui":0, "tablet_android":0, "tv_tv":0,
         "other_other":0}
    cash_games_dict = {"web_webui":0, "mobile_webui":0,"mobile_android":0, 
         "mobile_ios":0, 
         "tablet_ios":0, "tablet_webui":0, "tablet_android":0, "tv_tv":0,
         "other_other":0}
    
    df = Device_info.objects.all()
    df_dict = {}
    for i in df:
        df_dict[str(i.id)] = "%s_%s"%(i.device_type.lower(), i.client_type.lower())
    
    for l in fresh_logins:
        t_count = l.get('count', 0)
        key = df_dict.get(str(l.get("device_info_id")))
        try:
            fresh_logins_dt[key] = fresh_logins_dt[key]+t_count
        except KeyError:
            fresh_logins_dt['web_webui'] = fresh_logins_dt['web_webui']+t_count

    for i in unique_logins:
        t_count = i.get('count', 0)
        key = df_dict.get(str(i.get("device_info_id")))
        try:
            ul_dt[key] = ul_dt[key]+t_count
        except KeyError:
            ul_dt['web_webui'] = ul_dt['web_webui']+t_count
    for j in orders:
        t_count = j.get('count', 0)
        key = df_dict.get(str(j.get("device_info_id")))
        try:
            ol_dt[key] = ol_dt[key]+t_count
        except KeyError:
            ol_dt['web_webui'] = ul_dt['web_webui']+t_count
    
    for k in orders_amount_list:
        t_count = k.get('sum_amount', 0)
        key = df_dict.get(str(k.get("device_info_id")))
        try:
            ol_sum_dt[key] = ol_sum_dt[key]+t_count
        except KeyError:
            ol_sum_dt['web_webui'] = ol_sum_dt['web_webui']+t_count

    for k in reg_list:
        t_count = k.get('count', 0)
        key = df_dict.get(str(k.get("device_info_id")))
        try:
            reg_dict[key] = reg_dict[key]+t_count
        except KeyError:
            reg_dict['web_webui'] = reg_dict['web_webui']+t_count
    
    if networkids and siteids:
        
        cash_games_played_list = Playerrakelog.objects.filter(timestamp__gte=from_date, timestamp__lte=to_date,
                                         amouttype="11",fkgame_id__isnull=False,
                                         tournament_id__isnull=True,
                                         playerid__playerandsitemapping__siteid__id__in=siteids,
                                         playerid__playerandsitemapping__networkid__networkid__in=networkids).values('device_info_id').\
                                         annotate(count=Count('fkgame_id', distinct=True))
        free_games_played_list = Playerrakelog.objects.filter(timestamp__gte=from_date, timestamp__lte=to_date,
                                         amouttype="22",fkgame_id__isnull=False,
                                         tournament_id__isnull=True,
                                         playerid__playerandsitemapping__siteid__id__in=siteids,
                                         playerid__playerandsitemapping__networkid__networkid__in=networkids).values('device_info_id').\
                                         annotate(count=Count('fkgame_id', distinct=True))
    else:
        
        cash_games_played_list = Playerrakelog.objects.filter(timestamp__gte=from_date, timestamp__lte=to_date,
                                         amouttype="11",fkgame_id__isnull=False,tournament_id__isnull=True).values('device_info_id').\
                                         annotate(count=Count('fkgame_id', distinct=True))
        free_games_played_list = Playerrakelog.objects.filter(timestamp__gte=from_date, timestamp__lte=to_date,
                                         amouttype="22",fkgame_id__isnull=False,tournament_id__isnull=True).values('device_info_id').\
                                         annotate(count=Count('fkgame_id', distinct=True))
    if affiliateid:
        cash_games_played_list = cash_games_played_list.filter(playerid__affiliate=affiliateid)
        free_games_played_list = free_games_played_list.filter(playerid__affiliate=affiliateid)
    if campaignid:
        cash_games_played_list = cash_games_played_list.filter(playerid__campaign=campaignid)
        free_games_played_list = free_games_played_list.filter(playerid__campaign=campaignid)

    for k in free_games_played_list:
        t_count = k.get('count', 0)
        key = df_dict.get(str(k.get("device_info_id")))
        try:
            free_games_dict[key] = free_games_dict[key]+t_count
        except KeyError:
            free_games_dict['web_webui'] = free_games_dict['web_webui']+t_count
    
    for k in cash_games_played_list:
        t_count = k.get('count', 0)
        key = df_dict.get(str(k.get("device_info_id")))
        try:
            cash_games_dict[key] = cash_games_dict[key]+t_count
        except KeyError:
            cash_games_dict['web_webui'] = cash_games_dict['web_webui']+t_count
            
    
    totals = {"registrations":0,"fl":0,"ul":0,"orders":0,"deposits":0,"fgp":0,"cgp":0}
    t_reg, t_fl,t_ul,t_orders,t_deposits,t_fgp,f_cgp = 0,0,0,0,0,0,0
    for i in data_list:
        channel_type = i.get('channel')
        subchannel_type = i.get('subchannel')
        key = "%s_%s"%(channel_type.lower(), subchannel_type.lower())
        i['fgp'] = free_games_dict.get(key, 0)
        i['cgp'] = cash_games_dict.get(key, 0)
        i['subchannel'] = "Browser" if subchannel_type == "WebUI" else subchannel_type
        i['fl'] = fresh_logins_dt.get(key, 0)
        i['ul'] = ul_dt.get(key, 0)
        i['orders'] = ol_dt.get(key, 0)
        i['deposits'] = ol_sum_dt.get(key, 0)
        i['registrations'] = reg_dict.get(key, 0)
        i['device'] = "%s - %s"% (channel_type, i['subchannel'])
        t_reg += reg_dict.get(key, 0)
        t_fl += fresh_logins_dt.get(key, 0)
        t_ul += ul_dt.get(key, 0)
        t_orders +=  ol_dt.get(key, 0)
        t_deposits +=  ol_sum_dt.get(key, 0)
        t_fgp += i['fgp']
        f_cgp += i['cgp']

    totals = {'channel':"Total","subchannel":"Total", "device":"Total",
              "registrations":t_reg,"fl":t_fl,"ul":t_ul,"orders":t_orders,"deposits":t_deposits,"fgp":t_fgp,
              "cgp":f_cgp}
    data_list.append(totals)
    return data_list

def get_tournaments_played(playerids):
    total_cash_games_played_list = Playerrakelog.objects.filter(Q(playerid__in=playerids)&
                                                                (Q(fkgame_id__isnull=True)|Q(fkgame_id=0)),
                                                                 tournament_id__isnull=False,
                                                ).values_list('playerid_id').\
                                                annotate(total=Count('tournament_id', distinct=True))
    total_cash_games_dict = dict( (int(i[0]),i[1])  for i in total_cash_games_played_list )

    return {"t_cash":total_cash_games_dict}


def get_players_total_games_played(playerids):
    total_cash_games_played_list = Playerrakelog.objects.filter(Q(playerid__in=playerids)&
                                                                (Q(amouttype="11")| ( Q(amouttype__isnull=True) & ~Q(rakeamount=0))),
                                                                fkgame_id__isnull=False,
                                                                 tournament_id__isnull=True,
                                                ).values_list('playerid_id').\
                                                annotate(total=Count('fkgame_id', distinct=True))
    
    total_free_games_played_list = Playerrakelog.objects.filter( Q(playerid__in=playerids)&
                                                                (Q(amouttype="22")| ( Q(amouttype__isnull=True) & Q(rakeamount=0))),
                                                                 fkgame_id__isnull=False,
                                                                 tournament_id__isnull=True,
                                               ).values_list('playerid_id').\
                                                annotate(total=Count('fkgame_id', distinct=True))
    
    total_cash_games_dict = dict( (int(i[0]),i[1])  for i in total_cash_games_played_list )
    total_free_games_dict = dict( (int(i[0]),i[1])  for i in total_free_games_played_list )

    return {"tgp_cash":total_cash_games_dict, "tgp_free": total_free_games_dict}

def get_players_games_played_in_given_dates(playerids, from_date, to_date):
    total_cash_games_played_list = Playerrakelog.objects.filter(Q(timestamp__gte=from_date)&
                                                                Q(timestamp__lte=to_date) &
                                         Q(playerid__in=playerids)&
                                          (Q(amouttype="11")| ( Q(amouttype__isnull=True) & ~Q(rakeamount=0))),
                                         fkgame_id__isnull=False, 
                                         tournament_id__isnull=True,
                                                ).values_list('playerid_id').\
                                                annotate(total=Count('playerid_id'))
    
    total_free_games_played_list = Playerrakelog.objects.filter(Q(timestamp__gte=from_date)&
                                                                Q(timestamp__lte=to_date) &
                                         Q(playerid__in=playerids)& 
                                        (Q(amouttype="22")| ( Q(amouttype__isnull=True) & Q(rakeamount=0))),
                                         fkgame_id__isnull=False, 
                                         tournament_id__isnull=True,
                                                ).values_list('playerid_id').\
                                                annotate(total=Count('fkgame_id', distinct=True))
    
    cash_games_dict = dict( (int(i[0]),i[1])  for i in total_cash_games_played_list )
    free_games_dict = dict( (int(i[0]),i[1])  for i in total_free_games_played_list )

    return {"cash":cash_games_dict, "free": free_games_dict}

def get_players_gpr_free_cash(playerids):
    resp = {"gpr_free": {},
                "gpr_cash":{}}
    if playerids:
        playerids_str_list = ', '.join(str(x) for x in playerids)
        cursor = connection.cursor()
        #query = """
        #    SELECT  p.id, COUNT(r.id) FROM userapp_playerinfo p
        #    INNER JOIN userapp_reconcilelogs r ON (p.id=r.playerid_id AND DATE(p.registeredon)=DATE(r.timestamp) AND 
        #    transaction_type = "WAGERING"
        #     AND amounttype="FUNCHIPS")
        #    INNER JOIN userapp_playerinfo1 p1 ON p1.playerid_id=p.id
        #    INNER JOIN userapp_device_info d ON p1.device_info_id=d.id
        #    where p.id IN (%s)
        #    GROUP BY p.id;""" % playerids_str_list
        query = """
            SELECT  p.id, COUNT(distinct r.fkgame_id) FROM userapp_playerinfo p
            INNER JOIN userapp_playerrakelog r ON (p.id=r.playerid_id AND DATE(p.registeredon)=DATE(r.timestamp)
                AND (r.amouttype='22' OR (r.amouttype IS NULL AND r.rakeamount = 0)))
            WHERE p.id IN (%s)
            GROUP BY p.id;
        """%playerids_str_list
        cursor.execute(query)
        player_gpr_free_dict = dict((int(i[0]), int(i[1]))  for i in  cursor.fetchall())
        #query = """
        #            SELECT  p.id, COUNT(r.id) FROM userapp_playerinfo p
        #            INNER JOIN userapp_reconcilelogs r ON (p.id=r.playerid_id
        #            AND DATE(p.registeredon)=DATE(r.timestamp)  AND transaction_type="WAGERING"
        #            AND amounttype="CASH") 
        #            WHERE p.id IN (%s)
        #            GROUP BY p.id;
        #    """ % playerids_str_list
        query = """
            SELECT  p.id, COUNT(distinct r.fkgame_id) FROM userapp_playerinfo p
            INNER JOIN userapp_playerrakelog r ON (p.id=r.playerid_id AND DATE(p.registeredon)=DATE(r.timestamp)
                AND (r.amouttype='11' OR (r.amouttype IS NULL OR r.rakeamount != 0)))
            WHERE p.id IN (%s)
            GROUP BY p.id;
        """%playerids_str_list
        cursor.execute(query)
        player_gpr_cash_dict = dict((int(i[0]), int(i[1]))  for i in  cursor.fetchall())
        resp["gpr_free"] = player_gpr_free_dict
        resp["gpr_cash"] = player_gpr_cash_dict
    return resp