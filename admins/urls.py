""" url related to affiliate"""
from django.contrib import admin
from django.conf.urls import include, url
from .views import *
from . import views
from rest_framework import routers
from rest_framework.routers import DefaultRouter
admin.autodiscover()

# Create a router and register our viewsets with it.
router = routers.DefaultRouter(trailing_slash=False)
router.register(r'manage-affiliates', views.AffiliateViewSet)
router.register(r'referral-affiliates', views.AffiliateViewSet)
router.register(r'add-affiliate', views.AffiliateViewSet)
router.register(r'pending-applications', views.AffiliateViewSet)
router.register(r'affiliatecommission', views.AffiliateCommissionViewSet)
router.register(r'commissions-default', views.AffiliateCommissionViewSet)
router.register(r'commissions-campaigns', views.AffiliateCommissionViewSet)
router.register(r'commissions-brands', views.AffiliateCommissionViewSet)
router.register(r'commissions-country', views.AffiliateCommissionViewSet)
router.register(r'deductions', views.DeductionsViewSet)
router.register(r'camptrackmapp', views.CamptrackMappViewSet)
router.register(r'campaign', views.CampaignViewSet)
router.register(r'brand', views.BrandViewSet)
router.register(r'currency', views.CurrencyViewSet)
router.register(r'banners', views.BannerViewSet)
router.register(r'textlinks', views.TextLinkViewSet)
router.register(r'landing-pages', views.LpagesViewSet)
router.register(r'mailers', views.MailersViewSet)
router.register(r'page-peels', views.pagepeelViewSet)
router.register(r'screenshots', views.ScreenShotViewSet)
router.register(r'mediacategory', views.MediaCategoryViewSet)
router.register(r'videos', views.VideoViewSet)
router.register(r'affiliate-manager', views.AccountManagerViewset)
router.register(r'payment-details', views.AffiliateAccountDetailsViewSet)
router.register(r'software-provider-authentication', views.SoftwareProvidersMapping)
router.register(r'proprietary', views.FileUploadViewSet)
router.register(r'brand-mapping', views.SiteSoftwareMapping)
router.register(r'add-new-user', views.AddUserMapping)
router.register(r'tiered-structure', views.TieredStructureViewset)
router.register(r'payment-report', views.PaymentHistoryViewSet)

# urlpatterns = patterns('admins.views',

#         url(r'^admin/$',Signin.as_view(), name='partnerd'),
#         url(r'^admin/home/$','adminhome', name='partner'),
#         url(r'^admin/login/$', Signin.as_view(), name='partner_login' ),
#         url(r'^admin/logout/$','logout', name='partner_logout' ),
#         url(r'^admin/', include(router.urls)),
        
#     ) 

urlpatterns = [
        # url(r'^$',Signin.as_view(), name='partnerds'),
    	url(r'^admin/$',Signin.as_view(), name='partnerd'),
        url(r'^admin/dashboard/$',adminshome, name='partner'),
        # url(r'^admin/homes/$',adminshome, name='partners'),
        url(r'^admin/noaccess/$',not_valid_page, name='nvp'),
        url(r'^admin/coming/$',comingsoon, name='csoon'),
        url(r'^admin/admin/$',comingsoon, name='acsoon'),
        url(r'^admin/payment-details/$',comingsoon, name='adcsoon'),
        url(r'^admin/change-password/$',changepassword, name='cpcsoon'),
        url(r'^admin/faq/$',comingsoon, name='faqcsoon'),
        url(r'^admin/articles/$',comingsoon, name='articles'),
        url(r'^admin/help-center/$',comingsoon, name='hccsoon'),
        url(r'^admin/submit-request/$',comingsoon, name='srcsoon'),
        url(r'^admin/login/$', Signin.as_view(), name='partner_login' ),
        url(r'^admin/logout/$',logout, name='partner_logout' ),
        url(r'^admin/earnings-report/$',viewtrackersinfoadmin, name='partner_viewtrackersinfo' ),
        url(r'^admin/device-report/$',devicestatsinfo, name='partner_deviceinfo' ),
        url(r'^admin/sub-affiliate-report/$',sub_affiliate_report, name='partner_subaffrpt' ),
        url(r'^admin/campaigns-report/$',campaignreportinfo, name='partner_campaigninfo' ),
        url(r'^admin/dynamic-report/$',dynamicreportinfo, name='partner_dynamicinfo' ),
        url(r'^admin/affiliate-activity/$',affiliatereportinfo, name='partner_affiliateinfo' ),
        url(r'^admin/conversions-report/$',conversionreportinfo, name='partner_conversionstatsinfo' ),
        url(r'^admin/brand-report/$',advertisersreportinfo, name='partner_advertisersinfo' ),
        url(r'^admin/player-report/$',activedepositorsinfo, name='partner_activedepositorsinfo'),
        url(r'^admin/messages/$',messages_list, name='partner_messages'),
        url(r'^admin/message-status/$',message_view, name='partner_message_view'),
        url(r'^admin/provider-logs/$',latestdatadump, name='partner_latestdatadump'),
        url(r'^admin/savestatus/$',savestatus, name='partner_savestatus' ),
        url(r'^admin/thanks/$',thanks, name='partner_thanks'),
        url(r'^admin/thanks-admin/$',thanksadmin, name='partner_thanksadmin'),
        url(r'^admin/writedomain/$',writedomain, name='partner_writedomain'),
        url(r'^admin/search-affiliate/$',get_affiliates_filter, name='partner_search' ),
        url(r'^admin/invite-affiliate/$',invite_affiliate, name='partner_invite' ),
        url(r'^admin/affiliate-list/$',affiliate_list, name='partner_aff_list' ),
        url(r'^admin/set-accessible/$',set_program_accessible, name='affiliate_affp-set'),
        url(r'^admin/acceptaffiliateprog/$',acceptaffiliate, name='affiliate_acceptaffiliate'),
        url(r'^admin/selectlp/$',selectlp, name='affiliate_selectlp'),
        url(r'^admin/sdignin/$',sdignin, name='affiliate_sdignin'),
        url(r'^admin/asignin/$',asignin, name='admin_asignin'),
        # Set brand order for admin
        url(r'^admin/brand-order/$',brand_order, name='brand_order'),
        url(r'^admin/email-client-integration/$',email_client_integration, name='email_client_integration'),
        url(r'^admin/email-client-service/$',email_client_service, name='email_client_service'),
        # url(r'^admin/softwareproviders/$',get_softwareproviders, name='partner_softwareproviders' ),
        url(r'^admin/', include(router.urls, namespace="partner")),
        url(r'^admin/whats-new/$', whats_new, name='whats_new'),
        url(r'^admin/get-notifications/$', get_notifications, name='get_notifications'),
        url(r'^admin/wynta-login/$', wynta_login, name='wynta_login'),
        url(r'^admin/registration-fields', registration_page_edit, name='registration_page_edit'),
        url(r'^admin/reg-page-edit-service/', reg_page_edit_service, name='reg_page_edit_service'),
        url(r'^admin/threshold/$',ThresholdAdmin.as_view(), name='admin_threshold'),
        url(r'^admin/save-payment-report/$', save_payment_history, name='save_payment_history'),
        url(r'^admin/logs/$', logs, name='logs'),
        url(r'^admin/get-logs/$', get_logs, name='get_logs'),
        url(r'^admin/get-provider-logs/$', get_provider_logs, name='get_provider_logs'),
        url(r'^admin/search-commission/$', search_commission, name='search_commission'),
        url(r'^admin/search-country/$', search_country, name='search_country'),
        url(r'^admin/view-commission/$', view_commission, name='view_commission'),
        url(r'^admin/invoice/$', invoice, name='invoice')  
]
