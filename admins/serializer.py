from django.contrib.auth import authenticate
from django.utils.translation import ugettext_lazy as _
from rest_framework import exceptions, serializers
from affiliate.models import *
from userapp.models import *
from connectors.models import *

from django.contrib.auth.models import User


class AffiliateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Affiliateinfo
        # exclude = ('ipaddress','registeredon', )
        fields = '__all__'

class AffiliatecommissionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Affiliatecommission
        # fields = '__all__'
        readonly_fields = ('revsharepercent', )

class AccountDetailsSerializer(serializers.ModelSerializer):

    class Meta:
        model = AccountDetails
        fields = '__all__'
        # readonly_fields = ('revsharepercent', )

class DeductionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Deduction
        fields = '__all__'
        # readonly_fields = ('revsharepercent', )

class TieredStructureSerializer(serializers.ModelSerializer):

    class Meta:
        model = TieredStructure
        fields = '__all__'

class CampaignTrackerMappingSerializer(serializers.ModelSerializer):

    class Meta:
        model = CampaignTrackerMapping
        exclude = ('campaign_reg_code', 'goalid', 'url', )

class CampaignSerializer(serializers.ModelSerializer):

    class Meta:
        model = Campaign
        exclude = ('language', )

class CurrencySerializer(serializers.ModelSerializer):

    class Meta:
        model = Currency
        fields = '__all__'

class SiteSerializer(serializers.ModelSerializer):

    class Meta:
        model = Site
        fields = '__all__'

class BannerImagesSerializer(serializers.ModelSerializer):

    class Meta:
        model = BannerImages
        exclude = ('created', )
        # readonly_fields = ('url', 'image')

class LpagesSerializer(serializers.ModelSerializer):

    class Meta:
        exclude = ('created', )
        model = Lpages

class TlinksSerializer(serializers.ModelSerializer):

    class Meta:
        exclude = ('created', )
        model = Tlinks

class MailerSerializer(serializers.ModelSerializer):

    class Meta:
        # exclude = ('created', )
        model = Mailer

class VideoSerializer(serializers.ModelSerializer):

    class Meta:
        exclude = ('created', )
        model = Video

class MediaCategorySerializer(serializers.ModelSerializer):

    class Meta:
        exclude = ('user', )
        model = MediaCategory
        # readonly_fields = ('url', 'image')

class AccountManagerSerializer(serializers.ModelSerializer):

    class Meta:
        model = AccountManager
        fields = '__all__'

class NetworkAffiliateProgrammeMappingSerializer(serializers.ModelSerializer):

    class Meta:
        model = NetworkAffiliateProgrammeMapping
        fields = '__all__'

class FileUploadSerializer(serializers.ModelSerializer):

    class Meta:
        model = FileUpload
        fields = '__all__'


class SiteWhitelabelMappingSerializer(serializers.ModelSerializer):

    class Meta:
        model = SiteWhitelabelMapping
        fields = '__all__'


class PartnerUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = PartnerUser
        fields = '__all__'

class MessagelogsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Messagelogs
        fields = '__all__'

class PaymentHistorySerializer(serializers.ModelSerializer):

    class Meta:
        model = PaymentHistory
        fields = '__all__'
