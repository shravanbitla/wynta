affiliate = [
{
    "Affiliateinfo":{
    "networkid" :  "Network Id",
    "account_manager" :  "Account Manager",
    "mappingid" :  "Mapping Id",
    "website" :  "Website",
    "siteid" :  "Site",
    "username" :  "Username",
    "password" :  "Password",
    "contactname" :  "First Name",
    "lastname" : "Last Name",
    "companyname" :  "Company Name",
    "email" :  "Email",
    "address" :  "Address",
    "address2" :  "Address2",
    "city" :  "City",
    "state" :  "State",
    "country" :  "Country",
    "postcode" :  "Post Code",
    "telephone" :  "Phone",
    "ipaddress" :  "Ip Adress",
    "registeredon" :  "Registered On",
    "active" :  "Active",
    "status" :  "Status",
    "mailrequired" :  "Mail Required",
    "mobilerequired" :  "Mobile Required",
    "im" :  "IM",
    "skype" :  "Skype",
    "dob" :  "Date Of Birth",
    "campaignlimit" :  "Campaign Limit",
    "superaffiliate" :  "Super Affiliate Id",
    "accessible" :  "Accessible",
    "lastlogin" :  "Last Login"
}
}, 

{
 "Inviteaffiliate": {
    "fk_affiliateinfo" :  "Affiliate",
    "account_manager" :  "Account Manager"
 }},
{
 "Advertiser": {
  
    "account_manager" :  "User",
    "link" :  "Link",
    "description" :  "Description",
    "name" : "Name",
    "created_timestamp" : "Created On"
 }},
{
 "Offer": {
    "networkid" :  "Network",
    "advertiser" :  "Advertiser",
    "siteid" : "Site",
    "fk_affiliateinfo" :  "Affiliate",
    "link" :  "Link",
    "name" :  "Name",
    "created_timestamp" :  "Created On"
 }},
{
 "MappingParameters": {
    "client" :  "Site",
    "generic_parameter" :  "Generic Parameter",
    "client_parameter" :  "Client Paramater"
 }},
{
 "Chequedetails": {
    
    "fk_affiliateinfo" :  "Affiliate",
    "chequepayableto" :  "Cheque Payable"
 }},
 {
 "Wiretransfer": {
    "fk_affiliateinfo" :  "Affiliate",
    "accountnumber" :  "Account Number",
    "ifsccode" :  "IFSC CODE",
    "accounttype" :  "Account Type",
    "accountname" :  "Account Name",
    "bankname" :  "Bank Name",
    "bankaddress" :  "Bank Address"
 }},
{
 "MediaCategory": {
    "networkid" : "Network", 
    "siteid" :  "Site",
    "category_name" :  "Name",
    "user" :  "User"
 }},
{
 "Campaign": {
    "networkid" :  "Network",
    "siteid" :  "Site",
    "fk_affiliateinfo" :  "Affiliate Info",
    "account_manager" :  "Account Manager",
    "link" :  "Link",
    "name" :  "Name",
    "created_timestamp" :  "Created At",
    "media_type" :  "Media Type",
    "media_category" :  "Media Category",
    "language" :  "Language"
 }},

{
 "CampaignTrackerMapping": {    
    "fk_campaign" :  "Campaign",
    "fk_affiliateinfo" :  "Affiliate",
    "trackerid" :  "Tracker Id",
    "url" :  "URL",
    "campaign_reg_code" :  "Campaign Registation Code",
    "goalid" :  "Goal Id"
 }},
{
 "CampaignClicksCount": {
    "fk_campaign" :  "Campaign Tracker Mapping",
    "date" :  "Date",
    "platform" :  "Platform",
    "clicks" :  "Clicks",
    "country" :  "Country"
 }},
   
{
 "Notes": {
    "siteid" : "Site", 
    "fk_affiliateinfo" :  "Affiliate",
    "notes" :  "Notes"
 }},
{
 "Currency": {
    "siteid" : "Site",
    "currency" :  "Currency"
 }},

{
 "BannerImages": {
  
    "language" :  "Language",
    "title" :  "title",
    "siteid" :  "Site",
    "created" :  "Created",
    "width" :  "Width",
    "height" :  "Height",
    "link" :  "link",
    "banner" :  "Banner Images",
    "user" :  "User",
    "media_type" : "Media Type",
    "mappingid" :  "Mapping Id"
 }},

{
 "Bannerlogs": {
   
    "fk_campaign" :  "Campaign",
    "fk_bannerimages" :  "Banner Images",
    "link" :  "Link"

 }}, 
{
 "Affiliatecommission": {
    "revshare" :  "Revenue Share",
    "fk_affiliateinfo" :  "Affiliate",
    "fk_campaign" :  "Campaign",
    "siteid" :  "Site",
    "country" :  "Country",
    "pocdeduction" :  "POC Deduction",
    "account_manager" :  "Account Manager",
    "revsharepercent" :  "Revnue Share Percent",
    "cpachoice" :  "CPA Choice", 
    "criteriaacquisition" :  "Criteria Acquisition",
    "criteriavalue" :  "Criteria Value",
    "cpacommissionvalue" :  "CPA Commision Value",
    "referalchoice" :  "Referal Choice",
    "referalcommissionvalue" :  "Referal commission value",
    "start_date" :  "Start Date",
    "end_date" :  "End Date",
    "active" :  "Active",
    "currency" :  "Currency",
    "ringfence" :  "Ringfence",
    "exclude_rev" :  "Exclude Revenue",
    "monthToDate":   "Month to Date",
    "comm_type" :  "Commission Type",
    "notes" :  "Notes",
    "created_at" :  "Set On"
 }},
{
 "Deduction": {
    "siteid" : "Site",
    "pocdeduction" :  "POC Deduction",
    "adminfee" :  "Admin Fee",
    "start_date" :  "Start Date",
    "end_date" :  "End Date",
    "country" :  "Country", 
    "poctvalue" :  "POCT Value",
    "adminfeevalue" :  "Admin Fee Value"
 }},
{
 "TieredStructure": {
    "siteid" : "Site",
    "account_manager" : "Account Manager", 
    "start_date" :  "Start Date",
    "end_date" :  "End Date",
    "stype" :  "S Type",
    "options" :  "Options"
 }},
 {
 "Hitcount": {
    "fk_affiliateinfo" :  "Affiliate",
    "fk_campaign" :  "Campaign",
    "timestamp" :  "Created On",
    "hits" :  "Htis",
    "device_info_id" : "Device Info"
 }},
 {
 "AccountDetails": {
    "fk_affiliateinfo" : "Affiliate",
    "accountnumber" :  "Account Number",
    "sortcode" :  "Sort Code",
    "accounttype" :  "Account Type",
    "accountname" :  "Account Name",
    "iban" :  "IBAN",
    "swift" :  "Swift",
    "bankname" :  "Bank Name",
    "bankaddress" :  "Bank Address",
    "beneficiaryaddress" :  "Beneficiary Address",
    "currency" : "Currency",
    "paypal_username" :  "Paypal Username",
    "paypal_email" :  "Paypal Email",
    "neteller_email" :  "Neteller Email",
    "neteller_username" :  "Netller Username",
    "neteller_accountnumber" :  "Neteller Account Number"
 }},
 {
 "AffiliatePasswordResetlogs": {
    "id" :  "Id",
    "affiliateid" : "Affiliate",
    "token" :  "Token",
    "expiretimestamp" :  "Expire Timestamp"
 }},
 {
 "AffiliateContactUs": {
    "name" :  "Name",
    "username" : "Username",
    "email" :  "Email",
    "subject" :  "Subject",
    "message" :  "Message"
 }},
 {
 "WyntaSupport": {
    "mobile" :  "Mobile",
    "email" :  "Email",
    "companyname" :  "Company Name"
 }},
 {
 "FozilSupport": {
    "mobile" :  "Mobile",
    "ipaddress" :  "Ip Adress",
    "timestamp" :  "Created On",
    "email" :  "Email",
    "data" :  "Data"
 }},
{
 "VcommFPAffiliateMapping": {
    "vcommsubid" :  "Vcomm Sub ID",
    "vcommsubname" :  "Vcomm Sub Name",
    "affiliateid" : "Affiliate"
 }},
{
 "Messagelogs": {
    "affiliate" :  "Affiliate Info",
    "account_manager" :  "AccountManager", 
    "user" : "User",
    "messagetrigger" :  "MessageTrigger",
    "from_email" :  "From Email",
    "to_email" :  "To Email",
    "timestamp" : "Created On",
    "content" :  "Content",
    "status" :  "Status",
    "subject" :  "Subject",
    "affstatus" :  "Affiliate Status"
 }},
{
 "Mailer": {
   
    "subject" : "Subject",
    "siteid" :  "Site",
    "content" :  "Content",
    "user" :  "User"
 }},
{
 "Mailerlogs": {
    "fk_campaign" :  "Campaign",
    "mailer" : "Mailer",
    "link" :  "Link"
 }},
{
 "ScreenShotslogs": {
    "fk_campaign" :  "Campaign",
    "screenimages" :  "BannerImages",
    "link" :  "Link"
 }},
{
 "PagePeellogs": {
    "fk_campaign" :  "Campaign",
    "pagepeelimage" :  "BannerImages",
    "link" :  "Link"
 }},
{
 "Video": {
    "language" :  "Language",
    "title" :  "Title",
    "siteid" :  "Site",
    "created" :  "Created On",
    "link" :  "Link",
    "video" :  "Videos Path",
    "user" :  "User"
 }},
{
 "Videologs": {
    "fk_campaign" :  "Campaign",
    "fk_video" :  "Video",
    "link" :  "Link"
 }},
{
 "Tlinks": {
    "language" :  "Language",
    "title" :  "Title",
    "siteid" :  "Site",
    "created" :  "Created On",
    "link" :  "Link",
    "user" :  "User"

 }},
 {
 "Tlinkslogs": {
   
    "fk_campaign" :  "Campaign",
    "fk_tlink" :  "Tlinks", 
    "link" :  "Link"
 }},
{
 "Lpages": {
    "language" :  "Language",
    "title" :  "Title",
    "siteid" :  "Site",
    "created" :  "Created On",
    "link" :  "Link",
    "lpages" :  "Lpages",
    "user" :  "User"
 }},
{
 "LandingPagelogs": {
    "timestamp" :  "Created On",
    "action" :  "Action",
    "object_id" :  "Id",
    "created_at" :  "Set On",
    "last_modified" :  "Last Modified",
    "serialized_data" :  "Seriazlized Data",
    "changed_fields" :  "Changed Fields",
    "content_type" :  "Content Type",
    "request_event" :  "Request Event",
    "affiliateprogramme" :  "AffiliateProgramme",
    "user" :  "User", 
    "ipaddress" :  "Ip Adress"
}},
{
 "PaymentHistory": {
    "status" :  "Status",
    "payment_type" :  "Payment Type",
    "timestamp" :  "Created On",
    "month" :  "Month",
    "year" :  "Year",
    "amount" :  "Amount",
    "adjusted_amount" : "Adjusted Amount",
    "created_at" :  "Set On",
    "last_modified" :  "Last Modified",
    "fk_affiliateinfo" :  "Affiliate Info",
    "paid_status" :  "Paid Status",
    "carried_amount" :  "Carried Amount",
   "threshold_amount" :  "Threshold Amount",
    "balance" :  "Balance"
 }
},
{
 "Threshold": {
    "account_manager" :  "Account Manager",
    "bankwire" :  "Bank Wire Transfer",
    "neteller" :  "Neteller",
    "paypal" :  "Paypal"
 }},
{
 "Invoice": {
    "logopath" :  "Logo Path",
    "pay_history" :  "Pay History",
    "timestamp" :  "Created On"
 }},
{ 
   "Device_info":{
    "id" : "Id",
    "device_type" : "Device Type",
    "client_type" : "Client Type",
    "operating_system" : "Operating System"
   }},


{ "Network":{
    "networkid" : "Network",
    "networkname" : "Network Name",
    "connect_type" : "Connect Type"
}},

{ "NetworkAndSiteMap":{
    "networkid" : "Network", 
    "siteid" : "Site",
    "logo" : "Logo Path"
}},
{ 
   "Country":{ 
    "countryname" : "Country Name"
}},

{ "State":{
    
    "country" : "Country",
    "statename" : "State Name"
}},

{ "AffiliateProgramme":{
    "name" : "Name",
    "domain" : "Domain",
    "wynta_domain" : "Wynta Domain",
    "logo" : "Logo Path",
    "bgcolor" : "BG color",
    "bgtext" : "BG Text",
    "buttonimage" : "Button Images",
    "buttoncolor" : "Button Color",
    "buttontext" : "Button Text",
    "tablecolor" : "Table Color",
    "tabletext" : "Table Text",
    "networkid" : "Network Id",
    "ssl_state" : "SSL State",
    # order : models.IntegerField(null:True, blank:True)
    "accessible" : "Accessible"
}},

{ "MessageTemplate":{

    "subject" : "Subject",
    "content" : "Content"
}},

{ "CurrencyConverted":{
   
    "eurtogbp" : "Euro To GBP",
    "gbptoeur" : "GBP TO Euro",
    "date" : "Date"
}},

{ "MessageTrigger":{

    "affiliateprogramme" : "Affiliate Programme",
    "templates" : "Templates",
    "triggercondition" : "Trigger Condition",
    "status" : "Status"
}},

{ "AccountManager":{
    "user" : "User",
    # user : models.ForeignKey('auth.user')
    "affiliateprogramme" : "Affiliate Programme",
    "timestamp" : "Created on",
    "email" : "Email",
    "amcactive" : "AMC Active",
    "pmcactive" : "PMC Active",
    "status" : "Status",
    "siteid" : "Site",
    "rev_percent_cut" : "Revenue Percent Cut"
}},

{ "SiteOrder":{
    "accountmanager_id" : "Account Manager", 
    "site_id" : "Site", 
    "order_id" : "Orde Id"
}},

{ "SuperAdmin":{
    "user" : "User",
    # user : models.ForeignKey('auth.user')
    "timestamp" : "Created On",
    "email" : "Email"
}},

{ "NetworkAffiliateProgrammeMapping":{
    "networkid" : "Network", 
    "programme" : "Affiliate Programme", 
    "accmanager" : "Account Manager",
    "siteid" : "Site", 
    "username" : "Username",
    "password" : "Password",
    "retrive" : "Retrieve"
}},

{ "PartnerUser":{
    "user" : "User",
    # user : models.ForeignKey('auth.user')
    "programme" : "Affiliate Programme", 
    "accmanager" : "Account Manager", 
    "timestamp" : "Created On",
    "email" : "Email",
    "options" : "Options",
    "role" : "Role"
}},
        
{ "Notifications":{
    "accmanager" : "Account Manager",
    "affinfo" : "Affiliate",
    "notificationtype" : "Notification Type",
    "title" : "Title",
    "content" : "Content",
    "redirecturl" : "Redirect Url",
    "lastupdated" : "Last Upadated",
    "lastseen" : "Last Seen",
    "category" : "Category"
}},

{ "RegistrationPageEdit":{
    "accmanager" : "Account Manager",
    "options" : "Options"
}},

{ "Emailclient":{
    "accmanager" : "AccountManager",
    "username" : "Username",
    "password" : "Password",
    "emailclient" : "Email Client",
    "domain" : "Domain",
    "apikey" : "API Key",
    "partuser" : "Partner User"
}},
{ "Onelogin":{
    "account" : "Accounts",
    "ipaddress" : "IP Address",
    "timestamp" : "Created On"
}},
{ "User":{
    "password" : "Password",
   "last_login": "Last Login",
   "is_superuser": "Super User",
   "first_name" : "First Name",
   "last_name" : "Last Name",
   "email" : "Email",
   "is_staff" : "Staff",
   "is_active" : "Active",
   "date_joined" : "Date Joined"
}},
{
   "Site" : {
      "domain" : "Domain",
      "name" : "Name"
   }
}
]

