"""Create your views here."""
#!/usr/bin/python
# -*- coding: latin-1 -*-
import ast
import random
import csv
import collections
import os, sys
import shutil
import math
import json
import subprocess
import string
from django.contrib.auth import authenticate, login
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.views.generic.base import TemplateView
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.contrib.sites.models import Site
from django.core.mail import EmailMessage
from django.contrib.auth.hashers import make_password, check_password
# # # # # # # # # # from django.contrib.sites.models import get_current_site
from django.contrib.admin.views.decorators import staff_member_required
from django.utils.decorators import method_decorator
from django.db.models import Sum, Count
from django.db import connection
from django.contrib.sites.models import Site
from django.shortcuts import render
from affiliate.utils import *
from django.contrib.auth.models import User
from affiliate.dashboard_data import *
from userapp.models import *
from userapp.mails import *
from affiliate.models import *
from connectors.models import *
from connectors.retrievers import *
from userapp.function import *
from .forms import *
from django.db.models import Q
from userapp.utils import get_network_sites_list, error_log, get_site_player_first_register_timestamp
from datetime import date, datetime, time
import calendar
# from django.contrib.admin.views.decorators import staff_member_required
from userapp.utils import get_fromdate_todate_for_django_query_filter, get_hexdigest,\
    get_site_player_first_register_timestamp
from userapp.utils import monthdelta, MyEncoder,get_paginate_dimensions, get_chips_type
from affiliate.utils import enc_password
from copy import deepcopy
from affiliate.dashboard_data import get_affiliate_campaign_link
#from userapp.userapp_db_entry import get_channels_list
from functools import wraps
from connectors.models import *

from rest_framework.response import Response
from rest_framework import exceptions, filters, viewsets
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import renderers
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import detail_route, parser_classes
from django_filters.rest_framework import DjangoFilterBackend
# from rest_framework.mixins import RetrieveModelMixin, UpdateModelMixin
from rest_framework.parsers import FormParser, MultiPartParser
# Create your views here.
# from rest_framework.status import HTTP_201_CREATED, HTTP_400_BAD_REQUEST
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from .serializer import *
from django.shortcuts import get_list_or_404, get_object_or_404
import time


def user_class_role_management(extra_value=None):
    def _user_class_role_management(view_func):
        def _decorator(clss, request, *args, **kwargs):
            pid = request.session.get('partnerid')
            if not pid:
                return HttpResponseRedirect(reverse('partner_login'),)
            user = User.objects.get(id=pid)
            partuser_obj = []
            acc_manager = None
            acc_manager_obj = AccountManager.objects.filter(user=user)
            if acc_manager_obj:
                acc_manager = acc_manager_obj[0]
            else:
                partuser_obj = PartnerUser.objects.filter(user=user)
                acc_manager = partuser_obj[0].accmanager
            if acc_manager.status == 'Aborted':
                request.session['Aborted'] = True
                request.session['partnerid'] = False 
                return HttpResponseRedirect(reverse('partnerd'))
            if not acc_manager or (partuser_obj and extra_value not in ast.literal_eval(partuser_obj[0].options)):
                return HttpResponseRedirect(reverse('nvp'),)
            else:
                response = view_func(clss, request, *args, **kwargs)
                # maybe do something after the view_func call
                return response
        return wraps(view_func)(_decorator)
    return _user_class_role_management


def user_role_management(extra_value=None):
    def _user_role_management(view_func):
        def _decorator(request):
            pid = request.session.get('partnerid')
            if not pid:
                return HttpResponseRedirect(reverse('partner_login'),)
            user = User.objects.get(id=pid)
            partuser_obj = []
            acc_manager = None
            acc_manager_obj = AccountManager.objects.filter(user=user)
            if acc_manager_obj:
                acc_manager = acc_manager_obj[0]
            else:
                partuser_obj = PartnerUser.objects.filter(user=user)
                acc_manager = partuser_obj[0].accmanager
            if acc_manager.status == 'Aborted':
                request.session['Aborted'] = True
                request.session['partnerid'] = False
                return HttpResponseRedirect(reverse('partnerd'))
            if not acc_manager or (partuser_obj and extra_value not in ast.literal_eval(partuser_obj[0].options)):
                return HttpResponseRedirect(reverse('nvp'),)
            else:
                request.user.username = user.username
                request.user.email = user.email
                kwargs = {'acc_manager': acc_manager, 'partuser_obj': partuser_obj,
                            'user': user}
                response = view_func(request, **kwargs)
                # maybe do something after the view_func call
                return response
        return wraps(view_func)(_decorator)
    return _user_role_management

def get_u_details(request):
    pid = request.session.get('partnerid')
    user = User.objects.get(id=pid)
    acc_manager_obj = AccountManager.objects.filter(user=user)
    partuser_obj = []
    if acc_manager_obj:
        acc_manager = acc_manager_obj[0]
    else:
        partuser_obj = PartnerUser.objects.filter(user=user)
        acc_manager = partuser_obj[0].accmanager
    request.user.username = user.username
    request.user.email = user.email
    return acc_manager, partuser_obj, user


def not_valid_page(request):
    acc_manager, partuser_obj, user = get_u_details(request)
    context_dict = {'partner': acc_manager.affiliateprogramme.name,
            'partner_obj':acc_manager.affiliateprogramme,
            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []}
    return render(request, 'partner/notvalidurl.html', context_dict)

def delete_affiliate_session(request):
    if hasattr(request, 'session'):
        for key in request.session.keys():
            del request.session[key]

def logout(request):
    """ Logout affiliate from website """
    try:
        delete_affiliate_session(request)
    except KeyError:
        pass
    return HttpResponseRedirect(reverse("partner_login"))

def get_affiliate_session_id(request):
    affiliate_id = None
    if hasattr(request, 'session'):
        pid = request.session.get('partner_session_id')
    return pid

def savestatus(request):
    # Save status of the pending affiliate in pending affiliates page.
    resp_dict = {}
    try:
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner'),)
        acc_manager, partuser_obj, user = get_u_details(request)
        aff_id = request.GET.get('aff_id')
        status = request.GET.get('status')
        s_aff = Affiliateinfo.objects.get(id=int(aff_id))
        s_aff.status = status
        s_aff.save()
        if status == 'Approved':
            statuschangemail(s_aff,partuser_obj, s_aff, 'AFFILIATE_SIGN_UP_APPROVAL')
        else:
            statuschangemail(s_aff,partuser_obj, s_aff, 'AFFILIATE_SIGN_UP_REJECTED')
        resp_dict['status'] = 'Success'
        request.session['pending_count'] = Affiliateinfo.objects.filter(
                        account_manager=s_aff.account_manager,
                        status="Pending").count()
    except Exception as e:
        resp_dict['error_message'] = str(e)
        resp_dict['status'] = 'Error'
        return HttpResponse(json.dumps(resp_dict, indent=4))
    return HttpResponse(json.dumps(resp_dict, indent=4))


def acceptaffiliate(request):
    context_dict = {}
    try:
        aff_prog = AffiliateProgramme.objects.filter(Q(domain__icontains=str(request.META['HTTP_HOST'])) | Q(
                                    wynta_domain__icontains=str(request.META['HTTP_HOST'])))
        if aff_prog:
            aff_prog = aff_prog[0]
        elif len(aff_prog) == 0:
            return render(request, 'siteinvalid.html', context_dict)
        else:
            if 'fozil' in request.META['HTTP_HOST']:
                aff_prog = AffiliateProgramme.objects.all()[0]
            else:
                aff_prog = AffiliateProgramme.objects.all()[1]
        aid = request.GET.get('aid')
        obj = Affiliateinfo.objects.select_related('account_manager').get(id=aid)
        account_manager = AccountManager.objects.filter(affiliateprogramme=aff_prog)[0]
        aff_obj = Affiliateinfo.objects.create(username=obj.username,
                   password=obj.password, contactname=obj.contactname,
                   companyname=obj.companyname,
                   email=obj.email,
                   address=obj.address, city=obj.city,
                   state=obj.state, country=obj.country,
                   ipaddress=obj.ipaddress,
                   registeredon=datetime.now(),
                   postcode=obj.postcode, telephone=obj.telephone,
                   dob=obj.dob,
                   account_manager=account_manager)
        networkid_obj = Network.objects.all()
        for nt in networkid_obj:
            aff_obj.networkid.add(nt)
            aff_obj.save()
        for siteobj in account_manager.siteid.all():
            aff_obj.siteid.add(siteobj)
            aff_obj.save()
        aff_obj.save()
        return HttpResponseRedirect("/affiliate/login/")
    except Exception as e:
        return HttpResponseRedirect("/affiliate/login/")


def set_program_accessible(request):
    context_dict = {}
    pid = request.session.get('partnerid')
    if pid:
        user = User.objects.get(id=pid)
        acc_manager_obj = AccountManager.objects.filter(user=user)
        access = request.POST.get('access')
        if access == 'true':
            access = True
        else:
            access = False
        prog_obj = acc_manager_obj[0]
        aff_prog_obj = prog_obj.affiliateprogramme
        aff_prog_obj.accessible = access
        aff_prog_obj.save()
        context_dict['status'] = 'Success'
        return HttpResponse(json.dumps(context_dict, indent=4))
    else:
        context_dict['status'] = 'Error'
        context_dict['message'] = str(e)
        return HttpResponse(json.dumps(context_dict, indent=4))

def invite_affiliate(request):
    context_dict = {}
    try:
        pid = request.session.get('partnerid')
        user = User.objects.get(id=pid)
        acc_manager_obj = AccountManager.objects.filter(user=user)
        partuser_obj = []
        if acc_manager_obj:
            acc_manager = acc_manager_obj[0]
        else:
            partuser_obj = PartnerUser.objects.filter(user=user)
            acc_manager = partuser_obj[0].accmanager
        aff_id = request.GET.get('aid')
        aff_obj = Affiliateinfo.objects.select_related('account_manager').get(id=aff_id)
        try:
            affiliateinvite(acc_manager, aff_obj, 'INVITE_AFFILIATE')
            invite_obj, created = Inviteaffiliate.objects.get_or_create(fk_affiliateinfo=aff_obj,
                account_manager=acc_manager)
            invite_obj.save()
        except Exception as e:
            context_dict['status'] = 'Error'
            return HttpResponse(json.dumps(context_dict, indent=4))
        context_dict['status'] = 'Success'
        context_dict['message'] = 'Successfully Invited.'
        return HttpResponse(json.dumps(context_dict, indent=4))
    except Exception as e:
        context_dict['status'] = 'Error'
        return HttpResponse(json.dumps(context_dict, indent=4))


def affiliate_list(request):
    context_dict = {}
    try:
        pid = request.session.get('partnerid')
        user = User.objects.get(id=pid)
        acc_manager_obj = AccountManager.objects.filter(user=user)
        partuser_obj = []
        if acc_manager_obj:
            acc_manager = acc_manager_obj[0]
        else:
            partuser_obj = PartnerUser.objects.filter(user=user)
            acc_manager = partuser_obj[0].accmanager
        available_aff_list = Affiliateinfo.objects.filter(account_manager=acc_manager)
        aff_to_call_list = Affiliateinfo.objects.exclude(account_manager=acc_manager)
        aff_to_call_list = aff_to_call_list.filter(accessible=True)
        avv_aff_prog = []
        for i in available_aff_list:
            att_list = [i.id, i.username, i.email, i.companyname]
            avv_aff_prog.append(att_list)
        context_dict['avv_aff'] = avv_aff_prog

        display_aff_prog = []
        for i in aff_to_call_list:
            if 'vcommsub' in i.email.lower():
                continue
            invite_obj = Inviteaffiliate.objects.filter(fk_affiliateinfo=i,
                account_manager=acc_manager)
            att_list = [i.id, i.username, i.email, i.companyname]
            if invite_obj:
                att_list.append('invited')
                display_aff_prog.insert(0, att_list)
            else:
                att_list.append('invite')
                display_aff_prog.append(att_list)
        context_dict['call_aff'] = display_aff_prog
        context_dict['accessible'] = acc_manager.affiliateprogramme.accessible
        context_dict['status'] = 'Success'
        context_dict['message'] = 'Successfully Requested.'
        return HttpResponse(json.dumps(context_dict, indent=4))
    except Exception as e:
        context_dict['status'] = 'Error'
        return HttpResponse(json.dumps(context_dict, indent=4))


class Signin(TemplateView):

    def get(self, request):
        context_dict = {}
        id = request.session.get('partnerid')
        if id:
            return HttpResponseRedirect(reverse('partner'),)
        ap_obj = AffiliateProgramme.objects.filter(Q(domain__icontains=str(request.META['HTTP_HOST'])) | Q(
                                    wynta_domain__icontains=str(request.META['HTTP_HOST'])))
        if ap_obj:
            ap_obj = ap_obj[0]
        elif len(ap_obj) == 0:
            ap_obj = AffiliateProgramme.objects.all()[0]
            # return render(request, 'siteinvalid.html', context_dict)
        else:
            if 'fozil' in request.META['HTTP_HOST']:
                ap_obj = AffiliateProgramme.objects.all()[0]
            else:
                ap_obj = AffiliateProgramme.objects.all()[1]
        aborted = request.session.get("Aborted")
        if aborted:
            context_dict['message'] = "Your trial period has expired! </br> You can go ahead and upgrade Or </br>contact Wynta team for an extension of the trial."
            del request.session["Aborted"]
        context_dict['partner'] = ap_obj.name
        context_dict['partner_obj'] = ap_obj
        # pwd_reset_done = request.session.get("aff_pwd_reset_done")
        # if pwd_reset_done:
        #     context_dict['message'] = "Your password changed successfully. Please login to continue."
        #     del request.session["aff_pwd_reset_done"]
        return render(request, 'partner/auth/login.html', context_dict) 
                                  #context_instance=RequestContext(request))

    def post(self, request):
        context_dict = {}
        username = str(request.POST.get('username'))
        user_obj = User.objects.filter(email=username)
        ap_obj = AffiliateProgramme.objects.filter(Q(domain__icontains=str(request.META['HTTP_HOST'])) | Q(
                                    wynta_domain__icontains=str(request.META['HTTP_HOST'])))
        if ap_obj:
            ap_obj = ap_obj[0]
            context_dict['partner'] = ap_obj.name
            context_dict['partner_obj'] = ap_obj
        # ap_obj = AffiliateProgramme.objects.filter(Q(domain__icontains=str(request.META['HTTP_HOST'])) | Q(
        # if ap_obj:
        #     ap_obj = ap_obj[0]
        # else:
        #     return render(request, 'siteinvalid.html', context_dict)
        # context_dict['partner'] = ap_obj.name
        # context_dict['partner_obj'] = ap_obj
        if not user_obj:
            context_dict['error'] = True
            context_dict['message'] = "The email address entered is not a valid one."
            context_dict['status'] = 'Error'
            return render(request, 'partner/auth/login.html', context_dict)
        password = str(request.POST.get('passwd'))
        user = authenticate(username=user_obj[0].username, password=password)
        if user is not None:
            if user.is_active:
                acc_manager_obj = AccountManager.objects.filter(user=user)
                if acc_manager_obj:
                    acc_manager = acc_manager_obj[0]
                else:
                    partuser_obj = PartnerUser.objects.filter(user=user)
                if len(acc_manager_obj) < 1 and len(partuser_obj) < 1 :
                    context_dict['error'] = True
                    context_dict['message'] = "The email address is not a account manager."
                    context_dict['status'] = 'Error'
                    return render(request, 'partner/auth/login.html', context_dict) 
                    # return render_to_response('partner/auth/login.html',
                    #           context_dict,
                    #           context_instance=RequestContext(request))
                    # return HttpResponse(json.dumps(context_dict), content_type='application/json')
                # if ap_obj not in acc_manager_obj:
                #     context_dict['error'] = True
                #     context_dict['message'] = "The email address is not a account manager."
                #     context_dict['status'] = 'Error'
                #     return render(request, 'partner/auth/login.html', context_dict)
                login(request, user)
                request.session['partnerid'] = str(user.id)
                request.session['partner_session_id'] = str(user.id)
                return HttpResponseRedirect(reverse('partner'),)
            else:
                context_dict['error'] = True
                context_dict['message'] = "The email address is not active."
                context_dict['status'] = 'Error'
                return render(request, 'partner/auth/login.html', context_dict) 
                # return render_to_response('partner/auth/login.html',
                #               context_dict,
                #               context_instance=RequestContext(request))
        else:
            context_dict['error'] = True
            context_dict['message'] = "The email address or password entered is incorrect."
            context_dict['status'] = 'Error'
            return render(request, 'partner/auth/login.html', context_dict)
            # return render_to_response('partner/auth/login.html',
            #                   context_dict,
            #                   context_instance=RequestContext(request))

def paginationdiv(data, cpage):
    page_div = ''
    check_dot = 0
    len_d = int(math.ceil(data['count']/10.0))
    if len_d > 5:
        check_dot = 1
    # elif len_d < 2:
    #     return ''
    if data['next']:
        fields = data['next'].split('?')[1].split('&')[0] if (data['next'] and '&' in data['next']) else ''
    elif data['previous']:
        fields = data['previous'].split('?')[1].split('&')[0] if (data['previous'] and '&' in data['previous']) else ''
    if data['previous']:
        page_div = '<a class="paginate_button previous" aria-controls="player_data" id="player_data_previous" href="'+data['previous']+'">Previous</a>' #'<li><a href="'+data['previous']+'" aria-label="Previous"><span aria-hidden="true">Previous</span></a></li>'
    else:
        page_div = '<a class=" paginate_button previous disabled" aria-controls="player_data" id="player_data_previous"> Previous</a>' #'<li class="disabled"><a href="." aria-label="Previous"><span aria-hidden="true">Previous</span></a></li>'
    for i in range(1,int(math.ceil(data['count']/10.0))+1):
        if i == 1 and cpage == i:
            page_div += '<a class="paginate_button paging_simple_numbers current" style="background:#19b8f8; color:#fff;" aria-controls="player_data" data-dt-idx="'+str(i)+'" tabindex="0">%s</a>' % str(i) 
        elif cpage == i:
            page_div += '<a class="paginate_button paging_simple_numbers current" style="background:#19b8f8; color:#fff;" aria-controls="player_data" href="?page='+str(i)+'&'+fields+'" data-dt-idx="'+str(i)+'"  tabindex="0">%s</a>' % str(i)
        elif i==1 or i==len_d:
            page_div +='<a class="paginate_button " aria-controls="player_data" data-dt-idx="'+str(i)+'"  href="?page='+str(i)+'&'+fields+'" tabindex="0">'+str(i)+'</a>'
        elif abs(cpage - i) == 1: 
            page_div +='<a class="paginate_button " aria-controls="player_data" data-dt-idx="'+str(i)+'"  href="?page='+str(i)+'&'+fields+'" tabindex="0">'+str(i)+'</a>'
        elif check_dot ==1 :
            page_div += '<span class="ellipsis">..</span>'
            check_dot += 1
        elif check_dot == 2 and i > cpage:
            page_div += '<span class="ellipsis">..</span>'
            check_dot += 1

    if data['next']:
        page_div += '<a class="paginate_button next" aria-controls="player_data" id="player_data_next" rel="next" href="'+data['next']+'&'+fields+'">Next </a>' #'<li><a href="'+data['next']+'" aria-label="Next"><span aria-hidden="true">Next</span></a></li>'
    else:
        page_div += '<a class="paginate_button next disabled" aria-controls="player_data" id="player_data_next" rel="next">Next</span>' #'<li class="disabled"><a href="." aria-label="Next"><span aria-hidden="true">Next</span></a></li>'
    return page_div


# def paginationdiv(data, cpage):
#     page_div = ''
#     fields = data['next'].split('?')[1].split('&')[0] if (data['next'] and '&' in data['next']) else ''
#     if data['previous']:
#         page_div = '<li><a href="'+data['previous']+'" aria-label="Previous"><span aria-hidden="true">Previous</span></a></li>'
#     else:
#         page_div = '<li class="disabled"><a href="." aria-label="Previous"><span aria-hidden="true">Previous</span></a></li>'
#     for i in range(1,int(math.ceil(data['count']/20.0))+1):
#         if i == 1 and cpage == i:
#             page_div += '<li><a href=".">'+str(i)+'</a></li>'
#         elif cpage == i:
#             page_div += '<li class="active"><a href="?page='+str(i)+'&'+fields+'">'+str(i)+'</a></li>'
#         else:
#             page_div += '<li><a href="?page='+str(i)+'&'+fields+'">'+str(i)+'</a></li>'
#     if data['next']:
#         page_div += '<li><a href="'+data['next']+'" aria-label="Next"><span aria-hidden="true">Next</span></a></li>'
#     else:
#         page_div += '<li class="disabled"><a href="." aria-label="Next"><span aria-hidden="true">Next</span></a></li>'
#     return page_div

class NonDestructiveModelViewSet(viewsets.ModelViewSet):

    def destroy(self, request, *args, **kwargs):
        raise exceptions.MethodNotAllowed("Delete")


def affiliates_filter_list(user, sub_aff_list):
    header_list = [{'title': 'Affiliate Id'}, {'title': 'Username'}, 
                    {'title':'First Name'},{'title':'Last Name'}, {'title':'Company Name'}, {'title':'Super Affiliate'},
                    {'title':'Payment Method'}, {'title':'Status'},
                     {'title': 'View/ Edit'}]
    data_list = []
    for i in sub_aff_list:
        val_list = []
        ac_m = ''
        s_aff= ''
        camp_d = '<a href="/admin/campaign?aff_id='+str(i.id)+'"><button class="btn btn-success">Campaign</button></a>'
        # aff_c_obj = Affiliatecommission.objects.filter(fk_affiliateinfo=int(i.id))
        # if aff_c_obj:
        #     comm_page = '<a href="/admin/affiliatecommission?aff_id='+str(i.id)+'"><button class="btn btn-success">Commission</button></a>'
        # else:
        #     comm_page = '<a href="/admin/affiliatecommission/c'+str(i.id)+'"><button class="btn btn-success">Commission</button></a>'
        # added logic for payment type
        pay_m = AccountDetails.objects.filter(fk_affiliateinfo=i.id)
        if pay_m:
            payment_type = str(pay_m[0].accounttype)
            if payment_type == 'None':
                if pay_m[0].accountnumber and pay_m[0].accountnumber!=' ':
                    payment_type = 'Bank Wire Transfer'
                elif pay_m[0].paypal_email and pay_m[0].paypal_email!=' ':
                    payment_type = 'Paypal'
                elif pay_m[0].neteller_accountnumber and pay_m[0].neteller_accountnumber!=' ':
                    payment_type = "Neteller"
                else:
                    payment_type = 'No Details Given'
        else:
            payment_type = "No Details Given"
        report_ele = '<a href="/admin/affiliate-activity/?aff_id='+str(i.id)+'"><button class="btn btn-success">Report</button></a>'
        edit_ele = '<a href="/admin/add-affiliate/'+str(i.id)+'"><button class="btn btn-success">View/ Edit</button></a>'
        '<form class="button-form" action="/admin/add-affiliate/'+str(i.id)+'" enctype="multipart/form-data" data-method="DELETE"><button class="btn btn-danger">Delete</button></form>'
        del_ele = '<form class="button-form" action="/admin/add-affiliate/'+str(i.id)+'" data-method="DELETE"><button class="btn btn-danger">Delete</button></form>' 
        save_status = '<button class="btn btn-purple" onclick="savestatus(this)" hrefs="/admin/savestatus/?aff_id='+str(i.id)+'">Save</button>'
        select_status = '<select class="selectpicker" name="status" >'
        val_list = [str(i.id), str(i.email),
                    str(i.contactname) if i.contactname else '-', 
                    str(i.lastname) if i.lastname else '-', 
                    str(i.companyname)if i.companyname else '-',
                    str(i.superaffiliate.username) if i.superaffiliate else '-', 
                    payment_type, str(i.status), edit_ele]
        data_list.append(val_list)
    return header_list, data_list


class AddUserMapping(viewsets.ModelViewSet):
    """
    Endpoint: /admin/affiliate/

    GET: Typical RESTful Response including Pagination.

    `search` query parameter => ?search=<search text>

    PUT & PATCH are available though no validations
                beyond data types exist on it for now.

    """
    queryset = PartnerUser.objects.all()
    serializer_class = PartnerUserSerializer
    filter_backends = (filters.SearchFilter,
                       DjangoFilterBackend, filters.OrderingFilter,)
    parser_classes = (MultiPartParser, FormParser,)
    renderer_classes = (renderers.JSONRenderer, renderers.TemplateHTMLRenderer)

    @user_class_role_management('Users')
    def list(self, request, *args, **kwargs):
        acc_manager, partuser_obj, user = get_u_details(request)
        queryset = self.filter_queryset(self.get_queryset())
        queryset = queryset.filter(programme=acc_manager.affiliateprogramme,
                                    accmanager=acc_manager)
        page = True
        if page is not None:
            serializer = self.get_serializer(queryset, many=True)
            if request.accepted_renderer.format == 'html':
                cpage = int(request.GET.get('page')) if request.GET.get('page') else 1
                header_list = [ {'title': 'Email'},{'title': 'First Name'},{'title': 'Last Name'},
                         {'title': 'Role'}, {'title': 'Joined On'},
                         {'title': 'View/ Edit'}]
                
                page_div = ''
                data_list = []
                accm_user = User.objects.get(id=acc_manager.user_id)
                data_list.append([str(accm_user.email),
                                    str(accm_user.first_name) if accm_user.first_name else '-',
                                    str(accm_user.last_name) if accm_user.last_name else '-',
                                    'Account Manager', str(accm_user.date_joined), '<a href="/admin/affiliate-manager/put" class="btn btn-success">View/ Edit</a>'])
                for i in serializer.data:
                    ac_m = AffiliateProgramme.objects.get(id=i['programme']).name
                    if i['user']:
                        user_obj = User.objects.get(id=i['user'])
                    edit_ele = '<a href="/admin/add-new-user/'+str(i['id'])+'"><button class="btn btn-success">View/ Edit</button></a>'
                    del_ele = '<form class="button-form" action="/admin/add-new-user/'+str(i['id'])+'" data-method="DELETE"><button class="btn btn-danger">Delete</button></form>' 
                    data_list.append([ str(user_obj.email),
                                        str(user_obj.first_name) if user_obj.first_name else '-',
                                        str(user_obj.last_name) if user_obj.last_name else '-', str(i['role']), str(user_obj.date_joined),
                                        edit_ele])
                return Response({'header_list': header_list, 'data_list': data_list, 
                                'pagination_div': page_div,
                                'partner': acc_manager.affiliateprogramme.name,
                                'partner_obj': acc_manager.affiliateprogramme,
                                'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
 
                           }, template_name='partner/adduser_list.html')
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        # if request.accepted_renderer.format == 'html':
        #     return Response({'data': serializer.data}, template_name='partner/index.html')
        return Response(serializer.data)

    @user_class_role_management('Users')
    def create(self, request, *args, **kwargs):
        pid = request.session.get('partnerid')
        data = {}
        request.data._mutable = True
        user_obj = User.objects.filter(username=request.data['email'],
                                 email=request.data['email'])
        passwd = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(6))
        if not user_obj:
            user_obj = User.objects.create_user(username=request.data['email'],
                                     email=request.data['email'],
                                     password=passwd)
            user_obj.save()
        else:
            acc_manager, partuser_obj, user = get_u_details(request)
            data['partner'] = acc_manager.affiliateprogramme.name
            data['partner_obj'] = acc_manager.affiliateprogramme
            data['menu_items'] = {'Dashboard': [], 'Affiliates': ['Manage Affiliates', 
                    'Referral Affiliates', 'Add Affiliate','Pending Applications','Search Affiliate'],
                    'Commissions': ['Default', 'Brand'],
                    'Marketing':['Banners', 'Landing Pages','Videos', 'Campaign',
                    'Page Peel', 'Screenshot', 'Text Links'],
                    'Deductions': [],
                    'Reports': ['Earnings Report', 'Conversions Report',
                    'Campaigns report', 'Dynamic report', 'Active Depositors',
                    'Affiliate Activity', 'Brand Report', 'Device Report',
                    'Sub Affiliate Report'],
                    'Users': ['Affiliate Managers','Add User'],
                    'Settings': ['Account Details','Change Password',
                    'Software Provider Authentication','Brand Mapping','Proprietary']}
            data['pid'] = request.GET.get('pid', '')
            data['call'] = 'POST'
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'Error': 'Email address already exist.',
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/adduser.html')
        user = User.objects.get(id=pid)
        acc_manager_obj = AccountManager.objects.filter(user=user)
        partuser_obj = []
        if acc_manager_obj:
            acc_manager = acc_manager_obj[0]
        else:
            partuser_obj = PartnerUser.objects.filter(user=user)
            acc_manager = partuser_obj[0].accmanager
        request.data['programme'] = acc_manager.affiliateprogramme.id
        request.data['accmanager'] = acc_manager.id
        request.data['user'] = user_obj.id
        request.data['options'] = dict(request.data)['choice']
        # if request.data.get('mobilerequired') == 'on':
        # request.data['mobilerequired'] = True
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        request.session['created'] = True
        id_ = serializer.data['id']
        addusermail(user_obj, acc_manager_obj[0], 'ADDUSER', passwd)
        return HttpResponseRedirect("/admin/add-new-user/"+str(id_))

    def perform_create(self, serializer):
        serializer.save()

    @user_class_role_management('Users')
    def retrieve(self, request, pk=None):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        try:
            instance = self.get_object()
            # queryset = get_object_or_404(queryset, pk=pk)
            # queryset = self.filter_queryset(self.get_queryset())
            serializer = self.get_serializer(instance)
        except:
            serializer = self.get_serializer(data={})
            serializer.is_valid()
        # queryset = get_object_or_404(queryset, pk=pk)
        # queryset = self.filter_queryset(self.get_queryset())
        # serializer = self.get_serializer(instance)
        if request.accepted_renderer.format == 'html':
            try:
                data = serializer.data
            except:
                data = {}
            if data.get('options'):
                data['options']=[str(i) for i in eval(data['options'])]
            else:
                data['options'] = []
            acc_manager, partuser_obj, user = get_u_details(request)
            msg = ''
            if request.session.get('created'):
                crt = request.session.get('created')
                if crt:
                    msg = 'User added successfully.'
                else:
                    msg = 'There is an error while adding User.'
                del request.session['created']
            data['message'] = msg
            data['partner'] = acc_manager.affiliateprogramme.name
            data['partner_obj'] = acc_manager.affiliateprogramme
            data['menu_items'] = {'Dashboard': [], 'Affiliates': ['Manage Affiliates', 
                    'Referral Affiliates', 'Add Affiliate','Pending Applications','Search Affiliate'],
                    'Commissions': ['Default','Brand', 'Country'],
                    'Marketing':['Banners', 'Landing Pages','Videos', 'Campaign',
                    'Page Peel', 'Screenshot', 'Text Links'],
                    'Deductions': [],
                    'Reports': ['Earnings Report', 'Conversions Report',
                    'Campaigns report', 'Dynamic report', 'Active Depositors',
                    'Affiliate Activity', 'Brand Report', 'Device Report',
                    'Sub Affiliate Report'],
                    'Users': ['Affiliate Managers','Add User'],
                    'Settings': ['Account Details','Change Password',
                    'Software Provider Authentication','Brand Mapping','Proprietary']}
            data['call'] = 'POST' if self.kwargs.get('pk') == 'post' else 'PUT'
            data['pid'] = request.GET.get('pid', '')
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/adduser.html')
        return Response(serializer.data)

    @user_class_role_management('Users')
    def update(self, request, *args, **kwargs):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        request.data._mutable = True
        request.data['options'] = dict(request.data)['choice']
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        # dict_data = serializer.data
        # dict_data['siteid'] = request.data['siteid'][0]
        # serializer.data = dict_data
        self.perform_update(serializer)
        if request.accepted_renderer.format == 'html':
            data = serializer.data
            if data.get('options'):
                data['options']=[str(i) for i in eval(data['options'])]
            acc_manager, partuser_obj, user = get_u_details(request)
            data['message'] = 'Updated User Role details.'
            data['partner'] = acc_manager.affiliateprogramme.name
            data['partner_obj'] = acc_manager.affiliateprogramme
            # data['user'] = PartnerUser.objects.get(id=data['id']).user.email
            data['menu_items'] = {'Dashboard': [], 'Affiliates': ['Manage Affiliates', 
                    'Referral Affiliates', 'Add Affiliate','Pending Applications','Search Affiliate'],
                    'Commissions': ['Default','Brand', 'Country'],
                    'Marketing':['Banners', 'Landing Pages','Videos', 'Campaign',
                    'Page Peel', 'Screenshot', 'Text Links'],
                    'Deductions': [],
                    'Reports': ['Earnings Report', 'Conversions Report',
                    'Campaigns report', 'Dynamic report', 'Active Depositors',
                    'Affiliate Activity', 'Brand Report', 'Device Report',
                    'Sub Affiliate Report'],
                    'Users': ['Affiliate Managers','Add User'],
                    'Settings': ['Account Details','Change Password',
                    'Software Provider Authentication','Brand Mapping','Proprietary']}
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/adduser.html')
        return Response(serializer.data)

class SoftwareProvidersMapping(viewsets.ModelViewSet):
    """
    Endpoint: /admin/affiliate/

    GET: Typical RESTful Response including Pagination.

    `search` query parameter => ?search=<search text>

    PUT & PATCH are available though no validations
                beyond data types exist on it for now.

    """
    queryset = NetworkAffiliateProgrammeMapping.objects.all()
    serializer_class = NetworkAffiliateProgrammeMappingSerializer
    filter_backends = (filters.SearchFilter,
                       DjangoFilterBackend, filters.OrderingFilter,)
    parser_classes = (MultiPartParser, FormParser,)
    renderer_classes = (renderers.JSONRenderer, renderers.TemplateHTMLRenderer)

    @user_class_role_management('Software Provider Authentication')
    def list(self, request, *args, **kwargs):
        acc_manager, partuser_obj, user = get_u_details(request)
        queryset = self.filter_queryset(self.get_queryset())
        queryset = queryset.filter(programme=acc_manager.affiliateprogramme,
                                    accmanager=acc_manager,siteid__siteorder__accountmanager_id=acc_manager)
        queryset = queryset.order_by("siteid__siteorder__order_id")
        page = True
        if page is not None:
            serializer = self.get_serializer(queryset, many=True)
            if request.accepted_renderer.format == 'html':
                cpage = int(request.GET.get('page')) if request.GET.get('page') else 1
                header_list = [{'title': 'Network Id'}, {'title': 'Software Provider'},
                        {'title': 'Affiliate Program'}, {'title': 'Brand'},
                        {'title': 'Data Retrieval'},
                         {'title': 'View/ Edit'}]
                
                page_div = ''
                data_list = []
                for i in serializer.data:
                    active = False
                    retrive = '<span class="red-dot"></span>'
                    if i['retrive'] == True:
                        active = True
                        retrive = '<span class="green-dot"></span>'
                    network_obj = ''
                    site_obj = ''
                    brand = ''
                    ac_m = AffiliateProgramme.objects.get(id=i['programme']).name
                    if i.get('networkid'):
                        network_obj = Network.objects.get(networkid=i['networkid']).networkname
                    if i.get('siteid'):
                        brand = Site.objects.get(id=i['siteid']).name
                    edit_ele = '<a href="/admin/software-provider-authentication/'+str(i['id'])+'"><button class="btn btn-success">View/ Edit</button></a>'
                    del_ele = '<form class="button-form" action="/admin/software-provider-authentication/'+str(i['id'])+'" data-method="DELETE"><button class="btn btn-danger">Delete</button></form>' 
                    data_list.append([str(i['networkid']), str(network_obj), str(ac_m), str(brand), str(active),
                                     edit_ele])
                return Response({'header_list': header_list, 'data_list': data_list, 
                                'pagination_div': page_div,
                                'partner': acc_manager.affiliateprogramme.name,
                                'partner_obj': acc_manager.affiliateprogramme,
                                'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
 
                           }, template_name='partner/softwarepromapping_list.html')
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        # if request.accepted_renderer.format == 'html':
        #     return Response({'data': serializer.data}, template_name='partner/index.html')
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        pid = request.session.get('partnerid')
        if request.data['siteid']:
            net_aff_map = NetworkAffiliateProgrammeMapping.objects.filter(
                siteid=int(request.data['siteid']))
            if len(net_aff_map) > 0:
                request.session['affiliate_exist'] = str(net_aff_map[0].id)
                return HttpResponseRedirect('/admin/software-provider-authentication/'+str(net_aff_map[0].id))
        request.data._mutable = True
        if request.data.get('retrive') == 'true':
            request.data['retrive'] = True
        else:
            request.data['retrive'] = False
        user = User.objects.get(id=pid)
        acc_manager_obj = AccountManager.objects.filter(user=user)
        partuser_obj = []
        if acc_manager_obj:
            acc_manager = acc_manager_obj[0]
        else:
            partuser_obj = PartnerUser.objects.filter(user=user)
            acc_manager = partuser_obj[0].accmanager
        request.data['programme'] = acc_manager.affiliateprogramme.id
        request.data['accmanager'] = acc_manager.id
        # if request.data.get('mobilerequired') == 'on':
        # request.data['mobilerequired'] = True
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        networkobj = Network.objects.filter(
                networkid=int(request.data['networkid']))
        if networkobj[0].networkname == 'ProgressPlay':
            resp = on_demand_pp(request, request.data['siteid'])
            return HttpResponseRedirect('/admin/dashboard/')
        request.session['created'] = True
        id_ = serializer.data['id']
        return HttpResponseRedirect("/admin/software-provider-authentication/"+str(id_))

    def perform_create(self, serializer):
        serializer.save()

    @user_class_role_management('Software Provider Authentication')
    def retrieve(self, request, pk=None):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        try:
            instance = self.get_object()
            # queryset = get_object_or_404(queryset, pk=pk)
            # queryset = self.filter_queryset(self.get_queryset())
            serializer = self.get_serializer(instance)
        except:
            serializer = self.get_serializer(data={})
            serializer.is_valid()
        # queryset = get_object_or_404(queryset, pk=pk)
        # queryset = self.filter_queryset(self.get_queryset())
        # serializer = self.get_serializer(instance)
        if request.accepted_renderer.format == 'html':
            try:
                data = serializer.data
            except:
                data = {}
            msg = ''
            if request.session.get('created'):
                crt = request.session.get('created')
                if crt:
                    msg = 'Software Authentication details added successfully.'
                else:
                    msg = 'There is an error while adding Software Authentication details.'
                del request.session['created']
            if request.session.get('networkid'):
                data['networkid'] = request.session.get('networkid')
                data['siteid'] = request.session.get('siteid')
                del request.session['networkid']
                del request.session['siteid']
            if request.session.get('affiliate_exist'):
                msg = 'Credentials already set for the Brand.'
                del request.session['affiliate_exist']
            data['message'] = msg
            data['providers'] = [[i.networkid, i.networkname] for i in Network.objects.exclude(connect_type="UPLOAD")]
            net_obj = Network.objects.filter(connect_type="UPLOAD")
            if net_obj:
                net_obj = net_obj[0]
                data['providers'].append([net_obj.networkid,net_obj.networkname])
            acc_manager, partuser_obj, user = get_u_details(request)
            data['partner'] = acc_manager.affiliateprogramme.name
            data['partner_obj'] = acc_manager.affiliateprogramme
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            data['call'] = 'POST' if self.kwargs.get('pk') == 'post' else 'PUT'
            data['pid'] = request.GET.get('pid', '')
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/softwareproviderack.html')
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        request.data._mutable = True
        if request.data.get('retrive') == 'true':
            request.data['retrive'] = True
        else:
            request.data['retrive'] = False
        user = User.objects.get(id=pid)
        acc_manager_obj = AccountManager.objects.filter(user=user)
        partuser_obj = []
        if acc_manager_obj:
            acc_manager = acc_manager_obj[0]
        else:
            partuser_obj = PartnerUser.objects.filter(user=user)
            acc_manager = partuser_obj[0].accmanager
        request.data['programme'] = acc_manager.affiliateprogramme.id
        request.data['accmanager'] = acc_manager.id
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        # dict_data = serializer.data
        # dict_data['siteid'] = request.data['siteid'][0]
        # serializer.data = dict_data
        self.perform_update(serializer)
        if request.accepted_renderer.format == 'html':
            data = serializer.data
            data['providers'] = [[i.networkid, i.networkname] for i in Network.objects.exclude(connect_type="UPLOAD")]
            net_obj = Network.objects.filter(connect_type="UPLOAD")
            if net_obj:
                net_obj = net_obj[0]
                data['providers'].append([net_obj.networkid,net_obj.networkname])
            acc_manager, partuser_obj, user = get_u_details(request)
            data['message'] = 'Updated Software authentication details.'
            data['partner'] = acc_manager.affiliateprogramme.name
            data['partner_obj'] = acc_manager.affiliateprogramme
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/softwareproviderack.html')
        return Response(serializer.data)


class SiteSoftwareMapping(viewsets.ModelViewSet):
    """
    Endpoint: /admin/affiliate/

    GET: Typical RESTful Response including Pagination.

    `search` query parameter => ?search=<search text>

    PUT & PATCH are available though no validations
                beyond data types exist on it for now.

    """
    queryset = SiteWhitelabelMapping.objects.all()
    serializer_class = SiteWhitelabelMappingSerializer
    filter_backends = (filters.SearchFilter,
                       DjangoFilterBackend, filters.OrderingFilter,)
    parser_classes = (MultiPartParser, FormParser,)
    renderer_classes = (renderers.JSONRenderer, renderers.TemplateHTMLRenderer)

    @user_class_role_management('Brand Mapping')
    def list(self, request, *args, **kwargs):
        acc_manager, partuser_obj, user = get_u_details(request)
        queryset = self.filter_queryset(self.get_queryset())
        queryset = queryset.filter(accmanager=acc_manager,siteid__siteorder__accountmanager_id=acc_manager)
        request.session['acc_manager'] = acc_manager
        page = True
        if page is not None:
            serializer = self.get_serializer(queryset, many=True)
            if request.accepted_renderer.format == 'html':
                cpage = int(request.GET.get('page')) if request.GET.get('page') else 1
                # adding order and DT_RowId in header list
                header_list = [{'title': 'Order'},{'title': 'DT_RowId'},
                    {'title': 'Network Id'}, {'title': 'Software Provider'}, {'title': 'Brand'}, 
                        {'title': 'Affiliate Program'}, {'title': 'White Label ID'},
                        {'title': 'Tracker Range'}, {'title': 'Redirect URL'},
                         {'title': 'View/ Edit'}]
                
                page_div = ''
                data_list = []
                # modifying the query set
                queryset = queryset.select_related("programme", "networkid").\
                                                order_by('siteid__siteorder__order_id')
                for i in queryset:
                    network_obj = ''
                    site_obj = ''
                    ac_m = ''
                    if i.programme:
                        ac_m = i.programme.name 
                    if i.networkid:
                        network_obj = i.networkid.networkname
                    if i.siteid:
                        site_obj = i.siteid.name
                    edit_ele = '<a href="/admin/brand-mapping/'+str(i.id)+'"><button class="btn btn-success">View/ Edit</button></a>'
                    del_ele = '<form class="button-form" action="/admin/brand-mapping/'+str(i.id)+'" data-method="DELETE"><button class="btn btn-danger">Delete</button></form>' 
                    siteorder = SiteOrder.objects.filter(accountmanager_id=acc_manager,site_id=i.siteid)[0]
                    DT_RowId = int(siteorder.id)
                    order = int(siteorder.order_id)
                    data_list.append([order,DT_RowId,str(i.networkid_id), str(network_obj), str(site_obj),
                                     str(ac_m), str(i.whitelabelid), str(i.start_limit)+' - ' +str(i.end_limit),
                                     str(i.sp_track_url),
                                     edit_ele])
                return Response({'header_list': header_list, 'data_list': data_list, 
                                'pagination_div': page_div,
                                'partner': acc_manager.affiliateprogramme.name,
                                'partner_obj': acc_manager.affiliateprogramme,
                                'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
 
                           }, template_name='partner/sitesoftwaremapping_list.html')
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        # if request.accepted_renderer.format == 'html':
        #     return Response({'data': serializer.data}, template_name='partner/index.html')
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        try:
            pid = request.session.get('partnerid')
            request.data._mutable = True
            user = User.objects.get(id=pid)
            acc_manager_obj = AccountManager.objects.filter(user=user)
            partuser_obj = []
            if acc_manager_obj:
                acc_manager = acc_manager_obj[0]
            else:
                partuser_obj = PartnerUser.objects.filter(user=user)
                acc_manager = partuser_obj[0].accmanager
            site_logo = request.FILES.get('image')
            site_name = request.POST.get('site_name')
            currency = request.POST.get('currency')
            site_domain = request.POST.get('site_domain')
            networkid = request.POST.get('networkid')
            #print 'hi request', request.POST
            network_obj = Network.objects.get(networkid=networkid)
            site_obj, created = Site.objects.get_or_create(domain=site_domain,
                                name=site_name)
            site_obj.save()
            siteorder = SiteOrder.objects.filter(accountmanager_id=acc_manager).order_by("-order_id")
            order_id = 1
            if siteorder:
                order_id = int(siteorder[0].order_id)+1
            siteorderobj, created = SiteOrder.objects.get_or_create(accountmanager_id=acc_manager,site_id=site_obj)
            if created:
                siteorderobj.order_id= order_id
                siteorderobj.save()
            if network_obj.networkname.strip().lower() in ['jumpman', 'nektan']:
                user_group = False
                if 'jumpman' in network_obj.networkname.strip().lower():
                    location = "/sftp/shared/"+site_name.replace(' ', '').lower()
                    user_group = 'jumpmansftp'
                elif 'nektan' in network_obj.networkname.strip().lower():
                    location = "/sftp_necktan/shared/"+site_name.replace(' ', '').lower()
                    user_group = 'nektan_sftp'
                if user_group:
                    subprocess.call('sudo /var/www/wynta/ftpfoldercreate.sh '+str(location)+' '+ str(user_group), 
                                        shell=True)
                    if 'jumpman' in network_obj.networkname.strip().lower():
                        send_ap_jumpman_mail(acc_manager,
                            'Jumpman_Brand_Setup',
                            network_obj.networkname, site_obj.name,
                             location, True)
                        send_ap_jumpman_mail(acc_manager,
                            'Jumpman_Client_Email',
                            network_obj.networkname, site_obj.name,
                             location)
            network_site_map, created = NetworkAndSiteMap.objects.get_or_create(networkid=network_obj,
                                siteid=site_obj,
                                logo=site_logo)
            network_site_map.save()
            # network_aff_obj = NetworkAffiliateProgrammeMapping.objects.get_or_create(
            #                     accmanager=acc_manager,
            #                     programme=acc_manager.affiliateprogramme)
            # if network_aff_obj:
            #     network_aff_obj[0].siteid.add(site_obj)
            #     network_aff_obj[0].save()
            request.data['programme'] = acc_manager.affiliateprogramme.id
            request.data['accmanager'] = acc_manager.id
            request.data['siteid'] = site_obj.id
            currency_obj, created = Currency.objects.get_or_create(siteid=site_obj,
                                    currency=currency)
            if created:
                currency_obj.save()
            if site_obj not in acc_manager.siteid.all():
                acc_manager.siteid.add(site_obj)
                acc_manager.save()
            # when a brand is created setting it to last order
            #order_id = int(SiteOrder.objects.filter(accountmanager_id=acc_manager).order_by("-order_id")[0].order_id)+1
            #siteorder = SiteOrder.objects.create(accountmanager_id=acc_manager,site_id=site_obj,order_id=order_id)
            #siteorder.save()
            aff_objs = Affiliateinfo.objects.filter(
                account_manager=acc_manager
                )
            for aff_obj in aff_objs:
                aff_obj.siteid.add(site_obj)
                aff_obj.save()
            print 'hi this is done 1'
            camp_obj, created = Campaign.objects.get_or_create(
                    name=site_name, siteid=site_obj,
                    account_manager = acc_manager,
                    networkid=network_obj, media_type='T')
            camp_obj.save()
            print 'Hi this is done 2'
            # if request.data.get('mobilerequired') == 'on':
            # request.data['mobilerequired'] = True
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            request.session['networkid'] = network_obj.networkid
            request.session['siteid'] = site_obj.id
            id_ = serializer.data['id']
            print 'hi this is done 3'
            if 'pid=new' in request.META['HTTP_REFERER']:
                if network_obj.connect_type in ['UPLOAD', 'FTP']:
                    return HttpResponseRedirect('/admin/dashboard')
                return HttpResponseRedirect('/admin/software-provider-authentication/post?pid=new')
            elif network_obj.connect_type not in ['UPLOAD', 'FTP']:
                return HttpResponseRedirect('/admin/software-provider-authentication/post')
            request.session['created'] = True
            return HttpResponseRedirect("/admin/brand-mapping/"+str(id_))
            print 'hi this is done 4'
        except Exception as e:
            print 'sitemap', str(e)
            data = {}
            data['providers'] = [[i.networkid, i.networkname] for i in Network.objects.exclude(connect_type="UPLOAD")]
            net_obj = Network.objects.filter(connect_type="UPLOAD")
            if net_obj:
                net_obj = net_obj[0]
                data['providers'].append([net_obj.networkid,net_obj.networkname])
            acc_manager, partuser_obj, user = get_u_details(request)
            data['partner'] = acc_manager.affiliateprogramme.name
            data['partner_obj'] = acc_manager.affiliateprogramme
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'Error':'validation Error.',
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/sitesoftwaremapping.html')


    def perform_create(self, serializer):
        serializer.save()

    @user_class_role_management('Brand Mapping')
    def retrieve(self, request, pk=None):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        try:
            instance = self.get_object()
            # queryset = get_object_or_404(queryset, pk=pk)
            # queryset = self.filter_queryset(self.get_queryset())
            serializer = self.get_serializer(instance)
        except:
            serializer = self.get_serializer(data={})
            serializer.is_valid()
        # queryset = get_object_or_404(queryset, pk=pk)
        # queryset = self.filter_queryset(self.get_queryset())
        # serializer = self.get_serializer(instance)
        if request.accepted_renderer.format == 'html':
            try:
                data = serializer.data
            except:
                data = {}
            if data:
                net_obj = NetworkAndSiteMap.objects.filter(siteid=data['siteid'])
                if net_obj:
                    data['logo'] = net_obj[0].logo.url if net_obj[0].logo else ''
            msg = ""
            if request.session.get('created'):
                crt = request.session.get('created')
                if crt:
                    msg = 'Brand details added successfully.'
                else:
                    msg = 'There is an error while adding Brand details.'
                del request.session['created']
            if not msg:
                msg = request.session.get('success_msg', '')
                if msg:
                    del request.session['success_msg']
                    data['domainsuccess'] = 'Success'
            data['message'] = msg
            data['providers'] = [[i.networkid, i.networkname] for i in Network.objects.exclude(connect_type="UPLOAD")]
            net_obj = Network.objects.filter(connect_type="UPLOAD")
            if net_obj:
                net_obj = net_obj[0]
                data['providers'].append([net_obj.networkid,net_obj.networkname])
            acc_manager, partuser_obj, user = get_u_details(request)
            if data.get('siteid'):
                currency_obj = Currency.objects.filter(siteid__id=data['siteid'])
                if currency_obj:
                    data['currency'] = currency_obj[0].currency
            data['partner'] = acc_manager.affiliateprogramme.name
            data['partner_obj'] = acc_manager.affiliateprogramme
            data['currency_list'] = [[j['id__max'], j['currency']] for j in Currency.objects.values('currency').annotate(Max('id')).distinct()]
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            data['call'] = 'POST' if self.kwargs.get('pk') == 'post' else 'PUT'
            data['pid'] = request.GET.get('pid', '')
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/sitesoftwaremapping.html')
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        request.data._mutable = True
        user = User.objects.get(id=pid)
        acc_manager_obj = AccountManager.objects.filter(user=user)
        partuser_obj = []
        currency = request.data['currency']
        site_logo = request.FILES.get('image')
        network_obj = Network.objects.get(networkid=request.data['networkid'])
        site_obj = Site.objects.get(id=request.data['siteid'])
        network_site_map = NetworkAndSiteMap.objects.get(networkid=network_obj,
                                siteid=site_obj)
        if site_logo:
            network_site_map.logo=site_logo
        network_site_map.save()
        if acc_manager_obj:
            acc_manager = acc_manager_obj[0]
        else:
            partuser_obj = PartnerUser.objects.filter(user=user)
            acc_manager = partuser_obj[0].accmanager
        request.data['programme'] = acc_manager.affiliateprogramme.id
        request.data['accmanager'] = acc_manager.id
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        currency_obj, created = Currency.objects.get_or_create(siteid=site_obj)
        currency_obj.currency=currency
        currency_obj.save()
        # dict_data = serializer.data
        # dict_data['siteid'] = request.data['siteid'][0]
        # serializer.data = dict_data
        self.perform_update(serializer)
        if request.accepted_renderer.format == 'html':
            data = serializer.data
            net_obj = NetworkAndSiteMap.objects.filter(siteid=data['siteid'])
            if net_obj:
                data['logo'] = net_obj[0].logo.url if net_obj[0].logo else ''
            data['providers'] = [[i.networkid, i.networkname] for i in Network.objects.exclude(connect_type="UPLOAD")]
            net_obj = Network.objects.filter(connect_type="UPLOAD")
            if net_obj:
                net_obj = net_obj[0]
                data['providers'].append([net_obj.networkid,net_obj.networkname])
            if data.get('siteid'):
                currency_obj = Currency.objects.filter(siteid__id=data['siteid'])
                if currency_obj:
                    data['currency'] = currency_obj[0].currency
            acc_manager, partuser_obj, user = get_u_details(request)
            data['message'] = 'Brand details updated.'
            data['partner'] = acc_manager.affiliateprogramme.name
            data['currency_list'] = [[j['id__max'], j['currency']] for j in Currency.objects.values('currency').annotate(Max('id')).distinct()]
            data['partner_obj'] = acc_manager.affiliateprogramme
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/sitesoftwaremapping.html')
        return Response(serializer.data)

def brand_order(request, *args, **kwargs):
    """
    Function receives the updated rows and commits the changes
    into the SiteOrder table
    """
    try:
        if request.method=="POST":
            modifiedRows = json.loads(request.body)
            for modifiedRow in modifiedRows:
                SiteOrder.objects.filter(id=modifiedRow['id']).update(order_id=modifiedRow['order'])
    except Exception as e:        
        return HttpResponse(json.dumps({'result': e}))
    return HttpResponse(json.dumps({'result':'success'}))

def get_softwareproviders(request):
    context_dict = {}
    pid = request.session.get('partnerid')
    if not pid:
        return HttpResponseRedirect(reverse('partner_login'),)
    user = User.objects.get(id=pid)
    link = request.GET.get('link')
    partuser_obj = []
    acc_manager_obj = AccountManager.objects.filter(user=user)
    if acc_manager_obj:
        acc_manager = acc_manager_obj[0]
    else:
        partuser_obj = PartnerUser.objects.filter(user=user)
        acc_manager = partuser_obj[0].accmanager
    request.user.username = user.username
    request.user.email = user.email
    context_dict['partner'] = acc_manager.affiliateprogramme.name
    context_dict['partner_obj'] = acc_manager.affiliateprogramme
    context_dict['part_user'] = ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
    data['providers'] = [[i.networkid, i.networkname] for i in Network.objects.exclude(connect_type="UPLOAD")]
    net_obj = Network.objects.filter(connect_type="UPLOAD")
    if net_obj:
        net_obj = net_obj[0]
        data['providers'].append([net_obj.networkid,net_obj.networkname])
    if not request.method == "GET":
        username = request.POST.get('username')
        password = request.POST.get('password')
        network = request.POST.get('network')
        retrive = request.POST.get('retrive')
        network_obj = Network.objects.get(networkid=network)
        net_aff_map = NetworkAffiliateProgrammeMapping.objects.get_or_create(networkid=network_obj,
                                    programme=acc_manager.affiliateprogramme)
        net_aff_map.username = username
        net_aff_map.password = password
        net_aff_map.retrive = retrive
        acc_manager.save()
    return render(request, 'partner/softwareproviderack.html', context_dict)

@user_role_management('Search Affiliate')
def get_affiliates_filter(request, **kwargs):
    context_dict = {}
    filt_er = request.GET.get('filter')
    pid = request.session.get('partnerid')
    if not pid:
        return HttpResponseRedirect(reverse('partner_login'),)
    user = User.objects.get(id=pid)
    acc_manager, partuser_obj, user = get_u_details(request)
    number_of_items = 10000
    nexting_page = 0
    previous_page = 0
    starting_page = 0
    page = 0
    search_type = None
    domain = None
    if request.method == "GET":
        custom = "Custom"
        starting_page = request.GET.get('start_page')
        nexting_page = request.GET.get('next')
        previous_page = request.GET.get('prev')
        page = request.GET.get('page')
    else:
        search_type = str(request.POST.get("searchtype"))
        search_name = str(request.POST.get("searchname"))
        searchsoft = str(request.POST.get("searchsoft"))
        domain = request.POST.get("sitename")
    sub_aff_list = Affiliateinfo.objects.filter(account_manager=acc_manager)#.exclude(username=acc_manager.user.username)
    site_ids_list = get_brand_order(acc_manager)
    site_ids = site_ids_list
    site_name = 'All Websites'
    if domain and domain != 'All':
        site_domain_list = domain.split(',')
        site_ids = site_ids_list.filter(domain__in=site_domain_list)
        site_name = 'Multiple sites.'
        if len(domain.split(',')) <2:
            site_name = site_ids_list.get(domain=site_domain_list[0]).name
    if search_type == 'id':
        sub_aff_list = sub_aff_list.filter(id=int(search_name))
    elif search_type == 'name':
        sub_aff_list = sub_aff_list.filter(Q(contactname__icontains=search_name) | Q(lastname__icontains=search_name))
    elif search_type == 'email':
        sub_aff_list = sub_aff_list.filter(Q(email__icontains=search_name) | Q(username__icontains=search_name))
    elif search_type == 'country':
        country_str = Country.objects.filter(countryname__icontains=search_name)
        country_str = [str(i.id) for i in country_str]
        country_aff_list = sub_aff_list.filter(country__in=country_str)
        sub_aff_list = sub_aff_list.filter(country__icontains=search_name)
        sub_aff_list = sub_aff_list|country_aff_list
    elif search_type == 'brand':
        site_obj = Site.objects.filter(name__icontains=search_name)
        sub_aff_list = sub_aff_list.filter(siteid__in=site_obj)
    elif search_type == 'software':
        network_obj = Network.objects.filter(networkname__icontains=searchsoft)
        sub_aff_list = sub_aff_list.filter(networkid__in=network_obj)
    elif search_type == 'superaff':
        sup_obj = Affiliateinfo.objects.filter(username__icontains=search_name)
        sub_aff_list = sub_aff_list.filter(superaffiliate__in=sup_obj)
    count = sub_aff_list.count()
    start_record, end_record, start_page, \
            end_page, next_page, cur_page, page_count = get_paginate_dimensions(page, starting_page, 
                                                                                nexting_page, previous_page, 
                                                                                count, number_of_items)
    #from_date = date(2012, 01,01)
    #to_date = datetime.now()+timedelta(days=1)
    header_list, data_list = affiliates_filter_list(acc_manager, sub_aff_list)

    pagination_div  = makePaginationDiv(start_page, page_count, cur_page, next_page, count)
    
    context_dict = {"data_list":data_list,"header_list":header_list,
                        "pagination_div":pagination_div, 'domain': site_name}
    if request.POST.get("format") == "json":
        context_dict["status"] = "success"
        context_dict['data_list'] = data_list
        context_dict["header_list"] = header_list
        return HttpResponse(json.dumps(context_dict, indent=4))
    context_dict['network_list'] = [[i.networkid, i.networkname] for i in Network.objects.all()]

    context_dict['data_list'] = json.dumps(data_list)
    context_dict["header_list"] = json.dumps(header_list)
    context_dict['partner'] = acc_manager.affiliateprogramme.name
    context_dict['partner_obj'] = acc_manager.affiliateprogramme
    context_dict['part_user'] = ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
    # context_dict["channels_list"] = get_channels_list()
    # context_dict["channel"] = channel if channel else "All"
    return render(request, 'partner/searchafflist.html', context_dict)

# @staff_member_required
class AffiliateViewSet(viewsets.ModelViewSet):

    """
    Endpoint: /admin/affiliate/

    GET: Typical RESTful Response including Pagination.

    `search` query parameter => ?search=<search text>

    PUT & PATCH are available though no validations
                beyond data types exist on it for now.

    """
    queryset = Affiliateinfo.objects.all()
    serializer_class = AffiliateSerializer
    filter_backends = (filters.SearchFilter,
                       DjangoFilterBackend, filters.OrderingFilter,)
    parser_classes = (MultiPartParser, FormParser,)
    renderer_classes = (renderers.JSONRenderer, renderers.TemplateHTMLRenderer)
    pagination_class = None

    @user_class_role_management('Manage Affiliates')
    def list(self, request, *args, **kwargs):
        filt_er = request.GET.get('filter')
        acc_manager, partuser_obj, user = get_u_details(request)
        queryset = self.filter_queryset(self.get_queryset())
        if filt_er == 'pending':
            queryset = queryset.filter(status="Pending")
        if filt_er == 'referral':
            queryset = queryset.filter(superaffiliate__isnull=False)
        queryset = queryset.filter(account_manager=acc_manager).exclude(username=user.username)
        queryset = queryset.order_by('-registeredon')
        count_d = len(queryset)
        # page = True
        serializer = self.get_serializer(queryset, many=True)
        if request.accepted_renderer.format == 'html':
            cpage = int(request.GET.get('page')) if request.GET.get('page') else 1
            header_list = [{'title': 'Affiliate Id'}, {'title': 'Username'}, 
                        {'title': 'First Name'}, {'title': 'Last Name'}, 
                        {'title':'Company Name'},  {'title':'Super Affiliate'},
                        {'title':'Payment Method'},{'title':'Status'},
                        {'title': 'View/ Edit'}]
            # if filt_er == 'pending':
            #     # header_list.insert(7,{'title': 'Save'})
            #     del header_list[4:8]
            #     header_list.insert(4,{'title': 'Status'})
            # ,{'title': 'Delete'}]
            if filt_er == 'inactive':
                header_list.insert(2,{'title':'Activity'})
            
            page_div = ''
            data_list = []
            for i in serializer.data:
                val_list = []
                ac_m = ''
                s_aff= ''
                if i['account_manager']:
                    ac_m = AccountManager.objects.filter(id=i['account_manager'])
                    ac_m = ac_m[0].user.first_name if ac_m else ''
                if i['superaffiliate']:
                    s_aff = Affiliateinfo.objects.filter(id=i['superaffiliate'])
                    s_aff = s_aff[0].username
                comm_page = ''
                camp_d = '<a href="/admin/campaign?aff_id='+str(i['id'])+'"><button class="btn btn-success">Campaigns</button></a>'
                # aff_c_obj = Affiliatecommission.objects.filter(fk_affiliateinfo=int(i['id']))
                # if aff_c_obj:
                #     comm_page = '<a href="/admin/affiliatecommission?aff_id='+str(i['id'])+'"><button class="btn btn-success">Commission</button></a>'
                # else:
                #     comm_page = '<a href="/admin/affiliatecommission/c'+str(i['id'])+'"><button class="btn btn-success">Commission</button></a>'
                # added logic for payment type
                pay_m = AccountDetails.objects.filter(fk_affiliateinfo=i['id'])
                if pay_m:
                    payment_type = str(pay_m[0].accounttype)
                    if payment_type == 'None':
                        if pay_m[0].accountnumber and pay_m[0].accountnumber!=' ':
                            payment_type = 'Bank Wire Transfer'
                        elif pay_m[0].paypal_email and pay_m[0].paypal_email!=' ':
                            payment_type = 'Paypal'
                        elif pay_m[0].neteller_accountnumber and pay_m[0].neteller_accountnumber!=' ':
                            payment_type = "Neteller"
                        else:
                            payment_type = 'No Details Given'
                    else:
                        payment_type = 'No Details Given'
                else:
                    payment_type = "No Details Given"
                report_ele = '<a href="/admin/affiliate-activity/?aff_id='+str(i['id'])+'"><button class="btn btn-success">Report</button></a>'
                edit_ele = '<a href="/admin/add-affiliate/'+str(i['id'])+'"><button class="btn btn-success">View/ Edit</button></a>'
                '<form class="button-form" action="/admin/add-affiliate/'+str(i['id'])+'" enctype="multipart/form-data" data-method="DELETE"><button class="btn btn-danger">Delete</button></form>'
                del_ele = '<form class="button-form" action="/admin/add-affiliate/'+str(i['id'])+'" data-method="DELETE"><button class="btn btn-danger">Delete</button></form>' 
                save_status = '<button class="btn btn-purple" onclick="savestatus(this)" hrefs="/admin/savestatus/?aff_id='+str(i['id'])+'">Save</button>'
                select_status = '<select class="selectpicker" name="status" onchange="savestatus(this)" hrefs="/admin/savestatus/?aff_id='+str(i['id'])+'">'
                if str(i['status']) == 'Pending':
                    select_status += '<option value="Pending" selected>Pending</option>'
                else:
                    select_status += '<option value="Pending">Pending</option>'
                if str(i['status']) == 'Approved':
                    select_status += '<option value="Approved" selected>Approved</option>'
                else:
                    select_status += '<option value="Approved">Approved</option>'
                if str(i['status']) == 'Rejected':
                    select_status += '<option value="Rejected" selected>Rejected</option></select>'
                else:
                    select_status += '<option value="Rejected">Rejected</option></select>'
                val_list = [str(i['id']), str(i['email']), str(i['contactname']),str(i['lastname']),str(i['companyname']), 
                            str(s_aff) if s_aff else '-', payment_type, select_status,  
                            edit_ele]
                if filt_er == 'inactive':
                    # header_list.insert(2,{'title':'Activity'})
                    val_list.insert(2,str(i['active']))
                # if filt_er == 'pending':
                #     # val_list.insert(7,save_status)
                #     val_list[4] = select_status
                #     del val_list[5:8]
                data_list.append(val_list)
            msg = ''
            if request.session.get('deleted'):
                msg = 'The affiliate has been successfully deleted.'
                if request.session.get('deleted'):
                    del request.session['deleted']
            return Response({'header_list': header_list, 'count_d': count_d, 'data_list': data_list, 'pagination_div': page_div,
                        'aff_page': filt_er,
                        'message':msg,
                        'partner': acc_manager.affiliateprogramme.name,
                        'partner_obj': acc_manager.affiliateprogramme,
                        'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                        template_name='partner/affiliate_list.html')
        # return self.get_paginated_response(serializer.data)
        # serializer = self.get_serializer(queryset, many=True)
        if request.accepted_renderer.format == 'html':
            return Response({'data': serializer.data, 'count_d': count_d, 'page': filt_er}, template_name='partner/affiliate_list.html')
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        try:
            pid = request.session.get('partnerid')
            if not pid:
                return HttpResponseRedirect(reverse('partner_login'),)
            user = User.objects.get(id=pid)
            acc_manager = AccountManager.objects.filter(user=user)[0]
            request.data._mutable = True
            site_urls = request.data.getlist('site_url')
            if site_urls:
                del request.data['site_url']
            mailidcount = Affiliateinfo.objects.filter(
                email=request.data['email'], account_manager=acc_manager).count()
            if mailidcount > 0:
                # request.session['created'] = 'Exist'
                id_ = 'post'
                request.data["message"] = '<div class="alert alert-danger alert-danger-msg"><center> <span>Email address has already been registered with us.</span></center></div>'
                request.data["call"] = 'POST'
                request.data['country_list'] = Country.objects.all().values_list('id', 'countryname')
                request.data["site_urls_list"] = [['',i] for i in site_urls]
                request.data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
                return Response({'data':request.data}, template_name='partner/affiliate.html')
            networkid = NetworkAndSiteMap.objects.get(siteid__id=request.data['siteid'])
            request.data['dob'] = datetime.strptime(request.data['dob'], '%d-%m-%Y').strftime("%Y-%m-%d")
            request.data['networkid'] = networkid.networkid.networkid
            request.data['account_manager'] = acc_manager.id
            request.data._mutable = True
            request.data['ipaddress'] = request.META['REMOTE_ADDR']
            password = request.data.get("password")
            if password and 'sha1$' not in password:
                request.data['password'] = enc_password(password)
            if request.data.get('mailrequired', '') == 'on':
                request.data['mailrequired'] = True
            else:
                request.data['mailrequired'] = False
            if request.data.get('mobilerequired', '') == 'on':
                request.data['mobilerequired'] = True
            else:
                request.data['mobilerequired'] = False
            if request.data.get('active', '') == 'on':
                request.data['active'] = True
            else:
                request.data['active'] = False
            if request.data.get('im', '') == 'on':
                request.data['im'] = True
            else:
                request.data['im'] = False
            request.data['registeredon'] = datetime.now()
            request.data['username'] = request.data['email']
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            aff_obj = Affiliateinfo.objects.get(id=serializer.data['id'])
            aff_comm_obj = Affiliatecommission.objects.create(fk_affiliateinfo=aff_obj,
                                            revshare=True,
                                            revsharepercent=25)
            aff_comm_obj.save()
            for url_d in site_urls:
                if not url_d:
                    continue
                aff_site, created = AffiliateSite.objects.get_or_create(fk_affiliateinfo=aff_obj,
                                            info = url_d)
                aff_site.save()
            request.session['created'] = True
            id_ = serializer.data['id']
        except Exception as e:
            print str(e)
            request.session['created'] = False
            id_ = 'post'
        return HttpResponseRedirect("/admin/add-affiliate/"+str(id_))

    def perform_create(self, serializer):
        serializer.save()

    @user_class_role_management('Add Affiliate')
    def retrieve(self, request, pk=None):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        try:
            instance = self.get_object()
            # queryset = get_object_or_404(queryset, pk=pk)
            # queryset = self.filter_queryset(self.get_queryset())
            serializer = self.get_serializer(instance)
        except:
            serializer = self.get_serializer(data={})
            serializer.is_valid()
        if request.accepted_renderer.format == 'html':
            data = serializer.data
            acc_manager, partuser_obj, user = get_u_details(request)
            if data.get('dob'):
                data['dob'] = datetime.strptime(data['dob'], "%Y-%m-%d").strftime('%d-%m-%Y')
            if data.get('registeredon'):
                data['registeredon'] = datetime.strptime(data['registeredon'], "%Y-%m-%dT%H:%M:%S").strftime('%d-%m-%Y %H:%M:%S')
            data['partner'] = acc_manager.affiliateprogramme.name
            if data.get('id'):
                acc_details = AccountDetails.objects.filter(fk_affiliateinfo__id = data['id'])
                if acc_details:
                    data['account_id'] = str(acc_details[0].id)
                else:
                    aff_info = Affiliateinfo.objects.get(id = data['id'])
                    data['account_id'] = AccountDetails.objects.create(fk_affiliateinfo = aff_info)
            if data.get('id'):
                aff_c_obj = Affiliatecommission.objects.filter(fk_affiliateinfo=data['id'])
                if aff_c_obj:
                    data['comm_page'] = '<a href="/admin/affiliatecommission?aff_id='+str(data['id'])+'" class="btn btn-success payment-button rounded">Commission</a>'
                else:
                    data['comm_page'] = '<a href="/admin/affiliatecommission/c'+str(data['id'])+'" class="btn btn-success payment-button rounded">Commission</a>'
            if data.get('id'):
                request.session['affiliate_session_id'] = data['id']
                data['login'] = '<a href="https://'+ acc_manager.affiliateprogramme.domain +'/affiliate/" target="_blank" class="btn btn-success pl-4 pr-4 pull-right" style="margin-top:-5px;">Login</a>'
            if data.get('siteid'):
                data['siteid'] = [str(i) for i in data['siteid']]
            if not data.get('skype'):
                data['skype'] = ''
            if not data.get('country'):
                data['country'] = '4'
            msg = ''
            crt = request.session.get('created')
            deleted = request.session.get('deleted')
            if request.session.get('created') or request.session.get('deleted'):
                if crt == 'Exist':
                    msg = '<div class="alert alert-danger alert-danger-msg"><center> <span>Email address has already been registered with us.</span></center></div>'
                elif crt == True:
                    msg = '<div class="alert alert-success alert-success-msg"><center> <span>Affiliate added successfully.</span></center></div>'
                elif crt == False:
                    msg = '<div class="alert alert-danger alert-danger-msg"><center> <span>Invalid data. Please enter valid details.</span></center></div>'
                elif deleted:
                    msg = '<div class="alert alert-success alert-success-msg"><center> <span>The affiliate has been successfully deleted.</span></center></div>'
                else:
                    msg = '<div class="alert alert-danger alert-danger-msg"><center> <span>There is an error while adding affiliate.</span></center></div>'
                if crt:
                    del request.session['created']
                if deleted:
                    del request.session['deleted']
            elif request.session.get('created') == False:
                msg = '<div class="alert alert-danger alert-danger-msg"><center> <span>Invalid data. Please enter valid details.</span></center></div>'
                del request.session['created']
            elif request.session.get('created') == 'Exist':
                msg = '<div class="alert alert-danger alert-danger-msg"><center> <span>Email already Registered with Us</span></center></div>'
                del request.session['created']
            data['message'] = msg
            data['partner_obj'] = acc_manager.affiliateprogramme
            data['country_list'] = Country.objects.all().values_list('id', 'countryname')
            data['network_list'] = [[i.networkid, i.networkname] for i in acc_manager.affiliateprogramme.networkid.all()]
            data['account_managers'] = [[i.id, i.user.username] for i in AccountManager.objects.all()]
            data['affiliate_status'] = [j[0] for j in Affiliateinfo.AFFILIATESTATUS]
            data['affiliates_list'] = [[i.id, i.email] for i in Affiliateinfo.objects.filter(account_manager=acc_manager)]
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            if 'id' in data:
                data['site_urls_list'] =[[i.id, i.info] for i in AffiliateSite.objects.filter(fk_affiliateinfo__id = data['id'])]
            data['call'] = 'PUT'
            if self.kwargs.get('pk') == 'post':
                data['siteid'] = [str(i[0]) for i in data['sites_list']]
                data['call'] = 'POST'
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/affiliate.html')
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        partial = kwargs.pop('partial', False)
        context_dict = {}
        instance = self.get_object()
        affiliate = instance
        acc_manager, partuser_obj, user = get_u_details(request)
        mailidcount = Affiliateinfo.objects.filter(
            email=request.data['email'], account_manager=acc_manager).count()
        if mailidcount > 1:
            request.session['created'] = 'Exist'
            id_ = 'post'
            return HttpResponseRedirect("/admin/add-affiliate/"+str(id_))
        request.data._mutable = True
        password = request.data.get("password")
        status = request.data.get("status")
        site_urls = request.data.getlist('site_url')
        if site_urls:
            del request.data['site_url']
            for url_d in site_urls:
                if not url_d:
                    continue
                aff_site, created = AffiliateSite.objects.get_or_create(fk_affiliateinfo=affiliate,
                                            info = url_d)
                aff_site.save()
        AffiliateSite.objects.filter(fk_affiliateinfo=affiliate).exclude(info__in=site_urls).delete()
        if password and password !='' and 'sha1$' not in password:
            request.data['password'] = enc_password(password)
        else:
            del request.data['password']
        if request.data.get('mailrequired', '') == 'on':
            request.data['mailrequired'] = True
        else:
            request.data['mailrequired'] = False
        if request.data.get('mobilerequired', '') == 'on':
            request.data['mobilerequired'] = True
        else:
            request.data['mobilerequired'] = False
        if request.data.get('active', '') == 'on':
            request.data['active'] = True
        else:
            request.data['active'] = False
        if request.data.get('im', '') == 'on':
            request.data['im'] = True
        else:
            request.data['im'] = False
        duplicate = False
        if affiliate.email != request.data['email']:
            mailidcount = Affiliateinfo.objects.filter(email=request.data['email'], 
                account_manager=affiliate.account_manager).count()
            if mailidcount > 0:
                duplicate = True
                context_dict["message"] = 'Email address is already \
                registered with '+ affiliate.account_manager.affiliateprogramme.name
        if instance.status != request.data.get("status") and not duplicate:
            if status == 'Approved':
                # add_message_logs(affiliate, trigger_obj, affiliate.account_manager.email, affiliate.email)
                statuschangemail(affiliate, partuser_obj,affiliate, 'AFFILIATE_SIGN_UP_APPROVAL')
            elif status == 'Rejected':
                # add_message_logs(affiliate, trigger_obj, affiliate.account_manager.email, affiliate.email)
                statuschangemail(affiliate, partuser_obj, affiliate, 'AFFILIATE_SIGN_UP_REJECTED')
        if request.data.get('dob'):
            request.data['dob'] = datetime.strptime(request.data['dob'], '%d-%m-%Y').strftime("%Y-%m-%d")
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        print 'check edit affiliate', request.data
        serializer.is_valid(raise_exception=False)
        if not duplicate:
            self.perform_update(serializer)
        data = serializer.data
        data["duplicate"] = duplicate
        if request.accepted_renderer.format == 'html':
            acc_manager, partuser_obj, user = get_u_details(request)
            if data.get('dob'):
                data['dob'] = datetime.strptime(data['dob'], "%Y-%m-%d").strftime('%d-%m-%Y')
            if data.get('registeredon'):
                data['registeredon'] = datetime.strptime(data['registeredon'], "%Y-%m-%dT%H:%M:%S").strftime('%d-%m-%Y %H:%M:%S')
            data['partner'] = acc_manager.affiliateprogramme.name
            data['siteid'] = [str(i) for i in data['siteid']]
            data['message'] = 'Affiliate details have been updated successfully.'
            request.session['pending_count'] = Affiliateinfo.objects.filter(
                                account_manager=acc_manager,
                                status="Pending").count()
            data["duplicate"] = duplicate
            data['country_list'] = Country.objects.all().values_list('id', 'countryname')
            data['partner_obj'] = acc_manager.affiliateprogramme
            data['network_list'] = [[i.networkid, i.networkname] for i in acc_manager.affiliateprogramme.networkid.all()]
            data['account_managers'] = [[i.id, i.user.username] for i in AccountManager.objects.all()]
            data['affiliate_status'] = [j[0] for j in Affiliateinfo.AFFILIATESTATUS]
            data['affiliates_list'] = [[i.id, i.email] for i in Affiliateinfo.objects.filter(account_manager=acc_manager)]
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            data['site_urls_list'] =[[i.id, i.info] for i in AffiliateSite.objects.filter(fk_affiliateinfo__id = data['id'])]
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/affiliate.html')
        return Response(data)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        request.session['deleted'] = True
        return self.list(request)

    def perform_destroy(self, instance):
        instance.delete()


class AffiliateAccountDetailsViewSet(viewsets.ModelViewSet):

    """
    Endpoint: /admin/affiliatecommission/

    GET: Typical RESTful Response including Pagination.

    `search` query parameter => ?search=<search text>

    PUT & PATCH are available though no validations
                beyond data types exist on it for now.

    """
    queryset = AccountDetails.objects.all()
    serializer_class = AccountDetailsSerializer
    filter_backends = (filters.SearchFilter,
                       DjangoFilterBackend, filters.OrderingFilter,)
    parser_classes = (MultiPartParser, FormParser,)
    renderer_classes = (renderers.JSONRenderer, renderers.TemplateHTMLRenderer)

    @user_class_role_management('Account Details')
    def list(self, request, *args, **kwargs):
        acc_manager, partuser_obj, user = get_u_details(request)
        queryset = self.filter_queryset(self.get_queryset())
        queryset = queryset.filter(fk_affiliateinfo__account_manager=acc_manager)
        page = True
        if page is not None:
            serializer = self.get_serializer(queryset, many=True)
            if request.accepted_renderer.format == 'html':
                cpage = int(request.GET.get('page')) if request.GET.get('page') else 1
                header_list = [{'title': 'Affiliate Id'}, {'title': 'Affiliate'}, {'title':'Payment Type'},
                        {'title': 'Account Number'},{'title':'Beneficiary Name'},{'title':'Beneficiary Email'},
                        {'title':'Beneficiary Address'},{'title':'Bank Name'},{'title':'Bank Address'},
                        {'title':'Sort Code'},{'title':'IBAN'},{'title':'SWIFT'},
                        {'title': 'View/ Edit'}]
                
                page_div = ''
                data_list = []
                for i in serializer.data:
                    if not i['fk_affiliateinfo']:
                        continue
                    ac_m = Affiliateinfo.objects.get(id=i['fk_affiliateinfo']).email
                    # added logic for payment type
                    print('acc id',i['id'])
                    payment_type = str(i["accounttype"])
                    if payment_type == 'None':
                        if i["accountnumber"] and i["accountnumber"]!=' ':
                            payment_type = 'Bank Wire Transfer'
                        elif i["paypal_email"] and i["paypal_email"]!=' ' :
                            payment_type = 'Paypal'
                        elif i["neteller_accountnumber"] and i["neteller_accountnumber"]!=' ':
                            payment_type = "Neteller"
                        else:
                            payment_type = "No Details Given"
                    if payment_type == 'Neteller' or payment_type == 'Paypal':
                        if payment_type == 'Neteller':
                            account_number = i["neteller_accountnumber"] if i["neteller_accountnumber"] else '-'
                            ben_name = i["neteller_username"] if i["neteller_username"] else '-'
                            bank_name = '-'
                            ben_email = i["neteller_email"] if i["neteller_email"] else '-'
                        elif payment_type == 'Paypal':
                            account_number = '-'
                            ben_name = i["paypal_username"] if i["paypal_username"] else '-'
                            bank_name = '-'
                            ben_email = i["paypal_email"] if i["paypal_email"] else '-'
                        bankaddress = '-'
                        iban = '-'
                        swift = '-'
                        ben_address = '-'
                        sortcode = '-'
                    else:
                        account_number = str(i["accountnumber"]) if i["accountnumber"] and i["accountnumber"]!=' ' else '-'
                        ben_name = i["accountname"] if i["accountname"] and i["accountname"]!=' ' else '-'
                        bank_name = i["bankname"] if i["bankname"] and i["bankname"]!= ' ' else '-'
                        ben_email = '-'
                        bankaddress = i["bankaddress"] if i["bankaddress"] and i["bankaddress"]!=' ' else '-'
                        iban = i["iban"] if i["iban"] and i["iban"]!=' ' else '-'
                        swift = i["swift"] if i["swift"] and i["swift"]!=' ' else '-'
                        ben_address = i["beneficiaryaddress"] if i["beneficiaryaddress"] and i["beneficiaryaddress"]!=' ' else '-'
                        sortcode = i["sortcode"] if i["sortcode"] and i["sortcode"]!=' ' else '-'
                    edit_ele = '<a href="/admin/payment-details/'+str(i['id'])+'"><button class="btn btn-success">View/ Edit</button></a>'
                    del_ele = '<form class="button-form" action="/admin/payment-details/'+str(i['id'])+'" data-method="DELETE"><button class="btn btn-danger">Delete</button></form>' 
                    data_list.append([i['fk_affiliateinfo'],str(ac_m),payment_type, 
                                    account_number, ben_name, ben_email, ben_address, bank_name,
                                    bankaddress, sortcode,iban, swift,  
                                     edit_ele])
                return Response({'header_list': header_list, 'data_list': json.dumps(data_list), 'pagination_div': page_div,
                            'partner': acc_manager.affiliateprogramme.name,
                            'partner_obj': acc_manager.affiliateprogramme,
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/accountdetails_list.html')
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        # if request.accepted_renderer.format == 'html':
        #     return Response({'data': serializer.data}, template_name='partner/index.html')
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        request.session['created'] = True
        id_ = serializer.data['id']
        return HttpResponseRedirect("/admin/payment-details/"+str(id_))

    def perform_create(self, serializer):
        serializer.save()

    @user_class_role_management('Account Details')
    def retrieve(self, request, pk=None):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        try:
            instance = self.get_object()
            # queryset = get_object_or_404(queryset, pk=pk)
            # queryset = self.filter_queryset(self.get_queryset())
            serializer = self.get_serializer(instance)
        except:
            serializer = self.get_serializer(data={})
            serializer.is_valid()
        # queryset = get_object_or_404(queryset, pk=pk)
        # queryset = self.filter_queryset(self.get_queryset())
        # serializer = self.get_serializer(instance)
        if request.accepted_renderer.format == 'html':
            try:
                data = serializer.data
            except:
                data = {}
            msg = ''
            if request.session.get('created'):
                crt = request.session.get('created')
                if crt:
                    msg = 'Payment details have been successfully updated.'
                else:
                    msg = 'There is an error while adding payment details.'
                del request.session['created']
            data['message'] = msg
            data['currency_list'] = [[j['id__max'], j['currency']] for j in Currency.objects.values('currency').annotate(Max('id')).distinct()]
            # data['currency_list'] = [[j.id, j.currency] for j in Currency.objects.all()]
            # data['affiliates_list'] = [[i.id, i.username] for i in Affiliateinfo.objects.all()]
            acc_manager, partuser_obj, user = get_u_details(request)
            data['affiliates_list'] = [[i.id, i.email] for i in Affiliateinfo.objects.filter(account_manager=acc_manager)]
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            data['partner'] = acc_manager.affiliateprogramme.name
            data['partner_obj'] = acc_manager.affiliateprogramme
            data['call'] = 'POST' if self.kwargs.get('pk') == 'post' else 'PUT'
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/accountdetails.html')
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        acc_manager, partuser_obj, user = get_u_details(request)
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        request.data._mutable = True
        # checking for accounttype
        if request.data['neteller_accountnumber']:
            request.data['accounttype'] = "Neteller"
        elif request.data['paypal_email'].encode() != ' ':
            request.data['accounttype'] = "Paypal"
        elif request.data['accountnumber']:
            request.data['accounttype'] = "Bank Wire Transfer"
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=False)
        self.perform_update(serializer)
        if serializer.data.get('id'):
            pay_obj = AccountDetails.objects.get(id=serializer.data['id'])
            if pay_obj:
                headers = {'Reply-To': acc_manager.user.email}
                subject = "Change of Payment Details on {0}".format(acc_manager.affiliateprogramme.name)
                content = """Dear {0},<br><br>
                            Your account manager has successfully updated your payment details for {1} payments on {2}.<br><br>
                            Thanks,<br>
                            Team @ Wynta""".format(pay_obj.fk_affiliateinfo.contactname,pay_obj.accounttype,acc_manager.affiliateprogramme.name)
                check_EmailClient(acc_manager, partuser_obj, subject, content, """%s""" % 'support@wynta.com', [acc_manager.user.email], [pay_obj.fk_affiliateinfo.email], headers, pay_obj.fk_affiliateinfo,None,None, None)
        if request.accepted_renderer.format == 'html':
            data = serializer.data
            # data['currency_list'] = [[j.id, j.currency] for j in Currency.objects.all()]
            # data['affiliates_list'] = [[i.id, i.username] for i in Affiliateinfo.objects.all()]
            acc_manager, partuser_obj, user = get_u_details(request)
            data['message'] = 'Payment details have been successfully updated.'
            data['currency_list'] = [[j['id__max'], j['currency']] for j in Currency.objects.values('currency').annotate(Max('id')).distinct()]
            data['affiliates_list'] = [[i.id, i.email] for i in Affiliateinfo.objects.filter(account_manager=acc_manager)]
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            data['partner'] = acc_manager.affiliateprogramme.name
            data['partner_obj'] = acc_manager.affiliateprogramme
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/accountdetails.html')
        return Response(serializer.data)


class TieredStructureViewset(viewsets.ModelViewSet):

    """
    Endpoint: /admin/tiered-structure/

    GET: Typical RESTful Response including Pagination.

    `search` query parameter => ?search=<search text>

    PUT & PATCH are available though no validations
                beyond data types exist on it for now.

    """
    queryset = TieredStructure.objects.all()
    serializer_class = TieredStructureSerializer
    filter_backends = (filters.SearchFilter,
                       DjangoFilterBackend, filters.OrderingFilter,)
    parser_classes = (MultiPartParser, FormParser,)
    renderer_classes = (renderers.JSONRenderer, renderers.TemplateHTMLRenderer)

    @user_class_role_management('TieredStructure')
    def list(self, request, *args, **kwargs):
        acc_manager, partuser_obj, user = get_u_details(request)
        queryset = self.filter_queryset(self.get_queryset())
        queryset = queryset.filter(account_manager=acc_manager)
        filt_er = request.GET.get('aff_id', '')
        if filt_er:
            affiliate_obj = Affiliateinfo.objects.get(id=filt_er)
            queryset = queryset.filter(fk_affiliateinfo=affiliate_obj)
        queryset = queryset.order_by('-id')
        page = True
        if page is not None:
            serializer = self.get_serializer(queryset, many=True)
            if request.accepted_renderer.format == 'html':
                cpage = int(request.GET.get('page')) if request.GET.get('page') else 1
                header_list = [{'title': 'Brand'}, {'title': 'Tiered Type'}, 
                        {'title':'Tiered Structure'},
                         {'title':'From date'}, {'title':'To Date'}, {'title': 'View/ Edit'}]
                
                page_div = ''
                data_list = []
                for i in serializer.data:
                    brand = ''
                    country = ''
                    ttype = ''
                    if i.get('stype'):
                        ttype = str(i['stype'])
                    if i.get('siteid'):
                        brand = Site.objects.get(id=i['siteid']).name
                    edit_ele = '<a href="/admin/tiered-structure/'+str(i['id'])+'"><button class="btn btn-success">View/ Edit</button></a>'
                    del_ele = '<form class="button-form" action="/admin/tiered-structure/'+str(i['id'])+'" data-method="DELETE"><button class="btn btn-danger">Delete</button></form>' 
                    start_date = ''
                    end_date = ''
                    if i['start_date']:
                        start_date = datetime.strptime(i['start_date'], "%Y-%m-%d").strftime('%d-%m-%Y')
                    if i['end_date']:
                        end_date = datetime.strptime(i['end_date'], "%Y-%m-%d").strftime('%d-%m-%Y')
                    options = ast.literal_eval(i['options'])
                    ordered_options = collections.OrderedDict()
                    for d in sorted(options.keys()):
                        ordered_options[d] = options[d]
                    options = ', '.join("%s @%s%%" % ('-'.join(map(str, v)), x) for x, v in ordered_options.iteritems())
                    data_list.append([str(brand), ttype, str(options),
                                    start_date, 
                                    end_date,
                                     edit_ele])
                return Response({'header_list': header_list, 'data_list': data_list, 'pagination_div': page_div,
                                'partner': acc_manager.affiliateprogramme.name,
                                'partner_obj': acc_manager.affiliateprogramme,
                                'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
                                }, template_name='partner/tieredstructure_list.html')
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        # if request.accepted_renderer.format == 'html':
        #     return Response({'data': serializer.data}, template_name='partner/index.html')
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        data = {}
        acc_manager, partuser_obj, user = get_u_details(request)
        request.data._mutable = True
        request_dict = request.data
        tiered_dict = {v:[request_dict.getlist('from')[i], request_dict.getlist('to')[i]] for i,v in enumerate(request_dict.getlist('val'))}
        if request.data.get('start_date'):
            request.data['start_date'] = datetime.strptime(request.data['start_date'], '%d-%m-%Y').strftime("%Y-%m-%d")
        if request.data.get('end_date'):
            request.data['end_date'] = datetime.strptime(request.data['end_date'], '%d-%m-%Y').strftime("%Y-%m-%d")
        request.data['active'] = True
        request.data['account_manager'] = acc_manager.id
        request.data['options'] = json.dumps(tiered_dict)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        request.session['created'] = True
        id_ = serializer.data['id']
        return HttpResponseRedirect("/admin/tiered-structure/"+str(id_))

    def perform_create(self, serializer):
        serializer.save()

    @user_class_role_management('TieredStructure')
    def retrieve(self, request, pk=''):
        acc_manager, partuser_obj, user = get_u_details(request)
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        try:
            instance = self.get_object()
            serializer = self.get_serializer(instance)
        except:
            serializer = self.get_serializer(data={})
            serializer.is_valid()
        # queryset = get_object_or_404(queryset, pk=pk)
        # queryset = self.filter_queryset(self.get_queryset())
        # serializer = self.get_serializer(instance)
        if request.accepted_renderer.format == 'html':
            try:
                data = serializer.data
            except:
                data = {}
            if data.get('options'):
                options = ast.literal_eval(data['options'])
                ordered_options = collections.OrderedDict()
                for d in sorted(options.keys()):
                    ordered_options[d] = options[d]
                data['options'] = ordered_options
            msg = ''
            if request.session.get('created'):
                crt = request.session.get('created')
                if crt:
                    msg = 'Structure set successfully.'
                else:
                    msg = 'There is an error while setting Structure.'
                del request.session['created']
            if data.get('start_date'):
                data['start_date'] = datetime.strptime(data['start_date'], "%Y-%m-%d").strftime('%d-%m-%Y')
            else:
                data['start_date'] = ''
            if data.get('end_date'):
                data['end_date'] = datetime.strptime(data['end_date'], "%Y-%m-%d").strftime('%d-%m-%Y')
            else:
                data['end_date'] = ''
            data['stype_list'] = [j[0] for j in TieredStructure.STYPE]
            data['message'] = msg
            data['partner'] = acc_manager.affiliateprogramme.name
            data['partner_obj'] = acc_manager.affiliateprogramme
            data['country_list'] = Country.objects.all().values_list('id', 'countryname')
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            data['call'] = 'POST' if self.kwargs.get('pk') == 'post' else 'PUT'
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/tieredstructure.html')
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        request.data._mutable = True
        request_dict = request.data
        tiered_dict = {v:[request_dict.getlist('from')[i], request_dict.getlist('to')[i]] for i,v in enumerate(request_dict.getlist('val'))}
        if request.data.get('start_date') and str(request.data['start_date']).lower() != 'none':
            request.data['start_date'] = datetime.strptime(request.data['start_date'], '%d-%m-%Y').strftime("%Y-%m-%d")
        if request.data.get('end_date') and str(request.data['end_date']).lower() != 'none':
            request.data['end_date'] = datetime.strptime(request.data['end_date'], '%d-%m-%Y').strftime("%Y-%m-%d")
        request.data['options'] = json.dumps(tiered_dict)
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=False)
        self.perform_update(serializer)
        if request.accepted_renderer.format == 'html':
            data = serializer.data
            acc_manager, partuser_obj, user = get_u_details(request)
            data['message'] = 'Structure has been updated successfully.'
            if data.get('start_date'):
                data['start_date'] = datetime.strptime(data['start_date'], "%Y-%m-%d").strftime('%d-%m-%Y')
            else:
                data['start_date'] = ''
            if data.get('end_date'):
                data['end_date'] = datetime.strptime(data['end_date'], "%Y-%m-%d").strftime('%d-%m-%Y')
            else:
                data['end_date'] = ''
            if data.get('options'):
                data['options']=ast.literal_eval(data['options'])
            data['stype_list'] = [j[0] for j in TieredStructure.STYPE]
            data['country_list'] = Country.objects.all().values_list('id', 'countryname')
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            data['partner'] = acc_manager.affiliateprogramme.name
            data['partner_obj'] = acc_manager.affiliateprogramme
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/tieredstructure.html')
        return Response(serializer.data)

class DeductionsViewSet(viewsets.ModelViewSet):

    """
    Endpoint: /admin/deductions/

    GET: Typical RESTful Response including Pagination.

    `search` query parameter => ?search=<search text>

    PUT & PATCH are available though no validations
                beyond data types exist on it for now.

    """
    queryset = Deduction.objects.all()
    serializer_class = DeductionSerializer
    filter_backends = (filters.SearchFilter,
                       DjangoFilterBackend, filters.OrderingFilter,)
    parser_classes = (MultiPartParser, FormParser,)
    renderer_classes = (renderers.JSONRenderer, renderers.TemplateHTMLRenderer)

    @user_class_role_management('Deductions')
    def list(self, request, *args, **kwargs):
        acc_manager, partuser_obj, user = get_u_details(request)
        queryset = self.filter_queryset(self.get_queryset())
        queryset = queryset.filter(siteid__in=acc_manager.siteid.all())
        queryset = queryset.order_by('-id')
        page = True
        if page is not None:
            serializer = self.get_serializer(queryset, many=True)
            if request.accepted_renderer.format == 'html':
                cpage = int(request.GET.get('page')) if request.GET.get('page') else 1
                header_list = [{'title': 'Brand'}, {'title': 'Country'},
                        {'title':'Deductions'},
                         {'title':'From date'}, {'title':'To Date'}, {'title': 'View/ Edit'}]
                
                page_div = ''
                data_list = []
                for i in serializer.data:
                    brand = ''
                    country = 'All'
                    if i.get('siteid'):
                        brand = Site.objects.get(id=i['siteid']).name
                    if i.get('country'):
                        country = Country.objects.filter(
                                    id__in=i['country']).values_list(
                                    'countryname', flat=True)
                        country = str(','.join(country))
                    edit_ele = '<a href="/admin/deductions/'+str(i['id'])+'"><button class="btn btn-success">View/ Edit</button></a>'
                    del_ele = '<form class="button-form" action="/admin/deductions/'+str(i['id'])+'" data-method="DELETE"><button class="btn btn-danger">Delete</button></form>' 
                    start_date = ''
                    end_date = ''
                    if i['start_date']:
                        start_date = datetime.strptime(i['start_date'], "%Y-%m-%d").strftime('%d-%m-%Y')
                    if i['end_date']:
                        end_date = datetime.strptime(i['end_date'], "%Y-%m-%d").strftime('%d-%m-%Y')
                    data_list.append([str(brand), str(country), 
                                'POCT: %s, Admin Fee: %s'%(i['poctvalue'], i['adminfeevalue']),
                                    start_date, end_date, edit_ele])
                return Response({'header_list': header_list, 'data_list': data_list, 'pagination_div': page_div,
                                'partner': acc_manager.affiliateprogramme.name,
                                'partner_obj': acc_manager.affiliateprogramme,
                                'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
                                }, template_name='partner/deductions_list.html')
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        # if request.accepted_renderer.format == 'html':
        #     return Response({'data': serializer.data}, template_name='partner/index.html')
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        data = {}
        acc_manager, partuser_obj, user = get_u_details(request)
        request.data._mutable = True
        if request.data.get('start_date'):
            request.data['start_date'] = datetime.strptime(request.data['start_date'], '%d-%m-%Y').strftime("%Y-%m-%d")
        if request.data.get('end_date'):
            request.data['end_date'] = datetime.strptime(request.data['end_date'], '%d-%m-%Y').strftime("%Y-%m-%d")
        request.data['adminfee'] = request.data.get('adminfee', 'false')
        request.data['pocdeduction'] = request.data.get('pocdeduction', 'false')
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        request.session['created'] = True
        id_ = serializer.data['id']
        return HttpResponseRedirect("/admin/deductions/"+str(id_))

    def perform_create(self, serializer):
        serializer.save()

    @user_class_role_management('Deductions')
    def retrieve(self, request, pk=''):
        acc_manager, partuser_obj, user = get_u_details(request)
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        try:
            instance = self.get_object()
            serializer = self.get_serializer(instance)
        except:
            serializer = self.get_serializer(data={})
            serializer.is_valid()
        # queryset = get_object_or_404(queryset, pk=pk)
        # queryset = self.filter_queryset(self.get_queryset())
        # serializer = self.get_serializer(instance)
        if request.accepted_renderer.format == 'html':
            try:
                data = serializer.data
            except:
                data = {}
            msg = ''
            if request.session.get('created'):
                crt = request.session.get('created')
                if crt:
                    msg = 'Deductions Set successfully.'
                else:
                    msg = 'There is an error while setting Deductions.'
                del request.session['created']
            if data.get('start_date'):
                data['start_date'] = datetime.strptime(data['start_date'], "%Y-%m-%d").strftime('%d-%m-%Y')
            else:
                data['start_date'] = ''
            if data.get('end_date'):
                data['end_date'] = datetime.strptime(data['end_date'], "%Y-%m-%d").strftime('%d-%m-%Y')
            else:
                data['end_date'] = ''
            if data.get('siteid'):
                data['brand'] = Site.objects.get(id=data.get('siteid')).name
            if data.get('country'):
                data['country'] = [str(i) for i in data['country']]
            data['message'] = msg
            data['partner'] = acc_manager.affiliateprogramme.name
            data['partner_obj'] = acc_manager.affiliateprogramme
            data['country_list'] = Country.objects.all().values_list('id', 'countryname')
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            data['call'] = 'POST' if self.kwargs.get('pk') == 'post' else 'PUT'
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/deductions.html')
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        request.data._mutable = True
        if request.data.get('start_date') and str(request.data['start_date']).lower() != 'none':
            request.data['start_date'] = datetime.strptime(request.data['start_date'], '%d-%m-%Y').strftime("%Y-%m-%d")
        if request.data.get('end_date') and str(request.data['end_date']).lower() != 'none':
            request.data['end_date'] = datetime.strptime(request.data['end_date'], '%d-%m-%Y').strftime("%Y-%m-%d")
        request.data['adminfee'] = request.data.get('adminfee', 'false')
        request.data['pocdeduction'] = request.data.get('pocdeduction', 'false')
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=False)
        self.perform_update(serializer)
        if request.accepted_renderer.format == 'html':
            data = serializer.data
            acc_manager, partuser_obj, user = get_u_details(request)
            data['message'] = 'Deductions has been updated successfully.'
            if data.get('start_date'):
                data['start_date'] = datetime.strptime(data['start_date'], "%Y-%m-%d").strftime('%d-%m-%Y')
            else:
                data['start_date'] = ''
            if data.get('end_date'):
                data['end_date'] = datetime.strptime(data['end_date'], "%Y-%m-%d").strftime('%d-%m-%Y')
            else:
                data['end_date'] = ''
            if data.get('country'):
                data['country'] = [str(i) for i in data['country']]
            data['country_list'] = Country.objects.all().values_list('id', 'countryname')
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            data['partner'] = acc_manager.affiliateprogramme.name
            data['partner_obj'] = acc_manager.affiliateprogramme
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/deductions.html')
        return Response(serializer.data)


class AffiliateCommissionViewSet(viewsets.ModelViewSet):

    """
    Endpoint: /admin/affiliatecommission/

    GET: Typical RESTful Response including Pagination.

    `search` query parameter => ?search=<search text>

    PUT & PATCH are available though no validations
                beyond data types exist on it for now.

    """
    queryset = Affiliatecommission.objects.all()
    serializer_class = AffiliatecommissionSerializer
    filter_backends = (filters.SearchFilter,
                       DjangoFilterBackend, filters.OrderingFilter,)
    parser_classes = (MultiPartParser, FormParser,)
    renderer_classes = (renderers.JSONRenderer, renderers.TemplateHTMLRenderer)

    @user_class_role_management('Country')
    def list(self, request, *args, **kwargs):
        acc_manager, partuser_obj, user = get_u_details(request)
        queryset = self.filter_queryset(self.get_queryset())
        filt_er = request.GET.get('aff_id', '')
        #queryset = queryset.filter(
        #     Q(fk_affiliateinfo__account_manager=acc_manager) | Q(
        #     fk_campaign__account_manager=acc_manager) |
        #     Q(account_manager=acc_manager) | Q(siteid__in=acc_manager.siteid.all()))
        #queryset = queryset.filter(account_manager=acc_manager)
        affiliate_obj = None
        if filt_er:
            affiliate_obj = Affiliateinfo.objects.get(id=filt_er)
            queryset = queryset.filter(fk_affiliateinfo=affiliate_obj)
        queryset = queryset.order_by('-id')
        page = True
        if page is not None:
            serializer = self.get_serializer(queryset, many=True)
            if request.accepted_renderer.format == 'html':
                cpage = int(request.GET.get('page')) if request.GET.get('page') else 1
                header_list = [{'title': 'Affiliate/Campaign'}, {'title': 'Brand'}, {'title': 'Country'},
                        {'title':'Commission'},
                         {'title':'From date'}, {'title':'To Date'}, {'title': 'View/ Edit'}]
                
                page_div = ''
                data_list = []
                for i in serializer.data:
                    ac_m = ''
                    brand = ''
                    country = ''
                    if i.get('fk_affiliateinfo'):
                        ac_m = Affiliateinfo.objects.filter(id=i['fk_affiliateinfo']).first()
                        if ac_m:
                            ac_m = ac_m.email
                    elif i.get('fk_campaign'):
                        ac_m = CampaignTrackerMapping.objects.get(id=i['fk_campaign']).fk_campaign
                        brand = ac_m.siteid.name
                        ac_m = ac_m.name
                    if i.get('siteid'):
                        brand = Site.objects.get(id=i['siteid']).name
                    # if i.get('country'):
                    #     country = Country.objects.get(id=i['country']).countryname
                    country = ''
                    edit_ele = '<a href="/admin/affiliatecommission/'+str(i['id'])+'"><button class="btn btn-success">View/ Edit</button></a>'
                    del_ele = '<form class="button-form" action="/admin/affiliatecommission/'+str(i['id'])+'" data-method="DELETE"><button class="btn btn-danger">Delete</button></form>' 
                    rate_d = 'REVSHARE: '+str(i['revsharepercent'])+ ' + CPA: '+str(i['cpacommissionvalue'])+ ' + Referral: '+str(i['referalcommissionvalue'])
                    start_date = ''
                    end_date = ''
                    if i['start_date']:
                        start_date = datetime.strptime(i['start_date'], "%Y-%m-%d").strftime('%d-%m-%Y')
                    if i['end_date']:
                        end_date = datetime.strptime(i['end_date'], "%Y-%m-%d").strftime('%d-%m-%Y')
                    data_list.append([str(ac_m), str(brand), str(country), rate_d,
                                    start_date, 
                                    end_date,
                                     edit_ele])
                return Response({'header_list': header_list, 'data_list': data_list, 'pagination_div': page_div,
                                'partner': acc_manager.affiliateprogramme.name,
                                'affid': 'c'+str(affiliate_obj.id) if affiliate_obj else 'post',
                                'partner_obj': acc_manager.affiliateprogramme,
                                'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
                                }, template_name='partner/affiliatecommission_list.html')
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        # if request.accepted_renderer.format == 'html':
        #     return Response({'data': serializer.data}, template_name='partner/index.html')
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        data = {}
        acc_manager, partuser_obj, user = get_u_details(request)
        if request.session.get('affiliate_exist'):
            del request.session['affiliate_exist']
        if request.data.get('type'):
            comm_type = request.data.get('type')
        else:
            comm_type = None
        # if request.data.get('fk_affiliateinfo'):
        #     aff_c_obj = []
        #     if request.data.get('siteid'):
        #         aff_c_obj = Affiliatecommission.objects.filter(
        #             fk_affiliateinfo=int(request.data['fk_affiliateinfo']),
        #             siteid=int(request.data['siteid']))
        #     aff_s_obj = Affiliatecommission.objects.filter(
        #         fk_affiliateinfo=int(request.data['fk_affiliateinfo']),
        #         siteid__isnull=True)
        #     if len(aff_c_obj) > 0:
        #         request.session['affiliate_exist'] = str(aff_c_obj[0].id)
        #         return HttpResponseRedirect('/admin/affiliatecommission/'+str(aff_c_obj[0].id))
        #     elif len(aff_s_obj) > 0 and not request.data.get('siteid'):
        #         request.session['affiliate_exist'] = str(aff_s_obj[0].id)
        #         return HttpResponseRedirect('/admin/affiliatecommission/'+str(aff_s_obj[0].id))
        if comm_type == 'default':
            request.data['account_manager'] = acc_manager.id
        request.data._mutable = True
        if request.data.get('start_date'):
            request.data['start_date'] = datetime.strptime(request.data['start_date'], '%d-%m-%Y').strftime("%Y-%m-%d")
        if request.data.get('end_date'):
            request.data['end_date'] = datetime.strptime(request.data['end_date'], '%d-%m-%Y').strftime("%Y-%m-%d")
        request.data['active'] = True
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        # if request.data.get('monthToDate'):
        m_to_d = request.data.get('monthToDate')
        if m_to_d:
            run_player_report(comm_type, 
                request.data.get('siteid'),
                request.data.get('fk_affiliateinfo'),
                acc_manager)
        headers = self.get_success_headers(serializer.data)
        request.session['created'] = True
        id_ = serializer.data['id']
        return HttpResponseRedirect("/admin/affiliatecommission/"+str(id_))

    def perform_create(self, serializer):
        serializer.save()

    @user_class_role_management('Default')
    def retrieve(self, request, pk=''):
        acc_manager, partuser_obj, user = get_u_details(request)
        campaign_ = {}
        default_ = {}
        country_ = {}
        affiliate_ = {}
        aff_c_obj = []
        brand_ = False
        c_type = 'Commission'
        if 'campaign' in request.META['PATH_INFO']:
            if 'c' in pk and 'a' not in pk:
                campaign_ = pk.replace('c','')
            elif 'c' in pk and 'a' in pk:
                campaign_, affiliate_ = pk.split('c')[1].split('a')
            c_type = 'Commission by Campaigns'
        elif 'affiliate' in request.META['PATH_INFO']:
            if 'c' in pk:
                affiliate_ = pk.replace('c','')
            c_type = 'Commission by Affiliates'
        elif 'country' in request.META['PATH_INFO']:
            country_ = 'country'
            c_type = 'Commission by Country'
        elif 'default' in request.META['PATH_INFO']:
            if 'c' in pk:
                default_ = True
            aff_c_obj = Affiliatecommission.objects.filter(account_manager=acc_manager)
            c_type = 'Commission: Default'
        else:
            if 'c' in pk:
                brand_ = pk.replace('c','')
            c_type = 'Commission: Brands'
        pk = None

        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        try:
            if len(aff_c_obj) > 0:
                instance = aff_c_obj[0]
            else:
                instance = self.get_object()
            # queryset = get_object_or_404(queryset, pk=pk)
            # queryset = self.filter_queryset(self.get_queryset())
            serializer = self.get_serializer(instance)
        except:
            serializer = self.get_serializer(data={})
            serializer.is_valid()
        # queryset = get_object_or_404(queryset, pk=pk)
        # queryset = self.filter_queryset(self.get_queryset())
        # serializer = self.get_serializer(instance)
        if request.accepted_renderer.format == 'html':
            try:
                data = serializer.data
            except:
                data = {}
            data['criteria_list'] = [j[0] for j in Affiliatecommission.CRITERIA]

            if affiliate_:
                data['fk_affiliateinfo'] = affiliate_
            if data.get('fk_affiliateinfo'):
                data['affiliate'] = Affiliateinfo.objects.filter(
                    id=data.get('fk_affiliateinfo'))[0].email

            if campaign_:
                aff_obj = Affiliateinfo.objects.get(id=affiliate_)
                linkobj = Campaign.objects.get(id=campaign_)
                got_error, camp_track_map = add_campaign_tracker(aff_obj, linkobj)
                data['fk_campaign'] = camp_track_map.id
                del data['affiliate']
                del data['fk_affiliateinfo']
            if data.get('fk_campaign'):
                data['campaign'] = CampaignTrackerMapping.objects.filter(
                    id=data.get('fk_campaign'))[0].fk_campaign.name

            if brand_:
                data['siteid'] = brand_
            if data.get('siteid'):
                data['brand'] = Site.objects.get(id=data.get('siteid')).name
            if country_:
                data['country'] = country_
            if data.get('country'):
                data['country'] = [str(i) for i in data['country']]
            #     data['countryname'] = Country.objects.get(id=data.get('country')).countryname

            if aff_c_obj:
                data['account_manager'] = acc_manager.id
            msg = ''
            if request.session.get('created'):
                crt = request.session.get('created')
                if crt:
                    msg = 'Commission Set successfully.'
                else:
                    msg = 'There is an error while setting Commission.'
                del request.session['created']
            request.session['c_type'] = c_type
            data['c_type'] = c_type
            if data.get('start_date'):
                data['start_date'] = datetime.strptime(data['start_date'], "%Y-%m-%d").strftime('%d-%m-%Y')
            else:
                data['start_date'] = ''
            if data.get('end_date'):
                data['end_date'] = datetime.strptime(data['end_date'], "%Y-%m-%d").strftime('%d-%m-%Y')
            else:
                data['end_date'] = ''
            data['message'] = msg
            data['partner'] = acc_manager.affiliateprogramme.name
            data['partner_obj'] = acc_manager.affiliateprogramme
            data['country_list'] = Country.objects.all().values_list('id', 'countryname')
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            data['campaign_list'] = [[int(i.id), (i.fk_campaign.name).encode('utf-8').strip(), int(i.fk_campaign.siteid.id), int(i.fk_campaign.fk_affiliateinfo_id) if i.fk_campaign.fk_affiliateinfo_id != None else ' '] for i in CampaignTrackerMapping.objects.filter(fk_campaign__account_manager=acc_manager)]
            print('campagin list',data['campaign_list'])
            data['affiliates_list'] = [[i.id, i.email] for i in Affiliateinfo.objects.filter(account_manager=acc_manager)]
            if data.get('fk_campaign'):
                data['fk_campaign'] = int(data['fk_campaign'])
            if data.get('c_type') == 'Commission: Default':
                if data.get('fk_campaign'):
                    del data['fk_campaign']
            if request.session.get('affiliate_exist'):
                data['affiliate_exist'] = 'Commission is already set for the affiliate.'
                del request.session['affiliate_exist']
            data['call'] = 'POST' if 'c' in self.kwargs.get('pk') else 'PUT'
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/affiliatecommission.html')
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        print('request data>>>>>>>>>>>>>>>>>>>>>>>',request.data)
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        request.data._mutable = True
        if request.data.get('start_date') and str(request.data['start_date']).lower() != 'none':
            request.data['start_date'] = datetime.strptime(request.data['start_date'], '%d-%m-%Y').strftime("%Y-%m-%d")
        if request.data.get('end_date') and str(request.data['end_date']).lower() != 'none':
            request.data['end_date'] = datetime.strptime(request.data['end_date'], '%d-%m-%Y').strftime("%Y-%m-%d")
        request.data['revshare'] = request.data.get('revshare', 'false')
        request.data['cpachoice'] = request.data.get('cpachoice', 'false')
        request.data['referalchoice'] = request.data.get('referalchoice', 'false')
        if request.data['referalchoice'].encode() == 'false':
            request.data['referalcommissionvalue'] = None
        request.data['pocdeduction'] = request.data.get('pocdeduction', 'false')
        request.data['ringfence'] = request.data.get('ringfence', 'false')
        request.data['exclude_rev'] = request.data.get('exclude_rev', 'false')
        request.data['fk_campaign'] = request.data.get('fk_campaign', None)
        if request.data.get('fk_campaign') == None:
            del request.data['fk_campaign']
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=False)
        self.perform_update(serializer)
        if request.accepted_renderer.format == 'html':
            data = serializer.data
            acc_manager, partuser_obj, user = get_u_details(request)
            c_type = "Commission"
            if request.session.get('c_type'):
                c_type = request.session.get('c_type')
                del request.session['c_type']
            data['c_type'] = c_type
            data['message'] = 'Commission has been updated successfully.'
            if data.get('start_date'):
                data['start_date'] = datetime.strptime(data['start_date'], "%Y-%m-%d").strftime('%d-%m-%Y')
            else:
                data['start_date'] = ''
            if data.get('end_date'):
                data['end_date'] = datetime.strptime(data['end_date'], "%Y-%m-%d").strftime('%d-%m-%Y')
            else:
                data['end_date'] = ''
            data['criteria_list'] = [j[0] for j in Affiliatecommission.CRITERIA]
            data['country_list'] = Country.objects.all().values_list('id', 'countryname')
            data['affiliates_list'] = [[i.id, i.email] for i in Affiliateinfo.objects.filter(account_manager=acc_manager)]
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            data['partner'] = acc_manager.affiliateprogramme.name
            data['partner_obj'] = acc_manager.affiliateprogramme
            if data['fk_campaign']:
                data['fk_campaign'] = int(data['fk_campaign'])
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/affiliatecommission.html')
        return Response(serializer.data)

@csrf_exempt
def search_commission(request):
    try:
        if request.method == 'POST':    
            data = json.loads(request.body)
            acc_manager, partuser_obj, user = get_u_details(request)
            queryset = Affiliatecommission.objects.all()
            # import pdb
            # pdb.set_trace()
            header_list = []
            if data.get('type'):
                if data['type'] == 'default':
                    queryset = queryset.filter(fk_affiliateinfo=None,siteid=None,fk_campaign=None)
                if data['type'] == 'brand':
                    if data.get('brand'):
                        queryset = queryset.filter(fk_affiliateinfo=None,siteid__in=data['brand'],fk_campaign=None)
                        header_list.append({'title': 'Brand'})
                if data['type'] == 'affiliate':
                    if data.get('subaff_id'):
                        queryset = queryset.filter(fk_affiliateinfo=data['subaff_id'],fk_campaign=None)
                        header_list.append({'title': 'Affiliate'})
                if data['type'] == 'affiliateplusbrand':
                    if data.get('subaff_id') and data.get('brand'):
                        queryset = queryset.filter(fk_affiliateinfo=data['subaff_id'],siteid__in=data['brand'],fk_campaign=None)
                        header_list.append({'title': 'Affiliate'})
                        header_list.append({'title': 'Brand'})
                if data['type'] == 'campaign':
                    if data.get('campaign'):
                        queryset = queryset.filter(fk_affiliateinfo=None,siteid=None,fk_campaign=data['campaign'])
                        header_list.append({'title': 'Campaign'})
                if data['type'] == 'campaignplusbrand':
                    if data.get('campaign') and data.get('brand'):
                        queryset = queryset.filter(fk_affiliateinfo=None,siteid__in=data['brand'],fk_campaign=data['campaign'])
                        header_list.append({'title': 'Brand'})
                        header_list.append({'title': 'Campaign'})
            header_list.append({'title': 'Start Date'})
            header_list.append({'title': 'End Date'})
            if data.get('start_date') and data.get('end_date'):
                data['start_date'] = (datetime.strptime(data['start_date'],'%d-%m-%Y')).date()
                data['end_date'] = (datetime.strptime(data['end_date'],'%d-%m-%Y')).date()
                queryset = queryset.filter(Q(start_date__gte=data['start_date'],start_date__lte=data['end_date']) | Q(end_date__gte =data['start_date'],end_date__lte=data['end_date']))
            elif data.get('end_date'):
                data['end_date'] = (datetime.strptime(data['end_date'],'%d-%m-%Y')).date()
                queryset = queryset.filter(end_date=data['end_date'])
            elif data.get('start_date'):
                data['start_date'] = (datetime.strptime(data['start_date'],'%d-%m-%Y')).date()
                queryset = queryset.filter(start_date=data['start_date'])
            if data.get('country'):
                queryset = queryset.filter(country__in=data['country'])
                for i in queryset:
                    print(i.id)
                contry_dict = {}
                checked_ids_c = []
                for i in queryset:
                    if i.id not in checked_ids_c:
                        c = i.country.all()
                        for j in c:
                            if not contry_dict.get(i.id):
                                contry_dict[i.id] = [j.countryname]
                            else:
                                contry_dict[i.id].append(j.countryname)
                        checked_ids_c.append(i.id)
            else:
                contry_dict = []
            header_list.append({'title':'Countries'})
            # if queryset:
            header_list.append({'title':'Commission'})
            header_list.append({'title':'Month To Date'})
            header_list.append({'title':'Set On'})  
            header_list.append({'title':'View/Edit'}) 
            data_list = []
            checked_ids = []
            for row in queryset:
                if row.id not in checked_ids:
                    data = []
                    for col in header_list:
                        if col['title'] == 'Affiliate':
                            data.append(row.fk_affiliateinfo.email)
                        if col['title'] == 'Brand':
                            data.append(row.siteid.name)
                        if col['title'] == 'Campaign':
                            data.append(row.fk_campaign.fk_campaign.name)
                        if col['title'] == 'Start Date':
                            print('here>>>>>>.',row.start_date)
                            data.append((row.start_date).strftime("%d/%m/%Y") if row.start_date else '-')
                        if col['title'] == 'End Date':
                            data.append((row.end_date).strftime("%d/%m/%Y") if row.end_date else '-')
                        if col['title'] == 'Countries':
                            if contry_dict:  
                                country_list = contry_dict.get(row.id)
                                country_number = len(country_list)
                                country_list = str(','.join(country_list))
                                # # country_list.replace("'","")
                                # if country_number > 4:
                                data.append('<a onclick="showCountries('+ str(row.id)+ ')"> '+str(country_number)+' selected  </a>' )
                            else:
                                data.append('-')
                            # else:    
                            # data.append('<p id="'+row.id +'">'+country_list+'</p>')
                        if col['title'] == 'Commission':
                            data.append('REVSHARE: '+str(row.revsharepercent)+ ' + CPA: '+str(row.cpacommissionvalue)+ ' + Referral: '+str(row.referalcommissionvalue))
                        if col['title'] == 'Month To Date':
                            data.append('<i class="fa fa-check applied-yes" aria-hidden="true" style="color:green;font-size: 20px;"></i>' if row.monthToDate else '<i class="fa fa-times applied-no" aria-hidden="true" style="color:red;font-size: 20px;"></i>')
                        if col['title'] == 'Set On':
                            data.append((row.created_at).strftime("%d/%m/%Y %H:%M:%S") if row.created_at else '-')
                        if col['title'] == 'View/Edit':
                            data.append('<a href="/admin/affiliatecommission/'+str(row.id)+'"><button type="button" class="btn btn-info">View/ Edit</button></a>')
                    data_list.append(data)
                checked_ids.append(row.id)
            return HttpResponse(json.dumps({'header_list': header_list, 'data_list': data_list}))
        
    except Exception as e:
        print('exception',e)
        import traceback
        traceback.print_exc()

@csrf_exempt
def search_country(request):
    try:
        if request.method == 'POST':
            print('request body',request.body)
            data =  json.loads(request.body )
            print('date>>>>',data)
            queryset = Affiliatecommission.objects.get(id=int(data['id']))
            country_list = []
            c = queryset.country.all()
            for j in c:
                country_list.append(j.countryname)
            print('country list',country_list)  
            # country_list = str(','.join(country_list))
            return HttpResponse(json.dumps({'country_list': country_list}))
    except Exception as e:
        print('exception',e)
        import traceback
        traceback.print_exc()

def view_commission(request):
    acc_manager, partuser_obj, user = get_u_details(request)
    pid = request.session.get('partnerid')
    if not pid:
        return HttpResponseRedirect(reverse('partner_login'),)
    context_dict = {}
    context_dict['partner'] = acc_manager.affiliateprogramme.name
    context_dict['partner_obj'] = acc_manager.affiliateprogramme
    context_dict['country_list'] = [(str(i.id), str(i.countryname)) for i in Country.objects.all()]
    context_dict['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
    context_dict['campaign_list'] = [[int(i.id), (i.fk_campaign.name).encode('utf-8').strip(), int(i.fk_campaign.siteid.id), int(i.fk_campaign.fk_affiliateinfo_id) if i.fk_campaign.fk_affiliateinfo_id != None else ' '] for i in CampaignTrackerMapping.objects.filter(fk_campaign__account_manager=acc_manager)]
    context_dict['affiliates_list'] = [[i.id, i.email] for i in Affiliateinfo.objects.filter(account_manager=acc_manager)]
    # return Response({'data': data, 'partner': data['partner'],
    #                         'partner_obj':data['partner_obj'],
    #                         'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
    #                         template_name='partner/view-commission.html')
    return render(request, 'partner/view-commission.html', context_dict)

#@staff_member_required
class CamptrackMappViewSet(NonDestructiveModelViewSet):

    """
    Endpoint: /admin/camptrackmapp/

    GET: Typical RESTful Response including Pagination.

    `search` query parameter => ?search=<search text>

    PUT & PATCH are available though no validations
                beyond data types exist on it for now.

    """
    queryset = CampaignTrackerMapping.objects.all()
    serializer_class = CampaignTrackerMappingSerializer
    filter_backends = (filters.SearchFilter,
                       DjangoFilterBackend, filters.OrderingFilter,)
    renderer_classes = (renderers.JSONRenderer, renderers.TemplateHTMLRenderer)

    @user_class_role_management('Campaign Track')
    def list(self, request, *args, **kwargs):
        acc_manager, partuser_obj, user = get_u_details(request)
        queryset = self.filter_queryset(self.get_queryset())
        queryset = queryset.filter(fk_affiliateinfo__account_manager=acc_manager)
        page = True
        if page is not None:
            serializer = self.get_serializer(queryset, many=True)
            if request.accepted_renderer.format == 'html':
                cpage = int(request.GET.get('page')) if request.GET.get('page') else 1
                header_list = [{'title': 'Affiliate'}, {'title':'Campaign'},
                         {'title':'Tracker Id'}]
                
                page_div = ''
                data_list = []
                for i in serializer.data:
                    if not i['fk_affiliateinfo']:
                        continue
                    if not i['fk_campaign']:
                        continue
                    camp_name = Campaign.objects.get(id=i['fk_campaign']).name
                    ac_m = Affiliateinfo.objects.get(id=i['fk_affiliateinfo']).email
                    edit_ele = '<a href="/admin/camptrackmapp/'+str(i['id'])+'"><button class="btn btn-success">View/ Edit</button></a>'
                    del_ele = '<form class="button-form" action="/admin/camptrackmapp/'+str(i['id'])+'" data-method="DELETE"><button class="btn btn-danger">Delete</button></form>' 
                    data_list.append([str(ac_m), str(camp_name), str(i['trackerid'])])
                return Response({'header_list': header_list, 'data_list': data_list, 'pagination_div': page_div,
                                'partner': acc_manager.affiliateprogramme.name,
                                'partner_obj': acc_manager.affiliateprogramme,
                                'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
                           }, template_name='partner/camp_track_map_list.html')
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        # if request.accepted_renderer.format == 'html':
        #     return Response({'data': serializer.data}, template_name='partner/index.html')
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return self.list(request)

    def perform_create(self, serializer):
        serializer.save()

    @user_class_role_management('Campaign Track')
    def retrieve(self, request, pk=None):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        try:
            instance = self.get_object()
            # queryset = get_object_or_404(queryset, pk=pk)
            # queryset = self.filter_queryset(self.get_queryset())
            serializer = self.get_serializer(instance)
        except:
            serializer = self.get_serializer(data={})
            serializer.is_valid()
        # queryset = get_object_or_404(queryset, pk=pk)
        # queryset = self.filter_queryset(self.get_queryset())
        # serializer = self.get_serializer(instance)
        if request.accepted_renderer.format == 'html':
            data = serializer.data
            camp_aff_dict = {}
            for camp in Campaign.objects.all():
                if camp.fk_affiliateinfo.id in camp_aff_dict:
                    camp_aff_dict[int(camp.fk_affiliateinfo.id)].append([int(camp.id), str(camp.name)])
                else:
                    camp_aff_dict[int(camp.fk_affiliateinfo.id)] = [[int(camp.id), str(camp.name)]]
            data['campaign_list'] = camp_aff_dict
            acc_manager, partuser_obj, user = get_u_details(request)
            data['affiliates_list'] = [[i.id, i.email] for i in Affiliateinfo.objects.filter(account_manager=acc_manager)]
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            data['partner'] = acc_manager.affiliateprogramme.name
            data['partner_obj'] = acc_manager.affiliateprogramme
            data['call'] = 'POST' if self.kwargs.get('pk') == 'post' else 'PUT'
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/camp_track_map.html')
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        request.data._mutable = True
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=False)
        self.perform_update(serializer)
        if request.accepted_renderer.format == 'html':
            data = serializer.data
            camp_aff_dict = {}
            for camp in Campaign.objects.all():
                if camp.fk_affiliateinfo.id in camp_aff_dict:
                    camp_aff_dict[int(camp.fk_affiliateinfo.id)].append([int(camp.id), str(camp.name)])
                else:
                    camp_aff_dict[int(camp.fk_affiliateinfo.id)] = [[int(camp.id), str(camp.name)]]
            data['campaign_list'] = camp_aff_dict
            acc_manager, partuser_obj, user = get_u_details(request)
            data['affiliates_list'] = [[i.id, i.email] for i in Affiliateinfo.objects.filter(account_manager=acc_manager)]
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            data['partner'] = acc_manager.affiliateprogramme.name
            data['partner_obj'] = acc_manager.affiliateprogramme
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/camp_track_map.html')
        return Response(serializer.data)

def get_brand_order(acc_manager):
    """
    Returns query set in order according to site order table
    """
    queryset = acc_manager.siteid.filter(siteorder__accountmanager_id=acc_manager)
    return queryset.order_by('siteorder__order_id')
     

class BrandViewSet(NonDestructiveModelViewSet):

    """
    Endpoint: /admin/brand/

    GET: Typical RESTful Response including Pagination.

    `search` query parameter => ?search=<search text>

    PUT & PATCH are available though no validations
                beyond data types exist on it for now.

    """
    # pagination_class = None
    queryset = Site.objects.all()
    serializer_class = SiteSerializer
    filter_backends = (filters.SearchFilter,
                       DjangoFilterBackend, filters.OrderingFilter,)
    parser_classes = (MultiPartParser, FormParser,)

    renderer_classes = (renderers.JSONRenderer, renderers.TemplateHTMLRenderer)

    # def paginate_queryset(self, queryset, view=None):
    #     # couldn't find a better solution
    #     return None

    @user_class_role_management('Brand')
    def list(self, request, *args, **kwargs):
        acc_manager, partuser_obj, user = get_u_details(request)
        queryset = self.filter_queryset(self.get_queryset())
        queryset = get_brand_order(acc_manager)
        # page = True
        media_type_dict = {j[0]:j[1] for j in Campaign.MEDIA_TYPE}
        # if page is not None:
        serializer = self.get_serializer(queryset, many=True)
        if request.accepted_renderer.format == 'html':
            # cpage = int(request.GET.get('page')) if request.GET.get('page') else 1
            header_list = [{'title': 'Brand'}, 
                    {'title':'Commission level'},
                     {'title':'Commission rate'},
                     {'title':'Custom'}, {'title':'View'}
                     ]
            
            page_div = ''
            data_list = []
            for i in serializer.data:
                rate_d = ''
                level = 'Default'
                div_r = '<span class="red-dot" style="width:10px; height:10px; border-radius:30px; display: inline-block; text-align: center; color:red; background:red;"></span>'
                camp_c_obj = Affiliatecommission.objects.filter(siteid=int(i['id']), 
                            fk_affiliateinfo__isnull=True, fk_campaign__isnull=True,
                            account_manager__isnull=True)
                if camp_c_obj:
                    camp = camp_c_obj[0]
                    level = 'Brand'
                    div_r = '<span class="green-dot" style="width:10px; height:10px; border-radius:30px; display: inline-block; text-align: center; color:green; background:green;"></span>'
                    curren = Currency.objects.filter(siteid=camp.siteid)
                    if curren:
                        curren = curren[0].currency
                    else:
                        curren = ''
                    rate_d = 'REVSHARE: '+str(camp.revsharepercent)+ ' + CPA: '+str(camp.cpacommissionvalue)
                    comm_page = '<a href="/admin/commissions-brands/'+str(camp.id)+'"><button class="btn btn-success">Commission</button></a>'
                else:
                    camp_c_obj = Affiliatecommission.objects.filter(account_manager=acc_manager,
                                    )
                    if camp_c_obj:
                        camp = camp_c_obj[0]
                        rate_d = 'REVSHARE: '+str(camp.revsharepercent)+ ' + CPA: '+ str(camp.cpacommissionvalue)
                    comm_page = '<a href="/admin/commissions-brands/c'+str(i['id'])+'"><button class="btn btn-success">Commission</button></a>'
                data_list.append([str(i['name']), level, rate_d,
                    div_r, comm_page])
            return Response({'header_list': header_list, 
                'data_list': data_list,
                'partner': acc_manager.affiliateprogramme.name,
                'partner_obj': acc_manager.affiliateprogramme,
                'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
                }, template_name='partner/brand_list.html')
        return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        # if request.accepted_renderer.format == 'html':
        #     return Response({'data': serializer.data}, template_name='partner/index.html')
        return Response(serializer.data)


def selectlp(request):
    resp_dict = {}
    try:
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner'),)
        camp_id = request.GET.get('cid')
        lid = request.GET.get('lpid')
        camp_obj_or_msg = Campaign.objects.get(id=int(camp_id))
        if lid:
            lpageobj = Lpages.objects.get(id=lid)
            lpagelogobj, created = LandingPagelogs.objects.get_or_create(fk_campaign=camp_obj_or_msg, fk_lpage=lpageobj)
            lpagelogobj.save()
        else:
            lpagelogobj = LandingPagelogs.objects.filter(fk_campaign=camp_obj_or_msg)
            if lpagelogobj:
                lpagelogobj.delete()
        resp_dict['status'] = 'Success'
    except Exception as e:
        resp_dict['error_message'] = str(e)
        resp_dict['status'] = 'Error'
        return HttpResponse(json.dumps(resp_dict, indent=4))
    return HttpResponse(json.dumps(resp_dict, indent=4))

# @staff_member_required
class CampaignViewSet(viewsets.ModelViewSet):

    """
    Endpoint: /admin/campaign/

    GET: Typical RESTful Response including Pagination.

    `search` query parameter => ?search=<search text>

    PUT & PATCH are available though no validations
                beyond data types exist on it for now.

    """
    # pagination_class = None
    queryset = Campaign.objects.all()
    serializer_class = CampaignSerializer
    filter_backends = (filters.SearchFilter,
                       DjangoFilterBackend, filters.OrderingFilter,)
    parser_classes = (MultiPartParser, FormParser,)

    renderer_classes = (renderers.JSONRenderer, renderers.TemplateHTMLRenderer)

    # def paginate_queryset(self, queryset, view=None):
    #     # couldn't find a better solution
    #     return None

    @user_class_role_management('Campaign')
    def list(self, request, *args, **kwargs):
        acc_manager, partuser_obj, user = get_u_details(request)
        filt_er = request.GET.get('aff_id', '')
        global_ = request.GET.get('global', '')
        siteid = request.GET.getlist('siteid', '')
        queryset = self.filter_queryset(self.get_queryset())
        f_queryset = []
        g_queryset = []
        # queryset = queryset.filter(fk_affiliateinfo__account_manager=acc_manager)
        affiliates_obj = Affiliateinfo.objects.filter(
                account_manager=acc_manager).exclude(username=user.username)
        sites_obj = get_brand_order(acc_manager)
        if filt_er:
            affiliate_obj = affiliates_obj.get(id=filt_er)
            if siteid:
                sites_obj = Site.objects.filter(id__in=siteid)
            else:
                sites_obj = affiliate_obj.siteid.all()
            # if global_:
            g_queryset = queryset.filter(
                    Q(fk_affiliateinfo__isnull=True) &
                    Q(account_manager=acc_manager) &
                    Q(siteid__in=sites_obj)).order_by('-created_timestamp')
            # if not global_:
            f_queryset = queryset.filter(Q(fk_affiliateinfo=affiliate_obj) &
                        Q(siteid__in=sites_obj)).order_by('-created_timestamp')
            f_queryset = f_queryset.filter(fk_affiliateinfo__account_manager=acc_manager)
            querysets = [g_queryset, f_queryset]
            # queryset = queryset.filter(Q(Q(fk_affiliateinfo=affiliate_obj) | 
            #              Q(account_manager=acc_manager)) &
            #             Q(siteid__in=sites_obj)).order_by('-created_timestamp')
            # page = True
            media_type_dict = {j[0]:j[1] for j in Campaign.MEDIA_TYPE}
            # if page is not None:
        else:
            querysets = []
        data_list = []
        header_list = [{'title': 'Name', "width":"200px"}, 
                 {'title':'Brand', "width":"200px"}, 
                 {'title':'Type', "width":"160px"}, 
                 {'title':'Added on', "width":"190px"},
                 { "title": "Language", "width":"160px"},
                 {'title':'Landing page', "width":"180px"},
                 { "title": "Code", "width":"160px"},
                 {'title':'Commission', "width":"180px"}, 
                 {'title':'Delete', "width":"160px"}
                 ]
        lang_selector = {'English':'en',
                    'Spanish':'sv',
                    'Finnish' : 'fi',
                    'German':'de'}
        for queryset in querysets:
            serializer = self.get_serializer(queryset, many=True)
            for i in serializer.data:
                imagelink = ''
                ac_m = ''
                lang = None
                camp_obj = Campaign.objects.get(id=i['id'])
                if i['media_type'] == 'V':
                    videologobj = Videologs.objects.filter(fk_campaign=camp_obj)
                    if videologobj:
                        imagelink = videologobj[0].fk_video.video.url if videologobj[0].fk_video.video else ''
                        lang = videologobj[0].fk_video.language
                elif i['media_type'] in ['SS', 'P', 'B']:
                    bannerlogobj = Bannerlogs.objects.filter(fk_campaign=camp_obj)
                    if bannerlogobj:
                        imagelink = bannerlogobj[0].fk_bannerimages.banner.url if bannerlogobj[0].fk_bannerimages.banner else ''
                        lang = bannerlogobj[0].fk_bannerimages.language
                elif i['media_type'] == 'V':
                    lpagelogobj = LandingPagelogs.objects.filter(fk_campaign=camp_obj)
                    if lpagelogobj:
                        lang = lpagelogobj[0].fk_lpage.language
                lpagelogobj = LandingPagelogs.objects.filter(fk_campaign=camp_obj)
                if lang:
                    lang = lang_selector[lang]
                    url_builder = '<button class="btn btn-purple" bannerlink="'+str(imagelink)+'" mediatype="'+str(i['media_type'])+'" onclick="getcode(this)" \
                                hrefs="/affiliate/gettrackerlink/?dlang='+lang+'&camp_id='+str(
                            camp_obj.id)+'&aff_id='+str(affiliate_obj.id) +'" target="_blank">Get Code</button>'
                else:
                    url_builder = '<button class="btn btn-purple" bannerlink="'+str(imagelink)+'" mediatype="'+str(i['media_type'])+'" onclick="getcode(this)" \
                                hrefs="/affiliate/gettrackerlink/?camp_id='+str(
                            camp_obj.id)+'&aff_id='+str(affiliate_obj.id) +'" target="_blank">Get Code</button>'
                camp_track_obj = CampaignTrackerMapping.objects.filter(fk_campaign=int(i['id']), fk_affiliateinfo=affiliate_obj)
                camp_c_obj = None
                if camp_track_obj:
                    camp_c_obj = Affiliatecommission.objects.filter(fk_campaign=camp_track_obj[0])
                if camp_c_obj:
                    comm_page = '<a href="/admin/commissions-campaigns/'+str(camp_c_obj[0].id)+'"><button class="btn btn-success">Commission</button></a>'
                else:
                    comm_page = '<a href="/admin/commissions-campaigns/c'+str(i['id'])+'a'+str(affiliate_obj.id)+'"><button class="btn btn-success">Commission</button></a>'
                del_camp = '<button class="btn btn-purple" onclick="deletecampaign(this)" hrefs="/admin/campaign/'+str(i['id'])+'">Delete</button>'
                site_name = Site.objects.get(id=i['siteid']).name
                if i.get('fk_affiliateinfo'):
                    ac_m = Affiliateinfo.objects.get(id=i['fk_affiliateinfo']).email
                elif i.get('media_type') == 'T':
                    ac_m = site_name
                else:
                    ac_m = i['name'].encode('utf-8')
                select_lp = ''
                lp_objs = Lpages.objects.filter(siteid=camp_obj.siteid)
                if lp_objs:
                    select_lp = '<select class="selectpicker" name="status" onchange="savelp(this)" style="display:block !important" hrefs="/admin/selectlp/?cid='+str(i['id'])+'&aid='+str(affiliate_obj.id)+'">'
                    select_lp += '<option value="">Landing Page</option>'
                    for lp in lp_objs:
                        title = str(lp.title)[:12] + '..' if len(lp.title) > 12 else str(lp.title)
                        if lpagelogobj and lp == lpagelogobj[0].fk_lpage:
                            select_lp += '<option value="'+str(lp.id)+'" selected>'+title+'</option>'
                        else:
                            select_lp += '<option value="'+str(lp.id)+'">'+title+'</option>'
                    select_lp += '</select>'
                lang_select = '<select class="selectpicker" name="status" style="display:block !important" >\
                            <option value="">Default</option><option value="en">English</option><option value="de">German</option>\
                            <option value="sv">Spanish</option><option value="fi">Finnish</option></select>'
                # edit_ele = '<a href="/admin/campaign/'+str(i['id'])+'"><button class="btn btn-success">View/ Edit</button></a>'
                del_ele = '<form class="button-form" action="/admin/campaign/'+str(i['id'])+'" data-method="DELETE"><button class="btn btn-danger">Delete</button></form>' 
                data_list.append([i['name'].encode('utf-8'), site_name.encode('utf-8'), 
                                media_type_dict.get(str(i['media_type']),''), 
                                str(i['created_timestamp']).split('T')[0], lang_select,
                                select_lp, url_builder, comm_page, del_camp])
        msg = ''
        if request.session.get('deleted'):
            msg = 'This campaign has been successfully deleted.'
            if request.session.get('deleted'):
                del request.session['deleted']
        if request.accepted_renderer.format == 'html':
            return Response({'header_list': header_list, 
                'data_list': data_list,
                'aff_id':filt_er,
                'global': global_,
                'message':msg,
                'siteid':[str(i.id) for i in sites_obj],
                'sites_list' : [[i.id, i.name] for i in get_brand_order(acc_manager)],
                "aff_list": [{'id':i.id, 'username': i.email.replace('vcommsubaff', '')} for i in affiliates_obj],
                'partner': acc_manager.affiliateprogramme.name,
                'partner_obj': acc_manager.affiliateprogramme,
                'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
                }, template_name='partner/campaign_list.html')
        else:
            return Response({'header_list': header_list, 
                'data_list': data_list,
                'aff_id':filt_er,
                'status':'success'})

        serializer = self.get_serializer(queryset, many=True)
        # if request.accepted_renderer.format == 'html':
        #     return Response({'data': serializer.data}, template_name='partner/index.html')
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        request.session['created'] = True
        id_ = serializer.data['id']
        return HttpResponseRedirect("/admin/campaign/"+str(id_))

    def perform_create(self, serializer):
        serializer.save()

    def parse_serialized_data(self, data, acc_manager):
        site_media_dict = {}
        for media_c in MediaCategory.objects.all():
            if media_c.siteid.id in site_media_dict:
                site_media_dict[int(media_c.siteid.id)].append([int(media_c.id), str(media_c.category_name)])
            else:
                site_media_dict[int(media_c.siteid.id)] = [[int(media_c.id), str(media_c.category_name)]]
        data['partner'] = acc_manager.affiliateprogramme.name
        data['partner_obj'] = acc_manager.affiliateprogramme
        data['media_list'] = [[j[0], j[1]] for j in Campaign.MEDIA_TYPE]
        data['affiliates_list'] = [[i.id, i.email] for i in Affiliateinfo.objects.filter(account_manager=acc_manager)]
        data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
        data['mediacategory_list'] = site_media_dict
        data['network_list'] = [[i.networkid, i.networkname] for i in acc_manager.affiliateprogramme.networkid.all()]
        return data

    @user_class_role_management('Campaign')
    def retrieve(self, request, pk=None):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        try:
            instance = self.get_object()
            # queryset = get_object_or_404(queryset, pk=pk)
            # queryset = self.filter_queryset(self.get_queryset())
            serializer = self.get_serializer(instance)
        except:
            serializer = self.get_serializer(data={})
            serializer.is_valid()
        # instance = self.get_object()
        # # queryset = get_object_or_404(queryset, pk=pk)
        # # queryset = self.filter_queryset(self.get_queryset())
        # serializer = self.get_serializer(instance)
        if request.accepted_renderer.format == 'html':
            data = serializer.data
            acc_manager, partuser_obj, user = get_u_details(request)
            data = self.parse_serialized_data(data, acc_manager)
            msg = ''
            if request.session.get('created') or request.session.get('deleted'):
                crt = request.session.get('created')
                deleted = request.session.get('deleted')
                if crt:
                    msg = 'Campaign added successfully.'
                elif deleted:
                    msg = 'The campaign has been successfully deleted.'
                else:
                    msg = 'There is an error while adding campaign.'
                if crt:
                    del request.session['created']
                if deleted:
                    del request.session['deleted']
            data['message'] = msg
            data['call'] = 'POST' if self.kwargs.get('pk') == 'post' else 'PUT'
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/campaign.html')
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        request.data._mutable = True
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=False)
        self.perform_update(serializer)
        if request.accepted_renderer.format == 'html':
            data = serializer.data
            acc_manager, partuser_obj, user = get_u_details(request)
            data = self.parse_serialized_data(data, acc_manager)
            data['partner'] = acc_manager.affiliateprogramme.name
            data['partner_obj'] = acc_manager.affiliateprogramme
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/campaign.html')
        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        request.session['deleted'] = True
        return self.list(request)

    def perform_destroy(self, instance):
        instance.delete()

# @staff_member_required
class CurrencyViewSet(NonDestructiveModelViewSet):

    """
    Endpoint: /admin/currency/

    GET: Typical RESTful Response including Pagination.

    `search` query parameter => ?search=<search text>

    PUT & PATCH are available though no validations
                beyond data types exist on it for now.

    """
    queryset = Currency.objects.all()
    serializer_class = CurrencySerializer
    filter_backends = (filters.SearchFilter,
                       DjangoFilterBackend, filters.OrderingFilter,)
    parser_classes = (MultiPartParser, FormParser,)
    renderer_classes = (renderers.JSONRenderer, renderers.TemplateHTMLRenderer)


    def list(self, request, *args, **kwargs):
        acc_manager, partuser_obj, user = get_u_details(request)
        queryset = self.filter_queryset(self.get_queryset())
        page = True
        if page is not None:
            serializer = self.get_serializer(queryset, many=True)
            if request.accepted_renderer.format == 'html':
                cpage = int(request.GET.get('page')) if request.GET.get('page') else 1
                header_list = [{'title': 'Currency Symbol'}, {'title':'Site'}, {'title': 'View/ Edit'}]
                
                page_div = ''
                data_list = []
                for i in serializer.data:
                    if not i['siteid']:
                        continue
                    site_name = Site.objects.get(id=i['siteid']).name
                    edit_ele = '<a href="/admin/currency/'+str(i['id'])+'"><button class="btn btn-success">View/ Edit</button></a>'
                    del_ele = '<form class="button-form" action="/admin/currency/'+str(i['id'])+'" data-method="DELETE"><button class="btn btn-danger">Delete</button></form>' 
                    data_list.append([str(i['currency']), str(site_name),
                                     edit_ele])
                return Response({'header_list': header_list, 'data_list': data_list, 'pagination_div': page_div,
                                'partner': acc_manager.affiliateprogramme.name,
                                'partner_obj': acc_manager.affiliateprogramme,
                                'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
                           }, template_name='partner/currency_list.html')
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        # if request.accepted_renderer.format == 'html':
        #     return Response({'data': serializer.data}, template_name='partner/index.html')
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return self.list(request)

    def perform_create(self, serializer):
        serializer.save()
    def retrieve(self, request, pk=None):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        try:
            instance = self.get_object()
            # queryset = get_object_or_404(queryset, pk=pk)
            # queryset = self.filter_queryset(self.get_queryset())
            serializer = self.get_serializer(instance)
        except:
            serializer = self.get_serializer(data={})
            serializer.is_valid()
        # queryset = get_object_or_404(queryset, pk=pk)
        # queryset = self.filter_queryset(self.get_queryset())
        # serializer = self.get_serializer(instance)
        if request.accepted_renderer.format == 'html':
            data = serializer.data
            data['sites_list'] = [[i.id, i.name] for i in Site.objects.all()]
            acc_manager, partuser_obj, user = get_u_details(request)
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            data['partner'] = acc_manager.affiliateprogramme.name
            data['partner_obj'] = acc_manager.affiliateprogramme
            data['call'] = 'POST' if self.kwargs.get('pk') == 'post' else 'PUT'
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/currency.html')
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        request.data._mutable = True
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=False)
        self.perform_update(serializer)
        if request.accepted_renderer.format == 'html':
            data = serializer.data
            acc_manager, partuser_obj, user = get_u_details(request)
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            data['partner'] = acc_manager.affiliateprogramme.name
            data['partner_obj'] = acc_manager.affiliateprogramme
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/currency.html')
        return Response(serializer.data)



# @staff_member_required
class BannerViewSet(viewsets.ModelViewSet):

    """
    Endpoint: /admin/banner/

    GET: Typical RESTful Response including Pagination.

    `search` query parameter => ?search=<search text>

    PUT & PATCH are available though no validations
                beyond data types exist on it for now.

    """
    queryset = BannerImages.objects.filter(media_type='B')
    serializer_class = BannerImagesSerializer
    filter_backends = (filters.SearchFilter,
                       DjangoFilterBackend, filters.OrderingFilter,)
    parser_classes = (MultiPartParser, FormParser,)
    renderer_classes = (renderers.JSONRenderer, renderers.TemplateHTMLRenderer)

    @user_class_role_management('Banners')
    def list(self, request, *args, **kwargs):
        acc_manager, partuser_obj, user = get_u_details(request)
        queryset = self.filter_queryset(self.get_queryset())
        queryset = queryset.filter(user=acc_manager.user)
        page = True
        if page is not None:
            serializer = self.get_serializer(queryset, many=True)
            if request.accepted_renderer.format == 'html':
                media_type_dict = dict(BannerImages.MEDIA_TYPE)
                cpage = int(request.GET.get('page')) if request.GET.get('page') else 1
                header_list = [{'title': 'ID'}, {'title':'Brand'}, 
                        {'title': 'Title'}, {'title':'Media Type'}, {'title':'Size'},
                          {'title':'Language'}, {'title':'Account Manager'}, 
                          {'title': 'View/ Edit'}]
                page_div = ''
                data_list = []
                for i in serializer.data:
                    if not i.get('siteid'):
                        continue
                    if not i.get('user'):
                        ac_m = ''
                    else:
                        ac_m = User.objects.get(id=i['user']).username
                    site_name = Site.objects.get(id=i['siteid']).name
                    edit_ele = '<a href="/admin/banners/'+str(i['id'])+'"><button class="btn btn-success">View/ Edit</button></a>'
                    del_ele = '<form class="button-form" action="/admin/banners/'+str(i['id'])+'" data-method="DELETE"><button class="btn btn-danger">Delete</button></form>' 
                    data_list.append([str(i['id']), site_name.encode('utf-8'), 
                        i['title'].encode('utf-8'), 
                        media_type_dict[str(i['media_type'])], '%sx%s'%(i['width'],i['height']),
                        str(i['language']), str(ac_m),
                        edit_ele])
                msg = ''
                if request.session.get('deleted'):
                    msg = 'Banner has been successfully deleted.'
                    if request.session.get('deleted'):
                        del request.session['deleted']
                return Response({'header_list': header_list, 
                            'type':'Banners',
                            'message':msg,
                            'call_type': 'banners',
                            'data_list': data_list, 'pagination_div': page_div,
                            'partner': acc_manager.affiliateprogramme.name,
                            'partner_obj': acc_manager.affiliateprogramme,
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
                           }, template_name='partner/bimages_list.html')
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        # if request.accepted_renderer.format == 'html':
        #     return Response({'data': serializer.data}, template_name='partner/index.html')
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        acc_manager, partuser_obj, user = get_u_details(request)
        pid = request.session.get('partnerid')
        request.data._mutable = True
        request.data['user'] = acc_manager.user.id
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        site_obj = Site.objects.get(id=request.data['siteid'])
        network_obj = NetworkAndSiteMap.objects.get(siteid=site_obj)
        camp_obj, created = Campaign.objects.get_or_create(
            name='%s - %sx%s'%(request.data['title'], request.data['width'],request.data['height']),
            siteid=site_obj,
            account_manager=acc_manager,
            networkid=network_obj.networkid, media_type=request.data['media_type'])
        camp_obj.save()
        bannerobj = BannerImages.objects.get(id=serializer.data['id'])
        bannerlogobj = Bannerlogs.objects.create(
            fk_campaign=camp_obj, fk_bannerimages=bannerobj)
        bannerlogobj.save()
        request.session['created'] = True
        id_ = serializer.data['id']
        return HttpResponseRedirect("/admin/banners/"+str(id_))

    def perform_create(self, serializer):
        serializer.save()

    @user_class_role_management('Banners')
    def retrieve(self, request, pk=None):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        try:
            instance = self.get_object()
            # queryset = get_object_or_404(queryset, pk=pk)
            # queryset = self.filter_queryset(self.get_queryset())
            serializer = self.get_serializer(instance)
        except:
            serializer = self.get_serializer(data={})
            serializer.is_valid()
        # queryset = get_object_or_404(queryset, pk=pk)
        # queryset = self.filter_queryset(self.get_queryset())
        # serializer = self.get_serializer(instance)
        if request.accepted_renderer.format == 'html':
            data = serializer.data
            
            acc_manager, partuser_obj, user = get_u_details(request)
            msg = ''
            if request.session.get('created') or request.session.get('deleted'):
                crt = request.session.get('created')
                deleted = request.session.get('deleted')
                if crt:
                    msg = 'Banner has been successfully submitted.'
                elif deleted:
                    msg = 'Banner has been successfully deleted.'
                else:
                    msg = 'There is an error while adding banner.'
                if crt:
                    del request.session['created']
                if deleted:
                    del request.session['deleted']
            data['message'] = msg
            if data and data.get('link'):
                lpage = Lpages.objects.filter(link=data['link'])
                if lpage:
                    data['lp_link'] = lpage[0].title
            lpages = Lpages.objects.filter(siteid__in=get_brand_order(acc_manager))
            lp_banners = {}
            for i in lpages:
                link = i.link.encode('utf-8').strip()
                title = i.title.encode('utf-8').strip()
                if lp_banners.get(int(i.siteid.id)):
                    lp_banners[int(i.siteid.id)].append([link,title])
                else:
                    lp_banners[int(i.siteid.id)] = [[link,title]]
            data['lp_links'] = lp_banners
            data['partner'] = acc_manager.affiliateprogramme.name
            data['language_list'] = ['English', 'German', 'Finnish']
            data['partner_obj'] = acc_manager.affiliateprogramme
            data['media_list'] = dict(BannerImages.MEDIA_TYPE)
            data['affiliates_list'] = [[i.id, i.email] for i in Affiliateinfo.objects.filter(account_manager=acc_manager)]
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            data['call'] = 'POST' if self.kwargs.get('pk') == 'post' else 'PUT'
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/bimages.html')
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        request.data._mutable = True
        acc_manager, partuser_obj, user = get_u_details(request)
        request.data['user'] = acc_manager.user.id
        if request.data.get('banner', '') == '':
            del request.data['banner']
            del request.data['height']
            del request.data['width']
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=False)
        self.perform_update(serializer)
        if request.accepted_renderer.format == 'html':
            data = serializer.data
            data['media_list'] = dict(BannerImages.MEDIA_TYPE)
            if data and data.get('link'):
                lpage = Lpages.objects.filter(link=data['link'])
                if lpage:
                    data['lp_link'] = lpage[0].title
            acc_manager, partuser_obj, user = get_u_details(request)
            lpages = Lpages.objects.filter(siteid__in=get_brand_order(acc_manager))
            lp_banners = {}
            for i in lpages:
                link = i.link.encode('utf-8').strip()
                title = i.title.encode('utf-8').strip()
                if lp_banners.get(int(i.siteid.id)):
                    lp_banners[int(i.siteid.id)].append([link,title])
                else:
                    lp_banners[int(i.siteid.id)] = [[link,title]]
            data['lp_links'] = lp_banners
            data['message'] = 'Updated banner details.'
            data['affiliates_list'] = [[i.id, i.email] for i in Affiliateinfo.objects.filter(account_manager=acc_manager)]
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            data['partner'] = acc_manager.affiliateprogramme.name
            data['message'] = 'Banner has been successfully edited.'
            data['partner_obj'] = acc_manager.affiliateprogramme
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/bimages.html')
        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        # b_image = Bannerlogs.objects.filter(fk_bannerimages=instance)
        # if b_image:
        #     b_image[0].fk_campaign.delete()
        #     b_image[0].delete()
        self.perform_destroy(instance)
        request.session['deleted'] = True
        return self.list(request)

    def perform_destroy(self, instance):
        instance.delete()


class TextLinkViewSet(viewsets.ModelViewSet):

    """
    Endpoint: /root/text link/

    GET: Typical RESTful Response including Pagination.

    `search` query parameter => ?search=<search text>

    PUT & PATCH are available though no validations
                beyond data types exist on it for now.

    """
    queryset = Tlinks.objects.all()
    serializer_class = TlinksSerializer
    filter_backends = (filters.SearchFilter,
                       DjangoFilterBackend, filters.OrderingFilter,)
    parser_classes = (MultiPartParser, FormParser,)
    renderer_classes = (renderers.JSONRenderer, renderers.TemplateHTMLRenderer)

    @user_class_role_management('Text Links')
    def list(self, request, *args, **kwargs):
        acc_manager, partuser_obj, user = get_u_details(request)
        queryset = self.filter_queryset(self.get_queryset())
        queryset = queryset.filter(user=acc_manager.user)
        page = True
        if page is not None:
            serializer = self.get_serializer(queryset, many=True)
            if request.accepted_renderer.format == 'html':
                cpage = int(request.GET.get('page')) if request.GET.get('page') else 1
                header_list = [{'title': 'ID'},{'title': 'Title'}, 
                         {'title':'Site'}, {'title':'Account Manager'}, {'title': 'View/ Edit'}]
                
                page_div = ''
                data_list = []
                for i in serializer.data:
                    if not i.get('siteid'):
                        continue
                    ac_m = ''
                    if i.get('user'):
                        ac_m = User.objects.get(id=i['user']).username
                    site_name = Site.objects.get(id=i['siteid']).name
                    edit_ele = '<a href="/admin/textlinks/'+str(i['id'])+'"><button class="btn btn-success">View/ Edit</button></a>'
                    del_ele = '<form class="button-form" action="/admin/textlinks/'+str(i['id'])+'" data-method="DELETE"><button class="btn btn-danger">Delete</button></form>' 
                    data_list.append([str(i['id']), i['title'].encode('utf-8'), str(site_name), str(ac_m),
                                     edit_ele])
                msg = ''
                if request.session.get('deleted'):
                    msg = 'The Text Link has been successfully deleted.'
                    if request.session.get('deleted'):
                        del request.session['deleted']
                return Response({'header_list': header_list, 
                                'message':msg,
                                'data_list': data_list, 'pagination_div': page_div,
                                'partner': acc_manager.affiliateprogramme.name,
                                'partner_obj': acc_manager.affiliateprogramme,
                                'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
                           }, template_name='partner/tlinks_list.html')
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        # if request.accepted_renderer.format == 'html':
        #     return Response({'data': serializer.data}, template_name='partner/index.html')
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        pid = request.session.get('partnerid')
        request.data._mutable = True
        acc_manager, partuser_obj, user = get_u_details(request)
        request.data['user'] = acc_manager.user.id
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        site_obj = Site.objects.get(id=request.data['siteid'])
        network_obj = NetworkAndSiteMap.objects.get(siteid=site_obj)
        camp_obj, created = Campaign.objects.get_or_create(
            name=request.data['title'], siteid=site_obj,
            account_manager=acc_manager,
            networkid=network_obj.networkid, media_type='T')
        camp_obj.save()
        tobj = Tlinks.objects.get(id=serializer.data['id'])
        logobj = Tlinkslogs.objects.create(
            fk_campaign=camp_obj, fk_tlink=tobj)
        logobj.save()
        request.session['created'] = True
        id_ = serializer.data['id']
        return HttpResponseRedirect("/admin/textlinks/"+str(id_))

    def perform_create(self, serializer):
        serializer.save()

    @user_class_role_management('Text Links')
    def retrieve(self, request, pk=None):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        try:
            instance = self.get_object()
            # queryset = get_object_or_404(queryset, pk=pk)
            # queryset = self.filter_queryset(self.get_queryset())
            serializer = self.get_serializer(instance)
        except:
            serializer = self.get_serializer(data={})
            serializer.is_valid()
        # queryset = get_object_or_404(queryset, pk=pk)
        # queryset = self.filter_queryset(self.get_queryset())
        # serializer = self.get_serializer(instance)
        if request.accepted_renderer.format == 'html':
            try:
                data = serializer.data
            except:
                data = {}
            acc_manager, partuser_obj, user = get_u_details(request)
            msg = ''
            if request.session.get('created') or request.session.get('deleted'):
                crt = request.session.get('created')
                deleted = request.session.get('deleted')
                if crt:
                    msg = 'Text link added successfully.'
                elif deleted:
                    msg = 'The Text link has been successfully deleted.'
                else:
                    msg = 'There is an error while adding Text link.'
                if crt:
                    del request.session['created']
                if deleted:
                    del request.session['deleted']
            data['message'] = msg
            data['affiliates_list'] = [[i.id, i.email] for i in Affiliateinfo.objects.filter(account_manager=acc_manager)]
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            data['partner'] = acc_manager.affiliateprogramme.name
            data['language_list'] = ['English', 'German', 'Finnish']
            data['partner_obj'] = acc_manager.affiliateprogramme
            data['call'] = 'POST' if self.kwargs.get('pk') == 'post' else 'PUT'
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/tlinks.html')
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        request.data._mutable = True
        acc_manager, partuser_obj, user = get_u_details(request)
        request.data['user'] = acc_manager.user.id
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=False)
        self.perform_update(serializer)
        if request.accepted_renderer.format == 'html':
            data = serializer.data
            acc_manager, partuser_obj, user = get_u_details(request)
            data['message'] = 'Updated Text link details.'
            data['affiliates_list'] = [[i.id, i.email] for i in Affiliateinfo.objects.filter(account_manager=acc_manager)]
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            data['partner'] = acc_manager.affiliateprogramme.name
            data['message'] = 'Text link has been successfully edited.'
            data['partner_obj'] = acc_manager.affiliateprogramme
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/tlinks.html')
        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        # t_logs = Tlinkslogs.objects.filter(fk_lpage=instance)
        # if t_logs:
        #     t_logs[0].fk_campaign.delete()
        #     t_logs[0].delete()
        self.perform_destroy(instance)
        request.session['deleted'] = True
        return self.list(request)

    def perform_destroy(self, instance):
        instance.delete()


# @staff_member_required
class LpagesViewSet(viewsets.ModelViewSet):

    """
    Endpoint: /root/farmland/

    GET: Typical RESTful Response including Pagination.

    `search` query parameter => ?search=<search text>

    PUT & PATCH are available though no validations
                beyond data types exist on it for now.

    """
    queryset = Lpages.objects.all()
    serializer_class = LpagesSerializer
    filter_backends = (filters.SearchFilter,
                       DjangoFilterBackend, filters.OrderingFilter,)
    parser_classes = (MultiPartParser, FormParser,)
    renderer_classes = (renderers.JSONRenderer, renderers.TemplateHTMLRenderer)

    @user_class_role_management('Landing Pages')
    def list(self, request, *args, **kwargs):
        acc_manager, partuser_obj, user = get_u_details(request)
        queryset = self.filter_queryset(self.get_queryset())
        queryset = queryset.filter(user=acc_manager.user)
        page = True
        if page is not None:
            serializer = self.get_serializer(queryset, many=True)
            if request.accepted_renderer.format == 'html':
                cpage = int(request.GET.get('page')) if request.GET.get('page') else 1
                header_list = [{'title': 'ID'},{'title': 'Title'}, 
                         {'title':'Site'}, {'title':'Account Manager'}, {'title': 'View/ Edit'}]
                
                page_div = ''
                data_list = []
                for i in serializer.data:
                    if not i.get('siteid'):
                        continue
                    ac_m = ''
                    if i.get('user'):
                        ac_m = User.objects.get(id=i['user']).username
                    site_name = Site.objects.get(id=i['siteid']).name
                    edit_ele = '<a href="/admin/landing-pages/'+str(i['id'])+'"><button class="btn btn-success">View/ Edit</button></a>'
                    del_ele = '<form class="button-form" action="/admin/landing-pages/'+str(i['id'])+'" data-method="DELETE"><button class="btn btn-danger">Delete</button></form>' 
                    data_list.append([str(i['id']), i['title'].encode('utf-8'), str(site_name), str(ac_m),
                                     edit_ele])
                msg = ''
                if request.session.get('deleted'):
                    msg = 'The landing page has been successfully deleted.'
                    if request.session.get('deleted'):
                        del request.session['deleted']
                return Response({'header_list': header_list, 
                                'message':msg,
                                'data_list': data_list, 'pagination_div': page_div,
                                'partner': acc_manager.affiliateprogramme.name,
                                'partner_obj': acc_manager.affiliateprogramme,
                                'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
                           }, template_name='partner/lpages_list.html')
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        # if request.accepted_renderer.format == 'html':
        #     return Response({'data': serializer.data}, template_name='partner/index.html')
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        pid = request.session.get('partnerid')
        request.data._mutable = True
        acc_manager, partuser_obj, user = get_u_details(request)
        request.data['user'] = acc_manager.user.id
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        site_obj = Site.objects.get(id=request.data['siteid'])
        network_obj = NetworkAndSiteMap.objects.get(siteid=site_obj)
        camp_obj, created = Campaign.objects.get_or_create(
            name=request.data['title'], siteid=site_obj,
            account_manager=acc_manager,
            networkid=network_obj.networkid, media_type='LP')
        camp_obj.save()
        lpageobj = Lpages.objects.get(id=serializer.data['id'])
        lpagelogobj = LandingPagelogs.objects.create(
            fk_campaign=camp_obj, fk_lpage=lpageobj)
        lpagelogobj.save()
        request.session['created'] = True
        id_ = serializer.data['id']
        return HttpResponseRedirect("/admin/landing-pages/"+str(id_))

    def perform_create(self, serializer):
        serializer.save()

    @user_class_role_management('Landing Pages')
    def retrieve(self, request, pk=None):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        try:
            instance = self.get_object()
            # queryset = get_object_or_404(queryset, pk=pk)
            # queryset = self.filter_queryset(self.get_queryset())
            serializer = self.get_serializer(instance)
        except:
            serializer = self.get_serializer(data={})
            serializer.is_valid()
        # queryset = get_object_or_404(queryset, pk=pk)
        # queryset = self.filter_queryset(self.get_queryset())
        # serializer = self.get_serializer(instance)
        if request.accepted_renderer.format == 'html':
            try:
                data = serializer.data
            except:
                data = {}
            acc_manager, partuser_obj, user = get_u_details(request)
            msg = ''
            if request.session.get('created') or request.session.get('deleted'):
                crt = request.session.get('created')
                deleted = request.session.get('deleted')
                if crt:
                    msg = 'Landing page added successfully.'
                elif deleted:
                    msg = 'The landing page has been successfully deleted.'
                else:
                    msg = 'There is an error while adding landing page.'
                if crt:
                    del request.session['created']
                if deleted:
                    del request.session['deleted']
            data['message'] = msg
            data['affiliates_list'] = [[i.id, i.email] for i in Affiliateinfo.objects.filter(account_manager=acc_manager)]
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            data['partner'] = acc_manager.affiliateprogramme.name
            data['language_list'] = ['English', 'German', 'Finnish']
            data['partner_obj'] = acc_manager.affiliateprogramme
            data['call'] = 'POST' if self.kwargs.get('pk') == 'post' else 'PUT'
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/lpages.html')
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        request.data._mutable = True
        acc_manager, partuser_obj, user = get_u_details(request)
        request.data['user'] = acc_manager.user.id
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=False)
        self.perform_update(serializer)
        if request.accepted_renderer.format == 'html':
            data = serializer.data
            acc_manager, partuser_obj, user = get_u_details(request)
            data['message'] = 'Updated Landing-page details.'
            data['affiliates_list'] = [[i.id, i.email] for i in Affiliateinfo.objects.filter(account_manager=acc_manager)]
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            data['partner'] = acc_manager.affiliateprogramme.name
            data['message'] = 'Landing-page has been successfully edited.'
            data['partner_obj'] = acc_manager.affiliateprogramme
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/lpages.html')
        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        # l_image = LandingPagelogs.objects.filter(fk_lpage=instance)
        # if l_image:
        #     l_image[0].fk_campaign.delete()
        #     l_image[0].delete()
        self.perform_destroy(instance)
        request.session['deleted'] = True
        return self.list(request)

    def perform_destroy(self, instance):
        instance.delete()

# @staff_member_required
class MailersViewSet(NonDestructiveModelViewSet):

    """
    Endpoint: /admin/mailers/

    GET: Typical RESTful Response including Pagination.

    `search` query parameter => ?search=<search text>

    PUT & PATCH are available though no validations
                beyond data types exist on it for now.

    """
    queryset = Mailer.objects.all()
    serializer_class = MailerSerializer
    filter_backends = (filters.SearchFilter,
                       DjangoFilterBackend, filters.OrderingFilter,)
    # search_fields = ('farmer_id__name',)
    # filter_fields = ('farmer_id__name',)
    parser_classes = (MultiPartParser, FormParser,)
    renderer_classes = (renderers.JSONRenderer, renderers.TemplateHTMLRenderer)


    def list(self, request, *args, **kwargs):
        acc_manager, partuser_obj, user = get_u_details(request)
        queryset = self.filter_queryset(self.get_queryset())
        queryset = queryset.filter(user=acc_manager.user)
        page = True
        if page is not None:
            serializer = self.get_serializer(queryset, many=True)
            if request.accepted_renderer.format == 'html':
                cpage = int(request.GET.get('page')) if request.GET.get('page') else 1
                header_list = [{'title': 'Title'}, 
                         {'title':'Site'}, {'title':'Account Manager'}, {'title': 'View/ Edit'}]
                
                page_div = ''
                data_list = []
                for i in serializer.data:
                    if not i.get('siteid'):
                        continue
                    ac_m = ''
                    if i.get('user'):
                        ac_m = User.objects.get(id=i['user']).username
                    site_name = Site.objects.get(id=i['siteid']).name
                    edit_ele = '<a href="/admin/mailers/'+str(i['id'])+'"><button class="btn btn-success">View/ Edit</button></a>'
                    del_ele = '<form class="button-form" action="/admin/mailers/'+str(i['id'])+'" data-method="DELETE"><button class="btn btn-danger">Delete</button></form>' 
                    data_list.append([str(i['subject']), str(site_name), str(ac_m),
                                     edit_ele])
                return Response({'header_list': header_list, 'data_list': data_list, 'pagination_div': page_div,
                                'partner': acc_manager.affiliateprogramme.name,
                                'partner_obj': acc_manager.affiliateprogramme,
                                'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
                           }, template_name='partner/mailer_list.html')
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        # if request.accepted_renderer.format == 'html':
        #     return Response({'data': serializer.data}, template_name='partner/index.html')
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        acc_manager, partuser_obj, user = get_u_details(request)
        serializer = self.get_serializer(data=request.data)
        request.data['user'] = acc_manager.user.id
        print('request .dat',request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        site_obj = Site.objects.get(id=request.data['siteid'])
        network_obj = NetworkAndSiteMap.objects.get(siteid=site_obj)
        camp_obj, created = Campaign.objects.get_or_create(
            name='%s'%(request.data['subject'], ),
            siteid=site_obj,
            account_manager=acc_manager,
            networkid=network_obj.networkid, media_type=request.data['media_type'])
        camp_obj.save()
        mailerobj = Mailer.objects.get(id=serializer.data['id'])
        mailerlogobj = Mailerlogs.objects.create(
            fk_campaign=camp_obj, mailer=mailerobj, link = request.data['link'])
        mailerlogobj.save()
        request.session['created'] = True
        id_ = serializer.data['id']
        return HttpResponseRedirect("/admin/mailers/"+str(id_))
        return self.list(request)

    def perform_create(self, serializer):
        serializer.save()

    def retrieve(self, request, pk=None):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        try:
            instance = self.get_object()
            # queryset = get_object_or_404(queryset, pk=pk)
            # queryset = self.filter_queryset(self.get_queryset())
            serializer = self.get_serializer(instance)
        except:
            serializer = self.get_serializer(data={})
            serializer.is_valid()
        # queryset = get_object_or_404(queryset, pk=pk)
        # queryset = self.filter_queryset(self.get_queryset())
        # serializer = self.get_serializer(instance)
        if request.accepted_renderer.format == 'html':
            data = serializer.data
            acc_manager, partuser_obj, user = get_u_details(request)
            if data and data.get('link'):
                lpage = Lpages.objects.filter(link=data['link'])
                if lpage:
                    data['lp_link'] = lpage[0].title
            lpages = Lpages.objects.filter(siteid__in=get_brand_order(acc_manager))
            lp_banners = {}
            for i in lpages:
                link = i.link.encode('utf-8').strip()
                title = i.title.encode('utf-8').strip()
                if lp_banners.get(int(i.siteid.id)):
                    lp_banners[int(i.siteid.id)].append([link,title])
                else:
                    lp_banners[int(i.siteid.id)] = [[link,title]]
            data['lp_links'] = lp_banners
            data['partner'] = acc_manager.affiliateprogramme.name
            data['language_list'] = ['English', 'German', 'Finnish']
            data['partner_obj'] = acc_manager.affiliateprogramme
            data['affiliates_list'] = [[i.id, i.email] for i in Affiliateinfo.objects.filter(account_manager=acc_manager)]
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            data['partner'] = acc_manager.affiliateprogramme.name
            data['partner_obj'] = acc_manager.affiliateprogramme
            data['call'] = 'POST' if self.kwargs.get('pk') == 'post' else 'PUT'
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/mailers.html')
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        request.data._mutable = True
        acc_manager, partuser_obj, user = get_u_details(request)
        request.data['user'] = acc_manager.user.id
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=False)
        self.perform_update(serializer)
        if request.accepted_renderer.format == 'html':
            data = serializer.data
            acc_manager, partuser_obj, user = get_u_details(request)
            data['affiliates_list'] = [[i.id, i.email] for i in Affiliateinfo.objects.filter(account_manager=acc_manager)]
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            data['partner'] = acc_manager.affiliateprogramme.name
            data['partner_obj'] = acc_manager.affiliateprogramme
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/mailers.html')
        return Response(serializer.data)


# @staff_member_required
class pagepeelViewSet(NonDestructiveModelViewSet):

    """
    Endpoint: /admin/pagepl/

    GET: Typical RESTful Response including Pagination.

    `search` query parameter => ?search=<search text>

    PUT & PATCH are available though no validations
                beyond data types exist on it for now.

    """
    queryset = BannerImages.objects.filter(media_type='P')
    serializer_class = BannerImagesSerializer
    filter_backends = (filters.SearchFilter,
                       DjangoFilterBackend, filters.OrderingFilter,)
    parser_classes = (MultiPartParser, FormParser,)
    renderer_classes = (renderers.JSONRenderer, renderers.TemplateHTMLRenderer)

    @user_class_role_management('Page Peel')
    def list(self, request, *args, **kwargs):
        acc_manager, partuser_obj, user = get_u_details(request)
        queryset = self.filter_queryset(self.get_queryset())
        queryset = queryset.filter(user=acc_manager.user)
        page = True
        if page is not None:
            serializer = self.get_serializer(queryset, many=True)
            if request.accepted_renderer.format == 'html':
                media_type_dict = dict(BannerImages.MEDIA_TYPE)
                cpage = int(request.GET.get('page')) if request.GET.get('page') else 1
                header_list = [{'title': 'ID'}, {'title':'Brand'},
                              {'title': 'Title'}, {'title':'Media Type'}, {'title':'Size'},
                              {'title':'Language'}, {'title':'Account Manager'}, 
                              {'title': 'View/ Edit'}]

                page_div = ''
                data_list = []
                for i in serializer.data:
                    if not i.get('siteid'):
                        continue
                    if not i.get('user'):
                        ac_m = ''
                    else:
                        ac_m = User.objects.get(id=i['user']).username
                    site_name = Site.objects.get(id=i['siteid']).name
                    edit_ele = '<a href="/admin/page-peels/'+str(i['id'])+'"><button class="btn btn-success">View/ Edit</button></a>'
                    del_ele = '<form class="button-form" action="/admin/page-peels/'+str(i['id'])+'" data-method="DELETE"><button class="btn btn-danger">Delete</button></form>' 
                    data_list.append([str(i['id']), site_name.encode('utf-8'), 
                        i['title'].encode('utf-8'), 
                        media_type_dict[str(i['media_type'])], '%sx%s'%(i['width'],i['height']),
                        str(i['language']), str(ac_m),
                        edit_ele])
                msg = ''
                if request.session.get('deleted'):
                    msg = 'Page peel has been successfully deleted.'
                    if request.session.get('deleted'):
                        del request.session['deleted']
                return Response({'header_list': header_list, 
                                'type':'Page Peels',
                                'message': msg,
                                'call_type': 'page-peels',
                                'data_list': data_list, 'pagination_div': page_div,
                                'partner': acc_manager.affiliateprogramme.name,
                                'partner_obj': acc_manager.affiliateprogramme,
                                'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
                           }, template_name='partner/bimages_list.html')
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        # if request.accepted_renderer.format == 'html':
        #     return Response({'data': serializer.data}, template_name='partner/index.html')
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        pid = request.session.get('partnerid')
        request.data._mutable = True
        acc_manager, partuser_obj, user = get_u_details(request)
        request.data['user'] = acc_manager.user.id
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        site_obj = Site.objects.get(id=request.data['siteid'])
        network_obj = NetworkAndSiteMap.objects.get(siteid=site_obj)
        camp_obj, created = Campaign.objects.get_or_create(
            name=request.data['title'], siteid=site_obj,
            account_manager=acc_manager,
            networkid=network_obj.networkid, media_type=request.data['media_type'])
        camp_obj.save()
        bannerobj = BannerImages.objects.get(id=serializer.data['id'])
        bannerlogobj = Bannerlogs.objects.create(
            fk_campaign=camp_obj, fk_bannerimages=bannerobj)
        bannerlogobj.save()
        request.session['created'] = True
        id_ = serializer.data['id']
        return HttpResponseRedirect("/admin/page-peels/"+str(id_))

    def perform_create(self, serializer):
        serializer.save()

    @user_class_role_management('Pagepeel')
    def retrieve(self, request, pk=None):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        try:
            instance = self.get_object()
            # queryset = get_object_or_404(queryset, pk=pk)
            # queryset = self.filter_queryset(self.get_queryset())
            serializer = self.get_serializer(instance)
        except:
            serializer = self.get_serializer(data={})
            serializer.is_valid()
        # queryset = get_object_or_404(queryset, pk=pk)
        # queryset = self.filter_queryset(self.get_queryset())
        # serializer = self.get_serializer(instance)
        if request.accepted_renderer.format == 'html':
            data = serializer.data
            
            acc_manager, partuser_obj, user = get_u_details(request)
            msg = ''
            if request.session.get('created') or request.session.get('deleted'):
                crt = request.session.get('created')
                deleted = request.session.get('deleted')
                if crt:
                    msg = 'Page peel has been successfully submitted.'
                elif deleted:
                    msg = 'Page peel has been successfully deleted.'
                else:
                    msg = 'There is an error while adding page peel.'
                if crt:
                    del request.session['created']
                if deleted:
                    del request.session['deleted']
            data['message'] = msg
            if data and data.get('link'):
                lpage = Lpages.objects.filter(link=data['link'])
                if lpage:
                    data['lp_link'] = lpage[0].title
            lpages = Lpages.objects.filter(siteid__in=get_brand_order(acc_manager))
            lp_banners = {}
            for i in lpages:
                if lp_banners.get(int(i.siteid.id)):
                    lp_banners[int(i.siteid.id)].append([str(i.link),str(i.title)])
                else:
                    lp_banners[int(i.siteid.id)] = [[str(i.link),str(i.title)]]
            data['lp_links'] = lp_banners
            data['partner'] = acc_manager.affiliateprogramme.name
            data['language_list'] = ['English', 'German', 'Finnish']
            data['partner_obj'] = acc_manager.affiliateprogramme
            data['media_list'] = dict(BannerImages.MEDIA_TYPE)
            data['affiliates_list'] = [[i.id, i.email] for i in Affiliateinfo.objects.filter(account_manager=acc_manager)]
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            data['call'] = 'POST' if self.kwargs.get('pk') == 'post' else 'PUT'
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/pagep.html')
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        request.data._mutable = True
        acc_manager, partuser_obj, user = get_u_details(request)
        request.data['user'] = acc_manager.user.id
        if request.data.get('banner', '') == '':
            del request.data['banner']
            del request.data['height']
            del request.data['width']
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=False)
        self.perform_update(serializer)
        if request.accepted_renderer.format == 'html':
            data = serializer.data
            data['media_list'] = dict(BannerImages.MEDIA_TYPE)
            if data and data.get('link'):
                lpage = Lpages.objects.filter(link=data['link'])
                if lpage:
                    data['lp_link'] = lpage[0].title
            acc_manager, partuser_obj, user = get_u_details(request)
            lpages = Lpages.objects.filter(siteid__in=get_brand_order(acc_manager))
            lp_banners = {}
            for i in lpages:
                if lp_banners.get(int(i.siteid.id)):
                    lp_banners[int(i.siteid.id)].append([str(i.link),str(i.title)])
                else:
                    lp_banners[int(i.siteid.id)] = [[str(i.link),str(i.title)]]
            data['lp_links'] = lp_banners
            data['message'] = 'Page peel has been successfully edited.'
            data['affiliates_list'] = [[i.id, i.email] for i in Affiliateinfo.objects.filter(account_manager=acc_manager)]
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            data['partner'] = acc_manager.affiliateprogramme.name
            # data['message'] = 'successfully submitted.'
            data['partner_obj'] = acc_manager.affiliateprogramme
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/pagep.html')
        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        # b_image = Bannerlogs.objects.filter(fk_bannerimages=instance)
        # if b_image:
        #     b_image[0].fk_campaign.delete()
        #     b_image[0].delete()
        self.perform_destroy(instance)
        request.session['deleted'] = True
        return self.list(request)

    def perform_destroy(self, instance):
        instance.delete()

# @staff_member_required
class ScreenShotViewSet(NonDestructiveModelViewSet):

    """
    Endpoint: /admin/screenshot/

    GET: Typical RESTful Response including Pagination.

    `search` query parameter => ?search=<search text>

    PUT & PATCH are available though no validations
                beyond data types exist on it for now.

    """
    queryset = BannerImages.objects.filter(media_type='SS')
    serializer_class = BannerImagesSerializer
    filter_backends = (filters.SearchFilter,
                       DjangoFilterBackend, filters.OrderingFilter,)
    parser_classes = (MultiPartParser, FormParser,)
    renderer_classes = (renderers.JSONRenderer, renderers.TemplateHTMLRenderer)

    @user_class_role_management('Screenshot')
    def list(self, request, *args, **kwargs):
        acc_manager, partuser_obj, user = get_u_details(request)
        queryset = self.filter_queryset(self.get_queryset())
        queryset = queryset.filter(user=acc_manager.user)
        page = True
        if page is not None:
            serializer = self.get_serializer(queryset, many=True)
            if request.accepted_renderer.format == 'html':
                media_type_dict = dict(BannerImages.MEDIA_TYPE)
                cpage = int(request.GET.get('page')) if request.GET.get('page') else 1
                header_list = [{'title': 'ID'}, {'title':'Brand'}, 
                        {'title': 'Title'}, {'title':'Media Type'}, {'title':'Size'},
                          {'title':'Language'}, {'title':'Account Manager'}, 
                          {'title': 'View/ Edit'}]
                                
                page_div = ''
                data_list = []
                for i in serializer.data:
                    if not i.get('siteid'):
                        continue
                    if not i.get('user'):
                        ac_m = ''
                    else:
                        ac_m = User.objects.get(id=i['user']).username
                    site_name = Site.objects.get(id=i['siteid']).name
                    edit_ele = '<a href="/admin/screenshots/'+str(i['id'])+'"><button class="btn btn-success">View/ Edit</button></a>'
                    del_ele = '<form class="button-form" action="/admin/screenshots/'+str(i['id'])+'" data-method="DELETE"><button class="btn btn-danger">Delete</button></form>' 
                    data_list.append([str(i['id']), site_name.encode('utf-8'), 
                        i['title'].encode('utf-8'), 
                        media_type_dict[str(i['media_type'])], '%sx%s'%(i['width'],i['height']),
                        str(i['language']), str(ac_m),
                        edit_ele])
                msg = ''
                if request.session.get('deleted'):
                    msg = 'Screenshot has been successfully deleted.'
                    if request.session.get('deleted'):
                        del request.session['deleted']
                return Response({'header_list': header_list, 
                                'type':'Screenshots',
                                'message':msg,
                                'call_type': 'screenshots',
                                'data_list': data_list, 'pagination_div': page_div,
                                'partner': acc_manager.affiliateprogramme.name,
                                'partner_obj': acc_manager.affiliateprogramme,
                                'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
                           }, template_name='partner/bimages_list.html')
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        # if request.accepted_renderer.format == 'html':
        #     return Response({'data': serializer.data}, template_name='partner/index.html')
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        pid = request.session.get('partnerid')
        request.data._mutable = True
        acc_manager, partuser_obj, user = get_u_details(request)
        request.data['user'] = acc_manager.user.id
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        site_obj = Site.objects.get(id=request.data['siteid'])
        network_obj = NetworkAndSiteMap.objects.get(siteid=site_obj)
        camp_obj, created = Campaign.objects.get_or_create(
            name=request.data['title'], siteid=site_obj,
            account_manager=acc_manager,
            networkid=network_obj.networkid, media_type=request.data['media_type'])
        camp_obj.save()
        bannerobj = BannerImages.objects.get(id=serializer.data['id'])
        bannerlogobj = Bannerlogs.objects.create(
            fk_campaign=camp_obj, fk_bannerimages=bannerobj)
        bannerlogobj.save()
        request.session['created'] = True
        id_ = serializer.data['id']
        return HttpResponseRedirect("/admin/screenshots/"+str(id_))

    def perform_create(self, serializer):
        serializer.save()

    @user_class_role_management('Screenshot')
    def retrieve(self, request, pk=None):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        try:
            instance = self.get_object()
            # queryset = get_object_or_404(queryset, pk=pk)
            # queryset = self.filter_queryset(self.get_queryset())
            serializer = self.get_serializer(instance)
        except:
            serializer = self.get_serializer(data={})
            serializer.is_valid()
        # queryset = get_object_or_404(queryset, pk=pk)
        # queryset = self.filter_queryset(self.get_queryset())
        # serializer = self.get_serializer(instance)
        if request.accepted_renderer.format == 'html':
            data = serializer.data
            
            acc_manager, partuser_obj, user = get_u_details(request)
            msg = ''
            if request.session.get('created') or request.session.get('deleted'):
                crt = request.session.get('created')
                deleted = request.session.get('deleted')
                if crt:
                    msg = 'Screenshot has been successfully submitted.'
                elif deleted:
                    msg = 'Screenshot has been successfully deleted.'
                else:
                    msg = 'There is an error while adding screenshot.'
                if crt:
                    del request.session['created']
                if deleted:
                    del request.session['deleted']
            data['message'] = msg
            if data and data.get('link'):
                lpage = Lpages.objects.filter(link=data['link'])
                if lpage:
                    data['lp_link'] = lpage[0].title
            lpages = Lpages.objects.filter(siteid__in=get_brand_order(acc_manager))
            lp_banners = {}
            for i in lpages:
                if lp_banners.get(int(i.siteid.id)):
                    lp_banners[int(i.siteid.id)].append([str(i.link),str(i.title)])
                else:
                    lp_banners[int(i.siteid.id)] = [[str(i.link),str(i.title)]]
            data['lp_links'] = lp_banners
            data['partner'] = acc_manager.affiliateprogramme.name
            data['language_list'] = ['English', 'German', 'Finnish']
            data['partner_obj'] = acc_manager.affiliateprogramme
            data['media_list'] = dict(BannerImages.MEDIA_TYPE)
            data['affiliates_list'] = [[i.id, i.email] for i in Affiliateinfo.objects.filter(account_manager=acc_manager)]
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            data['call'] = 'POST' if self.kwargs.get('pk') == 'post' else 'PUT'
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/screenshot.html')
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        request.data._mutable = True
        acc_manager, partuser_obj, user = get_u_details(request)
        request.data['user'] = acc_manager.user.id
        if request.data.get('banner', '') == '':
            del request.data['banner']
            del request.data['height']
            del request.data['width']
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=False)
        self.perform_update(serializer)
        if request.accepted_renderer.format == 'html':
            data = serializer.data
            data['media_list'] = dict(BannerImages.MEDIA_TYPE)
            if data and data.get('link'):
                lpage = Lpages.objects.filter(link=data['link'])
                if lpage:
                    data['lp_link'] = lpage[0].title
            acc_manager, partuser_obj, user = get_u_details(request)
            lpages = Lpages.objects.filter(siteid__in=get_brand_order(acc_manager))
            lp_banners = {}
            for i in lpages:
                if lp_banners.get(int(i.siteid.id)):
                    lp_banners[int(i.siteid.id)].append([str(i.link),str(i.title)])
                else:
                    lp_banners[int(i.siteid.id)] = [[str(i.link),str(i.title)]]
            data['lp_links'] = lp_banners
            data['message'] = 'Screenshot has been successfully edited.'
            data['affiliates_list'] = [[i.id, i.email] for i in Affiliateinfo.objects.filter(account_manager=acc_manager)]
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            data['partner'] = acc_manager.affiliateprogramme.name
            # data['message'] = 'successfully submitted.'
            data['partner_obj'] = acc_manager.affiliateprogramme
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/screenshot.html')
        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        # b_image = Bannerlogs.objects.filter(fk_bannerimages=instance)
        # if b_image:
        #     b_image[0].fk_campaign.delete()
        #     b_image[0].delete()
        self.perform_destroy(instance)
        request.session['deleted'] = True
        return self.list(request)

    def perform_destroy(self, instance):
        instance.delete()

# @staff_member_required
class VideoViewSet(viewsets.ModelViewSet):

    """
    Endpoint: /admin/video/

    GET: Typical RESTful Response including Pagination.

    `search` query parameter => ?search=<search text>

    PUT & PATCH are available though no validations
                beyond data types exist on it for now.

    """
    queryset = Video.objects.all()
    serializer_class = VideoSerializer
    filter_backends = (filters.SearchFilter,
                       DjangoFilterBackend, filters.OrderingFilter,)
    parser_classes = (MultiPartParser, FormParser,)
    renderer_classes = (renderers.JSONRenderer, renderers.TemplateHTMLRenderer)

    @user_class_role_management('Videos')
    def list(self, request, *args, **kwargs):
        acc_manager, partuser_obj, user = get_u_details(request)
        queryset = self.filter_queryset(self.get_queryset())
        queryset = queryset.filter(user=acc_manager.user)
        page = True
        if page is not None:
            serializer = self.get_serializer(queryset, many=True)
            if request.accepted_renderer.format == 'html':
                cpage = int(request.GET.get('page')) if request.GET.get('page') else 1
                header_list = [{'title': 'ID'}, {'title': 'Title'},
                         {'title':'Site'}, {'title':'Account Manager'}, {'title': 'View/ Edit'}]
                
                page_div = ''
                data_list = []
                for i in serializer.data:
                    if not i.get('siteid'):
                        continue
                    ac_m = ''
                    if i.get('user'):
                        ac_m = User.objects.get(id=i['user']).username
                    site_name = Site.objects.get(id=i['siteid']).name
                    edit_ele = '<a href="/admin/videos/'+str(i['id'])+'"><button class="btn btn-success">View/ Edit</button></a>'
                    del_ele = '<form class="button-form" action="/admin/videos/'+str(i['id'])+'" data-method="DELETE"><button class="btn btn-danger">Delete</button></form>' 
                    data_list.append([str(i['id']), i['title'].encode('utf-8'), str(site_name), str(ac_m),
                                     edit_ele])
                msg = ''
                if request.session.get('deleted'):
                    msg = 'Video has been successfully deleted.'
                    if request.session.get('deleted'):
                        del request.session['deleted']
                return Response({'header_list': header_list, 
                                'data_list': data_list,
                                'message': msg,
                                'pagination_div': page_div,
                                'partner': acc_manager.affiliateprogramme.name,
                                'partner_obj': acc_manager.affiliateprogramme,
                                'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
                           }, template_name='partner/video_list.html')
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        # if request.accepted_renderer.format == 'html':
        #     return Response({'data': serializer.data}, template_name='partner/index.html')
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        try:
            pid = request.session.get('partnerid')
            request.data._mutable = True
            acc_manager, partuser_obj, user = get_u_details(request)
            request.data['user'] = acc_manager.user.id
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            site_obj = Site.objects.get(id=request.data['siteid'])
            network_obj = NetworkAndSiteMap.objects.get(siteid=site_obj)
            camp_obj, created = Campaign.objects.get_or_create(
                    name=request.data['title'], siteid=site_obj,
                    account_manager=acc_manager,
                    networkid=network_obj.networkid, media_type='V')
            camp_obj.save()
            videoobj = Video.objects.get(id=serializer.data['id'])
            videologobj = Videologs.objects.create(
                fk_campaign=camp_obj, fk_video=videoobj)
            videologobj.save()
            request.session['created'] = True
            id_ = serializer.data['id']
            return HttpResponseRedirect("/admin/videos/"+str(id_))
        except Exception as e:
            data = {}
            data['affiliates_list'] = [[i.id, i.email] for i in Affiliateinfo.objects.filter(account_manager=acc_manager)]
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            data['partner'] = acc_manager.affiliateprogramme.name
            data['partner_obj'] = acc_manager.affiliateprogramme
            data['call'] = 'POST'
            data['message'] = str(e)
            return Response({'data': data, 
                            'message': str(e),
                            'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/video.html')

    def perform_create(self, serializer):
        serializer.save()

    @user_class_role_management('Videos')
    def retrieve(self, request, pk=None):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        try:
            instance = self.get_object()
            # queryset = get_object_or_404(queryset, pk=pk)
            # queryset = self.filter_queryset(self.get_queryset())
            serializer = self.get_serializer(instance)
            data = None
        except:
            serializer = self.get_serializer(data={})
            serializer.is_valid()
            data = {}
        # queryset = get_object_or_404(queryset, pk=pk)
        # queryset = self.filter_queryset(self.get_queryset())
        # serializer = self.get_serializer(instance)
        if request.accepted_renderer.format == 'html':
            if data is None:
                data = serializer.data
            acc_manager, partuser_obj, user = get_u_details(request)
            msg = ''
            if request.session.get('created') or request.session.get('deleted'):
                crt = request.session.get('created')
                deleted = request.session.get('deleted')
                if crt:
                    msg = 'Video has been successfully submitted.'
                elif deleted:
                    msg = 'Video has been successfully deleted.'
                else:
                    msg = 'There is an error while adding video.'
                if crt:
                    del request.session['created']
                if deleted:
                    del request.session['deleted']
            data['message'] = msg
            data['affiliates_list'] = [[i.id, i.email] for i in Affiliateinfo.objects.filter(account_manager=acc_manager)]
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            data['partner'] = acc_manager.affiliateprogramme.name
            data['language_list'] = ['English', 'German', 'Finnish']
            data['partner_obj'] = acc_manager.affiliateprogramme
            data['call'] = 'POST' if self.kwargs.get('pk') == 'post' else 'PUT'
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/video.html')
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        request.data._mutable = True
        if not request.FILES.get('video'):
            del request.data['video']
        acc_manager, partuser_obj, user = get_u_details(request)
        request.data['user'] = acc_manager.user.id
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=False)
        self.perform_update(serializer)
        if request.accepted_renderer.format == 'html':
            data = serializer.data
            acc_manager, partuser_obj, user = get_u_details(request)
            data['message'] = 'Updated Video.'
            data['affiliates_list'] = [[i.id, i.email] for i in Affiliateinfo.objects.filter(account_manager=acc_manager)]
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            data['partner'] = acc_manager.affiliateprogramme.name
            data['message'] = 'Video has been successfully edited.'
            data['partner_obj'] = acc_manager.affiliateprogramme
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/video.html')
        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        # videologobj = Videologs.objects.filter(fk_video=instance)
        # if videologobj:
        #     videologobj[0].fk_campaign.delete()
        #     videologobj[0].delete()
        self.perform_destroy(instance)
        request.session['deleted'] = True
        return self.list(request)

    def perform_destroy(self, instance):
        instance.delete()


# @staff_member_required
class MediaCategoryViewSet(NonDestructiveModelViewSet):

    """
    Endpoint: /root/mediacategory/

    GET: Typical RESTful Response including Pagination.

    `search` query parameter => ?search=<search text>

    PUT & PATCH are available though no validations
                beyond data types exist on it for now.

    """
    queryset = MediaCategory.objects.all()
    serializer_class = MediaCategorySerializer
    filter_backends = (filters.SearchFilter,
                       DjangoFilterBackend, filters.OrderingFilter,)
    parser_classes = (MultiPartParser, FormParser,)
    renderer_classes = (renderers.JSONRenderer, renderers.TemplateHTMLRenderer)


    def list(self, request, *args, **kwargs):
        acc_manager, partuser_obj, user = get_u_details(request)
        queryset = self.filter_queryset(self.get_queryset())
        queryset = queryset.filter(user=acc_manager.user)
        page = True
        if page is not None:
            serializer = self.get_serializer(queryset, many=True)
            if request.accepted_renderer.format == 'html':
                cpage = int(request.GET.get('page')) if request.GET.get('page') else 1
                header_list = [{'title': 'Network'},
                         {'title':'Site'}, {'title':'Account Manager'}, {'title':'Category'}, {'title': 'View/ Edit'}]
                
                page_div = ''
                data_list = []
                for i in serializer.data:
                    if not i.get('siteid'):
                        continue
                    ac_m = ''
                    if i.get('user'):
                        ac_m = User.objects.get(id=i['user']).username
                    nt_name = Network.objects.get(networkid=i['networkid']).networkname
                    site_name = Site.objects.get(id=i['siteid']).name
                    edit_ele = '<a href="/admin/mediacategory/'+str(i['id'])+'"><button class="btn btn-success">View/ Edit</button></a>'
                    del_ele = '<form class="button-form" action="/admin/mediacategory/'+str(i['id'])+'" data-method="DELETE"><button class="btn btn-danger">Delete</button></form>' 
                    data_list.append([str(nt_name), str(site_name), str(ac_m), str(i['category_name']),
                                     edit_ele])
                return Response({'header_list': header_list, 'data_list': data_list, 'pagination_div': page_div,
                                'partner': acc_manager.affiliateprogramme.name,
                                'partner_obj': acc_manager.affiliateprogramme,
                                'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
                           }, template_name='partner/mediacategory_list.html')
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        # if request.accepted_renderer.format == 'html':
        #     return Response({'data': serializer.data}, template_name='partner/index.html')
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return self.list(request)

    def perform_create(self, serializer):
        serializer.save()

    def retrieve(self, request, pk=None):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        try:
            instance = self.get_object()
            # queryset = get_object_or_404(queryset, pk=pk)
            # queryset = self.filter_queryset(self.get_queryset())
            serializer = self.get_serializer(instance)
        except:
            serializer = self.get_serializer(data={})
            serializer.is_valid()
        # queryset = get_object_or_404(queryset, pk=pk)
        # queryset = self.filter_queryset(self.get_queryset())
        # serializer = self.get_serializer(instance)
        if request.accepted_renderer.format == 'html':
            data = serializer.data
            acc_manager, partuser_obj, user = get_u_details(request)
            data['partner'] = acc_manager.affiliateprogramme.name
            data['partner_obj'] = acc_manager.affiliateprogramme
            data['call'] = 'POST' if self.kwargs.get('pk') == 'post' else 'PUT'
            data['sites_list'] = [[i.id, i.name] for i in Site.objects.all()]
            data['network_list'] = [[i.networkid, i.networkname] for i in acc_manager.affiliateprogramme.networkid.all()]
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/mediacategory.html')
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        request.data._mutable = True
        acc_manager, partuser_obj, user = get_u_details(request)
        request.data['user'] = acc_manager.user.id
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=False)
        self.perform_update(serializer)
        if request.accepted_renderer.format == 'html':
            data = serializer.data
            acc_manager, partuser_obj, user = get_u_details(request)
            data['partner'] = acc_manager.affiliateprogramme.name
            data['partner_obj'] = acc_manager.affiliateprogramme
            data['sites_list'] = [[i.id, i.name] for i in Site.objects.all()]
            data['network_list'] = [[i.networkid, i.networkname] for i in acc_manager.affiliateprogramme.networkid.all()]
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/mediacategory.html')
        return Response(serializer.data)

@user_role_management('Conversions Report')
def conversionreportinfo(request, **kwargs):
    context_dict = {}
    acc_manager = kwargs.get('acc_manager')
    partuser_obj = kwargs.get('partuser_obj')
    user = kwargs.get('user')
    net_ids_list = []
    number_of_items = 10000
    nexting_page = 0
    previous_page = 0
    starting_page = 0
    page = 0
    site_ids_list = get_brand_order(acc_manager)
    site_ids = site_ids_list
    format_d = request.POST.get("format")
    domain = None
    site_name = 'All Websites'
    if request.method == "GET":
        custom = "Custom"
        fromdate = request.GET.get("fromdate")
        todate = request.GET.get("todate")
        if not fromdate or not todate:
            custom = None
        campaign_id = None
        starting_page = request.GET.get('start_page')
        nexting_page = request.GET.get('next')
        previous_page = request.GET.get('prev')
        page = request.GET.get('page')
        if not starting_page  and not nexting_page and not previous_page and not page:
            form_data = {}
            request.session['partner_conversioninfo'] = form_data
        else:
            form_data = request.session.get('partner_conversioninfo')
            custom = form_data.get("customid")
            fromdate = form_data.get("fromdate")
            todate = form_data.get("todate")
            siteids = form_data.get("siteids")
            campaign_id = form_data.get("campaign_id")
        if not fromdate and not todate:
            custom = "Custom"
            todate = datetime.now().strftime("%m/%d/%Y")
            current_month , current_year =  datetime.now().month, datetime.now().year
            fromdate = "%s/01/%s" % (current_month, current_year )
    else:
        custom = str(request.POST.get("customid"))
        fromdate = str(request.POST.get("fromdate"))
        todate = str(request.POST.get("todate"))
        siteids = request.POST.getlist("siteids")
        domain = request.POST.get("sitename")

        if domain:
            format_d = 'reload'
        site_ids = site_ids_list
        if domain and domain != 'All':
            site_domain_list = domain.split(',')
            site_ids = site_ids_list.filter(domain__in=site_domain_list)
            site_name = 'Multiple sites.'
            if len(domain.split(',')) <2:
                site_name = site_ids_list.get(domain=site_domain_list[0]).name
        campaign_id = request.POST.get('campaign_id')
        if campaign_id == 'all':
            campaign_id = None
        
        form_data = {'fromdate':fromdate, 'todate':todate,'customid':custom, "siteids":siteids,
                     
                     "campaign_id":campaign_id}
        request.session['partner_viewtrackersinfo'] = form_data

    country_list = Country.objects.all().values_list('countryname', flat=True).distinct()
    currencyfilter = request.POST.get("currency", 'euro')
    group_d = str(request.POST.get("groupby", 'brand'))
    country = request.POST.getlist('country')
    # if country:
    #     country = [country]
    # else:
    #     country = country_list

    if campaign_id:
        campaign_id = int(campaign_id)
    # filter_networks = [1]
    # if siteids:
    #     filter_sites = [int(i) for i in siteids]
    # else:
    #     filter_sites = [int(i.id) for i in site_ids_list]
    # siteid_list = get_site_ids_data_list(site_ids_list, filter_sites)
    sitefilter = request.POST.getlist("siteid")
    campfilter = request.POST.get("campfilter")
    aff_id = request.POST.get('aff_id')
    camp_links = Campaign.objects.filter(
        fk_affiliateinfo__account_manager=acc_manager).order_by('-id')

    if campfilter:
        camp_links = camp_links.filter(id=campfilter)
        if campfilter:
            campaign_id = int(campfilter)
    if sitefilter:
        site_ids = site_ids.filter(id__in=sitefilter)
    else:
        sitefilter = [str(i.id) for i in site_ids]
    date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
    from_date = date_times_dict.get('fromdate')
    to_date = date_times_dict.get('todate')
    print 'domain', domain, site_ids
    # camp_links = Campaign.objects.filter(fk_affiliateinfo=obj).select_related('siteid').\
    #                     order_by('-id')
    aff_list = Affiliateinfo.objects.filter(account_manager=acc_manager).order_by('-id')
    if aff_id:
        aff_m_list = aff_list.filter(id=aff_id)
    else:
        aff_m_list = aff_list
    camp_track_links = CampaignTrackerMapping.objects.filter(
            fk_affiliateinfo__in=aff_m_list,
            fk_campaign__siteid__in=site_ids).select_related('fk_campaign', 'fk_affiliateinfo')
    count = camp_track_links.count()
    start_record, end_record, start_page, \
            end_page, next_page, cur_page, page_count = get_paginate_dimensions(page, starting_page,
                                                                                nexting_page, previous_page,
                                                                                count, number_of_items)
    #from_date = date(2012, 01,01)
    #to_date = datetime.now()+timedelta(days=1)
    header_list, data_list, currency = Process_conversion_stats(acc_manager, from_date, to_date, 
        camp_track_links, site_ids, country, currencyfilter, group_d)


    pagination_div  = makePaginationDiv(start_page, page_count, cur_page, next_page, count)

    context_dict = {"data_list":data_list,"header_list":header_list,
                        "from_date":from_date.strftime('%B %d, %Y'),"to_date":to_date.strftime('%B %d, %Y') ,
                        "ui_from_date":datetime_or_date_to_string(from_date),
                        "ui_to_date":datetime_or_date_to_string(to_date),
                       'campaign_id': campaign_id,"pagination_div":pagination_div, 'domain': site_name,
                       'currency': currency}
    if format_d == "json":
        context_dict["status"] = "success"
        context_dict['data_list'] = data_list
        context_dict["header_list"] = header_list
        return HttpResponse(json.dumps(context_dict, indent=4))
    context_dict["country_list"] = country_list
    context_dict['sites_obj_list'] = site_ids_list
    context_dict['siteid'] = sitefilter if sitefilter else []
    context_dict['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
    context_dict["aff_list"] = [{'id':i.id, 'username': i.email.replace('vcommsubaff', '')} for i in aff_list]
    context_dict['data_list'] = json.dumps(data_list)
    context_dict["header_list"] = json.dumps(header_list)
    context_dict['partner'] = acc_manager.affiliateprogramme.name
    context_dict['partner_obj'] = acc_manager.affiliateprogramme
    context_dict['part_user'] = ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
    # context_dict["channels_list"] = get_channels_list()
    # context_dict["channel"] = channel if channel else "All"
    return render(request, 'partner/conversionreport.html', context_dict)


# def pmcreportinfo(request, **kwargs):
#     context_dict = {}
#     acc_manager = kwargs.get('acc_manager')
#     partuser_obj = kwargs.get('partuser_obj')
#     user = kwargs.get('user')
#     net_ids_list = []
#     number_of_items = 10000
#     nexting_page = 0
#     previous_page = 0
#     starting_page = 0
#     page = 0
#     site_ids_list = acc_manager.siteid.all().order_by('-id')
#     site_ids = site_ids_list
#     format_d = request.POST.get("format")
#     domain = None
#     site_name = 'All Websites'
#     if request.method == "GET":
#         custom = "Custom"
#         fromdate = request.GET.get("fromdate")
#         todate = request.GET.get("todate")
#         if not fromdate or not todate:
#             custom = None
#         campaign_id = None
#         starting_page = request.GET.get('start_page')
#         nexting_page = request.GET.get('next')
#         previous_page = request.GET.get('prev')
#         page = request.GET.get('page')
#         if not starting_page  and not nexting_page and not previous_page and not page:
#             form_data = {}
#             request.session['partner_pmcinfo'] = form_data
#         else:
#             form_data = request.session.get('partner_pmcinfo')
#             custom = form_data.get("customid")
#             fromdate = form_data.get("fromdate")
#             todate = form_data.get("todate")
#             siteids = form_data.get("siteids")
#         if not fromdate and not todate:
#             custom = "Custom"
#             todate = datetime.now().strftime("%m/%d/%Y")
#             current_month , current_year =  datetime.now().month, datetime.now().year
#             fromdate = "%s/01/%s" % (current_month, current_year )
#     else:
#         custom = str(request.POST.get("customid"))
#         fromdate = str(request.POST.get("fromdate"))
#         todate = str(request.POST.get("todate"))
#         siteids = request.POST.getlist("siteids")
#         domain = request.POST.get("sitename")

#         if domain:
#             format_d = 'reload'
#         site_ids = site_ids_list
#         if domain and domain != 'All':
#             site_domain_list = domain.split(',')
#             site_ids = site_ids_list.filter(domain__in=site_domain_list)
#             site_name = 'Multiple sites.'
#             if len(domain.split(',')) <2:
#                 site_name = site_ids_list.get(domain=site_domain_list[0]).name
        
#         form_data = {'fromdate':fromdate, 'todate':todate,'customid':custom, "siteids":siteids}
#         request.session['partner_pmcinfo'] = form_data

#     sitefilter = request.POST.getlist("siteid")
    
#     if sitefilter:
#         site_ids = site_ids.filter(id__in=sitefilter)
#     date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
#     from_date = date_times_dict.get('fromdate')
#     to_date = date_times_dict.get('todate')
#     print 'domain', domain, site_ids
    
#     count = 100
#     start_record, end_record, start_page, \
#             end_page, next_page, cur_page, page_count = get_paginate_dimensions(page, starting_page,
#                                                                                 nexting_page, previous_page,
#                                                                                 count, number_of_items)
#     #from_date = date(2012, 01,01)
#     #to_date = datetime.now()+timedelta(days=1)
#     header_list, data_list, currency = Process_pmc_stats(acc_manager, from_date, to_date, site_ids)


#     pagination_div  = makePaginationDiv(start_page, page_count, cur_page, next_page, count)

#     context_dict = {"data_list":data_list,"header_list":header_list,
#                         "from_date":from_date.strftime('%B %d, %Y'),"to_date":to_date.strftime('%B %d, %Y') ,
#                         "ui_from_date":datetime_or_date_to_string(from_date),
#                         "ui_to_date":datetime_or_date_to_string(to_date),
#                        "pagination_div":pagination_div, 'domain': site_name,
#                        'currency': currency}
#     if format_d == "json":
#         context_dict["status"] = "success"
#         context_dict['data_list'] = data_list
#         context_dict["header_list"] = header_list
#         return HttpResponse(json.dumps(context_dict, indent=4))
#     context_dict['sites_obj_list'] = site_ids_list
#     context_dict['data_list'] = json.dumps(data_list)
#     context_dict["header_list"] = json.dumps(header_list)
#     context_dict['partner'] = acc_manager.affiliateprogramme.name
#     context_dict['partner_obj'] = acc_manager.affiliateprogramme
#     context_dict['part_user'] = ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
#     # context_dict["channels_list"] = get_channels_list()
#     # context_dict["channel"] = channel if channel else "All"
#     return render(request, 'partner/activedeositreport.html', context_dict)



@user_role_management('Active Depositors')
def activedepositorsinfo(request, **kwargs):
    context_dict = {}
    acc_manager = kwargs.get('acc_manager')
    partuser_obj = kwargs.get('partuser_obj')
    user = kwargs.get('user')
    net_ids_list = []
    number_of_items = 10000
    nexting_page = 0
    previous_page = 0
    starting_page = 0
    page = 0
    site_ids_list = get_brand_order(acc_manager)
    site_ids = site_ids_list
    format_d = request.POST.get("format")
    domain = None
    site_name = 'All Websites'
    if request.method == "GET":
        custom = "Custom"
        fromdate = request.GET.get("fromdate")
        todate = request.GET.get("todate")
        if not fromdate or not todate:
            custom = None
        campaign_id = None
        starting_page = request.GET.get('start_page')
        nexting_page = request.GET.get('next')
        previous_page = request.GET.get('prev')
        page = request.GET.get('page')
        if not starting_page  and not nexting_page and not previous_page and not page:
            form_data = {}
            request.session['partner_conversioninfo'] = form_data
        else:
            form_data = request.session.get('partner_conversioninfo')
            custom = form_data.get("customid")
            fromdate = form_data.get("fromdate")
            todate = form_data.get("todate")
            siteids = form_data.get("siteids")
            campaign_id = form_data.get("campaign_id")
        if not fromdate and not todate:
            custom = "Custom"
            todate = datetime.now().strftime("%m/%d/%Y")
            current_month , current_year =  datetime.now().month, datetime.now().year
            fromdate = "%s/01/%s" % (current_month, current_year )
    else:
        custom = str(request.POST.get("customid"))
        fromdate = str(request.POST.get("fromdate"))
        todate = str(request.POST.get("todate"))
        siteids = request.POST.getlist("siteids")
        domain = request.POST.get("sitename")

        if domain:
            format_d = 'reload'
        site_ids = site_ids_list
        if domain and domain != 'All':
            site_domain_list = domain.split(',')
            site_ids = site_ids_list.filter(domain__in=site_domain_list)
            site_name = 'Multiple sites.'
            if len(domain.split(',')) <2:
                site_name = site_ids_list.get(domain=site_domain_list[0]).name
        campaign_id = request.POST.get('campaign_id')
        if campaign_id == 'all':
            campaign_id = None
        
        form_data = {'fromdate':fromdate, 'todate':todate,'customid':custom, "siteids":siteids,
                     
                     "campaign_id":campaign_id}
        request.session['partner_viewtrackersinfo'] = form_data

    if campaign_id:
        campaign_id = int(campaign_id)
    # filter_networks = [1]
    # if siteids:
    #     filter_sites = [int(i) for i in siteids]
    # else:
    #     filter_sites = [int(i.id) for i in site_ids_list]
    # siteid_list = get_site_ids_data_list(site_ids_list, filter_sites)
    sitefilter = request.POST.getlist("siteid")
    campfilter = request.POST.get("campfilter")
    aff_id = request.POST.get('aff_id')
    country_list = Country.objects.all().values_list('countryname', flat=True).distinct()
    country = request.POST.getlist('country')
    # if country:
    #     country = [country]
    if not country:
        country = list(country_list)
        country.append('')
    # camp_links = Campaign.objects.filter(fk_affiliateinfo=obj).order_by('-id')

    # if campfilter:
    #     camp_links = camp_links.filter(id=campfilter)
    #     if campfilter:
    #         campaign_id = int(campfilter)
    if sitefilter:
        site_ids = site_ids.filter(id__in=sitefilter)
    else:
        sitefilter = [str(i.id) for i in site_ids]
    date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
    from_date = date_times_dict.get('fromdate')
    to_date = date_times_dict.get('todate')
    print 'domain', domain, site_ids
    # camp_links = Campaign.objects.filter(fk_affiliateinfo=obj).select_related('siteid').\
    #                     order_by('-id')
    aff_list = Affiliateinfo.objects.filter(account_manager=acc_manager).order_by('-id')
    if aff_id:
        aff_m_list = aff_list.filter(id=aff_id)
    else:
        aff_m_list = aff_list
    camp_track_links = CampaignTrackerMapping.objects.filter(
            fk_affiliateinfo__in=aff_m_list,
            fk_campaign__siteid__in=site_ids).select_related('fk_campaign', 'fk_affiliateinfo')
    count = camp_track_links.count()
    start_record, end_record, start_page, \
            end_page, next_page, cur_page, page_count = get_paginate_dimensions(page, starting_page,
                                                                                nexting_page, previous_page,
                                                                                count, number_of_items)
    #from_date = date(2012, 01,01)
    #to_date = datetime.now()+timedelta(days=1)
    header_list, data_list, currency = Process_activedepositor_stats(acc_manager, from_date, to_date, camp_track_links, site_ids, country)


    pagination_div  = makePaginationDiv(start_page, page_count, cur_page, next_page, count)

    context_dict = {"data_list":data_list,"header_list":header_list,
                        "from_date":from_date.strftime('%B %d, %Y'),"to_date":to_date.strftime('%B %d, %Y') ,
                        "ui_from_date":datetime_or_date_to_string(from_date),
                        "ui_to_date":datetime_or_date_to_string(to_date),
                       'campaign_id': campaign_id,"pagination_div":pagination_div, 'domain': site_name,
                       'currency': currency}
    if format_d == "json":
        context_dict["status"] = "success"
        context_dict['data_list'] = data_list
        context_dict["header_list"] = header_list
        return HttpResponse(json.dumps(context_dict, indent=4))
    context_dict['sites_obj_list'] = site_ids_list
    context_dict['siteid'] = sitefilter if sitefilter else []
    context_dict['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
    context_dict["aff_list"] = [{'id':i.id, 'username': i.email.replace('vcommsubaff', '')} for i in aff_list]
    context_dict['data_list'] = json.dumps(data_list)
    context_dict["country_list"] = country_list
    context_dict["header_list"] = json.dumps(header_list)
    context_dict['partner'] = acc_manager.affiliateprogramme.name
    context_dict['partner_obj'] = acc_manager.affiliateprogramme
    context_dict['part_user'] = ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
    # context_dict["channels_list"] = get_channels_list()
    # context_dict["channel"] = channel if channel else "All"
    return render(request, 'partner/activedeositreport.html', context_dict)

@user_role_management('Earnings Report')
def viewtrackersinfoadmin(request, **kwargs):
    context_dict = {}
    acc_manager = kwargs.get('acc_manager')
    partuser_obj = kwargs.get('partuser_obj')
    user = kwargs.get('user')
    net_ids_list = []
    number_of_items = 10000
    nexting_page = 0
    previous_page = 0
    starting_page = 0
    page = 0
    site_ids_list = get_brand_order(acc_manager)
    site_ids = site_ids_list
    format_d = request.POST.get("format")
    domain = None
    site_name = 'All Websites'
    if request.method == "GET":
        custom = "Custom"
        fromdate = request.GET.get("fromdate")
        todate = request.GET.get("todate")
        if not fromdate or not todate:
            custom = None
        campaign_id = None
        starting_page = request.GET.get('start_page')
        nexting_page = request.GET.get('next')
        previous_page = request.GET.get('prev')
        page = request.GET.get('page')
        if not starting_page  and not nexting_page and not previous_page and not page:
            form_data = {}
            request.session['partner_viewtrackersinfo'] = form_data
        else:
            form_data = request.session.get('partner_viewtrackersinfo')
            custom = form_data.get("customid")
            fromdate = form_data.get("fromdate")
            todate = form_data.get("todate")
            siteids = form_data.get("siteids")
            campaign_id = form_data.get("campaign_id")
        if not fromdate and not todate:
            custom = "Custom"
            todate = datetime.now().strftime("%m/%d/%Y")
            current_month , current_year =  datetime.now().month, datetime.now().year
            fromdate = "%s/01/%s" % (current_month, current_year )
    else:
        custom = str(request.POST.get("customid"))
        fromdate = str(request.POST.get("fromdate"))
        todate = str(request.POST.get("todate"))
        siteids = request.POST.getlist("siteids")
        domain = request.POST.get("sitename")

        if domain:
            format_d = 'reload'
        site_ids = site_ids_list
        if domain and domain != 'All':
            site_domain_list = domain.split(',')
            site_ids = site_ids_list.filter(domain__in=site_domain_list)
            site_name = 'Multiple sites.'
            if len(domain.split(',')) <2:
                site_name = site_ids_list.get(domain=site_domain_list[0]).name
        campaign_id = request.POST.get('campaign_id')
        if campaign_id == 'all':
            campaign_id = None
        
        form_data = {'fromdate':fromdate, 'todate':todate,'customid':custom, "siteids":siteids,
                     "campaign_id":campaign_id}
        request.session['partner_viewtrackersinfo'] = form_data

    if campaign_id:
        campaign_id = int(campaign_id)
    
    sitefilter = request.POST.getlist("siteid")
    campfilter = request.POST.get("campfilter")
    aff_id = request.POST.get('aff_id')
    currencyfilter = request.POST.get("currency", 'euro')
    group_d = str(request.POST.get("groupby", 'brand'))
    country_list = Country.objects.all().values_list('countryname', flat=True).distinct()
    country = request.POST.getlist('country')
    # if country:
    #     country = [country]
    # else:
    #     country = country_list
    camp_links = Campaign.objects.filter(
                fk_affiliateinfo__account_manager=acc_manager).order_by('-id')

    if campfilter:
        camp_links = camp_links.filter(id=campfilter)
        if campfilter:
            campaign_id = int(campfilter)
    if sitefilter:
        site_ids = site_ids.filter(id__in=sitefilter)
    else:
        sitefilter = [str(i.id) for i in site_ids]
    date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
    from_date = date_times_dict.get('fromdate')
    to_date = date_times_dict.get('todate')
    print 'domain', domain, site_ids
    aff_list = Affiliateinfo.objects.filter(account_manager=acc_manager).order_by('-id')
    if aff_id:
        aff_m_list = aff_list.filter(id=aff_id)
    else:
        aff_m_list = aff_list
    camp_track_links = CampaignTrackerMapping.objects.filter(
            fk_affiliateinfo__in=aff_m_list,
            fk_campaign__siteid__in=site_ids).select_related('fk_campaign', 'fk_affiliateinfo')
    count = camp_track_links.count()
    start_record, end_record, start_page, \
            end_page, next_page, cur_page, page_count = get_paginate_dimensions(page, starting_page,
                                                                                nexting_page, previous_page,
                                                                                count, number_of_items)
    #from_date = date(2012, 01,01)
    #to_date = datetime.now()+timedelta(days=1)
    # if group_d != 'campaign':
    header_list, data_list, currency = Process_campaign_admin_stats(acc_manager, from_date, to_date,
                        camp_track_links, site_ids, aff_m_list, country, group_d, currencyfilter)
    # else:
    #     header_list, data_list, currency = Process_campaign_camp_admin_stats(acc_manager, from_date, to_date,
    #                     camp_track_links, site_ids, aff_m_list, country, currencyfilter)

    pagination_div  = makePaginationDiv(start_page, page_count, cur_page, next_page, count)

    context_dict = {"data_list":data_list,"header_list":header_list,
                        "from_date":from_date.strftime('%B %d, %Y'),"to_date":to_date.strftime('%B %d, %Y') ,
                        "ui_from_date":datetime_or_date_to_string(from_date),
                        "ui_to_date":datetime_or_date_to_string(to_date),
                       'campaign_id': campaign_id,"pagination_div":pagination_div, 'domain': site_name,
                       'currency': currency}
    if format_d == "json":
        context_dict["status"] = "success"
        context_dict['data_list'] = data_list
        context_dict["header_list"] = header_list
        return HttpResponse(json.dumps(context_dict, indent=4))
    context_dict['sites_obj_list'] = site_ids_list
    context_dict["aff_list"] = [{'id':i.id, 'username': i.email.replace('vcommsubaff', '')} for i in aff_list]
    context_dict["country_list"] = country_list
    context_dict['data_list'] = json.dumps(data_list)
    context_dict['siteid'] = sitefilter if sitefilter else []
    context_dict['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
    context_dict["header_list"] = json.dumps(header_list)
    context_dict['partner'] = acc_manager.affiliateprogramme.name
    context_dict['partner_obj'] = acc_manager.affiliateprogramme
    context_dict['part_user'] = ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
    # context_dict["channels_list"] = get_channels_list()
    # context_dict["channel"] = channel if channel else "All"
    return render(request, 'partner/viewtrackers.html', context_dict)
    # return render_to_response('affiliates/viewtrackers.html', context_dict,
    #                               context_instance=RequestContext(request))

@user_role_management('Sub Affiliate Report')
def sub_affiliate_report(request, **kwargs):
    context_dict = {}
    acc_manager = kwargs.get('acc_manager')
    partuser_obj = kwargs.get('partuser_obj')
    user = kwargs.get('user')
    net_ids_list = []
    number_of_items = 10000
    nexting_page = 0
    previous_page = 0
    starting_page = 0
    page = 0
    site_ids_list = get_brand_order(acc_manager)
    affiliates_obj = Affiliateinfo.objects.filter(account_manager=acc_manager).exclude(username=user.username)
    site_ids = site_ids_list
    format_d = request.POST.get("format")
    domain = None
    aff_id = ''
    site_name = 'All Websites'
    aff_id = request.GET.get('aff_id')
    if request.method == "GET":
        custom = "Custom"
        fromdate = request.GET.get("fromdate")
        todate = request.GET.get("todate")
        if not fromdate or not todate:
            custom = None
        campaign_id = None
        starting_page = request.GET.get('start_page')
        nexting_page = request.GET.get('next')
        previous_page = request.GET.get('prev')
        page = request.GET.get('page')
        subaff_id = None
        # device = request.POST.get("device", 'Desktop')
        if not starting_page  and not nexting_page and not previous_page and not page:
            form_data = {}
            request.session['affiliate_campaignsinfo'] = form_data
        else:
            form_data = request.session.get('affiliate_campaignsinfo')
            custom = form_data.get("customid")
            fromdate = form_data.get("fromdate")
            todate = form_data.get("todate")
            siteids = form_data.get("siteids")
            campaign_id = form_data.get("campaign_id")
        if not fromdate and not todate:
            custom = "Custom"
            todate = datetime.now().strftime("%m/%d/%Y")
            current_month , current_year =  datetime.now().month, datetime.now().year
            fromdate = "%s/01/%s" % (current_month, current_year )
    else:
        custom = str(request.POST.get("customid"))
        fromdate = str(request.POST.get("fromdate"))
        todate = str(request.POST.get("todate"))
        siteids = request.POST.getlist("siteids")
        aff_id = request.POST.get('aff_id')
        subaff_id = request.POST.get('sub_aff_id')
        campaign_id = request.POST.get('campaign_id')
        
        domain = request.POST.get("sitename")
        if domain:
            format_d = 'reload'
        site_ids = site_ids_list
        print 'site_ids', site_ids_list
        if domain and domain != 'All':
            site_domain_list = domain.split(',')
            print 'site_ids', site_domain_list
            site_ids = site_ids_list.filter(domain__in=site_domain_list)
            print 'site_ids_l', site_ids
            site_name = 'Multiple sites.'
            if len(domain.split(',')) <2:
                site_name = site_ids_list.get(domain=site_domain_list[0]).name
        
        # device = request.POST.get("device", 'Desktop')
        if campaign_id == 'all':
            campaign_id = None
        form_data = {'fromdate':fromdate, 'todate':todate,'customid':custom, "siteids":siteids,
                     "campaign_id":campaign_id}
        request.session['affiliate_campaignsinfo'] = form_data

    country_list = Country.objects.all().values_list('countryname', flat=True).distinct()
    country = request.POST.getlist('country')
    currencyfilter = request.POST.get("currency", 'euro')
    group_d = str(request.POST.get("groupby", 'affiliate'))
    # if country:
    #     country = [country]
    # else:
    #     country = country_list
    if campaign_id:
        campaign_id = int(campaign_id)
    sitefilter = request.POST.getlist("siteid")
    if sitefilter:
        site_ids = site_ids.filter(id__in=sitefilter)
    else:
        sitefilter = [str(i.id) for i in site_ids]
    if aff_id:
        affiliates_m_obj = affiliates_obj.filter(id=aff_id)
    else:
        affiliates_m_obj = affiliates_obj

    if subaff_id:
        affiliates_m_obj = affiliates_obj.filter(id=subaff_id)
    # filter_networks = [1]
    # if siteids:
    #     filter_sites = [int(i) for i in siteids]
    # else:
    #     filter_sites = [int(i.id) for i in site_ids_list]
    # siteid_list = get_site_ids_data_list(site_ids_list, filter_sites)

    date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
    from_date = date_times_dict.get('fromdate')
    to_date = date_times_dict.get('todate')
    # camp_links = Campaign.objects.filter(fk_affiliateinfo=obj, siteid__in=site_ids).select_related('siteid').\
    #                     order_by('-id')
    camp_track_links = CampaignTrackerMapping.objects.filter(
        fk_affiliateinfo__in = affiliates_m_obj,
        fk_affiliateinfo__account_manager=acc_manager, fk_campaign__siteid__in=site_ids)
    count = camp_track_links.count()
    start_record, end_record, start_page, \
            end_page, next_page, cur_page, page_count = get_paginate_dimensions(page, starting_page,
                                                                                nexting_page, previous_page,
                                                                                count, number_of_items)
    #from_date = date(2012, 01,01)
    #to_date = datetime.now()+timedelta(days=1)
    # header_list, data_list, currency = affiliate_reports_stats(affiliates_m_obj, from_date, to_date, camp_track_links, site_ids, country)

    header_list, data_list, currency = sub_affiliate_group_report_stats(acc_manager, from_date, to_date, camp_track_links,
                    site_ids, affiliates_m_obj, country, group_d, currencyfilter)
    pagination_div  = makePaginationDiv(start_page, page_count, cur_page, next_page, count)

    context_dict = {"data_list":data_list,"header_list":header_list,
                        "from_date":from_date.strftime('%B %d, %Y'),"to_date":to_date.strftime('%B %d, %Y') ,
                        "ui_from_date":datetime_or_date_to_string(from_date),
                        "ui_to_date":datetime_or_date_to_string(to_date),
                       'campaign_id': campaign_id,"pagination_div":pagination_div,
                       'currency': currency, 'domain': site_name, 'aff_id':aff_id
                       }

    if format_d == "json":
        context_dict["status"] = "success"
        context_dict['data_list'] = data_list
        context_dict["header_list"] = header_list
        return HttpResponse(json.dumps(context_dict, indent=4))
    context_dict['sites_obj_list'] = site_ids_list
    context_dict['siteid'] = sitefilter if sitefilter else []
    context_dict['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
    sub_aff_obj = Affiliateinfo.objects.filter(superaffiliate__in=affiliates_obj)
    super_aff_obj = []
    sub_aff_dict = {}
    for i in affiliates_obj:
        sub_aff_dict[i.id] = [[j[0], str(j[1]).replace('vcommsubaff', '')] for j in Affiliateinfo.objects.filter(superaffiliate=i).values_list('id', 'email')]
        if sub_aff_dict[i.id]:
            super_aff_obj.append(i)
    context_dict["aff_list"] = [{'id':i.id, 'username': i.email.replace('vcommsubaff', '')} for i in super_aff_obj]
    context_dict["sub_aff_dict"] = json.dumps(sub_aff_dict)
    context_dict['partner'] = acc_manager.affiliateprogramme.name
    context_dict['partner_obj'] = acc_manager.affiliateprogramme
    context_dict["country_list"] = country_list
    context_dict['part_user'] = ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
    # context_dict["campaign_list"] = campaign_list
    context_dict['data_list'] = json.dumps(data_list)
    context_dict["header_list"] = json.dumps(header_list)
    # context_dict["channels_list"] = get_channels_list()
    # context_dict["channel"] = channel if channel else "All"
    return render(request, 'partner/subaffiliatereport.html', context_dict)


@user_role_management('Device Report')
def devicestatsinfo(request, **kwargs):
    context_dict = {}
    acc_manager = kwargs.get('acc_manager')
    partuser_obj = kwargs.get('partuser_obj')
    user = kwargs.get('user')
    # campaign_list = Campaign.objects.filter(fk_affiliateinfo=obj)
    net_ids_list = []

    number_of_items = 10000
    nexting_page = 0
    previous_page = 0
    starting_page = 0
    page = 0
    site_ids_list = get_brand_order(acc_manager)
    site_ids = site_ids_list
    format_d = request.POST.get("format")
    domain = None
    site_name = 'All Websites'
    if request.method == "GET":
        custom = "Custom"
        fromdate = request.GET.get("fromdate")
        todate = request.GET.get("todate")
        if not fromdate or not todate:
            custom = None
        campaign_id = None
        starting_page = request.GET.get('start_page')
        nexting_page = request.GET.get('next')
        previous_page = request.GET.get('prev')
        page = request.GET.get('page')
        device = request.POST.get("device", 'All')
        if not starting_page  and not nexting_page and not previous_page and not page:
            form_data = {}
            request.session['affiliate_deviceinfo'] = form_data
        else:
            form_data = request.session.get('affiliate_deviceinfo')
            custom = form_data.get("customid")
            fromdate = form_data.get("fromdate")
            todate = form_data.get("todate")
            siteids = form_data.get("siteids")
            campaign_id = form_data.get("campaign_id")
        if not fromdate and not todate:
            custom = "Custom"
            todate = datetime.now().strftime("%m/%d/%Y")
            current_month , current_year =  datetime.now().month, datetime.now().year
            fromdate = "%s/01/%s" % (current_month, current_year )
    else:
        custom = str(request.POST.get("customid"))
        fromdate = str(request.POST.get("fromdate"))
        todate = str(request.POST.get("todate"))
        siteids = request.POST.getlist("siteids")
        campaign_id = request.POST.get('campaign_id')
        aff_id = request.POST.get('aff_id')
        domain = request.POST.get("sitename")

        if domain:
            format_d = 'reload'
        site_ids = site_ids_list
        if domain and domain != 'All':
            site_domain_list = domain.split(',')
            site_ids = site_ids_list.filter(domain__in=site_domain_list)
            site_name = 'Multiple sites.'
            if len(domain.split(',')) <2:
                site_name = site_ids_list.get(domain=site_domain_list[0]).name
        
        device = request.POST.get("device", 'All')
        if campaign_id == 'all':
            campaign_id = None
        form_data = {'fromdate':fromdate, 'todate':todate,'customid':custom, "siteids":siteids,
                     "campaign_id":campaign_id}
        request.session['affiliate_deviceinfo'] = form_data

    if campaign_id:
        campaign_id = int(campaign_id)
    # filter_networks = [1]
    # if siteids:
    #     filter_sites = [int(i) for i in siteids]
    # else:
    #     filter_sites = [int(i.id) for i in site_ids_list]
    # siteid_list = get_site_ids_data_list(site_ids_list, filter_sites)
    sitefilter = request.POST.getlist("siteid")
    campfilter = request.POST.get("campfilter")
    aff_id = request.POST.get('aff_id')
    country_list = Country.objects.all().values_list('countryname', flat=True).distinct()
    country = request.POST.getlist('country')
    # if country:
    #     country = [country]
    if not country:
        country = country_list
    camp_links = Campaign.objects.filter(
        fk_affiliateinfo__account_manager=acc_manager).order_by('-id')

    if campfilter:
        camp_links = camp_links.filter(id=campfilter)
        if campfilter:
            campaign_id = int(campfilter)
    if sitefilter:
        site_ids = site_ids.filter(id__in=sitefilter)
    else:
        sitefilter = [str(i.id) for i in site_ids]
    date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
    from_date = date_times_dict.get('fromdate')
    to_date = date_times_dict.get('todate')
    # camp_links = Campaign.objects.filter(fk_affiliateinfo=obj, siteid__in=site_ids).select_related('siteid').\
    #                     order_by('-id')
    aff_list = Affiliateinfo.objects.filter(account_manager=acc_manager).order_by('-id')
    if aff_id:
        aff_m_list = aff_list.filter(id=aff_id)
    else:
        aff_m_list = aff_list
    camp_track_links = CampaignTrackerMapping.objects.filter(
            fk_affiliateinfo__in=aff_m_list,
            fk_campaign__siteid__in=site_ids).select_related('fk_campaign', 'fk_affiliateinfo')
    count = camp_track_links.count()
    start_record, end_record, start_page, \
            end_page, next_page, cur_page, page_count = get_paginate_dimensions(page, starting_page,
                                                                                nexting_page, previous_page,
                                                                                count, number_of_items)
    #from_date = date(2012, 01,01)
    #to_date = datetime.now()+timedelta(days=1)
    header_list, data_list, currency = device_report_stats(acc_manager, from_date, to_date, camp_track_links, device, site_ids, country)


    pagination_div  = makePaginationDiv(start_page, page_count, cur_page, next_page, count)

    context_dict = {"data_list":data_list,"header_list":header_list,
                        "from_date":from_date.strftime('%B %d, %Y'),"to_date":to_date.strftime('%B %d, %Y') ,
                        "ui_from_date":datetime_or_date_to_string(from_date),
                        "ui_to_date":datetime_or_date_to_string(to_date),
                       'campaign_id': campaign_id,"pagination_div":pagination_div,
                       'devices': ["All", "Mobile", "Web"], 'd':device, 'domain': site_name, 'currency': currency}

    if format_d == "json":
        context_dict["status"] = "success"
        context_dict['data_list'] = data_list
        context_dict["header_list"] = header_list
        return HttpResponse(json.dumps(context_dict, indent=4))
    context_dict['sites_obj_list'] = site_ids_list
    context_dict['siteid'] = sitefilter if sitefilter else []
    context_dict['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
    context_dict["aff_list"] = [{'id':i.id, 'username': i.email.replace('vcommsubaff', '')} for i in aff_list]
    context_dict['partner'] = acc_manager.affiliateprogramme.name
    context_dict['partner_obj'] = acc_manager.affiliateprogramme
    context_dict["country_list"] = country_list
    context_dict['part_user'] = ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
    context_dict['data_list'] = json.dumps(data_list)
    context_dict["header_list"] = json.dumps(header_list)
    # context_dict["channels_list"] = get_channels_list()
    # context_dict["channel"] = channel if channel else "All"
    return render(request, 'partner/devicereportinfo.html', context_dict)


@user_role_management('Affiliate Activity')
def affiliatereportinfo(request, **kwargs):
    context_dict = {}
    acc_manager = kwargs.get('acc_manager')
    partuser_obj = kwargs.get('partuser_obj')
    user = kwargs.get('user')
    net_ids_list = []
    number_of_items = 10000
    nexting_page = 0
    previous_page = 0
    starting_page = 0
    page = 0
    site_ids_list = get_brand_order(acc_manager)
    affiliates_obj = Affiliateinfo.objects.filter(account_manager=acc_manager) #.exclude(username=user.username)
    site_ids = site_ids_list
    format_d = request.POST.get("format")
    domain = None
    aff_id = ''
    site_name = 'All Websites'
    aff_id = request.GET.get('aff_id')
    if request.method == "GET":
        custom = "Custom"
        fromdate = request.GET.get("fromdate")
        todate = request.GET.get("todate")
        if not fromdate or not todate:
            custom = None
        campaign_id = None
        starting_page = request.GET.get('start_page')
        nexting_page = request.GET.get('next')
        previous_page = request.GET.get('prev')
        page = request.GET.get('page')
        # device = request.POST.get("device", 'Desktop')
        if not starting_page  and not nexting_page and not previous_page and not page:
            form_data = {}
            request.session['affiliate_campaignsinfo'] = form_data
        else:
            form_data = request.session.get('affiliate_campaignsinfo')
            custom = form_data.get("customid")
            fromdate = form_data.get("fromdate")
            todate = form_data.get("todate")
            siteids = form_data.get("siteids")
            campaign_id = form_data.get("campaign_id")
        if not fromdate and not todate:
            custom = "Custom"
            todate = datetime.now().strftime("%m/%d/%Y")
            current_month , current_year =  datetime.now().month, datetime.now().year
            fromdate = "%s/01/%s" % (current_month, current_year )
    else:
        custom = str(request.POST.get("customid"))
        fromdate = str(request.POST.get("fromdate"))
        todate = str(request.POST.get("todate"))
        siteids = request.POST.getlist("siteids")
        aff_id = request.POST.get('aff_id')
        campaign_id = request.POST.get('campaign_id')
        
        domain = request.POST.get("sitename")
        if domain:
            format_d = 'reload'
        site_ids = site_ids_list
        print 'site_ids', site_ids_list
        if domain and domain != 'All':
            site_domain_list = domain.split(',')
            print 'site_ids', site_domain_list
            site_ids = site_ids_list.filter(domain__in=site_domain_list)
            print 'site_ids_l', site_ids
            site_name = 'Multiple sites.'
            if len(domain.split(',')) <2:
                site_name = site_ids_list.get(domain=site_domain_list[0]).name
        
        # device = request.POST.get("device", 'Desktop')
        if campaign_id == 'all':
            campaign_id = None
        form_data = {'fromdate':fromdate, 'todate':todate,'customid':custom, "siteids":siteids,
                     "campaign_id":campaign_id}
        request.session['affiliate_campaignsinfo'] = form_data

    country_list = Country.objects.all().values_list('countryname', flat=True).distinct()
    country = request.POST.getlist('country')
    currencyfilter = request.POST.get("currency", 'euro')
    group_d = str(request.POST.get("groupby", 'affiliate'))
    # if country:
    #     country = [country]
    # else:
    #     country = country_list
    if campaign_id:
        campaign_id = int(campaign_id)
    sitefilter = request.POST.getlist("siteid")
    if sitefilter:
        site_ids = site_ids.filter(id__in=sitefilter)
    else:
        sitefilter = [str(i.id) for i in site_ids]
    if aff_id:
        affiliates_m_obj = affiliates_obj.filter(id=aff_id)
    else:
        affiliates_m_obj = affiliates_obj
    # filter_networks = [1]
    # if siteids:
    #     filter_sites = [int(i) for i in siteids]
    # else:
    #     filter_sites = [int(i.id) for i in site_ids_list]
    # siteid_list = get_site_ids_data_list(site_ids_list, filter_sites)

    date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
    from_date = date_times_dict.get('fromdate')
    to_date = date_times_dict.get('todate')
    # camp_links = Campaign.objects.filter(fk_affiliateinfo=obj, siteid__in=site_ids).select_related('siteid').\
    #                     order_by('-id')
    camp_track_links = CampaignTrackerMapping.objects.filter(
        fk_affiliateinfo__in = affiliates_m_obj,
        fk_affiliateinfo__account_manager=acc_manager, fk_campaign__siteid__in=site_ids)
    count = camp_track_links.count()
    start_record, end_record, start_page, \
            end_page, next_page, cur_page, page_count = get_paginate_dimensions(page, starting_page,
                                                                                nexting_page, previous_page,
                                                                                count, number_of_items)
    #from_date = date(2012, 01,01)
    #to_date = datetime.now()+timedelta(days=1)
    # header_list, data_list, currency = affiliate_reports_stats(affiliates_m_obj, from_date, to_date, camp_track_links, site_ids, country)

    header_list, data_list, currency = affiliate_group_report_stats(acc_manager, from_date, to_date, camp_track_links,
                    site_ids, affiliates_m_obj, country, group_d, currencyfilter)
    pagination_div  = makePaginationDiv(start_page, page_count, cur_page, next_page, count)

    context_dict = {"data_list":data_list,"header_list":header_list,
                        "from_date":from_date.strftime('%B %d, %Y'),"to_date":to_date.strftime('%B %d, %Y') ,
                        "ui_from_date":datetime_or_date_to_string(from_date),
                        "ui_to_date":datetime_or_date_to_string(to_date),
                       'campaign_id': campaign_id,"pagination_div":pagination_div,
                       'currency': currency, 'domain': site_name, 'aff_id':aff_id
                       }

    if format_d == "json":
        context_dict["status"] = "success"
        context_dict['data_list'] = data_list
        context_dict["header_list"] = header_list
        return HttpResponse(json.dumps(context_dict, indent=4))
    context_dict['sites_obj_list'] = site_ids_list
    context_dict['siteid'] = sitefilter if sitefilter else []
    context_dict['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
    context_dict["aff_list"] = [{'id':i.id, 'username': i.email.replace('vcommsubaff', '')} for i in affiliates_obj]
    context_dict['partner'] = acc_manager.affiliateprogramme.name
    context_dict['partner_obj'] = acc_manager.affiliateprogramme
    context_dict["country_list"] = country_list
    context_dict['part_user'] = ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
    # context_dict["campaign_list"] = campaign_list
    context_dict['data_list'] = json.dumps(data_list)
    context_dict["header_list"] = json.dumps(header_list)
    # context_dict["channels_list"] = get_channels_list()
    # context_dict["channel"] = channel if channel else "All"
    return render(request, 'partner/affiliatereport.html', context_dict)


@user_role_management('Brand Report')
def advertisersreportinfo(request, **kwargs):
    context_dict = {}
    acc_manager = kwargs.get('acc_manager')
    partuser_obj = kwargs.get('partuser_obj')
    user = kwargs.get('user')
    net_ids_list = []
    network_site_obj = NetworkAndSiteMap.objects.filter(networkid__in=acc_manager.affiliateprogramme.networkid.all())
    affiliates_obj = Affiliateinfo.objects.filter(account_manager=acc_manager)
    number_of_items = 10000
    nexting_page = 0
    previous_page = 0
    starting_page = 0
    page = 0
    site_ids_list = get_brand_order(acc_manager)
    site_ids = site_ids_list
    format_d = request.POST.get("format")
    domain = None
    site_name = 'All Websites'
    if request.method == "GET":
        custom = "Custom"
        fromdate = request.GET.get("fromdate")
        todate = request.GET.get("todate")
        if not fromdate or not todate:
            custom = None
        campaign_id = None
        starting_page = request.GET.get('start_page')
        nexting_page = request.GET.get('next')
        previous_page = request.GET.get('prev')
        page = request.GET.get('page')
        # device = request.POST.get("device", 'Desktop')
        if not starting_page  and not nexting_page and not previous_page and not page:
            form_data = {}
            request.session['affiliate_campaignsinfo'] = form_data
        else:
            form_data = request.session.get('affiliate_campaignsinfo')
            custom = form_data.get("customid")
            fromdate = form_data.get("fromdate")
            todate = form_data.get("todate")
            siteids = form_data.get("siteids")
            campaign_id = form_data.get("campaign_id")
        if not fromdate and not todate:
            custom = "Custom"
            todate = datetime.now().strftime("%m/%d/%Y")
            current_month , current_year =  datetime.now().month, datetime.now().year
            fromdate = "%s/01/%s" % (current_month, current_year )
    else:
        custom = str(request.POST.get("customid"))
        fromdate = str(request.POST.get("fromdate"))
        todate = str(request.POST.get("todate"))
        siteids = request.POST.getlist("siteids")
        campaign_id = request.POST.get('campaign_id')
        
        domain = request.POST.get("sitename")
        if domain:
            format_d = 'reload'
        site_ids = site_ids_list
        print 'site_ids', site_ids_list
        if domain and domain != 'All':
            site_domain_list = domain.split(',')
            print 'site_ids', site_domain_list
            site_ids = site_ids_list.filter(domain__in=site_domain_list)
            print 'site_ids_l', site_ids
            site_name = 'Multiple sites.'
            if len(domain.split(',')) <2:
                site_name = site_ids_list.get(domain=site_domain_list[0]).name
        
        # device = request.POST.get("device", 'Desktop')
        if campaign_id == 'all':
            campaign_id = None
        form_data = {'fromdate':fromdate, 'todate':todate,'customid':custom, "siteids":siteids,
                     "campaign_id":campaign_id}
        request.session['affiliate_campaignsinfo'] = form_data

    country_list = Country.objects.all().values_list('countryname', flat=True).distinct()
    country = request.POST.getlist('country')
    aff_id = request.POST.get('aff_id')
    currencyfilter = request.POST.get("currency", 'euro')
    group_d = str(request.POST.get("groupby", 'brand'))
    # if country:
    #     country = [country]
    if not country:
        country = country_list
    if campaign_id:
        campaign_id = int(campaign_id)
    sitefilter = request.POST.getlist("siteid")
    if sitefilter:
        site_ids = site_ids.filter(id__in=sitefilter)
    else:
        sitefilter = [str(i.id) for i in site_ids]
    # filter_networks = [1]
    # if siteids:
    #     filter_sites = [int(i) for i in siteids]
    # else:
    #     filter_sites = [int(i.id) for i in site_ids_list]
    # siteid_list = get_site_ids_data_list(site_ids_list, filter_sites)
    aff_list = Affiliateinfo.objects.filter(account_manager=acc_manager).order_by('-id')
    if aff_id:
        aff_m_list = aff_list.filter(id=aff_id)
    else:
        aff_m_list = aff_list
    date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
    from_date = date_times_dict.get('fromdate')
    to_date = date_times_dict.get('todate')
    # camp_links = Campaign.objects.filter(fk_affiliateinfo=obj, siteid__in=site_ids).select_related('siteid').\
    #                     order_by('-id')
    camp_track_links = CampaignTrackerMapping.objects.filter(fk_affiliateinfo__account_manager=acc_manager, fk_campaign__siteid__in=site_ids)
    count = camp_track_links.count()
    start_record, end_record, start_page, \
            end_page, next_page, cur_page, page_count = get_paginate_dimensions(page, starting_page,
                                                                                nexting_page, previous_page,
                                                                                count, number_of_items)
    #from_date = date(2012, 01,01)
    #to_date = datetime.now()+timedelta(days=1)
    header_list, data_list, currency = advertiser_reports_stats(aff_m_list, from_date, to_date,
         site_ids, currencyfilter, country)

    pagination_div  = makePaginationDiv(start_page, page_count, cur_page, next_page, count)

    context_dict = {"data_list":data_list,"header_list":header_list,
                        "from_date":from_date.strftime('%B %d, %Y'),"to_date":to_date.strftime('%B %d, %Y') ,
                        "ui_from_date":datetime_or_date_to_string(from_date),
                        "ui_to_date":datetime_or_date_to_string(to_date),
                       'campaign_id': campaign_id,"pagination_div":pagination_div,
                       'currency': currency, 'domain': site_name
                       }

    if format_d == "json":
        context_dict["status"] = "success"
        context_dict['data_list'] = data_list
        context_dict["header_list"] = header_list
        return HttpResponse(json.dumps(context_dict, indent=4))
    context_dict['sites_obj_list'] = site_ids_list
    context_dict['siteid'] = sitefilter if sitefilter else []
    context_dict['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
    context_dict["aff_list"] = [{'id':i.id, 'username': i.email.replace('vcommsubaff', '')} for i in aff_list]
    context_dict['partner'] = acc_manager.affiliateprogramme.name
    context_dict['partner_obj'] = acc_manager.affiliateprogramme
    context_dict["country_list"] = country_list
    context_dict['part_user'] = ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
    # context_dict["campaign_list"] = campaign_list
    context_dict['data_list'] = json.dumps(data_list)
    context_dict["header_list"] = json.dumps(header_list)
    # context_dict["channels_list"] = get_channels_list()
    # context_dict["channel"] = channel if channel else "All"
    return render(request, 'partner/advertisersreport.html', context_dict)


@user_role_management('Dynamic report')
def dynamicreportinfo(request, **kwargs):
    context_dict = {}
    acc_manager = kwargs.get('acc_manager')
    partuser_obj = kwargs.get('partuser_obj')
    user = kwargs.get('user')
    net_ids_list = []

    number_of_items = 10000
    nexting_page = 0
    previous_page = 0
    starting_page = 0
    page = 0
    site_ids_list = get_brand_order(acc_manager)
    site_ids = site_ids_list
    format_d = request.POST.get("format")
    domain = None
    site_name = 'All Websites'
    if request.method == "GET":
        custom = "Custom"
        fromdate = request.GET.get("fromdate")
        todate = request.GET.get("todate")
        if not fromdate or not todate:
            custom = None
        campaign_id = None
        starting_page = request.GET.get('start_page')
        nexting_page = request.GET.get('next')
        previous_page = request.GET.get('prev')
        page = request.GET.get('page')
        # device = request.POST.get("device", 'Desktop')
        if not starting_page  and not nexting_page and not previous_page and not page:
            form_data = {}
            request.session['affiliate_campaignsinfo'] = form_data
        else:
            form_data = request.session.get('affiliate_campaignsinfo')
            custom = form_data.get("customid")
            fromdate = form_data.get("fromdate")
            todate = form_data.get("todate")
            siteids = form_data.get("siteids")
            campaign_id = form_data.get("campaign_id")
        if not fromdate and not todate:
            custom = "Custom"
            todate = datetime.now().strftime("%m/%d/%Y")
            current_month , current_year =  datetime.now().month, datetime.now().year
            fromdate = "%s/01/%s" % (current_month, current_year )
    else:
        custom = str(request.POST.get("customid"))
        fromdate = str(request.POST.get("fromdate"))
        todate = str(request.POST.get("todate"))
        siteids = request.POST.getlist("siteids")
        campaign_id = request.POST.get('campaign_id')
        
        domain = request.POST.get("sitename")
        if domain:
            format_d = 'reload'
        site_ids = site_ids_list
        print 'site_ids', site_ids_list
        if domain and domain != 'All':
            site_domain_list = domain.split(',')
            print 'site_ids', site_domain_list
            site_ids = site_ids_list.filter(domain__in=site_domain_list)
            print 'site_ids_l', site_ids
            site_name = 'Multiple sites.'
            if len(domain.split(',')) <2:
                site_name = site_ids_list.get(domain=site_domain_list[0]).name
        
        # device = request.POST.get("device", 'Desktop')
        if campaign_id == 'all':
            campaign_id = None
        form_data = {'fromdate':fromdate, 'todate':todate,'customid':custom, "siteids":siteids,
                     "campaign_id":campaign_id}
        request.session['affiliate_campaignsinfo'] = form_data

    if campaign_id:
        campaign_id = int(campaign_id)
    # filter_networks = [1]
    # if siteids:
    #     filter_sites = [int(i) for i in siteids]
    # else:
    #     filter_sites = [int(i.id) for i in site_ids_list]
    # siteid_list = get_site_ids_data_list(site_ids_list, filter_sites)
    # aff_list = Affiliateinfo.objects.filter(account_manager=acc_manager,
    #     siteid__in=site_ids).select_related('siteid').\
    #                     order_by('-id')
    sitefilter = request.POST.getlist("siteid")
    campfilter = request.POST.get("campfilter")
    aff_id = request.POST.get('aff_id')
    country_list = Country.objects.all().values_list('countryname', flat=True).distinct()
    country = request.POST.getlist('country')
    currencyfilter = request.POST.get("currency", 'euro')
    group_d = str(request.POST.get("groupby", 'dynamic'))
    # if country:
    #     country = [country]
    # else:
    #     country = country_list
    camp_links = Campaign.objects.filter(
                    fk_affiliateinfo__account_manager=acc_manager,
                    siteid__in=site_ids).order_by('-id')

    if campfilter:
        camp_links = camp_links.filter(id=campfilter)
        if campfilter:
            campaign_id = int(campfilter)
    if sitefilter:
        site_ids = site_ids.filter(id__in=sitefilter)
    else:
        sitefilter = [str(i.id) for i in site_ids]
    date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
    from_date = date_times_dict.get('fromdate')
    to_date = date_times_dict.get('todate')
    
    aff_list = Affiliateinfo.objects.filter(account_manager=acc_manager).order_by('-id')
    if aff_id:
        aff_m_list = aff_list.filter(id=aff_id)
    else:
        aff_m_list = aff_list
    camp_track_links = CampaignTrackerMapping.objects.filter(
            fk_affiliateinfo__in=aff_m_list,
            fk_campaign__siteid__in=site_ids).select_related('fk_campaign', 'fk_affiliateinfo')
    count = camp_track_links.count()
    start_record, end_record, start_page, \
            end_page, next_page, cur_page, page_count = get_paginate_dimensions(page, starting_page,
                                                                                nexting_page, previous_page,
                                                                                count, number_of_items)
    #from_date = date(2012, 01,01)
    #to_date = datetime.now()+timedelta(days=1)
    header_list, data_list, currency = dynamic_reports_admin_stats(acc_manager, from_date, to_date,
                    camp_track_links, site_ids, aff_m_list, country, currencyfilter, group_d)


    pagination_div  = makePaginationDiv(start_page, page_count, cur_page, next_page, count)

    context_dict = {"data_list":data_list,"header_list":header_list,
                        "from_date":from_date.strftime('%B %d, %Y'),"to_date":to_date.strftime('%B %d, %Y') ,
                        "ui_from_date":datetime_or_date_to_string(from_date),
                        "ui_to_date":datetime_or_date_to_string(to_date),
                       'campaign_id': campaign_id,"pagination_div":pagination_div,
                       'currency': currency, 'domain': site_name
                       }

    if format_d == "json":
        context_dict["status"] = "success"
        context_dict['data_list'] = data_list
        context_dict["header_list"] = header_list
        return HttpResponse(json.dumps(context_dict, indent=4))
    context_dict['sites_obj_list'] = site_ids_list
    context_dict['siteid'] = sitefilter if sitefilter else []
    context_dict['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
    context_dict["aff_list"] = [{'id':i.id, 'username': i.email.replace('vcommsubaff', '')} for i in aff_list]
    context_dict['partner'] = acc_manager.affiliateprogramme.name
    context_dict['partner_obj'] = acc_manager.affiliateprogramme
    context_dict["country_list"] = country_list
    context_dict['part_user'] = ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
    # context_dict["campaign_list"] = campaign_list
    context_dict['data_list'] = json.dumps(data_list)
    context_dict["header_list"] = json.dumps(header_list)
    # context_dict["channels_list"] = get_channels_list()
    # context_dict["channel"] = channel if channel else "All"
    return render(request, 'partner/dynamicreport.html', context_dict)

@user_role_management('Campaigns report')
def campaignreportinfo(request, **kwargs):
    context_dict = {}
    acc_manager = kwargs.get('acc_manager')
    partuser_obj = kwargs.get('partuser_obj')
    user = kwargs.get('user')
    net_ids_list = []

    number_of_items = 10000
    nexting_page = 0
    previous_page = 0
    starting_page = 0
    page = 0
    site_ids_list = get_brand_order(acc_manager)
    site_ids = site_ids_list
    format_d = request.POST.get("format")
    domain = None
    site_name = 'All Websites'
    if request.method == "GET":
        custom = "Custom"
        fromdate = request.GET.get("fromdate")
        todate = request.GET.get("todate")
        if not fromdate or not todate:
            custom = None
        campaign_id = None
        starting_page = request.GET.get('start_page')
        nexting_page = request.GET.get('next')
        previous_page = request.GET.get('prev')
        page = request.GET.get('page')
        # device = request.POST.get("device", 'Desktop')
        if not starting_page  and not nexting_page and not previous_page and not page:
            form_data = {}
            request.session['affiliate_campaignsinfo'] = form_data
        else:
            form_data = request.session.get('affiliate_campaignsinfo')
            custom = form_data.get("customid")
            fromdate = form_data.get("fromdate")
            todate = form_data.get("todate")
            siteids = form_data.get("siteids")
            campaign_id = form_data.get("campaign_id")
        if not fromdate and not todate:
            custom = "Custom"
            todate = datetime.now().strftime("%m/%d/%Y")
            current_month , current_year =  datetime.now().month, datetime.now().year
            fromdate = "%s/01/%s" % (current_month, current_year )
    else:
        custom = str(request.POST.get("customid"))
        fromdate = str(request.POST.get("fromdate"))
        todate = str(request.POST.get("todate"))
        siteids = request.POST.getlist("siteids")
        campaign_id = request.POST.get('campaign_id')
        
        domain = request.POST.get("sitename")
        if domain:
            format_d = 'reload'
        site_ids = site_ids_list
        print 'site_ids', site_ids_list
        if domain and domain != 'All':
            site_domain_list = domain.split(',')
            print 'site_ids', site_domain_list
            site_ids = site_ids_list.filter(domain__in=site_domain_list)
            print 'site_ids_l', site_ids
            site_name = 'Multiple sites.'
            if len(domain.split(',')) <2:
                site_name = site_ids_list.get(domain=site_domain_list[0]).name
        
        # device = request.POST.get("device", 'Desktop')
        if campaign_id == 'all':
            campaign_id = None
        form_data = {'fromdate':fromdate, 'todate':todate,'customid':custom, "siteids":siteids,
                     "campaign_id":campaign_id}
        request.session['affiliate_campaignsinfo'] = form_data

    if campaign_id:
        campaign_id = int(campaign_id)
    # filter_networks = [1]
    # if siteids:
    #     filter_sites = [int(i) for i in siteids]
    # else:
    #     filter_sites = [int(i.id) for i in site_ids_list]
    # siteid_list = get_site_ids_data_list(site_ids_list, filter_sites)
    # aff_list = Affiliateinfo.objects.filter(account_manager=acc_manager,
    #     siteid__in=site_ids).select_related('siteid').\
    #                     order_by('-id')
    sitefilter = request.POST.getlist("siteid")
    campfilter = request.POST.get("campfilter")
    aff_id = request.POST.get('aff_id')
    country_list = Country.objects.all().values_list('countryname', flat=True).distinct()
    country = request.POST.getlist('country')
    currencyfilter = request.POST.get("currency", 'euro')
    group_d = str(request.POST.get("groupby", 'brand'))
    # if country:
    #     country = [country]
    # else:
    #     country = country_list
    camp_links = Campaign.objects.filter(
                    fk_affiliateinfo__account_manager=acc_manager,
                    siteid__in=site_ids).order_by('-id')

    if campfilter:
        camp_links = camp_links.filter(id=campfilter)
        if campfilter:
            campaign_id = int(campfilter)
    if sitefilter:
        site_ids = site_ids.filter(id__in=sitefilter)
    else:
        sitefilter = [str(i.id) for i in site_ids]
    date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
    from_date = date_times_dict.get('fromdate')
    to_date = date_times_dict.get('todate')
    
    aff_list = Affiliateinfo.objects.filter(account_manager=acc_manager).order_by('-id')
    if aff_id:
        aff_m_list = aff_list.filter(id=aff_id)
    else:
        aff_m_list = aff_list
    camp_track_links = CampaignTrackerMapping.objects.filter(
            fk_affiliateinfo__in=aff_m_list,
            fk_campaign__siteid__in=site_ids).select_related('fk_campaign', 'fk_affiliateinfo')
    count = camp_track_links.count()
    start_record, end_record, start_page, \
            end_page, next_page, cur_page, page_count = get_paginate_dimensions(page, starting_page,
                                                                                nexting_page, previous_page,
                                                                                count, number_of_items)
    #from_date = date(2012, 01,01)
    #to_date = datetime.now()+timedelta(days=1)
    header_list, data_list, currency = campaign_reports_admin_stats(acc_manager, from_date, to_date,
     camp_track_links, site_ids, aff_m_list, country, currencyfilter, group_d)


    pagination_div  = makePaginationDiv(start_page, page_count, cur_page, next_page, count)

    context_dict = {"data_list":data_list,"header_list":header_list,
                        "from_date":from_date.strftime('%B %d, %Y'),"to_date":to_date.strftime('%B %d, %Y') ,
                        "ui_from_date":datetime_or_date_to_string(from_date),
                        "ui_to_date":datetime_or_date_to_string(to_date),
                       'campaign_id': campaign_id,"pagination_div":pagination_div,
                       'currency': currency, 'domain': site_name
                       }

    if format_d == "json":
        context_dict["status"] = "success"
        context_dict['data_list'] = data_list
        context_dict["header_list"] = header_list
        return HttpResponse(json.dumps(context_dict, indent=4))
    context_dict['sites_obj_list'] = site_ids_list
    context_dict['siteid'] = sitefilter if sitefilter else []
    context_dict['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
    context_dict["aff_list"] = [{'id':i.id, 'username': i.email.replace('vcommsubaff', '')} for i in aff_list]
    context_dict['partner'] = acc_manager.affiliateprogramme.name
    context_dict['partner_obj'] = acc_manager.affiliateprogramme
    context_dict["country_list"] = country_list
    context_dict['part_user'] = ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
    # context_dict["campaign_list"] = campaign_list
    context_dict['data_list'] = json.dumps(data_list)
    context_dict["header_list"] = json.dumps(header_list)
    # context_dict["channels_list"] = get_channels_list()
    # context_dict["channel"] = channel if channel else "All"
    return render(request, 'partner/offersreport.html', context_dict)


@user_role_management('Dashboard')
def adminshome(request, **kwargs):
    """
    function to call affiliate admin default template
    template name :  affiliate/admin/index.html
    """
    context_dict = {}
    acc_manager = kwargs.get('acc_manager')
    partuser_obj = kwargs.get('partuser_obj')
    user = kwargs.get('user')
    # acc_manager, partuser_obj, user = get_u_details(request)
    network_site_obj = NetworkAndSiteMap.objects.filter(networkid__in=acc_manager.affiliateprogramme.networkid.all())
    affiliates_obj = Affiliateinfo.objects.filter(account_manager=acc_manager)
    number_of_items = 10000
    nexting_page = 0
    previous_page = 0
    starting_page = 0
    page = 0
    site_ids_list = get_brand_order(acc_manager)
    net_ids_list = [1]
    custom = str(request.POST.get("customid"))
    fromdate = str(request.POST.get("fromdate"))
    todate = str(request.POST.get("todate"))
    domain = request.POST.get("sitename")
    currencyfilter = request.POST.get("currency", 'euro')
    affiliate_id = request.POST.get("affiliateid")
    site_name = 'All Websites'
    site_ids = site_ids_list
    if domain and domain != 'All':
        site_domain_list = domain.split(',')
        site_ids = site_ids_list.filter(domain__in=site_domain_list)
        site_name = 'Multiple sites.'
        if len(domain.split(',')) <2:
            site_name = site_ids_list.get(domain=site_domain_list[0]).name
    siteids = request.POST.getlist("siteids")
    if request.method == "GET":
        to_day_date = datetime.now()
        date = to_day_date.day
        year = to_day_date.year
        month = to_day_date.month
        dates_tuple = calendar.monthrange(year, month)
        fromdate = "%s/%s/%s" %(month, 1, year)
        todate = "%s/%s/%s" %(month, date, year)
        custom = "Custom"
        siteids = None
        channel = request.GET.get("channel")
    else:
        custom = str(request.POST.get("customid"))
        fromdate = str(request.POST.get("fromdate"))
        todate = str(request.POST.get("todate"))
        channel = request.POST.get("channel", None)
        if channel and channel == "All":
            channel = None
    country = Country.objects.all().values_list('countryname', flat=True).distinct()
    commission = []
    print 'dashboard check', custom, fromdate, todate
    date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
    from_date = date_times_dict.get('fromdate')
    to_date = date_times_dict.get('todate')
    request.session['affiliate_dashboard_dates'] = date_times_dict
    affiliate_objs = affiliates_obj.exclude(username=acc_manager.user.username)
    context_dict = get_affiliate_admin_home_stats(affiliates_obj, commission, from_date, to_date, site_ids, currencyfilter)
    context_dict['device_list'] = ['Web', 'Mobile', 'Tablet', 'Television']
    # context_dict['device_report'] = get_device_info_chart(affiliates_obj, affiliates_obj, commission, from_date, to_date, site_ids)
    
    context_dict['affiliates_count'] = len(affiliates_obj)
    context_dict['sites_count'] = len(site_ids_list)
    context_dict['subaff_list'] = affiliates_obj
    context_dict['campaign_count'] = Campaign.objects.filter(fk_affiliateinfo__in=affiliates_obj).count()
    pending_aff = affiliates_obj.filter(status='Pending').count()
    rejected_aff = affiliates_obj.filter(status='Rejected').count()
    approved_aff = context_dict['affiliates_count'] - (pending_aff + rejected_aff)
    pending_aff_list = []
    for aff in affiliates_obj.filter(status="Pending").order_by('-id')[:5]:
        edit_ele = '<a href="/admin/add-affiliate/'+str(aff.id)+'"><button class="btn btn-success">View/Edit</button></a>'
        pending_aff_list.append([str(aff.id), str(aff.companyname), edit_ele])
    context_dict['pending_affiliates'] = pending_aff_list
    context_dict['affiliate_status'] = [pending_aff, approved_aff, rejected_aff]
    request.session['pending_count'] = affiliates_obj.filter(account_manager=acc_manager,
                         status="Pending").count()
    header_list, data_list, currency = activity_admin_report_stats(affiliate_objs, from_date, to_date, site_ids, currencyfilter)
    
    header_b_list, data_b_list, currency = advertiser_reports_stats(affiliates_obj, from_date, to_date, site_ids, currencyfilter, country)

    from_date = date_times_dict.get('fromdate').strftime('%B %d, %Y')
    to_date = date_times_dict.get('todate').strftime('%B %d, %Y')
    ui_from_date = datetime_or_date_to_string(date_times_dict.get('fromdate'))
    ui_to_date = datetime_or_date_to_string(date_times_dict.get('todate'))
    context_dict['stats_date'] = "%s - %s" %(from_date, to_date)
    context_dict["from_date"] = from_date
    context_dict["to_date"] = to_date
    context_dict["ui_from_date"] = ui_from_date

    context_dict["ui_to_date"] = ui_to_date
    context_dict['sites_obj_list'] = site_ids_list

    count = len(data_list)
    start_record, end_record, start_page, \
            end_page, next_page, cur_page, page_count = get_paginate_dimensions(page, starting_page, 
                                                                                nexting_page, previous_page, 
                                                                                count, number_of_items)

    pagination_div  = makePaginationDiv(start_page, page_count, cur_page, next_page, count)

    # if partuser_obj:
    #     context_dict["menu_items_list"] = partuser_obj[0].options
    # else:
    #     context_dict["menu_items_list"] = 'ALL'
    context_dict["data_b_list"] = json.dumps(data_b_list)
    context_dict["header_b_list"] = json.dumps(header_b_list)
    context_dict["currency"] = currency
    context_dict["pagination_div"] = pagination_div
    # if request.POST.get("format") == "json":
    #     context_dict["status"] = "success"
    #     context_dict['data_list'] = data_list
    #     context_dict["header_list"] = header_list
    #     return HttpResponse(json.dumps(context_dict, indent=4))
    context_dict['domain'] = site_name
    context_dict['data_list'] = json.dumps(data_list)
    context_dict['partner'] = acc_manager.affiliateprogramme.name
    context_dict['partner_obj'] = acc_manager.affiliateprogramme
    context_dict['part_user'] = ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
    context_dict['affiliates_obj'] = affiliates_obj
    context_dict["header_list"] = json.dumps(header_list)
    return render(request, 'partner/index.html', context_dict)


def adminhome(request):
    """
    function to call affiliate admin default template
    template name :  affiliate/admin/index.html
    """
    context_dict = {}
    pid = request.session.get('partnerid')
    if not pid:
        return HttpResponseRedirect(reverse('partner_login'),)
    user = User.objects.get(id=pid)
    link = request.GET.get('link')
    partuser_obj = []
    acc_manager_obj = AccountManager.objects.filter(user=user)
    if acc_manager_obj:
        acc_manager = acc_manager_obj[0]
    else:
        partuser_obj = PartnerUser.objects.filter(user=user)
        acc_manager = partuser_obj[0].accmanager
    request.user.username = user.username
    request.user.email = user.email
    context_dict['partner'] = acc_manager.affiliateprogramme.name
    context_dict['partner_obj'] = acc_manager.affiliateprogramme
    context_dict['part_user'] = ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
    if link:
        context_dict['mainp'] = link.split(',')[0]
        context_dict['page'] = link.split(',')[1]
    return render(request, 'partner/index.html', context_dict)

def comingsoon(request):
    """
    function to call affiliate admin default template
    template name :  affiliate/admin/index.html
    """
    context_dict = {}
    pid = request.session.get('partnerid')
    if not pid:
        return HttpResponseRedirect(reverse('partner_login'),)
    user = User.objects.get(id=pid)
    link = request.GET.get('link')
    partuser_obj = []
    acc_manager_obj = AccountManager.objects.filter(user=user)
    if acc_manager_obj:
        acc_manager = acc_manager_obj[0]
    else:
        partuser_obj = PartnerUser.objects.filter(user=user)
        acc_manager = partuser_obj[0].accmanager
    request.user.username = user.username
    request.user.email = user.email
    context_dict['partner'] = acc_manager.affiliateprogramme.name
    context_dict['partner_obj'] = acc_manager.affiliateprogramme
    context_dict['part_user'] = ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
    if link:
        context_dict['mainp'] = link.split(',')[0]
        context_dict['page'] = link.split(',')[1]
    return render(request, 'partner/coming-soon.html', context_dict)


class AccountManagerViewset(NonDestructiveModelViewSet):

    """
    Endpoint: /admin/currency/

    GET: Typical RESTful Response including Pagination.

    `search` query parameter => ?search=<search text>

    PUT & PATCH are available though no validations
                beyond data types exist on it for now.

    """
    queryset = AccountManager.objects.all()
    serializer_class = AccountManagerSerializer
    filter_backends = (filters.SearchFilter,
                       DjangoFilterBackend, filters.OrderingFilter,)
    parser_classes = (MultiPartParser, FormParser,)
    renderer_classes = (renderers.JSONRenderer, renderers.TemplateHTMLRenderer)

    @user_class_role_management('Affiliate Managers')
    def retrieve(self, request, pk=None):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        try:
            instance = self.get_object()
            # queryset = get_object_or_404(queryset, pk=pk)
            # queryset = self.filter_queryset(self.get_queryset())
            serializer = self.get_serializer(instance)
        except:
            serializer = self.get_serializer(data={})
            serializer.is_valid()
        # queryset = get_object_or_404(queryset, pk=pk)
        # queryset = self.filter_queryset(self.get_queryset())
        # serializer = self.get_serializer(instance)
        if request.accepted_renderer.format == 'html':
            data = serializer.data
            user = User.objects.get(id=pid)
            acc_manager_obj = AccountManager.objects.filter(user=user)
            partuser_obj = []
            if acc_manager_obj:
                acc_manager = acc_manager_obj[0]
            else:
                partuser_obj = PartnerUser.objects.filter(user=user)
                acc_manager = partuser_obj[0].accmanager
            data['id'] = acc_manager.id
            data['first_name'] = user.first_name
            data['last_name'] = user.last_name
            request.user.username = user.username
            request.user.email = user.email
            data['date_joined'] = acc_manager.timestamp.strftime("%d-%m-%Y")
            data['partner'] = acc_manager.affiliateprogramme.name
            data['partner_obj'] = acc_manager.affiliateprogramme
            data['call'] = 'POST' if self.kwargs.get('pk') == 'post' else 'PUT'
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/accmanager.html')
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        request.data._mutable = True
        user = User.objects.get(id=pid)
        user.first_name = request.data.get('first_name')
        request.data.pop('first_name')
        user.last_name = request.data.get('last_name')
        request.data.pop('last_name')
        user.email = request.data.get('email')
        request.data.pop('email')
        password = request.data.get('passwd')
        if password:
            request.data.pop('passwd')
            # password = 'vanessa'
            passwd = make_password(password)
            user.set_password(passwd)
        user.save()
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=False)
        self.perform_update(serializer)
        if request.accepted_renderer.format == 'html':
            data = serializer.data
            user = User.objects.get(id=pid)
            acc_manager_obj = AccountManager.objects.filter(user=user)
            partuser_obj = []
            if acc_manager_obj:
                acc_manager = acc_manager_obj[0]
            else:
                partuser_obj = PartnerUser.objects.filter(user=user)
                acc_manager = partuser_obj[0].accmanager
            data['first_name'] = user.first_name
            data['last_name'] = user.last_name
            request.user.username = user.username
            request.user.email = user.email
            data['date_joined'] = str(acc_manager.timestamp).split(' ')[0]
            data['partner'] = acc_manager.affiliateprogramme.name
            data['partner_obj'] = acc_manager.affiliateprogramme
            data['call'] = 'POST' if self.kwargs.get('pk') == 'post' else 'PUT'
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/accmanager.html')
        return Response(serializer.data)

class FileUploadViewSet(NonDestructiveModelViewSet):

    """
    Endpoint: /admin/fileupload/

    GET: Typical RESTful Response including Pagination.

    `search` query parameter => ?search=<search text>

    PUT & PATCH are available though no validations
                beyond data types exist on it for now.

    """
    queryset = FileUpload.objects.all() #filter(media_type='B')
    serializer_class = FileUploadSerializer
    filter_backends = (filters.SearchFilter,
                       DjangoFilterBackend, filters.OrderingFilter,)
    parser_classes = (MultiPartParser, FormParser,)
    renderer_classes = (renderers.JSONRenderer, renderers.TemplateHTMLRenderer)

    @user_class_role_management('Proprietary')
    def list(self, request, *args, **kwargs):
        acc_manager, partuser_obj, user = get_u_details(request)
        queryset = self.filter_queryset(self.get_queryset())
        queryset = queryset.filter(user=acc_manager.user).order_by('-date')
        page = True
        if page is not None:
            serializer = self.get_serializer(queryset, many=True)
            if request.accepted_renderer.format == 'html':
                cpage = int(request.GET.get('page')) if request.GET.get('page') else 1
                header_list = [{'title': 'File'}, {'title':'Date'}, {'title':'Site'},
                    {'title':'Super Affiliate'}, {'title':'Status'}, {'title': 'View/ Edit'}]
                
                page_div = ''
                data_list = []
                for i in serializer.data:
                    if not i.get('siteid'):
                        continue
                    if not i.get('affiliate'):
                        ac_m = ''
                    else:
                        ac_m = Affiliateinfo.objects.get(id=i['affiliate']).username
                    site_name = Site.objects.get(id=i['siteid']).name
                    edit_ele = '<a href="/admin/proprietary/'+str(i['id'])+'"><button class="btn btn-success">View/ Edit</button></a>'
                    del_ele = '<form class="button-form" action="/admin/proprietary/'+str(i['id'])+'" data-method="DELETE"><button class="btn btn-danger">Delete</button></form>' 
                    data_list.append([str(i['excel']).split('excel/')[1], 
                                    datetime.strptime(i['date'], '%Y-%m-%d').strftime('%d-%m-%Y'),
                                     str(site_name), 
                                    str(ac_m), str(i['status']), edit_ele])
                return Response({'header_list': header_list, 'data_list': data_list, 'pagination_div': page_div,
                                'partner': acc_manager.affiliateprogramme.name,
                                'partner_obj': acc_manager.affiliateprogramme,
                                'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
                           }, template_name='partner/fileupload_list.html')
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        # if request.accepted_renderer.format == 'html':
        #     return Response({'data': serializer.data}, template_name='partner/index.html')
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        date = str(request.POST.get('date'))
        request.data['date'] = datetime.strptime(request.data['date'], '%d-%m-%Y').strftime("%Y-%m-%d")
        date = request.data['date']
        siteid = request.POST.get('siteid')
        site_obj = Site.objects.get(id=siteid)
        file_obj = FileUpload.objects.filter(siteid=site_obj, date=date)
        if not file_obj:
            status = retrive_from_excel(request)
            request.data._mutable = True
            request.data['status'] = status
            acc_manager, partuser_obj, user = get_u_details(request)
            request.data['user'] = acc_manager.user.id
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            a = self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            data = serializer.data
            request.session['created'] = status
            id_ = serializer.data['id']
            if status == 'ERR':
                return self.retrieve(request)
        else:
            request.session['created'] = 'TRR'
            id_ = file_obj[0].id
        return HttpResponseRedirect("/admin/proprietary/"+str(id_))

    def perform_create(self, serializer):
        serializer.save()

    @user_class_role_management('Proprietary')
    def retrieve(self, request, pk=None):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        try:
            instance = self.get_object()
            # queryset = get_object_or_404(queryset, pk=pk)
            # queryset = self.filter_queryset(self.get_queryset())
            serializer = self.get_serializer(instance)
        except:
            serializer = self.get_serializer(data={})
            serializer.is_valid()
        # queryset = get_object_or_404(queryset, pk=pk)
        # queryset = self.filter_queryset(self.get_queryset())
        # serializer = self.get_serializer(instance)
        if request.accepted_renderer.format == 'html':
            data = serializer.data
            acc_manager, partuser_obj, user = get_u_details(request)
            data['affiliates_list'] = [[i.id, i.email] for i in Affiliateinfo.objects.filter(account_manager=acc_manager)]
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            data['partner'] = acc_manager.affiliateprogramme.name
            data['partner_obj'] = acc_manager.affiliateprogramme
            data['call'] = 'POST' if self.kwargs.get('pk') == 'post' else 'PUT'
            msg = ''
            if request.session.get('created'):
                crt = request.session.get('created')
                if crt == 'PRO':
                    msg = 'Uploaded file successfully.'
                elif crt == 'ERR':
                    msg = 'There is an Issue with the file you have uploaded.'
                else:
                    msg = 'Already there is data for brand on that date.'
                del request.session['created']
            data['message'] = msg
            if data.get('date'):
                data['date'] = datetime.strptime(data['date'], '%Y-%m-%d').strftime('%d-%m-%Y')
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/fileupload.html')
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        request.data._mutable = True
        status = 'PRO'
        request.data['date'] = datetime.strptime(request.data['date'], '%d-%m-%Y').strftime('%Y-%m-%d')
        if request.FILES.get('excel'):
            status = retrive_from_excel(request)
        else:
            del request.data['excel']
        request.data['status'] = status
        acc_manager, partuser_obj, user = get_u_details(request)
        request.data['user'] = acc_manager.user.id
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=False)
        self.perform_update(serializer)
        if request.accepted_renderer.format == 'html':
            data = serializer.data
            acc_manager, partuser_obj, user = get_u_details(request)
            if data.get('date'):
                data['date'] = datetime.strptime(data['date'], '%Y-%m-%d').strftime('%d-%m-%Y')
            data['message'] = 'File data updated.'
            data['message'] = 'Updated successfully'
            data['affiliates_list'] = [[i.id, i.email] for i in Affiliateinfo.objects.filter(account_manager=acc_manager)]
            data['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
            data['partner'] = acc_manager.affiliateprogramme.name
            data['partner_obj'] = acc_manager.affiliateprogramme
            return Response({'data': data, 'partner': data['partner'],
                            'partner_obj':data['partner_obj'],
                            'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                            template_name='partner/fileupload.html')
        return Response(serializer.data)


def asignin(request):
    try:
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        affid = request.GET.get('affid')
        obj = Affiliateinfo.objects.select_related(
                'account_manager').get(id=affid)
        request.session['affid'] = str(obj.id)
        request.session['affiliate_session_id'] = str(obj.id)
        return HttpResponseRedirect(reverse('affiliate_home'),)
    except:
        return HttpResponseRedirect(reverse('partner_login'),)

def sdignin(request):
    try:
        aid = request.GET.get('aid')
        if not aid:
            return HttpResponseRedirect(reverse('partner_login'),)
        ap_obj = AffiliateProgramme.objects.get(id=aid)
        acc_manager_obj = AccountManager.objects.filter(affiliateprogramme=ap_obj)
        if acc_manager_obj:
            acc_manager = acc_manager_obj[0]
        request.session['partnerid'] = str(acc_manager.user.id)
        request.session['partner_session_id'] = str(acc_manager.user.id)
        return HttpResponseRedirect(reverse('partner'),)
    except:
        return HttpResponseRedirect(reverse('partner_login'),)


def thanks(request):
    pid = request.GET.get('pid')
    if not pid:
        return HttpResponseRedirect(reverse('partner_login'),)
    user = User.objects.get(id=pid)
    partuser_obj = []
    acc_manager_obj = AccountManager.objects.filter(user=user)
    if acc_manager_obj:
        acc_manager = acc_manager_obj[0]
    else:
        partuser_obj = PartnerUser.objects.filter(user=user)
        acc_manager = partuser_obj[0].accmanager
    affiliateprogramregister(acc_manager, 'NEW_ADVERTISER')
    message = """Affiliate Program %s \
            is registered with wynta. email address of the account manager is %s\
            """%(acc_manager.affiliateprogramme.name, user.email)
    msg = EmailMessage('Affiliate Program registered.', message
        , """%s""" % 'support@wynta.com', ['shravan.bitla@gridlogic.in','vanessa@fozilmedia.com'])
    msg.content_subtype = "html"
    msg.send()
    request.user.username = user.username
    request.user.email = user.email
    request.session['partnerid'] = str(user.id)
    request.session['partner_session_id'] = str(user.id)
    context_dict = {'partner': acc_manager.affiliateprogramme.name,
                            'partner_obj':acc_manager.affiliateprogramme}
    return render(request, 'partner/thanks.html', context_dict)


def thanksadmin(request):
    pid = request.session.get('partnerid')
    if not pid:
        return HttpResponseRedirect(reverse('partner_login'),)
    user = User.objects.get(id=pid)
    partuser_obj = []
    acc_manager_obj = AccountManager.objects.filter(user=user)
    if acc_manager_obj:
        acc_manager = acc_manager_obj[0]
    else:
        partuser_obj = PartnerUser.objects.filter(user=user)
        acc_manager = partuser_obj[0].accmanager
    request.user.username = user.username
    request.user.email = user.email
    request.session['partnerid'] = str(user.id)
    request.session['partner_session_id'] = str(user.id)
    context_dict = {'partner': acc_manager.affiliateprogramme.name,
                            'partner_obj':acc_manager.affiliateprogramme}
    return render(request, 'partner/thanks-admin.html', context_dict)

import requests
import time
import subprocess
from threading import Timer
def add_lets_encrypt_files(domain, aff_id):
    domain_c = False
    print 'start adding files'
    filename = "/etc/apache2/sites-enabled/"+domain+'.conf'
    filename_d = "/etc/apache2/sites-available/"+domain+'.conf'
    for file in os.listdir('/etc/letsencrypt/live/'):
        if domain in file:
            domain_c = file
            print 'action success'
            break
    print 'check ', domain_c
    if domain_c:
        apache_str = 'WSGIApplicationGroup %%{GLOBAL}\n\
            WSGIDaemonProcess %s user=www-data group=www-data processes=2 threads=3 python-path=/var/www/env/WYNTA/lib/python2.7/site-packages/ display-name=%s-wyntaProcessDev\n\
            WSGIProcessGroup %s\n\
            \n\
            <virtualhost *:80>\n\
                    CustomLog /var/log/apache2/wynta.log combined\n\
                    ErrorLog /var/log/apache2/wynta_error.log\n\
            \n\
                    ServerName %s\n\
                    ServerAlias %s\n\
            \n\
                    WSGIScriptAlias / /var/www/wynta/apache/d1wynta.wsgi\n\
            \n\
                    <Directory "/var/www/wynta/apache">\n\
                            Allow from all\n\
                    </Directory>\n\
                    Alias /static/admin /var/www/env/WYNTA/lib/python2.7/site-packages/django/contrib/admin/static/admin\n\
                    Alias /static_files/admin /var/www/env/WYNTA/lib/python2.7/site-packages/django/contrib/admin/static/admin\n\
                    Alias /static_files /var/www/wynta/d1_static_files\n\
                    Alias /media/ /var/www/wynta/media/\n\
                    Alias /.well-known/acme-challenge/ /var/www/wynta/static_files/.well-known/acme-challenge/\n\
                    <Directory "/var/www/wynta/media">\n\
                            AllowOverride All\n\
                            Order deny,allow\n\
                            Allow from all\n\
                    </Directory>\n\
            \n\
                    FileETag INode MTime Size\n\
            RewriteEngine on\n\
            RewriteCond %%{SERVER_NAME} =%s\n\
            RewriteRule ^ https://%%{SERVER_NAME}%%{REQUEST_URI} [END,NE,R=permanent]\n\
            </virtualhost>\n\
            \n\
            <VirtualHost *:443>\n\
                CustomLog /var/log/apache2/wynta.log combined\n\
                ErrorLog /var/log/apache2/wynta_error.log\n\
            \n\
                ServerName %s\n\
                ServerAlias %s\n\
            \n\
                WSGIScriptAlias / /var/www/wynta/apache/d1wynta.wsgi\n\
            \n\
                <Directory "/var/www/wynta/apache">\n\
                            Allow from all\n\
                </Directory>\n\
                Alias /static/admin /var/www/env/WYNTA/lib/python2.7/site-packages/django/contrib/admin/static/admin\n\
                Alias /static_files/admin /var/www/env/WYNTA/lib/python2.7/site-packages/django/contrib/admin/static/admin\n\
                Alias /static_files /var/www/wynta/d1_static_files\n\
                Alias /media/ /var/www/wynta/media/\n\
                Alias /.well-known/acme-challenge/ /var/www/wynta/static_files/.well-known/acme-challenge/\n\
                <Directory "/var/www/wynta/media">\n\
                        AllowOverride All\n\
                        Order deny,allow\n\
                        Allow from all\n\
                </Directory>\n\
            \n\
                FileETag INode MTime Size\n\
            \n\
            Include /etc/letsencrypt/options-ssl-apache.conf\n\
            SSLCertificateFile /etc/letsencrypt/live/%s/fullchain.pem\n\
            SSLCertificateKeyFile /etc/letsencrypt/live/%s/privkey.pem\n\
            </VirtualHost>'%(domain, domain, domain, domain, domain, domain, domain, domain, domain_c, domain_c)
        # request.session['success_msg'] = 'Your custom domain '+domain+' is created.'
        file_t = open(filename, "w+")
        file_t.write(apache_str)
        file_t.close()
        file_d = open(filename_d, "w+")
        file_d.write(apache_str)
        file_d.close()
        print 'Refreshed restartapache sh'
    try:
        # os.system('sudo /var/www/wynta/apacherestart.sh')
        subprocess.call('sudo /var/www/wynta/apacherestart.sh', shell=True)
        af_prog = AffiliateProgramme.objects.get(id=aff_id)
        af_prog.ssl_state = 'FINISHED'
        af_prog.save()
    except Exception as e:
        print str(e)
        print 'Refreshed restartapache apache test'
    print 'Refreshed apachectl'


def run_ssl_job(domain, aff_id):
    try:
        print 'start the ssl job'
        # os.system('sudo /var/www/wynta/restartapache.sh '+str(domain))
        #subprocess.call('sudo /var/www/wynta/restartapache.sh '+str(domain), shell=True)
        subprocess.call('sudo /var/www/wynta/restartapache.sh '+str(domain), shell=True)
        #s = subprocess.Popen("sudo /var/www/wynta/restartapache.sh "+ str(domain), stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        #print 'std out for ssl', s.communicate()
    except OSError as exception:
        print 'error with OS script ', str(exception)
    except Exception as e:
        print 'error with shell script ', str(e)


#@user_role_management('Domain and SSL')
def writedomain(request):
    pid = request.session.get('partnerid')
    if not pid:
        return HttpResponseRedirect(reverse('partner_login'),)
    try:
        user = User.objects.get(id=pid)
        partuser_obj = []
        acc_manager_obj = AccountManager.objects.filter(user=user)
        if acc_manager_obj:
            acc_manager = acc_manager_obj[0]
        else:
            partuser_obj = PartnerUser.objects.filter(user=user)
            acc_manager = partuser_obj[0].accmanager
        request.user.username = user.username
        request.user.email = user.email
        request.session['partnerid'] = str(user.id)
        request.session['partner_session_id'] = str(user.id)
        domain_db = acc_manager.affiliateprogramme.domain if acc_manager.affiliateprogramme.domain else '' 
        context_dict = {'partner': acc_manager.affiliateprogramme.name, 'domain': domain_db,
                                'partner_obj':acc_manager.affiliateprogramme}
        if request.method == "GET":
            context_dict['pid'] = request.GET.get('pid', '')
            return render(request, 'partner/domainselect.html', context_dict)
        else:
            domain = request.POST.get('domain')
            apache_str = 'WSGIApplicationGroup %%{GLOBAL}\n\
                WSGIDaemonProcess %s user=www-data group=www-data processes=32 threads=1 python-path=/var/www/env/WYNTA/lib/python2.7/site-packages/ display-name=%s-wyntaProcessDev\n\
                WSGIProcessGroup %s\n\
                \n\
                <virtualhost *:80>\n\
                        CustomLog /var/log/apache2/wynta.log combined\n\
                        ErrorLog /var/log/apache2/wynta_error.log\n\
                \n\
                        ServerName %s\n\
                        ServerAlias %s\n\
                \n\
                        WSGIScriptAlias / /var/www/wynta/apache/d1wynta.wsgi\n\
                \n\
                        <Directory "/var/www/wynta/apache">\n\
                                Allow from all\n\
                        </Directory>\n\
                        Alias /static/admin /var/www/env/WYNTA/lib/python2.7/site-packages/django/contrib/admin/static/admin\n\
                        Alias /static_files/admin /var/www/env/WYNTA/lib/python2.7/site-packages/django/contrib/admin/static/admin\n\
                        Alias /static_files /var/www/wynta/d1_static_files\n\
                        Alias /media/ /var/www/wynta/media/\n\
                        Alias /.well-known/acme-challenge/ /var/www/wynta/static_files/.well-known/acme-challenge/\n\
                        <Directory "/var/www/wynta/media">\n\
                                AllowOverride All\n\
                                Order deny,allow\n\
                                Allow from all\n\
                        </Directory>\n\
                \n\
                        FileETag INode MTime Size\n\
                RewriteEngine on\n\
                RewriteCond %%{SERVER_NAME} =%s\n\
                RewriteRule ^ https://%%{SERVER_NAME}%%{REQUEST_URI} [END,NE,R=permanent]\n\
                </virtualhost>\n\
                \n\
                <VirtualHost *:443>\n\
                    CustomLog /var/log/apache2/wynta.log combined\n\
                    ErrorLog /var/log/apache2/wynta_error.log\n\
                \n\
                    ServerName %s\n\
                    ServerAlias %s\n\
                \n\
                    WSGIScriptAlias / /var/www/wynta/apache/d1wynta.wsgi\n\
                \n\
                    <Directory "/var/www/wynta/apache">\n\
                                Allow from all\n\
                    </Directory>\n\
                    Alias /static/admin /var/www/env/WYNTA/lib/python2.7/site-packages/django/contrib/admin/static/admin\n\
                    Alias /static_files/admin /var/www/env/WYNTA/lib/python2.7/site-packages/django/contrib/admin/static/admin\n\
                    Alias /static_files /var/www/wynta/d1_static_files\n\
                    Alias /media/ /var/www/wynta/media/\n\
                    Alias /.well-known/acme-challenge/ /var/www/wynta/static_files/.well-known/acme-challenge/\n\
                    <Directory "/var/www/wynta/media">\n\
                            AllowOverride All\n\
                            Order deny,allow\n\
                            Allow from all\n\
                    </Directory>\n\
                \n\
                    FileETag INode MTime Size\n\
                \n\
                </VirtualHost>'%(domain, domain, domain, domain, domain, domain, domain, domain)
            filename = "/etc/apache2/sites-enabled/"+domain+'.conf'
            filename_d = "/etc/apache2/sites-available/"+domain+'.conf'
            if not os.path.exists(os.path.dirname(filename)):
                try:
                    os.makedirs(os.path.dirname(filename))
                except OSError as exc: # Guard against race condition
                    print str(exc)
                    if exc.errno != errno.EEXIST:
                        raise
                except Exception as e:
                    print str(e)
            if not os.path.exists(os.path.dirname(filename_d)):
                try:
                    os.makedirs(os.path.dirname(filename_d))
                except OSError as exc: # Guard against race condition
                    print str(exc)
                    if exc.errno != errno.EEXIST:
                        raise
                except Exception as e:
                    print str(e)
            file_t = open(filename, "w+")
            file_t.write(apache_str)
            file_t.close()
            file_d = open(filename_d, "w+")
            file_d.write(apache_str)
            file_d.close()
            acc_manager.affiliateprogramme.ssl_state = 'STARTED'
            acc_manager.affiliateprogramme.domain = domain
            acc_manager.affiliateprogramme.save()
            t = Timer(10.0, run_ssl_job, [domain, acc_manager.affiliateprogramme.id])
            t.start()
            d = Timer(1000.0, add_lets_encrypt_files, [domain, acc_manager.affiliateprogramme.id])
            d.start()
    except Exception as e:
        print 'Overall error in domain script ', str(e)
    return HttpResponseRedirect('/admin/brand-mapping/post?pid=new')

def latestdatadump(request):
    pid = request.session.get('partnerid')
    context_dict = {}
    number_of_items = 10000
    nexting_page = 0
    previous_page = 0
    starting_page = 0
    page = 0
    if not pid:
        return HttpResponseRedirect(reverse('partner_login'),)
    try:
        user = User.objects.get(id=pid)
        partuser_obj = []
        acc_manager_obj = AccountManager.objects.filter(user=user)
        if acc_manager_obj:
            acc_manager = acc_manager_obj[0]
        else:
            partuser_obj = PartnerUser.objects.filter(user=user)
            acc_manager = partuser_obj[0].accmanager
        request.user.username = user.username
        request.user.email = user.email
        sitefilter = request.POST.getlist("siteid")
        request.session['partnerid'] = str(user.id)
        request.session['partner_session_id'] = str(user.id)
    except Exception as e:
        print 'error with dump files ', str(e)   
    context_dict['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
    context_dict['partner'] = acc_manager.affiliateprogramme.name
    context_dict['partner_obj'] = acc_manager.affiliateprogramme
    context_dict['part_user'] = ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
    # context_dict["channels_list"] = get_channels_list()
    # context_dict["channel"] = channel if channel else "All"
    return render(request, 'partner/latest_data_dump.html', context_dict)

def get_provider_logs(request):
    try:
        if request.method == "GET":
            header_list = [
                            { "title": "Brand"},
                            { "title": "Date"},
                            { "title": "Provider Logs"}
                        ]
            data_list = [] 
            siteid = request.GET.get('siteid')
            site_obj = Site.objects.get(id=siteid)
            from_date = request.GET.get('fromDate')
            to_date = request.GET.get('toDate')
            fwyear, fwmonth, fwday = to_date.split('-')
            fwyear, fwmonth, fwday = int(fwyear), int(fwmonth), int(fwday)
            from datetime import date
            to_date = date(fwyear, fwmonth, fwday)
            to_date = str(to_date + timedelta(days=1))
            while from_date != to_date:
                json_date = '%s-%s'%(siteid,str(from_date))
                print('json_date>>>>',json_date)
                json_dump_files = [filename for filename in os.listdir('/var/www/wynta/media/json_dump/') if filename.startswith(str(json_date))]
                for json_dump in json_dump_files:
                    f_ = '<a href="/media/json_dump/'+json_dump+'" target="_blank"><button class="btn btn-success" style="width: 100px;">Raw File</button></a>'
                    data_list.append([site_obj.name, str(from_date), f_])
                fwyear, fwmonth, fwday = from_date.split('-')
                fwyear, fwmonth, fwday = int(fwyear), int(fwmonth), int(fwday)
                from_date = date(fwyear, fwmonth, fwday)
                from_date = str(from_date + timedelta(days=1))
            return HttpResponse(json.dumps({'header_list':header_list,'data_list':data_list, 'status':'Success'}))
    except Exception as e:
        return HttpResponse(json.dumps({'status': str(e)}))


def message_view(request):
    # View specific Email info
    context_dict = {}
    acc_manager, partuser_obj, user = get_u_details(request)
    m_id = request.GET.get('m_id')
    view_id = request.GET.get('view', 0)
    if request.method == 'POST':
        to_email = request.POST.getlist('to_email')
        from_email = request.POST.get('from_email')
        subject = request.POST.get('subject')
        content = request.POST.get('content')
        try:
            emails_list = to_email
            custom_admin_email(acc_manager, partuser_obj, emails_list, from_email, content, subject)
            resp = 'Success'
        except Exception as e:
            print str(e)
            resp = 'Error'
        context_dict = {'from': user.email, 'sent': resp}
        context_dict['status'] = 'Success'
        return HttpResponse(json.dumps(context_dict, indent=4))
    else:
        if int(view_id) == 1:
            message_obj = Messagelogs.objects.get(id=m_id)
            message_obj.status = 'R'
            message_obj.save()
            s_r = 'Received'
            if message_obj.from_email == acc_manager.user.email:
                s_r = 'Sent'
            data_dict = {'from': message_obj.from_email, 'to': message_obj.to_email,
                            'subject': message_obj.subject,
                            'content': message_obj.content,
                            'timestamp': str(message_obj.timestamp),
                            's_r': s_r}
        elif int(view_id) == 0:
            if m_id:
                message_obj = Messagelogs.objects.get(id=m_id)
                message_obj.status = 'D'
                message_obj.save()
                context_dict['status'] = 'Success'
                return HttpResponse(json.dumps(context_dict, indent=4))
            data_dict = {'from': user.email}
    context_dict['data'] = data_dict
    aff_list = Affiliateinfo.objects.filter(account_manager=acc_manager).order_by('-id')
    context_dict["aff_list"] = [{'id':i.id, 'username': i.email.replace('vcommsubaff', '')} for i in aff_list]
    context_dict['partner'] = acc_manager.affiliateprogramme.name
    context_dict['partner_obj'] = acc_manager.affiliateprogramme
    context_dict['part_user'] = ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
    # context_dict["channels_list"] = get_channels_list()
    # context_dict["channel"] = channel if channel else "All"
    return render(request, 'partner/messages.html', context_dict)


def messages_list(request):
    # View all messages that are sent and received.
    pid = request.session.get('partnerid')
    context_dict = {}
    number_of_items = 10000
    nexting_page = 0
    previous_page = 0
    starting_page = 0
    page = 0
    status_dict = dict(Messagelogs.STATUS_TYPE)
    if not pid:
        return HttpResponseRedirect(reverse('partner_login'),)
    try:
        user = User.objects.get(id=pid)
        partuser_obj = []
        acc_manager_obj = AccountManager.objects.filter(user=user)
        if acc_manager_obj:
            acc_manager = acc_manager_obj[0]
        else:
            partuser_obj = PartnerUser.objects.filter(user=user)
            acc_manager = partuser_obj[0].accmanager
        request.user.username = user.username
        request.user.email = user.email
        sitefilter = request.POST.getlist("siteid")
        request.session['partnerid'] = str(user.id)
        request.session['partner_session_id'] = str(user.id)
        header_list = [
                   { "title": "From"},
                   { "title": "To"},
                   { "title": "Sent/Received"},
                   { "title": "Status"},
                   { "title": "Time"},
                   { "title": "View"},
                   { "title": "Action"}
               ]
        data_list = []
        messages_objs = Messagelogs.objects.filter(
                            Q(account_manager=acc_manager)|Q(affiliate__account_manager=acc_manager)
                            ).exclude(status='D').order_by('-timestamp')
        for message_obj in messages_objs:
            del_ele = '<button class="btn btn-danger" onclick="savestatus(this)" hrefs="/admin/message-status/?m_id='+str(message_obj.id)+'">Delete</button>' 
            views = '<a href="/admin/message-status/?m_id='+str(message_obj.id)+'&view=1"><button class="btn btn-purple" >View</button></a>'
            s_r = 'Received'
            if message_obj.from_email == acc_manager.user.email:
                s_r = 'Sent'
            data_list.append([message_obj.from_email, 
                            message_obj.to_email, s_r, status_dict[message_obj.status],
                            str(message_obj.timestamp), views, del_ele])
    except Exception as e:
        print 'error with dump files ', str(e)
        context_dict['error'] = str(e)
    context_dict['data_list'] = json.dumps(data_list)
    context_dict['sites_list'] = [[i.id, i.name] for i in get_brand_order(acc_manager)]
    context_dict["header_list"] = json.dumps(header_list)
    context_dict['partner'] = acc_manager.affiliateprogramme.name
    context_dict['partner_obj'] = acc_manager.affiliateprogramme
    context_dict['part_user'] = ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
    # context_dict["channels_list"] = get_channels_list()
    # context_dict["channel"] = channel if channel else "All"
    return render(request, 'partner/messages_list.html', context_dict)


def changepassword(request):
    pid = request.session.get('partnerid')
    context_dict = {}
    if not pid:
        return HttpResponseRedirect(reverse('partner_login'),)
    acc_manager, partuser_obj, user = get_u_details(request)
    m_id = request.GET.get('m_id')
    view_id = request.GET.get('view', 0)
    if request.method == 'POST':
        currentpass = request.POST.get('currentpass')
        newpass = request.POST.get('newpass')
        confirmpass = request.POST.get('confirmpass')
        try:
            if newpass != confirmpass:
                context_dict['status'] = 'Error'
                context_dict['message'] = 'New Password and confirm Password does not match.'
                # elif not authenticate(username=user.username, password=currentpass):
                # context_dict['status'] = 'Error'
                # context_dict['message'] = 'Current Password is not valid.'
            else:
                user.set_password(newpass)
                user.save()
                context_dict['status'] = 'Success'
                context_dict['message'] = 'Password Changed Successfully' 
        except Exception as e:
            context_dict['status'] = 'Error'
            context_dict['message'] = 'Issue in reseting your password.' 
    context_dict['partner'] = acc_manager.affiliateprogramme.name
    context_dict['partner_obj'] = acc_manager.affiliateprogramme
    context_dict['part_user'] = ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
    return render(request, 'partner/passwordchange.html', context_dict)


def change_logs_list(change_log_list):
    try:
        header_list = [{'title': 'User'}, {'title': 'IP Address'}, {'title': 'Affiliate'}, 
                        {'title':'Activity'}, {'title':'Changed Fields'}, {'title':'Preivious data'},
                        {'title':'Changed data'},{'title': 'Date'}]
        model_dict = { 
        'Brand' : 'Site', 
        'Affiliate' : 'Affiliateinfo',
        'Campaign': 'Campaign',
        'Banners': 'BannerImages', 
        'Commission' :'Affiliatecommission', 
        'Brand mapping': 'SiteWhitelabelMapping',  
        'Account Manger profile': 'AccountManager', 
        'Affiliate Program domain': 'AffiliateProgramme', 
        'Authentication Credentials': 'NetworkAffiliateProgrammeMapping',
        'Landing Pages': 'Lpages', 
        'Message Logs': 'Messagelogs',
        'Account details page': 'AccountDetails', 
        'Text Links': 'Tlinks'
        }
        from .mapping import affiliate
        data_list = []
        for i in change_log_list:
            model_name = i.content_type.model_class().__name__
            print('model_name is',model_name)
            changed = json.loads(i.changed_fields)
            changed_fields = []
            previous_data = []
            changed_data = []
            print('changed data', changed)
            for table in affiliate:
                for key, value in table.iteritems():
                    if changed:
                        if key==model_name:
                            for key1, value1 in table[key].iteritems():
                                print('here at  table',table[key], key1, value1)
                                if key1 in changed:
                                    changed_fields.append(value1)
                                    previous_data.append({value1:changed[key1][0]})
                                    changed_data.append({value1:changed[key1][1]})
            for key, value in model_dict.iteritems():
                if model_name == value:
                    model_name = key
            object_id = i.object_id                
            fk_email = i.fk_affiliateinfo.email if i.fk_affiliateinfo else '-'
            val_list = [str(i.user.username), str(i.ipaddress), str(fk_email), 
                        str(model_name), str(changed_fields), str(previous_data), str(changed_data),
                        str(i.timestamp)]
            data_list.append(val_list)
        return header_list, data_list
    except Exception as e:
        print e
        import traceback
        traceback.print_exc()

@user_role_management('Logs')
def logs(request, **kwargs):
    context_dict = {}
    pid = request.session.get('partnerid')
    if not pid:
        return HttpResponseRedirect(reverse('partner_login'),)
    user = User.objects.get(id=pid)
    acc_manager, partuser_obj, user = get_u_details(request)
    number_of_items = 10000
    nexting_page = 0
    previous_page = 0
    starting_page = 0
    page = 0
    site_ids_list = get_brand_order(acc_manager)
    affiliates_obj = Affiliateinfo.objects.filter(account_manager=acc_manager).exclude(username=user.username)
    site_ids = site_ids_list
    format_d = request.POST.get("format")
    domain = None
    aff_id = ''
    site_name = 'All Websites'
    aff_id = request.GET.get('aff_id')
    if request.method == "GET":
        custom = "Custom"
        fromdate = request.GET.get("fromdate")
        todate = request.GET.get("todate")
        if not fromdate or not todate:
            custom = None
        campaign_id = None
        starting_page = request.GET.get('start_page')
        nexting_page = request.GET.get('next')
        previous_page = request.GET.get('prev')
        page = request.GET.get('page')
        # device = request.POST.get("device", 'Desktop')
        if not starting_page  and not nexting_page and not previous_page and not page:
            form_data = {}
            request.session['affiliate_campaignsinfo'] = form_data
        else:
            form_data = request.session.get('affiliate_campaignsinfo')
            custom = form_data.get("customid")
            fromdate = form_data.get("fromdate")
            todate = form_data.get("todate")
            siteids = form_data.get("siteids")
            campaign_id = form_data.get("campaign_id")
        if not fromdate and not todate:
            custom = "Custom"
            todate = datetime.now().strftime("%m/%d/%Y")
            current_month , current_year =  datetime.now().month, datetime.now().year
            fromdate = "%s/01/%s" % (current_month, current_year )
    else:
        custom = str(request.POST.get("customid"))
        fromdate = str(request.POST.get("fromdate"))
        todate = str(request.POST.get("todate"))
        siteids = request.POST.getlist("siteids")
        aff_id = request.POST.get('aff_id')
        
        domain = request.POST.get("sitename")
        if domain:
            format_d = 'reload'
        site_ids = site_ids_list
        print 'site_ids', site_ids_list
        if domain and domain != 'All':
            site_domain_list = domain.split(',')
            print 'site_ids', site_domain_list
            site_ids = site_ids_list.filter(domain__in=site_domain_list)
            print 'site_ids_l', site_ids
            site_name = 'Multiple sites.'
            if len(domain.split(',')) <2:
                site_name = site_ids_list.get(domain=site_domain_list[0]).name
        
        # device = request.POST.get("device", 'Desktop')
        #if campaign_id == 'all':
        #    campaign_id = None
        form_data = {'fromdate':fromdate, 'todate':todate,'customid':custom, "siteids":siteids,
                     }
        request.session['affiliate_campaignsinfo'] = form_data

    country_list = Country.objects.all().values_list('countryname', flat=True).distinct()
    country = request.POST.getlist('country')
    currencyfilter = request.POST.get("currency", 'euro')
    group_d = str(request.POST.get("groupby", 'affiliate'))

    sitefilter = request.POST.getlist("siteid")
    if sitefilter:
        site_ids = site_ids.filter(id__in=sitefilter)
    else:
        sitefilter = [str(i.id) for i in site_ids]
    aff_prog = acc_manager.affiliateprogramme

    date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
    from_date = date_times_dict.get('fromdate')
    to_date = date_times_dict.get('todate')
    # camp_links = Campaign.objects.filter(fk_affiliateinfo=obj, siteid__in=site_ids).select_related('siteid').\
    #                     order_by('-id')
    change_log_list = ChangeLog.objects.filter(
                        timestamp__gte=str(from_date),
                        timestamp__lte=str(to_date),
                        affiliateprogramme=aff_prog)
    #change_log_list = ChangeLog.objects.all()
    count = change_log_list.count() 
    start_record, end_record, start_page, \
            end_page, next_page, cur_page, page_count = get_paginate_dimensions(page, starting_page,
                                                                                nexting_page, previous_page,
                                                                                count, number_of_items)
    #from_date = date(2012, 01,01)
    #to_date = datetime.now()+timedelta(days=1)
    # header_list, data_list, currency = affiliate_reports_stats(affiliates_m_obj, from_date, to_date, camp_track_links, site_ids, country)

    header_list, data_list = change_logs_list(change_log_list)
    pagination_div  = makePaginationDiv(start_page, page_count, cur_page, next_page, count)

    context_dict = {"data_list":data_list,"header_list":header_list,
                        "from_date":from_date.strftime('%B %d, %Y'),"to_date":to_date.strftime('%B %d, %Y') ,
                        "ui_from_date":datetime_or_date_to_string(from_date),
                        "ui_to_date":datetime_or_date_to_string(to_date),
                       "pagination_div":pagination_div,
                       'domain': site_name
                       }

    if format_d == "json":
        context_dict["status"] = "success"
        context_dict['data_list'] = data_list
        context_dict["header_list"] = header_list
        return HttpResponse(json.dumps(context_dict, indent=4))
    context_dict['sites_obj_list'] = site_ids_list
    context_dict['siteid'] = sitefilter if sitefilter else []
    context_dict['sites_list'] = [[i.id, i.name] for i in acc_manager.siteid.all().order_by('-id')]
    context_dict["aff_list"] = [{'id':i.id, 'username': i.email.replace('vcommsubaff', '')} for i in affiliates_obj]
    context_dict['partner'] = acc_manager.affiliateprogramme.name
    context_dict['partner_obj'] = acc_manager.affiliateprogramme
    context_dict['part_user'] = ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
    # context_dict["campaign_list"] = campaign_list
    context_dict['data_list'] = json.dumps(data_list)
    context_dict["header_list"] = json.dumps(header_list)
    return render(request, 'partner/logs.html', context_dict)

def get_logs(request):
    resp_dict = {}
    try:
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner'),)
        user = User.objects.get(id=pid)
        acc_manager, partuser_obj, user = get_u_details(request)
        aff_prog = acc_manager.affiliateprogramme
        object_id = request.GET.get('object_id')
        model_name = request.GET.get('model')
        ct = ContentType.objects.filter(model=model_name).first()
        change_log_objs = ChangeLog.objects.filter(
                        affiliateprogramme=aff_prog,
                        object_id=object_id,
                        content_type=ct)
        change_dict_list = []
        for obj in change_log_objs:
            obj_dict = {}
            obj_dict['diff'] = json.loads(obj.changed_fields)
            obj_dict['time'] = str(obj.timestamp)
            obj_dict['user'] = obj.user.username
            obj_dict['action'] = obj.action
            obj_dict['ipaddress'] = obj.ipaddress
            change_dict_list.append(obj_dict)
        resp_dict['change_logs'] = change_dict_list
        resp_dict['status'] = 'Success'
    except Exception as e:
        resp_dict['error_message'] = str(e)
        resp_dict['status'] = 'Error'
        return HttpResponse(json.dumps(resp_dict, indent=4))
    return HttpResponse(json.dumps(resp_dict, indent=4))

@user_role_management('Registration Fields')
def registration_page_edit(request, **kwargs):
    pid = request.session.get('partnerid')
    context_dict = {}
    if not pid:
        return HttpResponseRedirect(reverse('partner'),)
    user = User.objects.get(id=pid)
    acc_manager, partuser_obj, user = get_u_details(request)
    context_dict['partner'] = acc_manager.affiliateprogramme.name
    return render(request, 'partner/registration-page-edit.html', context_dict)

def reg_page_edit_service(request):
    pid = request.session.get('partnerid')
    context_dict = {}
    if not pid:
        return HttpResponseRedirect(reverse('partner_login'),)
    acc_manager, partuser_obj, user = get_u_details(request)
    if request.method == "GET":
        reg_page_edit = RegistrationPageEdit.objects.filter(accmanager=acc_manager)
        options = []
        if reg_page_edit:
            for i in reg_page_edit[0].options:
                options.append(i.encode())
        return HttpResponse(json.dumps({"options": options}))
    else:
        data = json.loads(request.body)
        reg_page_edit = RegistrationPageEdit.objects.filter(accmanager=acc_manager)
        if reg_page_edit:
            reg_page_edit = reg_page_edit[0]
            reg_page_edit = RegistrationPageEdit.objects.filter(accmanager = acc_manager).update(options = data["options"])
        else:
            reg_page_edit = RegistrationPageEdit.objects.create(accmanager = acc_manager,
                                                            options = data["options"])
        if reg_page_edit:
            print('emailclient',reg_page_edit)
            context_dict["status"] = "success"
            return HttpResponse(json.dumps(context_dict))

class ThresholdAdmin(TemplateView):
    """ class edit profile """
    @user_class_role_management('Threshold')
    def get(self, request):
        context_dict = {}
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        user = User.objects.get(id=pid)
        acc_manager, partuser_obj, user = get_u_details(request)
        # aff_com_list = Affiliatecommission.objects.filter( fk_affiliateinfo__in=[affid])
        # aff_com_dict = dict( (i.fk_affiliateinfo_id, i)  for i in aff_com_list)
        data_list = []
        threshold_details = Threshold.objects.filter(account_manager = acc_manager)
        context_dict["partner"] = acc_manager.affiliateprogramme.name
        context_dict["partner_obj"] = acc_manager.affiliateprogramme
        context_dict["domain"] = "All Websites"
        context_dict["data"] = threshold_details[0] if threshold_details else {}
        currency_l = []
        # context_dict["commission_obj"] = aff_com_dict.get(obj.id)
        return render(request, 'partner/threshold.html', context_dict)

    @user_class_role_management('Threshold')
    def post(self, request):
        context_dict = {}
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner_login'),)
        user = User.objects.get(id=pid)
        acc_manager, partuser_obj, user = get_u_details(request)
        try:
            bankwire = request.POST.get("bankwire")
            neteller = request.POST.get("neteller")
            paypal = request.POST.get("paypal")
            
            threshold_details_obj, created = Threshold.objects.get_or_create(
                            account_manager=acc_manager)
            threshold_details_obj.bankwire = bankwire
            threshold_details_obj.neteller = neteller
            threshold_details_obj.paypal = paypal
            threshold_details_obj.save()
        except Exception as e:
            print str(e)
            context_dict["error_message"] = 'Error in editing Threshold info'
            context_dict["domain"] = "All Websites"
            return render(request, 'partner/threshold.html', context_dict)
        context_dict["partner"] = acc_manager.affiliateprogramme.name
        context_dict["partner_obj"] = acc_manager.affiliateprogramme
        context_dict["message"] = 'Your payment details have been successfully submitted.'
        context_dict["data"] = threshold_details_obj
        context_dict["domain"] = "All Websites"
        return render(request, 'partner/threshold.html', context_dict)


def save_payment_history(request):
    # Save status of the payment history page.
    resp_dict = {}
    try:
        pid = request.session.get('partnerid')
        if not pid:
            return HttpResponseRedirect(reverse('partner'),)
        payment_id = request.GET.get('payment_id')
        adjustments = request.GET.get('adjustments', 0.0)
        status = request.GET.get('status')
        adjustments = adjustments if adjustments != '' else 0.0
        s_aff = PaymentHistory.objects.get(id=int(payment_id))
        s_aff.status = status
        s_aff.adjusted_amount = adjustments
        if status == 'paid':
            s_aff.balance = 0
        else:
            s_aff.balance = s_aff.amount + s_aff.carried_amount + adjustments
        s_aff.save()
        resp_dict['status'] = 'Success'
    except Exception as e:
        import traceback
        traceback.print_exc()
        resp_dict['error_message'] = str(e)
        resp_dict['status'] = 'Error'
        return HttpResponse(json.dumps(resp_dict, indent=4))
    return HttpResponse(json.dumps(resp_dict, indent=4))

from io import BytesIO
from django.http import HttpResponse
from django.template.loader import get_template

from xhtml2pdf import pisa

def render_to_pdf(template_src, context_dict={}):
    template = get_template(template_src)
    html  = template.render(context_dict)
    result = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("ISO-8859-1")), result)
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type='application/pdf')
    return None

def invoice(request):
    if request.method == 'GET':
        pid =  request.GET.get('pid', None)
        context = {}
        template = get_template('partner/invoice.html')
        if pid:
            s_aff = PaymentHistory.objects.get(id=int(pid))
            account = AccountDetails.objects.filter(fk_affiliateinfo=s_aff.fk_affiliateinfo).order_by('id').first()
            if account:
                if account.accountnumber:
                    context['account_type'] = 'Bank Wire Transfer'
                    context['payment_cols'] = "<th>Account Number</th><th>Bank Name</th><th>Bank Address</th>"
                    context['payment_rows'] = "<td>"+uc_util(account.accountnumber)+"</td><td>"+uc_util(account.bankname)+"</td><td>"+uc_util(account.bankaddress)+"</td>"
                    context['accountnumber'] = uc_util(account.accountnumber)
                    context['bankname'] = uc_util(account.bankname)
                    context['bankaddress'] = uc_util(account.bankaddress)
                if account.paypal_username:
                    context['account_type'] = 'PAYPAL'
                    # context['payment_cols'] = "<th>Paypal Username</th><th>Paypal Email</th>"
                    # context['payment_rows'] = "<td>"+uc_util(account.paypal_username)+"</td><td>"+uc_util(account.paypal_email)+"</td>"
                    context['paypal_username'] = uc_util(account.paypal_username)
                    context['paypal_email'] = uc_util(account.paypal_email)
                elif account.neteller_username:
                    context['account_type'] = 'NETELLER'
                    # context['payment_cols'] = "<th>Account Number</th><th>Neteller Email</th><th>Neteller Username</th>"
                    # context['payment_details'] = "<td>"+uc_util(account.neteller_accountnumber)+"</td><td>"+uc_util(account.neteller_email)+"</td><td>"+uc_util(account.neteller_username)+"</td>"
                    context['neteller_accountnumber'] = uc_util(account.neteller_accountnumber)
                    context['neteller_email'] = uc_util(account.neteller_email)
                    context['neteller_username'] =uc_util(account.neteller_username)
            else:
                context['account_type'] = 'NO DETAILS GIVEN'
            context['partner'] = s_aff.fk_affiliateinfo.account_manager.affiliateprogramme.name
            context['admin_email'] = s_aff.fk_affiliateinfo.account_manager.email
            context['aff_name'] = s_aff.fk_affiliateinfo.email
            context['aff_company'] = s_aff.fk_affiliateinfo.companyname
            context['aff_address'] = s_aff.fk_affiliateinfo.address
            context['aff_address2'] = s_aff.fk_affiliateinfo.address2
            context['aff_city'] = s_aff.fk_affiliateinfo.city 
            context['aff_state'] = s_aff.fk_affiliateinfo.state
            context['aff_country'] = s_aff.fk_affiliateinfo.country
            from datetime import date
            print(int(s_aff.year),int(s_aff.month),1)
            context['month'] = (datetime.date(datetime(int(s_aff.year),int(s_aff.month),1))).strftime('%B')
            context['year'] = s_aff.year
            context['balance'] = s_aff.balance
            path = '/var/www/wynta/wynta_new/static_files/new_wynta/images/affLogos/'
            programme_logo = {
                'Fozil Partners' : path + 'fozilpartners.jpg',
                'vCommission UK Partners' : path + 'vcommission.jpg',
                'BETNEO LIMITED' : path + 'betneo.jpg',
                'Dukes Casino Affiliates' : path + 'dukescasino.jpg',
                'Maxi Affiliates' : path + 'maxiaffiliates.jpg',
                'vstarbet affiliate program' : path + 'starbet.jpg',
                'Swish Bet Affiliates' : path + 'swishbet.jpg',
                'Super Win UK' : path + 'superwin.jpg',
            }
            context['logo'] = programme_logo[context['partner']] if context['partner'] in programme_logo else path+'wynta-logo.jpg' 
            print('here  contet',context['logo'])
            invoice_obj = Invoice.objects.create(logopath=context['logo'], pay_history=s_aff, timestamp = datetime.now())
            context['invoice_id'] = invoice_obj.id
            context['invoice_date'] = (datetime.now()).strftime("%d/%m/%Y")
            if invoice_obj:
                html = template.render(context)
                pdf = render_to_pdf('partner/invoice.html', context)
                if pdf:
                    response = HttpResponse(pdf, content_type='application/pdf')
                    filename = "Invoice_%s.pdf" %(invoice_obj.id)
                    content = "inline; filename='%s'" %(filename)
                    content = "attachment; filename='%s'" %(filename)
                    response['Content-Disposition'] = content
                    return response
                return HttpResponse("Not found")

def uc_util(a):
    if a:
        return a.encode('utf-8').strip()
    else:
        return None


class PaymentHistoryViewSet(viewsets.ModelViewSet):

    """
    Endpoint: /admin/payment-report/

    GET: Typical RESTful Response including Pagination.

    `search` query parameter => ?search=<search text>

    PUT & PATCH are available though no validations
                beyond data types exist on it for now.

    """
    queryset = PaymentHistory.objects.all()
    serializer_class = PaymentHistorySerializer
    filter_backends = (filters.SearchFilter,
                       DjangoFilterBackend, filters.OrderingFilter,)
    parser_classes = (MultiPartParser, FormParser,)
    renderer_classes = (renderers.JSONRenderer, renderers.TemplateHTMLRenderer)
    pagination_class = None

    @user_class_role_management('Payment Report')
    def list(self, request, *args, **kwargs):
        format_d = request.GET.get("format")
        custom = str(request.GET.get("customid", "Last 45 Days"))
        fromdate = request.GET.get("fromdate")
        todate = request.GET.get("todate")
        if fromdate:
            fromdate = str(fromdate).split('/')
            fromdate = fromdate[0]+'/01/'+fromdate[1]
            todate = str(todate).split('/')
            todate = todate[0]+'/01/'+todate[1]
        aff_id = request.GET.get('aff_id')
        queryset = self.filter_queryset(self.get_queryset())
        acc_manager, partuser_obj, user = get_u_details(request)
        queryset = queryset.filter(fk_affiliateinfo__account_manager=acc_manager)
        print request.GET
        if aff_id:
            queryset = queryset.filter(fk_affiliateinfo=aff_id)
        date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
        if date_times_dict:
            from_date = date_times_dict.get('fromdate')
            to_date = date_times_dict.get('todate')
            if to_date.year != from_date.year:
                f_queryset = queryset.filter(Q(year=to_date.year)&Q(month__lte=to_date.month))
                g_queryset = queryset.filter(Q(year__gte=from_date.year)&Q(month__gte=from_date.month))
                queryset = f_queryset|g_queryset
            else:
                queryset = queryset.filter(month__lte=to_date.month,
                                month__gte=from_date.month,
                                year__lte=to_date.year,
                                year__gte=from_date.year)
            print date_times_dict
        affiliates_obj = Affiliateinfo.objects.filter(
                account_manager=acc_manager).exclude(username=user.username)
        queryset = queryset.order_by('-timestamp')
        count_d = len(queryset)
        # page = True
        context_dict = {}
        serializer = self.get_serializer(queryset, many=True)
        if serializer:
            cpage = int(request.GET.get('page')) if request.GET.get('page') else 1
            header_list = [{'title': 'Id'}, {'title': 'Affiliate Id'}, {'title': 'Affiliate'}, {'title': 'Month'}, 
                        {'title':'Earnings'}, {'title':'Carry Over'}, 
                        {'title':'Adjustments'}, {'title':'Balance'}, {'title':'Threshold'},
                        {'title':'Status'} 
                        ]
            page_div = ''
            data_list = []
            for i in serializer.data:
                val_list = []
                ac_m = ''
                s_aff= ''
                if i['fk_affiliateinfo']:
                    s_aff = Affiliateinfo.objects.filter(id=i['fk_affiliateinfo'])
                    s_aff = str(s_aff[0].email.replace('vcommsubaff', ''))
                comm_page = ''
                past_month = datetime.now().replace(day=1) - timedelta(days=1)
                if str(past_month.month) == str(i['month']) and str(past_month.year) == str(i['year']):
                    adjustments = '<input type="text" name="adjustments" oninput="savestatus(this)" hrefs="/admin/save-payment-report/?payment_id='+str(i['id'])+'" value="'+str(i['adjusted_amount'])+'" class="adjustments">'
                else:
                    adjustments = '<input type="text" value="'+str(i['adjusted_amount'])+'" readonly>'
                save_status = '<button class="btn btn-purple" onclick="savestatus(this)" hrefs="/admin/save-payment-report/?payment_id='+str(i['id'])+'">Save</button>'
                select_status = '<select class="picker" name="status" onchange="savestatus(this)" hrefs="/admin/save-payment-report/?payment_id='+str(i['id'])+'">'
                if str(i['status']) == 'pay':
                    select_status += '<option value="pay" selected>To be Paid</option>'
                else:
                    select_status += '<option value="pay">To be Paid</option>'
                if str(i['status']) == 'paid':
                    select_status += '<option value="paid" selected>Paid</option>'
                else:
                    select_status += '<option value="paid">Paid</option>'
                if str(i['status']) == 'carried':
                    select_status += '<option value="carried" selected>Carried Over</option></select>'
                else:
                    select_status += '<option value="carried">Carried Over</option></select>'
                val_list = [str(i['id']), str(i['fk_affiliateinfo']), s_aff, str(i['month'])+'-'+str(i['year']),
                            str(i['amount']), str(i['carried_amount']), adjustments,
                            str(i['balance']),
                            str(i['threshold_amount']),
                            select_status]
                data_list.append(val_list)
            print 'Hi', format_d, data_list
            if format_d == "json":
                context_dict["status"] = "success"
                context_dict['data_list'] = data_list
                context_dict["header_list"] = header_list
                return HttpResponse(json.dumps(context_dict, indent=4))
            msg = ''
            print 'wrong'
            if request.session.get('status_changed'):
                msg = 'The Payment status changed.'
                if request.session.get('status_changed'):
                    del request.session['status_changed']
            return Response({'header_list': header_list, 'count_d': count_d, 'data_list': data_list, 'pagination_div': page_div,
                        'message':msg,
                        "from_date":from_date.strftime('%B %d, %Y'),"to_date":to_date.strftime('%B %d, %Y') ,
                        "ui_from_date":datetime_or_date_to_string(from_date),
                        "ui_to_date":datetime_or_date_to_string(to_date),
                        'aff_list': [{'id':i.id, 'username': i.email.replace('vcommsubaff', '')} for i in affiliates_obj],
                        'partner': acc_manager.affiliateprogramme.name,
                        'partner_obj': acc_manager.affiliateprogramme,
                        'part_user':ast.literal_eval(partuser_obj[0].options) if partuser_obj else []},
                        template_name='partner/payment_history_list.html')
        # return self.get_paginated_response(serializer.data)
        # serializer = self.get_serializer(queryset, many=True)
        if request.accepted_renderer.format == 'html':
            return Response({'data': serializer.data, 'count_d': count_d, 'page': filt_er}, template_name='partner/affiliate_list.html')
        return Response(serializer.data)

def whats_new(request):
    pid = request.session.get('partnerid')
    context_dict = {}
    if not pid:
        return HttpResponseRedirect(reverse('partner_login'),)
    acc_manager, partuser_obj, user = get_u_details(request)
    if request.method == 'POST':
        notificationtype = request.POST.get('select_type', None)
        title = request.POST.get('title', None)
        content = request.POST.get('content', None)
        redirecturl = request.POST.get('redirectURL', None)
        category = request.POST.get('category', None)
        id = request.POST.get('id',None)
        if not id:
            lastseen =datetime.now()
            notifications_obj = Notifications.objects.create(accmanager=acc_manager,
                                                            notificationtype=notificationtype,
                                                            title=title,
                                                            content=content,
                                                            redirecturl=redirecturl,
                                                            lastupdated=lastseen,
                                                            lastseen=lastseen,
                                                            category=category)
        else:
            id = int(id.encode())
            notifications_obj = Notifications.objects.filter(id=id).update(notificationtype=notificationtype,
                                                                            title=title,
                                                                            content=content,
                                                                            redirecturl=redirecturl,
                                                                            lastupdated=datetime.now(),
                                                                            category=category)
        if notifications_obj:
            return HttpResponse(json.dumps({'status':"Success"}))
    else:
        header_list = [
                   { "title": "Notification Type"},
                   { "title": "Title"},
                   { "title": "Content"},
                   { "title": "Redirect URL"}
               ]
        if acc_manager.id ==7:
            header_list.append({"title":"Category"})
            header_list.append({"title":"View/Edit"})
        data_list = []
        notifications_obj =  Notifications.objects.filter(accmanager=AccountManager.objects.get(id=7)).order_by('-id')
        if notifications_obj:    
            for i in notifications_obj:
                if i.notificationtype and i.content and i.redirecturl:    
                    if i.notificationtype == 'F':
                        notificationtype = 'New Feature'
                    elif i.notificationtype == 'A':
                        notificationtype = 'Announcement'
                    else:
                        notificationtype = 'New Release'
                    redirectURL = '<a href="'+ i.redirecturl +'" class="btn btn-success pl-3 pr-3 shadow-none" target="_blank">Click Here</a>'
                    if acc_manager.id == 7:
                        edit_ele = '<button class="btn btn-success" id='+ str(i.id)+' onclick="getRow(this)">View/Edit</button></a>'
                        data_list.append([notificationtype,i.title,i.content,redirectURL,i.category,edit_ele])
                    else:
                        data_list.append([notificationtype,i.title,i.content,redirectURL])
        context_dict['data_list'] = json.dumps(data_list)
        context_dict["header_list"] = json.dumps(header_list)
        context_dict['acc_manager'] = json.dumps(acc_manager.id)
        context_dict["page_view"] = json.dumps('admin')
        print('acc manager id', context_dict['acc_manager'])
        return render(request,'partner/whats-new.html', context_dict)


def get_notifications(request):
    pid = request.session.get('partnerid')
    context_dict = {}
    # if not pid:
    #     return HttpResponseRedirect(reverse('partner_login'),)
    # acc_manager, partuser_obj, user = get_u_details(request)
    # if request.method == 'GET':
    #     lastseen = datetime.now()
    #     if acc_manager ==7:
    #         noti_acc_obj = Notifications.objects.filter(accmanager=acc_manager).order_by('-id')
    #     else:
    #         noti_acc_obj = Notifications.objects.filter(accmanager=acc_manager)
    #     if noti_acc_obj:
    #         before_last_seen = noti_acc_obj[0].lastseen
    #         # noti_acc_obj.update(lastseen=lastseen)
    #     else:
    #         before_last_seen = None
    #         noti_acc_obj = Notifications.objects.create(accmanager=acc_manager)
    #     if before_last_seen:    
    #         notifications_obj = Notifications.objects.filter(accmanager=AccountManager.objects.get(id=7),
    #                                                         lastupdated__lte=lastseen, lastupdated__gte=before_last_seen).filter((Q(category='Admin')) | (Q(category='Both'))).order_by('-id')
    #     else:
    #         notifications_obj = Notifications.objects.filter(accmanager=AccountManager.objects.get(id=7),
    #                                                         lastupdated__lte=lastseen).filter((Q(category='Admin')) | (Q(category='Both'))).order_by('-id')
        
    #     print('notifications',notifications_obj)
    
    #     notifications = []
    #     if notifications_obj:
    #         for i in notifications_obj:
    #             notification = {}
    #             notification['id'] = i.id
    #             notification['notificationtype'] = i.notificationtype
    #             notification['title'] = i.title
    #             notification['content'] = i.content
    #             notification['redirecturl'] = i.redirecturl
    #             notifications.append(notification)
    # if request.method == 'POST':
    #     lastseen = datetime.now()
    #     if acc_manager ==7:
    #         noti_acc_obj = Notifications.objects.filter(accmanager=acc_manager).order_by('-id')
    #     else:
    #         noti_acc_obj = Notifications.objects.filter(accmanager=acc_manager)
    #     noti_acc_obj.update(lastseen=lastseen)
    notifications = []
    return HttpResponse(json.dumps({'notifications' : notifications, 'status' : 'Success'}))

@user_role_management('Email Client Integration')
def email_client_integration(request, **kwargs):
    pid = request.session.get('partnerid')
    acc_manager, partuser_obj, user = get_u_details(request)
    context_dict = {}
    if not pid:
        return HttpResponseRedirect(reverse('partner_login'),)
    context_dict['partner'] = acc_manager.affiliateprogramme.name
    return render(request, 'partner/email-client-integration.html', context_dict)

def email_client_service(request): 
    pid = request.session.get('partnerid')
    user = User.objects.get(id=pid)
    acc_manager_obj = AccountManager.objects.filter(user=user)
    partuser_obj = []
    acc_manager = []
    if acc_manager_obj:
        acc_manager = acc_manager_obj[0]
    else:
        partuser_obj = PartnerUser.objects.filter(user=user)
        if partuser_obj:
            partuser = partuser_obj[0]
    request.user.username = user.username
    request.user.email = user.email
    context_dict = {}
    if not pid:
        return HttpResponseRedirect(reverse('partner_login'),)
    if request.method == 'GET':
        if acc_manager:
            email_client_obj = Emailclient.objects.filter(accmanager = acc_manager)
        elif partuser:
            email_client_obj = Emailclient.objects.filter(partuser = partuser)
        if email_client_obj:
            email_client_obj = email_client_obj[0]
            context_dict['username'] = email_client_obj.username
            context_dict['password'] = email_client_obj.password
            context_dict['emailclient'] = email_client_obj.emailclient.encode()
            context_dict['domain'] = email_client_obj.domain
            context_dict['apikey'] = email_client_obj.apikey
        else:
            context_dict['username'] = ''
            context_dict['password'] = ''
            context_dict['emailclient'] = None
            context_dict['domain'] = ''
            context_dict['apikey'] = ''
        return HttpResponse(json.dumps(context_dict))

    if request.method == 'POST':    
        data = json.loads(request.body)
        for key,value in data.items():
            if data[key]:
                data[key]=data[key].encode()
        if acc_manager:
            email_client_obj = Emailclient.objects.filter(accmanager = acc_manager)
            if email_client_obj:
                email_client_obj = email_client_obj[0]
                if (data["username"] and data['password']) or (data["domain"] and data["apikey"]):
                    email_client_obj = Emailclient.objects.filter(accmanager = acc_manager).update(username = data["username"],
                                                                                                    password = data["password"],
                                                                                                    emailclient = data["emailclient"],
                                                                                                    domain = data["domain"],
                                                                                                    apikey = data["apikey"])
                else:
                    email_client_obj =  Emailclient.objects.filter(accmanager = acc_manager).delete()
                    context_dict["status"] = "success"
                    return HttpResponse(json.dumps(context_dict))
            else:
                email_client_obj = Emailclient.objects.create(accmanager = acc_manager,
                                                                username = data["username"],
                                                                password = data["password"],
                                                                emailclient = data["emailclient"],
                                                                domain = data["domain"],
                                                                apikey = data["apikey"])
        elif partuser:
            email_client_obj = Emailclient.objects.filter(partuser = partuser)
            if email_client_obj:
                email_client_obj = email_client_obj[0]
                if (data["username"] and data['password']) or (data["domain"] and data["apikey"]):
                    email_client_obj = Emailclient.objects.filter(partuser = partuser).update(username = data["username"],
                                                                                                    password = data["password"],
                                                                                                    emailclient = data["emailclient"],
                                                                                                    domain = data["domain"],
                                                                                                    apikey = data["apikey"])
                else:
                    email_client_obj =  Emailclient.objects.filter(partuser = partuser).delete()
                    context_dict["status"] = "success"
                    return HttpResponse(json.dumps(context_dict))
            else:
                email_client_obj = Emailclient.objects.create(partuser = partuser,
                                                                username = data["username"],
                                                                password = data["password"],
                                                                emailclient = data["emailclient"],
                                                                domain = data["domain"],
                                                                apikey = data["apikey"])
        
        if email_client_obj:
            print('emailclient',email_client_obj)
            context_dict["status"] = "success"
            return HttpResponse(json.dumps(context_dict))
