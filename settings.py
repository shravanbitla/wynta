"""
this is django setting file for rummy project 
"""
from env_settings.base import *

# debug = True will disbale 404 page and False will be 
DEBUG = True
#DEBUG = False

ALLOWED_HOSTS = ['*','wynta.glserv.info']
PROJECT_PATH = os.path.dirname(os.path.dirname(__file__))
DIRNAME = os.path.abspath(os.path.dirname(__file__))


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'livefozilp',                      # Or path to database file if using sqlite3.
        'USER': 'root',                      # Not used with sqlite3.
        'PASSWORD': 'test5',                  # Not used with sqlite3.
        'HOST': '',
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
    # 'default': {
    #     'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
    #     'NAME': 'wyntadb',                      # Or path to database file if using sqlite3.
    #     'USER': 'fozilproot',                      # Not used with sqlite3.
    #     'PASSWORD': 'fozilproot#$%^Rew',                  # Not used with sqlite3.
    #     'HOST': 'fozilp-wynta.czkmjxaekygv.eu-west-2.rds.amazonaws.com',
    #     'PORT': '3306',                      # Set to empty string for default. Not used with sqlite3.
    # }
}


TINYMCE_JS_URL = PROJECT_PATH + '/static_files/js/tiny_mce/tiny_mce.js'

#TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    #os.path.join(DIRNAME, 'templates'),
    # Don't forget to use absolute paths, not relative paths.
#)

STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(DIRNAME, 'static_files'),
)

PP_User_Name = 'adminsticky'
PP_Password = 'SSwlm^49'

ROOT_URLCONF = 'urls'

LOGGING['handlers']['error_log_handler'].update({'filename':'errors_log_Wynta.log'})

ROBOT_META_FOLLOW_INDEX = False

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
           os.path.join(DIRNAME, 'templates')
        ],
        'OPTIONS': {
            'context_processors': [
                # Insert your TEMPLATE_CONTEXT_PROCESSORS here or use this
                # list if you haven't customized them:
                "django.core.context_processors.debug",
                "django.core.context_processors.i18n",
                "django.core.context_processors.static",
                "django.core.context_processors.media",
                "django.contrib.auth.context_processors.auth",
                "django.core.context_processors.request",
                "custom_template_processor.get_meta_flags"
            ],
            'loaders': [
               'django.template.loaders.filesystem.Loader',
               'django.template.loaders.app_directories.Loader',
            ]
        },
    },
]

cache_time = 172800

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}