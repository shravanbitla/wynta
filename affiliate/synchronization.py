import json
from logging import getLogger
from datetime import datetime
from django.middleware import csrf

from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import gettext as _

from .serializers import ChangeLogSerializer
from .models import *
from userapp.models import *
from threadlocals.threadlocals import get_current_request
from middleware import CurrentUserMiddleware
logger = getLogger(__name__)


class ChangeLogActions:
    CREATE = 'create'
    UPDATE = 'update'
    DELETE = 'delete'

CHANGE_LOG_DIFF_EXCLUDED_FIELDS = []

class ChangeLogSynchronizer:

    @classmethod
    def post_save_receiver(cls, sender, instance, **kwargs):
        created = kwargs.get('created', True)
        if created:
            action = ChangeLogActions.CREATE
        else:
            action = ChangeLogActions.UPDATE
        cls.save_and_trace_change(instance, action)

    @classmethod
    def m2m_changed_receiver(
            cls, sender, instance, action, reverse, model, pk_set, using, **kwargs):
        # All the m2m changes will be logged as Update as they won't create new record
        change_log_action = ChangeLogActions.UPDATE
        cls.save_and_trace_change(instance, change_log_action)

    @classmethod
    def post_delete_receiver(cls, sender, instance, **kwargs):
        action = ChangeLogActions.DELETE
        cls.save_and_trace_change(instance, action)

    @classmethod
    def get_action(cls, **kwargs):
        created = kwargs.get('created', True)
        if created:
            return ChangeLogActions.CREATE
        else:
            return ChangeLogActions.UPDATE

    @classmethod
    def get_selected_version(cls, user):
        if hasattr(user, 'profile'):
            return user.profile.selected_version
        else:
            return None

    @classmethod
    def get_serialized_data(cls, instance):
        s = ChangeLogSerializer()
        return s.serialize(instance)

    @classmethod
    def get_object_diff(cls, instance_id, ct, current_data):
        changed_fields = dict()
        from .models import *
        prev_obj = ChangeLog.objects.filter(
            content_type=ct,
            object_id=instance_id
        ).order_by('-timestamp').first()

        if prev_obj and prev_obj.serialized_data:
            prev_data = json.loads(prev_obj.serialized_data)
        else:
            prev_data = {}

        for attr, current_val in current_data.items():
            if attr not in CHANGE_LOG_DIFF_EXCLUDED_FIELDS:
                # This logic is for when prev_obj or attr is not available
                if attr not in prev_data:
                    # checking for current_value to exclude the comparision between empty values
                    # (like empty list -[]) and N/A in UI
                    if current_val and current_val != 'N/A':
                        changed_fields[attr] = ('N/A', current_val)
                else:
                    prev_val = prev_data[attr]
                    if prev_val != current_val:
                        changed_fields[attr] = (prev_data[attr], current_val)

        return changed_fields, prev_obj

    @classmethod
    def save_and_trace_change(cls, instance, action):
        """
        Method that handles the logging off all the changes that happened on the System.
        Changes will get logged on :
            Basic logging system.
            Audit model (ChangeLog) table on database.

        :param: obj the objects whose change is to be logged
        :param: user who is responsible of the change
        :param: action that was performed (creation, update or delete)

        """
        from .models import *
        try:
            if ChangeLog == type(instance):
                return True
            print 'change log instance', action, instance
            requestS = get_current_request()
            if not requestS:
                return True
            username = requestS.user.username
            print('username1', username)
            user = None
            aff_obj = None
            aff_prog = None
            ipaddress = requestS.META['REMOTE_ADDR']
            request = CurrentUserMiddleware().thread_local
            username = request.current_user if hasattr(request, 'current_user') else username
            print('username2', username)
            import uuid
            request_event = request.x_trace_token if hasattr(request, 'x_trace_token') else requestS.META.get('X-Trace-Token', str(uuid.uuid4()))
            import json
            print 'check 0 done', user, username, request
            if username:
                print 'check -1 done'
                user = User.objects.filter(username=username).first()
                acc_manager = AccountManager.objects.filter(user=user).first()
                if not acc_manager:
                    partuser_obj = PartnerUser.objects.filter(user=user)
                    if partuser_obj:
                        acc_manager = partuser_obj[0].accmanager
                aff_prog = acc_manager.affiliateprogramme
            else:
                # aff_id = request.session['affid'] if request.session.get('affid') else None
                aff_id = None
                if not aff_id:
                    aff_id = request.aff_id if hasattr(request, 'aff_id') else None
                if not aff_id:
                    aff_id =  requestS.session['affid'] if requestS.session.get('affid') else None
                if aff_id is not None:
                    aff_obj = Affiliateinfo.objects.get(id=aff_id)
                    aff_prog = aff_obj.account_manager.affiliateprogramme
            print 'check 1 done'  
            ct = ContentType.objects.get_for_model(instance)
            if user and instance:
                # We log the change as a formatted string at the "info" level
                print 'check 2 done'
                serialized_data = cls.get_serialized_data(instance)
                diff = None
                if action in [ChangeLogActions.UPDATE, ChangeLogActions.DELETE]:
                    if action == ChangeLogActions.UPDATE:
                        diff, prev_obj = cls.get_object_diff(instance.id, ct, serialized_data)
                        if not diff and action == ChangeLogActions.UPDATE:
                            return
                    else:
                        prev_obj = ChangeLog.objects.filter(
                            content_type=ct,
                            object_id=instance.id
                        ).order_by('-timestamp').first()
                    """ Check if it is duplicate entry with request event,
                    user and instance code """
                    print 'check 3 done'
                    if not request_event:
                        request_event = csrf.get_token(request)
                    if prev_obj and prev_obj.request_event == request_event:
                        print 'check 4 done'
                        if action == ChangeLogActions.DELETE:
                            prev_obj.action = action
                            """On delete an update is first triggered,
                            Updating those changes into serialized data."""
                            if prev_obj.changed_fields:
                                prev_diff = json.loads(prev_obj.changed_fields)
                                delete_field = prev_diff if prev_diff else {}
                                for k, v in delete_field.items():
                                    serialized_data[k] = v[0]
                        elif prev_obj.action == ChangeLogActions.UPDATE:
                            prev_diff = json.loads(prev_obj.changed_fields)
                            diff.update(prev_diff)
                            prev_obj.changed_fields = json.dumps(diff)
                        prev_obj.serialized_data = json.dumps(serialized_data)
                        prev_obj.save(update_fields=['serialized_data', 'changed_fields', 'action'])
                        return
                if not request_event:
                    request_event = csrf.get_token(request)
                print 'change log 5 hi', diff, serialized_data, ct
                change_log_obj = ChangeLog.objects.create(
                    timestamp=datetime.utcnow(),
                    action=action,
                    user=user,
                    content_type=ct,
                    object_id=instance.id,
                    serialized_data=json.dumps(serialized_data),
                    changed_fields=json.dumps(diff),
                    request_event=request_event,
                    affiliateprogramme=aff_prog,
                    fk_affiliateinfo=aff_obj,
                    ipaddress=ipaddress
                )
        except Exception as e:
            print('exception in synchronization.py',e)
            import traceback
            traceback.print_exc()

