from django.apps import AppConfig


class CommonConfig(AppConfig):
    name = 'affiliate'
    label = 'affiliate'
    def ready(self):
        from .signals import connect_signals
        connect_signals()
