""" affiliate/models.py"""
import os
from PIL import Image as PImage
# # # # # # # # # # from django.contrib.sites.models import get_current_site
from django.contrib.auth.models import User
from django.db import models
from settings import MEDIA_ROOT
from django.contrib.sites.models import Site
from json_field import JSONField
from django.utils import timezone
#from userapp.models import Network
from django.apps import apps
from django.db import models
from django.utils.translation import gettext_lazy as _
from .synchronization import ChangeLogActions
from django.contrib.contenttypes.models import ContentType


class EnumField(models.Field):
    """
    custom Enum Field
    """

    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 104
        super(EnumField, self).__init__(*args, **kwargs)
        if not self.choices:
            raise AttributeError('EnumField requires `choices` attribute.')

    def db_type(self, connection):
        return "enum(%s)" % ','.join("'%s'" % k for (k, _) in self.choices)


class Affiliateinfo(models.Model):
    """
    table to store affiliate signup data .
    """
    AFFILIATESTATUS = (("Pending","Pending"),
            ("Approved","Approved"),
            ("Rejected","Rejected"),)
    networkid = models.ManyToManyField("userapp.Network", db_column='networkid', null=True,blank=True)
    account_manager = models.ForeignKey("userapp.AccountManager", db_column='accountmanager', null=True, blank=True)
    # pmcmanager = models.ForeignKey("pmc.PmcAccountManager", db_column='pmcmanager', null=True, blank=True)
    mappingid = models.IntegerField(null=True,blank=True)
    website = models.CharField(max_length=500, null=True, blank=True)
    siteid = models.ManyToManyField(Site, null=True,blank=True)
    username = models.CharField(max_length=128)
    password = models.CharField(max_length=128)
    contactname = models.CharField(max_length=256 , null=True,blank=True)
    lastname = models.CharField(max_length=30, null=True, blank=True)
    companyname = models.CharField(max_length=256, null=True, blank=True)
    email = models.EmailField()
    address = models.CharField(max_length=256, null=True, blank=True)
    address2 = models.CharField(max_length=256, null=True, blank=True)
    city = models.CharField(max_length=100, null=True, blank=True)
    state = models.CharField(max_length=256, null=True, blank=True)
    country = models.CharField(max_length=256, null=True, blank=True)
    postcode = models.CharField(max_length=100, null=True, blank=True)
    telephone = models.BigIntegerField(null=True, blank=True)
    ipaddress = models.GenericIPAddressField(null=True,blank=True)
    registeredon = models.DateTimeField(default=timezone.now)
    active = models.BooleanField(default=False)
    status = models.CharField(max_length=30, choices=AFFILIATESTATUS, default='Pending', verbose_name ='Status')
    mailrequired = models.BooleanField(default=True)
    mobilerequired = models.BooleanField(default=True)
    im = models.BooleanField(default=True)
    skype = models.CharField(max_length=100, null=True, blank=True)
    dob = models.DateField(default=timezone.now)
    campaignlimit = models.IntegerField(default=100)
    superaffiliate = models.ForeignKey('self', db_column='superaffiliateid', null=True, blank=True)
    accessible = models.BooleanField(default=True)
    lastlogin = models.DateTimeField(default=timezone.now)

    def __str__(self):
        """ it will return username of each affiliate"""
        return str(self.username)
    @property
    def getAffRefChoice(self):
        """ Function to return referal choice for an affiliate"""
        try:
            commission = Affiliatecommission.objects.filter(fk_affiliateinfo_id=self.id, siteid__isnull=True)
            if commission:
                commission = commission[0]
            if commission.referalchoice:
                return True
            else:
                return False
        except Exception:
            return False


class AffiliateSite(models.Model):
    """docstring for Inviteaffiliate"""
    fk_affiliateinfo = models.ForeignKey(Affiliateinfo, null=True)
    site_id = models.IntegerField(null=True, blank=True)
    info = models.CharField(max_length=250, null=True, blank=True)

    def __str__(self):
        return str(self.id)  


class Inviteaffiliate(models.Model):
    """docstring for Inviteaffiliate"""
    fk_affiliateinfo = models.ForeignKey(Affiliateinfo, null=True)
    account_manager = models.ForeignKey("userapp.AccountManager", db_column='accountmanager', null=True, blank=True)

    def __str__(self):
        return str(self.id)  


class Advertiser(models.Model):
    """
    To store Advertiser(links) of affiliates
    """
    account_manager = models.ForeignKey('auth.user')
    link = models.TextField()
    description = models.TextField()
    name = models.CharField(max_length=30, unique=True)
    created_timestamp = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return str(self.id)

class Offer(models.Model):
    """
    To store Offer(links) of affiliates
    """
    networkid = models.ForeignKey("userapp.Network", db_column='networkid',null=True,blank=True)
    advertiser = models.ForeignKey(Advertiser)
    siteid = models.ForeignKey(Site, db_column='siteid',null=True,blank=True)
    fk_affiliateinfo = models.ForeignKey(Affiliateinfo, null=True)
    link = models.TextField()
    name = models.CharField(max_length=30, unique=True)
    created_timestamp = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return str(self.id)

class MappingParameters(models.Model):
    client = models.ForeignKey(Site)
    generic_parameter = models.CharField(max_length=100)
    client_parameter = models.CharField(max_length=100)


class Chequedetails(models.Model):
    """
    store cheque details of each affiliate
    """
    fk_affiliateinfo = models.ForeignKey(Affiliateinfo, null=True,blank=True)
    chequepayableto = models.CharField(max_length=30, null=True, unique=True)

    def __str__(self):
        """ return chequepayableto name """
        return str(self.chequepayableto)


class Wiretransfer(models.Model):
    """
    To store wire details of each affiliates
    """
    fk_affiliateinfo = models.ForeignKey(Affiliateinfo)
    accountnumber = models.CharField(max_length=30)
    ifsccode = models.CharField(max_length=30)
    accounttype = models.CharField(max_length=30)
    accountname = models.CharField(max_length=30)
    bankname = models.CharField(max_length=30)
    bankaddress = models.CharField(max_length=30)

    def __str__(self):
        """ return unique id of each wire detail"""
        return str(self.id)

class MediaCategory(models.Model):
    networkid = models.ForeignKey("userapp.Network", db_column='networkid', null=True,blank=True)
    siteid = models.ForeignKey(Site, db_column='siteid', null=True, blank=True)
    category_name = models.CharField(max_length=200)
    user = models.ForeignKey(User, null=True, blank=True)

class Campaign(models.Model):
    """
    To store campaign(links) of affiliates
    """
    #advertiser = models.ForeignKey(Advertiser)
    TEXT = 'T'
    BANNER = 'B'
    HTML5 = 'H'
    English = 'English'
    Spanish = 'Spanish'
    Finnish = 'Finnish'
    German = 'German'
    PAGE = 'P'
    VIDEO = 'V'
    MAILERS = 'M'
    SCREENSHOTS = 'SS'
    LANDINGPAGES = 'LP'
    # MEDIA_TYPE = ((TEXT,"TEXT Link"),
    #                 (BANNER,"BANNER"),
    #                 (HTML5,"HTML5"),
    #                 (PAGE,"PAGE PEELS"),
    #                 (MAILERS,"MAILERS"),
    #                 (SCREENSHOTS,"SCREENSHOTS"),
    #                 (VIDEO,"VIDEO"),
    #                 (LANDINGPAGES,"LANDINGPAGES"),)
    MEDIA_TYPE = ((TEXT,"Text Link"),
                    (BANNER,"Banner"),
                    (PAGE,"Page Peel"),
                    (LANDINGPAGES,"Landing Page"),
                    (SCREENSHOTS,"Screenshot"),
                    (VIDEO,"Video"),
                    (MAILERS,"MAILERS"),
                    )

    Language = ((English,"English"),
                    (Spanish,"Spanish"),
                    (Finnish,"Finnish"),
                    (German,"German"),)

    networkid = models.ForeignKey("userapp.Network", db_column='networkid',null=True, blank=True)
    siteid = models.ForeignKey(Site, db_column='siteid', null=True, blank=True)
    fk_affiliateinfo = models.ForeignKey(Affiliateinfo, null=True, blank=True)
    account_manager = models.ForeignKey("userapp.AccountManager", db_column='accountmanager', null=True, blank=True)
    link = models.TextField(null=True, blank=True)
    name = models.CharField(max_length=200)
    created_timestamp = models.DateTimeField(default=timezone.now)
    media_type = EnumField(choices=MEDIA_TYPE,verbose_name ='Media Type', null=True, blank=True)
    media_category = models.ForeignKey(MediaCategory, db_column='media_category', null=True, blank=True)
    language = EnumField(choices=Language,verbose_name ='Language', null=True, blank=True)


    def __str__(self):
        return str(self.id)


class CampaignTrackerMapping(models.Model):
    """
    To store mapping of Tracker and campaign (links) of affiliates.
    """
    fk_campaign = models.ForeignKey(Campaign, db_column='fk_campaign_id', null=True, blank=True)
    fk_affiliateinfo = models.ForeignKey(Affiliateinfo, db_column='fk_affiliateinfo_id', null=True, blank=True)
    trackerid = models.IntegerField(null=True, blank=True)
    url = models.TextField(null=True,blank=True)
    campaign_reg_code = models.CharField(max_length=50, null=True, blank=True)
    goalid = models.IntegerField(default=1)

    # def __str__(self):
    #     """ it will return username of each affiliate"""
    #     return str(self.trackerid)
    def __str__(self):
        return "{0}".format(str(self.id))


class CampaignClicksCount(models.Model):
    """
    To store mapping of Clicks and campaign count.
    insert into affiliate_campaignclickscount(fk_campaign_id, device, date, clicks) 
    select fk_campaign_id, platform, date(date), sum(clicks) from connectors_postbackclicks where date <= "2018-06-03 23:59:59"
    group by fk_campaign_id, platform , date(date) order by date(date);
    """
    fk_campaign = models.ForeignKey(CampaignTrackerMapping, db_column='fk_campaign_id', null=True, blank=True)
    date = models.DateField()
    platform = models.CharField(max_length=30)
    clicks = models.IntegerField(null=True, blank=True)
    country = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return str(self.id)


# class AffiliateIdMapping(models.Model):
#     """
#     To store mapping of Tracker and campaign (links) of affiliates.
#     """
#     fk_affiliateinfo = models.ForeignKey(Affiliateinfo, db_column='fk_affiliateinfo_id', null=True, blank=True)
#     masteraffiliate = models.IntegerField(null=True, blank=True)
#     siteid = models.ForeignKey(Site, null=True, blank=True)
#     # def __str__(self):
#     #     """ it will return username of each affiliate"""
#     #     return str(self.trackerid)
#     def __str__(self):
#         return "{0}".format(str(self.fk_affiliateinfo.username))


class Notes(models.Model):
    """
    To store text notes of each affiiliates stored in eGCS.
    """
    siteid = models.ForeignKey(Site, db_column='siteid',null=True,blank=True)
    fk_affiliateinfo = models.ForeignKey(Affiliateinfo, null=True)
    notes = models.TextField()

class Currency(models.Model):
    """Currency as of the site. """
    siteid = models.ForeignKey(Site,null=True,blank=True)
    currency = models.CharField(max_length=30)


class BannerImages(models.Model):
    """
    To store banner image path of each campaign
    """
    BANNER = 'B'
    PAGE = 'P'
    SCREENSHOTS = 'SS'
    MEDIA_TYPE = ((BANNER,"Banner"),
                    (PAGE,"Page Peel"),
                    (SCREENSHOTS,"Screenshot"),
                    )
    Language = (('English',"English"),
                    ('Finnish',"Finnish"),
                    ('German',"German"),)
    language = EnumField(choices=Language, null=True, blank=True)
    title = models.CharField(max_length=60, blank=True, null=True)
    siteid = models.ForeignKey(Site, db_column='siteid_id', null=True,blank=True)
    created = models.DateTimeField(auto_now_add=True)
    width = models.IntegerField(blank=True, null=True)
    height = models.IntegerField(blank=True, null=True)
    link = models.TextField(max_length=500, null=True, blank=True)
    banner = models.ImageField(upload_to="bannerimages/", null=True, blank=True)
    user = models.ForeignKey(User, null=True, blank=True)
    media_type = EnumField(choices=MEDIA_TYPE,verbose_name ='Media Type')
    mappingid = models.IntegerField(blank=True, null=True)

    def __unicode__(self):
        """ Return name of banner """
        return self.banner.name

    class Meta:
        verbose_name = 'Images'
        verbose_name_plural = 'Images'


class Bannerlogs(models.Model):
    """
    Save banner logs of each campaign
    """
    fk_campaign = models.ForeignKey(Campaign, null=True, blank=True)
    fk_bannerimages = models.ForeignKey(BannerImages, null=True, blank=True)
    link = models.TextField(max_length=100, null=True, blank=True)

    def __str__(self):
        """ Return name of banner """
        return self.fk_campaign.name

    def __unicode__(self):
        """ Return name of banner """
        return self.fk_campaign.name
    

class Affiliatecommission(models.Model):
    """
    Affiliate commision related info will store into this table CPA ,CPR , Revenue ,%
    """
    CRITERIA = (
            ("Registration","Registration"),
            ("FTD","FTD"),
            ("Wagering","Wagering"))
    revshare = models.BooleanField(verbose_name="RevShare Flag", default=False)
    fk_affiliateinfo = models.ForeignKey(Affiliateinfo, null=True, blank=True)
    fk_campaign = models.ForeignKey(CampaignTrackerMapping, null=True, blank=True)
    siteid = models.ForeignKey(Site, null=True, blank=True)
    country = models.ManyToManyField("userapp.Country", null=True, blank=True)
    pocdeduction = models.BooleanField(verbose_name="POC Deduction", default=True)
    account_manager = models.ForeignKey("userapp.AccountManager", db_column='accountmanager', null=True, blank=True)
    revsharepercent = models.IntegerField(verbose_name="Revshare Percent", null=True, blank=True)
    cpachoice = models.BooleanField(verbose_name="CPA Flag", default=False)
    criteriaacquisition = EnumField(choices=CRITERIA, verbose_name ='Criteria', null=True, blank=True)
    criteriavalue = models.FloatField(verbose_name ='Criteria Value', default=0.00, null=True, blank=True)
    cpacommissionvalue = models.IntegerField(verbose_name ='CPA Commision Value', null=True, blank=True)
    referalchoice = models.BooleanField(default=False, verbose_name="Referal Flag", db_column='cprchoice')
    referalcommissionvalue = models.IntegerField(db_column='cprcommissionvalue', verbose_name="Referal commission value", null=True, blank=True)
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    active = models.BooleanField(verbose_name="Active", default=True)
    currency = models.CharField(max_length=30, null=True, blank=True)
    ringfence = models.BooleanField(default=False)
    exclude_rev = models.BooleanField(default=True)
    monthToDate = models.BooleanField(default=False)
    comm_type = models.CharField(max_length=30, null=True, blank=True)
    notes = models.CharField(max_length=1000, null=True, blank=True)
    created_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        """ return unique id of each wire detail"""
        return 'Revshare-%s;CPA-%s;ReferalCommission-%s'%(str(self.revsharepercent),
                         str(self.cpacommissionvalue), str(self.referalcommissionvalue))


class PlayerAcquisitionReached(models.Model):
    CRITERIA = (
            ("Deposit","Deposit"),
            ("Wagering","Wagering"))
    playerid = models.IntegerField(null=True, blank=True)
    siteid = models.ForeignKey(Site, null=True,blank=True)
    criteria = EnumField(choices=CRITERIA, null=True, blank=True)
    baseline_met = models.BooleanField(default=True)
    date = models.DateField(null=True, blank=True)


class Deduction(models.Model):
    """Deductions as of the site. """
    siteid = models.ForeignKey(Site,null=True,blank=True)
    pocdeduction = models.BooleanField(default=True)
    adminfee = models.BooleanField(default=True)
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    country = models.ManyToManyField("userapp.Country", null=True, blank=True)
    poctvalue = models.FloatField(default=0.00, null=True, blank=True)
    adminfeevalue = models.FloatField(default=0.00, null=True, blank=True)

class TieredStructure(models.Model):
    """Tiered Structure as of the site. """
    STYPE = (
            ("Revenue","Revenue"),
            ("FTD","FTD"))
    siteid = models.ForeignKey(Site,null=True,blank=True)
    account_manager = models.ForeignKey("userapp.AccountManager", 
        db_column='accountmanager', null=True, blank=True)
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    stype = EnumField(choices=STYPE, null=True, blank=True)
    options = JSONField(null=True, blank=True)

class Hitcount(models.Model):
    
    """
    to store hit traffic coming from each affiliate campaign
    """
    fk_affiliateinfo = models.ForeignKey(Affiliateinfo, null=True, blank=True)
    fk_campaign = models.ForeignKey(Campaign, null=True, blank=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    hits = models.IntegerField(default=1)
    device_info_id = models.ForeignKey('userapp.Device_info', null=True, blank=True, db_column="device_info_id")

class AccountDetails(models.Model):
    """
    To store wire details of each affiliates
    """
    fk_affiliateinfo = models.ForeignKey(Affiliateinfo)
    accountnumber = models.CharField(max_length=250, null=True, blank=True)
    sortcode = models.CharField(max_length=250, null=True, blank=True)
    accounttype = models.CharField(max_length=250, null=True, blank=True)
    accountname = models.CharField(max_length=250, null=True, blank=True)
    iban = models.CharField(max_length=250, null=True, blank=True)
    swift = models.CharField(max_length=250, null=True, blank=True)
    bankname = models.CharField(max_length=250, null=True, blank=True)
    bankaddress = models.CharField(max_length=950, null=True, blank=True)
    beneficiaryaddress = models.CharField(max_length=950, null=True, blank=True)
    currency = models.ForeignKey(Currency, null=True, blank=True)
    paypal_username = models.CharField(max_length=200, blank=True, null=True)
    paypal_email = models.CharField(max_length=200, blank=True, null=True)
    neteller_email = models.CharField(max_length=200, blank=True, null=True)
    neteller_username = models.CharField(max_length=200, blank=True, null=True)
    neteller_accountnumber = models.CharField(max_length=200, blank=True, null=True)


    def __str__(self):
        """ return unique id of each wire detail"""
        return str(self.id)

class AffiliatePasswordResetlogs(models.Model):
    """
    affiliate password reset log model
    """
    id = models.AutoField(primary_key=True)
    affiliateid = models.ForeignKey(Affiliateinfo)
    token = models.CharField(max_length=20)
    expiretimestamp = models.DateTimeField(default=timezone.now)

class AffiliateContactUs(models.Model):
    """
    affiliate contact us form
    """
    name = models.CharField(max_length=50)
    username = models.CharField(max_length=20, null=True, blank=True)
    email = models.CharField(max_length=20, null=True, blank=True)
    subject = models.CharField(max_length=20, null=True, blank=True)
    message = models.TextField()

class WyntaSupport(models.Model):
    """
    affiliate contact us form
    """
    mobile = models.IntegerField(null=True, blank=True)
    email = models.CharField(max_length=100, null=True, blank=True)
    companyname = models.CharField(max_length=20, null=True, blank=True)


class FozilSupport(models.Model):
    """
    affiliate contact us form
    """
    mobile = models.IntegerField(null=True, blank=True)
    ipaddress = models.CharField(max_length=250, null=True, blank=True)
    timestamp = models.DateTimeField(default=timezone.now)
    email = models.CharField(max_length=100, null=True, blank=True)
    data = JSONField(null=True, blank=True)


class VcommFPAffiliateMapping(models.Model):
    """
    To store mapping of Vcomm affiliate with FP affiliate
    """
    vcommsubid = models.CharField(max_length=200, null=True, blank=True)
    vcommsubname = models.CharField(max_length=200, null=True, blank=True)
    affiliateid = models.ForeignKey(Affiliateinfo, null=True, blank=True)

class Messagelogs(models.Model):
    """
    Save Message logs of each campaign
    """
    READ = 'R'
    UNREAD = 'U'
    DELETED = 'D'
    STATUS_TYPE = ((READ,"READ"),
                    (UNREAD,"UNREAD"),
                    (DELETED,"DELETED"),
                    )
    affiliate = models.ForeignKey(Affiliateinfo, null=True, blank=True)
    account_manager = models.ForeignKey('userapp.AccountManager', null=True, blank=True)
    user = models.ForeignKey(User, null=True, blank=True)
    messagetrigger = models.ForeignKey('userapp.MessageTrigger', null=True, blank=True)
    from_email = models.CharField(max_length=200, null=True, blank=True)
    to_email = models.CharField(max_length=200, null=True, blank=True)
    timestamp = models.DateTimeField(default=timezone.now)
    content = models.TextField(blank=True, null=True)
    status = EnumField(choices=STATUS_TYPE)
    subject = models.TextField(blank=True, null=True)
    affstatus = EnumField(choices=STATUS_TYPE)

class Mailer(models.Model):
    """
    To Manage Email templates
    """
    subject = models.CharField(max_length=100)
    siteid = models.ForeignKey(Site, db_column='siteid_id', null=True,blank=True)
    content = models.TextField(blank=True, null=True)
    user = models.ForeignKey(User, null=True, blank=True)

    def __str__(self):
        """Return subject"""
        return str(self.subject)

class Mailerlogs(models.Model):
    """
    Save Mailer logs of each campaign
    """
    fk_campaign = models.ForeignKey(Campaign, null=True, blank=True)
    mailer = models.ForeignKey(Mailer, null=True, blank=True)
    link = models.TextField(max_length=100, null=True, blank=True)

class ScreenShotslogs(models.Model):
    """
    Save Screen shot logs of each campaign
    """
    fk_campaign = models.ForeignKey(Campaign, null=True, blank=True)
    screenimages = models.ForeignKey(BannerImages, null=True, blank=True)
    link = models.TextField(max_length=100, null=True, blank=True)
    
class PagePeellogs(models.Model):
    """
    Save Page peel logs of each campaign
    """
    fk_campaign = models.ForeignKey(Campaign, null=True, blank=True)
    pagepeelimage = models.ForeignKey(BannerImages, null=True, blank=True)
    link = models.TextField(max_length=100, null=True, blank=True)

class Video(models.Model):
    """
    To store banner image path of each campaign
    """
    Language = (('English',"English"),
                    ('Finnish',"Finnish"),
                    ('German',"German"),)
    language = EnumField(choices=Language,verbose_name ='Language', null=True, blank=True)
    title = models.CharField(max_length=60, blank=True, null=True)
    siteid = models.ForeignKey(Site, db_column='siteid_id', null=True,blank=True)
    created = models.DateTimeField(auto_now_add=True)
    link = models.TextField(max_length=500, null=True, blank=True)
    video = models.FileField(upload_to="videos/", null=True, blank=True)
    user = models.ForeignKey(User, null=True, blank=True)

    def __unicode__(self):
        """ Return name of banner """
        return self.video.name

class Videologs(models.Model):
    """
    Save Video logs of each campaign
    """
    fk_campaign = models.ForeignKey(Campaign, null=True, blank=True)
    fk_video = models.ForeignKey(Video, null=True, blank=True)
    link = models.TextField(max_length=100, null=True, blank=True)

    def __str__(self):
        """ Return name of banner """
        return self.fk_campaign.name

    def __unicode__(self):
        """ Return name of banner """
        return self.fk_campaign.name

class Tlinks(models.Model):
    """
    To store Text Link of each campaign
    """
    Language = (('English',"English"),
                    ('Finnish',"Finnish"),
                    ('German',"German"),)
    language = EnumField(choices=Language,verbose_name ='Language', null=True, blank=True)
    title = models.CharField(max_length=60, blank=True, null=True)
    siteid = models.ForeignKey(Site, db_column='siteid_id', null=True,blank=True)
    created = models.DateTimeField(auto_now_add=True)
    link = models.TextField(max_length=500, null=True, blank=True)
    user = models.ForeignKey(User, null=True, blank=True)

    def __unicode__(self):
        """ Return name of banner """
        return self.link

class Tlinkslogs(models.Model):
    """
    Save Text Links logs of each campaign
    """
    fk_campaign = models.ForeignKey(Campaign, null=True, blank=True)
    fk_tlink = models.ForeignKey(Tlinks, null=True, blank=True)
    link = models.TextField(max_length=100, null=True, blank=True)

    def __str__(self):
        """ Return name of banner """
        return self.fk_campaign.name

    def __unicode__(self):
        """ Return name of banner """
        return self.fk_campaign.name

class Lpages(models.Model):
    """
    To store banner image path of each campaign
    """
    Language = (('English',"English"),
                    ('Finnish',"Finnish"),
                    ('German',"German"),)
    language = EnumField(choices=Language,verbose_name ='Language', null=True, blank=True)
    title = models.CharField(max_length=60, blank=True, null=True)
    siteid = models.ForeignKey(Site, db_column='siteid_id', null=True,blank=True)
    created = models.DateTimeField(auto_now_add=True)
    link = models.TextField(max_length=500, null=True, blank=True)
    lpages = models.FileField(null=True, blank=True, upload_to="lpages/")
    user = models.ForeignKey(User, null=True, blank=True)

    def __unicode__(self):
        """ Return name of banner """
        return self.link

class LandingPagelogs(models.Model):
    """
    Save Landing Page logs of each campaign
    """
    fk_campaign = models.ForeignKey(Campaign, null=True, blank=True)
    fk_lpage = models.ForeignKey(Lpages, null=True, blank=True)
    link = models.TextField(max_length=100, null=True, blank=True)

    def __str__(self):
        """ Return name of banner """
        return self.fk_campaign.name

    def __unicode__(self):
        """ Return name of banner """
        return self.fk_campaign.name

class ChangeLog(models.Model):
    class Action:
        CHOICES = [
            (ChangeLogActions.CREATE, _('create')),
            (ChangeLogActions.UPDATE, _('update')),
            (ChangeLogActions.DELETE, _('delete'))
        ]

    timestamp = models.DateTimeField(default=timezone.now)
    action = models.CharField(max_length=32, choices=Action.CHOICES, db_index=True)
    object_id = models.PositiveIntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    serialized_data = models.TextField(null=True)
    changed_fields = models.TextField(null=True)
    content_type = models.ForeignKey(ContentType, null=True, blank=True)
    request_event = models.CharField(max_length=200, db_index=True)
    affiliateprogramme = models.ForeignKey("userapp.AffiliateProgramme", null=True, blank=True)
    user = models.ForeignKey(User, null=True, blank=True)
    fk_affiliateinfo = models.ForeignKey(Affiliateinfo, null=True, blank=True)
    ipaddress = models.CharField(max_length=32, null=True, blank=True)

    class Meta:
        ordering = ["-timestamp"]
        # indexes = [models.Index(fields=['object_id'])]

    @property
    def is_update(self):
        return self.action == ChangeLogActions.UPDATE

    @property
    def changed_field_attributes(self):
        if isinstance(self.changed_fields, str):
            changed_fields = json.loads(self.changed_fields)
        else:
            return None
        if changed_fields:
            return ', '.join(changed_fields.keys())
        else:
            return None

    @property
    def drawer_table(self):
        if self.is_update:
            return self.changed_fields
        else:
            return self.serialized_data

    @property
    def get_previous_record(self):
        return ChangeLog.objects.filter(
            object_id=self.object_id,
            content_type=self.content_type,
            timestamp__lt=self.timestamp
        ).first()

class PaymentHistory(models.Model):
    class Action:
        CHOICES = [
            ('pay', _('pay')),
            ('paid', _('paid')),
            ('carried', _('carried')),
            ('rejected', _('rejected'))
        ]
        PAYMENTS = [
            ('neteller', _('neteller')),
            ('bankwire', _('bankwire')),
            ('paypal', _('paypal'))
        ]

    status = models.CharField(max_length=32, choices=Action.CHOICES, db_index=True)
    payment_type = models.CharField(max_length=32, choices=Action.PAYMENTS, db_index=True)
    timestamp = models.DateTimeField(default=timezone.now)
    month = models.IntegerField(null=True, blank=True)
    year = models.IntegerField(null=True, blank=True)
    amount = models.FloatField(default=0.00, null=True, blank=True)
    adjusted_amount = models.FloatField(default=0.00, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    fk_affiliateinfo = models.ForeignKey(Affiliateinfo, null=True, blank=True)
    paid_status = models.BooleanField(default=False)
    carried_amount = models.FloatField(default=0.00, null=True, blank=True)
    threshold_amount = models.FloatField(default=0.00, null=True, blank=True)
    balance = models.FloatField(default=0.00, null=True, blank=True)

class Threshold(models.Model):
    account_manager = models.ForeignKey("userapp.AccountManager",
        db_column='accountmanager', null=True, blank=True)
    bankwire = models.FloatField(default=0.00, null=True, blank=True)
    neteller = models.FloatField(default=0.00, null=True, blank=True)
    paypal = models.FloatField(default=0.00, null=True, blank=True)

class Invoice(models.Model):
    logopath = models.CharField(max_length=2000, null=True, blank=True)
    pay_history = models.ForeignKey(PaymentHistory, null=True, blank=True)
    timestamp = models.DateField(default=timezone.now)