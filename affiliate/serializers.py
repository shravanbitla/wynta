import datetime
import decimal
import uuid

from django.db.models import DecimalField
from django.utils.duration import duration_string
from django.utils.encoding import is_protected_type
from django.utils.functional import Promise
from django.utils.numberformat import format as number_format


class ChangeLogSerializer:
    def __init__(self):
        self._current = {}

    def add_field(self, attr, val):
        val = self.format_value(val)
        self._current[attr] = val

    def format_value(self, val):
        if val in [None, '', []]:
            return 'N/A'
        elif isinstance(val, (datetime.datetime, datetime.date, datetime.time)):
            return val.isoformat()
        elif isinstance(val, datetime.timedelta):
            return duration_iso_string(val)
        elif isinstance(val, (decimal.Decimal, uuid.UUID, Promise)):
            return str(val)
        elif isinstance(val, list):
            return ', '.join(val)
        else:
            return val

    def serialize(self, obj, excluded_fields=None):
        # Use the concrete parent class' _meta instead of the object's _meta
        # This is to avoid local_fields problems for proxy models. Refs #17717.
        concrete_model = obj._meta.concrete_model
        pk_parent = None
        for field in concrete_model._meta.local_fields:
            if field.serialize or field is pk_parent:
                if field.rel is None:
                    if excluded_fields is None or field.attname not in excluded_fields:
                        self.handle_field(obj, field)
                else:
                    if excluded_fields is None or field.attname[:-3] not in excluded_fields:
                        self.handle_fk_field(obj, field)
        for field in concrete_model._meta.many_to_many:
            if field.serialize:
                if excluded_fields is None or field.attname not in excluded_fields:
                    self.handle_m2m_field(obj, field)

        return self._current

    def handle_field(self, obj, field):
        self.add_field(field.name, self._value_from_field(obj, field))

    def handle_fk_field(self, obj, field):
        related = getattr(obj, field.name)
        if related:
            val = related.__str__()
        else:
            val = None
        self.add_field(field.name, val)

    def handle_m2m_field(self, obj, field):
        if field.rel.through._meta.auto_created:
            self.add_field(field.name, [
                related.__str__() for related in getattr(obj, field.name).iterator()
            ])

    def _value_from_field(self, obj, field):
        value = field.value_from_object(obj)
        if value and isinstance(field, DecimalField):
            value = number_format(value, '.', field.decimal_places)
            return value.rstrip('0').rstrip('.')
        # Protected types (i.e., primitives like None, numbers and dates)
        # are passed through as is. All other values are
        # converted to string first.
        return value if is_protected_type(value) else field.value_to_string(obj)
