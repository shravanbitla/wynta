

def get_users_aff_camp_financials(aff_id, campaign_id, from_date_time, to_date_time, extra_params_dict={}):
    resp_dict = {}
    temp = {}
    obj = Affiliateinfo.objects.get(id=aff_id)
    commission = Affiliatecommission.objects.get(fk_affiliateinfo=obj)
    resp_dict['obj'] = obj
    result_list = []
    total_sum_deposit = 0
    total_sum_withdrawal = 0
    total_rake_sum = 0
    total_invested_sum = 0
    total_adustments_sum = 0
    registertype = extra_params_dict.get('registertype')
    bet_type = extra_params_dict.get('bet_type')
    if registertype and registertype == 'new users':
        if not campaign_id:
            player_objs_list = Playerinfo.objects.filter(affiliate=obj,
                                                     registeredon__gte=from_date_time,
                                                     registeredon__lte=to_date_time).order_by('-id')
        else:
            player_objs_list = Playerinfo.objects.filter(affiliate=obj,campaign__id=campaign_id,
                                                     registeredon__gte=from_date_time,
                                                     registeredon__lte=to_date_time).order_by('-id')
    elif registertype and registertype == 'old users':
        if not campaign_id:
            player_objs_list = Playerinfo.objects.filter(affiliate=obj,
                                                     registeredon__lte=from_date_time).order_by('-id')
        else:
            player_objs_list = Playerinfo.objects.filter(affiliate=obj,campaign__id=campaign_id,
                                                     registeredon__lte=from_date_time).order_by('-id')
    else:
        if not campaign_id:
            player_objs_list = Playerinfo.objects.filter(affiliate=obj).order_by('-id')
        else:
            player_objs_list = Playerinfo.objects.filter(affiliate=obj,
                                                         campaign__id=campaign_id).order_by('-id')
    add_total_list = False
    for player in player_objs_list:
        add_total_list = True
        temp = []
        sum_deposits = Order.objects.filter(Q(direction='DEPOSIT') & Q(ordertype='BANKING') 
                                    & Q(orderstate='FINISHED') & Q(playerid__affiliate=obj) &
                                    Q(timestamp__gte=from_date_time) &
                                    Q(timestamp__lte=to_date_time) & Q(playerid=player)).\
                                    aggregate(Sum('ordermoney')).values()[0]
        if sum_deposits in [None, 'None', 0]:
            sum_deposits = 0
        sum_withdrawal = Order.objects.filter(Q(direction='WITHDRAWL') & Q(ordertype='BANKING') 
                                    & Q(orderstate='FINISHED') & Q(playerid__affiliate=obj) &
                                    Q(timestamp__gte=from_date_time) &
                                    Q(timestamp__lte=to_date_time) & Q(playerid=player)).\
                                    aggregate(Sum('ordermoney')).values()[0]
        if sum_withdrawal in [None, 'None', 0]:
            sum_withdrawal = 0
        realadjustmentdeposit = Order.objects.filter(Q(playerid__affiliate=obj) & Q(direction='DEPOSIT') & 
                                                 Q(ordertype='REAL_ADJUSTMENT')&
                                                 Q(timestamp__gte=from_date_time) &
                                                 Q(timestamp__lte=to_date_time)& Q(playerid=player)).aggregate(Sum('ordermoney'))
    
        if realadjustmentdeposit and realadjustmentdeposit.get('ordermoney__sum'):
            radeposits = realadjustmentdeposit.get('ordermoney__sum')
        else:
            radeposits = 0
    
        realadjustmentwithdrawl = Order.objects.filter(Q(playerid__affiliate=obj) & Q(direction='WITHDRAWL') &
                                                        Q(ordertype='REAL_ADJUSTMENT')&
                                                     Q(timestamp__gte=from_date_time) &
                                                     Q(timestamp__lte=to_date_time)& Q(playerid=player)).aggregate(Sum('ordermoney'))
        if realadjustmentwithdrawl and realadjustmentwithdrawl.get('ordermoney__sum'):
            rawithdrawl = realadjustmentwithdrawl.get('ordermoney__sum')
        else:
            rawithdrawl = 0
        adjustments = radeposits - rawithdrawl
        u_total = sum_deposits - sum_withdrawal + adjustments
        accurals = ((u_total * commission.revsharepercent) / 100)#accurals
        
        cursor = connection.cursor()
        cursor.execute("""SELECT SUM( pin.investedamount), SUM(r.rakemoney) FROM userapp_playersinvolved pin
            INNER JOIN  userapp_game g ON g.id= pin.fkgame_id
            INNER JOIN userapp_rakelogs r ON g.gameid=r.fkgame_id
            INNER JOIN userapp_gamesetting gs ON g.gamesettingid = gs.id
            INNER JOIN userapp_stream s ON s.id=g.streamid
            WHERE pin.playerid_id=%s AND pin.investedamount IS NOT NULL
            AND s.fromchips=%s AND s.tochips=%s AND pin.timestamp >= %s AND
            pin.timestamp <= %s;""",(player.id, bet_type, bet_type,from_date_time, to_date_time,))
    
        results = cursor.fetchall()
        rake_sum = results[0][0] if results[0][0] else 0
        invested_sum = results[0][1] if results[0][1] else 0
        p_type = "None"
        if player.registeredon <= from_date_time:
            p_type = "OF" if sum_deposits == 0 else "OC"
        elif from_date_time <= player.registeredon <= to_date_time:
            p_type = "NF" if sum_deposits == 0 else "NC"
        temp = [player.username, player.id, p_type, sum_deposits, sum_withdrawal, rake_sum, invested_sum, accurals]
        
        total_sum_deposit += sum_deposits
        total_sum_withdrawal += sum_withdrawal
        total_rake_sum += rake_sum
        total_invested_sum += invested_sum
        total_adustments_sum += accurals
        result_list.append(temp)
    
    if add_total_list:
    #adding totals list
        result_list.append(["TOTAL","","",total_sum_deposit,
                        total_sum_withdrawal,
                       total_rake_sum,
                        total_invested_sum, 
                        total_adustments_sum])
    resp_dict['users_data_list'] = result_list
    resp_dict['revsharepercent'] = commission.revsharepercent
    return resp_dict

def get_affiliate_data_for_give_dates(obj, commision, from_date_time, to_date_time):
    temp = {}
    temp['username'] = obj.username
    temp['registration']= Playerinfo.objects.filter(affiliate=obj,
                                                    registeredon__gte=from_date_time,
                                                    registeredon__lte=to_date_time).count()
                                                    
    temp['affiliate_logins'] = Loginon.objects.filter(affiliateid=obj, loginok=True,
                                                                   logintime__gte=from_date_time.date(),
                                                                   logintime__lte=to_date_time.date()).count()
    temp['affiliate_uniquelogins'] = Loginon.objects.filter(affiliateid=obj, loginok=True,uniquelogin=True,
                                                                   logintime__gte=from_date_time.date(),
                                                                   logintime__lte=to_date_time.date()).count()
    affhit = Hitcount.objects.filter(fk_affiliateinfo=obj,
                                     timestamp__gte=from_date_time,
                                      timestamp__lte=to_date_time).aggregate(Sum('hits'))
    if affhit and affhit.get('hits__sum'):
        temp['hitcount'] = affhit.get('hits__sum')
    else:
        temp['hitcount'] = 0
    temp['firsttimeobj'] = Order.objects.filter(Q(direction='DEPOSIT') & Q(firstdeposit=True) & 
                                        Q(orderstate='FINISHED')& Q(playerid__affiliate=obj) &
                                        Q(timestamp__gte=from_date_time) &
                                           Q(timestamp__lte=to_date_time)).count()
    deposits = Order.objects.filter(Q(direction='DEPOSIT') & Q(ordertype='BANKING') 
                                    & Q(orderstate='FINISHED') & Q(playerid__affiliate=obj) &
                                    Q(timestamp__gte=from_date_time) &
                                    Q(timestamp__lte=to_date_time)).aggregate(Sum('ordermoney'))
    if deposits and deposits.get('ordermoney__sum'):
        temp['sumdeposit'] = deposits.get('ordermoney__sum')
    else:
        temp['sumdeposit'] = 0
    withdrawl = Order.objects.filter(Q(direction='WITHDRAWL') & Q(orderstate='FINISHED') & Q(playerid__affiliate=obj)&
                                    Q(timestamp__gte=from_date_time) &
                                    Q(timestamp__lte=to_date_time)).aggregate(Sum('ordermoney'))
    if withdrawl and withdrawl.get('ordermoney__sum'):
        temp['sumwithdrawl'] = withdrawl.get('ordermoney__sum')
    else:
        temp['sumwithdrawl'] = 0
    commission = Affiliatecommission.objects.get(fk_affiliateinfo=obj)
    temp['cpa'] = (temp['firsttimeobj']) * (commission.cpacommissionvalue)
    temp['cpr'] = (temp['registration']) * (commission.cprcommissionvalue)
    realadjustmentdeposit = Order.objects.filter(Q(playerid__affiliate=obj) & Q(direction='DEPOSIT') & 
                                                 Q(ordertype='REAL_ADJUSTMENT')&
                                                 Q(timestamp__gte=from_date_time) &
                                                 Q(timestamp__lte=to_date_time)).aggregate(Sum('ordermoney'))
    
    if realadjustmentdeposit and realadjustmentdeposit.get('ordermoney__sum'):
        radeposits = realadjustmentdeposit.get('ordermoney__sum')
    else:
        radeposits = 0

    realadjustmentwithdrawl = Order.objects.filter(Q(playerid__affiliate=obj) & Q(direction='WITHDRAWL') &
                                                    Q(ordertype='REAL_ADJUSTMENT')&
                                                 Q(timestamp__gte=from_date_time) &
                                                 Q(timestamp__lte=to_date_time)).aggregate(Sum('ordermoney'))
    if realadjustmentwithdrawl and realadjustmentwithdrawl.get('ordermoney__sum'):
        rawithdrawl = realadjustmentwithdrawl.get('ordermoney__sum')
    else:
        rawithdrawl = 0
    temp['adjustments'] = radeposits - rawithdrawl
    temp['total'] = temp['sumdeposit'] - temp['sumwithdrawl'] + temp['adjustments']
    #temp['net'] = (temp['total'] * commission.revsharepercent)/100
    temp['net'] = ((temp['total'] * commission.revsharepercent)/100)+temp['cpa']+temp['cpr']
    #temp['revsharepercent'] = commission.revsharepercent
    return temp
    
def get_campaign_data_for_given_dates(obj, commission, from_date_time, to_date_time, date_wise=False):
    resp_list = []
    links = Campaign.objects.filter(fk_affiliateinfo=obj)
    #commission = Affiliatecommission.objects.get(fk_affiliateinfo=obj)
    for i in links:
        camp_dict = {}
        if date_wise:
            camp_dict['DateTime'] = from_date_time
        camp_dict['datepicker1'] = from_date_time.strftime("%m/%d/%Y")
        camp_dict['datepicker2'] = to_date_time.strftime("%m/%d/%Y")
        camp_dict['id'] = "%s-%s" %(i.id,i.name)
        
        camp_dict['campaignregistration'] = Playerinfo.objects.filter(campaign=i,
                                                                       registeredon__gte=from_date_time,
                                                                       registeredon__lte=to_date_time).count()
        
        camp_dict['campaign_logins'] = Loginon.objects.filter(campaignid=i, loginok=True,
                                                                   logintime__gte=from_date_time.date(),
                                                                   logintime__lte=to_date_time.date()).count()
        camp_dict['campaign_uniquelogins'] = Loginon.objects.filter(campaignid=i, loginok=True,uniquelogin=True,
                                                                   logintime__gte=from_date_time.date(),
                                                                   logintime__lte=to_date_time.date()).count()
        
                                                                   
        hit_count_obj =  Hitcount.objects.filter(fk_campaign=i,
                                                 timestamp__gte=from_date_time,
                                                 timestamp__lte=to_date_time).aggregate(Sum('hits'))
        if hit_count_obj and hit_count_obj.get('hits__sum'):
            camp_dict['campaignhitcount'] = hit_count_obj['hits__sum']
        else:
            camp_dict['campaignhitcount'] = 0

        camp_dict['campaignsingletime'] = Order.objects.filter(Q(direction='DEPOSIT') & Q(firstdeposit=True) & 
                                                    Q(orderstate='FINISHED')& Q(playerid__campaign=i)&
                                                    Q(timestamp__gte=from_date_time) &
                                                 Q(timestamp__lte=to_date_time)).count()
        
        
        camp_dict['campaigncpalist'] = camp_dict['campaignsingletime'] * (commission.cpacommissionvalue)
        camp_dict['campaigncprlist'] = camp_dict['campaignregistration'] * (commission.cprcommissionvalue)
        
        
        campaigndeposits_obj = Order.objects.filter(Q(direction='DEPOSIT') & Q(ordertype='BANKING') & 
                                                Q(orderstate='FINISHED') &
                                                 Q(playerid__campaign=i) & Q(timestamp__gte=from_date_time) &
                                                 Q(timestamp__lte=to_date_time)).aggregate(Sum('ordermoney'))
        if campaigndeposits_obj and campaigndeposits_obj.get('ordermoney__sum'):
            camp_dict['campaignsumdeposit'] = campaigndeposits_obj['ordermoney__sum']
        else:
            camp_dict['campaignsumdeposit'] = 0
        campaignwithdrawl_obj = Order.objects.filter(Q(direction='WITHDRAWL') & 
                                                 Q(orderstate='FINISHED') & 
                                                 Q(playerid__campaign=i) & Q(timestamp__gte=from_date_time) &
                                                 Q(timestamp__lte=to_date_time)).aggregate(Sum('ordermoney'))
        if campaignwithdrawl_obj and campaignwithdrawl_obj.get('ordermoney__sum'):
            camp_dict['campaignsumwithdrawl'] = campaignwithdrawl_obj['ordermoney__sum']
        else:
            camp_dict['campaignsumwithdrawl'] = 0
        
        '''real adjustment for camapaign '''
        campaignradeposits_obj = Order.objects.filter(Q(playerid__campaign=i) & Q(direction='DEPOSIT')
                                                & Q(ordertype='REAL_ADJUSTMENT') & Q(timestamp__gte=from_date_time) &
                                                 Q(timestamp__lte=to_date_time)).aggregate(Sum('ordermoney'))

        if campaignradeposits_obj and campaignradeposits_obj.get('ordermoney__sum'):
            campaignradeposits = campaignradeposits_obj['ordermoney__sum']
        else:
            campaignradeposits = 0
        
        campaignrawithdrawls_obj = Order.objects.filter(Q(playerid__campaign=i) & Q(direction='WITHDRAWL') 
                                                    & Q(ordertype='REAL_ADJUSTMENT') & Q(timestamp__gte=from_date_time) &
                                                 Q(timestamp__lte=to_date_time)).aggregate(Sum('ordermoney'))
        if campaignrawithdrawls_obj and campaignrawithdrawls_obj.get('ordermoney__sum'):
            campaignrawithdrawls = campaignrawithdrawls_obj['ordermoney__sum']
        else:
            campaignrawithdrawls = 0
        camp_dict['campaignrealadjustment'] = campaignradeposits - campaignrawithdrawls
        
        camp_dict['campaigntotal'] = camp_dict['campaignsumdeposit']-camp_dict['campaignsumwithdrawl'] +\
                         camp_dict['campaignrealadjustment']#revenue
        #camp_dict['campaignnet'] = camp_dict['campaigntotal'] * (commission.revsharepercent / 100)
        camp_dict['campaignnet'] = ((camp_dict['campaigntotal'] * commission.revsharepercent) / 100)+\
                                    camp_dict['campaigncpalist']+camp_dict['campaigncprlist'] #accurals
        #camp_dict['revsharepercent'] = commission.revsharepercent
        total_flag = camp_dict.get("campaignregistration")+camp_dict.get("campaignhitcount")+ camp_dict.get("campaignsingletime")+\
                    camp_dict.get("campaignsumwithdrawl")+camp_dict.get("campaignsumwithdrawl")+camp_dict.get("campaignrealadjustment")+\
                    camp_dict.get("campaigntotal")+camp_dict.get("campaignnet")
        
        if total_flag != 0 and date_wise:
            resp_list.append(camp_dict)
        elif not date_wise:
            resp_list.append(camp_dict) 
    return resp_list

def get_affiliate_financial_data(aff_id, from_date_time, to_date_time, date_wise=False):
    resp_dict = {}
    obj = Affiliateinfo.objects.get(id=aff_id)
    commission = Affiliatecommission.objects.get(fk_affiliateinfo=obj)
    resp_dict['obj'] = obj
    if date_wise:
        date_wise_resp_list = []
        total_reg_count = 0
        total_clicks_count = 0
        total_acquitions_count = 0
        total_deposits_amount = 0
        total_withdrawl_amount = 0
        total_adjustments_amount = 0
        total_revenue_amount = 0
        total_accurals_amount = 0
        total_campaign_logins = 0
        total_campaign_uniquelogins = 0
        delta = to_date_time.date() - from_date_time.date()
        for i in range(delta.days + 1):
            fromdate =  datetime.combine(from_date_time.date() + timedelta (days=i), time(0,0,0))
            todate = datetime.combine(fromdate, time(23,59,59))
            g = get_campaign_data_for_given_dates(obj,commission, fromdate, todate, date_wise)
            for j in g:
                total_reg_count += j.get("campaignregistration")
                total_clicks_count += j.get("campaignhitcount")
                total_acquitions_count += j.get("campaignsingletime")
                total_deposits_amount += j.get("campaignsumdeposit")
                total_withdrawl_amount += j.get("campaignsumwithdrawl")
                total_adjustments_amount += j.get("campaignrealadjustment")
                total_revenue_amount += j.get("campaigntotal")
                total_accurals_amount += j.get("campaignnet")
                total_campaign_logins += j.get("campaign_logins")
                total_campaign_uniquelogins += j.get("campaign_uniquelogins")
            date_wise_resp_list.extend(g)
        totals_dict = {'campaignregistration':total_reg_count,'campaignhitcount':total_clicks_count,
                       'campaignsingletime':total_acquitions_count,'campaignsumdeposit':total_deposits_amount,
                       'campaignsumwithdrawl':total_withdrawl_amount,'campaignrealadjustment':total_adjustments_amount,
                       'campaigntotal':total_revenue_amount,'campaignnet':total_accurals_amount,'DateTime':"TOTAL",
                       'campaign_logins':total_campaign_logins,'campaign_uniquelogins':total_campaign_uniquelogins}
        date_wise_resp_list.append(totals_dict)
        resp_dict['date_wise_data'] = date_wise_resp_list
    resp_dict['revsharepercent'] = commission.revsharepercent
    return resp_dict

def affiliate_user_reports(request):
    affID = request.session.get('affid')
    try:
        obj = Affiliateinfo.objects.get(id=affID)
    except:
        return HttpResponseRedirect('/affiliate/')
    resp_dict = {}
    """
    if not hasattr(request, 'session'):
        return HttpResponseRedirect(reverse("affiliate"))
    affID = request.session.get("affid")
    if not affID:
        return HttpResponseRedirect(reverse("affiliate"))
    """
    campaign_list = Campaign.objects.filter(fk_affiliateinfo__id=affID)
    if request.method == "GET":
        dates_dict = get_fromdate_todate_for_django_query_filter("Last 30 Days", None, None)
        to_date_time = dates_dict.get('todate')
        from_date_time = dates_dict.get('fromdate')
        registertype = "new users"
        campaign_id = None
        date_type = "Last 30 Days"
        bet_type = "CASH"
    else:
        date_type = request.POST.get('date')
        bet_type = request.POST.get("bet_type")
        newusers = request.POST.get('newusers')
        oldusers = request.POST.get('oldusers')
        registertype = None
        if newusers == "on" and oldusers == "on":
            registertype = "all users"
        if not registertype and newusers == "on":
            registertype = "new users"
        if not registertype and oldusers == "on":
            registertype = "old users"
        campaign_id = request.POST.get('campaign_id')
        if date_type and date_type.lower() != "all":
            fromdate = request.POST.get('datepicker1')
            todate = request.POST.get('datepicker2')
            dates_dict = get_fromdate_todate_for_django_query_filter(date_type, fromdate, todate)
            to_date_time = dates_dict.get('todate')
            from_date_time = dates_dict.get('fromdate')
        else:
            dates_dict = get_site_player_first_register_timestamp()
            to_date_time = dates_dict.get('todate')
            from_date_time = dates_dict.get('fromdate')
            
    registertype = registertype.lower() if registertype else None
    campaign_id = campaign_id if campaign_id else campaign_list[0].id
    users_data_dict = get_users_aff_camp_financials(affID, campaign_id, from_date_time, to_date_time,
                                                    {'registertype':registertype,'bet_type':bet_type})
    resp_dict['users_data_list'] = users_data_dict.get("users_data_list")
    resp_dict['affiliate_id'] = affID
    resp_dict['obj'] = users_data_dict.get("obj")
    resp_dict['registertype'] = registertype.upper()
    resp_dict['from_date'] = from_date_time
    resp_dict['to_date'] = to_date_time
    resp_dict['campaignID'] = campaign_id
    resp_dict['campaign_list'] = campaign_list
    resp_dict['custom'] = date_type
    resp_dict['bet_type'] = bet_type
    if date_type.lower() == "custom":
        resp_dict['from_ui_date'] = fromdate
        resp_dict['to_ui_date'] = todate
    resp_dict['revsharepercent'] = users_data_dict.get("revsharepercent")
    return render_to_response('affiliates/affiliate_user_financials.html',
                              resp_dict, context_instance=RequestContext(request))

def affiliate_campaign_financial(request):
    """ player site affiliate financial display """
    
    aff_id = request.session.get('affid')
    try:
        obj = Affiliateinfo.objects.get(id=aff_id)
    except:
        return HttpResponseRedirect('/affiliate/')
    """
    if not hasattr(request, 'session'):
        return HttpResponseRedirect(reverse("affiliate"))
    aff_id = request.session.get("affid")
    if not aff_id:
        return HttpResponseRedirect(reverse("affiliate"))
    """
    obj = Affiliateinfo.objects.get(id=aff_id)
    if request.method == "GET":
        dates_dict = get_fromdate_todate_for_django_query_filter(None, None, None)
        to_date_time = dates_dict.get('todate')
        from_date_time = dates_dict.get('fromdate')
        date_type = "Today"
    else:
        date_type = request.POST.get('date')
        if date_type and date_type.lower() != "all":
            fromdate = request.POST.get('datepicker1')
            todate = request.POST.get('datepicker2')
            dates_dict = get_fromdate_todate_for_django_query_filter(date_type, fromdate, todate)
            to_date_time = dates_dict.get('todate')
            from_date_time = dates_dict.get('fromdate')
        else:
            dates_dict = get_site_player_first_register_timestamp()
            to_date_time = dates_dict.get('todate')
            from_date_time = dates_dict.get('fromdate')
    player_objs_list = Playerinfo.objects.filter(affiliate=obj,
                                                 registeredon__gte=from_date_time,
                                                 registeredon__lte=to_date_time).order_by('-id').\
                                                 values('id','username','registeredon','verified','creationip','state__statename')
    firsttime_depositors_list = Order.objects.filter(Q(direction='DEPOSIT') & Q(firstdeposit=True) & Q(orderstate= 'FINISHED')&
                                         Q(playerid__affiliate=obj) &
                                           Q(timestamp__gte=from_date_time) &
                                           Q(timestamp__lte=to_date_time)).order_by('-id').values('playerid_id','playerid__username',
                                                                           'playerid__registeredon','playerid__state__statename',
                                                                           'playerid__deposits')
    resp_dict = {}
    commission = Affiliatecommission.objects.get(fk_affiliateinfo=obj)
    resp_dict['affiliate_info'] = [get_affiliate_data_for_give_dates(obj,commission, from_date_time,to_date_time)]
    resp_dict['campaign_info'] = get_campaign_data_for_given_dates(obj, commission, from_date_time, to_date_time)
    resp_dict['affiliate_id'] = id
    resp_dict['player_objs_list'] = player_objs_list
    resp_dict['firsttime_depositors_list'] = firsttime_depositors_list
    resp_dict['obj'] = obj
    #resp_dict['date_wise_data'] = affiliate_data_dict.get("date_wise_data")
    resp_dict['custom'] = date_type
    resp_dict['from_date'] = from_date_time
    resp_dict['to_date'] = to_date_time
    resp_dict['revsharepercent'] = commission.revsharepercent
    return render_to_response('affiliates/affiliate_campaign_financial.html',
                              resp_dict, context_instance=RequestContext(request))

def financialhome(request, id):
    aff_id = request.session.get('affid')
    try:
        obj = Affiliateinfo.objects.get(id=aff_id)
    except:
        return HttpResponseRedirect('/affiliate/')
    if request.method == "GET":
        dates_dict = get_fromdate_todate_for_django_query_filter(None, None, None)
        to_date_time = dates_dict.get('todate')
        from_date_time = dates_dict.get('fromdate')
        date_type = "Today"
    else:
        date_type = request.POST.get('date')
        if date_type and date_type.lower() != "all":
            fromdate = request.POST.get('datepicker1')
            todate = request.POST.get('datepicker2')
            dates_dict = get_fromdate_todate_for_django_query_filter(date_type, fromdate, todate)
            to_date_time = dates_dict.get('todate')
            from_date_time = dates_dict.get('fromdate')
        else:
            dates_dict = get_site_player_first_register_timestamp()
            to_date_time = dates_dict.get('todate')
            from_date_time = dates_dict.get('fromdate')
    resp_dict = {}
    affiliate_data_dict = get_affiliate_financial_data(obj.id, from_date_time, to_date_time, True)
    #resp_dict['affiliate_info'] = affiliate_data_dict.get("affiliate_info")
    #resp_dict['campaign_info'] = affiliate_data_dict.get("campaign_info")
    resp_dict['affiliate_id'] = id
    #resp_dict['player_objs_list'] = player_objs_list
    #resp_dict['firsttime_depositors_list'] = firsttime_depositors_list
    resp_dict['obj'] = obj
    resp_dict['date_wise_data'] = affiliate_data_dict.get("date_wise_data")
    resp_dict['custom'] = date_type
    resp_dict['from_date'] = from_date_time
    resp_dict['to_date'] = to_date_time
    resp_dict['revsharepercent'] = affiliate_data_dict.get('revsharepercent')
    return render_to_response('affiliates/affiliate-financials.html',
                              resp_dict, context_instance=RequestContext(request))

def affiliatefinancial(request, id):
    if request.method == "GET":
        dates_dict = get_fromdate_todate_for_django_query_filter(None, None, None)
        to_date_time = dates_dict.get('todate')
        from_date_time = dates_dict.get('fromdate')
        date_type = "Today"
    else:
        date_type = request.POST.get('date')
        if date_type and date_type.lower() != "all":
            fromdate = request.POST.get('datepicker1')
            todate = request.POST.get('datepicker2')
            dates_dict = get_fromdate_todate_for_django_query_filter(date_type, fromdate, todate)
            to_date_time = dates_dict.get('todate')
            from_date_time = dates_dict.get('fromdate')
        else:
            dates_dict = get_site_player_first_register_timestamp()
            to_date_time = dates_dict.get('todate')
            from_date_time = dates_dict.get('fromdate')
    player_objs_list = Playerinfo.objects.filter(affiliate__id=id,
                                                 registeredon__gte=from_date_time,
                                                 registeredon__lte=to_date_time).order_by('-id').\
                                                 values('id','username','registeredon','verified','creationip','state__statename')
    firsttime_depositors_list = Order.objects.filter(Q(direction='DEPOSIT') & Q(firstdeposit=True) & Q(orderstate= 'FINISHED')&
                                         Q(playerid__affiliate__id=id) &
                                           Q(timestamp__gte=from_date_time) &
                                           Q(timestamp__lte=to_date_time)).order_by('-id').values('playerid_id','playerid__username',
                                                                           'playerid__registeredon','playerid__state__statename',
                                                                           'playerid__deposits')
    resp_dict = {}
    affiliate_data_dict = get_affiliate_financial_data(id, from_date_time, to_date_time, True)
    obj = Affiliateinfo.objects.get(id=id)
    resp_dict['affiliate_info'] = [get_affiliate_data_for_give_dates(obj,commission, from_date_time,to_date_time)]
    resp_dict['campaign_info'] = get_campaign_data_for_given_dates(obj, commission, from_date_time, to_date_time)
    #resp_dict['affiliate_info'] = affiliate_data_dict.get("affiliate_info")
    #resp_dict['campaign_info'] = affiliate_data_dict.get("campaign_info")
    resp_dict['affiliate_id'] = id
    resp_dict['obj'] = affiliate_data_dict.get("obj")
    resp_dict['player_objs_list'] = player_objs_list
    resp_dict['firsttime_depositors_list'] = firsttime_depositors_list
    resp_dict['date_wise_data'] = affiliate_data_dict.get("date_wise_data")
    resp_dict['custom'] = date_type
    if date_type.lower() == 'Custom':
        resp_dict['from_ui_date'] = fromdate
        resp_dict['to_ui_date'] = todate
    return render_to_response('affiliates/admin/affiliate-financials.html', resp_dict,
                              context_instance=RequestContext(request))

def get_user_activities_response(request, aff_obj):
    resp_dict = {}
    resp_dict['obj'] = aff_obj
    resp_dict["affiliate_id"] = aff_obj.id
    resp_dict["campaign_list"] = Campaign.objects.filter(fk_affiliateinfo=aff_obj)
    if request.method == "GET": 
        username = request.GET.get('username')
        custom = request.GET.get('custom')
        from_ui_date, to_ui_date = None, None
        if custom and custom.lower()=="custom":
            from_ui_date = request.GET.get('from_ui_date')
            to_ui_date = request.GET.get('to_ui_date')
            print "asdddddddd"
            date_type = custom
        else:
            date_type = "Today"
        dates_dict = get_fromdate_todate_for_django_query_filter(date_type, from_ui_date, to_ui_date)
        print "dates_dict",dates_dict
        to_date_time = dates_dict.get('todate')
        from_date_time = dates_dict.get('fromdate')
        all = "off"
        deposits, withdrawals, adjustments, bonuscode, deductions, rake = "on","on","off", None,"on","on"
        all_flag = False
    else:
        username = request.POST.get('username')
        deposits = request.POST.get('deposits')
        withdrawals = request.POST.get('withdrawals')
        deductions = request.POST.get('deductions')
        rake = request.POST.get('rakes')
        adjustments = request.POST.get('adjustments')
        accurals = request.POST.get('accurals')
        bonuscode = request.POST.get('bonuscode')
        #campaign_id = request.POST.get('campaign_id')
        all = request.POST.get('all')
        date_type = request.POST.get('date')
        if date_type and date_type.lower() != "all":
            fromdate = request.POST.get('datepicker1')
            todate = request.POST.get('datepicker2')
            dates_dict = get_fromdate_todate_for_django_query_filter(date_type, fromdate, todate)
            to_date_time = dates_dict.get('todate')
            from_date_time = dates_dict.get('fromdate')
        else:
            dates_dict = get_site_player_first_register_timestamp()
            to_date_time = dates_dict.get('todate')
            from_date_time = dates_dict.get('fromdate')

    direction_list = []
    if not username or username == "":
        userID = ""
        today_date = datetime.now()
        player_ids_list = []
        player_objs_list = []
        ff = Order.objects.filter(timestamp__day = today_date.day,playerid__affiliate=aff_obj,
                             timestamp__month = datetime.now().month, 
                             timestamp__year=datetime.now().year,orderstate="FINISHED").\
                             exclude(modeofdeposit='MANUAL_ADMIN').order_by('-id')
        for l in ff:
            if l.playerid_id not in  player_ids_list:
                player_ids_list.append(l.playerid_id)
                player_objs_list.append(l.playerid)        
        re = Reconcilelogs.objects.filter(timestamp__day = today_date.day,playerid__affiliate=aff_obj,
                             timestamp__month = datetime.now().month, 
                             timestamp__year=datetime.now().year,transaction_type="WAGERING",
                             amounttype="CASH").order_by('-id')
        for k in re:
            if k.playerid_id not in  player_ids_list:
                player_ids_list.append(k.playerid_id)
                player_objs_list.append(k.playerid)
    else:
        player_objs_list = Playerinfo.objects.filter(username=username)
        if len(player_objs_list) == 0:
            resp_dict["error_message"] = "Player Does not Exist in database."
            return resp_dict
        userID = player_objs_list[0].id
    try:
        commission = Affiliatecommission.objects.get(fk_affiliateinfo=aff_obj)
    except Affiliatecommission.DoesNotExist:
        commission = Affiliatecommission()
        commission.revsharepercent = 0
    resp_dict['revsharepercent'] = commission.revsharepercent
    
    all_flag, deposits_flag, withdrawals_flag, adjustments_flag, deductions_flag, rakes_flag, accurals_flag = False,\
                    False, False,False,True,False,True
    if all and str(all) == "on":
        deposits, withdrawals, adjustments, bonuscode, deductions, rake = "on","on","on", None,"on","on"
        all_flag = True
    if deposits and str(deposits) == 'on':
        direction_list.append("DEPOSIT")
        deposits_flag = True
    if withdrawals and str(withdrawals) == "on":
        direction_list.append("WITHDRAWL")
        withdrawals_flag = True
        
    ordertype = ""
    if adjustments and str(adjustments) == "on":
        if not direction_list:
            direction_list = ["DEPOSIT", "WITHDRAWL"]
        ordertype = "REAL_ADJUSTMENT"
        adjustments_flag = True
    resp_list = []
    for player_obj in player_objs_list:
        order_objs = []
        if direction_list: 
            order_objs = Order.objects.filter(direction__in=direction_list, playerid=player_obj,orderstate="FINISHED",
                                              timestamp__lte=to_date_time,timestamp__gte=from_date_time).order_by('-id')
        if bonuscode and str(bonuscode)=="None":
            if order_objs:
                order_objs = order_objs.filter(bonuscode=bonuscode)
            else:
                order_objs = Order.objects.filter(bonuscode=bonuscode, playerid=player_obj,orderstate="FINISHED",
                                                  timestamp__lte=to_date_time,timestamp__gte=from_date_time).order_by('-id')
        
        if ordertype:
            order_objs = order_objs.filter(ordertype=ordertype)
        
        if deductions and str(deductions) == "on":
            if not direction_list:
                order_objs = Order.objects.filter(fee__isnull=False, 
                                                  playerid=player_obj,orderstate="FINISHED",
                                                  timestamp__lte=to_date_time,timestamp__gte=from_date_time).order_by('-id')
        games_rake_list = []
        if rake and str(rake)=="on":
            rakes_flag = True
            cursor = connection.cursor()
            to_date_time_str = to_date_time.strftime("%Y-%m-%d %H:%M:%S")
            from_date_time_str = from_date_time.strftime("%Y-%m-%d %H:%M:%S")
            cursor.execute("""SELECT  g.tableid, g.gameid, pin.timestamp, pin.investedamount, 
                r.rakemoney, gs.rake, s.id, gs.id, gs.bet
                FROM userapp_playersinvolved pin
                INNER JOIN  userapp_game g ON g.id= pin.fkgame_id
                INNER JOIN userapp_rakelogs r ON g.gameid=r.fkgame_id
                INNER JOIN userapp_gamesetting gs ON g.gamesettingid = gs.id
                INNER JOIN userapp_stream s ON s.id=g.streamid
                WHERE pin.playerid_id=%s AND pin.investedamount IS NOT NULL
                AND s.fromchips=%s AND s.tochips=%s AND
                pin.timestamp BETWEEN %s AND %s ORDER BY pin.timestamp DESC;""",(player_obj.id, "CASH", "CASH", from_date_time_str, to_date_time_str))
            results = cursor.fetchall()
            for j in results:
                temp = []
                temp.append("TableID - %s"%(j[0]))
                temp.append(j[2])
                temp.append("Gameplay")
                temp.append(j[3])
                temp.append("%s"%j[5]+"%")
                temp.append(j[3] * commission.revsharepercent / 100)
                temp.append("StreamID : %s  GamesettingID : %s  Bet : %s"%(j[6], j[7], j[8]))
                games_rake_list.append(temp)
        for i in order_objs:
            temp = []
            temp.append("%s-%s"%("orderID",i.id))
            temp.append(i.timestamp)
            if ordertype:
                temp.append("%s - %s"%(i.direction, ordertype))
            else:
                temp.append(i.direction)
            temp.append(i.ordermoney)
            temp.append(i.fee)
            if i.direction == "DEPOSIT":
                temp.append(i.ordermoney * commission.revsharepercent / 100)
            else:
                temp.append("NA")
            temp.append("")
            resp_list.append(temp)
        if games_rake_list:
            resp_list.extend(games_rake_list)
    timestamps_list = [i[1] for i in resp_list]
    sorted_timestamps_list = sorted(range(len(timestamps_list)), key=lambda k: timestamps_list[k], reverse=True)
    sorted_resp_list = [resp_list[k] for k in sorted_timestamps_list]
    resp_dict["acitivites_list"] = sorted_resp_list
    resp_dict['campaignID'] = player_obj.campaign.name if username else "N.A"
    resp_dict['username'] = username if username else "N.A"
    resp_dict['userid'] = userID
    resp_dict['from_date'] = from_date_time
    resp_dict['to_date'] = to_date_time
    resp_dict['all_flag'] = all_flag
    resp_dict['deposits_flag'] = deposits_flag
    resp_dict['withdrawals_flag'] = withdrawals_flag
    resp_dict['adjustments_flag'] = adjustments_flag
    resp_dict['deductions_flag'] = deductions_flag
    resp_dict['rakes_flag'] = rakes_flag
    resp_dict['accurals_flag'] = accurals_flag
    resp_dict['custom'] = date_type
    return resp_dict


@check_ip_required
@staff_member_required
def admin_affiliate_user_activities(request, id):
    aff_obj = Affiliateinfo.objects.get(id=id)
    response_dict = get_user_activities_response(request, aff_obj)
    return render_to_response('affiliates/admin/affiliate_users_activities.html', response_dict,
                              context_instance=RequestContext(request))

def affiliate_user_activities(request):
    aff_id = request.session.get("affid") 
    try:
        aff_obj = Affiliateinfo.objects.get(id=aff_id)
    except Affiliateinfo.DoesNotExist:
        return HttpResponseRedirect("/affiliate/")
    response_dict = get_user_activities_response(request, aff_obj)
    response_dict["obj"] = aff_obj
    return render_to_response('affiliates/affiliate_users_activities.html', response_dict,
                              context_instance=RequestContext(request))

@check_ip_required
@staff_member_required
def admin_affiliate_user_reports(request, id):
    resp_dict = {}
    campaign_list = Campaign.objects.filter(fk_affiliateinfo__id=id)
    if request.method == "GET":
        dates_dict = get_fromdate_todate_for_django_query_filter(None, None, None)
        to_date_time = dates_dict.get('todate')
        from_date_time = dates_dict.get('fromdate')
        registertype = "new users"
        campaign_id = None
        date_type = "Today"
    else:
        date_type = request.POST.get('date')
        registertype = request.POST.get('registertype')
        newusers = request.POST.get('newusers')
        oldusers = request.POST.get('oldusers')
        registertype = None
        if newusers == "on" and oldusers == "on":
            registertype = "all users"
        if not registertype and newusers == "on":
            registertype = "new users"
        if not registertype and oldusers == "on":
            registertype = "old users"
        campaign_id = request.POST.get('campaign_id')
        if date_type and date_type.lower() != "all":
            fromdate = request.POST.get('datepicker1')
            todate = request.POST.get('datepicker2')
            dates_dict = get_fromdate_todate_for_django_query_filter(date_type, fromdate, todate)
            to_date_time = dates_dict.get('todate')
            from_date_time = dates_dict.get('fromdate')
        else:
            dates_dict = get_site_player_first_register_timestamp()
            to_date_time = dates_dict.get('todate')
            from_date_time = dates_dict.get('fromdate')

    registertype = registertype.lower() if registertype else None
    campaign_id = campaign_id if campaign_id else campaign_list[0].id
    users_data_dict = get_users_aff_camp_financials(id, campaign_id, from_date_time, to_date_time,
                                                    {'registertype': registertype})
    resp_dict['users_data_list'] = users_data_dict.get("users_data_list")
    resp_dict['affiliate_id'] = id
    resp_dict['obj'] = users_data_dict.get("obj")
    resp_dict['registertype'] = registertype
    resp_dict['from_date'] = from_date_time
    resp_dict['to_date'] = to_date_time
    resp_dict['campaignID'] = campaign_id
    resp_dict['campaign_list'] = campaign_list
    resp_dict['custom'] = date_type
    resp_dict['revsharepercent'] = users_data_dict.get("revsharepercent")
    return render_to_response('affiliates/admin/affiliate_users_financial.html', resp_dict,
                              context_instance=RequestContext(request))

def get_affiliate_logins_or_uniquelogins(request, affid, uniquelogins=False):
    obj = Affiliateinfo.objects.get(id=affid)
    resp_dict = {}
    if request.method == "GET":
        campaignID = request.GET.get('campaignid')
        date_type = request.GET.get('date')
        from_date = request.GET.get('datepicker1')
        todate = request.GET.get('datepicker2')
        if not date_type:
            date_type = "Today"
            from_date, todate = None,None
        dates_dict = get_fromdate_todate_for_django_query_filter(date_type, from_date, todate)
        to_date_time = dates_dict.get('todate')
        from_date_time = dates_dict.get('fromdate')
        
    else:
        campaignID = request.POST.get('campaignid')
        date_type = request.POST.get('date')
        if date_type and date_type.lower() != "all":
            fromdate = request.POST.get('datepicker1')
            todate = request.POST.get('datepicker2')
            dates_dict = get_fromdate_todate_for_django_query_filter(date_type, fromdate, todate)
            to_date_time = dates_dict.get('todate')
            from_date_time = dates_dict.get('fromdate')
        else:
            dates_dict = get_site_player_first_register_timestamp()
            to_date_time = dates_dict.get('todate')
            from_date_time = dates_dict.get('fromdate')
    campaignID = int(campaignID.split('-')[0]) if campaignID else None
    if uniquelogins:
        if campaignID:
            resp_dict['campaignID'] = campaignID
            campaign_obj = Campaign.objects.get(id=campaignID)
            resp_dict['campaign_name'] = campaign_obj.name
            l = Loginon.objects.filter(loginok=True, uniquelogin=True, logintime__gte=from_date_time.date(),
                                  logintime__lte=to_date_time.date(), campaignid=campaign_obj)
        else:
            l = Loginon.objects.filter(loginok=True, uniquelogin=True, logintime__gte=from_date_time.date(),
                                  logintime__lte=to_date_time.date(), affiliateid=obj)
    else:
        if campaignID:
            resp_dict['campaignID'] = campaignID
            campaign_obj = Campaign.objects.get(id=campaignID)
            resp_dict['campaign_name'] = campaign_obj.name
            l = Loginon.objects.filter(loginok=True,  logintime__gte=from_date_time.date(),
                                  logintime__lte=to_date_time.date(), campaignid=campaign_obj)
        else:
            l = Loginon.objects.filter(loginok=True, logintime__gte=from_date_time.date(),
                                  logintime__lte=to_date_time.date(), affiliateid=obj)
    l_list = []
    for i in l:
        temp = {}
        temp['playerid'] = i.playerid_id
        temp['playername'] = i.playerid.username
        temp['realchips'] = i.playerid.realchips
        temp['freechips'] = i.playerid.funchips
        if uniquelogins:
            temp['cashgames'] = Reconcilelogs.objects.filter(timestamp__lte = to_date_time,
                             timestamp__gte=from_date_time,transaction_type="WAGERING",
                             amounttype="CASH",playerid=i.playerid).count()
            temp['freegames'] = Reconcilelogs.objects.filter(playerid=i.playerid,
                              timestamp__lte = to_date_time,
                             timestamp__gte=from_date_time,transaction_type="WAGERING",
                             amounttype="FUNCHIPS").count()
            temp['deposits'] = Order.objects.filter(direction="DEPOSIT",orderstate="FINISHED",playerid=i.playerid, 
                                                    timestamp__gte = from_date_time, 
                                                    timestamp__lte=to_date_time).\
                                                    exclude(ordertype='REAL_ADJUSTMENT').\
                                                    aggregate(Sum('ordermoney')).values()[0]
        l_list.append(temp)
    resp_dict['players_info'] = l_list
    resp_dict['result_count'] = len(l_list)
    resp_dict['obj'] = obj
    resp_dict['campaign_list'] = Campaign.objects.filter(fk_affiliateinfo=obj)
    resp_dict['custom'] = date_type
    if date_type.lower() == 'custom':
        resp_dict['from_ui_date'] = from_date_time
        resp_dict['to_ui_date'] = from_date_time
    resp_dict['from_date_time'] = from_date_time
    resp_dict['to_date_time'] = to_date_time
    return resp_dict

@check_ip_required
@staff_member_required
def admin_affiliate_user_logins(request, id):
    resp_dict = get_affiliate_logins_or_uniquelogins(request, id, False)
    return render_to_response('affiliates/admin/affiliate_logins.html',
                                 resp_dict, context_instance=RequestContext(request)) 
@check_ip_required
@staff_member_required
def admin_affiliate_user_uniquelogins(request, id):
    resp_dict = get_affiliate_logins_or_uniquelogins(request, id, True)
    return render_to_response('affiliates/admin/affiliate_uniquelogins.html',
                                 resp_dict, context_instance=RequestContext(request)) 
