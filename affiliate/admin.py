""" affiliate/admin.py"""
from django.contrib import admin
from .utils import *
# # # # # # # # # # from django.contrib.sites.models import get_current_site
from affiliate.models import *
from userapp.models import *
from affiliate.views import *


class AffiliateinfoAdmin(admin.ModelAdmin):
    """ affiliate info admin display"""
    list_display = ['id', 'username', 'email', 'status']

    def save_model(self, request, obj, form, change):
        """ to check that admin of egcs can not have 5 account """
        affiliate = form.save(commit = False)
        password = form.cleaned_data.get("password")
        status = form.cleaned_data.get("status")
        if form.instance.id:
            if password and 'sha1$' not in password:
                affiliate.password = enc_password(password)
            if status == 'Approved':
                statuschangemail(affiliate, affiliate, 'AFFILIATE_SIGN_UP_APPROVAL')
            elif status == 'Rejected':
                # add_message_logs(affiliate, trigger_obj, affiliate.account_manager.email, affiliate.email)
                statuschangemail(affiliate, affiliate, 'AFFILIATE_SIGN_UP_REJECTED')
        else:
            affiliate.password = enc_password(password)
        affiliate.save()
        aff_comm_obj, created = Affiliatecommission.objects.get_or_create(fk_affiliateinfo=affiliate)
        if created:
            aff_comm_obj.revshare=True
            aff_comm_obj.revsharepercent=25
            aff_comm_obj.save()

admin.site.register(Affiliateinfo, AffiliateinfoAdmin)


class CampaignAdmin(admin.ModelAdmin):
    """ campaign ui display """
    list_display = ['id','link', 'name']
admin.site.register(Campaign, CampaignAdmin)

class CurrencyAdmin(admin.ModelAdmin):
    """ campaign ui display """
    list_display = [ 'siteid', 'currency']
admin.site.register(Currency, CurrencyAdmin)

class CampaignTrackAdmin(admin.ModelAdmin):
    """ campaign ui display """
    list_display = [ 'id', 'url', 'trackerid', 'get_campname']

    def get_campname(self, obj):
        return obj.fk_campaign.name if obj.fk_campaign else ''

admin.site.register(CampaignTrackerMapping, CampaignTrackAdmin)


# class AffiliateIdMappingAdmin(admin.ModelAdmin):
#     """ campaign ui display """
#     list_display = [ 'id', 'siteid', 'masteraffiliate', 'fk_affiliateinfo']


# admin.site.register(AffiliateIdMapping, AffiliateIdMappingAdmin)

from django import forms

# class MediaTypeAdmin(admin.ModelAdmin):
#     """ Media Type ui display """
#     list_display = ['type', 'id']
# admin.site.register(MediaType, MediaTypeAdmin)

class MediaCategorForm(forms.ModelForm):

    class Meta:
        model = MediaCategory
        fields = "__all__"

    def clean(self):
        cleaned_data = self.cleaned_data
        category_name = cleaned_data.get("category_name")
        siteid = cleaned_data.get("siteid")
        m_obj = MediaCategory.objects.filter(category_name=category_name, siteid=siteid)
        if m_obj:
            raise forms.ValidationError(u"Already a category with this name exist for the site you have selected.")
        return cleaned_data

class MediaCategoryAdmin(admin.ModelAdmin):
    """ campaign ui display """
    form = MediaCategorForm
    list_display = [ 'siteid', 'category_name']

admin.site.register(MediaCategory, MediaCategoryAdmin)

class BannerImagesAdmin(admin.ModelAdmin):
    """ Banner Images"""
    list_display = ['id', 'user', 'siteid', 'banner', 'height', 'width', 'media_type']
admin.site.register(BannerImages, BannerImagesAdmin)

class MailerAdmin(admin.ModelAdmin):
    """ Banner Images"""
    list_display = ['siteid', 'subject']

    class Media:
        """ to call tinymce javascript"""
        js = ("/static_files/js/tiny_mce/tiny_mce.js", '/static_files/js/textareas.js')

admin.site.register(Mailer, MailerAdmin)

class VideoAdmin(admin.ModelAdmin):
    """ Banner Images"""
    list_display = ['id', 'user', 'siteid', 'video', 'title']
admin.site.register(Video, VideoAdmin)

class LpagesAdmin(admin.ModelAdmin):
    """ Banner Images"""
    list_display = ['id', 'user', 'siteid', 'lpages', 'title']
admin.site.register(Lpages, LpagesAdmin)


class BannerlogsAdmin(admin.ModelAdmin):
    """ banner logs image display"""
    list_display = ['id', 'link', 'fk_bannerimages', 'fk_campaign']
admin.site.register(Bannerlogs, BannerlogsAdmin)

class PagePeellogsAdmin(admin.ModelAdmin):
    """ banner logs image display"""
    list_display = ['pagepeelimage', 'fk_campaign']
admin.site.register(PagePeellogs, PagePeellogsAdmin)

class MailerlogsAdmin(admin.ModelAdmin):
    """ banner logs image display"""
    list_display = ['mailer', 'fk_campaign']
admin.site.register(Mailerlogs, MailerlogsAdmin)

class ScreenShotslogsAdmin(admin.ModelAdmin):
    """ banner logs image display"""
    list_display = ['id', 'screenimages', 'fk_campaign']
admin.site.register(ScreenShotslogs, ScreenShotslogsAdmin)

class VideologsAdmin(admin.ModelAdmin):
    """ banner logs image display"""
    list_display = ['id', 'fk_video', 'fk_campaign']
admin.site.register(Videologs, VideologsAdmin)

class LandingPagelogsAdmin(admin.ModelAdmin):
    """ banner logs image display"""
    list_display = ['id','fk_lpage', 'fk_campaign']
admin.site.register(LandingPagelogs, LandingPagelogsAdmin)

class AffiliatecommissionAdmin(admin.ModelAdmin):
    """ affiliate commisssion simple admin ui """
    list_display = ['id', 'siteid', 'fk_affiliateinfo', 'revsharepercent', 'criteriaacquisition',
                    'cpacommissionvalue', 'referalchoice',
                    'referalcommissionvalue']
    filter_horizontal = ('country',)
admin.site.register(Affiliatecommission, AffiliatecommissionAdmin)

class AccountDetailsAdmin(admin.ModelAdmin):
    """ affiliate commisssion simple admin ui """
    list_display = ['fk_affiliateinfo', 'bankname']
admin.site.register(AccountDetails, AccountDetailsAdmin)

class VcommFPAffiliateMappingAdmin(admin.ModelAdmin):
    """ affiliate commisssion simple admin ui """
    list_display = ['vcommsubid', 'affiliateid']

admin.site.register(VcommFPAffiliateMapping, VcommFPAffiliateMappingAdmin)

class MessagelogsAdmin(admin.ModelAdmin):
    """ affiliate commisssion simple admin ui """
    list_display = ['affiliate', 'from_email', 'to_email', 'timestamp']
admin.site.register(Messagelogs, MessagelogsAdmin)
