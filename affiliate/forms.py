from django.forms import ModelForm
from .models import *

class ContactUsForm(ModelForm):
    class Meta:
        model = AffiliateContactUs
        fields = ['username', 'name', 'subject', 'message', 'email']