from django.apps import apps
from django.db.models.signals import post_save, pre_delete, m2m_changed
from django.contrib.auth.models import User, Group
from django.conf import settings

from .synchronization import ChangeLogSynchronizer
from .models import *

APP_NAMES = [app_name.split('.')[0] for app_name in ['userapp', 'affiliate']]

POST_SAVE_SIGNALS_MODELS = [User, Group]
M2M_CHANGED_SIGNALS_MODELS = [User.groups.through]
EXCLUDED_MODELS = [ChangeLog]


def connect_signals():
    #import pdb;pdb.set_trace()
    for app_name in APP_NAMES:
        app_models = apps.get_app_config(app_name).get_models()

        for model in app_models:
            if model not in EXCLUDED_MODELS:
                POST_SAVE_SIGNALS_MODELS.append(model)

                for field in model._meta.many_to_many:
                    M2M_CHANGED_SIGNALS_MODELS.append(
                        getattr(model, field.name).through
                    )
            print model

    for sender in POST_SAVE_SIGNALS_MODELS:
        post_save.connect(ChangeLogSynchronizer.post_save_receiver, sender=sender)
        pre_delete.connect(ChangeLogSynchronizer.post_delete_receiver, sender=sender)

    for sender in M2M_CHANGED_SIGNALS_MODELS:
        m2m_changed.connect(ChangeLogSynchronizer.m2m_changed_receiver, sender=sender)
