import ast
from django.db.models import Sum, Count, Max
from dateutil.rrule import WEEKLY, SU, rrule, DAILY
import calendar
from datetime import date, time, datetime

from userapp.models import *
from affiliate.models import *
from userapp.utils import monthdelta, error_log
from affiliate.utils import enc_password
from userapp.userapp_db_entry import get_selected_channel_devices_info_objs,\
    get_device_info_obj
from connectors.models import *
from userapp.utils import *
from userapp.mails import *
from functools import reduce
from django.contrib.sites.models import Site
import json
from django.db import connection
import datetime
from dateutil.rrule import rrule, MONTHLY, YEARLY
from affiliate.views import *
import functools
from django.db.models import F
# from settings import cache_time
cache_time = 172000

# from django.core.cache import cache


def get_last_12_months_list(to_date):
    month = to_date.month
    year = to_date.year
    d = []
    y = year
    nm = month if month != 12 else 1
    ny = year if month !=12 else year+1
    d.append((ny, nm))
    for i in range(1,13):
        m = month - i
        if m == 0:
            m = 12
            y = year-1
        d.append((y, m))
    d.reverse()
    return d

def get_currency_ids_dict(site_objs):
    currency_dict = {}
    for site in site_objs:
        if site.id in currency_dict:
            currency = currency_dict[site]
        else:
            currency_obj = Currency.objects.filter(siteid=site)
            currency_dict[site.id] = currency_obj[0].currency if currency_obj else ''
            currency = currency_dict[site.id]
    return currency_dict

def get_currency_dict(site_objs):
    currency_dict = {}
    for site in site_objs:
        if site in currency_dict:
            currency = currency_dict[site]
        else:
            currency_obj = Currency.objects.filter(siteid=site)
            currency_dict[site] = currency_obj[0].currency if currency_obj else ''
            currency = currency_dict[site]
    return currency_dict

def get_commission_aff_dict(obj, aff_list):
    commission_dict = {}
    commission_obj = Affiliatecommission.objects.filter(
                fk_affiliateinfo__in=aff_list).values('fk_affiliateinfo',
        'criteriaacquisition', 'criteriavalue',
        'cpacommissionvalue', 'revsharepercent',
        'referalcommissionvalue', 'country')
    comm_aff_dict = {i['fk_affiliateinfo']: i for i in commission_obj}
    for aff in aff_list:
        if aff.id in comm_aff_dict:
            commission = comm_aff_dict[aff.id]
            commission_dict[aff.id] = {'cpa':[commission['criteriaacquisition'], 
                        commission['criteriavalue'], commission['cpacommissionvalue']],
                        'revshare': commission['revsharepercent'],
                        'refferal': commission['referalcommissionvalue'],
                        'country': commission['country'] if commission.get('country') else False}
    return commission_dict

def get_commission_affiliate_dict(aff_list, site_ids, account_m_dict={}, d_date=False):
    comm_brand_dict, comm_default_dict, comm_aff_dict, comm_dict = {}, {}, {}, {}
    commission_dict = None
    # commission_dict = cache.get(str(campaign_ids_id.sort())+str(aff_list_ids.sort())+str(acc_manger)+str(site_ids_id.sort()))
    if not commission_dict:
        commission_dict = {}
        commission_obj = None
        if aff_list:
            commission_aff_obj = Affiliatecommission.objects.filter(
                                active=True,fk_affiliateinfo_id__in=aff_list,
                                siteid_id__in=site_ids).values('fk_affiliateinfo',
                                'siteid',
                                'criteriaacquisition', 'criteriavalue',
                                'cpacommissionvalue', 'revsharepercent',
                                'referalcommissionvalue', 'pocdeduction',
                                'start_date', 'end_date', 'country', 
                                'currency','ringfence', 'exclude_rev')
            commission_obj = Affiliatecommission.objects.filter(
                                active=True,fk_affiliateinfo_id__in=aff_list,
                                siteid__isnull=True).values('fk_affiliateinfo',
                                'siteid',
                                'criteriaacquisition', 'criteriavalue',
                                'cpacommissionvalue', 'revsharepercent',
                                'referalcommissionvalue', 'pocdeduction',
                                'start_date', 'end_date', 'country', 
                                'currency','ringfence', 'exclude_rev')
            comm_aff_obj_dict = {(i['fk_affiliateinfo']): i for i in commission_obj}
            comm_aff_dict = {(i['fk_affiliateinfo'], i['siteid']): i for i in commission_aff_obj}
        if site_ids:
            commission_obj = Affiliatecommission.objects.filter(active=True,
                                siteid_id__in=site_ids).values('siteid',
                                'criteriaacquisition', 'criteriavalue',
                                'cpacommissionvalue', 'revsharepercent',
                                'referalcommissionvalue', 'pocdeduction',
                                'start_date', 'end_date', 'country', 
                                'currency','ringfence', 'exclude_rev')
            comm_brand_dict = {(i['siteid']): i for i in commission_obj}
        # if country_list:
        # country_commission_obj = Affiliatecommission.objects.filter(active=True,
        #                     country__isnull=False).values('country').distinct()
        # if not commission_obj:
        commission_obj = Affiliatecommission.objects.filter(active=True).values('account_manager',
                                'criteriaacquisition', 'criteriavalue',
                                'cpacommissionvalue', 'revsharepercent',
                                'referalcommissionvalue', 'pocdeduction',
                                'start_date', 'end_date', 'country', 
                                'currency','ringfence', 'exclude_rev')
        comm_default_dict = {(i['account_manager']): i for i in commission_obj}
        if aff_list:
            for aff in aff_list:
                for i in site_ids:
                    if (aff, i) in comm_aff_dict:
                        commission = comm_aff_dict[(aff, i)]
                    elif (aff) in comm_aff_obj_dict:
                        commission = comm_aff_obj_dict[(aff)]
                    elif (i) in comm_brand_dict:
                        commission = comm_brand_dict[(i)]
                    elif comm_default_dict and account_m_dict.get(aff) and comm_default_dict.get((account_m_dict.get(aff))):
                        commission = comm_default_dict[(account_m_dict.get(aff))]
                    else:
                        continue
                    commission_dict[(aff,i)] = {'cpa':[commission['criteriaacquisition'], 
                            commission['criteriavalue'], commission['cpacommissionvalue']],
                            'revshare': commission['revsharepercent'],
                            'refferal': commission['referalcommissionvalue'],
                            'pocdeduction': commission['pocdeduction'],
                            'ringfence': commission['ringfence'],
                            'exclude_rev': commission['exclude_rev'],
                            'currency': commission['currency'] if commission.get('currency') else 'gbp',
                            'country': commission['country'] if commission.get('country') else False}
    return commission_dict

def get_commission_all_dict(camp_track_links, aff_list, site_ids, d_date=False, aff_c=False):
    comm_brand_dict, comm_default_dict, comm_aff_dict, comm_dict = {}, {}, {}, {}
    campaign_ids = [i.fk_campaign for i in camp_track_links]
    campaign_ids_id = [i.id for i in campaign_ids]
    aff_list_ids = [i.id for i in aff_list]
    site_ids_id = [i.id for i in site_ids]
    commission_dict = None
    # commission_dict = cache.get(str(campaign_ids_id.sort())+str(aff_list_ids.sort())+str(acc_manger)+str(site_ids_id.sort()))
    if not commission_dict:
        commission_dict = {}
        commission_obj = None
        if camp_track_links:
            campaign_ids = [i.fk_campaign for i in camp_track_links]
            commission_obj = Affiliatecommission.objects.filter(
                                Q(start_date__lte=d_date)|Q(start_date__isnull=True),
                                Q(end_date__gte=d_date)|Q(end_date__isnull=True),
                                Q(fk_campaign__in=camp_track_links)).values('fk_campaign',
                                'revshare', 'cpachoice', 'referalchoice',
                                'criteriaacquisition', 'criteriavalue',
                                'cpacommissionvalue', 'revsharepercent',
                                'referalcommissionvalue', 'pocdeduction',
                                'start_date', 'end_date', 'country', 
                                'currency','ringfence', 'exclude_rev')
            comm_dict = {(i['fk_campaign'], i['country']): i for i in commission_obj}
        if aff_list:
            commission_aff_obj = Affiliatecommission.objects.filter(
                                Q(start_date__lte=d_date)|Q(start_date__isnull=True),
                                Q(end_date__gte=d_date)|Q(end_date__isnull=True),
                                Q(active=True),Q(fk_affiliateinfo__in=aff_list),
                                Q(siteid__in=site_ids)).values('id','fk_affiliateinfo',
                                'siteid', 'revshare', 'cpachoice', 'referalchoice',
                                'criteriaacquisition', 'criteriavalue',
                                'cpacommissionvalue', 'revsharepercent',
                                'referalcommissionvalue', 'pocdeduction',
                                'start_date', 'end_date', 'country', 
                                'currency','ringfence', 'exclude_rev')
            commission_obj = Affiliatecommission.objects.filter(
                                Q(start_date__lte=d_date)|Q(start_date__isnull=True),
                                Q(end_date__gte=d_date)|Q(end_date__isnull=True),
                                Q(active=True),Q(fk_affiliateinfo__in=aff_list),
                                Q(siteid__isnull=True)).values('id','fk_affiliateinfo',
                                'siteid','revshare', 'cpachoice', 'referalchoice',
                                'criteriaacquisition', 'criteriavalue',
                                'cpacommissionvalue', 'revsharepercent',
                                'referalcommissionvalue', 'pocdeduction',
                                'start_date', 'end_date', 'country', 
                                'currency','ringfence', 'exclude_rev')
            comm_aff_obj_dict = {(i['fk_affiliateinfo'], i['country']): i for i in commission_obj}
            comm_aff_dict = {(i['fk_affiliateinfo'], i['siteid'], i['country']): i for i in commission_aff_obj}
        if site_ids:
            commission_obj = Affiliatecommission.objects.filter(
                                Q(start_date__lte=d_date)|Q(start_date__isnull=True),
                                Q(end_date__gte=d_date)|Q(end_date__isnull=True),
                                Q(active=True),Q(siteid__in=site_ids),Q(fk_affiliateinfo__isnull=True)).values('siteid',
                                'revshare', 'cpachoice', 'referalchoice',
                                'criteriaacquisition', 'criteriavalue',
                                'cpacommissionvalue', 'revsharepercent',
                                'referalcommissionvalue', 'pocdeduction',
                                'start_date', 'end_date', 'country', 
                                'currency','ringfence', 'exclude_rev')
            comm_brand_dict = {(i['siteid'], i['country']): i for i in commission_obj}
        # if country_list:
        country_commission_obj = Affiliatecommission.objects.filter(active=True,
                            country__isnull=False).values('country').distinct()
        country_list = [i['country'] for i in country_commission_obj]
        country_list.append(None)
        # if not commission_obj:
        commission_obj = Affiliatecommission.objects.filter(active=True,
                                fk_affiliateinfo__isnull=True,
                                siteid__isnull=True,fk_campaign__isnull=True).values('account_manager',
                                'revshare', 'cpachoice', 'referalchoice',
                                'criteriaacquisition', 'criteriavalue',
                                'cpacommissionvalue', 'revsharepercent',
                                'referalcommissionvalue', 'pocdeduction',
                                'start_date', 'end_date', 'country', 
                                'currency','ringfence', 'exclude_rev')
        comm_default_dict = {(i['account_manager'], i['country']): i for i in commission_obj}
        camp_country_list = [(x,y) for x in camp_track_links.order_by('id') for y in country_list]
        for camp_country_track in camp_country_list:
            default = False
            camp_track = camp_country_track[0]
            country_d = camp_country_track[1]
            if (camp_track.id, country_d) in comm_dict:
                commission = comm_dict[(camp_track.id, country_d)]
            elif (camp_track.id, None) in comm_dict:
                commission = comm_dict[(camp_track.id, None)]
            elif camp_track.fk_affiliateinfo and (camp_track.fk_affiliateinfo.id, camp_track.fk_campaign.siteid.id, country_d) in comm_aff_dict:
                commission = comm_aff_dict[(camp_track.fk_affiliateinfo.id, camp_track.fk_campaign.siteid.id, country_d)]
            elif camp_track.fk_affiliateinfo and (camp_track.fk_affiliateinfo.id, camp_track.fk_campaign.siteid.id, None) in comm_aff_dict:
                commission = comm_aff_dict[(camp_track.fk_affiliateinfo.id, camp_track.fk_campaign.siteid.id, None)]
            elif camp_track.fk_affiliateinfo and (camp_track.fk_affiliateinfo.id,country_d)  in comm_aff_obj_dict:
                commission = comm_aff_obj_dict[(camp_track.fk_affiliateinfo.id,country_d)]
            elif camp_track.fk_affiliateinfo and (camp_track.fk_affiliateinfo.id,None)  in comm_aff_obj_dict:
                commission = comm_aff_obj_dict[(camp_track.fk_affiliateinfo.id,None)]
            elif camp_track.fk_campaign.siteid and (camp_track.fk_campaign.siteid.id,country_d) in comm_brand_dict:
                commission = comm_brand_dict[(camp_track.fk_campaign.siteid.id,country_d)]
                default = True
            elif camp_track.fk_campaign.siteid and (camp_track.fk_campaign.siteid.id,None) in comm_brand_dict:
                commission = comm_brand_dict[(camp_track.fk_campaign.siteid.id,None)]
                default = True
            elif comm_default_dict and camp_track.fk_affiliateinfo and camp_track.fk_affiliateinfo.account_manager and comm_default_dict.get((camp_track.fk_affiliateinfo.account_manager.id, country_d)):
                commission = comm_default_dict[(camp_track.fk_affiliateinfo.account_manager.id, country_d)]
                default = True
            elif comm_default_dict and camp_track.fk_affiliateinfo and camp_track.fk_affiliateinfo.account_manager and comm_default_dict.get((camp_track.fk_affiliateinfo.account_manager.id, None)):
                commission = comm_default_dict[(camp_track.fk_affiliateinfo.account_manager.id, None)]
                default = True
            else:
                continue
            commission_dict[(camp_track.id, country_d)] = {'cpa':[commission['criteriaacquisition'], 
                        commission['criteriavalue'], commission['cpacommissionvalue'] if commission['cpachoice'] else None],
                        'revshare': commission['revsharepercent'] if commission['revshare'] else 0,
                        'refferal': commission['referalcommissionvalue'] if commission['referalchoice'] else 0,
                        'pocdeduction': commission['pocdeduction'],
                        'ringfence': commission['ringfence'],
                        'exclude_rev': commission['exclude_rev'],
                        'currency': commission['currency'] if commission.get('currency') else 'gbp',
                        'country': commission['country'] if commission.get('country') else False,
                        'default': default}
        if aff_c:
            aff_country_list = [(x,y) for x in aff_list for y in country_list]
            print 'brand c', comm_brand_dict, aff_list
            for aff_country in aff_country_list:
                aff = aff_country[0]
                country_d = aff_country[1]
                for i in aff.siteid.all():
                    default = False
                    if (aff.id, i.id, country_d) in comm_aff_dict:
                        commission = comm_aff_dict[(aff.id, i.id, country_d)]
                    elif (aff.id, i.id, None) in comm_aff_dict:
                        commission = comm_aff_dict[(aff.id, i.id, None)]
                    elif (aff.id, country_d) in comm_aff_obj_dict:
                        commission = comm_aff_obj_dict[(aff.id, country_d)]
                    elif (aff.id, None) in comm_aff_obj_dict:
                        commission = comm_aff_obj_dict[(aff.id, None)]
                    elif (i.id, country_d) in comm_brand_dict:
                        commission = comm_brand_dict[(i.id, country_d)]
                    elif (i.id, None) in comm_brand_dict:
                        commission = comm_brand_dict[(i.id, None)]
                    elif comm_default_dict and aff.account_manager and comm_default_dict.get((aff.account_manager.id, country_d)):
                        commission = comm_default_dict[(aff.account_manager.id, country_d)]
                        default =True
                    elif comm_default_dict and aff.account_manager and comm_default_dict.get((aff.account_manager.id, None)):
                        commission = comm_default_dict[(aff.account_manager.id, None)]
                        default =True
                    else:
                        continue
                    commission_dict["%s/%s/%s"%(aff.id,i.id, country_d)] = {'cpa':[commission['criteriaacquisition'], 
                                commission['criteriavalue'], commission['cpacommissionvalue']],
                                'revshare': commission['revsharepercent'],
                                'refferal': commission['referalcommissionvalue'],
                                'pocdeduction': commission['pocdeduction'],
                                'ringfence': commission['ringfence'],
                                'exclude_rev': commission['exclude_rev'],
                                'currency': commission['currency'] if commission.get('currency') else 'gbp',
                        'country': commission['country'] if commission.get('country') else False,
                        'default': default}
        elif not camp_track_links:
            aff_country_list = [(x,y) for x in aff_list for y in country_list]
            for aff_country in aff_country_list:
                aff = aff_country[0]
                country_d = aff_country[1]
                if (aff.id, country_d) in comm_aff_obj_dict:
                    commission = comm_aff_obj_dict[(aff.id, country_d)]
                elif comm_default_dict and aff.account_manager and comm_default_dict.get((aff.account_manager.id, country_d)):
                    commission = comm_default_dict[(aff.account_manager.id, country_d)]
                else:
                    continue
                commission_dict[(aff.id, country_d)] = {'cpa':[commission['criteriaacquisition'], 
                            commission['criteriavalue'], commission['cpacommissionvalue']],
                            'revshare': commission['revsharepercent'],
                            'refferal': commission['referalcommissionvalue'],
                            'pocdeduction': commission['pocdeduction'],
                            'ringfence': commission['ringfence'],
                            'exclude_rev': commission['exclude_rev'],
                            'currency': commission['currency'] if commission.get('currency') else 'gbp',
                        'country': commission['country'] if commission.get('country') else False}
        # cache.set(str(campaign_ids_id.sort())+str(aff_list_ids.sort())+str(acc_manger)+str(site_ids_id.sort()),
        #  commission_dict, cache_time)
    return commission_dict


def get_commission_dict(obj, camp_track_links, aff_list, site_ids, aff_c=False):
    comm_brand_dict, comm_default_dict, comm_aff_dict, comm_dict = {}, {}, {}, {}
    try:
        acc_manger = aff_list[0].account_manager
    except:
        if isinstance(obj, AccountManager):
            acc_manger = obj
        else:
            acc_manger = obj.account_manager
    campaign_ids = [i.fk_campaign for i in camp_track_links]
    campaign_ids_id = [i.id for i in campaign_ids]
    aff_list_ids = [i.id for i in aff_list]
    site_ids_id = [i.id for i in site_ids]
    commission_dict = None
    # commission_dict = cache.get(str(campaign_ids_id.sort())+str(aff_list_ids.sort())+str(acc_manger)+str(site_ids_id.sort()))
    if not commission_dict:
        commission_dict = {}
        commission_obj = None
        if camp_track_links:
            campaign_ids = [i.fk_campaign for i in camp_track_links]
            commission_obj = Affiliatecommission.objects.filter(active=True,
                                fk_campaign__in=camp_track_links).values('fk_campaign',
                                'criteriaacquisition', 'criteriavalue',
                                'cpacommissionvalue', 'revsharepercent',
                                'referalcommissionvalue', 'pocdeduction',
                                'start_date', 'end_date', 'country', 'currency')
            comm_dict = {(i['fk_campaign'], i['country']): i for i in commission_obj}
        if aff_list:
            commission_aff_obj = Affiliatecommission.objects.filter(
                                active=True,fk_affiliateinfo__in=aff_list,
                                siteid__in=site_ids).values('fk_affiliateinfo',
                                'siteid',
                                'criteriaacquisition', 'criteriavalue',
                                'cpacommissionvalue', 'revsharepercent',
                                'referalcommissionvalue', 'pocdeduction',
                                'start_date', 'end_date', 'country', 'currency')
            commission_obj = Affiliatecommission.objects.filter(
                                active=True,fk_affiliateinfo__in=aff_list,
                                siteid__isnull=True).values('fk_affiliateinfo',
                                'siteid',
                                'criteriaacquisition', 'criteriavalue',
                                'cpacommissionvalue', 'revsharepercent',
                                'referalcommissionvalue', 'pocdeduction',
                                'start_date', 'end_date', 'country', 'currency')
            comm_aff_obj_dict = {(i['fk_affiliateinfo'], i['country']): i for i in commission_obj}
            comm_aff_dict = {(i['fk_affiliateinfo'], i['siteid'], i['country']): i for i in commission_aff_obj}
        if site_ids:
            commission_obj = Affiliatecommission.objects.filter(active=True,
                                siteid__in=site_ids).values('siteid',
                                'criteriaacquisition', 'criteriavalue',
                                'cpacommissionvalue', 'revsharepercent',
                                'referalcommissionvalue', 'pocdeduction',
                                'start_date', 'end_date', 'country', 'currency')
            comm_brand_dict = {(i['siteid'], i['country']): i for i in commission_obj}
        # if country_list:
        country_commission_obj = Affiliatecommission.objects.filter(active=True,
                            country__isnull=False).values('country').distinct()
        country_list = [i['country'] for i in country_commission_obj]
        country_list.append(None)
        # if not commission_obj:
        commission_obj = Affiliatecommission.objects.filter(active=True).values('account_manager',
                                'criteriaacquisition', 'criteriavalue',
                                'cpacommissionvalue', 'revsharepercent',
                                'referalcommissionvalue', 'pocdeduction',
                                'start_date', 'end_date', 'country', 'currency')
        comm_default_dict = {(i['account_manager'], i['country']): i for i in commission_obj}
        camp_country_list = [(x,y) for x in camp_track_links.order_by('id') for y in country_list]
        for camp_country_track in camp_country_list:
            camp_track = camp_country_track[0]
            country_d = camp_country_track[1]
            if (camp_track.id, country_d) in comm_dict:
                commission = comm_dict[(camp_track.id, country_d)]
            elif camp_track.fk_affiliateinfo and (camp_track.fk_affiliateinfo.id, camp_track.fk_campaign.siteid.id, country_d) in comm_aff_dict:
                commission = comm_aff_dict[(camp_track.fk_affiliateinfo.id, camp_track.fk_campaign.siteid.id, country_d)]
            elif camp_track.fk_affiliateinfo and (camp_track.fk_affiliateinfo.id,country_d)  in comm_aff_obj_dict:
                commission = comm_aff_obj_dict[(camp_track.fk_affiliateinfo.id,country_d)]
            elif camp_track.fk_campaign.siteid and (camp_track.fk_campaign.siteid.id,country_d) in comm_brand_dict:
                commission = comm_brand_dict[(camp_track.fk_campaign.siteid.id,country_d)]
            elif comm_default_dict and camp_track.fk_affiliateinfo and camp_track.fk_affiliateinfo.account_manager and comm_default_dict.get((camp_track.fk_affiliateinfo.account_manager.id, country_d)):
                commission = comm_default_dict[(camp_track.fk_affiliateinfo.account_manager.id, country_d)]
            else:
                continue
            commission_dict[(camp_track.id, country_d)] = {'cpa':[commission['criteriaacquisition'], 
                        commission['criteriavalue'], commission['cpacommissionvalue']],
                        'revshare': commission['revsharepercent'],
                        'refferal': commission['referalcommissionvalue'],
                        'pocdeduction': commission['pocdeduction'],
                        'currency': commission['currency'] if commission.get('currency') else 'gbp',
                        'country': commission['country'] if commission.get('country') else False}
        if aff_c:
            aff_country_list = [(x,y) for x in aff_list for y in country_list]
            print 'brand c', comm_brand_dict, aff_list
            for aff_country in aff_country_list:
                aff = aff_country[0]
                country_d = aff_country[1]
                for i in aff.siteid.all():
                    if (aff.id, i.id, country_d) in comm_aff_dict:
                        commission = comm_aff_dict[(aff.id, i.id, country_d)]
                    elif (aff.id, country_d) in comm_aff_obj_dict:
                        commission = comm_aff_obj_dict[(aff.id, country_d)]
                    elif (i.id, country_d) in comm_brand_dict:
                        commission = comm_brand_dict[(i.id, country_d)]
                    elif comm_default_dict and aff.account_manager and comm_default_dict.get((aff.account_manager.id, country_d)):
                        commission = comm_default_dict[(aff.account_manager.id, country_d)]
                    else:
                        continue
                    commission_dict["%s/%s/%s"%(aff.id,i.id, country_d)] = {'cpa':[commission['criteriaacquisition'], 
                                commission['criteriavalue'], commission['cpacommissionvalue']],
                                'revshare': commission['revsharepercent'],
                                'refferal': commission['referalcommissionvalue'],
                                'pocdeduction': commission['pocdeduction'],
                                'currency': commission['currency'] if commission.get('currency') else 'gbp',
                        'country': commission['country'] if commission.get('country') else False}
        elif not camp_track_links:
            aff_country_list = [(x,y) for x in aff_list for y in country_list]
            for aff_country in aff_country_list:
                aff = aff_country[0]
                country_d = aff_country[1]
                if (aff.id, country_d) in comm_aff_obj_dict:
                    commission = comm_aff_obj_dict[(aff.id, country_d)]
                elif comm_default_dict and aff.account_manager and comm_default_dict.get((aff.account_manager.id, country_d)):
                    commission = comm_default_dict[(aff.account_manager.id, country_d)]
                else:
                    continue
                commission_dict[(aff.id, country_d)] = {'cpa':[commission['criteriaacquisition'], 
                            commission['criteriavalue'], commission['cpacommissionvalue']],
                            'revshare': commission['revsharepercent'],
                            'refferal': commission['referalcommissionvalue'],
                            'pocdeduction': commission['pocdeduction'],
                        'currency': commission['currency'] if commission.get('currency') else 'gbp',
                        'country': commission['country'] if commission.get('country') else False}
        # cache.set(str(campaign_ids_id.sort())+str(aff_list_ids.sort())+str(acc_manger)+str(site_ids_id.sort()),
        #  commission_dict, cache_time)
    return commission_dict


def get_cliks_data(aff_objs, from_date, to_date, site_objs, camp=None):
    count_obj = CampaignClicksCount.objects.filter(
                        fk_campaign__fk_campaign__siteid__in=site_objs)
    # count_obj = PostBackClicks.objects.filter(
    #                                 siteid__in=site_objs)
    process_obj = PPPlayerProcessed.objects.filter(siteid__in=site_objs)
    if camp:
        count_obj = count_obj.filter(fk_campaign__in=camp)
        process_obj = process_obj.filter(fk_campaign__in=camp)
    else:
        count_obj = count_obj.filter(fk_campaign__fk_affiliateinfo__in=aff_objs)
        process_obj = process_obj.filter(fk_campaign__fk_affiliateinfo__in=aff_objs)
    if from_date == to_date:
        to_date = datetime.strptime(to_date, '%Y-%m-%d') + timedelta(days=1)
        count_obj = count_obj.filter(date=from_date).aggregate(
                        clicks= Sum('clicks'))
        # count_obj = count_obj.filter(date__gte=from_date,
        #                             date__lte=to_date)
        registration_data = process_obj.filter(registrationdate__gte=from_date,
                                    registrationdate__lte=to_date).values('playerid').distinct()
        ftd_data = process_obj.filter(ftddate__gte=from_date,
                            ftddate__lte=to_date
                            ,deposit__gt=0).values('playerid').distinct()
    else:
        count_obj = count_obj.filter(date__gte=str(from_date).split(' ')[0],
                        date__lte=str(to_date).split(' ')[0]).aggregate(
                        clicks= Sum('clicks'))
        # count_obj = count_obj.filter(date__gte=from_date,
        #                 date__lte=to_date)
        registration_data = process_obj.filter(registrationdate__gte=from_date,
                        registrationdate__lte=to_date).values('playerid').distinct()
        ftd_data = process_obj.filter(deposit__gt=0,
                    ftddate__gte=from_date,
                    ftddate__lte=to_date).values('playerid').distinct()
    click_count = count_obj['clicks'] if count_obj['clicks'] else 0
    return {'registrations':len(registration_data), 
                        'clicks': click_count, 'ftd': len(ftd_data)}

def get_device_info_chart(aff_obj, obj, commission, from_date_time, to_date_time, site_ids_list):
    mobile_tracker_obj = PPPlayerProcessed.objects.filter(fk_campaign__fk_affiliateinfo__in=obj,
                            registrationdate__gte=str(from_date_time).split(' ')[0],
                            registrationdate__lte=str(to_date_time),
                            platform='Mobile',
                            siteid__in=site_ids_list).values('playerid').distinct()

    web_tracker_obj = PPPlayerProcessed.objects.filter(fk_campaign__fk_affiliateinfo__in=obj,
                        registrationdate__gte=str(from_date_time).split(' ')[0],
                        registrationdate__lte=str(to_date_time),
                        platform='Web',
                        siteid__in=site_ids_list).values('playerid').distinct()

    mobile_ftd_obj = PPPlayerProcessed.objects.filter(fk_campaign__fk_affiliateinfo__in=obj,
                            ftddate__gte=str(from_date_time).split(' ')[0],
                            ftddate__lte=str(to_date_time),
                            platform='Mobile',
                            siteid__in=site_ids_list,
                            ftddate = F('date')
                            ).exclude(ftddate__isnull=True).values('playerid').distinct()

    web_ftd_obj = PPPlayerProcessed.objects.filter(fk_campaign__fk_affiliateinfo__in=obj,
                        ftddate__gte=str(from_date_time).split(' ')[0],
                        ftddate__lte=str(to_date_time),
                        platform='Web',
                        siteid__in=site_ids_list).values('playerid').distinct()
    mobile_clicks_obj = CampaignClicksCount.objects.filter(fk_campaign__fk_affiliateinfo__in=obj,
                    date__gte=str(from_date_time).split(' ')[0],
                    date__lte=str(to_date_time).split(' ')[0],
                    fk_campaign__fk_campaign__siteid__in=site_ids_list,
                    platform='Mobile').aggregate(
                        clicks= Sum('clicks'))
    mobile_clicks_obj = mobile_clicks_obj['clicks'] if mobile_clicks_obj['clicks'] else 0
    web_clicks_obj = CampaignClicksCount.objects.filter(fk_campaign__fk_affiliateinfo__in=obj,
                    date__gte=str(from_date_time).split(' ')[0],
                    date__lte=str(to_date_time).split(' ')[0],
                    fk_campaign__fk_campaign__siteid__in=site_ids_list,
                    platform='Web').aggregate(
                        clicks= Sum('clicks'))
    web_clicks_obj = web_clicks_obj['clicks'] if web_clicks_obj['clicks'] else 0
    # regis_d = {'name': 'Registrations', 'color': '#7cb5ec', 'data': [
    #                     10, 12, 0, 0
    #                 ]
    #             }
    clicks_d = {'name': 'Clicks', 'color': '#93c83d', 'data': [
                        web_clicks_obj, mobile_clicks_obj, 0, 0
                    ]
                }
    ftd_d = {'name': 'FTD', 'color': '#0b99d7', 'data': [
                        len(web_ftd_obj), len(mobile_ftd_obj), 0, 0
                    ]
                }

    regis_d = {'name': 'Registrations', 'data': [
                        len(web_tracker_obj), len(mobile_tracker_obj), 0, 0
                    ]
                }
    # clicks_d = {'name': 'Clicks', 'data': [
    #                     web_tracker_obj['clicks'], mobile_tracker_obj['clicks'], 0, 0
    #                 ]
    #             }
    # ftd_d = {'name': 'FTD', 'data': [
    #                     web_tracker_obj['ftd'], mobile_tracker_obj['ftd'], 0, 0
    #                 ]
    #             }
    return [regis_d, clicks_d, ftd_d]

def get_affiliate_dashboard_stats(aff_obj, commission, from_date_time, to_date_time, site_ids_list):
    resp = {}
    
    camp_links = CampaignTrackerMapping.objects.filter(
            fk_affiliateinfo__in=aff_obj,
            fk_campaign__siteid__in=site_ids_list).select_related('fk_campaign', 'fk_affiliateinfo')
    data_list, currency = group_by_results(from_date_time, to_date_time, site_ids_list, 
                            camp_links, 'lifetime', 'euro', 'lifetime')
    if data_list:
        data_list = data_list[0]
    else:
        data_list = [0]*25

    gbp_data_list, currency = group_by_results(from_date_time, to_date_time, site_ids_list, 
                            camp_links, 'lifetime', 'gbp', 'lifetime')
    if gbp_data_list:
        gbp_data_list = gbp_data_list[0]
    else:
        gbp_data_list = [0]*25
    tracker_api_obj = get_cliks_data(aff_obj, from_date_time, to_date_time, site_ids_list)
    resp['lt_clicks'] = tracker_api_obj['clicks']
    resp['lt_registrations'] = data_list[3]
    resp['lt_ftd']= data_list[5]
    resp['lt_bonus'] = data_list[19] if data_list[19] else 0.00
    resp['lt_dep_amount'] = data_list[15] if data_list[15] else 0.00
    resp['ut_dep_amount'] = gbp_data_list[15] if gbp_data_list[15] else 0.00
    resp['lt_with_amount'] = data_list[16] if data_list[16] else 0.00
    resp['ut_with_amount'] = gbp_data_list[16] if gbp_data_list[16] else 0.00
    resp['revenue'] = data_list[20] if data_list[20] else 0.00
    resp['ut_revenue'] = gbp_data_list[20] if gbp_data_list[20] else 0.00
    resp['lt_earnings'] = data_list[24] if data_list[24] else 0.00
    resp['ut_earnings'] = gbp_data_list[24] if gbp_data_list[24] else 0.00
    return resp


def get_sub_affiliates_commission_earnings(aff_obj, from_date_time, to_date_time, site_ids_list, currency_sites):
    total_commission = 0.0
    total_commission_u = 0.0
    resp = {}
    for obj in aff_obj:
        commission = Affiliatecommission.objects.filter(fk_affiliateinfo=obj).select_related('fk_affiliateinfo')
        if commission:
            commission = commission[0]
        else:
            commission = Affiliatecommission.objects.filter(fk_affiliateinfo=obj.superaffiliate).select_related('fk_affiliateinfo')[0]
        obj = [obj]
        upload_tracker_obj = UploadAdvanced.objects.filter(fk_campaign__fk_affiliateinfo__in=obj,
                            date__gte=str(from_date_time).split(' ')[0],
                            date__lte=str(to_date_time).split(' ')[0],
                            siteid__in=site_ids_list,
                            processed=True).aggregate(ftd = Sum('ftd'),
                            registrations= Sum('registrations'),rate=Sum('rate'),
                            deposit=Sum('deposit'),chargeback=Sum('chargeback'),
                            cashout=Sum('cashout'),void=Sum('void'),
                            bonus=Sum('bonus'),revenue=Sum('revenue'), clicks=Sum('clicks'))
        post_tracker_obj = PostBackAdvanced.objects.filter(fk_campaign__fk_affiliateinfo__in=obj,
                            date__gte=str(from_date_time).split(' ')[0],
                            date__lte=str(to_date_time).split(' ')[0],
                            siteid__in=site_ids_list).aggregate(ftd = Sum('ftd'),
                            registrations= Sum('registrations'),
                            deposit=Sum('deposit'),chargeback=Sum('chargeback'),
                            cashout=Sum('cashout'),reversal=Sum('reversal'),
                            bonus=Sum('bonus'),revenue=Sum('revenue'), clicks=Sum('clicks'))
        player_post_tracker_obj = PostBackPlayer.objects.filter(
                                fk_campaign__fk_affiliateinfo__in=obj,
                                    date__gte=str(from_date_time).split(' ')[0],
                                    date__lte=str(to_date_time).split(' ')[0],
                                    siteid__in=site_ids_list).aggregate(ftd = Sum('ftd'),
                                    deposit=Sum('deposit'),
                                    cashout=Sum('cashout'),reversal=Sum('reversal'),
                                    bonus=Sum('bonuses'),revenue=Sum('revenue'))
        player_post_tracker_obj['cashout'] = player_post_tracker_obj['cashout'] if player_post_tracker_obj['cashout'] else 0.0 \
                                     - player_post_tracker_obj['reversal'] if player_post_tracker_obj['reversal'] else 0.0

        tracker_api_obj = get_cliks_data(obj, from_date_time, to_date_time, site_ids_list)

        gbp_player_api_tracker_obj = PPPlayerProcessed.objects.filter(fk_campaign__fk_affiliateinfo__in=obj,
                    date__gte=str(from_date_time).split(' ')[0],
                    date__lte=str(to_date_time).split(' ')[0],
                    siteid__in=currency_sites[u'\xa3']).aggregate(
                    deposit=Sum('deposit'),chargeback=Sum('chargeback'),
                    cashout=Sum('cashout'),void=Sum('void'),reversechargeback=Sum('reversechargeback'),
                    bonus=Sum('bonuses'),
                    sidegamesbets=Sum('sidegamesbets'),sidegameswins=Sum('sidegameswins'),
                    jackpotcontribution=Sum('jackpotcontribution'),revenue=Sum('revenue'))

        player_api_tracker_obj = PPPlayerProcessed.objects.filter(
                    fk_campaign__fk_affiliateinfo__in=obj,
                        date__gte=str(from_date_time).split(' ')[0],
                        date__lte=str(to_date_time).split(' ')[0],
                        siteid__in=currency_sites[u'\u20ac']).aggregate(
                        deposit=Sum('deposit'),chargeback=Sum('chargeback'),
                        cashout=Sum('cashout'),void=Sum('void'),reversechargeback=Sum('reversechargeback'),
                        bonus=Sum('bonuses'),
                        sidegamesbets=Sum('sidegamesbets'),sidegameswins=Sum('sidegameswins'),
                        jackpotcontribution=Sum('jackpotcontribution'),revenue=Sum('revenue'))
        
        tracker_obj = {}
        track_total_dicts = [upload_tracker_obj, post_tracker_obj, tracker_api_obj]
        for d in track_total_dicts:
            for k, v in d.iteritems():
                tracker_obj[k] = tracker_obj.get(k,0) + (v if v else 0)

        player_tracker_obj = {}
        track_total_dicts = [player_post_tracker_obj, player_api_tracker_obj]
        for d in track_total_dicts:
            for k, v in d.iteritems():
                player_tracker_obj[k] = player_tracker_obj.get(k,0) + (v if v else 0)


        resp['lt_registrations'] = tracker_obj['registrations'] if tracker_obj['registrations'] else 0
        
        resp['lt_clicks'] = tracker_obj['clicks'] if tracker_obj['clicks'] else 0
        
        resp['lt_ftd']= tracker_obj['ftd'] if tracker_obj['ftd'] else 0

        resp['lt_bonus'] = round(player_tracker_obj['bonus'], 2) if player_tracker_obj['bonus'] else 0.0
        resp['ut_bonus'] = upload_tracker_obj['bonus'] if upload_tracker_obj['bonus'] else 0
        
        resp['lt_dep_amount'] = round(player_tracker_obj['deposit'], 2) if player_tracker_obj['deposit'] else 0.0
        resp['ut_dep_amount'] = upload_tracker_obj['deposit'] if upload_tracker_obj['deposit'] else 0
        
        resp['lt_with_amount'] = round(player_tracker_obj['cashout'], 2) if player_tracker_obj['cashout'] else 0.0
        resp['ut_with_amount'] = upload_tracker_obj['cashout'] if upload_tracker_obj['cashout'] else 0

        resp['revenue'] = round(player_tracker_obj['revenue'], 2) if player_tracker_obj['revenue'] else 0.0
        resp['ut_revenue'] = upload_tracker_obj['revenue'] if upload_tracker_obj['revenue'] else 0

        resp['ut_revenue'] += gbp_player_api_tracker_obj['revenue'] if gbp_player_api_tracker_obj['revenue'] else 0

        # if commission.pocdeduction:
        #     resp['revenue'] = resp['revenue']*(0.85)
        #     resp['ut_revenue'] = resp['ut_revenue']*(0.85)
        cpa_t = 0
        rev_t = 0
        cpr_t = 0
        cpa_u = 0
        rev_u = 0
        cpr_u = 0
        if commission.revshare:
            rev_t = (resp['revenue'] * commission.revsharepercent)/100
            rev_u = (resp['ut_revenue'] * commission.revsharepercent)/100
        if commission.cpachoice and commission.criteriaacquisition == 'FTD':
            cpa_gbp_ftd_data = PPPlayerProcessed.objects.filter(fk_campaign__fk_affiliateinfo__in=obj,
                                siteid__in=currency_sites[u'\xa3'],
                                ftddate__gte=from_date_time,
                                ftddate__lte=to_date_time
                                ,deposit__gt=commission.criteriavalue).values('playerid').distinct()
            cpa_ftd_data = PPPlayerProcessed.objects.filter(siteid__in=currency_sites[u'\u20ac'],
                                fk_campaign__fk_affiliateinfo__in=obj,
                                ftddate__gte=from_date_time,
                                ftddate__lte=to_date_time
                                ,deposit__gt=commission.criteriavalue).values('playerid').distinct()
            cpa_t = len(cpa_ftd_data) * (commission.cpacommissionvalue) if commission.cpacommissionvalue else 0
            cpa_u = len(cpa_gbp_ftd_data) * (commission.cpacommissionvalue) if commission.cpacommissionvalue else 0
        total_commission = total_commission + cpa_t + rev_t
        total_commission_u = total_commission_u + cpa_u + rev_u
    return total_commission, total_commission_u

def chart_data(aff_obj, obj, commission, from_date_time, to_date_time, site_ids_list):
    resp = {}

    player_tracker_obj = PPPlayerProcessed.objects.filter(
                    fk_campaign__fk_affiliateinfo__in=obj,
                    date__gte=str(from_date_time).split(' ')[0],
                    date__lte=str(to_date_time).split(' ')[0],
                    siteid__in=site_ids_list).aggregate(revenue=Sum('revenue'))
    resp['revenue'] = round(player_tracker_obj['revenue'], 2) if player_tracker_obj['revenue'] else 0.0
    resp['date_data'] = []
    resp['area_chart'] = [{'name':'Revenue', 'data':[]}]
    resp['barchart'] = [{'name': 'Clicks', 'color': '#93c83d', 'data':[]},
                        {'name':'Registrations', 'color': '#7cb5ec', 'data': []},
                        {'name':'FTD','color':'#0b99d7', 'data': []}
                        ]
    dates_list = get_buss_days_list(from_date_time.date(), to_date_time.date())
    upload_track_obj = UploadAdvanced.objects.filter(fk_campaign__fk_affiliateinfo__in=obj,
                    siteid__in=site_ids_list,
                    processed=True).values('date').aggregate(ftd = Sum('ftd'),
                    registrations= Sum('registrations'),revenue=Sum('revenue'), clicks=Sum('clicks'))
    upload_track_obj = {i['date']:i for i in upload_track_obj}
    post_track_obj = PostBackAdvanced.objects.filter(fk_campaign__fk_affiliateinfo__in=obj,
                        siteid__in=site_ids_list).values('date').aggregate(ftd = Sum('ftd'),
                    registrations= Sum('registrations'),revenue=Sum('revenue'), clicks=Sum('clicks'))
    post_track_obj = {i['date']:i for i in post_track_obj}
    player_post_track_obj = PostBackPlayer.objects.filter(fk_campaign__fk_affiliateinfo__in=obj,
                        siteid__in=site_ids_list).values('date').aggregate(revenue=Sum('revenue'))
    player_post_track_obj = {i['date']:i for i in player_post_track_obj}
    api_clicks = CampaignClicksCount.objects.filter(
                        fk_campaign__fk_affiliateinfo__in=obj,
                        fk_campaign__fk_campaign__siteid__in=site_ids_list).values(
                            'date').annotate(Sum('clicks'))
    api_clicks = {i['date']:i for i in api_clicks}
    api_registrations = PPPlayerProcessed.objects.filter(
                            fk_campaign__fk_affiliateinfo__in=[obj], 
                            siteid__in=site_ids,
                            registrationdate__gte=from_date,
                            registrationdate__lte=to_date)
    registration_dict = {}
    player_list = []
    for i in api_registrations:
        if i.playerid not in player_list:
            if registration_dict.get(str(i.registrationdate).split(' ')[0]):
                registration_dict[str(i.registrationdate).split(' ')[0]] += 1
            else:
                ftd_dict[str(i.registrationdate).split(' ')[0]] = 1
            player_list.append(i.playerid)
    api_registrations = registration_dict
    api_ftds = PPPlayerProcessed.objects.filter(
                        fk_campaign__fk_affiliateinfo__in=[obj], 
                        siteid__in=site_ids,
                        ftddate__gte=from_date,
                        ftddate__lte=to_date,
                        deposit__gt=0
                        )
    ftd_dict = {}
    player_list = []
    for i in api_ftds:
        if i.playerid not in player_list:
            if ftd_dict.get(str(i.registrationdate).split(' ')[0]):
                ftd_dict[str(i.registrationdate).split(' ')[0]] += 1
            else:
                ftd_dict[str(i.registrationdate).split(' ')[0]] = 1
            player_list.append(i.playerid)
    api_ftds = ftd_dict
    player_tracker_obj = PPPlayerProcessed.objects.filter(
                            fk_campaign__fk_affiliateinfo__in=obj,
                            siteid__in=site_ids_list).values(
                            'date').aggregate(
                            revenue=Sum('revenue'))
    player_tracker_obj = {i['date']:i for i in player_tracker_obj}
    for i in dates_list:
        data_resp = {'date': i}
        if upload_track_obj.get(i):
            u_ftds = upload_track_obj[i]['ftd__sum']
            u_regs = upload_track_obj[i]['registrations__sum']
            u_clicks = 0
            # data_resp['revenue'] = player_tracker_obj['revenue'] if player_tracker_obj['revenue'] else 0.00
            u_revenue = upload_track_obj[i]['revenue__sum']
        if player_tracker_obj.get(i):
            ftds = api_ftds[i]['playerid__count']
            regs = api_registrations[i]['playerid__count']
            clicks = api_clicks[i]['playerid__count']
            revenue = player_tracker_obj[i]['revenue__sum']
        data_resp['ftd'] = ftds + u_ftds
        data_resp['registrations'] = u_regs + regs
        data_resp['clicks'] = 0 + clicks
        # data_resp['revenue'] = player_tracker_obj['revenue'] if player_tracker_obj['revenue'] else 0.00
        data_resp['revenue'] = u_revenue + revenue

        resp['barchart'][0]['data'].append(int(data_resp['clicks']))
        resp['barchart'][2]['data'].append(int(data_resp['ftd']))
        resp['barchart'][1]['data'].append(int(data_resp['registrations']))
        resp['area_chart'][0]['data'].append(round(data_resp['revenue'],2))


        # resp['date_data'].append(data_resp)
    resp['dates_list'] = dates_list
    resp['bar_dates_list'] = []
    for d in dates_list:
        d = datetime.strptime(d,'%Y-%m-%d')
        if len(resp['bar_dates_list']) == 0:
            resp['bar_dates_list'].append(d.strftime('%b') + ' \n ' + d.strftime('%d'))
        elif int(d.strftime('%d')) == 1:
            resp['bar_dates_list'].append(d.strftime('%b') + ' \n ' + d.strftime('%d'))
        elif int(d.strftime('%d')) == 2 and len(dates_list) >50:
            resp['bar_dates_list'].append(d.strftime('%b') + ' \n ' + d.strftime('%d'))
        elif int(d.strftime('%d')) == 3 and len(dates_list) >100:
            resp['bar_dates_list'].append(d.strftime('%b') + ' \n ' + d.strftime('%d')) 
        else:
            resp['bar_dates_list'].append(d.strftime('%d'))

    device_revenue = {'MOBILE': 0.00, 'WEB': 0.00, 'TABLET': 0.00}
    device_post_tracker_obj = PostBackPlayer.objects.filter(
                                fk_campaign__fk_affiliateinfo__in=obj,
                                date__gte=str(from_date_time).split(' ')[0],
                                date__lte=str(to_date_time).split(' ')[0],
                                siteid__in=site_ids_list).values('platform').annotate(sum=Sum('revenue'))

    device_tracker_obj = PPPlayerProcessed.objects.filter(
                                fk_campaign__fk_affiliateinfo__in=obj,
                                date__gte=str(from_date_time).split(' ')[0],
                                date__lte=str(to_date_time).split(' ')[0],
                                siteid__in=site_ids_list).values('platform').annotate(sum=Sum('revenue'))

    for track_r in device_post_tracker_obj:
        if device_revenue.get(track_r['platform']):
            device_revenue[track_r['platform']] = device_revenue[track_r['platform']] + track_r['sum']
        else:
            device_revenue[track_r['platform']] = track_r['sum']

    for track_r in device_tracker_obj:
        if device_revenue.get(track_r['platform']):
            device_revenue[track_r['platform']] = device_revenue[track_r['platform']] + track_r['sum']
        else:
            device_revenue[track_r['platform']] = track_r['sum']

    resp['device_data'] = [device_revenue['MOBILE'], device_revenue['WEB'], device_revenue['TABLET']]

    try:
        rev_abs = abs(resp['revenue'])
        resp['mobile_revenue'] = round(((device_revenue['Mobile'] - (resp['revenue']))/rev_abs)*100,2)
        resp['web_revenue'] = round(((device_revenue['Web'] - (resp['revenue']))/rev_abs)*100,2)
        resp['tablet_revenue'] = round(((device_revenue['Tablet'] - (resp['revenue']))/rev_abs)*100,2)
    except:
        print 'device report', device_revenue, resp['revenue']
    device_register = {'Mobile': 0, 'Web': 0, 'Tablet': 0}
    # device_post_register_obj = PostBackPlayer.objects.filter(
    #                             fk_campaign__fk_affiliateinfo__in=obj,
    #                             date__gte=str(from_date_time).split(' ')[0],
    #                             date__lte=str(to_date_time).split(' ')[0],
    #                             siteid__in=site_ids_list).values(
    #                             'platform').annotate(sum=Sum('registrations'))

    # device_register_obj = PPPlayerProcessed.objects.filter(
    #                             fk_campaign__fk_affiliateinfo__in=obj,
    #                             date__gte=str(from_date_time).split(' ')[0],
    #                             date__lte=str(to_date_time).split(' ')[0],
    #                             siteid__in=site_ids_list).values(
    #                             'platform').annotate(sum=Sum('registrations'))

    # for track_r in device_post_register_obj:
    #     if device_register.get(track_r['platform']):
    #         device_register[track_r['platform']] = device_register[track_r['platform']] + track_r['sum']
    #     else:
    #         device_register[track_r['platform']] = track_r['sum']
    # for track_r in device_register_obj:
    #     if device_register.get(track_r['platform']):
    #         device_register[track_r['platform']] = device_register[track_r['platform']] + track_r['sum']
    #     else:
    #         device_register[track_r['platform']] = track_r['sum']

    # try:
    #     rev_abs = abs(resp['revenue'])
    #     resp['mobile_register'] = round(((device_register['Mobile'] - (resp['lt_registrations']))/resp['lt_registrations'])*100,2)
    #     resp['web_register'] = round(((device_register['Desktop'] - (resp['lt_registrations']))/resp['lt_registrations'])*100,2)
    #     resp['tablet_register'] = round(((device_register['Tablet'] - (resp['lt_registrations']))/resp['lt_registrations'])*100,2)
    # except:
    #     print 'register report', device_register_obj
    return resp


def get_top_creatives(site_ids_list):
    main_list = []
    newlist = []
    if site_ids_list:
        campaigns_data = PPPlayerProcessed.objects.filter(
                            siteid__in=site_ids_list).values(
                            'siteid',
                            'fk_campaign__fk_campaign', 
                            'fk_campaign__fk_campaign__media_type'
                            ).annotate(Sum('revenue'))
        newlist = sorted(campaigns_data, key=lambda k: k['revenue__sum'], reverse=True)
    for i, val in enumerate(newlist):
        if len(main_list) > 5:
            continue
        campaign_obj = Campaign.objects.filter(id=val['fk_campaign__fk_campaign'])
        if not campaign_obj:
            continue
        campaign_obj = campaign_obj[0]
        size = ' '
        bannerid = ''
        b_image_link = ''
        if campaign_obj.media_type in ['B', 'SS', 'P']:
            if campaign_obj.media_type == 'B':
                media_type = 'Banner'
            elif campaign_obj.media_type == 'SS':
                media_type = 'Screenshot'
            else:
                media_type = 'Pagepeel'
            m_obj = Bannerlogs.objects.filter(fk_campaign=campaign_obj)
            if m_obj:
                size = str(m_obj[0].fk_bannerimages.width) + 'X' + str(m_obj[0].fk_bannerimages.height)
                bannerid = m_obj[0].fk_bannerimages.id
                b_image_link = m_obj[0].fk_bannerimages.banner.url
        elif campaign_obj.media_type == 'V':
            media_type = 'Video'
            m_obj = Videologs.objects.filter(fk_campaign=campaign_obj)
            if m_obj:
                bannerid = m_obj[0].fk_video.id
                b_image_link = m_obj[0].fk_video.video.url
        else:
            continue
        main_list.append([str(campaign_obj.siteid.name), str(media_type), size, str(campaign_obj.name), 
                        str(bannerid), str(campaign_obj.media_type),
                         str(b_image_link), str(campaign_obj.media_type)])
    return main_list


def get_affiliate_home_stats_changed(aff_obj, obj, commission, from_date_time, to_date_time, site_ids_list, currency):
    resp = {}
    
    camp_links = CampaignTrackerMapping.objects.filter(
            fk_affiliateinfo__in=obj,
            fk_campaign__siteid__in=site_ids_list).select_related('fk_campaign', 'fk_affiliateinfo')
    data_list, currency = group_by_results(from_date_time, to_date_time, site_ids_list, 
                            camp_links, 'lifetime', 'euro', 'lifetime')
    if data_list:
        data_list = data_list[0]
    else:
        data_list = [0]*25

    gbp_data_list, currency = group_by_results(from_date_time, to_date_time, site_ids_list, 
                            camp_links, 'lifetime', 'gbp', 'lifetime')
    if gbp_data_list:
        gbp_data_list = gbp_data_list[0]
    else:
        gbp_data_list = [0]*25
    tracker_api_obj = get_cliks_data(obj, from_date_time, to_date_time, site_ids_list)
    resp['lt_clicks'] = tracker_api_obj['clicks']
    resp['lt_registrations'] = data_list[3]
    resp['lt_ftd']= data_list[5]
    resp['lt_bonus'] = data_list[19] if data_list[19] else 0.00
    resp['lt_dep_amount'] = data_list[15] if data_list[15] else 0.00
    resp['ut_dep_amount'] = gbp_data_list[15] if gbp_data_list[15] else 0.00
    resp['lt_with_amount'] = data_list[16] if data_list[16] else 0.00
    resp['ut_with_amount'] = gbp_data_list[16] if gbp_data_list[16] else 0.00
    resp['revenue'] = data_list[20] if data_list[20] else 0.00
    resp['ut_revenue'] = gbp_data_list[20] if gbp_data_list[20] else 0.00
    resp['lt_earnings'] = data_list[24] if data_list[24] else 0.00
    resp['ut_earnings'] = gbp_data_list[24] if gbp_data_list[24] else 0.00
    resp['area_chart'] = [{'name':'Revenue', 'data':[]}]
    resp['barchart'] = [{'name': 'Clicks', 'color': '#93c83d', 'data':[]},
                        {'name':'Registrations', 'color': '#7cb5ec', 'data': []},
                        {'name':'FTD','color':'#0b99d7', 'data': []}
                        ]
    data_date_list, currency_d = group_by_results(from_date_time, to_date_time, site_ids_list, 
                            camp_links, 'daily', currency, 'conversion')
    resp['bar_dates_list'] = []
    dates_list = get_buss_days_list(from_date_time.date(), to_date_time.date())
    resp['dates_list'] = dates_list
    date_data_dict = {i[0] : i for i in data_date_list}
    for date_d in dates_list:
        resp['barchart'][0]['data'].append(int(date_data_dict[date_d][2]) if date_data_dict.get(date_d) else 0)
        resp['barchart'][2]['data'].append(int(date_data_dict[date_d][6]) if date_data_dict.get(date_d) else 0)
        resp['barchart'][1]['data'].append(int(date_data_dict[date_d][4]) if date_data_dict.get(date_d) else 0)
        # resp['area_chart'][0]['data'].append(round(data_resp['revenue'],2))

        # resp['date_data'].append(data_resp)
    resp['bar_dates_list'] = []
    for d in dates_list:
        d = datetime.strptime(d,'%Y-%m-%d')
        if len(resp['bar_dates_list']) == 0:
            resp['bar_dates_list'].append(d.strftime('%b') + ' \n ' + d.strftime('%d'))
        elif int(d.strftime('%d')) == 1:
            resp['bar_dates_list'].append(d.strftime('%b') + ' \n ' + d.strftime('%d'))
        elif int(d.strftime('%d')) == 2 and len(dates_list) >50:
            resp['bar_dates_list'].append(d.strftime('%b') + ' \n ' + d.strftime('%d'))
        elif int(d.strftime('%d')) == 3 and len(dates_list) >100:
            resp['bar_dates_list'].append(d.strftime('%b') + ' \n ' + d.strftime('%d')) 
        else:
            resp['bar_dates_list'].append(d.strftime('%d'))
    device_revenue = {'MOBILE': 0.00, 'WEB': 0.00, 'TABLET': 0.00}
    device_post_tracker_obj = PostBackPlayer.objects.filter(
                                fk_campaign__fk_affiliateinfo__in=obj,
                                date__gte=str(from_date_time).split(' ')[0],
                                date__lte=str(to_date_time).split(' ')[0],
                                siteid__in=site_ids_list).values('platform').annotate(sum=Sum('revenue'))

    device_tracker_obj = PPPlayerProcessed.objects.filter(
                                fk_campaign__fk_affiliateinfo__in=obj,
                                date__gte=str(from_date_time).split(' ')[0],
                                date__lte=str(to_date_time).split(' ')[0],
                                siteid__in=site_ids_list).values('platform').annotate(sum=Sum('revenue'))

    for track_r in device_post_tracker_obj:
        if device_revenue.get(track_r['platform']):
            device_revenue[track_r['platform']] = device_revenue[track_r['platform']] + track_r['sum']
        else:
            device_revenue[track_r['platform']] = track_r['sum']

    for track_r in device_tracker_obj:
        if device_revenue.get(track_r['platform']):
            device_revenue[track_r['platform']] = device_revenue[track_r['platform']] + track_r['sum']
        else:
            device_revenue[track_r['platform']] = track_r['sum']

    resp['device_data'] = [device_revenue['MOBILE'], device_revenue['WEB'], device_revenue['TABLET']]

    try:
        rev_abs = abs(resp['revenue_c'])
        resp['mobile_revenue'] = round(((device_revenue['Mobile'] - (resp['revenue']))/rev_abs)*100,2)
        resp['web_revenue'] = round(((device_revenue['Web'] - (resp['revenue']))/rev_abs)*100,2)
        resp['tablet_revenue'] = round(((device_revenue['Tablet'] - (resp['revenue']))/rev_abs)*100,2)
    except:
        print 'device report', device_revenue, str(resp['revenue'])
    device_register = {'MOBILE': 0, 'WEB': 0, 'TABLET': 0}
    device_post_register_obj = PostBackPlayer.objects.filter(
                                fk_campaign__fk_affiliateinfo__in=obj,
                                date__gte=str(from_date_time).split(' ')[0],
                                date__lte=str(to_date_time).split(' ')[0],
                                siteid__in=site_ids_list).values(
                                'platform').annotate(sum=Sum('registrations'))

    device_register_obj = PPPlayerProcessed.objects.filter(
                                fk_campaign__fk_affiliateinfo__in=obj,
                                date__gte=str(from_date_time).split(' ')[0],
                                date__lte=str(to_date_time).split(' ')[0],
                                siteid__in=site_ids_list).values(
                                'platform').annotate(sum=Sum('registrations'))

    for track_r in device_post_register_obj:
        if device_register.get(track_r['platform']):
            device_register[track_r['platform']] = device_register[track_r['platform']] + track_r['sum']
        else:
            device_register[track_r['platform']] = track_r['sum']
    for track_r in device_register_obj:
        if device_register.get(track_r['platform']):
            device_register[track_r['platform']] = device_register[track_r['platform']] + track_r['sum']
        else:
            device_register[track_r['platform']] = track_r['sum']

    try:
        rev_abs = abs(resp['revenue'])
        resp['mobile_register'] = round(((device_register['Mobile'] - (resp['lt_registrations']))/resp['lt_registrations'])*100,2)
        resp['web_register'] = round(((device_register['Web'] - (resp['lt_registrations']))/resp['lt_registrations'])*100,2)
        resp['tablet_register'] = round(((device_register['Tablet'] - (resp['lt_registrations']))/resp['lt_registrations'])*100,2)
    except:
        print 'register report', device_register_obj

    resp['cpr'] = 0.00
    resp['cpa'] = data_list[22]
    resp['rs'] = data_list[23]
    # resp['lt_earnings'] += resp['ut_earnings']
    resp_d = {(i):(format(resp[i], '.2f') if type(resp[i]) == float else resp[i]) for i in resp}

    resp = resp_d
    return resp

def get_affiliate_home_stats(aff_obj, obj, commission, from_date_time, to_date_time, site_ids_list):
    resp = {}
    currency_sites = {}
    currency_objs = Currency.objects.filter(siteid__in=site_ids_list)
    for cur in currency_objs:
        if cur.currency in currency_sites:
            currency_sites[cur.currency].append(cur.siteid)
        else:
            currency_sites[cur.currency] = [cur.siteid]
    if not currency_sites.get(u'\xa3'):
        currency_sites[u'\xa3'] = []
    if not currency_sites.get(u'\u20ac'):
        currency_sites[u'\u20ac'] = []
    upload_tracker_obj = UploadAdvanced.objects.filter(fk_campaign__fk_affiliateinfo__in=obj,
                        date__gte=str(from_date_time).split(' ')[0],
                        date__lte=str(to_date_time).split(' ')[0],
                        siteid__in=site_ids_list,
                        processed=True).aggregate(ftd = Sum('ftd'),
                        registrations= Sum('registrations'),rate=Sum('rate'),
                        deposit=Sum('deposit'),chargeback=Sum('chargeback'),
                        cashout=Sum('cashout'),void=Sum('void'),
                        bonus=Sum('bonus'),revenue=Sum('revenue'), clicks=Sum('clicks'))
    post_tracker_obj = PostBackAdvanced.objects.filter(fk_campaign__fk_affiliateinfo__in=obj,
                        date__gte=str(from_date_time).split(' ')[0],
                        date__lte=str(to_date_time).split(' ')[0],
                        siteid__in=site_ids_list).aggregate(ftd = Sum('ftd'),
                        registrations= Sum('registrations'),
                        deposit=Sum('deposit'),chargeback=Sum('chargeback'),
                        cashout=Sum('cashout'),reversal=Sum('reversal'),
                        bonus=Sum('bonus'),revenue=Sum('revenue'), clicks=Sum('clicks'))
    player_post_tracker_obj = PostBackPlayer.objects.filter(
                            fk_campaign__fk_affiliateinfo__in=obj,
                                date__gte=str(from_date_time).split(' ')[0],
                                date__lte=str(to_date_time).split(' ')[0],
                                siteid__in=site_ids_list).aggregate(ftd = Sum('ftd'),
                                deposit=Sum('deposit'),
                                cashout=Sum('cashout'),reversal=Sum('reversal'),
                                bonus=Sum('bonuses'),revenue=Sum('revenue'))
    player_post_tracker_obj['cashout'] = player_post_tracker_obj['cashout'] if player_post_tracker_obj['cashout'] else 0.0 \
                                 - player_post_tracker_obj['reversal'] if player_post_tracker_obj['reversal'] else 0.0

    tracker_api_obj = get_cliks_data(obj, from_date_time, to_date_time, site_ids_list)

    gbp_player_api_tracker_obj = PPPlayerProcessed.objects.filter(fk_campaign__fk_affiliateinfo__in=obj,
                    date__gte=str(from_date_time).split(' ')[0],
                    date__lte=str(to_date_time).split(' ')[0],
                    siteid__in=currency_sites[u'\xa3']).aggregate(
                    deposit=Sum('deposit'),chargeback=Sum('chargeback'),
                    cashout=Sum('cashout'),void=Sum('void'),reversechargeback=Sum('reversechargeback'),
                    bonus=Sum('bonuses'),
                    sidegamesbets=Sum('sidegamesbets'),sidegameswins=Sum('sidegameswins'),
                    jackpotcontribution=Sum('jackpotcontribution'),revenue=Sum('revenue'))

    player_api_tracker_obj = PPPlayerProcessed.objects.filter(
                    fk_campaign__fk_affiliateinfo__in=obj,
                    date__gte=str(from_date_time).split(' ')[0],
                    date__lte=str(to_date_time).split(' ')[0],
                    siteid__in=currency_sites[u'\u20ac']).aggregate(
                    deposit=Sum('deposit'),chargeback=Sum('chargeback'),
                    cashout=Sum('cashout'),void=Sum('void'),reversechargeback=Sum('reversechargeback'),
                    bonus=Sum('bonuses'),
                    sidegamesbets=Sum('sidegamesbets'),sidegameswins=Sum('sidegameswins'),
                    jackpotcontribution=Sum('jackpotcontribution'),revenue=Sum('revenue'))
    
    tracker_obj = {}
    track_total_dicts = [upload_tracker_obj, post_tracker_obj, tracker_api_obj]
    for d in track_total_dicts:
        for k, v in d.iteritems():
            tracker_obj[k] = tracker_obj.get(k,0) + (v if v else 0)

    player_tracker_obj = {}
    track_total_dicts = [player_post_tracker_obj, player_api_tracker_obj]
    for d in track_total_dicts:
        for k, v in d.iteritems():
            player_tracker_obj[k] = player_tracker_obj.get(k,0) + (v if v else 0)


    resp['lt_registrations'] = tracker_obj['registrations'] if tracker_obj['registrations'] else 0
    
    resp['lt_clicks'] = tracker_obj['clicks'] if tracker_obj['clicks'] else 0
    
    resp['lt_ftd']= tracker_obj['ftd'] if tracker_obj['ftd'] else 0

    resp['lt_bonus'] = round(player_tracker_obj['bonus'], 2) if player_tracker_obj['bonus'] else 0.0
    resp['ut_bonus'] = upload_tracker_obj['bonus'] if upload_tracker_obj['bonus'] else 0
    
    resp['ut_bonus'] += gbp_player_api_tracker_obj['bonus'] if gbp_player_api_tracker_obj['bonus'] else 0

    resp['lt_dep_amount'] = round(player_tracker_obj['deposit'], 2) if player_tracker_obj['deposit'] else 0.0
    resp['ut_dep_amount'] = upload_tracker_obj['deposit'] if upload_tracker_obj['deposit'] else 0
    
    resp['ut_dep_amount'] += gbp_player_api_tracker_obj['deposit'] if gbp_player_api_tracker_obj['deposit'] else 0

    resp['lt_with_amount'] = round(player_tracker_obj['cashout'], 2) if player_tracker_obj['cashout'] else 0.0
    resp['ut_with_amount'] = upload_tracker_obj['cashout'] if upload_tracker_obj['cashout'] else 0

    resp['ut_with_amount'] += gbp_player_api_tracker_obj['cashout'] if gbp_player_api_tracker_obj['cashout'] else 0

    resp['revenue'] = round(player_tracker_obj['revenue'], 2) if player_tracker_obj['revenue'] else 0.0
    resp['ut_revenue'] = upload_tracker_obj['revenue'] if upload_tracker_obj['revenue'] else 0

    resp['ut_revenue'] += gbp_player_api_tracker_obj['revenue'] if gbp_player_api_tracker_obj['revenue'] else 0

    resp['revenue'] = round(resp['revenue'], 2)

    # if commission.pocdeduction:
    #     resp['revenue'] = resp['revenue']*(0.85)
    #     resp['ut_revenue'] = resp['ut_revenue']*(0.85)

    cpa_t = 0
    rev_t = 0
    cpr_t = 0
    cpa_u = 0
    rev_u = 0
    cpr_u = 0
    if len(obj) == 1:
        if commission.revshare:
            rev_t = (resp['revenue'] * commission.revsharepercent)/100
            rev_u = (resp['ut_revenue'] * commission.revsharepercent)/100
        if commission.cpachoice and commission.criteriaacquisition == 'FTD':
            cpa_gbp_ftd_data = PPPlayerProcessed.objects.filter(fk_campaign__fk_affiliateinfo__in=obj,
                                siteid__in=currency_sites[u'\xa3'],
                                ftddate__gte=from_date_time,
                                ftddate__lte=to_date_time
                                ,deposit__gt=commission.criteriavalue).values('playerid').distinct()
            cpa_ftd_data = PPPlayerProcessed.objects.filter(fk_campaign__fk_affiliateinfo__in=obj,
                                siteid__in=currency_sites[u'\u20ac'],
                                ftddate__gte=from_date_time,
                                ftddate__lte=to_date_time
                                ,deposit__gt=commission.criteriavalue).values('playerid').distinct()
            cpa_t = len(cpa_ftd_data) * (commission.cpacommissionvalue) if commission.cpacommissionvalue else 0
            cpa_u = len(cpa_gbp_ftd_data) * (commission.cpacommissionvalue) if commission.cpacommissionvalue else 0
        if commission.referalchoice:
            sub_commission, sub_commission_u = get_sub_affiliates_commission_earnings(aff_obj, 
                from_date_time, to_date_time, site_ids_list, currency_sites)
            cpr_t = (sub_commission * commission.referalcommissionvalue)/100
            cpr_u = (sub_commission_u * commission.referalcommissionvalue)/100
    else:
        if commission.revshare:
            rev_t = (resp['revenue'] * commission.revsharepercent)/100
            rev_u = (resp['ut_revenue'] * commission.revsharepercent)/100
        if commission.cpachoice and commission.criteriaacquisition == 'FTD':
            cpa_ftd_data = PPPlayerProcessed.objects.filter(fk_campaign__fk_affiliateinfo__in=obj,
                                siteid__in=site_ids_list,
                                ftddate__gte=from_date_time,
                                ftddate__lte=to_date_time
                                ,deposit__gt=commission.criteriavalue).values('playerid').distinct()
            cpa_t = len(cpa_ftd_data) * (commission.cpacommissionvalue) if commission.cpacommissionvalue else 0
    
    resp['lt_earnings'] = round(rev_t + cpr_t + cpa_t, 2)
    resp['ut_earnings'] = round(rev_u + cpr_u + cpa_u, 2)

    resp['date_data'] = []
    resp['area_chart'] = [{'name':'Revenue', 'data':[]}]
    resp['barchart'] = [{'name': 'Clicks', 'color': '#93c83d', 'data':[]},
                        {'name':'Registrations', 'color': '#7cb5ec', 'data': []},
                        {'name':'FTD','color':'#0b99d7', 'data': []}
                        ]
    dates_list = get_buss_d_days_list(from_date_time.date(), to_date_time.date())
    upload_track_obj = UploadAdvanced.objects.filter(fk_campaign__fk_affiliateinfo__in=obj,
                    siteid__in=site_ids_list,
                    date__gte=from_date_time,
                    date__lte=to_date_time,
                    processed=True).values('date').annotate(Sum('ftd'),
                    Sum('registrations'),Sum('revenue'), Sum('clicks'))
    upload_track_obj = {i['date']:i for i in upload_track_obj if i }
    post_track_obj = PostBackAdvanced.objects.filter(fk_campaign__fk_affiliateinfo__in=obj,
                        siteid__in=site_ids_list,
                        date__gte=from_date_time,
                    date__lte=to_date_time).values('date').annotate(Sum('ftd'),
                    Sum('registrations'),Sum('revenue'), Sum('clicks'))
    post_track_obj = {i['date']:i for i in post_track_obj}
    player_post_track_obj = PostBackPlayer.objects.filter(fk_campaign__fk_affiliateinfo__in=obj,
                        siteid__in=site_ids_list,
                        date__gte=from_date_time,
                    date__lte=to_date_time).values('date').annotate(Sum('revenue'))
    player_post_track_obj = {i['date']:i for i in player_post_track_obj}
    api_clicks = CampaignClicksCount.objects.filter(
                        fk_campaign__fk_affiliateinfo__in=obj,
                        fk_campaign__fk_campaign__siteid__in=site_ids_list,
                        date__gte=from_date_time,
                        date__lte=to_date_time).values(
                            'date').annotate(Sum('clicks'))
    api_clicks = {i['date']:i for i in api_clicks}
    api_registrations = PPPlayerProcessed.objects.filter(
                        fk_campaign__fk_affiliateinfo__in=obj, 
                        fk_campaign__fk_campaign__siteid__in=site_ids_list,
                        registrationdate__gte=from_date_time,
                        registrationdate__lte=to_date_time).values(
                        'date').annotate(Count('playerid', 
                                 distinct=True))
    api_registrations = {i['date']:i for i in api_registrations}
    api_ftds = PPPlayerProcessed.objects.filter(
                        fk_campaign__fk_affiliateinfo__in=obj, 
                        fk_campaign__fk_campaign__siteid__in=site_ids_list,
                        ftddate__gte=from_date_time,
                        ftddate__lte=to_date_time,
                        deposit__gt=0
                        ).values('date').annotate(Count('playerid', 
                                 distinct=True))
    api_ftds = {i['date']:i for i in api_ftds}
    player_tracker_obj = PPPlayerProcessed.objects.filter(
                            fk_campaign__fk_affiliateinfo__in=obj,
                            siteid__in=site_ids_list,
                            date__gte=from_date_time,
                    date__lte=to_date_time).values(
                            'date').annotate(
                            Sum('revenue'))
    player_tracker_obj = {i['date']:i for i in player_tracker_obj}
    print 'date ftds', api_ftds, api_registrations, api_clicks
    for i in dates_list:
        data_resp = {'date': i}
        ftds, u_ftds, u_regs, u_revenue, u_clicks, clicks, revenue, regs = 0,0,0,0,0,0,0,0

        if upload_track_obj.get(i):
            u_ftds = upload_track_obj[i]['ftd__sum']
            u_regs = upload_track_obj[i]['registrations__sum']
            u_clicks = 0
            # data_resp['revenue'] = player_tracker_obj['revenue'] if player_tracker_obj['revenue'] else 0.00
            u_revenue = upload_track_obj[i]['revenue__sum']
        if player_tracker_obj.get(i):
            if api_ftds.get(i):
                ftds = api_ftds[i]['playerid__count']
            if api_registrations.get(i):
                regs = api_registrations[i]['playerid__count']
            if api_clicks.get(i):
                clicks = api_clicks[i]['clicks__sum']
            revenue = player_tracker_obj[i]['revenue__sum']
        data_resp['ftd'] = ftds + u_ftds
        data_resp['registrations'] = u_regs + regs
        data_resp['clicks'] = 0 + clicks
        # data_resp['revenue'] = player_tracker_obj['revenue'] if player_tracker_obj['revenue'] else 0.00
        data_resp['revenue'] = u_revenue + revenue

        resp['barchart'][0]['data'].append(int(data_resp['clicks']))
        resp['barchart'][2]['data'].append(int(data_resp['ftd']))
        resp['barchart'][1]['data'].append(int(data_resp['registrations']))
        resp['area_chart'][0]['data'].append(round(data_resp['revenue'],2))

        # resp['date_data'].append(data_resp)
    dates_list = get_buss_days_list(from_date_time.date(), to_date_time.date())
    resp['dates_list'] = dates_list
    resp['bar_dates_list'] = []
    for d in dates_list:
        d = datetime.strptime(d,'%Y-%m-%d')
        if len(resp['bar_dates_list']) == 0:
            resp['bar_dates_list'].append(d.strftime('%b') + ' \n ' + d.strftime('%d'))
        elif int(d.strftime('%d')) == 1:
            resp['bar_dates_list'].append(d.strftime('%b') + ' \n ' + d.strftime('%d'))
        elif int(d.strftime('%d')) == 2 and len(dates_list) >50:
            resp['bar_dates_list'].append(d.strftime('%b') + ' \n ' + d.strftime('%d'))
        elif int(d.strftime('%d')) == 3 and len(dates_list) >100:
            resp['bar_dates_list'].append(d.strftime('%b') + ' \n ' + d.strftime('%d')) 
        else:
            resp['bar_dates_list'].append(d.strftime('%d'))

    device_revenue = {'MOBILE': 0.00, 'WEB': 0.00, 'TABLET': 0.00}
    device_post_tracker_obj = PostBackPlayer.objects.filter(
                                fk_campaign__fk_affiliateinfo__in=obj,
                                date__gte=str(from_date_time).split(' ')[0],
                                date__lte=str(to_date_time).split(' ')[0],
                                siteid__in=site_ids_list).values('platform').annotate(sum=Sum('revenue'))

    device_tracker_obj = PPPlayerProcessed.objects.filter(
                                fk_campaign__fk_affiliateinfo__in=obj,
                                date__gte=str(from_date_time).split(' ')[0],
                                date__lte=str(to_date_time).split(' ')[0],
                                siteid__in=site_ids_list).values('platform').annotate(sum=Sum('revenue'))

    for track_r in device_post_tracker_obj:
        if device_revenue.get(track_r['platform']):
            device_revenue[track_r['platform']] = device_revenue[track_r['platform']] + track_r['sum']
        else:
            device_revenue[track_r['platform']] = track_r['sum']

    for track_r in device_tracker_obj:
        if device_revenue.get(track_r['platform']):
            device_revenue[track_r['platform']] = device_revenue[track_r['platform']] + track_r['sum']
        else:
            device_revenue[track_r['platform']] = track_r['sum']

    resp['device_data'] = [device_revenue['MOBILE'], device_revenue['WEB'], device_revenue['TABLET']]

    try:
        rev_abs = abs(resp['revenue'])
        resp['mobile_revenue'] = round(((device_revenue['Mobile'] - (resp['revenue']))/rev_abs)*100,2)
        resp['web_revenue'] = round(((device_revenue['Web'] - (resp['revenue']))/rev_abs)*100,2)
        resp['tablet_revenue'] = round(((device_revenue['Tablet'] - (resp['revenue']))/rev_abs)*100,2)
    except:
        print 'device report', device_revenue, resp['revenue']
    device_register = {'Mobile': 0, 'Web': 0, 'Tablet': 0}
    device_post_register_obj = PostBackPlayer.objects.filter(
                                fk_campaign__fk_affiliateinfo__in=obj,
                                date__gte=str(from_date_time).split(' ')[0],
                                date__lte=str(to_date_time).split(' ')[0],
                                siteid__in=site_ids_list).values(
                                'platform').annotate(sum=Sum('registrations'))

    device_register_obj = PPPlayerProcessed.objects.filter(
                                fk_campaign__fk_affiliateinfo__in=obj,
                                date__gte=str(from_date_time).split(' ')[0],
                                date__lte=str(to_date_time).split(' ')[0],
                                siteid__in=site_ids_list).values(
                                'platform').annotate(sum=Sum('registrations'))

    for track_r in device_post_register_obj:
        if device_register.get(track_r['platform']):
            device_register[track_r['platform']] = device_register[track_r['platform']] + track_r['sum']
        else:
            device_register[track_r['platform']] = track_r['sum']
    for track_r in device_register_obj:
        if device_register.get(track_r['platform']):
            device_register[track_r['platform']] = device_register[track_r['platform']] + track_r['sum']
        else:
            device_register[track_r['platform']] = track_r['sum']

    try:
        rev_abs = abs(resp['revenue'])
        resp['mobile_register'] = round(((device_register['Mobile'] - (resp['lt_registrations']))/resp['lt_registrations'])*100,2)
        resp['web_register'] = round(((device_register['Web'] - (resp['lt_registrations']))/resp['lt_registrations'])*100,2)
        resp['tablet_register'] = round(((device_register['Tablet'] - (resp['lt_registrations']))/resp['lt_registrations'])*100,2)
    except:
        print 'register report', device_register_obj

    resp['cpr'] = cpr_t + cpr_u
    resp['cpa'] = cpa_t + cpa_u
    resp['rs'] = rev_t + rev_u
    resp['lt_earnings'] += resp['ut_earnings']
    resp_d = {(i):(format(resp[i], '.2f') if type(resp[i]) == float else resp[i]) for i in resp}

    resp = resp_d
    return resp

def get_7_days_data(aff_obj, site_ids_list, currency_sites):
    today = datetime.now()
    last_7_day = datetime.now() - timedelta(days=7)
    last_14_day = last_7_day - timedelta(days=7)
    total_upload_tracker_obj = UploadAdvanced.objects.filter(fk_campaign__fk_affiliateinfo__in=aff_obj,
                                    date__gte=str(last_14_day).split(' ')[0],
                                    date__lte=str(last_7_day).split(' ')[0],
                                    siteid__in=site_ids_list,
                                    processed=True).aggregate(ftd = Sum('ftd'),
                                    registrations= Sum('registrations'),rate=Sum('rate'),
                                    deposit=Sum('deposit'),chargeback=Sum('chargeback'),
                                    cashout=Sum('cashout'),void=Sum('void'),
                                    bonus=Sum('bonus'),revenue=Sum('revenue'), clicks=Sum('clicks'))
    total_post_tracker_obj = PostBackAdvanced.objects.filter(fk_campaign__fk_affiliateinfo__in=aff_obj,
                        date__gte=str(last_14_day).split(' ')[0],
                        date__lte=str(last_7_day).split(' ')[0],
                        siteid__in=site_ids_list).aggregate(ftd = Sum('ftd'),
                        registrations= Sum('registrations'),
                        deposit=Sum('deposit'),chargeback=Sum('chargeback'),
                        cashout=Sum('cashout'),reversal=Sum('reversal'),
                        bonus=Sum('bonus'),revenue=Sum('revenue'), clicks=Sum('clicks'))
    total_post_tracker_obj['cashout'] = total_post_tracker_obj['cashout'] if total_post_tracker_obj['cashout'] else 0.0 \
                                 - total_post_tracker_obj['reversal'] if total_post_tracker_obj['reversal'] else 0.0
    player_total_post_tracker_obj = PostBackPlayer.objects.filter(fk_campaign__fk_affiliateinfo__in=aff_obj,
                                date__gte=str(last_14_day).split(' ')[0],
                                date__lte=str(last_7_day).split(' ')[0],
                                siteid__in=site_ids_list).aggregate(ftd = Sum('ftd'),
                                deposit=Sum('deposit'),
                                cashout=Sum('cashout'),reversal=Sum('reversal'),
                                bonus=Sum('bonuses'),revenue=Sum('revenue'))
    player_total_post_tracker_obj['cashout'] = player_total_post_tracker_obj['cashout'] if player_total_post_tracker_obj['cashout'] else 0.0 \
                                 - player_total_post_tracker_obj['reversal'] if player_total_post_tracker_obj['reversal'] else 0.0
    total_api_tracker_obj = get_cliks_data(aff_obj,
                        datetime.combine(last_14_day, time(0,0,0)),
                        datetime.combine(last_7_day, time(23,59,59)),
                        site_ids_list)
    total_gbp_tracker_obj = []
    total_gbp_tracker_obj = PPPlayerProcessed.objects.filter(fk_campaign__fk_affiliateinfo__in=aff_obj,
                    date__gte=str(last_14_day).split(' ')[0],
                    date__lte=str(last_7_day).split(' ')[0],
                    siteid__in=currency_sites[u'\xa3']).aggregate(
                    deposit=Sum('deposit'),chargeback=Sum('chargeback'),
                    cashout=Sum('cashout'),void=Sum('void'),reversechargeback=Sum('reversechargeback'),
                    bonus=Sum('bonuses'),
                    sidegamesbets=Sum('sidegamesbets'),sidegameswins=Sum('sidegameswins'),
                    jackpotcontribution=Sum('jackpotcontribution'),revenue=Sum('revenue'))

    player_total_api_tracker_obj = PPPlayerProcessed.objects.filter(fk_campaign__fk_affiliateinfo__in=aff_obj,
                    date__gte=str(last_14_day).split(' ')[0],
                    date__lte=str(last_7_day).split(' ')[0],
                    siteid__in=currency_sites[u'\u20ac']).aggregate(
                    deposit=Sum('deposit'),chargeback=Sum('chargeback'),
                    cashout=Sum('cashout'),void=Sum('void'),reversechargeback=Sum('reversechargeback'),
                    bonus=Sum('bonuses'),
                    sidegamesbets=Sum('sidegamesbets'),sidegameswins=Sum('sidegameswins'),
                    jackpotcontribution=Sum('jackpotcontribution'),revenue=Sum('revenue'))
    total_tracker_obj = {}
    track_total_dicts = [total_upload_tracker_obj, total_post_tracker_obj, total_api_tracker_obj]
    for d in track_total_dicts:
        for k, v in d.iteritems():
            total_tracker_obj[k] = total_tracker_obj.get(k,0) + (v if v else 0)
    
    player_total_tracker_obj = {}
    track_total_dicts = [player_total_api_tracker_obj, player_total_post_tracker_obj]
    for d in track_total_dicts:
        for k, v in d.iteritems():
            player_total_tracker_obj[k] = player_total_tracker_obj.get(k,0) + (v if v else 0)
    # total_tracker_obj = { k: total_tracker_obj.get(k, 0) if total_tracker_obj.get(k, 0) else 0 +
    #             total_post_tracker_obj.get(k, 0) if total_post_tracker_obj.get(k, 0) else 0
    #             for k in set(total_tracker_obj) | set(total_post_tracker_obj) }
    upload_7_tracker_obj = UploadAdvanced.objects.filter(fk_campaign__fk_affiliateinfo__in=aff_obj,
                                    date__gte=str(last_7_day).split(' ')[0],
                                    date__lte=str(today).split(' ')[0],
                                    siteid__in=site_ids_list,
                                    processed=True).aggregate(ftd = Sum('ftd'),
                                    registrations= Sum('registrations'),rate=Sum('rate'),
                                    deposit=Sum('deposit'),chargeback=Sum('chargeback'),
                                    cashout=Sum('cashout'),void=Sum('void'),
                                    bonus=Sum('bonus'),revenue=Sum('revenue'), clicks=Sum('clicks'))
    post_7_tracker_obj = PostBackAdvanced.objects.filter(fk_campaign__fk_affiliateinfo__in=aff_obj,
                        date__gte=str(last_7_day).split(' ')[0],
                        date__lte=str(today).split(' ')[0],
                        siteid__in=site_ids_list).aggregate(ftd = Sum('ftd'),
                        registrations= Sum('registrations'),
                        deposit=Sum('deposit'),chargeback=Sum('chargeback'),
                        cashout=Sum('cashout'),reversal=Sum('reversal'),
                        bonus=Sum('bonus'),revenue=Sum('revenue'), clicks=Sum('clicks'))
    post_7_tracker_obj['cashout'] = post_7_tracker_obj['cashout'] if post_7_tracker_obj['cashout'] else 0.0 \
                                 - post_7_tracker_obj['reversal'] if post_7_tracker_obj['reversal'] else 0.0
    player_7_post_tracker_obj = PostBackPlayer.objects.filter(fk_campaign__fk_affiliateinfo__in=aff_obj,
                                date__gte=str(last_7_day).split(' ')[0],
                                date__lte=str(today).split(' ')[0],
                                siteid__in=site_ids_list).aggregate(ftd = Sum('ftd'),
                                deposit=Sum('deposit'),
                                cashout=Sum('cashout'),reversal=Sum('reversal'),
                                bonus=Sum('bonuses'),revenue=Sum('revenue'))
    player_7_post_tracker_obj['cashout'] = player_7_post_tracker_obj['cashout'] if player_7_post_tracker_obj['cashout'] else 0.0 \
                                 - player_7_post_tracker_obj['reversal'] if player_7_post_tracker_obj['reversal'] else 0.0
    
    # tracker_7_api_obj = get_cliks_data(aff_obj, last_7_day, today, site_ids_list)
    tracker_7_api_obj = get_cliks_data(aff_obj,
            datetime.combine(last_7_day, time(0,0,0)),
            datetime.combine(today, time(23,59,59)),
            site_ids_list)

    gbp_7_tracker_obj = PPPlayerProcessed.objects.filter(fk_campaign__fk_affiliateinfo__in=aff_obj,
                    date__gte=str(last_7_day).split(' ')[0],
                    date__lte=str(today).split(' ')[0],
                    siteid__in=currency_sites[u'\xa3']).aggregate(
                    deposit=Sum('deposit'),chargeback=Sum('chargeback'),
                    cashout=Sum('cashout'),void=Sum('void'),reversechargeback=Sum('reversechargeback'),
                    bonus=Sum('bonuses'),
                    sidegamesbets=Sum('sidegamesbets'),sidegameswins=Sum('sidegameswins'),
                    jackpotcontribution=Sum('jackpotcontribution'),revenue=Sum('revenue'))

    player_7_api_tracker_obj = PPPlayerProcessed.objects.filter(fk_campaign__fk_affiliateinfo__in=aff_obj,
                    date__gte=str(last_7_day).split(' ')[0],
                    date__lte=str(today).split(' ')[0],
                    siteid__in=currency_sites[u'\u20ac']).aggregate(
                    deposit=Sum('deposit'),chargeback=Sum('chargeback'),
                    cashout=Sum('cashout'),void=Sum('void'),reversechargeback=Sum('reversechargeback'),
                    bonus=Sum('bonuses'),
                    sidegamesbets=Sum('sidegamesbets'),sidegameswins=Sum('sidegameswins'),
                    jackpotcontribution=Sum('jackpotcontribution'),revenue=Sum('revenue'))
    tracker_7_obj = {}
    track_total_dicts = [upload_7_tracker_obj, post_7_tracker_obj, tracker_7_api_obj]
    for d in track_total_dicts:
        for k, v in d.iteritems():
            tracker_7_obj[k] = tracker_7_obj.get(k,0) + (v if v else 0)

    player_7_tracker_obj = {}
    track_total_dicts = [player_7_post_tracker_obj, player_7_api_tracker_obj]
    for d in track_total_dicts:
        for k, v in d.iteritems():
            player_7_tracker_obj[k] = player_7_tracker_obj.get(k,0) + (v if v else 0)

    return total_tracker_obj, tracker_7_obj, player_total_tracker_obj, player_7_tracker_obj,\
              total_upload_tracker_obj, upload_7_tracker_obj, total_gbp_tracker_obj, gbp_7_tracker_obj

def get_affiliate_admin_home_stats(aff_obj, commission, from_date_time, to_date_time, site_ids_list, currency):
    resp = {}
    currency_sites = {}
    currency_objs = Currency.objects.filter(siteid__in=site_ids_list)
    for cur in currency_objs:
        if cur.currency in currency_sites:
            currency_sites[cur.currency].append(cur.siteid)
        else:
            currency_sites[cur.currency] = [cur.siteid]
    if not currency_sites.get(u'\xa3'):
        currency_sites[u'\xa3'] = []
    if not currency_sites.get(u'\u20ac'):
        currency_sites[u'\u20ac'] = []
    today = datetime.now()
    last_7_day = datetime.now() - timedelta(days=6)
    last_14_day = last_7_day - timedelta(days=6)
    # total_tracker_obj, tracker_7_obj,\
    #          player_total_tracker_obj,\
    #          player_7_tracker_obj, total_upload_tracker_obj,\
    #          upload_7_tracker_obj, total_gbp_tracker_obj,\
    #          gbp_7_tracker_obj = get_7_days_data(aff_obj, site_ids_list, currency_sites)

    camp_links = CampaignTrackerMapping.objects.filter(
            fk_affiliateinfo__in=aff_obj,
            fk_campaign__siteid__in=site_ids_list).select_related('fk_campaign', 'fk_affiliateinfo')
    data_list, currency_d = group_by_results(from_date_time, to_date_time, site_ids_list, 
                            camp_links, 'lifetime', currency, 'lifetime')
    if data_list:
        data_list = data_list[0]
    else:
        data_list = [0]*25

    data_7_list, currency_d = group_by_results(last_7_day, today, site_ids_list, 
                            camp_links, 'lifetime', currency, 'lifetime')
    if data_7_list:
        data_7_list = data_7_list[0]
    else:
        data_7_list = [0]*25
    data_total_list, currency_d = group_by_results(last_14_day, last_7_day, site_ids_list, 
                            camp_links, 'lifetime', currency, 'lifetime')
    if data_total_list:
        data_total_list = data_total_list[0]
    else:
        data_total_list = [0]*25

    tracker_api_obj = get_cliks_data(aff_obj, from_date_time, to_date_time, site_ids_list)
    # resp['lt_clicks'] = tracker_api_obj['clicks']
    # resp['lt_registrations'] = data_list[3]
    # resp['lt_ftd']= data_list[4]
    # resp['lt_bonus'] = data_list[19]
    # resp['lt_dep_amount'] = data_list[15]
    # resp['lt_with_amount'] = data_list[16]
    # resp['revenue'] = data_list[20]
    # resp['lt_earnings'] = data_list[24]

    resp['lt_registrations'] = data_7_list[3]
    
    resp['registrations_percent'] = round((float(resp['lt_registrations'])/float(data_total_list[3]))*100, 2) if float(data_total_list[3]) else 0.00
    
    resp['lt_clicks'] = data_7_list[1]

    resp['clicks_percent'] = round((float(resp['lt_clicks'])/float(data_total_list[1]))*100, 2) if float(data_total_list[1]) else 0.00
    
    resp['lt_ftd']= data_7_list[5]

    resp['ftd_percent'] = round((float(resp['lt_ftd'])/float(data_total_list[5]))*100, 2) if float(data_total_list[5]) else 0.00
    

    resp['lt_bonus'] = data_list[19] if data_list[19] else 0.00

    resp['lt_dep_amount'] = data_7_list[15] if data_7_list[15] else 0.00

    resp['dep_percent'] = round((float(resp['lt_dep_amount'])/float(data_total_list[15]))*100, 2) if float(data_total_list[15]) else 0.00

    resp['lt_with_amount'] = data_7_list[16] if data_7_list[16] else 0.00
    resp['lt_revenue'] = data_7_list[20] if data_7_list[20] else 0.00


    resp['revenue_percent'] = round((float(data_7_list[20])/float(data_total_list[20]))*100, 2) if float(data_total_list[20]) else 0.00

    cpa_t = 0.00
    rev_t = 0.00
    cpr_t = 0.00
    
    resp['lt_earnings'] = data_7_list[24] if data_7_list[24] else 0.00

    resp['earnings_percent'] = round((float(data_7_list[24])/float(data_total_list[24]))*100, 2) if float(data_total_list[24]) else 0.00

    resp_d = {(i):(format(resp[i], '.2f') if type(resp[i]) == float else resp[i]) for i in resp}

    resp = resp_d
    resp['date_data'] = []
    resp['area_chart'] = [{'name':'Revenue', 'data':[]}]
    resp['barchart'] = [{'name': 'Clicks', 'color': '#93c83d', 'data':[]},
                        {'name':'Registrations', 'color': '#7cb5ec', 'data': []},
                        {'name':'FTD','color':'#0b99d7', 'data': []}
                        ]
    data_date_list, currency_d = group_by_results(from_date_time, to_date_time, site_ids_list, 
                            camp_links, 'daily', currency, 'earnings')
    resp['bar_dates_list'] = []
    dates_list = get_buss_days_list(from_date_time.date(), to_date_time.date())
    resp['dates_list'] = dates_list
    date_data_dict = {i[0] : i for i in data_date_list}
    for date_d in dates_list:
        resp['barchart'][0]['data'].append(int(date_data_dict[date_d][2]) if date_data_dict.get(date_d) else 0)
        resp['barchart'][2]['data'].append(int(date_data_dict[date_d][6]) if date_data_dict.get(date_d) else 0)
        resp['barchart'][1]['data'].append(int(date_data_dict[date_d][4]) if date_data_dict.get(date_d) else 0)
        # resp['area_chart'][0]['data'].append(round(date_data_dict[date_d][20],2))

        # resp['date_data'].append(data_resp)
    resp['bar_dates_list'] = []
    for d in dates_list:
        d = datetime.strptime(d,'%Y-%m-%d')
        if len(resp['bar_dates_list']) == 0:
            resp['bar_dates_list'].append(d.strftime('%b') + ' \n ' + d.strftime('%d'))
        elif int(d.strftime('%d')) == 1:
            resp['bar_dates_list'].append(d.strftime('%b') + ' \n ' + d.strftime('%d'))
        elif int(d.strftime('%d')) == 2 and len(dates_list) >50:
            resp['bar_dates_list'].append(d.strftime('%b') + ' \n ' + d.strftime('%d'))
        elif int(d.strftime('%d')) == 3 and len(dates_list) >100:
            resp['bar_dates_list'].append(d.strftime('%b') + ' \n ' + d.strftime('%d')) 
        else:
            resp['bar_dates_list'].append(d.strftime('%d'))

    resp['cpr'] = cpr_t
    resp['cpa'] = data_list[22]
    resp['rs'] = data_list[23]

    
    return resp


def check_and_create_affiliate_details(network, sitenames, username, password, email, contactname, companyname, address, address2, city,
                                      state, country, ipaddress, postcode, telephone, refid, lastname, ap_obj, options, dob=None):
    error_list = ["", "None", None]
    has_error = True
    aff_obj = "Please enter valid "

    if 'Email Address' in options:
        if username in error_list:
            aff_obj += "username."
            return (has_error, aff_obj)
    
    if 'Email Address' in options:
        if email in error_list:
            aff_obj += "email."
            return (has_error, aff_obj)
    
    if 'Password' in options:
        if password in error_list:
            aff_obj += "password."
            return (has_error, aff_obj)
    
    if 'First Name' in options:
        if contactname in error_list:
            aff_obj += "contactname."
            return (has_error, aff_obj)

    if 'Last Name' in options:
        if lastname in error_list:
            aff_obj += "lastname."
            return (has_error, aff_obj)

    if 'Company Name' in options:
        if companyname in error_list:
            aff_obj += "companyname."
            return (has_error, aff_obj)
    if 'Address 1' in options:
        if address in error_list:
            aff_obj += "address."
            return (has_error, aff_obj)

    if 'City' in options:
        if city in error_list:
            aff_obj += "city."
            return (has_error, aff_obj)

    if 'State' in options:
        if state in error_list:
            aff_obj += "state."
            return (has_error, aff_obj)

    if 'Country' in options:
        if country in error_list:
            aff_obj += "country."
            return (has_error, aff_obj)

    if ipaddress in error_list:
        return (has_error, aff_obj)

    ref_obj = None
    if refid and refid not in error_list:
        ref_obj = Affiliateinfo.objects.get(id=refid)
    if 'Postcode' in options:
        if postcode in error_list:
            aff_obj += "postcode."
            return (has_error, aff_obj)
    if 'Phone Number' in options:
        if telephone in error_list:
            aff_obj += "."
            return (has_error, aff_obj)
        try:
            telephone  = int(telephone)
        except:
            return (has_error, "Please enter valid telephone Number.")
    # try:
    #     postcode  = int(postcode)
    # except:
    #     postcode = postcode
        
    
    try:
        dob_list = dob.split("-")
        d = int(dob_list[0])
        m = int(dob_list[1])
        y = int(dob_list[2])
        dob = date(y,m,d)
    except:
        dob = datetime.now().date()

    mailvalidation = is_valid_email(email)
    if mailvalidation is False:
        aff_obj += "email."
        return (has_error, aff_obj)
    password = enc_password(password)   #   hash code of password 
    # mailidcount = Affiliateinfo.objects.filter(email=email).count()
    if ap_obj:
        acmanager = AccountManager.objects.filter(affiliateprogramme=ap_obj)[0]
    else:
        acmanager = AccountManager.objects.all()[0]
    mailidcount = Affiliateinfo.objects.filter(email=email, account_manager=acmanager).count()
    if mailidcount == 0:   # if Email is not registered
        pass
    else:
        aff_obj = 'Email address is Already Registered with us.'
        return (has_error, aff_obj)
    try:
        aff_obj = Affiliateinfo.objects.create(username=contactname, password=password, contactname=contactname,
                                               companyname=companyname, email=email, address=address, city=city,
                                               state=state, country=country, ipaddress=ipaddress,
                                               registeredon=datetime.now(), lastname=lastname,
                                               address2 = address2,
                                               postcode=postcode, telephone=telephone,
                                                dob=dob, account_manager=acmanager)
        # for i in sitenames:
        #     siteobj = Site.objects.get(domain=i)
        for siteobj in acmanager.siteid.all():
            aff_obj.siteid.add(siteobj)
            aff_obj.save()
        networkid_obj = Network.objects.all()
        for nt in networkid_obj:
            aff_obj.networkid.add(nt)
        if ref_obj:
            aff_obj.superaffiliate = ref_obj
            aff_obj.save()
        trigger_obj = MessageTrigger.objects.filter(
                                triggercondition='REGISTRATION', status=True)
        if trigger_obj:
            try:
                registrationmail(aff_obj, ap_obj)
            except Exception as e:
                error_log()
                print 'email not sent: ', str(e)
            trigger_obj = trigger_obj[0]
            # add_message_logs(aff_obj, trigger_obj, aff_obj.account_manager.email, email)
        #Affiliatecommission.objects.create(fk_affiliateinfo=aff_obj, revsharepercent=0, cpacommissionvalue=0, referalcommissionvalue=0)
    except Exception as e:
        error_log()
        return (has_error, str(e))
    return (False, aff_obj)

def validate_and_create_affiliate_payment_deatils(aff_obj, accountnumber, ifsccode,
                                                  accounttype, accountname, bankname, bankaddress,
                                                  paymenttype, chequepayableto):
    if paymenttype == "wire":
        if accountnumber == '':   #  account number can not be empty.
            pass
        else:
            Wiretransfer.objects.create(fk_affiliateinfo=aff_obj, accountnumber=accountnumber, ifsccode=ifsccode,
                                                accounttype=accounttype, accountname=accountname, bankname=bankname,
                                            bankaddress=bankaddress)
    if paymenttype == "cheque":
        try:
            Chequedetails.objects.create(fk_affiliateinfo=aff_obj, chequepayableto=chequepayableto)
        except Exception as e:
            error_log()

def validate_and_create_affiliate_commission(aff_obj, rscheck, cpacheck, cprcheck, rspercent, cpapercent, cprpercent):
    try:
        affcomm = Affiliatecommission.objects.get(fk_affiliateinfo=aff_obj)
        if rscheck == "on":
            affcomm.revshare = True
            affcomm.revsharepercent = rspercent if rspercent else 0
        if cpacheck == "on":
            affcomm.cpachoice = True
            affcomm.cpacommissionvalue = cpapercent if cpapercent else 0
        if cprcheck == "on":
            affcomm.referalchoice = True
            affcomm.referalcommissionvalue = cprpercent if cprpercent else 0
        affcomm.save()
    except Exception as e:
        error_log()
        

def get_affiliate_campaign_link(campaign_obj, aff_obj=None, parameters_dict=None, abs_url=None):
    if not parameters_dict:
        parameters_dict = {}
        parameters_dict["camp_id"] = campaign_obj.id
    linkurl = ""
    from urllib import urlencode, quote
    try:
        linkid = str(campaign_obj.id)
        if abs_url:
            linkurl = abs_url + '/affiliate/redirect?cid='+linkid
        else:
            linkurl = 'http://affiliates.fozilpartners.com/affiliate/redirect?cid='+linkid
        if aff_obj:
            aff_id = str(aff_obj.id)
            linkurl = linkurl + '&aid='+ str(aff_id)
        #linkurl = 'https://www.%s/affiliateregistration/%s/%s/'%(domain, affiliateid,  linkid)
        #aff_comm = Affiliatecommission.objects.get(fk_affiliateinfo=campaign_obj.fk_affiliateinfo)
        #utm_medium = "cpc" if aff_comm.cprchoice and aff_comm.cprcommissionvalue != 0 else "banner"
        #utm_source = campaign_obj.fk_affiliateinfo.username
        #utm_campaign = campaign_obj.name
        #params_str = "utm_source=%s&utm_medium=%s&"\
        #        "utm_campaign=%s&utm_term={keyword}&utm_content=signup"%(utm_source, utm_medium, utm_campaign)
        #linkurl = "%s?%s" % (linkurl, params_str)
    except Exception as e:
        error_log()
        print e
    return linkurl

def validate_and_create_campaign(aff_obj, linkname, domain, media_list, vcomm=None):
    sites_obj = aff_obj.siteid.all().order_by('id')
    has_error = True
    if not vcomm:
        if not linkname or linkname == "":
            error_message = "Please enter valid Campaign Name"
            return (has_error, error_message)

        if len(linkname) < 3 or len(linkname) > 25:
            error_message = "Campaign name should be 3-25 characters."
            return (has_error, error_message)
    if domain:
        try:
            selected_site_obj = sites_obj.get(domain=domain)
        except:
            selected_site_obj = Site.objects.get(domain=domain)
    else:
        try:
            selected_site_obj = sites_obj[0]
        except:
            selected_site_obj = Site.objects.all()[0]
    # if not domain:
    #     error_message = "Please select valid brand"
    #     return (has_error, error_message)
    camp_obj_count = Campaign.objects.filter(fk_affiliateinfo=aff_obj, siteid=selected_site_obj).count()
    if aff_obj.campaignlimit and camp_obj_count > aff_obj.campaignlimit:
        error_message = 'You can only create '+str(aff_obj.campaignlimit)+' campaigns. Please contact support to raise the limit.'
        return (has_error, error_message)

    linkobj_count = Campaign.objects.filter(name=linkname, fk_affiliateinfo=aff_obj).count()
    if linkobj_count > 0:
        error_message = 'The Campaign name is already registered.'
        return (has_error, error_message)
    
    network_obj = NetworkAndSiteMap.objects.get(siteid=selected_site_obj)
    if not media_list:
        linkobj = Campaign.objects.create(fk_affiliateinfo=aff_obj, name=linkname, siteid=selected_site_obj,
                                      networkid=network_obj.networkid)
    else:
        media_type = media_list[0]
        language = media_list[1]
        category = media_list[2]
        linkobj = Campaign.objects.create(fk_affiliateinfo=aff_obj, name=linkname, siteid=selected_site_obj,
                                      networkid=network_obj.networkid,
                                      media_type=media_type, language=language)
    
    linkurl = get_affiliate_campaign_link(linkobj, aff_obj)
    linkobj.link = linkurl
    linkobj.name = linkname
    linkobj.save()
    return (False, linkobj)

def add_post_back_campaign_tracker(aff_obj, camp_obj, netw_site_obj):
    camp_track_map, created = CampaignTrackerMapping.objects.get_or_create(fk_campaign=camp_obj, fk_affiliateinfo=aff_obj)
    if created:
        url = netw_site_obj.siteid.domain + '/?affid='+str(aff_obj.id) + '&cid='+ str(camp_obj.id)
        #url = 'https://www.progressplay.net/stickyslots.aspx?tracker=' + str(trackid)
        # camp_track_map.trackerid = trackid
        camp_track_map.goalid = 2
        camp_track_map.url = url
        # camp_track_map = CampaignTrackerMapping.objects.create(fk_campaign=camp_obj,
        #                     trackerid=trackid, fk_affiliateinfo=aff_obj, url=url)
        camp_track_map.save()
    return camp_track_map

def create_new_url_tracker(trackerid, camp_obj, aff_obj):
    site_level_map = SiteWhitelabelMapping.objects.get(siteid = camp_obj.siteid)
    camp_track_map, created = CampaignTrackerMapping.objects.get_or_create(fk_campaign=camp_obj, fk_affiliateinfo=aff_obj)
    url = site_level_map.sp_track_url + str(trackerid) + '&dynamic=aid'+ str(aff_obj.id) + 'cid'+ str(camp_obj.id)
    #url = 'https://www.progressplay.net/stickyslots.aspx?tracker=' + str(trackid)
    camp_track_map.trackerid = trackerid
    camp_track_map.url = url
    camp_track_map.goalid = 1
    camp_track_map.save()
    return camp_track_map


def add_campaign_tracker(aff_obj, camp_obj):
    try:
        camp_track_map = CampaignTrackerMapping.objects.filter(fk_campaign=camp_obj, fk_affiliateinfo=aff_obj)
        if camp_track_map:
            return (False, camp_track_map[0])
        netw_site_obj = NetworkAndSiteMap.objects.get(siteid=camp_obj.siteid)
        connect_type = netw_site_obj.networkid.connect_type
        _d = datetime.now().date()
        if connect_type == 'POST_BACK':
            camp_track_map = add_post_back_campaign_tracker(aff_obj, camp_obj, netw_site_obj)
            return (False, camp_track_map)
        site_level_map = SiteWhitelabelMapping.objects.get(siteid = camp_obj.siteid)
        if connect_type in ['UPLOAD', 'FTP']:
            camp_trackid_obj = CampaignTrackerMapping.objects.filter(fk_campaign__siteid=camp_obj.siteid)
            if not camp_trackid_obj:
                trackid = site_level_map.start_limit
            else:
                trackid_obj = camp_trackid_obj.aggregate(Max('trackerid'))
                trackid = trackid_obj['trackerid__max'] + 1
            camp_track_map, created = CampaignTrackerMapping.objects.get_or_create(fk_campaign=camp_obj, fk_affiliateinfo=aff_obj)
            if created:
                # if connect_type == 'UPLOAD':
                if netw_site_obj.networkid.networkname == 'Nektan':
                    url = site_level_map.sp_track_url
                else:
                    url = site_level_map.sp_track_url + str(trackid)
                # else:
                #     url = site_level_map.sp_track_url
                #url = 'https://www.progressplay.net/stickyslots.aspx?tracker=' + str(trackid)
                camp_track_map.trackerid = trackid
                camp_track_map.url = url
                camp_track_map.goalid = 3
                camp_track_map.save()
        else:
            trackid = site_level_map.start_limit
            camp_track_map, created = CampaignTrackerMapping.objects.get_or_create(fk_campaign=camp_obj, fk_affiliateinfo=aff_obj)
            if created:
                if netw_site_obj.networkid.networkname == 'ProgressPlay':
                    # if 'vcommsubaff' in aff_obj.username.lower() and camp_obj.siteid.id==1:
                    #     trackid = 730500
                    url = site_level_map.sp_track_url + str(trackid) #+ '&dynamic=aid'+ str(aff_obj.id) + 'cid'+ str(camp_obj.id)
                    camp_track_map.goalid = 1
                elif netw_site_obj.networkid.networkname == 'YayaGaming':
                    url = site_level_map.sp_track_url
                    camp_track_map.goalid = 5
                else:
                    url = site_level_map.sp_track_url + 'EP_00110000100000'
                    camp_track_map.goalid = 4
                #url = 'https://www.progressplay.net/stickyslots.aspx?tracker=' + str(trackid)
                camp_track_map.trackerid = trackid
                camp_track_map.url = url
                camp_track_map.save()
            # else:
            #     return (True, 'Tracker limit exceed. Will get back to you.')
    except Exception as e:
        return (True, str(e))
        print str(e)
    return (False, camp_track_map)

def get_total(data_list):
    final_list = []
    total_list = [x for x in zip(*data_list)][2:]
    print 'check sub aff', total_list
    for t in total_list:
        if '.' in str(sum(t)):
            final_list.append(str(sum(t)))
        else:
            final_list.append(sum(t))
    #total_list = [str(sum(t)) if '.' in str(sum(t)) else sum(t) for t in total_list]
    final_list.insert(0,'Total')
    final_list.insert(1, '')
    return final_list

def is_float(s):
    """ Returns True is string is a number. """
    try:
        float(s)
        return True
    except ValueError:
        return False

def activity_admin_report_stats(aff_objs, from_date, to_date, sites, currencyfilter):
    data_list = []
    currency = ''
    header_list = [
           { "title": "Affiliate", "width":"200px"},
           # { "title": "Email", "width":"350px"},
           { "title": "Clicks", "width":"80px"},
           { "title": "Registrations", "width":"80px"},
           { "title": "FTD", "width":"80px"},
           { "title": "Deposits", "width":"160px"},
           # { "title": "Cashout", "width":"160px"},
           { "title": "Revenue", "width":"160px"},
           ]
    # for i in camp_links:
    #     currency = currency_dict[i.fk_campaign.siteid]
    camp_links = CampaignTrackerMapping.objects.filter(
            fk_affiliateinfo__in=aff_objs,
            fk_campaign__siteid__in=sites).select_related('fk_campaign', 'fk_affiliateinfo')
    data_list, currency = group_by_results(from_date, to_date, sites, camp_links, 'affiliate',
                 currencyfilter, 'affiliate_admin')
    data_list.sort(key=lambda x: float(x[5].replace(u'\xa3','').replace(u'\u20ac','')), reverse=True)
    return (header_list, data_list[:5], currency)


def activity_report_stats(aff_obj, obj, commission, from_date, to_date, sites):
    data_list = []
    # commission = Affiliatecommission.objects.filter(fk_affiliateinfo__in=obj)
    # commission = commission[0]
    currency = ''
    header_list = [
           { "title": "Brand", "width":"220px"},
           { "title": "Deposits", "width":"200px"},
           # { "title": "Bonus", "width":"120px"},
           { "title": "Revenue", "width":"200px"},
           { "title": "CPA Commission", "width":"200px"},
           # { "title": "CPR Commission", "width":"200px"},
           { "title": "Revshare Commission", "width":"220px"},
           { "title": "Total Commission", "width":"180px"},
           ]
    camp_links = CampaignTrackerMapping.objects.filter(
            fk_affiliateinfo__in=obj,
            fk_campaign__siteid__in=sites).select_related('fk_campaign', 'fk_affiliateinfo')
    data_list, currency = group_by_results(from_date, to_date, sites, camp_links, 'brand',
                 'euro', 'brand_affiliate')
    return (header_list, data_list, currency)


def process_campaign_list(obj, from_date, to_date, camp_links, site_ids):
    data_list = []
    header_list = [
           { "title": "ID", "width":"180px"},
           { "title": "Brand", "width":"180px"},
           { "title": "Thumbnail", "width":"250px"},
           { "title": "Campaign Name", "width":"180px"},
           { "title": "Created On", "width":"180px"},
           { "title": "Media Type", "width":"180px"},
           # { "title": "media_category", "width":"180px"},
           { "title": "Language", "width":"160px"},
           { "title": "Get Code", "width":"220px"},
           ]
    lang_selector = {'English':'en',
                    'Spanish':'es',
                    'Finnish':'fi',
                    'German':'de'}
    for i in camp_links:
        imagelink = ''
        lang = None
        if i.media_type == 'V':
            videologobj = Videologs.objects.filter(fk_campaign=i)
            if videologobj:
                imagelink = videologobj[0].fk_video.video.url if videologobj[0].fk_video.video else ''
                lang = videologobj[0].fk_video.language
        elif i.media_type in ['SS', 'P', 'B']:
            bannerlogobj = Bannerlogs.objects.filter(fk_campaign=i)
            if bannerlogobj:
                imagelink = bannerlogobj[0].fk_bannerimages.banner.url if bannerlogobj[0].fk_bannerimages.banner else ''
                lang = bannerlogobj[0].fk_bannerimages.language
        elif i.media_type == 'LP':
            lpagelogobj = LandingPagelogs.objects.filter(fk_campaign=i)
            if lpagelogobj:
                lang = lpagelogobj[0].fk_lpage.language
                # imagelink = lpagelogobj[0].link
        # bannerlogobj = Bannerlogs.objects.filter(fk_campaign=i)
        # if bannerlogobj:
        #     imagelink = bannerlogobj[0].fk_bannerimages.banner.url
        tracker = i.name
        network_s_obj = NetworkAndSiteMap.objects.filter(siteid=i.siteid)
        url_t = network_s_obj[0].logo.url if network_s_obj else ''
        if lang:
            lang = lang_selector[lang]
            url_builder = '<button class="btn btn-purple" bannerlink="'+str(imagelink)+'" mediatype="'+str(i.media_type)+'" onclick="getcode(this)" hrefs="/affiliate/gettrackerlink/?dlang='+str(lang)+'&camp_id='+str(i.id)+'&aff_id='+str(obj.id) +'" target="_blank">Get code</button>'
        else:
            url_builder = '<button class="btn btn-purple" bannerlink="'+str(imagelink)+'" mediatype="'+str(i.media_type)+'" onclick="getcode(this)" hrefs="/affiliate/gettrackerlink/?camp_id='+str(i.id)+'&aff_id='+str(obj.id) +'" target="_blank">Get code</button>'
        image_thumbnail = '<img id="'+str(i.siteid.id)+'" title="Banner Image" class="img-responsive maxhight" src="'+url_t+'">'
        lang_select = '<select class="selectpicker" name="status" style="display:block !important" >\
                            <option value="">Default</option><option value="en">English</option><option value="de">German</option>\
                            <option value="sv">Swedish</option></select>'
        aff_data = [i.id, i.siteid.name, image_thumbnail,
                     tracker,
                     i.created_timestamp.strftime("%d-%m-%Y"), i.get_media_type_display(),
                      lang_select, url_builder]
        data_list.append(aff_data)
    return (header_list, data_list)


def Process_statement_list(obj):
    data_list = []
    header_list = [
           { "title": "Current Balance", "width":"170px"},
           { "title": "Month", "width":"200px"},
           { "title": "Brand", "width":"120px"},
           { "title": "Deal", "width":"120px"},
           { "title": "Registration", "width":"200px"},
           { "title": "FTDs", "width":"120px"},
           { "title": "NetCash", "width":"120px"},
           { "title": "CPA Earnings", "width":"200px"},
           { "title": "RevShare Earnings", "width":"200px"},
           { "title": "Total Earnings", "width":"180px"},
           ]
    return (header_list, data_list)

def Process_payment_list(obj):
    data_list = []
    header_list = [
           { "title": "Payment Date", "width":"170px"},
           { "title": "Opening Balance", "width":"200px"},
           { "title": "Adjustment", "width":"120px"},
           { "title": "Closing Balance", "width":"200px"},
           { "title": "Additional Incentive", "width":"220px"},
           { "title": "Approved/ Carried over", "width":"200px"},
           { "title": "Payment Method", "width":"180px"},
           ]
    return (header_list, data_list)


def Process_affiliate_list(obj, sub_aff_list):
    data_list = []
    header_list = [
           { "title": "Affiliate ID", "width":"170px"},
           { "title": "Affiliate name", "width":"200px"},
           { "title": "Email", "width":"120px"},
           { "title": "Registration Date", "width":"200px"},
           { "title": "Status", "width":"120px"},
           ]
    for i in sub_aff_list:
        aff_data = [i.id, i.username.replace('vcommsubaff', ''), str(i.email), 
                    i.registeredon.strftime("%d-%m-%Y") if i.registeredon else '',
                    i.status]
        data_list.append(aff_data)
    return (header_list, data_list)


def get_aff_earnings(total_d, aff_objs):
    aff_list_ids = [i.id for i in aff_objs]
    # cache_dict = cache.get(str(aff_list_ids.sort()))
    # if cache_dict:
    #     return cache_dict
    currency_earnings_dict = {}
    from_date = aff_objs[0].account_manager.timestamp
    to_date = datetime.now()
    site_ids = aff_objs[0].account_manager.siteid.all()
    camp_links = CampaignTrackerMapping.objects.filter(
            fk_affiliateinfo__in=aff_objs,
            fk_campaign__siteid__in=site_ids).select_related('fk_campaign', 'fk_affiliateinfo')
    data_list, currency = group_by_results(from_date, to_date, site_ids, camp_links, 'affiliate',
                 'euro', 'affiliate')
    currency_earnings_dict = {i[0]:{u'\u20ac': float(i[-1].replace(u'\u20ac', ''))} for i in data_list}
    return currency_earnings_dict

    
    currency_earnings_dict = {}
    commission_dict = get_commission_dict([], [], aff_objs, site_ids, True)
    currency_dict = get_currency_dict(site_ids)
    for site in site_ids:
        resp = {}
        netw_site_obj = NetworkAndSiteMap.objects.get(siteid=site)
        content_type = netw_site_obj.networkid.connect_type
        upload_tracker_obj = UploadAdvanced.objects.filter(fk_campaign__fk_affiliateinfo__in=aff_objs,
                    siteid=site,
                    processed=True).values(
                        'fk_campaign__fk_affiliateinfo').annotate(Sum('ftd'),
                    Sum('registrations'),Sum('rate'),
                    Sum('deposit'),Sum('chargeback'),
                    Sum('cashout'),Sum('void'),
                    Sum('bonus'),Sum('revenue'), Sum('clicks'))
        upload_tracker_obj = {i['fk_campaign__fk_affiliateinfo']:i for i in upload_tracker_obj}
        upload_player_tracker_obj = {}
        upload_date_player_obj = {}
        upload_ftd_player_obj = {}
        # tracker_obj = get_cliks_data([], from_date, to_date, site_ids, [i])
        # api_clicks = CampaignClicksCount.objects.filter(
        #                         fk_campaign__fk_campaign__siteid=site,
        #                         fk_campaign__fk_affiliateinfo__in=aff_objs,
        #                         date__gte=str(from_date).split(' ')[0],
        #                         date__lte=str(to_date).split(' ')[0]).values(
        #                         'fk_campaign__fk_affiliateinfo'
        #                         ).annotate(Sum('clicks'))
        # api_clicks = {i['fk_campaign__fk_affiliateinfo']:i for i in api_clicks}
        # api_registrations = PPPlayerProcessed.objects.filter(
        #                     fk_campaign__fk_affiliateinfo__in=aff_objs, 
        #                     registrationdate__gte=str(from_date),
        #                     registrationdate__lte=str(to_date),
        #                     country__in=country,
        #                     siteid=site).values('fk_campaign__fk_affiliateinfo'
        #                     ).annotate(Count('playerid', 
        #                              distinct=True))
        # api_registrations = {i['fk_campaign__fk_affiliateinfo']:i for i in api_registrations}
        # api_ftds = PPPlayerProcessed.objects.filter(
        #                     fk_campaign__fk_affiliateinfo__in=aff_objs, 
        #                     ftddate__gte=str(from_date),
        #                     ftddate__lte=str(to_date),
        #                     country__in=country,
        #                     siteid=site,deposit__gt=0
        #                     ).values('fk_campaign__fk_affiliateinfo'
        #                     ).annotate(Count('playerid', 
        #                              distinct=True))
        # api_ftds = {i['fk_campaign__fk_affiliateinfo']:i for i in api_ftds}

        api_player_tracker_obj = PPPlayerProcessed.objects.filter(
                                fk_campaign__fk_affiliateinfo__in=aff_objs,
                                siteid=site).values('fk_campaign__fk_affiliateinfo'
                                ).annotate(Sum('deposit'),Sum('chargeback'),
                                Sum('cashout'),Sum('void'),Sum('reversechargeback'),
                                Sum('bonuses'),
                                Sum('sidegamesbets'),Sum('sidegameswins'),
                                Sum('jackpotcontribution'),Sum('revenue'))
        api_player_tracker_obj = {i['fk_campaign__fk_affiliateinfo']:i for i in api_player_tracker_obj}
        
        # api_date_deposit_player_obj = PPPlayerProcessed.objects.filter(
        #                     fk_campaign__fk_affiliateinfo__in=aff_objs,
        #                     date__gte=str(from_date).split(' ')[0],
        #                     date__lte=str(to_date).split(' ')[0],
        #                     country__in=country,
        #                     siteid=site,deposit__gt=0).values(
        #                     'fk_campaign__fk_affiliateinfo').annotate(Count('playerid', 
        #                              distinct=True))
        # api_date_deposit_player_obj = {i['fk_campaign__fk_affiliateinfo']:i for i in api_date_deposit_player_obj}

        for i in aff_objs:
            currency = currency_dict[site]
            cpa_value = 0.00
            cpr_value = 0.00
            rev_value = 0.00
            total_value = 0.00
            ftds = 0
            
            revenue = 0.0
            cpa_acc = 0
            content_type = 'POST_BACK'
            if api_player_tracker_obj.get(i.id):
                revenue = api_player_tracker_obj[i.id]['revenue__sum']

            if upload_tracker_obj.get(i.id):
                content_type = 'UPLOAD'
                revenue = revenue + upload_tracker_obj[i.id]['revenue__sum']
                ftds = ftds + upload_tracker_obj[i.id]['ftd__sum']
                cpa_acc = upload_tracker_obj[i.id]['ftd__sum']
            resp['lt_revenue'] = round(revenue, 2)

            cpa_t = 0
            rev_t = 0
            cpr_t = 0
            commission = commission_dict.get("%s/%s"%(i.id,site.id)) if commission_dict.get("%s/%s"%(i.id,site.id)) else 0
            if commission:
                if not commission['cpa'][1]:
                    commission['cpa'][1] = 0
                # if commission['pocdeduction']:
                #     revenue = (revenue - (revenue*0.21))
                #     resp['lt_revenue'] = round(revenue, 2)
                if commission['cpa'] and commission['cpa'][0] == 'FTD' and commission['cpa'][2]:
                    res_list = []
                    api_ftd_player_obj = PPPlayerProcessed.objects.filter(
                                        fk_campaign__fk_affiliateinfo=i,
                                        siteid=site,deposit__gt=commission['cpa'][1],
                                        ftddate = F('date')
                                        ).values('playerid').distinct()
                    cpa_acc = cpa_acc + len(api_ftd_player_obj)
                    cpa_value = (cpa_acc) * float(commission['cpa'][2])
                    cpa_value = round(cpa_value, 2)
                if commission['revshare'] and revenue:    
                    rev_value = (revenue * (commission['revshare']))/100
                    rev_value = round(rev_value, 2)
            total_value = cpa_value + cpr_value + rev_value
            total_value = round(total_value, 2)
            if i in currency_earnings_dict and currency in currency_earnings_dict[i]:
                currency_earnings_dict[i][currency] += total_value
            elif i not in currency_earnings_dict:
                currency_earnings_dict[i] = {currency: total_value}
            else:
                currency_earnings_dict[i][currency] = total_value
    # cache.set(str(aff_list_ids.sort()), currency_earnings_dict,cache_time)
    return currency_earnings_dict

def aff_total_earning(aff, currency_earnings_dict, total_d):
    resp = ''
    for e in currency_earnings_dict[aff]:
        if total_d.get(e):
            total_d[e] += currency_earnings_dict[aff][e]
        else:
            total_d[e] = currency_earnings_dict[aff][e]
        resp += e + ' ' + str(currency_earnings_dict[aff][e]) + ' '
    return resp, total_d

def sub_affiliate_group_report_stats(obj, from_date, to_date, camp_links,
                    site_ids, aff_list, country, group_d, currencyfilter):
    data_list = []
    currency = ''
    currency_dict = {}
    header_list = [
            # { "title": "Date", "width":"130px"},
            # { "title": "Brand", "width":"130px"},
           # { "title": "Affiliate", "width":"180px"},
           { "title": "Group by", "width":"130px"},
        #    { "title": "Impressions", "width":"150px"},
        #    { "title": "Clicks", "width":"100px"},
           { "title": "Registrations", "width":"180px"},
           { "title": "FTD", "width":"100px"},
           { "title": "Deposits", "width":"100px"},
           # { "title": "Cashout", "width":"100px"},
        #    { "title": "Bonus", "width":"120px"},
           { "title": "Revenue", "width":"150px"},
           { "title": "CPA Commission", "width":"200px"},
           { "title": "Revshare Commission", "width":"230px"},
        #    { "title": "Referral Commission", "width":"230px"},
           { "title": "Total Commission", "width":"200px"},
           ]
    group_list = []
    data_list, currency = group_by_results(from_date, to_date, site_ids, camp_links, group_d,
                 currencyfilter, 'affiliate', country)
    return (header_list, data_list, currency)


def affiliate_group_report_stats(obj, from_date, to_date, camp_links,
                    site_ids, aff_list, country, group_d, currencyfilter):
    data_list = []
    currency = ''
    currency_dict = {}
    header_list = [
            # { "title": "Date", "width":"130px"},
            # { "title": "Brand", "width":"130px"},
           # { "title": "Affiliate", "width":"180px"},
           { "title": "Group by", "width":"130px"},
           { "title": "Impressions", "width":"150px"},
           { "title": "Clicks", "width":"100px"},
           { "title": "Registrations", "width":"180px"},
           { "title": "FTD", "width":"100px"},
           { "title": "Deposits", "width":"100px"},
           # { "title": "Cashout", "width":"100px"},
           { "title": "Bonus", "width":"120px"},
           { "title": "Revenue", "width":"150px"},
           { "title": "CPA Commission", "width":"200px"},
           { "title": "Revshare Commission", "width":"230px"},
           { "title": "Referral Commission", "width":"230px"},
           { "title": "Total Commission", "width":"200px"},
           ]
    group_list = []
    data_list, currency = group_by_results(from_date, to_date, site_ids, camp_links, group_d,
                 currencyfilter, 'affiliate_activity', country)
    return (header_list, data_list, currency)


def affiliate_reports_stats(aff_objs, from_date, to_date, camp_links, site_ids, country):
    data_list = []
    currency = ''
    currency_dict = {}
    header_list = [
            # { "title": "Date", "width":"130px"},
            { "title": "Brand", "width":"130px"},
           { "title": "Affiliate", "width":"180px"},
           { "title": "Impressions", "width":"150px"},
           { "title": "Clicks", "width":"100px"},
           { "title": "Registrations", "width":"180px"},
           { "title": "FTD", "width":"100px"},
           { "title": "Deposits", "width":"100px"},
           # { "title": "Cashout", "width":"100px"},
           { "title": "Bonus", "width":"120px"},
           { "title": "Revenue", "width":"150px"},
           { "title": "CPA Commission", "width":"200px"},
           { "title": "Revshare Commission", "width":"230px"},
           { "title": "Total Commission", "width":"200px"},
           ]
    commission_dict = get_commission_dict([], [], aff_objs, site_ids, True)
    currency_dict = get_currency_dict(site_ids)
    for site in site_ids:
        resp = {}
        netw_site_obj = NetworkAndSiteMap.objects.get(siteid=site)
        content_type = netw_site_obj.networkid.connect_type
        
        upload_tracker_obj = UploadAdvanced.objects.filter(fk_campaign__fk_affiliateinfo__in=aff_objs,
                    date__gte=str(from_date).split(' ')[0],
                    date__lte=str(to_date).split(' ')[0],
                    siteid=site,
                    processed=True).values(
                        'fk_campaign__fk_affiliateinfo').annotate(Sum('ftd'),
                    Sum('registrations'),Sum('rate'),
                    Sum('deposit'),Sum('chargeback'),
                    Sum('cashout'),Sum('void'),
                    Sum('bonus'),Sum('revenue'), Sum('clicks'))
        upload_tracker_obj = {i['fk_campaign__fk_affiliateinfo']:i for i in upload_tracker_obj}
        upload_player_tracker_obj = {}
        upload_date_player_obj = {}
        upload_ftd_player_obj = {}
        # tracker_obj = get_cliks_data([], from_date, to_date, site_ids, [i])
        api_clicks = CampaignClicksCount.objects.filter(
                                fk_campaign__fk_campaign__siteid=site,
                                fk_campaign__fk_affiliateinfo__in=aff_objs,
                                date__gte=str(from_date).split(' ')[0],
                                date__lte=str(to_date).split(' ')[0]).values(
                                'fk_campaign__fk_affiliateinfo'
                                ).annotate(Sum('clicks'))
        api_clicks = {i['fk_campaign__fk_affiliateinfo']:i for i in api_clicks}
        api_registrations = PPPlayerProcessed.objects.filter(
                            fk_campaign__fk_affiliateinfo__in=aff_objs, 
                            registrationdate__gte=str(from_date),
                            registrationdate__lte=str(to_date),
                            country__in=country,
                            siteid=site).values('fk_campaign__fk_affiliateinfo'
                            ).annotate(Count('playerid', 
                                     distinct=True))
        api_registrations = {i['fk_campaign__fk_affiliateinfo']:i for i in api_registrations}
        api_ftds = PPPlayerProcessed.objects.filter(
                            fk_campaign__fk_affiliateinfo__in=aff_objs, 
                            ftddate__gte=str(from_date),
                            ftddate__lte=str(to_date),
                            country__in=country,
                            siteid=site,deposit__gt=0
                            ).values('fk_campaign__fk_affiliateinfo'
                            ).annotate(Count('playerid', 
                                     distinct=True))
        api_ftds = {i['fk_campaign__fk_affiliateinfo']:i for i in api_ftds}

        api_player_tracker_obj = PPPlayerProcessed.objects.filter(
                                fk_campaign__fk_affiliateinfo__in=aff_objs,
                                date__gte=str(from_date).split(' ')[0],
                                date__lte=str(to_date).split(' ')[0],
                                country__in=country,
                                siteid=site).values('fk_campaign__fk_affiliateinfo'
                                ).annotate(Sum('deposit'),Sum('chargeback'),
                                Sum('cashout'),Sum('void'),Sum('reversechargeback'),
                                Sum('bonuses'),
                                Sum('sidegamesbets'),Sum('sidegameswins'),
                                Sum('jackpotcontribution'),Sum('revenue'))
        api_player_tracker_obj = {i['fk_campaign__fk_affiliateinfo']:i for i in api_player_tracker_obj}
        
        api_date_deposit_player_obj = PPPlayerProcessed.objects.filter(
                            fk_campaign__fk_affiliateinfo__in=aff_objs,
                            date__gte=str(from_date).split(' ')[0],
                            date__lte=str(to_date).split(' ')[0],
                            country__in=country,
                            siteid=site,deposit__gt=0).values(
                            'fk_campaign__fk_affiliateinfo').annotate(Count('playerid', 
                                     distinct=True))
        api_date_deposit_player_obj = {i['fk_campaign__fk_affiliateinfo']:i for i in api_date_deposit_player_obj}

        for i in aff_objs:
            currency = currency_dict[site]
            
            diposit_accounts = 0
            
            cpa_value = 0.00
            cpr_value = 0.00
            rev_value = 0.00
            total_value = 0.00
            days_count = to_date - from_date
            days_count = days_count.days
            revenue = 0.0
            deposit = 0.0
            cashout = 0.0
            chargeback = 0.0
            void = 0.0
            bonus = 0.0
            cpa_acc = 0
            registrations = 0
            ftds = 0
            cpa_acc = 0
            clicks = 0
            content_type = 'POST_BACK'
            clicks = api_clicks[i.id]['clicks__sum'] if api_clicks.get(i.id) else 0
            if api_player_tracker_obj.get(i.id):
                # if api_new_player_obj.get(i.id):
                #     new_player_deposit = api_new_player_obj.get(i.id)['totaldeposit__sum']
                #     avg_ltv = (api_new_player_obj[i.id]['revenue__sum']/api_new_side_player_obj[i.id]['sidegamesbets__sum'])
                # if api_active_player_obj.get(i.id):
                #     active_player_count = api_active_player_obj.get(i.id)['playerid__count']
                revenue = api_player_tracker_obj[i.id]['revenue__sum']
                deposit = api_player_tracker_obj[i.id]['deposit__sum']
                cashout = api_player_tracker_obj[i.id]['cashout__sum']
                bonus = api_player_tracker_obj[i.id]['bonuses__sum']
                
                
                if api_registrations.get(i.id):
                    registrations = api_registrations[i.id]['playerid__count']
                if api_ftds.get(i.id):
                    ftds = api_ftds[i.id]['playerid__count']
            if upload_tracker_obj.get(i.id):
                content_type = 'UPLOAD'
                revenue = revenue + upload_tracker_obj[i.id]['revenue__sum']
                deposit = deposit + upload_tracker_obj[i.id]['deposit__sum']
                cashout = cashout + upload_tracker_obj[i.id]['cashout__sum']
                bonus = bonus + upload_tracker_obj[i.id]['bonus__sum']
                registrations = registrations + upload_tracker_obj[i.id]['registrations__sum']
                ftds = ftds + upload_tracker_obj[i.id]['ftd__sum']
                cpa_acc = upload_tracker_obj[i.id]['ftd__sum']
                clicks = clicks + upload_tracker_obj[i.id]['clicks__sum']


            resp['lt_registrations'] = registrations

            resp['lt_clicks'] = clicks

            resp['lt_ftd']= ftds
            resp['lt_bonus'] = round(bonus, 2)
            resp['lt_dep_amount'] = round(deposit, 2)
            resp['lt_with_amount'] = round(cashout, 2)
            resp['lt_revenue'] = round(revenue, 2)

            cpa_t = 0
            rev_t = 0
            cpr_t = 0
            commission = commission_dict.get("%s/%s"%(i.id,site.id)) if commission_dict.get("%s/%s"%(i.id,site.id)) else 0
            if commission:
                if not commission['cpa'][1]:
                    commission['cpa'][1] = 0
                # if commission['pocdeduction']:
                #     revenue = (revenue - (revenue*0.21))
                #     resp['lt_revenue'] = round(revenue, 2)
                if commission['cpa'] and commission['cpa'][0] == 'FTD' and commission['cpa'][2]:
                    res_list = []
                    api_ftd_player_obj = PPPlayerProcessed.objects.filter(
                                        fk_campaign__fk_affiliateinfo=i,
                                        ftddate__gte=str(from_date).split(' ')[0],
                                        ftddate__lte=str(to_date),
                                        country__in=country,
                                        siteid=site,deposit__gt=commission['cpa'][1],
                                        ftddate = F('date')
                                        ).values('playerid').distinct()
                    cpa_acc = cpa_acc + len(api_ftd_player_obj)
                    cpa_value = (cpa_acc) * float(commission['cpa'][2])
                    cpa_value = round(cpa_value, 2)
                # elif commission['cpa'] and commission['cpa'][0] == 'FTD' and commission['cpa'][2] and content_type != 'UPLOAD':
                #     res_list = []
                #     api_ftd_player_obj = PPPlayerProcessed.objects.filter(fk_campaign=i,
                #                         date__gte=str(from_date).split(' ')[0],
                #                         date__lte=str(to_date).split(' ')[0],
                #                         siteid__in=site_ids,deposit__gt=commission['cpa'][1]
                #                         ).values('playerid').distinct()
                #     cpa_acc = len(api_ftd_player_obj)
                #     cpa_value = (cpa_acc) * float(commission['cpa'][2])
                #     cpa_value = round(cpa_value, 2)
                # if commission.referalchoice:
                #     cpr_value = (tracker_obj['cashout']) * (commission.referalcommissionvalue)
                if commission['revshare'] and revenue:    
                    rev_value = (revenue * (commission['revshare']))/100
                    rev_value = round(rev_value, 2)
            total_value = cpa_value + cpr_value + rev_value
            total_value = round(total_value, 2)
            # if commission.referalchoice:
            #     cpr_t = (resp['lt_registrations']) * (commission.referalcommissionvalue)
            resp['lt_earnings'] = total_value
            # resp_d = {(i):(format(resp[i], '.2f') if type(resp[i]) == float else resp[i]) for i in resp}
            # resp = resp_d
            aff_data = [site.name, i.username, 0, resp['lt_clicks'],
                    resp['lt_registrations'], resp['lt_ftd'], resp['lt_dep_amount'], resp['lt_with_amount'],
                    resp['lt_bonus'], resp['lt_revenue'], cpa_value, rev_value, resp['lt_earnings']]
            aff_data = [j if ('.' not in str(j) or '@' in str(j) or isinstance(j,str)) else (currency + str(j)) for j in aff_data]
            aff_data = [k if k != (currency + '0.0') else (currency + '0.00') for k in aff_data]
            data_list.append(aff_data)
    return (header_list, data_list, currency)


def advertiser_reports_stats(aff_objs, from_date, to_date, site_ids, currencyfilter, country):
    data_list = []
    currency = ''
    currency_dict = {}
    header_list = [
            { "title": "Brand", "width":"130px"},
           { "title": "Impressions", "width":"150px"},
           { "title": "Clicks", "width":"100px"},
           { "title": "Registrations", "width":"180px"},
           { "title": "FTD", "width":"100px"},
           { "title": "Deposits", "width":"100px"},
           # { "title": "Cashout", "width":"100px"},
           { "title": "Bonus", "width":"120px"},
           { "title": "Revenue", "width":"150px"},
           ]
    camp_links = CampaignTrackerMapping.objects.filter(
            fk_affiliateinfo__in=aff_objs,
            fk_campaign__siteid__in=site_ids).select_related('fk_campaign', 'fk_affiliateinfo')
    data_list, currency = group_by_results(from_date, to_date, site_ids, camp_links, 'brand',
                 currencyfilter, 'brand_admin')
    return (header_list, data_list, currency)


def dynamic_reports_stats(obj, from_date, to_date, camp_links, site_ids, currencyfilter, group_d, country):
    data_list = []
    total_list = ['Total', '']
    currency = ''
    currency_dict = {}
    header_list = [
           { "title": "Group by", "width":"150px"},
           { "title": "Impressions", "width":"140px"},
           { "title": "Clicks", "width":"120px"},
           { "title": "CTR", "width":"120px"},
           { "title": "Registrations", "width":"200px"},
           { "title": "Registration rate %", "width":"180px"},
           { "title": "FTD", "width":"120px"},
           { "title": "Conversion rate %", "width":"200px"},
           { "title": "New players", "width":"160px"},
           { "title": "Active players", "width":"200px"},
           { "title": "Depositing accounts", "width":"240px"},
           { "title": "Wagering accounts", "width":"230px"},
           { "title": "Total Wagers", "width":"180px"},
           { "title": "Avg active days", "width":"200px"},
           { "title": "Net per player", "width":"180px"},
           { "title": "Deposits", "width":"130px"},
           # { "title": "Cashout", "width":"130px"},
           { "title": "Chargeback", "width":"150px"},
           { "title": "Void", "width":"120px"},
           { "title": "Bonus", "width":"120px"},
           { "title": "Revenue", "width":"150px"},
           { "title": "Avg. Player LTV", "width":"200px"},
           { "title": "CPA Commission", "width":"200px"},
           { "title": "Revshare Commission", "width":"220px"},
           { "title": "Total Commission", "width":"180px"},
           ]
    data_list, currency = group_by_results(from_date, to_date, site_ids, camp_links, group_d,
                 currencyfilter, 'earnings', country)
    return (header_list, data_list, currency)

def dynamic_reports_admin_stats(obj, from_date, to_date, camp_links, site_ids,
                         aff_list, country, currencyfilter, group_d):
    data_list = []
    total_list = ['Total', '']
    currency = ''
    currency_dict = {}
    header_list = [
           { "title": "Dynamic", "width":"150px"},
           { "title": "Affiliate", "width":"150px"},
           { "title": "Impressions", "width":"140px"},
           { "title": "Clicks", "width":"120px"},
           { "title": "CTR", "width":"120px"},
           { "title": "Registrations", "width":"200px"},
           { "title": "Registration rate %", "width":"180px"},
           { "title": "FTD", "width":"120px"},
           { "title": "Conversion rate %", "width":"200px"},
           { "title": "New players", "width":"160px"},
           { "title": "Active players", "width":"200px"},
           { "title": "Depositing accounts", "width":"240px"},
           { "title": "Wagering accounts", "width":"230px"},
           { "title": "Total Wagers", "width":"180px"},
           { "title": "Avg active days", "width":"200px"},
           { "title": "Net per player", "width":"180px"},
           { "title": "Deposits", "width":"130px"},
           # { "title": "Cashout", "width":"130px"},
           { "title": "Chargeback", "width":"150px"},
           { "title": "Void", "width":"120px"},
           { "title": "Bonus", "width":"120px"},
           { "title": "Revenue", "width":"150px"},
           { "title": "Avg. Player LTV", "width":"200px"},
           { "title": "CPA Commission", "width":"200px"},
           { "title": "Revshare Commission", "width":"220px"},
           { "title": "Total Commission", "width":"180px"},
           ]
    # data_list, currency = group_by_results(from_date, to_date, site_ids, camp_links, group_d,
    #              currencyfilter, 'earnings', country)
    data_list, currency = campaign_dynamic_reports(from_date, to_date, site_ids, camp_links, 'dynamic',
                 currencyfilter, 'dynamic', country)
    return (header_list, data_list, currency)

def campaign_reports_admin_stats(obj, from_date, to_date, camp_links, site_ids,
                         aff_list, country, currencyfilter, group_d):
    data_list = []
    total_list = ['Total', '']
    currency = ''
    currency_dict = {}
    header_list = [
           # { "title": "Brand", "width":"120px"},
           # { "title": "Campaign name", "width":"200px"},
           # { "title": "Affiliate name", "width":"200px"},
           { "title": "Campaign", "width":"150px"},
           { "title": "Affiliate", "width":"200px"},
           { "title": "Impressions", "width":"150px"},
           { "title": "Clicks", "width":"120px"},
           { "title": "Registrations", "width":"150px"},
           { "title": "FTD", "width":"120px"},
           { "title": "Depositing accounts", "width":"220px"},
           { "title": "Deposits", "width":"150px"},
           { "title": "Revenue", "width":"150px"},
           { "title": "CPA Commission", "width":"200px"},
           { "title": "Revshare Commission", "width":"230px"},
           # { "title": "Referral Commission", "width":"230px"},
           { "title": "Total Commission", "width":"200px"},
           ]
    data_list, currency = campaign_dynamic_reports(from_date, to_date, site_ids, camp_links, 'campaign',
                 currencyfilter, 'campaign', country)
    return (header_list, data_list, currency)


def campaign_reports_stats(obj, from_date, to_date, camp_links, site_ids, currencyfilter, group_d, country):
    data_list = []
    total_list = ['Total', '']
    currency = ''
    currency_dict = {}
    header_list = [
           { "title": "Group by", "width":"150px"},
           { "title": "Impressions", "width":"150px"},
           { "title": "Clicks", "width":"120px"},
           { "title": "Registrations", "width":"150px"},
           { "title": "FTD", "width":"120px"},
           { "title": "Depositing accounts", "width":"220px"},
           { "title": "Deposits", "width":"150px"},
           { "title": "Revenue", "width":"150px"},
           { "title": "CPA Commission", "width":"200px"},
           { "title": "Revshare Commission", "width":"230px"},
           # { "title": "Referral Commission", "width":"230px"},
           { "title": "Total Commission", "width":"200px"},
           ]
    data_list, currency = group_by_results(from_date, to_date, site_ids, camp_links, group_d,
                 currencyfilter, 'campaign', country)
    for d_l in data_list:
        del d_l[-2]
    return (header_list, data_list, currency)


def get_buss_days_list(start_date, end_date):
    # end_date = start_date + timedelta(days=1) #date.today()
    # end_date = end_date.today().replace(day=end_date.day, month=end_date.month-2, year=end_date.year)
    delta = timedelta(days=1)
    d = start_date
    date_list = []
    while d <= end_date:
        date_list.append(d.strftime('%Y-%m-%d'))
        d += delta
    return date_list

def get_buss_d_days_list(start_date, end_date):
    # end_date = start_date + timedelta(days=1) #date.today()
    # end_date = end_date.today().replace(day=end_date.day, month=end_date.month-2, year=end_date.year)
    delta = timedelta(days=1)
    d = start_date
    date_list = []
    while d <= end_date:
        date_list.append(d)
        d += delta
    return date_list

def calculate_comm(commission, revenue, ftd_player_obj, date_player_obj):
    cpa_value = 0.00
    cpr_value = 0.00
    rev_value = 0.00
    total_value = 0.00
    if commission:
        commission = commission[0]
        # if commission.pocdeduction:
        #     revenue = revenue*(0.85)
        if commission.cpachoice and commission.criteriaacquisition == 'FTD':
            res_list = []
            cpa_accounts = ftd_player_obj.values('playerid').annotate(Sum('deposit'))
            for c in cpa_accounts:
                if c.get('deposit__sum') and c.get('deposit__sum') >= commission.criteriavalue:
                    res_list.append(c)
            cpa_acc = len(res_list)
            cpa_value = (cpa_acc) * float(commission.cpacommissionvalue) if commission.cpacommissionvalue else 0
            cpa_value = round(cpa_value, 2)
        # elif commission.cpachoice and commission.criteriaacquisition == 'Deposit':
        #     res_list = []
        #     cpa_accounts = date_player_obj.values('playerid').annotate(Sum('deposit'))
        #     for c in cpa_accounts:
        #         if c.get('deposit__sum') and c.get('deposit__sum') >= commission.criteriavalue:
        #             res_list.append(c)
        #     cpa_acc = len(res_list)
        #     cpa_value = (cpa_acc) * float(commission.cpacommissionvalue) if commission.cpacommissionvalue else 0
        #     cpa_value = round(cpa_value, 2)
        # if commission.referalchoice:
        #     cpr_value = (tracker_obj['cashout']) * (commission.referalcommissionvalue)
        if commission.revshare and revenue:
            rev_value = (revenue * (commission.revsharepercent))/100
            rev_value = round(rev_value, 2)
    total_value = cpa_value + cpr_value + rev_value
    total_value = round(total_value, 2)
    return {'cpa_value': cpa_value, 'cpr_value':cpr_value, 'rev_value': rev_value, 'total_commission':total_value}

def get_subaffiliate_data_set(subaffid, camp_track_links, subaffobj, from_date, to_date):
    resp = {'subaffid': subaffid}
    post_back_list = PostBackAdvanced.objects.filter(fk_campaign__fk_affiliateinfo=subaffobj)
    if camp_track_links:

        i = camp_track_links[0]
        currency = ''
        currency_obj = Currency.objects.filter(siteid=i.fk_campaign.siteid)
        if currency_obj:
            currency = currency_obj[0].currency
            resp['currency'] = currency

        if from_date and to_date:
            # date wise data appending
            resp['date_wise_data'] = []
            dates_list = get_buss_days_list(from_date, to_date)
            for _date in dates_list:
                data_resp = {}
                #Commission data
                commission = Affiliatecommission.objects.filter(fk_affiliateinfo=subaffobj).select_related('fk_affiliateinfo')
                if len(post_back_list) > 0:
                    tracker_obj = PostBackAdvanced.objects.filter(fk_campaign=i,
                                    date=_date)
                    ftd_player_obj = PostBackPlayer.objects.filter(fk_campaign=i,
                                        ftddate=_date)
                    date_player_obj = PostBackPlayer.objects.filter(fk_campaign=i,
                                    date=_date)
                else:
                    ftd_player_obj = PPPlayerProcessed.objects.filter(fk_campaign=i,
                                        ftddate=_date)
                    date_player_obj = PPPlayerProcessed.objects.filter(fk_campaign=i,
                                    date=_date)
                    tracker_obj = PPAdvancedProcessed.objects.filter(fk_campaign=i,
                                    date=_date)
                if tracker_obj:
                    tracker_obj = tracker_obj[0]
                    data_resp['date'] = str(_date)
                    data_resp['FTD'] = tracker_obj.ftd
                    data_resp['Registrations'] = tracker_obj.registrations
                    data_resp['Deposits'] = tracker_obj.deposit
                    data_resp['NGR'] = tracker_obj.revenue
                    comm_data = calculate_comm(commission, tracker_obj.revenue, ftd_player_obj, date_player_obj)
                    data_resp['commission_data'] = comm_data
                    resp['date_wise_data'].append(data_resp)

            # add final data and commission
            if len(post_back_list) > 0:
                total_tracker_obj = PostBackAdvanced.objects.filter(fk_campaign=i,
                                date__gte=str(from_date),
                                date__lte=str(to_date)).aggregate(ftd = Sum('ftd'),
                                registrations= Sum('registrations'),
                                deposit=Sum('deposit'),revenue=Sum('revenue'))
                ftd_player_obj = PostBackPlayer.objects.filter(fk_campaign=i,
                            date__gte=str(from_date).split(' ')[0],
                            date__lte=str(to_date).split(' ')[0])
                date_player_obj = PostBackPlayer.objects.filter(fk_campaign=i,
                            date__gte=str(from_date).split(' ')[0],
                            date__lte=str(to_date).split(' ')[0])
            else:
                date_player_obj = PPPlayerProcessed.objects.filter(fk_campaign=i,
                                    date__gte=str(from_date).split(' ')[0],
                                    date__lte=str(to_date).split(' ')[0])
                ftd_player_obj = PPPlayerProcessed.objects.filter(fk_campaign=i,
                                    ftddate__gte=str(from_date).split(' ')[0],
                                    ftddate__lte=str(to_date))

                total_tracker_obj = PPAdvancedProcessed.objects.filter(fk_campaign=i,
                                date__gte=str(from_date),
                                date__lte=str(to_date)).aggregate(ftd = Sum('ftd'),
                                registrations= Sum('registrations'),
                                deposit=Sum('deposit'),revenue=Sum('revenue'))

            final_resp = {}
            if total_tracker_obj:
                final_resp['FTD'] = total_tracker_obj['ftd']
                final_resp['Registrations'] = total_tracker_obj['registrations']
                final_resp['Deposits'] = total_tracker_obj['deposit']
                final_resp['NGR'] = total_tracker_obj['revenue']
                comm_data = calculate_comm(commission, total_tracker_obj['revenue'], ftd_player_obj, date_player_obj)
                final_resp['commission_data'] = comm_data
            resp['final_data'] = final_resp
            
        else:
            commission = Affiliatecommission.objects.filter(fk_affiliateinfo=subaffobj).select_related('fk_affiliateinfo')
            if len(post_back_list) > 0:
                tracker_obj = PostBackAdvanced.objects.filter(fk_campaign=i
                                ).aggregate(ftd = Sum('ftd'),
                                registrations= Sum('registrations'),
                                deposit=Sum('deposit'),revenue=Sum('revenue'))
                ftd_player_obj = PostBackPlayer.objects.filter(fk_campaign=i
                            )
                date_player_obj = PostBackPlayer.objects.filter(fk_campaign=i
                            )
            else:
                tracker_obj = PPAdvancedProcessed.objects.filter(fk_campaign=i)
                tracker_obj = tracker_obj.aggregate(ftd = Sum('ftd'),
                                registrations= Sum('registrations'),
                                deposit=Sum('deposit'),revenue=Sum('revenue'))
                ftd_player_obj = PPPlayerProcessed.objects.filter(fk_campaign=i,
                                )
                date_player_obj = PPPlayerProcessed.objects.filter(fk_campaign=i,
                                )

            final_resp = {}
            final_resp['FTD'] = tracker_obj['ftd']
            final_resp['Registrations'] = tracker_obj['registrations']
            final_resp['Deposits'] = tracker_obj['deposit']
            final_resp['NGR'] = tracker_obj['revenue']
            comm_data = calculate_comm(commission, tracker_obj['revenue'], ftd_player_obj, date_player_obj)
            final_resp['commission_data'] = comm_data
            resp['final_data'] = final_resp

    return resp

def get_admin_affiliate_data_set(admin_aff_obj, from_date, to_date):
    resp = []
    sub_aff_obj = Affiliateinfo.objects.filter(superaffiliate=admin_aff_obj)
    for sub_aff in sub_aff_obj:
        try:
            sub_aff_obj = VcommFPAffiliateMapping.objects.get(affiliateid=sub_aff)
            camp_track_links = CampaignTrackerMapping.objects.filter(fk_affiliateinfo=sub_aff)
            resp_dict = get_subaffiliate_data_set(sub_aff_obj.vcommsubid, camp_track_links, sub_aff, from_date, to_date)
            resp.append(resp_dict)
        except Exception as e:
            print 'exception', str(e)
            print 'affiliate not with Vcommaffiliatemapping ', sub_aff.id
    return resp

def Process_sub_affiliate_stats(aff_objs, camp_track_links, from_date, 
        to_date, site_ids, obj, currencyfilter, group_d, country):
    data_list = []
    total_list = []
    site_currency_dict = {}
    currency = ''
    for site in site_ids:
        currency_obj = Currency.objects.filter(siteid=site)
        if currency_obj:
            site_currency_dict[site] = currency_obj[0].currency
            currency = currency_obj[0].currency
    header_list = [
           { "title": "Group by", "width":"150px"},
           { "title": "Impressions", "width":"150px"},
           { "title": "Clicks", "width":"120px"},
           { "title": "Registrations", "width":"150px"},
           { "title": "FTD", "width":"120px"},
           { "title": "Depositing accounts", "width":"220px"},
           { "title": "Deposits", "width":"150px"},
           { "title": "Revenue", "width":"150px"},
           { "title": "CPA Commission", "width":"200px"},
           { "title": "Revshare Commission", "width":"230px"},
           { "title": "Total Commission", "width":"200px"},
           ]
    data_list, currency = group_by_results(from_date, to_date, site_ids, camp_track_links, group_d,
                 currencyfilter, 'campaign', country)
    for d_l in data_list:
        del d_l[-2]
    return (header_list, data_list, currency)


def Process_sub_affiliate_data_stats(aff_objs, from_date, to_date, site_ids):
    data_list = []
    total_list = []
    site_currency_dict = {}
    currency = ''
    header_list = [
           { "title": "Sub Affiliate Id", "width":"80px"},
           { "title": "Super Affiliate Id", "width":"80px"},
           { "title": "Site Id", "width":"180px"},
           { "title": "Brand", "width":"180px"},
           { "title": "Impressions", "width":"150px"},
           { "title": "Clicks", "width":"130px"},
           { "title": "Click Through Ratio", "width":"180px"},
           { "title": "Registrations", "width":"180px"},
           { "title": "Registration rate", "width":"180px"},
           { "title": "FTD", "width":"120px"},
           { "title": "Conversion rate %", "width":"200px"},
           { "title": "New players", "width":"160px"},
           { "title": "New player Deposits", "width":"240px"},
           { "title": "Active players", "width":"200px"},
           { "title": "Depositing accounts", "width":"240px"},
           { "title": "Wagering accounts", "width":"230px"},
           { "title": "Total Wagers", "width":"180px"},
           { "title": "Avg active days", "width":"200px"},
           { "title": "Gross Revenue", "width":"160px"},
           { "title": "Gross per player", "width":"180px"},
           { "title": "Net Revenue", "width":"160px"},
           { "title": "Net per player", "width":"180px"},
           { "title": "Total Bets", "width":"160px"},
           { "title": "Stake", "width":"180px"},
           { "title": "Product 1 Commission", "width":"180px"},
           { "title": "Casino Bets", "width":"160px"},
           { "title": "Casino Wagers", "width":"160px"},
           { "title": "Casino Revenue", "width":"180px"},
           { "title": "Casino Net Revenue", "width":"230px"},
           { "title": "Casino Commission", "width":"160px"},
           { "title": "Deposits", "width":"130px"},
           # { "title": "Cashout", "width":"130px"},
           { "title": "Chargeback", "width":"150px"},
           { "title": "Void", "width":"120px"},
           { "title": "Bonus", "width":"120px"},
           { "title": "Revenue", "width":"150px"},
           { "title": "Avg. Player LTV", "width":"200px"},
           { "title": "CPA Commission", "width":"200px"},
           { "title": "Revshare Commission", "width":"220px"},
           { "title": "Total Commission", "width":"180px"},
           ]
    # if group_by == 'date':
    #     header_list.insert(0,{ "title": "Date", "width":"180px"})
    # elif group_by == 'site':
    #     header_list.insert(0,{ "title": "Domain", "width":"180px"})
    for obj in aff_objs:
        for site in obj.siteid.all():
            if site in site_currency_dict:
                currency = site_currency_dict[site]
            else:
                currency = ''
                currency_obj = Currency.objects.filter(siteid=site)
                if currency_obj:
                    site_currency_dict[site] = currency_obj[0].currency
                    currency = currency_obj[0].currency
            netw_site_obj = NetworkAndSiteMap.objects.get(siteid=site)
            dates_list = get_buss_days_list(from_date, to_date)
            commission = Affiliatecommission.objects.filter(fk_affiliateinfo=obj).select_related('fk_affiliateinfo')
            post_back_list = PostBackAdvanced.objects.filter(fk_campaign__fk_affiliateinfo=obj)
            upload_back_list = UploadAdvanced.objects.filter(fk_campaign__fk_affiliateinfo=obj)
            content_type = netw_site_obj.networkid.connect_type
            if content_type == 'POST_BACK':
                tracker_obj = PostBackAdvanced.objects.filter(fk_campaign__fk_affiliateinfo=obj,
                                date__gte=str(from_date).split(' ')[0],
                                date__lte=str(to_date).split(' ')[0],
                                siteid=site).aggregate(ftd = Sum('ftd'),
                                registrations= Sum('registrations'),rate=Sum('rate'),
                                deposit=Sum('deposit'),chargeback=Sum('chargeback'),reversal=Sum('reversal'),
                                cashout=Sum('cashout'),void=Sum('void'),reversechargeback=Sum('reversechargeback'),
                                bonus=Sum('bonus'),expiredbonus=Sum('expiredbonus'),
                                sidegamesbets=Sum('sidegamesbets'),sidegameswins=Sum('sidegameswins'),
                                jackpotcontribution=Sum('jackpotcontribution'),revenue=Sum('revenue'), clicks=Sum('clicks'))
                player_tracker_obj = PostBackPlayer.objects.filter(fk_campaign__fk_affiliateinfo=obj,
                                        date__gte=str(from_date).split(' ')[0],
                                        date__lte=str(to_date).split(' ')[0],
                                        siteid=site).aggregate(deposit=Sum('deposit'),chargeback=Sum('chargeback'),
                                        cashout=Sum('cashout'),void=Sum('void'),reversechargeback=Sum('reversechargeback'),
                                        bonus=Sum('bonuses'),reversal=Sum('reversal'),
                                        sidegamesbets=Sum('sidegamesbets'),sidegameswins=Sum('sidegameswins'),
                                        jackpotcontribution=Sum('jackpotcontribution'),revenue=Sum('revenue'))
                player_tracker_obj['cashout'] = player_tracker_obj['cashout'] if player_tracker_obj['cashout'] else 0.0 \
                                     - player_tracker_obj['reversal'] if player_tracker_obj['reversal'] else 0.0
        
                # tracker_obj['cashout'] = tracker_obj['cashout'] if tracker_obj['cashout'] else 0.0 \
                #                          - tracker_obj['reversal'] if tracker_obj['reversal'] else 0.0
                new_player_obj = PostBackPlayer.objects.filter(fk_campaign__fk_affiliateinfo=obj, 
                                    registrationdate__gte=str(from_date),
                                    registrationdate__lte=str(to_date),
                                    siteid=site)
                active_player_obj = PostBackPlayer.objects.filter(fk_campaign__fk_affiliateinfo=obj,
                                    lastdate__gte=str(from_date),
                                    lastdate__lte=str(to_date),
                                    siteid=site)
                date_player_obj = PostBackPlayer.objects.filter(fk_campaign__fk_affiliateinfo=obj,
                                    date__gte=str(from_date).split(' ')[0],
                                    date__lte=str(to_date).split(' ')[0],
                                    siteid=site)
                ftd_player_obj = PostBackPlayer.objects.filter(fk_campaign__fk_affiliateinfo=obj,
                                    ftddate__gte=str(from_date).split(' ')[0],
                                    ftddate__lte=str(to_date),
                                    siteid=site)
                avg_active_players_obj = PostBackPlayer.objects.filter(fk_campaign__fk_affiliateinfo=obj,
                                    registrationdate__gte=str(from_date).split(' ')[0],
                                    lastdate__lte=str(to_date).split(' ')[0],
                                    siteid=site).values('playerid',
                                    'registrationdate', 'lastdate').distinct()
            elif content_type == 'UPLOAD':
                tracker_obj = UploadAdvanced.objects.filter(fk_campaign__fk_affiliateinfo=obj,
                                date__gte=str(from_date).split(' ')[0],
                                date__lte=str(to_date).split(' ')[0],
                                siteid=site,
                                processed=True).aggregate(ftd = Sum('ftd'),
                                registrations= Sum('registrations'),rate=Sum('rate'),
                                deposit=Sum('deposit'),chargeback=Sum('chargeback'),
                                cashout=Sum('cashout'),void=Sum('void'),
                                bonus=Sum('bonus'),revenue=Sum('revenue'), clicks=Sum('clicks'))
                player_tracker_obj = []
                new_player_obj = []
                active_player_obj = [] 
                date_player_obj = []
                ftd_player_obj = []
                avg_active_players_obj = []
            else:
                tracker_obj = get_cliks_data([obj], from_date, to_date, [site])
   
                player_tracker_obj = PPPlayerProcessed.objects.filter(fk_campaign__fk_affiliateinfo=obj,
                                        date__gte=str(from_date).split(' ')[0],
                                        date__lte=str(to_date).split(' ')[0],
                                        siteid=site).aggregate(
                                        deposit=Sum('deposit'),chargeback=Sum('chargeback'),
                                        cashout=Sum('cashout'),void=Sum('void'),reversechargeback=Sum('reversechargeback'),
                                        bonus=Sum('bonuses'),
                                        sidegamesbets=Sum('sidegamesbets'),sidegameswins=Sum('sidegameswins'),
                                        jackpotcontribution=Sum('jackpotcontribution'),revenue=Sum('revenue'))
                new_player_obj = PPPlayerProcessed.objects.filter(fk_campaign__fk_affiliateinfo=obj, 
                                    registrationdate__gte=str(from_date),
                                    registrationdate__lte=str(to_date),
                                    siteid=site)
                active_player_obj = PPPlayerProcessed.objects.filter(fk_campaign__fk_affiliateinfo=obj,
                            lastdate__gte=str(from_date),
                            lastdate__lte=str(to_date),
                            siteid=site)
                date_player_obj = PPPlayerProcessed.objects.filter(fk_campaign__fk_affiliateinfo=obj,
                            date__gte=str(from_date).split(' ')[0],
                            date__lte=str(to_date).split(' ')[0],
                            siteid=site)
                ftd_player_obj = PPPlayerProcessed.objects.filter(fk_campaign__fk_affiliateinfo=obj,
                                ftddate__gte=str(from_date).split(' ')[0],
                                ftddate__lte=str(to_date),
                                siteid=site)
                avg_active_players_obj = PPPlayerProcessed.objects.filter(fk_campaign__fk_affiliateinfo=obj,
                            registrationdate__gte=str(from_date).split(' ')[0],
                            lastdate__lte=str(to_date).split(' ')[0],
                            siteid=site).values('playerid',
                            'registrationdate', 'lastdate').distinct()

            newplayer_list = []
            new_player_count = 0
            new_player_deposit = 0.00
            active_player_count= 0
            diposit_accounts = 0
            wagering_accounts = 0
            total_wagers = 0.00
            
            days_count = to_date - from_date
            days_count = days_count.days
            avg_active_players_days = 0
            avg_active_players = 0
            avg_net_cash = 0.00
            avg_ltv = 0.00
            avg_ltv_list = []
            revenue_ltv = 0.0
            if content_type != 'UPLOAD':
                cpa_accounts = new_player_obj.values('playerid').annotate(Sum('deposit'))
                for c in cpa_accounts:
                    newplayer_list.append(c)
                
                if newplayer_list:
                    new_player_count = len(newplayer_list)
                    for j in newplayer_list:
                        new_player_deposit = new_player_deposit + j['deposit__sum']
                # new_player_count = new_player_obj.count()
                # new_player_deposit = new_player_obj.aggregate(deposit=Sum('deposit'))['deposit']
                active_player_count = len(active_player_obj.values('playerid').distinct())
                revenue = player_tracker_obj['revenue'] if player_tracker_obj['revenue'] else 0.0
                deposit = player_tracker_obj['deposit']
                cashout = player_tracker_obj['cashout']
                chargeback = player_tracker_obj['chargeback']
                void = player_tracker_obj['void']
                bonus = player_tracker_obj['bonus']
                deposit_list = [f for f in date_player_obj.values('playerid').annotate(Sum('deposit')) if f['deposit__sum']>0.0]
                side_bets_list = [k for k in date_player_obj.values('playerid').annotate(Sum('sidegamesbets')) if k['sidegamesbets__sum']>0.0]
                diposit_accounts = len(deposit_list)
                wagering_accounts = len(side_bets_list)
                if player_tracker_obj['sidegamesbets']:
                    total_wagers = round(player_tracker_obj['sidegamesbets'], 2)
                if player_tracker_obj['sidegamesbets']:
                    total_wagers = round(player_tracker_obj['sidegamesbets'], 2)
                for active_obj in avg_active_players_obj:
                    days_count = active_obj['registrationdate'] - active_obj['lastdate']
                    days_count = days_count.days
                    avg_active_players_days = avg_active_players_days + abs(days_count)
                if active_player_count and days_count and avg_active_players_obj:
                    avg_active_players = avg_active_players_days/len(avg_active_players_obj)
                    avg_active_players = int(avg_active_players)
                if player_tracker_obj['revenue'] and days_count:
                    net_res_list = []
                    net_cash_accounts = date_player_obj.values('playerid').annotate(Sum('sidegamesbets'))
                    for g in net_cash_accounts:
                        if g.get('sidegamesbets__sum') and g.get('sidegamesbets__sum') > 0.0:
                            net_res_list.append(g)
                    if net_res_list:
                        avg_net_cash = float(player_tracker_obj['revenue'])/float(len(net_res_list))
                        avg_net_cash = round(avg_net_cash, 2)
                avg_ltv_accounts = new_player_obj.values('playerid').annotate(Sum('revenue'), Sum('sidegamesbets'))
                avg_ltv_list = []
                revenue_ltv = 0.0
                if avg_ltv_accounts:
                    for g in avg_ltv_accounts:
                        if g.get('sidegamesbets__sum') and g.get('sidegamesbets__sum') > 0.0:
                            avg_ltv_list.append(g)
                            revenue_ltv = revenue_ltv + g.get('revenue__sum')
                if avg_ltv_list:
                    avg_ltv = float(revenue_ltv)/len(avg_ltv_list)
                    avg_ltv = round(avg_ltv, 2)
            
            if content_type == 'UPLOAD':
                revenue = tracker_obj['revenue'] if tracker_obj['revenue'] else 0.0
                deposit = tracker_obj['deposit']
                cashout = tracker_obj['cashout']
                chargeback = tracker_obj['chargeback']
                void = tracker_obj['void']
                bonus = tracker_obj['bonus']


            reg_rate = '0.00'
            ftd_rate = '0.00'
            if tracker_obj['clicks'] and tracker_obj['registrations']:
                reg_rate = float(tracker_obj['registrations'])/float(tracker_obj['clicks'])
                reg_rate = round(reg_rate*100, 2)
                reg_rate = str(reg_rate)
            if tracker_obj['registrations'] and tracker_obj['ftd']:
                ftd_rate = float(tracker_obj['ftd'])/float(tracker_obj['registrations'])
                ftd_rate = round(ftd_rate*100, 2)
                ftd_rate = str(ftd_rate)

            # tracker = i.fk_campaign.name.replace('vcommsubaff', '')

            # deals
            commission = Affiliatecommission.objects.filter(fk_affiliateinfo=obj).select_related('fk_affiliateinfo')
            cpa_value = 0.00
            cpr_value = 0.00
            rev_value = 0.00
            total_value = 0.00
            if commission:
                commission = commission[0]
                # if commission.pocdeduction:
                #     revenue = revenue*(0.85)
                if commission.cpachoice and commission.criteriaacquisition == 'FTD':
                    res_list = []
                    cpa_accounts = ftd_player_obj.values('playerid').annotate(Sum('deposit'))
                    for c in cpa_accounts:
                        if c.get('deposit__sum') and c.get('deposit__sum') >= commission.criteriavalue:
                            res_list.append(c)
                    cpa_acc = len(res_list)
                    cpa_value = (cpa_acc) * float(commission.cpacommissionvalue) if commission.cpacommissionvalue else 0
                    cpa_value = round(cpa_value, 2)
                # elif commission.cpachoice and commission.criteriaacquisition == 'Deposit' and content_type != 'UPLOAD':
                #     res_list = []
                #     cpa_accounts = date_player_obj.values('playerid').annotate(Sum('deposit'))
                #     for c in cpa_accounts:
                #         if c.get('deposit__sum') and c.get('deposit__sum') >= commission.criteriavalue:
                #             res_list.append(c)
                #     cpa_acc = len(res_list)
                #     cpa_value = (cpa_acc) * float(commission.cpacommissionvalue) if commission.cpacommissionvalue else 0
                #     cpa_value = round(cpa_value, 2)
                # if commission.referalchoice:
                #     cpr_value = (tracker_obj['cashout']) * (commission.referalcommissionvalue)
                if commission.revshare and revenue:
                    rev_value = (revenue * (commission.revsharepercent))/100
                    rev_value = round(rev_value, 2)
            total_value = cpa_value + cpr_value + rev_value
            total_value = round(total_value, 2)
            superaffid = obj.superaffiliate.id if obj.superaffiliate else ''
            aff_data = [obj.username.replace('vcommsubaff', ''), superaffid,
                        site.name, site.id, 0, tracker_obj['clicks'] if tracker_obj['clicks'] else 0, 0,
                        tracker_obj['registrations'] if tracker_obj['registrations'] else 0, reg_rate,
                        tracker_obj['ftd'] if tracker_obj['ftd'] else 0, ftd_rate, new_player_count,
                        round(new_player_deposit, 2), active_player_count, diposit_accounts, wagering_accounts, total_wagers,
                        avg_active_players, 0.00, 0.00, 0.00, avg_net_cash,
                        0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00,
                        round(deposit, 2) if deposit else 0.00,
                        round(cashout, 2) if cashout else 0.00,
                        round(chargeback, 2) if chargeback else 0.00, 
                        round(void, 2) if void else 0.00,
                        round(bonus, 2) if bonus else 0.00,
                        round(revenue, 2) if revenue else 0.00, round(avg_ltv, 2) if avg_ltv else 0.00,
                        cpa_value, rev_value, total_value]
            # if len(total_list) == 2:
            #     total_list = total_list + aff_data[2:]
            # else:
            #     total_list = [x + y if not isinstance(x,str) else x for x, y in zip(total_list, aff_data)]
            aff_data = [j if ('.' not in str(j) or isinstance(j,str)) else (currency + str(j)) for j in aff_data]
            aff_data = [k if k != (currency + '0.0') else (currency + '0.00') for k in aff_data]
            data_list.append(aff_data)
    # if len(data_list)>0:
    #     total_list = get_total(data_list)
    #     data_list.append(total_list)
    return (header_list, data_list, currency)


def Process_activedepositor_stats(obj, from_date, to_date, camp_links, site_ids, country):
    data_list = []
    total_list = ['Total', '']
    currency = ''
    currency_dict = {}
    header_list = [
           { "title": "Brand", "width":"200px"},
           { "title": "Player Id", "width":"180px"},
           { "title": "Affiliate ID", "width":"180px"},
           { "title": "ClickID", "width":"180px"},
           { "title": "Dynamic", "width":"180px"},
           { "title": "Campaign Name", "width":"180px"},
           { "title": "Registration Date", "width":"200px"},
           { "title": "FTD Date", "width":"160px"},
           { "title": "Total Deposits", "width":"180px"},
           { "title": "Charge Backs", "width":"180px"},
           { "title": "First Played", "width":"180px"},
           { "title": "Last Played", "width":"180px"},
           { "title": "Bets", "width":"120px"},
           { "title": "Wins", "width":"120px"},
           { "title": "Bonus", "width":"120px"},
           { "title": "Revenue", "width":"130px"},
           { "title": "Adjustments", "width":"150px"},
           # { "title": "Bingo Revenue"},
           # { "title": "Games Revenue"},
           { "title": "CPA Commission", "width":"180px"},
           { "title": "Revshare Commission", "width":"220px"},
           { "title": "Total Commission", "width":"200px"},
           ]
    player_id_list = []
    currency = ''
    currency_dict = get_currency_dict(site_ids)
    # commission_dict = get_commission_dict(obj, camp_links, aff_list, site_ids)
    # if i.fk_campaign.siteid in currency_dict:
    #     currency = currency_dict[i.fk_campaign.siteid]
    # else:
    #     currency_obj = Currency.objects.filter(siteid=i.fk_campaign.siteid)
    #     currency_dict[i.fk_campaign.siteid] = currency_obj[0].currency if currency_obj else ''
    #     currency = currency_dict[i.fk_campaign.siteid]
    newplayer_list = []
    # netw_site_obj = NetworkAndSiteMap.objects.get(siteid=i.fk_campaign.siteid)
    # content_type = netw_site_obj.networkid.connect_type
    # post_new_player_obj = PostBackPlayer.objects.filter(fk_campaign__in=camp_links,
    #                 registrationdate__gte=str(from_date),
    #                 registrationdate__lte=str(to_date),
    #                 siteid__in=site_ids)
    post_active_player_objs = PostBackPlayer.objects.filter(fk_campaign__in=camp_links,
                    lastdate__gte=str(from_date),
                    lastdate__lte=str(to_date),
                    siteid__in=site_ids).select_related(
                    'fk_campaign', 'fk_campaign__fk_campaign', 'siteid').order_by('-date')
    # api_new_player_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
    #                     registrationdate__gte=str(from_date),
    #                     registrationdate__lte=str(to_date),
    #                     siteid__in=site_ids)
    api_active_player_objs = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
                    date__gte=str(from_date).split(' ')[0],
                    date__lte=str(to_date).split(' ')[0],
                    country__in=country,
                    siteid__in=site_ids).select_related('fk_campaign', 
                    'fk_campaign__fk_affiliateinfo',
                    'fk_campaign__fk_campaign'
                    ,'siteid').order_by('-date')
    players_dict = {}
    deposit_list = [f['playerid'] for f in api_active_player_objs.values('playerid').annotate(Sum('deposit')) if f['deposit__sum']>0.0]
    for active_player_obj in api_active_player_objs:
        if active_player_obj.playerid not in player_id_list:
            players_dict[active_player_obj.playerid] = {
                                'brand': active_player_obj.siteid.name,
                                'siteid': active_player_obj.siteid,
                                'dynamic': active_player_obj.dynamic,
                                'wdynamic': active_player_obj.wdynamic,
                                'clickid': active_player_obj.clickid,
                                'country': active_player_obj.country,
                                'aff_id': active_player_obj.fk_campaign.fk_affiliateinfo.id,
                                'aff_name': active_player_obj.fk_campaign.fk_affiliateinfo.email,
                                'camp_name': active_player_obj.fk_campaign.fk_campaign.name,
                                'registrationdate': active_player_obj.registrationdate.strftime("%d-%m-%Y") if active_player_obj.registrationdate else '',
                                'ftddate':active_player_obj.ftddate.strftime("%d-%m-%Y") if active_player_obj.ftddate else '',
                                'deposit': active_player_obj.deposit,
                                'chargeback': active_player_obj.chargeback,
                                'firstplayed':active_player_obj.ftddate.strftime("%d-%m-%Y") if active_player_obj.ftddate else '',
                                'lastdate': active_player_obj.lastdate.strftime("%d-%m-%Y") if active_player_obj.lastdate else '',
                                'bets':active_player_obj.sidegamesbets,
                                'wins': active_player_obj.sidegameswins,
                                'bonus':active_player_obj.bonuses,
                                'revenue': active_player_obj.revenue,
                                }
        else:
            players_dict[active_player_obj.playerid]['deposit'] += active_player_obj.deposit
            players_dict[active_player_obj.playerid]['chargeback'] += active_player_obj.chargeback
            players_dict[active_player_obj.playerid]['bets'] += active_player_obj.sidegamesbets
            players_dict[active_player_obj.playerid]['wins'] += active_player_obj.sidegameswins
            players_dict[active_player_obj.playerid]['bonus'] += active_player_obj.bonuses
            players_dict[active_player_obj.playerid]['revenue'] += active_player_obj.revenue
        player_id_list.append(active_player_obj.playerid)
    for pid, values_obj in players_dict.iteritems():
        currency = currency_dict[values_obj['siteid']]
        aff_data = [values_obj['brand'], pid, values_obj['aff_id'],
                values_obj['clickid'], values_obj['dynamic'],
                values_obj['camp_name'], values_obj['registrationdate'], values_obj['ftddate'],
                (currency + str(round(values_obj['deposit'],2)) if '-' not in str(values_obj['deposit']) else '-'+currency+str(round(values_obj['deposit'],2)).replace('-','')) if values_obj['deposit'] else currency+'0.00', 
                (currency + str(round(values_obj['chargeback'],2)) if '-' not in str(values_obj['chargeback']) else '-'+currency+str(round(values_obj['chargeback'],2)).replace('-','')) if values_obj['chargeback'] else currency+'0.00',
                values_obj['firstplayed'], values_obj['lastdate'],
                (currency + str(round(values_obj['bets'],2)) if '-' not in str(values_obj['bets']) else '-'+currency+str(round(values_obj['bets'],2)).replace('-','')) if values_obj['bets'] else currency+'0.00',
                (currency + str(round(values_obj['wins'],2)) if '-' not in str(values_obj['wins']) else '-'+currency+str(round(values_obj['wins'],2)).replace('-','')) if values_obj['wins'] else currency+'0.00',
                (currency + str(round(values_obj['bonus'],2)) if '-' not in str(values_obj['bonus']) else '-'+currency+str(round(values_obj['bonus'],2)).replace('-','')) if values_obj['bonus'] else currency+'0.00',
                (currency + str(round(values_obj['revenue'],2)) if '-' not in str(values_obj['revenue']) else '-'+currency+str(round(values_obj['revenue'],2)).replace('-','')) if values_obj['revenue'] else currency+'0.00',
                currency+'0.00', currency+'0.00', currency+'0.00', currency+'0.00']
        # if len(total_list) == 2:
        #     total_list = total_list + aff_data[2:]
        # else:
        #     total_list = [x + y if (not isinstance(x,str) and not isinstance(x,datetime.datetime)) else x for x, y in zip(total_list, aff_data)]
        # aff_data = [j if ('.' not in str(j) or isinstance(j,str) and not isinstance(j,datetime)) else (currency + str(j)) for j in aff_data]
        # aff_data = [k if k != (currency + '0.0') else (currency + '0.00') for k in aff_data]
        data_list.append(aff_data)
    # total_list = get_total(data_list)
    # if len(data_list) > 0:
    #     total_list = [j if '.' not in str(j) else (currency + str(j)) for j in total_list]
    #     total_list[6] =  round(sum([float(_i[6]) for _i in data_list])/len(data_list), 2)
    #     total_list[8] = round(sum([float(_i[8]) for _i in data_list])/len(data_list), 2)
    #     #total_list = [k if k != (currency + '0.0') else (currency + '0.00') for k in total_list]
    #     data_list.append(total_list)
    # data_list = [[j if '.' not in str(j) else (currency + str(j)) for j in i] for i in data_list]
    return (header_list, data_list, currency)

def Process_conversion_stats(obj, from_date, to_date, camp_links, site_ids, country, currencyfilter, group_d):
    data_list = []
    currency = ''
    currency_dict = {}
    total_list = ['Total', '']
    header_list = [
           # { "title": "Brand", "width":"200px"},
           # { "title": "Campaign ID"},
           # { "title": "Campaign name", "width":"200px"},
           # { "title": "Affiliate name", "width":"200px"},
           { "title": "Group by", "width":"140px"},
           { "title": "Impressions", "width":"140px"},
           { "title": "Clicks", "width":"120px"},
           { "title": "CTR", "width":"120px"},
           { "title": "Registrations", "width":"200px"},
           { "title": "Registration rate %", "width":"180px"},
           { "title": "FTD", "width":"120px"},
           { "title": "Conversion rate %", "width":"200px"},
           ]
    data_list, currency = group_by_results(from_date, to_date, site_ids, camp_links, group_d,
                 currencyfilter, 'conversion', country)
    return (header_list, data_list, currency)


def Process_campaign_camp_admin_stats(obj, from_date, to_date, camp_links,
                    site_ids, aff_list, country, currencyfilter=None):
    country_list = []
    if country and type(country) == str:
        country_list = [country]
        country_id_list = '('+','.join(country)+')'
    else:
        # country_list = cache.get('country_list')
        if not country_list:
            country_list = [i.countryname for i in Country.objects.all()]
            # cache.set('country_list', country_list, cache_time)
    data_list = []
    # currency_dict = get_currency_dict(site_ids)
    currency_sites = {}
    currency_objs = Currency.objects.filter(siteid__in=site_ids)
    for cur in currency_objs:
        if cur.currency in currency_sites:
            currency_sites[cur.currency].append(int(cur.siteid.id))
        else:
            currency_sites[cur.currency] = [int(cur.siteid.id)]
    if not currency_sites.get(u'\xa3'):
        currency_sites[u'\xa3'] = []
    if not currency_sites.get(u'\u20ac'):
        currency_sites[u'\u20ac'] = []
    data_list = []
    currency = ''
    currency_dict = {}
    total_list = ['Total', '']
    header_list = [
           # { "title": "Brand", "width":"200px"},
           { "title": "Grouped by", "width":"200px"},
           # { "title": "Country"},
           # { "title": "Campaign name", "width":"200px"},
           # { "title": "Affiliate name", "width":"200px"},
           { "title": "Impressions", "width":"140px"},
           { "title": "Clicks", "width":"120px"},
           { "title": "CTR", "width":"120px"},
           { "title": "Registrations", "width":"200px"},
           { "title": "Registration rate %", "width":"180px"},
           { "title": "FTD", "width":"120px"},
           { "title": "Conversion rate %", "width":"200px"},
           { "title": "New players", "width":"160px"},
           { "title": "New player Deposits", "width":"240px"},
           { "title": "Active players", "width":"200px"},
           { "title": "Depositing accounts", "width":"240px"},
           { "title": "Wagering accounts", "width":"230px"},
           { "title": "Total Wagers", "width":"180px"},
           { "title": "Avg active days", "width":"200px"},
           { "title": "Net per player", "width":"180px"},
           { "title": "Deposits", "width":"130px"},
           # { "title": "Cashout", "width":"130px"},
           { "title": "Chargeback", "width":"150px"},
           { "title": "Void", "width":"120px"},
           { "title": "Bonus", "width":"120px"},
           { "title": "Revenue", "width":"150px"},
           { "title": "Avg. Player LTV", "width":"200px"},
           { "title": "CPA Commission", "width":"200px"},
           { "title": "Revshare Commission", "width":"220px"},
           { "title": "Total Commission", "width":"180px"},
           ]
    currency_dict = get_currency_dict(site_ids)
    commission_dict = get_commission_dict(obj, camp_links, aff_list, site_ids)
    i = [int(j.id) for j in camp_links]
    tup_i = '(-1)'
    if i:
        tup_i = '('+str(functools.reduce(lambda a,b : str(a)+','+str(b),i))+')'
    upload_tracker_obj = {}
    upload_tracker_obj = UploadAdvanced.objects.filter(fk_campaign__in=camp_links,
                    date__gte=str(from_date).split(' ')[0],
                    date__lte=str(to_date).split(' ')[0],
                    siteid__in=site_ids,
                    processed=True).values(
                        'fk_campaign').annotate(Sum('ftd'),
                    Sum('registrations'), Sum('clicks'))
    upload_tracker_obj = {i['fk_campaign']:i for i in upload_tracker_obj}
    upload_player_tracker_obj = {}
    upload_new_player_obj = {}
    upload_active_player_obj = {}
    upload_date_player_obj = {}
    upload_ftd_player_obj = {}
    upload_avg_active_players_obj = {}
    # tracker_obj = get_cliks_data([], from_date, to_date, site_ids, [i])
    api_clicks = CampaignClicksCount.objects.filter(fk_campaign__in=camp_links,
                            date__gte=str(from_date).split(' ')[0],
                            date__lte=str(to_date).split(' ')[0],
                            fk_campaign__fk_campaign__siteid__in=site_ids).values(
                            'fk_campaign').annotate(Sum('clicks'))
    api_clicks = {i['fk_campaign']:i for i in api_clicks}
    api_registrations = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links, 
                        registrationdate__gte=str(from_date),
                        registrationdate__lte=str(to_date),
                        country__in=country_list,
                        siteid__in=site_ids).values('fk_campaign').annotate(Count('playerid', 
                                 distinct=True))
    api_registrations = {i['fk_campaign']:i for i in api_registrations}
    api_ftds = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links, 
                        ftddate__gte=str(from_date),
                        ftddate__lte=str(to_date),
                        country__in=country_list,
                        siteid__in=site_ids,deposit__gt=0
                        ).values('fk_campaign').annotate(Count('playerid', 
                                 distinct=True))
    api_ftds = {i['fk_campaign']:i for i in api_ftds}
    api_player_tracker_obj = {}
    if currencyfilter == 'gbp':
        currency = u'\xa3'
    else:
        currency = u'\u20ac'
    if currency_sites[u'\u20ac']:
        euro_sites = '('+str(functools.reduce(lambda a,b : str(a)+','+str(b),currency_sites[u'\u20ac']))+')'
        euro_sql_query = """
                    select p.date,p.fk_campaign_id, p.siteid_id,
                    sum(p.registrations), sum(p.ftd),
                    sum(p.revenue),sum(p.revenue*c.eurtogbp),
                    sum(p.deposit),sum(p.deposit*c.eurtogbp),
                    sum(p.chargeback),sum(p.chargeback*c.eurtogbp),
                    sum(p.cashout),sum(p.cashout*c.eurtogbp),
                    sum(p.void),sum(p.void*c.eurtogbp),
                    sum(p.bonuses),sum(p.bonuses*c.eurtogbp),
                    sum(p.reversechargeback),sum(p.reversechargeback*c.eurtogbp),
                    sum(p.sidegamesbets),sum(p.sidegamesbets*c.eurtogbp),
                    sum(p.sidegameswins),sum(p.sidegameswins*c.eurtogbp) 
                    from connectors_ppplayerprocessed p join userapp_currencyconverted c on c.date=CAST(p.date AS DATE)
                     group by p.fk_campaign_id, p.date having p.date >= '%s' and p.date < '%s' and
                     p.siteid_id IN %s and p.fk_campaign_id IN %s 
                """%(str(from_date).split(' ')[0],
                    str(to_date).split(' ')[0], euro_sites, 
                    tup_i)
        if country:
            euro_sql_query += euro_sql_query + 'and p.country IN %s'%(country_id_list)
        euro_sql_query += ';'
        euro_camp_list = []
        cursor = connection.cursor()
        euro_set = cursor.execute(euro_sql_query)
        col_names = [desc[0] for desc in cursor.description]
        while True:
            row = cursor.fetchone()
            if row is None:
                break
            row_dict = dict(zip(col_names, row))
            euro_camp_list.append(row_dict)
        for i in euro_camp_list:
            if currencyfilter == 'gbp':
                currency = u'\xa3'
                dict_i = {(j.replace('sum(p.','').replace(')','__sum__togbp') if '*' not in j else j.replace('sum(p.','').replace('*c.eurtogbp)','__sum')):i[j] for j in i}
            else:
                currency = u'\u20ac'
                dict_i = {(j.replace('sum(p.','').replace(')','__sum') if '*' not in j else j.replace('sum(p.','').replace('*c.eurtogbp)','__sum__togbp')):i[j] for j in i}
            if api_player_tracker_obj.get(dict_i['fk_campaign_id']):
                for k in dict_i:
                    if type(dict_i[k]) in [int, float]:
                        api_player_tracker_obj[dict_i['fk_campaign_id']][k] += dict_i[k]
            else:
                api_player_tracker_obj[dict_i['fk_campaign_id']] = dict_i

        euro_sql_query = """
                    select p.date,p.fk_campaign_id, p.siteid_id,
                    sum(p.revenue),sum(p.revenue*c.eurtogbp),
                    sum(p.deposit),sum(p.deposit*c.eurtogbp),
                    sum(p.chargeback),sum(p.chargeback*c.eurtogbp),
                    sum(p.cashout),sum(p.cashout*c.eurtogbp),
                    sum(p.void),sum(p.void*c.eurtogbp),
                    sum(p.bonus),sum(p.bonus*c.eurtogbp)
                     from connectors_uploadadvanced p join userapp_currencyconverted c on c.date=CAST(p.date AS DATE)
                     group by p.fk_campaign_id, p.date having p.date >= '%s' and p.date < '%s' and
                     p.siteid_id IN %s and p.fk_campaign_id IN %s;
                """%(str(from_date).split(' ')[0],
                    str(to_date).split(' ')[0], euro_sites, 
                    tup_i)
        euro_camp_list = []
        cursor = connection.cursor()
        euro_set = cursor.execute(euro_sql_query)
        col_names = [desc[0] for desc in cursor.description]
        while True:
            row = cursor.fetchone()
            if row is None:
                break
            row_dict = dict(zip(col_names, row))
            euro_camp_list.append(row_dict)
        for i in euro_camp_list:
            if currencyfilter == 'gbp':
                currency = u'\xa3'
                dict_i = {(j.replace('sum(p.','').replace(')','__sum__togbp') if '*' not in j else j.replace('sum(p.','').replace('*c.eurtogbp)','__sum')):i[j] for j in i}
            else:
                currency = u'\u20ac'
                dict_i = {(j.replace('sum(p.','').replace(')','__sum') if '*' not in j else j.replace('sum(p.','').replace('*c.eurtogbp)','__sum__togbp')):i[j] for j in i}
            if upload_tracker_obj.get(dict_i['fk_campaign_id']):
                for k in dict_i:
                    if type(dict_i[k]) not in [int, float]:
                        continue
                    if upload_tracker_obj[dict_i['fk_campaign_id']].get(k):
                        upload_tracker_obj[dict_i['fk_campaign_id']][k] += dict_i[k]
                    else:
                        upload_tracker_obj[dict_i['fk_campaign_id']][k] = dict_i[k]
            else:
                upload_tracker_obj[dict_i['fk_campaign_id']] = dict_i

    if currency_sites[u'\xa3']:
        gbp_sites = '('+str(functools.reduce(lambda a,b : str(a)+','+str(b),currency_sites[u'\xa3']))+')'
        gbp_sql_query = """
                    select p.date, p.fk_campaign_id, p.siteid_id,
                    sum(p.registrations), sum(p.ftd),
                    sum(p.revenue),sum(p.revenue*c.gbptoeur),
                    sum(p.deposit),sum(p.deposit*c.gbptoeur),
                    sum(p.chargeback),sum(p.chargeback*c.gbptoeur),
                    sum(p.cashout),sum(p.cashout*c.gbptoeur),
                    sum(p.void),sum(p.void*c.gbptoeur),
                    sum(p.bonuses),sum(p.bonuses*c.gbptoeur),
                    sum(p.reversechargeback),sum(p.reversechargeback*c.gbptoeur),
                    sum(p.sidegamesbets),sum(p.sidegamesbets*c.gbptoeur),
                    sum(p.sidegameswins),sum(p.sidegameswins*c.gbptoeur)  
                    from connectors_ppplayerprocessed p join userapp_currencyconverted c on c.date=CAST(p.date AS DATE)
                     group by p.fk_campaign_id , p.date having p.date >= '%s' and p.date< '%s' and
                     p.siteid_id IN %s and p.fk_campaign_id IN %s;
                    """%(str(from_date).split(' ')[0],
                        str(to_date).split(' ')[0], gbp_sites, 
                    tup_i)
        if country:
            gbp_sql_query += gbp_sql_query + 'and p.country IN %s'%(country_id_list)
        gbp_sql_query += ';'
        gbp_camp_list = []
        cursor = connection.cursor()
        gbp_set = cursor.execute(gbp_sql_query)
        col_names = [desc[0] for desc in cursor.description]
        while True:
            row = cursor.fetchone()
            if row is None:
                break
            row_dict = dict(zip(col_names, row))
            gbp_camp_list.append(row_dict)
        for i in gbp_camp_list:
            if currencyfilter == 'euro':
                dict_i = {(j.replace('sum(p.','').replace(')','__sum__toeuro') if '*' not in j else j.replace('sum(p.','').replace('*c.gbptoeur)','__sum')):i[j] for j in i}
            else:
                dict_i = {(j.replace('sum(p.','').replace(')','__sum') if '*' not in j else j.replace('sum(p.','').replace('*c.gbptoeur)','__sum__toeuro')):i[j] for j in i}
            if api_player_tracker_obj.get(dict_i['fk_campaign_id']):
                for k in dict_i:
                    if type(dict_i[k]) in [int, float]:
                        api_player_tracker_obj[dict_i['fk_campaign_id']][k] += dict_i[k]
            else:
                api_player_tracker_obj[dict_i['fk_campaign_id']] = dict_i

        gbp_sql_query = """
                    select p.date, p.fk_campaign_id, p.siteid_id,
                    sum(p.revenue),sum(p.revenue*c.gbptoeur),
                    sum(p.deposit),sum(p.deposit*c.gbptoeur),
                    sum(p.chargeback),sum(p.chargeback*c.gbptoeur),
                    sum(p.cashout),sum(p.cashout*c.gbptoeur),
                    sum(p.void),sum(p.void*c.gbptoeur),
                    sum(p.bonus),sum(p.bonus*c.gbptoeur)
                     from connectors_uploadadvanced p join userapp_currencyconverted c on c.date=CAST(p.date AS DATE)
                     group by p.fk_campaign_id , p.date having p.date >= '%s' and p.date< '%s' and
                     p.siteid_id IN %s and p.fk_campaign_id IN %s;
                    """%(str(from_date).split(' ')[0],
                        str(to_date).split(' ')[0], gbp_sites, 
                    tup_i)
        gbp_camp_list = []
        cursor = connection.cursor()
        gbp_set = cursor.execute(gbp_sql_query)
        col_names = [desc[0] for desc in cursor.description]
        while True:
            row = cursor.fetchone()
            if row is None:
                break
            row_dict = dict(zip(col_names, row))
            gbp_camp_list.append(row_dict)
        for i in gbp_camp_list:
            if currencyfilter == 'euro':
                dict_i = {(j.replace('sum(p.','').replace(')','__sum__toeuro') if '*' not in j else j.replace('sum(p.','').replace('*c.gbptoeur)','__sum')):i[j] for j in i}
            else:
                dict_i = {(j.replace('sum(p.','').replace(')','__sum') if '*' not in j else j.replace('sum(p.','').replace('*c.gbptoeur)','__sum__toeuro')):i[j] for j in i}
            if upload_tracker_obj.get(dict_i['fk_campaign_id']):
                for k in dict_i:
                    if type(dict_i[k]) in [int, float]:
                        if upload_tracker_obj[dict_i['fk_campaign_id']].get(k):
                            upload_tracker_obj[dict_i['fk_campaign_id']][k] += dict_i[k]
                        else:
                            upload_tracker_obj[dict_i['fk_campaign_id']][k] = dict_i[k]
            else:
                upload_tracker_obj[dict_i['fk_campaign_id']] = dict_i
    
    # api_player_tracker_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
    #                         date__gte=str(from_date).split(' ')[0],
    #                         date__lte=str(to_date).split(' ')[0],
    #                         siteid__in=site_ids).values('fk_campaign').annotate(Sum('deposit'),Sum('chargeback'),
    #                         Sum('cashout'),Sum('void'),Sum('reversechargeback'),
    #                         Sum('bonuses'),
    #                         Sum('sidegamesbets'),Sum('sidegameswins'),
    #                         Sum('jackpotcontribution'),Sum('revenue'))
    # api_player_tracker_obj = {i['fk_campaign']:i for i in api_player_tracker_obj}
    api_new_player_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links, 
                        registrationdate__gte=str(from_date),
                        registrationdate__lte=str(to_date),
                        country__in=country_list,
                        siteid__in=site_ids,deposit__gt=0).values(
                        'fk_campaign').annotate(Sum('deposit'), Sum('revenue'))
    api_new_player_obj = {i['fk_campaign']:i for i in api_new_player_obj}
    api_active_player_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
                        lastdate__gte=str(from_date),
                        lastdate__lte=str(to_date),
                        country__in=country_list,
                        siteid__in=site_ids).values('fk_campaign').annotate(Count('playerid', 
                                 distinct=True))
    api_active_player_obj = {i['fk_campaign']:i for i in api_active_player_obj}
    api_date_deposit_player_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
                        date__gte=str(from_date).split(' ')[0],
                        date__lte=str(to_date).split(' ')[0],
                        country__in=country_list,
                        siteid__in=site_ids,deposit__gt=0).values(
                        'fk_campaign').annotate(Count('playerid', 
                                 distinct=True))
    api_date_deposit_player_obj = {i['fk_campaign']:i for i in api_date_deposit_player_obj}
    api_date_side_player_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
                        date__gte=str(from_date).split(' ')[0],
                        date__lte=str(to_date).split(' ')[0],
                        country__in=country_list,
                        siteid__in=site_ids,sidegamesbets__gt=0).values(
                        'fk_campaign').annotate(Count('playerid', 
                                 distinct=True))
    api_date_side_player_obj = {i['fk_campaign']:i for i in api_date_side_player_obj}

    api_new_side_player_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links, 
                        registrationdate__gte=str(from_date),
                        registrationdate__lte=str(to_date),
                        country__in=country_list,
                        siteid__in=site_ids,sidegamesbets__gt=0).values(
                        'fk_campaign').annotate(Sum('sidegamesbets'))
    api_new_side_player_obj = {i['fk_campaign']:i for i in api_new_side_player_obj}

    
    api_avg_active_players_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
                        registrationdate__gte=str(from_date).split(' ')[0],
                        lastdate__lte=str(to_date).split(' ')[0],
                        country__in=country_list,
                        siteid__in=site_ids).values(
                        'fk_campaign').annotate(Count('playerid', 
                                 distinct=True))
    api_avg_active_players_obj = {i['fk_campaign']:i for i in api_avg_active_players_obj}
    ftd_deposit_list = PPPlayerProcessed.objects.filter(fk_campaign=camp_links,
                ftddate__gte=str(from_date).split(' ')[0],
                ftddate__lte=str(to_date),
                siteid__in=site_ids,
                ftddate = F('date')
                ).values('playerid', 'deposit', 'fk_campaign').distinct()
    ftd_dict_deposit = {}
    for d in ftd_deposit_list:
        if int(d['fk_campaign']) in ftd_dict_deposit:
            ftd_dict_deposit[int(d['fk_campaign'])].append(d)
        else:
            ftd_dict_deposit[int(d['fk_campaign'])] = [d]
    for i in camp_links:
        # currency = currency_dict[i.fk_campaign.siteid]
        newplayer_list = []
        new_player_count = 0
        new_player_deposit = 0.00
        active_player_count= 0
        diposit_accounts = 0
        wagering_accounts = 0
        total_wagers = 0.00
        cpa_value = 0.00
        cpr_value = 0.00
        rev_value = 0.00
        total_value = 0.00
        days_count = to_date - from_date
        days_count = days_count.days
        avg_active_players_days = 0
        avg_active_players = 0
        avg_net_cash = 0.00
        avg_ltv = 0.00
        revenue = 0.0
        deposit = 0.0
        cashout = 0.0
        chargeback = 0.0
        void = 0.0
        bonus = 0.0
        registrations = 0
        ftds = 0
        clicks = 0
        cpa_acc = 0
        avg_ltv_list = []
        revenue_ltv = 0.0
        content_type = 'POST_BACK'
        clicks = api_clicks[i.id]['clicks__sum'] if api_clicks.get(i.id) else 0
        if api_player_tracker_obj.get(i.id):
            # cpa_accounts = new_player_obj.values('playerid').annotate(Sum('deposit'))
            # for c in cpa_accounts:
            #     newplayer_list.append(c)
            
            # if newplayer_list:
            #     new_player_count = len(newplayer_list)
            #     for j in newplayer_list:
            #         new_player_deposit = new_player_deposit + j['deposit__sum']
            # new_player_count = new_player_obj.count()
            if api_new_player_obj.get(i.id):
                new_player_deposit = api_new_player_obj.get(i.id)['deposit__sum']
                avg_ltv = (api_new_player_obj[i.id]['revenue__sum']/api_new_side_player_obj[i.id]['sidegamesbets__sum'])
            if api_active_player_obj.get(i.id):
                active_player_count = api_active_player_obj.get(i.id)['playerid__count']
            revenue = api_player_tracker_obj[i.id]['revenue__sum']
            deposit = api_player_tracker_obj[i.id]['deposit__sum']
            cashout = api_player_tracker_obj[i.id]['cashout__sum']
            chargeback = api_player_tracker_obj[i.id]['chargeback__sum']
            void = api_player_tracker_obj[i.id]['void__sum']
            bonus = api_player_tracker_obj[i.id]['bonuses__sum']
            # deposit_list = [f for f in date_player_obj.values('playerid').annotate(Sum('deposit')) if f['deposit__sum']>0.0]
            # side_bets_list = [k for k in date_player_obj.values('playerid').annotate(Sum('sidegamesbets')) if k['sidegamesbets__sum']>0.0]
            if api_date_deposit_player_obj.get(i.id):
                diposit_accounts = api_date_deposit_player_obj[i.id]['playerid__count']
            if api_date_side_player_obj.get(i.id):
                wagering_accounts = api_date_side_player_obj[i.id]['playerid__count']
                avg_net_cash = round(revenue/api_date_side_player_obj[i.id]['playerid__count'],2)
            if api_player_tracker_obj.get(i.id) and api_player_tracker_obj.get(i.id).get('sidegamesbets__sum'):
                total_wagers = round(api_player_tracker_obj[i.id]['sidegamesbets__sum'], 2)
            if api_avg_active_players_obj.get(i.id):
                avg_active_players = int(api_avg_active_players_obj[i.id]['playerid__count'])            
            if api_registrations.get(i.id):
                registrations = api_registrations[i.id]['playerid__count']
            if api_ftds.get(i.id):
                ftds = api_ftds[i.id]['playerid__count']
        if upload_tracker_obj.get(i.id):
            content_type = 'UPLOAD'
            if upload_tracker_obj[i.id].get('revenue__sum'):
                revenue_c = upload_tracker_obj[i.id]['revenue__sum']
            if upload_tracker_obj[i.id].get('deposit__sum'):
                deposit += upload_tracker_obj[i.id]['deposit__sum']
            if upload_tracker_obj[i.id].get('cashout__sum'):
                cashout += upload_tracker_obj[i.id]['cashout__sum']
            if upload_tracker_obj[i.id].get('chargeback__sum'):
                chargeback += upload_tracker_obj[i.id]['chargeback__sum']
            if upload_tracker_obj[i.id].get('void__sum'):
                void += upload_tracker_obj[i.id]['void__sum']
            if upload_tracker_obj[i.id].get('bonus__sum'):
                bonus += upload_tracker_obj[i.id]['bonus__sum']
            registrations = upload_tracker_obj[i.id]['registrations__sum']
            ftds = upload_tracker_obj[i.id]['ftd__sum']
            clicks = clicks + upload_tracker_obj[i.id]['clicks__sum']
            cpa_acc = ftds

        reg_rate = '0.00'
        ftd_rate = '0.00'
        if registrations and clicks:
            reg_rate = float(registrations)/float(clicks)
            reg_rate = round(reg_rate*100, 2)
            reg_rate = str(reg_rate)
        if registrations and ftds:
            ftd_rate = float(ftds)/float(registrations)
            ftd_rate = round(ftd_rate*100, 2)
            ftd_rate = str(ftd_rate)

        tracker = i.fk_campaign.name.replace('vcommsubaff', '')

        # deals
        commission = commission_dict.get(i.id) if commission_dict.get(i.id) else 0
        if commission:
            if not commission['cpa'][1]:
                commission['cpa'][1] = 0
            # if commission['pocdeduction']:
            #     revenue = (revenue - (revenue*0.21))
            if commission['cpa'] and commission['cpa'][0] == 'FTD' and commission['cpa'][2]:
                res_list = []
                if not ftd_dict_deposit.get(int(i.id)):
                    api_ftd_player_obj = []
                else:
                    api_ftd_player_obj = [j for j in ftd_dict_deposit[int(i.id)] if j['deposit'] > commission['cpa'][1]]
                # api_ftd_player_obj = PPPlayerProcessed.objects.filter(fk_campaign=i,
                #                     ftddate__gte=str(from_date).split(' ')[0],
                #                     ftddate__lte=str(to_date),
                #                     country__in=country,
                #                     siteid__in=site_ids,deposit__gt=commission['cpa'][1],
                #                     ftddate = F('date')
                #                     ).values('playerid').distinct()
                cpa_acc = cpa_acc + len(api_ftd_player_obj)
                cpa_value = (cpa_acc) * float(commission['cpa'][2])
                cpa_value = round(cpa_value, 2)
            # elif commission['cpa'] and commission['cpa'][0] == 'FTD' and commission['cpa'][2] and content_type != 'UPLOAD':
            #     res_list = []
            #     api_ftd_player_obj = PPPlayerProcessed.objects.filter(fk_campaign=i,
            #                         date__gte=str(from_date).split(' ')[0],
            #                         date__lte=str(to_date).split(' ')[0],
            #                         siteid__in=site_ids,deposit__gt=commission['cpa'][1]
            #                         ).values('playerid').distinct()
            #     cpa_acc = len(api_ftd_player_obj)
            #     cpa_value = (cpa_acc) * float(commission['cpa'][2])
            #     cpa_value = round(cpa_value, 2)
            # if commission.referalchoice:
            #     cpr_value = (tracker_obj['cashout']) * (commission.referalcommissionvalue)
            if commission['revshare'] and revenue:
                rev_value = (revenue * (commission['revshare']))/100
                rev_value = round(rev_value, 2)
        total_value = cpa_value + cpr_value + rev_value
        total_value = round(total_value, 2)

        aff_data = [tracker, 0, clicks, 0,
                    registrations, reg_rate,
                    ftds, ftd_rate, new_player_count,
                    round(new_player_deposit, 2), active_player_count, diposit_accounts, wagering_accounts, total_wagers,
                    avg_active_players, avg_net_cash,
                    round(deposit, 2) if deposit else 0.00,
                    round(cashout, 2) if cashout else 0.00,
                    round(chargeback, 2) if chargeback else 0.00, 
                    round(void, 2) if void else 0.00,
                    round(bonus, 2) if bonus else 0.00,
                    round(revenue, 2) if revenue else 0.00, round(avg_ltv,2) if avg_ltv else 0.00,
                    cpa_value, rev_value, total_value]
        # if len(total_list) == 2:
        #     total_list = total_list + aff_data[2:]
        # else:
        #     total_list = [x + y if not isinstance(x,str) else x for x, y in zip(total_list, aff_data)]
        aff_data = [j if ('.' not in str(j) or isinstance(j,str)) else (currency + str(j)) for j in aff_data]
        aff_data = [k if k != (currency + '0.0') else (currency + '0.00') for k in aff_data]
        data_list.append(aff_data)
    # total_list = get_total(data_list)
    # if len(data_list) > 0:
    #     total_list = [j if '.' not in str(j) else (currency + str(j)) for j in total_list]
    #     total_list[6] =  round(sum([float(_i[6]) for _i in data_list])/len(data_list), 2)
    #     total_list[8] = round(sum([float(_i[8]) for _i in data_list])/len(data_list), 2)
    #     #total_list = [k if k != (currency + '0.0') else (currency + '0.00') for k in total_list]
    #     data_list.append(total_list)
    # data_list = [[j if '.' not in str(j) else (currency + str(j)) for j in i] for i in data_list]
    return (header_list, data_list, currency)

from django.db import connection
from django.db.models import Sum, Count
def accounts_data_def(from_date, to_date, site_ids, camp_links, group_d, currencyfilter, report_type, country):
    if not country:
        country = [i.countryname for i in Country.objects.all()]
        country.append(None)
        country.append('')
    api_active_player_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
                            lastdate__gte=str(from_date),
                            lastdate__lte=str(to_date),
                            siteid__in=site_ids,
                            country__in=country)
    api_date_deposit_player_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
                        date__gte=str(from_date).split(' ')[0],
                        date__lte=str(to_date).split(' ')[0],
                        siteid__in=site_ids,deposit__gt=0,
                        country__in=country)
    api_date_side_player_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
                        date__gte=str(from_date).split(' ')[0],
                        date__lte=str(to_date).split(' ')[0],
                        siteid__in=site_ids,sidegamesbets__gt=0,
                        country__in=country)
    api_new_side_player_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links, 
                        registrationdate__gte=str(from_date),
                        registrationdate__lte=str(to_date),
                        siteid__in=site_ids,sidegamesbets__gt=0,
                        country__in=country)
    api_avg_active_players_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
                        registrationdate__gte=str(from_date).split(' ')[0],
                        lastdate__lte=str(to_date).split(' ')[0],
                        siteid__in=site_ids,
                        country__in=country)
    add_list = {'active_players': api_active_player_obj, 
                'deposit_players': api_date_deposit_player_obj, 
                'side_players': api_date_side_player_obj, 
                'new_side_player': api_new_side_player_obj,
                'avg_active_players': api_avg_active_players_obj}
    final_dict = {'active_players': {}, 
                'deposit_players': {},
                'side_players': {},
                'new_side_player': {},
                'avg_active_players': {}}
    if group_d == 'month':
        truncate_date = connection.ops.date_trunc_sql('month', 'date')
        for keys, d_app in add_list.iteritems():
            d_app = d_app.extra({'date':truncate_date})
            d_app = d_app.values('date').annotate(Count('playerid', distinct=True))
            # d_app = d_app.annotate(month=TruncMonth('date')
            #                 ).values('month').annotate(Count('playerid', 
            #                          distinct=True))
            final_dict[keys] = {i['date'].strftime("%Y-%m"):i['playerid__count'] for i in d_app}
    elif group_d == 'yearly':
        truncate_date = connection.ops.date_trunc_sql('year', 'date')
        for keys, d_app in add_list.iteritems():
            d_app = d_app.extra({'date':truncate_date})
            d_app = d_app.values('date').annotate(Count('playerid', 
                                     distinct=True))
            final_dict[keys] = {i['date'].strftime("%Y"):i['playerid__count'] for i in d_app}
    elif group_d == 'daily':
        truncate_date = connection.ops.date_trunc_sql('date', 'date')
        for keys, d_app in add_list.iteritems():
            d_app = d_app.extra({'date':truncate_date})
            d_app = d_app.values('date').annotate(Count('playerid', 
                                     distinct=True))
            final_dict[keys] = {i['date'].strftime("%Y-%m-%d"):i['playerid__count'] for i in d_app}
    elif group_d == 'campaign':
        for keys, d_app in add_list.iteritems():
            d_app = d_app.values('fk_campaign').annotate(Count('playerid', 
                                      distinct=True))
            final_dict[keys] = {(i['fk_campaign']):i['playerid__count'] for i in d_app}
    elif group_d == 'affiliate':
        for keys, d_app in add_list.iteritems():
            d_app = d_app.values('fk_campaign__fk_affiliateinfo').annotate(Count('playerid', 
                                     distinct=True))
            final_dict[keys] = {(i['fk_campaign__fk_affiliateinfo']):i['playerid__count'] for i in d_app}
    elif group_d == 'brand':
        for keys, d_app in add_list.iteritems():
            d_app = d_app.values('siteid').annotate(Count('playerid', 
                                      distinct=True))
            final_dict[keys] = {(i['siteid']):i['playerid__count'] for i in d_app}
    elif group_d == 'lifetime':
        for keys, d_app in add_list.iteritems():
            d_app = d_app.values('playerid').distinct().count()
            final_dict[keys] = {'lifetime': d_app}
    return final_dict

def group_by_results(from_date, to_date, site_ids, camp_links, group_d, currencyfilter, report_type, country=None):
    country_list = []
    data_list = []
    site_ids = [i.id for i in site_ids]
    i = [int(j.id) for j in camp_links]
    accounts_data = accounts_data_def(from_date, to_date, site_ids, 
                        camp_links, group_d, currencyfilter, report_type, country)

    tup_i = '(-1)'
    if i:
        tup_i = '('+str(functools.reduce(lambda a,b : str(a)+','+str(b),i))+')'
    if country and (type(country) == str or (isinstance(country, list) and len(country)==1)):
        country = country[0] if isinstance(country, list) else country
        country_id_list = '("'+country+'")'
    elif country and isinstance(country, list):
        country_id_list = '("'+'","'.join(country)+'")'
    else:
        country_list = [i.countryname for i in Country.objects.all()]
        # cache.set('country_list', country_list, cache_time)
    if not site_ids:
        return [[], '']
    euro_sites = '('+str(functools.reduce(lambda a,b : str(a)+','+str(b),site_ids))+')'
    if group_d == 'campaign':
        euro_sql_query = """
            select p.fk_campaign_id, p.date, p.fk_campaign_id, sum(p.clicks),
            sum(p.registrations), sum(p.ftd),
            sum(p.newplayers),
            sum(p.newplayersdeposit),sum(p.newplayersdeposit*c.eurtogbp),
            sum(p.activeplayers),
            sum(p.depositingaccounts),
            sum(p.wageringaccounts),
            sum(p.totalwagers),sum(p.totalwagers*c.eurtogbp),
            sum(p.avgactivedays),
            sum(p.netperplayer),sum(p.netperplayer*c.eurtogbp),
            sum(p.deposit),sum(p.deposit*c.eurtogbp),
            sum(p.cashout),sum(p.cashout*c.eurtogbp),
            sum(p.chargeback),sum(p.chargeback*c.eurtogbp),
            sum(p.void),sum(p.void*c.eurtogbp),
            sum(p.bonuses),sum(p.bonuses*c.eurtogbp),
            sum(p.revenue),sum(p.revenue*c.eurtogbp),
            sum(p.avgplayerltv),sum(p.avgplayerltv*c.eurtogbp),
            sum(p.cpacommission),sum(p.cpacommission*c.eurtogbp),
            sum(p.revsharecommission),sum(p.revsharecommission*c.eurtogbp),
            sum(p.totalcommission),sum(p.totalcommission*c.eurtogbp)  
            from connectors_campaigndatereport p join userapp_currencyconverted c on c.date=CAST(p.date AS DATE)
              where p.date >= '%s' and p.date <= '%s' and
             p.siteid_id IN %s and p.fk_campaign_id IN %s group by p.fk_campaign_id 
        """%(str(from_date).split(' ')[0],
            str(to_date).split(' ')[0], euro_sites, 
            tup_i)
        check_condition = 'fk_campaign_id'
    elif group_d == 'dynamic':
        euro_sql_query = """
            select p.wdynamic, p.fk_affiliateinfo_id, p.siteid_id, p.date, p.fk_campaign_id, sum(p.clicks),
            sum(p.registrations), sum(p.ftd),
            sum(p.newplayers),
            sum(p.newplayersdeposit),sum(p.newplayersdeposit*c.eurtogbp),
            sum(p.activeplayers),
            sum(p.depositingaccounts),
            sum(p.wageringaccounts),
            sum(p.totalwagers),sum(p.totalwagers*c.eurtogbp),
            sum(p.avgactivedays),
            sum(p.netperplayer),sum(p.netperplayer*c.eurtogbp),
            sum(p.deposit),sum(p.deposit*c.eurtogbp),
            sum(p.cashout),sum(p.cashout*c.eurtogbp),
            sum(p.chargeback),sum(p.chargeback*c.eurtogbp),
            sum(p.void),sum(p.void*c.eurtogbp),
            sum(p.bonuses),sum(p.bonuses*c.eurtogbp),
            sum(p.revenue),sum(p.revenue*c.eurtogbp),
            sum(p.avgplayerltv),sum(p.avgplayerltv*c.eurtogbp),
            sum(p.cpacommission),sum(p.cpacommission*c.eurtogbp),
            sum(p.revsharecommission),sum(p.revsharecommission*c.eurtogbp),
            sum(p.totalcommission),sum(p.totalcommission*c.eurtogbp)  
            from connectors_campaigndatereport p join userapp_currencyconverted c on c.date=CAST(p.date AS DATE)
              where p.date >= '%s' and p.date <= '%s' and 
             p.siteid_id IN %s and p.fk_campaign_id IN %s group by p.wdynamic,
              p.fk_affiliateinfo_id, p.siteid_id 
        """%(str(from_date).split(' ')[0],
            str(to_date).split(' ')[0], euro_sites, 
            tup_i)
        check_condition = 'wdynamic'
    elif group_d == 'month':
        euro_sql_query = """
            select DATE_FORMAT(p.date, "%s") as date,p.fk_affiliateinfo_id, p.siteid_id, p.fk_campaign_id, sum(p.clicks),
            sum(p.registrations), sum(p.ftd),
            sum(p.newplayers),
            sum(p.newplayersdeposit),sum(p.newplayersdeposit*c.eurtogbp),
            sum(p.activeplayers),
            sum(p.depositingaccounts),
            sum(p.wageringaccounts),
            sum(p.totalwagers),sum(p.totalwagers*c.eurtogbp),
            sum(p.avgactivedays),
            sum(p.netperplayer),sum(p.netperplayer*c.eurtogbp),
            sum(p.deposit),sum(p.deposit*c.eurtogbp),
            sum(p.cashout),sum(p.cashout*c.eurtogbp),
            sum(p.chargeback),sum(p.chargeback*c.eurtogbp),
            sum(p.void),sum(p.void*c.eurtogbp),
            sum(p.bonuses),sum(p.bonuses*c.eurtogbp),
            sum(p.revenue),sum(p.revenue*c.eurtogbp),
            sum(p.avgplayerltv),sum(p.avgplayerltv*c.eurtogbp),
            sum(p.cpacommission),sum(p.cpacommission*c.eurtogbp),
            sum(p.revsharecommission),sum(p.revsharecommission*c.eurtogbp),
            sum(p.totalcommission),sum(p.totalcommission*c.eurtogbp)  
            from connectors_campaigndatereport p join userapp_currencyconverted c on c.date=CAST(p.date AS DATE)
             where p.date >= '%s' and p.date <= '%s' and
             p.siteid_id IN %s and p.fk_campaign_id IN %s group by DATE_FORMAT(p.date, "%s"),
             p.siteid_id, p.fk_affiliateinfo_id 
        """%("%Y-%m", str(from_date).split(' ')[0],
            str(to_date).split(' ')[0], euro_sites, 
            tup_i, "%Y-%m")
        check_condition = 'date'
    elif group_d == 'daily':
        euro_sql_query = """
            select DATE_FORMAT(p.date, "%s") as date,p.fk_affiliateinfo_id, p.siteid_id, p.fk_campaign_id, sum(p.clicks),
            sum(p.registrations), sum(p.ftd),
            sum(p.newplayers),
            sum(p.newplayersdeposit),sum(p.newplayersdeposit*c.eurtogbp),
            sum(p.activeplayers),
            sum(p.depositingaccounts),
            sum(p.wageringaccounts),
            sum(p.totalwagers),sum(p.totalwagers*c.eurtogbp),
            sum(p.avgactivedays),
            sum(p.netperplayer),sum(p.netperplayer*c.eurtogbp),
            sum(p.deposit),sum(p.deposit*c.eurtogbp),
            sum(p.cashout),sum(p.cashout*c.eurtogbp),
            sum(p.chargeback),sum(p.chargeback*c.eurtogbp),
            sum(p.void),sum(p.void*c.eurtogbp),
            sum(p.bonuses),sum(p.bonuses*c.eurtogbp),
            sum(p.revenue),sum(p.revenue*c.eurtogbp),
            sum(p.avgplayerltv),sum(p.avgplayerltv*c.eurtogbp),
            sum(p.cpacommission),sum(p.cpacommission*c.eurtogbp),
            sum(p.revsharecommission),sum(p.revsharecommission*c.eurtogbp),
            sum(p.totalcommission),sum(p.totalcommission*c.eurtogbp) 
            from connectors_campaigndatereport p join userapp_currencyconverted c on c.date=CAST(p.date AS DATE)
             where p.date >= '%s' and p.date <= '%s' and
             p.siteid_id IN %s and p.fk_campaign_id IN %s group by DATE_FORMAT(p.date, "%s"), 
             p.siteid_id, p.fk_affiliateinfo_id
        """%("%Y-%m-%d", str(from_date).split(' ')[0],
            str(to_date).split(' ')[0], euro_sites, 
            tup_i, "%Y-%m-%d")
        check_condition = 'date'
    elif group_d == 'yearly':
        euro_sql_query = """
            select DATE_FORMAT(p.date, "%s") as date,p.fk_affiliateinfo_id, p.siteid_id, p.fk_campaign_id, sum(p.clicks),
            sum(p.registrations), sum(p.ftd),
            sum(p.newplayers),
            sum(p.newplayersdeposit),sum(p.newplayersdeposit*c.eurtogbp),
            sum(p.activeplayers),
            sum(p.depositingaccounts),
            sum(p.wageringaccounts),
            sum(p.totalwagers),sum(p.totalwagers*c.eurtogbp),
            sum(p.avgactivedays),
            sum(p.netperplayer),sum(p.netperplayer*c.eurtogbp),
            sum(p.deposit),sum(p.deposit*c.eurtogbp),
            sum(p.cashout),sum(p.cashout*c.eurtogbp),
            sum(p.chargeback),sum(p.chargeback*c.eurtogbp),
            sum(p.void),sum(p.void*c.eurtogbp),
            sum(p.bonuses),sum(p.bonuses*c.eurtogbp),
            sum(p.revenue),sum(p.revenue*c.eurtogbp),
            sum(p.avgplayerltv),sum(p.avgplayerltv*c.eurtogbp),
            sum(p.cpacommission),sum(p.cpacommission*c.eurtogbp),
            sum(p.revsharecommission),sum(p.revsharecommission*c.eurtogbp),
            sum(p.totalcommission),sum(p.totalcommission*c.eurtogbp) 
            from connectors_campaigndatereport p join userapp_currencyconverted c on c.date=CAST(p.date AS DATE)
             where p.date >= '%s' and p.date <= '%s' and
             p.siteid_id IN %s and p.fk_campaign_id IN %s group by DATE_FORMAT(p.date, "%s"),
             p.siteid_id, p.fk_affiliateinfo_id  
        """%("%Y", str(from_date).split(' ')[0],
            str(to_date).split(' ')[0], euro_sites, 
            tup_i, "%Y")
        check_condition = 'date'
    elif 'affiliate' in group_d:
        euro_sql_query = """
            select p.fk_affiliateinfo_id,p.fk_affiliateinfo_id, p.siteid_id, p.date, p.fk_campaign_id, sum(p.clicks),
            sum(p.registrations), sum(p.ftd),
            sum(p.newplayers),
            sum(p.newplayersdeposit),sum(p.newplayersdeposit*c.eurtogbp),
            sum(p.activeplayers),
            sum(p.depositingaccounts),
            sum(p.wageringaccounts),
            sum(p.totalwagers),sum(p.totalwagers*c.eurtogbp),
            sum(p.avgactivedays),
            sum(p.netperplayer),sum(p.netperplayer*c.eurtogbp),
            sum(p.deposit),sum(p.deposit*c.eurtogbp),
            sum(p.cashout),sum(p.cashout*c.eurtogbp),
            sum(p.chargeback),sum(p.chargeback*c.eurtogbp),
            sum(p.void),sum(p.void*c.eurtogbp),
            sum(p.bonuses),sum(p.bonuses*c.eurtogbp),
            sum(p.revenue),sum(p.revenue*c.eurtogbp),
            sum(p.avgplayerltv),sum(p.avgplayerltv*c.eurtogbp),
            sum(p.cpacommission),sum(p.cpacommission*c.eurtogbp),
            sum(p.revsharecommission),sum(p.revsharecommission*c.eurtogbp),
            sum(p.totalcommission),sum(p.totalcommission*c.eurtogbp) 
            from connectors_campaigndatereport p join userapp_currencyconverted c on c.date=CAST(p.date AS DATE)
             where p.date >= '%s' and p.date <= '%s' and
             p.siteid_id IN %s and p.fk_campaign_id IN %s group by p.siteid_id, p.fk_affiliateinfo_id
        """%(str(from_date).split(' ')[0],
            str(to_date).split(' ')[0], euro_sites, 
            tup_i)
        check_condition = 'fk_affiliateinfo_id'
    elif group_d == 'brand':
        euro_sql_query = """
            select p.fk_affiliateinfo_id, p.siteid_id, p.date,  p.fk_campaign_id, sum(p.clicks),
            sum(p.registrations), sum(p.ftd),
            sum(p.newplayers),
            sum(p.newplayersdeposit),sum(p.newplayersdeposit*c.eurtogbp),
            sum(p.activeplayers),
            sum(p.depositingaccounts),
            sum(p.wageringaccounts),
            sum(p.totalwagers),sum(p.totalwagers*c.eurtogbp),
            sum(p.avgactivedays),
            sum(p.netperplayer),sum(p.netperplayer*c.eurtogbp),
            sum(p.deposit),sum(p.deposit*c.eurtogbp),
            sum(p.cashout),sum(p.cashout*c.eurtogbp),
            sum(p.chargeback),sum(p.chargeback*c.eurtogbp),
            sum(p.void),sum(p.void*c.eurtogbp),
            sum(p.bonuses),sum(p.bonuses*c.eurtogbp),
            sum(p.revenue),sum(p.revenue*c.eurtogbp),
            sum(p.avgplayerltv),sum(p.avgplayerltv*c.eurtogbp),
            sum(p.cpacommission),sum(p.cpacommission*c.eurtogbp),
            sum(p.revsharecommission),sum(p.revsharecommission*c.eurtogbp),
            sum(p.totalcommission),sum(p.totalcommission*c.eurtogbp) 
            from connectors_campaigndatereport p join userapp_currencyconverted c on c.date=CAST(p.date AS DATE)
             where p.date >= '%s' and p.date <= '%s' and
             p.siteid_id IN %s and p.fk_campaign_id IN %s group by p.siteid_id, p.fk_affiliateinfo_id 
        """%(str(from_date).split(' ')[0],
            str(to_date).split(' ')[0], euro_sites, 
            tup_i)
        check_condition = 'siteid_id'
    elif group_d == 'lifetime':
        euro_sql_query = """
            select p.fk_affiliateinfo_id, p.siteid_id, sum(p.clicks),
            sum(p.registrations), sum(p.ftd),
            sum(p.newplayers),
            sum(p.newplayersdeposit),sum(p.newplayersdeposit*c.eurtogbp),
            sum(p.activeplayers),
            sum(p.depositingaccounts),
            sum(p.wageringaccounts),
            sum(p.totalwagers),sum(p.totalwagers*c.eurtogbp),
            sum(p.avgactivedays),
            sum(p.netperplayer),sum(p.netperplayer*c.eurtogbp),
            sum(p.deposit),sum(p.deposit*c.eurtogbp),
            sum(p.cashout),sum(p.cashout*c.eurtogbp),
            sum(p.chargeback),sum(p.chargeback*c.eurtogbp),
            sum(p.void),sum(p.void*c.eurtogbp),
            sum(p.bonuses),sum(p.bonuses*c.eurtogbp),
            sum(p.revenue),sum(p.revenue*c.eurtogbp),
            sum(p.avgplayerltv),sum(p.avgplayerltv*c.eurtogbp),
            sum(p.cpacommission),sum(p.cpacommission*c.eurtogbp),
            sum(p.revsharecommission),sum(p.revsharecommission*c.eurtogbp),
            sum(p.totalcommission),sum(p.totalcommission*c.eurtogbp) 
            from connectors_campaigndatereport p join userapp_currencyconverted c on c.date=CAST(p.date AS DATE)
             where p.date >= '%s' and p.date <= '%s' and
             p.siteid_id IN %s and p.fk_campaign_id IN %s group by p.siteid_id, p.fk_affiliateinfo_id
        """%(str(from_date).split(' ')[0],
            str(to_date).split(' ')[0], euro_sites, 
            tup_i)
        check_condition = 'lifetime'
    elif group_d == 'country':
        euro_sql_query = """
            select p.country,p.siteid_id, p.fk_affiliateinfo_id, sum(p.clicks),
            sum(p.registrations), sum(p.ftd),
            sum(p.newplayers),
            sum(p.newplayersdeposit),sum(p.newplayersdeposit*c.eurtogbp),
            sum(p.activeplayers),
            sum(p.depositingaccounts),
            sum(p.wageringaccounts),
            sum(p.totalwagers),sum(p.totalwagers*c.eurtogbp),
            sum(p.avgactivedays),
            sum(p.netperplayer),sum(p.netperplayer*c.eurtogbp),
            sum(p.deposit),sum(p.deposit*c.eurtogbp),
            sum(p.cashout),sum(p.cashout*c.eurtogbp),
            sum(p.chargeback),sum(p.chargeback*c.eurtogbp),
            sum(p.void),sum(p.void*c.eurtogbp),
            sum(p.bonuses),sum(p.bonuses*c.eurtogbp),
            sum(p.revenue),sum(p.revenue*c.eurtogbp),
            sum(p.avgplayerltv),sum(p.avgplayerltv*c.eurtogbp),
            sum(p.cpacommission),sum(p.cpacommission*c.eurtogbp),
            sum(p.revsharecommission),sum(p.revsharecommission*c.eurtogbp),
            sum(p.totalcommission),sum(p.totalcommission*c.eurtogbp) 
            from connectors_campaigndatereport p join userapp_currencyconverted c on c.date=CAST(p.date AS DATE)
             where p.date >= '%s' and p.date <= '%s' and
             p.siteid_id IN %s and p.fk_campaign_id IN %s  group by p.country,
             p.siteid_id, p.fk_affiliateinfo_id 
        """%(str(from_date).split(' ')[0],
            str(to_date).split(' ')[0], euro_sites, 
            tup_i)
        check_condition = 'country'
    if country:
        euro_sql_query = euro_sql_query.split('group by')[0]+'and p.country IN %s group by'%(country_id_list) + euro_sql_query.split('group by')[1]
    euro_sql_query += ';'
    euro_camp_list = []
    cursor = connection.cursor()
    euro_set = cursor.execute(euro_sql_query)
    col_names = [desc[0] for desc in cursor.description]
    while True:
        row = cursor.fetchone()
        if row is None:
            break
        row_dict = dict(zip(col_names, row))
        euro_camp_list.append(row_dict)
    if currencyfilter == 'gbp':
        currency = u'\xa3'
    else:
        currency = u'\u20ac'
    group_header = ''
    aff_list = camp_links.values_list('fk_affiliateinfo', flat=True).distinct()
    account_m_dict = {i.id:i.account_manager.id for i in Affiliateinfo.objects.filter(id__in=aff_list).select_related('account_manager')}
    commission_dict = {}
    if group_d != 'campaign':
        commission_dict = get_commission_affiliate_dict(aff_list, site_ids, account_m_dict, d_date=False)
    calculated_dict = {}
    for i in euro_camp_list:
        if currencyfilter == 'gbp':
            dict_i = {(j.replace('sum(p.','').replace(')','__togbp') if '*' not in j else j.replace('sum(p.','').replace('*c.eurtogbp)','__sum')):(i[j] if i[j] != None else 0) for j in i}
            dict_i['clicks__sum'] = i['sum(p.clicks)']
            dict_i['registrations__sum'] = i['sum(p.registrations)']
            dict_i['ftd__sum'] = i['sum(p.ftd)']
            dict_i['newplayers__sum'] = i['sum(p.newplayers)']
            dict_i['activeplayers__sum'] = i['sum(p.activeplayers)']
            dict_i['depositingaccounts__sum'] = i['sum(p.depositingaccounts)']
            dict_i['wageringaccounts__sum'] = i['sum(p.wageringaccounts)']
            dict_i['avgactivedays__sum'] = i['sum(p.avgactivedays)']
        else:
            dict_i = {(j.replace('sum(p.','').replace(')','__sum') if '*' not in j else j.replace('sum(p.','').replace('*c.eurtogbp)','__togbp')):(i[j] if i[j] != None else 0) for j in i}
        if group_d != 'campaign' and commission_dict:
            #sub affiliate commission
            referral_comm = None
            ringfence = None
            _comm = commission_dict.get((dict_i['fk_affiliateinfo_id'], dict_i['siteid_id']))
            if _comm:
                referral_comm = _comm['refferal']
            sub_aff_earnings = get_sub_aff_referral_earnings(dict_i['fk_affiliateinfo_id'],
                             from_date, to_date, dict_i['siteid_id'], currencyfilter)
            dict_i['referalcommission__sum'] = 0.0
            if sub_aff_earnings and referral_comm:
                dict_i['referalcommission__sum'] = (referral_comm*sub_aff_earnings)/100
                dict_i['totalcommission__sum'] += dict_i['referalcommission__sum']
            if _comm:
                ringfence = _comm['ringfence']
            if ringfence and dict_i['revsharecommission__sum'] < 0:
                dict_i['totalcommission__sum'] = dict_i['totalcommission__sum'] - dict_i['revsharecommission__sum']
                dict_i['revsharecommission__sum'] = 0
        if group_d == 'lifetime':
            grp_h = 'lifetime'
        else:
            grp_h = dict_i[check_condition]
        calculated_dict[grp_h] = { k: (dict_i.get(k, 0) + calculated_dict.get(grp_h, {}).get(k, 0) if 'sum' in k else dict_i.get(k)) for k in set(dict_i)}
    for ke_d in calculated_dict:
        # import pdb;pdb.set_trace()
        dict_i = calculated_dict[ke_d]
        if group_d == 'brand':
            group_header = Site.objects.get(id=dict_i[check_condition]).name
        elif group_d in ['yearly', 'month', 'daily']:
            group_header = dict_i[check_condition]
        elif group_d == 'campaign':
            group_header = CampaignTrackerMapping.objects.get(id=dict_i[check_condition]).fk_campaign.name
        elif group_d == 'affiliate':
            group_header = Affiliateinfo.objects.get(id=dict_i[check_condition]).email
        elif group_d == 'affiliate_report':
            group_header = dict_i[check_condition]
        elif group_d == 'country':
            group_header = dict_i[check_condition]
        elif group_d == 'lifetime':
            group_header = check_condition
            dict_i[check_condition] = 'lifetime'
        elif group_d == 'dynamic':
            group_header = dict_i[check_condition]
            # if not group_header:
            #     continue
        registrations = int(dict_i['registrations__sum']) if dict_i.get('registrations__sum') else 0 
        clicks = int(dict_i['clicks__sum']) if dict_i.get('clicks__sum') else 0 
        ftds = int(dict_i['ftd__sum']) if dict_i.get('ftd__sum') else 0 
        new_player_count = int(dict_i['newplayers__sum']) if dict_i.get('newplayers__sum') else 0 
        new_player_deposit = dict_i.get('newplayersdeposit__sum', 0.00)
        active_player_count = accounts_data['active_players'].get(dict_i[check_condition],0) #if dict_i.get('activeplayers__sum') else 0 
        diposit_accounts = accounts_data['deposit_players'].get(dict_i[check_condition],0) # int(dict_i['depositingaccounts__sum']) if dict_i.get('depositingaccounts__sum') else 0 
        wagering_accounts = accounts_data['side_players'].get(dict_i[check_condition],0) #int(dict_i['wageringaccounts__sum']) if dict_i.get('wageringaccounts__sum') else 0 
        total_wagers = dict_i.get('totalwagers__sum', 0.00)
        deposit = dict_i.get('deposit__sum', 0.00)
        cashout = dict_i.get('cashout__sum', 0.00)
        chargeback = dict_i.get('chargeback__sum', 0.00)
        void = dict_i.get('void__sum', 0.00)
        bonus = dict_i.get('bonuses__sum', 0.00)
        revenue = dict_i.get('revenue__sum', 0.00)
        avg_ltv = dict_i.get('avgplayerltv__sum', 0.00)
        cpa_value = round(dict_i['cpacommission__sum'],2) if dict_i.get('cpacommission__sum') else 0.00
        rev_value = round(dict_i['revsharecommission__sum'],2) if dict_i.get('revsharecommission__sum') else 0.00
        ref_value = round(dict_i['referalcommission__sum'],2) if dict_i.get('referalcommission__sum') else 0.00
        total_value = round(dict_i['totalcommission__sum'],2) if dict_i.get('totalcommission__sum') else 0.00
        avg_active_players = accounts_data['avg_active_players'].get(dict_i[check_condition],0) # int(dict_i['avgactivedays__sum']) if dict_i.get('avgactivedays__sum') else 0
        avg_net_cash = dict_i.get('netperplayer__sum', 0.00)
        reg_rate = 0
        ftd_rate = 0
        if clicks:
            reg_rate = float(registrations)/float(clicks)
        if registrations:
            ftd_rate = float(ftds)/float(registrations)

        if report_type == 'affiliate':
            aff_data = [registrations,
                    ftds, 
                    round(deposit, 2) if deposit else currency+'0.00',
                    # round(cashout, 2) if cashout else currency+'0.00',
                    # round(bonus, 2) if bonus else currency+'0.00',
                    round(revenue, 2) if revenue else currency+'0.00',
                    cpa_value if cpa_value else currency+'0.00', 
                    rev_value if rev_value else currency+'0.00',
                    # ref_value if ref_value else currency+'0.00',
                    total_value if total_value else currency+'0.00']
        elif 'affiliate_activity' in report_type:
            aff_data = [0,clicks,registrations,
                    ftds, 
                    round(deposit, 2) if deposit else currency+'0.00',
                    # round(cashout, 2) if cashout else currency+'0.00',
                    round(bonus, 2) if bonus else currency+'0.00',
                    round(revenue, 2) if revenue else currency+'0.00',
                    cpa_value if cpa_value else currency+'0.00', 
                    rev_value if rev_value else currency+'0.00',
                    ref_value if ref_value else currency+'0.00',
                    total_value if total_value else currency+'0.00']
        elif report_type == 'affiliate_report':
            aff_data = [0, clicks, 
                    registrations,
                    ftds, 
                    round(deposit, 2) if deposit else currency+'0.00',
                    # round(cashout, 2) if cashout else currency+'0.00',
                    round(bonus, 2) if bonus else currency+'0.00',
                    round(revenue, 2) if revenue else currency+'0.00',
                    cpa_value if cpa_value else 0.00, 
                    rev_value if rev_value else 0.00,
                    ref_value if ref_value else 0.00,
                    total_value if total_value else 0.0]
        elif report_type == 'affiliate_admin':
            aff_data = [clicks, 
                    registrations,
                    ftds, 
                    round(deposit, 2) if deposit else currency+'0.00',
                    # round(cashout, 2) if cashout else currency+'0.00',
                    round(revenue, 2) if revenue else currency+'0.00']
        elif report_type == 'brand_admin':
           aff_data = [0, clicks, 
                    registrations,
                    ftds, 
                    round(deposit, 2) if deposit else currency+'0.00',
                    # round(cashout, 2) if cashout else currency+'0.00',
                    round(bonus, 2) if bonus else currency+'0.00',
                    round(revenue, 2) if revenue else currency+'0.00']
        elif report_type == 'brand_pmc_admin':
            aff_data = [
                    registrations,
                    ftds, 
                    round(deposit, 2) if deposit else currency+'0.00',
                    round(cashout, 2) if cashout else currency+'0.00',
                    round(revenue, 2) if revenue else currency+'0.00']
        elif report_type == 'country_admin':
           aff_data = [clicks, 
                    registrations,
                    ftds, 
                    diposit_accounts,
                    wagering_accounts]
        elif report_type == 'country_report':
            aff_data = [clicks, 
                    registrations, ftds,
                    diposit_accounts, wagering_accounts,
                    round(deposit, 2) if deposit else currency+'0.00',
                    # round(cashout, 2) if cashout else currency+'0.00',
                    round(bonus, 2) if bonus else currency+'0.00',
                    round(revenue, 2) if revenue else currency+'0.00']
        elif report_type == 'brand_affiliate':
           aff_data = [ 
                    round(deposit, 2) if deposit else currency+'0.00',
                    round(revenue, 2) if revenue else currency+'0.00',
                    cpa_value if cpa_value else currency+'0.00', 
                    rev_value if rev_value else currency+'0.00', 
                    total_value if total_value else currency+'0.00']
        elif report_type == 'campaign':
            aff_data = [0, clicks, 
                    registrations,
                    ftds, 
                    diposit_accounts,
                    round(deposit, 2) if deposit else currency+'0.00',
                    round(revenue, 2) if revenue else currency+'0.00',
                    cpa_value if cpa_value else currency+'0.00', 
                    rev_value if rev_value else currency+'0.00',
                    ref_value if ref_value else currency+'0.00',
                    total_value if total_value else currency+'0.00']
        elif report_type == 'lifetime':
            aff_data = [0, clicks, 0,
                    registrations, str(round(reg_rate, 2)),
                    ftds, str(round(ftd_rate, 2)), new_player_count,
                    round(new_player_deposit, 2) if new_player_deposit else '0.00', active_player_count,
                     diposit_accounts, wagering_accounts, round(total_wagers,2) if total_wagers else '0.00',
                    avg_active_players, avg_net_cash,
                    round(deposit, 2) if deposit else '0.00',
                    round(cashout, 2) if cashout else '0.00',
                    round(chargeback, 2) if chargeback else '0.00', 
                    round(void, 2) if void else '0.00',
                    round(bonus, 2) if bonus else '0.00',
                    round(revenue, 2) if revenue else '0.00',
                    round(avg_ltv, 2) if avg_ltv else '0.00',
                    cpa_value, rev_value, total_value]
        elif report_type == 'conversion':
            aff_data = [0, clicks, 0, 
                    registrations, str(round(reg_rate, 2)),
                    ftds, str(round(ftd_rate, 2))]
        elif report_type == 'earnings':
            aff_data = [0, clicks, 0,
                    registrations, str(round(reg_rate, 2)),
                    ftds, str(round(ftd_rate, 2)), new_player_count, active_player_count,
                     diposit_accounts, wagering_accounts, round(total_wagers,2) if total_wagers else currency+'0.00',
                    avg_active_players, avg_net_cash,
                    round(deposit, 2) if deposit else currency+'0.00',
                    # round(cashout, 2) if cashout else currency+'0.00',
                    round(chargeback, 2) if chargeback else currency+'0.00', 
                    round(void, 2) if void else currency+'0.00',
                    round(bonus, 2) if bonus else currency+'0.00',
                    round(revenue, 2) if revenue else currency+'0.00',
                    round(avg_ltv, 2) if avg_ltv else currency+'0.00',
                    cpa_value if cpa_value else currency+'0.00', 
                    rev_value if rev_value else currency+'0.00',
                    ref_value if ref_value else currency+'0.00', 
                    total_value if total_value else currency+'0.00']
        if report_type != 'lifetime':
            if report_type != 'affiliate_report':
                aff_data = [k if (isinstance(k,unicode) or '.' not in str(k) or isinstance(k,str)) else ('-'+currency+str(k).replace('-', '') if '-' in str(k) else currency + str(k)) for k in aff_data]
            aff_data.insert(0,group_header)
        data_list.append(aff_data)
        print dict_i
    return [data_list, currency]

def get_sub_aff_referral_earnings(aff_id, from_date_time, to_date_time, site_id, currency):
    data_list = []
    sub_aff_list = Affiliateinfo.objects.filter(superaffiliate_id=aff_id)
    if sub_aff_list:
        site_obj = Site.objects.get(id=site_id)
        camp_track_links = CampaignTrackerMapping.objects.filter(fk_affiliateinfo__in=sub_aff_list,
                fk_campaign__siteid=site_obj).select_related('fk_campaign', 'fk_campaign__siteid')
        data_list, currency = group_by_results(from_date_time, to_date_time, [site_obj], 
                                camp_track_links, 'lifetime', currency, 'lifetime')
    return data_list[0][24] if data_list else 0

def campaign_dynamic_reports(from_date, to_date, site_ids, camp_links, group_d, currencyfilter, report_type, country=None):
    country_list = []
    data_list = []
    site_ids = [i.id for i in site_ids]
    i = [int(j.id) for j in camp_links]
    accounts_data = accounts_data_def(from_date, to_date, site_ids, 
                        camp_links, group_d, currencyfilter, report_type, country)

    tup_i = '(-1)'
    if i:
        tup_i = '('+str(functools.reduce(lambda a,b : str(a)+','+str(b),i))+')'
    if country and (type(country) == str or (isinstance(country, list) and len(country)==1)):
        country = country[0] if isinstance(country, list) else country
        country_id_list = '("'+country+'")'
    elif country and isinstance(country, list):
        country_id_list = '("'+'","'.join(country)+'")'
    else:
        country_list = [i.countryname for i in Country.objects.all()]
        # cache.set('country_list', country_list, cache_time)
    if not site_ids:
        return [[], '']
    euro_sites = '('+str(functools.reduce(lambda a,b : str(a)+','+str(b),site_ids))+')'
    if group_d == 'campaign':
        euro_sql_query = """
            select p.fk_campaign_id, p.fk_affiliateinfo_id, p.date, p.fk_campaign_id, sum(p.clicks),
            sum(p.registrations), sum(p.ftd),
            sum(p.newplayers),
            sum(p.newplayersdeposit),sum(p.newplayersdeposit*c.eurtogbp),
            sum(p.activeplayers),
            sum(p.depositingaccounts),
            sum(p.wageringaccounts),
            sum(p.totalwagers),sum(p.totalwagers*c.eurtogbp),
            sum(p.avgactivedays),
            sum(p.netperplayer),sum(p.netperplayer*c.eurtogbp),
            sum(p.deposit),sum(p.deposit*c.eurtogbp),
            sum(p.cashout),sum(p.cashout*c.eurtogbp),
            sum(p.chargeback),sum(p.chargeback*c.eurtogbp),
            sum(p.void),sum(p.void*c.eurtogbp),
            sum(p.bonuses),sum(p.bonuses*c.eurtogbp),
            sum(p.revenue),sum(p.revenue*c.eurtogbp),
            sum(p.avgplayerltv),sum(p.avgplayerltv*c.eurtogbp),
            sum(p.cpacommission),sum(p.cpacommission*c.eurtogbp),
            sum(p.revsharecommission),sum(p.revsharecommission*c.eurtogbp),
            sum(p.totalcommission),sum(p.totalcommission*c.eurtogbp)  
            from connectors_campaigndatereport p join userapp_currencyconverted c on c.date=CAST(p.date AS DATE)
              where p.date >= '%s' and p.date <= '%s' and
             p.siteid_id IN %s and p.fk_campaign_id IN %s group by p.fk_campaign_id,
             p.fk_affiliateinfo_id  
        """%(str(from_date).split(' ')[0],
            str(to_date).split(' ')[0], euro_sites, 
            tup_i)
        check_condition = 'fk_campaign_id'
    elif group_d == 'dynamic':
        euro_sql_query = """
            select p.wdynamic, p.fk_affiliateinfo_id, p.date, p.fk_campaign_id, sum(p.clicks),
            sum(p.registrations), sum(p.ftd),
            sum(p.newplayers),
            sum(p.newplayersdeposit),sum(p.newplayersdeposit*c.eurtogbp),
            sum(p.activeplayers),
            sum(p.depositingaccounts),
            sum(p.wageringaccounts),
            sum(p.totalwagers),sum(p.totalwagers*c.eurtogbp),
            sum(p.avgactivedays),
            sum(p.netperplayer),sum(p.netperplayer*c.eurtogbp),
            sum(p.deposit),sum(p.deposit*c.eurtogbp),
            sum(p.cashout),sum(p.cashout*c.eurtogbp),
            sum(p.chargeback),sum(p.chargeback*c.eurtogbp),
            sum(p.void),sum(p.void*c.eurtogbp),
            sum(p.bonuses),sum(p.bonuses*c.eurtogbp),
            sum(p.revenue),sum(p.revenue*c.eurtogbp),
            sum(p.avgplayerltv),sum(p.avgplayerltv*c.eurtogbp),
            sum(p.cpacommission),sum(p.cpacommission*c.eurtogbp),
            sum(p.revsharecommission),sum(p.revsharecommission*c.eurtogbp),
            sum(p.totalcommission),sum(p.totalcommission*c.eurtogbp)  
            from connectors_campaigndatereport p join userapp_currencyconverted c on c.date=CAST(p.date AS DATE)
              where p.date >= '%s' and p.date <= '%s' and 
             p.siteid_id IN %s and p.fk_campaign_id IN %s group by p.wdynamic,
              p.fk_affiliateinfo_id 
        """%(str(from_date).split(' ')[0],
            str(to_date).split(' ')[0], euro_sites, 
            tup_i)
        check_condition = 'wdynamic'
    if country:
        euro_sql_query = euro_sql_query.split('group by')[0]+'and p.country IN %s group by'%(country_id_list) + euro_sql_query.split('group by')[1]
    euro_sql_query += ';'
    euro_camp_list = []
    cursor = connection.cursor()
    euro_set = cursor.execute(euro_sql_query)
    col_names = [desc[0] for desc in cursor.description]
    while True:
        row = cursor.fetchone()
        if row is None:
            break
        row_dict = dict(zip(col_names, row))
        euro_camp_list.append(row_dict)
    if currencyfilter == 'gbp':
        currency = u'\xa3'
    else:
        currency = u'\u20ac'
    group_header = ''
    aff_list = camp_links.values_list('fk_affiliateinfo', flat=True).distinct()
    aff_id_dict = {i.id:i.email for i in Affiliateinfo.objects.filter(id__in=aff_list).select_related('account_manager')}
    camp_id_dict = {i.id:i.fk_campaign.name for i in camp_links.select_related('fk_campaign')}
    for i in euro_camp_list:
        if currencyfilter == 'gbp':
            dict_i = {(j.replace('sum(p.','').replace(')','__togbp') if '*' not in j else j.replace('sum(p.','').replace('*c.eurtogbp)','__sum')):(i[j] if i[j] != None else 0) for j in i}
            dict_i['clicks__sum'] = i['sum(p.clicks)']
            dict_i['registrations__sum'] = i['sum(p.registrations)']
            dict_i['ftd__sum'] = i['sum(p.ftd)']
            dict_i['newplayers__sum'] = i['sum(p.newplayers)']
            dict_i['activeplayers__sum'] = i['sum(p.activeplayers)']
            dict_i['depositingaccounts__sum'] = i['sum(p.depositingaccounts)']
            dict_i['wageringaccounts__sum'] = i['sum(p.wageringaccounts)']
            dict_i['avgactivedays__sum'] = i['sum(p.avgactivedays)']
        else:
            dict_i = {(j.replace('sum(p.','').replace(')','__sum') if '*' not in j else j.replace('sum(p.','').replace('*c.eurtogbp)','__togbp')):(i[j] if i[j] != None else 0) for j in i}
        if group_d == 'campaign':
            group_header = camp_id_dict[dict_i[check_condition]]
        elif group_d == 'dynamic':
            group_header = dict_i[check_condition]
        aff_header = aff_id_dict[dict_i['fk_affiliateinfo_id']]

        registrations = int(dict_i['registrations__sum']) if dict_i.get('registrations__sum') else 0 
        clicks = int(dict_i['clicks__sum']) if dict_i.get('clicks__sum') else 0 
        ftds = int(dict_i['ftd__sum']) if dict_i.get('ftd__sum') else 0 
        new_player_count = int(dict_i['newplayers__sum']) if dict_i.get('newplayers__sum') else 0 
        new_player_deposit = dict_i.get('newplayersdeposit__sum', 0.00)
        active_player_count = accounts_data['active_players'].get(dict_i[check_condition],0) #if dict_i.get('activeplayers__sum') else 0 
        diposit_accounts = accounts_data['deposit_players'].get(dict_i[check_condition],0) # int(dict_i['depositingaccounts__sum']) if dict_i.get('depositingaccounts__sum') else 0 
        wagering_accounts = accounts_data['side_players'].get(dict_i[check_condition],0) #int(dict_i['wageringaccounts__sum']) if dict_i.get('wageringaccounts__sum') else 0 
        total_wagers = dict_i.get('totalwagers__sum', 0.00)
        deposit = dict_i.get('deposit__sum', 0.00)
        cashout = dict_i.get('cashout__sum', 0.00)
        chargeback = dict_i.get('chargeback__sum', 0.00)
        void = dict_i.get('void__sum', 0.00)
        bonus = dict_i.get('bonuses__sum', 0.00)
        revenue = dict_i.get('revenue__sum', 0.00)
        avg_ltv = dict_i.get('avgplayerltv__sum', 0.00)
        cpa_value = round(dict_i['cpacommission__sum'],2) if dict_i.get('cpacommission__sum') else 0.00
        rev_value = round(dict_i['revsharecommission__sum'],2) if dict_i.get('revsharecommission__sum') else 0.00
        total_value = round(dict_i['totalcommission__sum'],2) if dict_i.get('totalcommission__sum') else 0.00
        avg_active_players = accounts_data['avg_active_players'].get(dict_i[check_condition],0) # int(dict_i['avgactivedays__sum']) if dict_i.get('avgactivedays__sum') else 0
        avg_net_cash = dict_i.get('netperplayer__sum', 0.00)
        reg_rate = 0
        ftd_rate = 0
        if clicks:
            reg_rate = float(registrations)/float(clicks)
        if registrations:
            ftd_rate = float(ftds)/float(registrations)

        
        if report_type == 'campaign':
            aff_data = [aff_header, 0, clicks, 
                    registrations,
                    ftds, 
                    diposit_accounts,
                    round(deposit, 2) if deposit else currency+'0.00',
                    round(revenue, 2) if revenue else currency+'0.00',
                    cpa_value if cpa_value else currency+'0.00', 
                    rev_value if rev_value else currency+'0.00', 
                    total_value if total_value else currency+'0.00']
        elif report_type == 'dynamic':
            aff_data = [aff_header, 0, clicks, 0,
                    registrations, str(round(reg_rate)),
                    ftds, str(round(ftd_rate)), new_player_count, active_player_count,
                     diposit_accounts, wagering_accounts, round(total_wagers,2) if total_wagers else currency+'0.00',
                    avg_active_players, avg_net_cash,
                    round(deposit, 2) if deposit else currency+'0.00',
                    # round(cashout, 2) if cashout else currency+'0.00',
                    round(chargeback, 2) if chargeback else currency+'0.00', 
                    round(void, 2) if void else currency+'0.00',
                    round(bonus, 2) if bonus else currency+'0.00',
                    round(revenue, 2) if revenue else currency+'0.00',
                    round(avg_ltv, 2) if avg_ltv else currency+'0.00',
                    cpa_value if cpa_value else currency+'0.00', 
                    rev_value if rev_value else currency+'0.00', 
                    total_value if total_value else currency+'0.00']
        
        if report_type != 'lifetime':
            aff_data = [k if (isinstance(k,unicode) or '.' not in str(k) or isinstance(k,str)) else ('-'+currency+str(k).replace('-', '') if '-' in str(k) else currency + str(k)) for k in aff_data]
            aff_data.insert(0,group_header)
        data_list.append(aff_data)
        print dict_i
    return [data_list, currency]

def Process_campaign_admin_stats(obj, from_date, to_date, camp_links,
                    site_ids, aff_list, country, group_d, currencyfilter):
    data_list = []
    currency = ''
    currency_dict = {}
    total_list = ['Total', '']
    header_list = [
           # { "title": "Brand", "width":"200px"},
           { "title": "Grouped by", "width":"200px"},
           # { "title": "Country"},
           # { "title": "Campaign name", "width":"200px"},
           # { "title": "Affiliate name", "width":"200px"},
           { "title": "Impressions", "width":"140px"},
           { "title": "Clicks", "width":"120px"},
           { "title": "CTR", "width":"120px"},
           { "title": "Registrations", "width":"200px"},
           { "title": "Registration rate %", "width":"180px"},
           { "title": "FTD", "width":"120px"},
           { "title": "Conversion rate %", "width":"200px"},
           { "title": "New players", "width":"160px"},
           { "title": "Active players", "width":"200px"},
           { "title": "Depositing accounts", "width":"240px"},
           { "title": "Wagering accounts", "width":"230px"},
           { "title": "Total Wagers", "width":"180px"},
           { "title": "Avg active days", "width":"200px"},
           { "title": "Net per player", "width":"180px"},
           { "title": "Deposits", "width":"130px"},
           # { "title": "Cashout", "width":"130px"},
           { "title": "Chargeback", "width":"150px"},
           { "title": "Void", "width":"120px"},
           { "title": "Bonus", "width":"120px"},
           { "title": "Revenue", "width":"150px"},
           { "title": "Avg. Player LTV", "width":"200px"},
           { "title": "CPA Commission", "width":"200px"},
           { "title": "Revshare Commission", "width":"220px"},
           { "title": "Referral Commission", "width":"220px"},
           { "title": "Total Commission", "width":"180px"},
           ]
    group_list = []
    data_list, currency = group_by_results(from_date, to_date, site_ids, camp_links,
                     group_d, currencyfilter, 'earnings', country)
    return (header_list, data_list, currency)

def db_process_hit(aff_data, obj, camp_links, content_type, from_date, to_date,
            site_ids, currencyfilter, country=None, group_d=None):
    country_list = []
    country_id_list = ''
    print 'brand group by start'
    if country and type(country) == str:
        country_list = [country]
        country_id_list = '('+','.join(country)+')'
    else:
        # country_list = cache.get('country_list')
        if not country_list:
            country_list = [i.countryname for i in Country.objects.all()]
            # cache.set('country_list', country_list, cache_time)
    data_list = []
    # currency_dict = get_currency_dict(site_ids)
    currency_sites = {}
    currency_objs = Currency.objects.filter(siteid__in=site_ids)
    for cur in currency_objs:
        if cur.currency in currency_sites:
            currency_sites[cur.currency].append(int(cur.siteid.id))
        else:
            currency_sites[cur.currency] = [int(cur.siteid.id)]
    if not currency_sites.get(u'\xa3'):
        currency_sites[u'\xa3'] = []
    if not currency_sites.get(u'\u20ac'):
        currency_sites[u'\u20ac'] = []

    if type(obj) != Affiliateinfo:
        commission_dict = get_commission_dict(obj[0], camp_links, obj, site_ids)
    else:
        commission_dict = get_commission_dict(obj, camp_links, [obj], site_ids)
    i = [int(j.id) for j in camp_links]
    tup_i = '(-1)'
    if i:
        tup_i = '('+str(functools.reduce(lambda a,b : str(a)+','+str(b),i))+')'
    upload_tracker_obj = {}
    upload_tracker_obj = UploadAdvanced.objects.filter(fk_campaign__in=camp_links,
                    date__gte=str(from_date).split(' ')[0],
                    date__lte=str(to_date).split(' ')[0],
                    siteid__in=site_ids,
                    processed=True).values(
                        'fk_campaign').annotate(Sum('ftd'),
                    Sum('registrations'), Sum('clicks'))
    upload_tracker_obj = {i['fk_campaign']:i for i in upload_tracker_obj}
    upload_player_tracker_obj = {}
    upload_new_player_obj = {}
    upload_active_player_obj = {}
    upload_date_player_obj = {}
    upload_ftd_player_obj = {}
    upload_avg_active_players_obj = {}
    # tracker_obj = get_cliks_data([], from_date, to_date, site_ids, [i])
    api_clicks = CampaignClicksCount.objects.filter(fk_campaign__in=camp_links,
                            date__gte=str(from_date).split(' ')[0],
                            date__lte=str(to_date).split(' ')[0],
                            fk_campaign__fk_campaign__siteid__in=site_ids).values(
                            'fk_campaign').annotate(Sum('clicks'))
    api_clicks = {i['fk_campaign']:i for i in api_clicks}
    api_registrations = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links, 
                        registrationdate__gte=str(from_date),
                        registrationdate__lte=str(to_date),
                        country__in=country_list,
                        siteid__in=site_ids).values('fk_campaign').annotate(Count('playerid', 
                                 distinct=True))
    api_registrations = {i['fk_campaign']:i for i in api_registrations}
    api_ftds = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links, 
                        ftddate__gte=str(from_date),
                        ftddate__lte=str(to_date),
                        country__in=country_list,
                        siteid__in=site_ids,deposit__gt=0
                        ).values('fk_campaign').annotate(Count('playerid', 
                                 distinct=True))
    api_ftds = {i['fk_campaign']:i for i in api_ftds}
    api_player_tracker_obj = {}
    if currencyfilter == 'gbp':
        currency = u'\xa3'
    else:
        currency = u'\u20ac'
    if currency_sites[u'\u20ac']:
        euro_sites = '('+str(functools.reduce(lambda a,b : str(a)+','+str(b),currency_sites[u'\u20ac']))+')'
        # euro_sql_query = """
        #             select p.date,p.fk_campaign_id, p.siteid_id,
        euro_sql_query = """
                    select p.date, p.fk_campaign_id, p.siteid_id, 
                    sum(p.registrations), sum(p.ftd),
                    sum(p.revenue),sum(p.revenue*c.eurtogbp),
                    sum(p.deposit),sum(p.deposit*c.eurtogbp),
                    sum(p.chargeback),sum(p.chargeback*c.eurtogbp),
                    sum(p.cashout),sum(p.cashout*c.eurtogbp),
                    sum(p.void),sum(p.void*c.eurtogbp),
                    sum(p.bonuses),sum(p.bonuses*c.eurtogbp),
                    sum(p.reversechargeback),sum(p.reversechargeback*c.eurtogbp),
                    sum(p.sidegamesbets),sum(p.sidegamesbets*c.eurtogbp),
                    sum(p.sidegameswins),sum(p.sidegameswins*c.eurtogbp) 
                    from connectors_ppplayerprocessed p join userapp_currencyconverted c on c.date=CAST(p.date AS DATE)
                     group by p.fk_campaign_id , p.date having p.date >= '%s' and p.date < '%s' and
                     p.siteid_id IN %s and p.fk_campaign_id IN %s 
                """%(str(from_date).split(' ')[0],
                    str(to_date).split(' ')[0], euro_sites, 
                    tup_i)
        if country_id_list:
            euro_sql_query += euro_sql_query + 'and p.country IN %s'%(country_id_list)
        euro_sql_query += ';'
        euro_camp_list = []
        cursor = connection.cursor()
        euro_set = cursor.execute(euro_sql_query)
        col_names = [desc[0] for desc in cursor.description]
        while True:
            row = cursor.fetchone()
            if row is None:
                break
            row_dict = dict(zip(col_names, row))
            euro_camp_list.append(row_dict)
        for i in euro_camp_list:
            if currencyfilter == 'gbp':
                dict_i = {(j.replace('sum(p.','').replace(')','__sum__togbp') if '*' not in j else j.replace('sum(p.','').replace('*c.eurtogbp)','__sum')):i[j] for j in i}
            else:
                dict_i = {(j.replace('sum(p.','').replace(')','__sum') if '*' not in j else j.replace('sum(p.','').replace('*c.eurtogbp)','__sum__togbp')):i[j] for j in i}
            if api_player_tracker_obj.get(dict_i['fk_campaign_id']):
                for k in dict_i:
                    if type(dict_i[k]) in [int, float]:
                        api_player_tracker_obj[dict_i['fk_campaign_id']][k] += dict_i[k]
            else:
                api_player_tracker_obj[dict_i['fk_campaign_id']] = dict_i

        euro_sql_query = """
                    select p.date,p.fk_campaign_id, p.siteid_id,
                    sum(p.revenue),sum(p.revenue*c.eurtogbp),
                    sum(p.deposit),sum(p.deposit*c.eurtogbp),
                    sum(p.chargeback),sum(p.chargeback*c.eurtogbp),
                    sum(p.cashout),sum(p.cashout*c.eurtogbp),
                    sum(p.void),sum(p.void*c.eurtogbp),
                    sum(p.bonus),sum(p.bonus*c.eurtogbp)
                     from connectors_uploadadvanced p join userapp_currencyconverted c on c.date=CAST(p.date AS DATE)
                     group by p.fk_campaign_id, p.date having p.date >= '%s' and p.date < '%s' and
                     p.siteid_id IN %s and p.fk_campaign_id IN %s;
                """%(str(from_date).split(' ')[0],
                    str(to_date).split(' ')[0], euro_sites, 
                    tup_i)
        euro_camp_list = []
        cursor = connection.cursor()
        euro_set = cursor.execute(euro_sql_query)
        col_names = [desc[0] for desc in cursor.description]
        while True:
            row = cursor.fetchone()
            if row is None:
                break
            row_dict = dict(zip(col_names, row))
            euro_camp_list.append(row_dict)
        for i in euro_camp_list:
            if currencyfilter == 'gbp':
                currency = u'\xa3'
                dict_i = {(j.replace('sum(p.','').replace(')','__sum__togbp') if '*' not in j else j.replace('sum(p.','').replace('*c.eurtogbp)','__sum')):i[j] for j in i}
            else:
                currency = u'\u20ac'
                dict_i = {(j.replace('sum(p.','').replace(')','__sum') if '*' not in j else j.replace('sum(p.','').replace('*c.eurtogbp)','__sum__togbp')):i[j] for j in i}
            if upload_tracker_obj.get(dict_i['fk_campaign_id']):
                for k in dict_i:
                    if type(dict_i[k]) not in [int, float]:
                        continue
                    if upload_tracker_obj[dict_i['fk_campaign_id']].get(k):
                        upload_tracker_obj[dict_i['fk_campaign_id']][k] += dict_i[k]
                    else:
                        upload_tracker_obj[dict_i['fk_campaign_id']][k] = dict_i[k]
            else:
                upload_tracker_obj[dict_i['fk_campaign_id']] = dict_i

    if currency_sites[u'\xa3']:
        gbp_sites = '('+str(functools.reduce(lambda a,b : str(a)+','+str(b),currency_sites[u'\xa3']))+')'
        gbp_sql_query = """
                    select p.date, p.fk_campaign_id, p.siteid_id,
                    sum(p.registrations), sum(p.ftd),
                    sum(p.revenue),sum(p.revenue*c.gbptoeur),
                    sum(p.deposit),sum(p.deposit*c.gbptoeur),
                    sum(p.chargeback),sum(p.chargeback*c.gbptoeur),
                    sum(p.cashout),sum(p.cashout*c.gbptoeur),
                    sum(p.void),sum(p.void*c.gbptoeur),
                    sum(p.bonuses),sum(p.bonuses*c.gbptoeur),
                    sum(p.reversechargeback),sum(p.reversechargeback*c.gbptoeur),
                    sum(p.sidegamesbets),sum(p.sidegamesbets*c.gbptoeur),
                    sum(p.sidegameswins),sum(p.sidegameswins*c.gbptoeur)  
                    from connectors_ppplayerprocessed p join userapp_currencyconverted c on c.date=CAST(p.date AS DATE)
                     group by p.fk_campaign_id , p.date having p.date >= '%s' and p.date< '%s' and
                     p.siteid_id IN %s and p.fk_campaign_id IN %s;
                    """%(str(from_date).split(' ')[0],
                        str(to_date).split(' ')[0], gbp_sites, 
                    tup_i)
        if country_id_list:
            gbp_sql_query += gbp_sql_query + 'and p.country IN %s'%(country_id_list)
        gbp_sql_query += ';'
        gbp_camp_list = []
        cursor = connection.cursor()
        gbp_set = cursor.execute(gbp_sql_query)
        col_names = [desc[0] for desc in cursor.description]
        while True:
            row = cursor.fetchone()
            if row is None:
                break
            row_dict = dict(zip(col_names, row))
            gbp_camp_list.append(row_dict)
        for i in gbp_camp_list:
            if currencyfilter == 'euro':
                dict_i = {(j.replace('sum(p.','').replace(')','__sum__toeuro') if '*' not in j else j.replace('sum(p.','').replace('*c.gbptoeur)','__sum')):i[j] for j in i}
            else:
                dict_i = {(j.replace('sum(p.','').replace(')','__sum') if '*' not in j else j.replace('sum(p.','').replace('*c.gbptoeur)','__sum__toeuro')):i[j] for j in i}
            if api_player_tracker_obj.get(dict_i['fk_campaign_id']):
                for k in dict_i:
                    if type(dict_i[k]) in [int, float]:
                        api_player_tracker_obj[dict_i['fk_campaign_id']][k] += dict_i[k]
            else:
                api_player_tracker_obj[dict_i['fk_campaign_id']] = dict_i

        gbp_sql_query = """
                    select p.date, p.fk_campaign_id, p.siteid_id,
                    sum(p.revenue),sum(p.revenue*c.gbptoeur),
                    sum(p.deposit),sum(p.deposit*c.gbptoeur),
                    sum(p.chargeback),sum(p.chargeback*c.gbptoeur),
                    sum(p.cashout),sum(p.cashout*c.gbptoeur),
                    sum(p.void),sum(p.void*c.gbptoeur),
                    sum(p.bonus),sum(p.bonus*c.gbptoeur)
                     from connectors_uploadadvanced p join userapp_currencyconverted c on c.date=CAST(p.date AS DATE)
                     group by p.fk_campaign_id , p.date having p.date >= '%s' and p.date< '%s' and
                     p.siteid_id IN %s and p.fk_campaign_id IN %s;
                    """%(str(from_date).split(' ')[0],
                        str(to_date).split(' ')[0], gbp_sites, 
                    tup_i)
        gbp_camp_list = []
        cursor = connection.cursor()
        gbp_set = cursor.execute(gbp_sql_query)
        col_names = [desc[0] for desc in cursor.description]
        while True:
            row = cursor.fetchone()
            if row is None:
                break
            row_dict = dict(zip(col_names, row))
            gbp_camp_list.append(row_dict)
        for i in gbp_camp_list:
            if currencyfilter == 'euro':
                dict_i = {(j.replace('sum(p.','').replace(')','__sum__toeuro') if '*' not in j else j.replace('sum(p.','').replace('*c.gbptoeur)','__sum')):i[j] for j in i}
            else:
                dict_i = {(j.replace('sum(p.','').replace(')','__sum') if '*' not in j else j.replace('sum(p.','').replace('*c.gbptoeur)','__sum__toeuro')):i[j] for j in i}
            if upload_tracker_obj.get(dict_i['fk_campaign_id']):
                for k in dict_i:
                    if type(dict_i[k]) in [int, float]:
                        if upload_tracker_obj[dict_i['fk_campaign_id']].get(k):
                            upload_tracker_obj[dict_i['fk_campaign_id']][k] += dict_i[k]
                        else:
                            upload_tracker_obj[dict_i['fk_campaign_id']][k] = dict_i[k]
            else:
                upload_tracker_obj[dict_i['fk_campaign_id']] = dict_i
    
    # api_player_tracker_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
    #                         date__gte=str(from_date).split(' ')[0],
    #                         date__lte=str(to_date).split(' ')[0],
    #                         siteid__in=site_ids).values('fk_campaign').annotate(Sum('deposit'),Sum('chargeback'),
    #                         Sum('cashout'),Sum('void'),Sum('reversechargeback'),
    #                         Sum('bonuses'),
    #                         Sum('sidegamesbets'),Sum('sidegameswins'),
    #                         Sum('jackpotcontribution'),Sum('revenue'))
    # api_player_tracker_obj = {i['fk_campaign']:i for i in api_player_tracker_obj}
    api_new_player_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links, 
                        registrationdate__gte=str(from_date),
                        registrationdate__lte=str(to_date),
                        country__in=country_list,
                        siteid__in=site_ids,deposit__gt=0).values(
                        'fk_campaign').annotate(Sum('deposit'), Sum('revenue'))
    api_new_player_obj = {i['fk_campaign']:i for i in api_new_player_obj}
    api_active_player_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
                        lastdate__gte=str(from_date),
                        lastdate__lte=str(to_date),
                        country__in=country_list,
                        siteid__in=site_ids).values('fk_campaign').annotate(Count('playerid', 
                                 distinct=True))
    api_active_player_obj = {i['fk_campaign']:i for i in api_active_player_obj}
    api_date_deposit_player_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
                        date__gte=str(from_date).split(' ')[0],
                        date__lte=str(to_date).split(' ')[0],
                        country__in=country_list,
                        siteid__in=site_ids,deposit__gt=0).values(
                        'fk_campaign').annotate(Count('playerid', 
                                 distinct=True))
    api_date_deposit_player_obj = {i['fk_campaign']:i for i in api_date_deposit_player_obj}
    api_date_side_player_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
                        date__gte=str(from_date).split(' ')[0],
                        date__lte=str(to_date).split(' ')[0],
                        country__in=country_list,
                        siteid__in=site_ids,sidegamesbets__gt=0).values(
                        'fk_campaign').annotate(Count('playerid', 
                                 distinct=True))
    api_date_side_player_obj = {i['fk_campaign']:i for i in api_date_side_player_obj}

    api_new_side_player_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links, 
                        registrationdate__gte=str(from_date),
                        registrationdate__lte=str(to_date),
                        country__in=country_list,
                        siteid__in=site_ids,sidegamesbets__gt=0).values(
                        'fk_campaign').annotate(Sum('sidegamesbets'))
    api_new_side_player_obj = {i['fk_campaign']:i for i in api_new_side_player_obj}

    
    api_avg_active_players_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
                        registrationdate__gte=str(from_date).split(' ')[0],
                        lastdate__lte=str(to_date).split(' ')[0],
                        country__in=country_list,
                        siteid__in=site_ids).values(
                        'fk_campaign').annotate(Count('playerid', 
                                 distinct=True))
    api_avg_active_players_obj = {i['fk_campaign']:i for i in api_avg_active_players_obj}
    newplayer_list = []
    new_player_count = 0
    new_player_deposit = 0.00
    active_player_count= 0
    diposit_accounts = 0
    wagering_accounts = 0
    total_wagers = 0.00
    total_value = 0.00
    days_count = to_date - from_date
    days_count = days_count.days
    avg_active_players_days = 0
    avg_active_players = 0
    avg_net_cash = 0.00
    avg_ltv = 0.00
    revenue = 0.0
    deposit = 0.0
    cashout = 0.0
    chargeback = 0.0
    void = 0.0
    bonus = 0.0
    registrations = 0
    revenue_c = 0.0
    ftds = 0
    cpa_acc = 0
    clicks = 0
    avg_ltv_list = []
    revenue_ltv = 0.0
    cpa_value = 0.00
    cpr_value = 0.00
    rev_value = 0.00
    reg_rate = '0.00'
    ftd_rate = '0.00'
    ftd_deposit_list = PPPlayerProcessed.objects.filter(fk_campaign=camp_links,
                ftddate__gte=str(from_date).split(' ')[0],
                ftddate__lte=str(to_date),
                siteid__in=site_ids,
                ftddate = F('date')
                ).values('playerid', 'deposit', 'fk_campaign').distinct()
    ftd_dict_deposit = {}
    for d in ftd_deposit_list:
        if int(d['fk_campaign']) in ftd_dict_deposit:
            ftd_dict_deposit[int(d['fk_campaign'])].append(d)
        else:
            ftd_dict_deposit[int(d['fk_campaign'])] = [d]
    for i in camp_links:
        cpa_acc = 0
        total_commission_acc = 0.00
        cpa_value_acc = 0.00
        cpr_value_acc = 0.00
        rev_value_acc = 0.00
        # currency = currency_dict[i.fk_campaign.siteid]
        content_type = 'POST_BACK'
        clicks += api_clicks[i.id]['clicks__sum'] if api_clicks.get(i.id) else 0
        if api_player_tracker_obj.get(i.id):
            # cpa_accounts = new_player_obj.values('playerid').annotate(Sum('deposit'))
            # for c in cpa_accounts:
            #     newplayer_list.append(c)
            
            # if newplayer_list:
            #     new_player_count = len(newplayer_list)
            #     for j in newplayer_list:
            #         new_player_deposit = new_player_deposit + j['deposit__sum']
            # new_player_count = new_player_obj.count()
            if api_new_player_obj.get(i.id) and api_new_side_player_obj.get(i.id):
                new_player_deposit += api_new_player_obj.get(i.id)['deposit__sum']
                avg_ltv += (api_new_player_obj[i.id]['revenue__sum']/api_new_side_player_obj[i.id]['sidegamesbets__sum'])
            if api_active_player_obj.get(i.id):
                active_player_count += api_active_player_obj.get(i.id)['playerid__count']
            revenue_c = api_player_tracker_obj[i.id]['revenue__sum']
            deposit += api_player_tracker_obj[i.id]['deposit__sum']
            cashout += api_player_tracker_obj[i.id]['cashout__sum']
            chargeback += api_player_tracker_obj[i.id]['chargeback__sum']
            void += api_player_tracker_obj[i.id]['void__sum']
            bonus += api_player_tracker_obj[i.id]['bonuses__sum']
            # deposit_list = [f for f in date_player_obj.values('playerid').annotate(Sum('deposit')) if f['deposit__sum']>0.0]
            # side_bets_list = [k for k in date_player_obj.values('playerid').annotate(Sum('sidegamesbets')) if k['sidegamesbets__sum']>0.0]
        if api_date_deposit_player_obj.get(i.id):
            diposit_accounts += api_date_deposit_player_obj[i.id]['playerid__count']
        if api_date_side_player_obj.get(i.id):
            wagering_accounts += api_date_side_player_obj[i.id]['playerid__count']
            avg_net_cash += round(revenue/api_date_side_player_obj[i.id]['playerid__count'],2)
        if api_player_tracker_obj.get(i.id) and api_player_tracker_obj.get(i.id).get('sidegamesbets__sum'):
            total_wagers += round(api_player_tracker_obj[i.id]['sidegamesbets__sum'], 2)
        if api_avg_active_players_obj.get(i.id):
            avg_active_players += int(api_avg_active_players_obj[i.id]['playerid__count'])            
        if api_registrations.get(i.id):
            registrations += api_registrations[i.id]['playerid__count']
        if api_ftds.get(i.id):
            ftds += api_ftds[i.id]['playerid__count']
        if upload_tracker_obj.get(i.id):
            content_type = 'UPLOAD'
            if upload_tracker_obj[i.id].get('revenue__sum'):
                revenue_c = upload_tracker_obj[i.id]['revenue__sum']
            if upload_tracker_obj[i.id].get('deposit__sum'):
                deposit += upload_tracker_obj[i.id]['deposit__sum']
            if upload_tracker_obj[i.id].get('cashout__sum'):
                cashout += upload_tracker_obj[i.id]['cashout__sum']
            if upload_tracker_obj[i.id].get('chargeback__sum'):
                chargeback += upload_tracker_obj[i.id]['chargeback__sum']
            if upload_tracker_obj[i.id].get('void__sum'):
                void += upload_tracker_obj[i.id]['void__sum']
            if upload_tracker_obj[i.id].get('bonus__sum'):
                bonus += upload_tracker_obj[i.id]['bonus__sum']
            registrations += upload_tracker_obj[i.id]['registrations__sum']
            ftds += upload_tracker_obj[i.id]['ftd__sum']
            clicks = clicks + upload_tracker_obj[i.id]['clicks__sum']
            cpa_acc = upload_tracker_obj[i.id]['ftd__sum']

        tracker = i.fk_campaign.name.replace('vcommsubaff', '')

        # deals
        commission = commission_dict.get(i.id) if commission_dict.get(i.id) else 0
        if commission:
            if not commission['cpa'][1]:
                commission['cpa'][1] = 0
            # if commission['pocdeduction']:
            #     revenue_c = (revenue_c - (revenue_c*0.21))
            if commission['cpa'] and commission['cpa'][0] == 'FTD' and commission['cpa'][2]:
                res_list = []
                # if i.fk_campaign.siteid.id in currency_sites[u'\u20ac']:
                #     euro_sites_query = """
                #         select distinct(p.playerid), p.deposit, c.eurtogbp, p.ftddate
                #         from connectors_ppplayerprocessed p join userapp_currencyconverted c on c.date= CAST(p.ftddate AS DATE)
                #          group by p.fk_campaign_id ,p.playerid having p.ftddate >= '%s' and p.ftddate< '%s' and
                #          p.fk_campaign_id = %s and p.deposit*c.eurtogbp > %s;
                #         """%(str(from_date).split(' ')[0],
                #             str(to_date).split(' ')[0], 
                #             i.id, commission['cpa'][1])
                #     euro_camp_list = []
                #     cursor = connection.cursor()
                #     euro_set = cursor.execute(euro_sites_query)
                #     col_names = [desc[0] for desc in cursor.description]
                #     while True:
                #         row = cursor.fetchone()
                #         if row is None:
                #             break
                #         row_dict = dict(zip(col_names, row))
                #         euro_camp_list.append(row_dict)
                #     api_ftd_player_obj = euro_camp_list
                # else:
                if not ftd_dict_deposit.get(int(i.id)):
                    api_ftd_player_obj = []
                else:
                    api_ftd_player_obj = [j for j in ftd_dict_deposit[int(i.id)] if j['deposit'] > commission['cpa'][1]]
                # api_ftd_player_obj = PPPlayerProcessed.objects.filter(fk_campaign=i,
                #                     ftddate__gte=str(from_date).split(' ')[0],
                #                     ftddate__lte=str(to_date),
                #                     siteid__in=site_ids,deposit__gt=commission['cpa'][1],
                #                     ftddate = F('date')
                #                     ).values('playerid').distinct()
                cpa_acc = cpa_acc + len(api_ftd_player_obj)
                cpa_value_acc += (cpa_acc) * float(commission['cpa'][2])
            # elif commission['cpa'] and commission['cpa'][0] == 'FTD' and commission['cpa'][2] and content_type != 'UPLOAD':
            #     res_list = []
            #     api_ftd_player_obj = PPPlayerProcessed.objects.filter(fk_campaign=i,
            #                         date__gte=str(from_date).split(' ')[0],
            #                         date__lte=str(to_date).split(' ')[0],
            #                         siteid__in=site_ids,deposit__gt=commission['cpa'][1]
            #                         ).values('playerid').distinct()
            #     cpa_acc = len(api_ftd_player_obj)
            #     cpa_value = (cpa_acc) * float(commission['cpa'][2])
            #     cpa_value = round(cpa_value, 2)
            # if commission.referalchoice:
            #     cpr_value = (tracker_obj['cashout']) * (commission.referalcommissionvalue)
            if commission['revshare'] and revenue_c:
                rev_value_acc += (revenue_c * (commission['revshare']))/100
        revenue += revenue_c
        total_commission_acc += cpa_value_acc + cpr_value_acc + rev_value_acc
        total_value += total_commission_acc
        cpa_value += cpa_value_acc
        rev_value += rev_value_acc
    total_value = round(total_value, 2)
    rev_value = round(rev_value, 2)
    cpa_value = round(cpa_value, 2)
    if registrations and clicks:
        reg_rate = float(registrations)/float(clicks)
        reg_rate = round(reg_rate*100, 2)
        reg_rate = str(reg_rate)
    if registrations and ftds:
        ftd_rate = float(ftds)/float(registrations)
        ftd_rate = round(ftd_rate*100, 2)
        ftd_rate = str(ftd_rate)
    aff_data += [0, clicks, 0,
                registrations, reg_rate,
                ftds, ftd_rate, new_player_count,
                round(new_player_deposit, 2) if new_player_deposit else currency+'0.00', active_player_count, diposit_accounts, wagering_accounts, total_wagers,
                avg_active_players, avg_net_cash,
                round(deposit, 2) if deposit else currency+'0.00',
                round(cashout, 2) if cashout else currency+'0.00',
                round(chargeback, 2) if chargeback else currency+'0.00', 
                round(void, 2) if void else currency+'0.00',
                round(bonus, 2) if bonus else currency+'0.00',
                round(revenue, 2) if revenue else currency+'0.00',
                round(avg_ltv, 2) if avg_ltv else currency+'0.00',
                cpa_value, rev_value, total_value]
    # if len(total_list) == 2:
    #     total_list = total_list + aff_data[2:]
    # else:
    #     total_list = [x + y if not isinstance(x,str) else x for x, y in zip(total_list, aff_data)]
    aff_data = [k if (isinstance(k,unicode) or '.' not in str(k) or isinstance(k,str)) else (currency + str(k)) for k in aff_data]
    # aff_data = [k if k != (currency + '0.0') else (currency + '0.00') for k in aff_data]
    # data_list.append(aff_data)
    print 'brnad group by end', site_ids
    return aff_data

def run_player_report(comm_type, siteid, fk_affiliateinfo, acmanager):
    date_now = datetime.now()
    date_check = datetime.now().replace(day=1)
    try:
        dates_days_list = [dt for dt in rrule(DAILY, dtstart=date_check, until=date_now)]
        for ind, d in enumerate(dates_days_list):
            if len(dates_days_list) == ind:
                continue
            from_date_time = datetime.combine(d, time(0,0,0))
            to_date_time = datetime.combine(d, time(23,59,59))
            #site_ids = Site.objects.filter(id=158)
            if siteid:
                site_ids = Site.objects.filter(id=siteid)
            if comm_type == 'default' or fk_affiliateinfo:
                site_ids = acmanager.siteid.all()
            for site_id in site_ids:
                campd_list = get_site_aff_camp([site_id])
                if fk_affiliateinfo:
                    campd_list[1] = campd_list[1].filter(fk_affiliateinfo_id=fk_affiliateinfo)
                db_campaign_entry_add(from_date_time, to_date_time, campd_list)
    except Exception as e:
        print 'Error', str(e)

def get_site_aff_camp(site_ids):
    # site_ids = Site.objects.all()
    #acmanager = AccountManager.objects.get(affiliateprogramme__name='Maxi Affiliates')
    #site_ids = acmanager.siteid.all()
    currency_sites = {}
    currency_objs = Currency.objects.filter(siteid__in=site_ids)
    for cur in currency_objs:
        if cur.currency in currency_sites:
            currency_sites[cur.currency].append(int(cur.siteid.id))
        else:
            currency_sites[cur.currency] = [int(cur.siteid.id)]
    if not currency_sites.get(u'\xa3'):
        currency_sites[u'\xa3'] = []
    if not currency_sites.get(u'\u20ac'):
        currency_sites[u'\u20ac'] = []
    aff_list = Affiliateinfo.objects.filter(siteid__in=site_ids)
    camp_links = CampaignTrackerMapping.objects.filter(fk_campaign__siteid__in=site_ids
                    ).select_related('fk_campaign','fk_affiliateinfo')
    # aff_list = Affiliateinfo.objects.filter(siteid__in=site_ids)
    # commission_dict = get_commission_all_dict(camp_links, aff_list, site_ids)
    country_ids_list = {i['countryname']: i['id'] for i in Country.objects.all().values('id', 'countryname')}
    return [currency_sites, camp_links, aff_list, site_ids, country_ids_list]


def db_campaign_entry_add(from_date, to_date, campd_list):
    currency_sites, camp_links, aff_list, site_ids, country_ids_list = campd_list
    if not aff_list:
        return True
    #import pdb;pdb.set_trace()
    # CampaignDateReport.objects.filter(siteid__in=site_ids, date=from_date.date()).delete()
    camp_link = PostBackClicks.objects.filter(fk_campaign__in=camp_links,
                date__gte=from_date,
                date__lte=to_date,
                ).values_list('fk_campaign_id', flat=True).order_by('fk_campaign_id').distinct()
    camp_link_2 = UploadAdvanced.objects.filter(fk_campaign__in=camp_links,
                    date__gte=str(from_date).split(' ')[0],
                    date__lte=str(to_date+timedelta(days=1)).split(' ')[0],
                    processed=True).values_list('fk_campaign_id', flat=True).order_by('fk_campaign_id').distinct()
    camp_link_1 = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
                date__gte=str(from_date).split(' ')[0],
                date__lt=str(to_date+timedelta(days=1)).split(' ')[0],
                ).values_list('fk_campaign_id', flat=True).order_by('fk_campaign_id').distinct()
    api_registrations = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
                        registrationdate__gte=str(from_date),
                        registrationdate__lte=str(to_date),
                        siteid__in=site_ids).values_list('fk_campaign', flat=True).distinct()
    api_ftds = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
                        ftddate__gte=str(from_date),
                        ftddate__lte=str(to_date),
                        siteid__in=site_ids,deposit__gt=0
                        ).exclude(ftddate__isnull=True).values_list('fk_campaign', flat=True).distinct()
    new_list = []
    new_list = [i for i in camp_link]
    new_list_1 = [i for i in camp_link_1]
    new_list_2 = [i for i in camp_link_2]
    new_list_3 = [i for i in api_registrations]
    new_list_4 = [i for i in api_ftds]
    camp_link_ids = list(set(new_list + new_list_1 + new_list_2 + new_list_3 + new_list_4))
    camp_links = camp_links.filter(id__in=camp_link_ids)
    if not camp_links:
        return True
    db_campaign_date_entry(currency_sites, camp_links, aff_list, site_ids, country_ids_list,
            from_date, to_date)

def db_campaign_date_entry(currency_sites, camp_links, aff_list, site_ids, country_ids_list,
            from_date, to_date):
    country_list = []
    country_id_list = ''
    data_list = []
    currencyfilter = 'euro'
    # import pdb;pdb.set_trace()
    commission_dict = get_commission_all_dict(camp_links, aff_list, site_ids, from_date)
    # if type(obj) != Affiliateinfo:
    #     commission_dict = get_commission_dict(obj[0], camp_links, obj, site_ids)
    # else:
    #     commission_dict = get_commission_dict(obj, camp_links, [obj], site_ids)
    aff_list = []
    i = [int(j.id) for j in camp_links]
    tup_i = '(-1)'
    if i:
        tup_i = '('+str(functools.reduce(lambda a,b : str(a)+','+str(b),i))+')'
    upload_tracker_obj = {}
    upload_tracker_obj = UploadAdvanced.objects.filter(fk_campaign__in=camp_links,
                    date__gte=str(from_date).split(' ')[0],
                    date__lte=str(to_date+timedelta(days=1)).split(' ')[0],
                    siteid__in=site_ids,
                    processed=True).values(
                        'fk_campaign').annotate(Sum('ftd'),
                    Sum('registrations'), Sum('clicks'))
    upload_tracker_obj = {(i['fk_campaign'], 'United Kingdom', None):i for i in upload_tracker_obj}
    upload_player_tracker_obj = {}
    upload_new_player_obj = {}
    upload_active_player_obj = {}
    upload_date_player_obj = {}
    upload_ftd_player_obj = {}
    upload_avg_active_players_obj = {}
    w_dynamics_list = PostBackClicks.objects.filter(fk_campaign__in=camp_links,
                            siteid__in=site_ids
                            ).values('wdynamic').distinct()
    w_dynamics_list = [i['wdynamic'] for i in w_dynamics_list]
    
    api_registrations = PPPlayerProcessed.objects.filter(siteid__in=site_ids).values('wdynamic').distinct()
    api_registrations_dynamics_list = [i['wdynamic'] for i in api_registrations]
    
    w_dynamics_list = w_dynamics_list + api_registrations_dynamics_list
    w_dynamics_list = list(set(w_dynamics_list))
    # tracker_obj = get_cliks_data([], from_date, to_date, site_ids, [i])
    api_clicks = PostBackClicks.objects.filter(fk_campaign__in=camp_links,
                            date__gte=from_date,
                            date__lte=to_date,
                            siteid__in=site_ids
                            ).values(
                            'fk_campaign', 'country', 'wdynamic').annotate(clicks__sum=Count('id'))
    api_clicks = {(i['fk_campaign'],i['country'], i['wdynamic']):i for i in api_clicks}
    api_registrations = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links, 
                        registrationdate__gte=str(from_date),
                        registrationdate__lte=str(to_date),
                        siteid__in=site_ids).values('fk_campaign', 'country', 'wdynamic').annotate(Count('playerid', 
                                 distinct=True))
    api_registrations = {(i['fk_campaign'],i['country'], i['wdynamic']):i for i in api_registrations}
    api_ftds = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links, 
                        ftddate__gte=str(from_date),
                        ftddate__lte=str(to_date),
                        siteid__in=site_ids,deposit__gt=0
                        ).exclude(ftddate__isnull=True).values('fk_campaign', 'country', 'wdynamic').annotate(Count('playerid', 
                                 distinct=True))
    api_ftds = {(i['fk_campaign'],i['country'], i['wdynamic']):i for i in api_ftds}
    api_player_tracker_obj = {}
    if currencyfilter == 'gbp':
        currency = u'\xa3'
    else:
        currency = u'\u20ac'
    if currency_sites[u'\u20ac']:
        euro_sites = '('+str(functools.reduce(lambda a,b : str(a)+','+str(b),currency_sites[u'\u20ac']))+')'
        # euro_sql_query = """
        #             select p.date,p.fk_campaign_id, p.siteid_id,
        euro_sql_query = """
                    select p.date, p.country, p.wdynamic, p.fk_campaign_id, p.siteid_id, 
                    sum(p.registrations), sum(p.ftd),
                    sum(p.revenue),sum(p.revenue*c.eurtogbp),
                    sum(p.deposit),sum(p.deposit*c.eurtogbp),
                    sum(p.chargeback),sum(p.chargeback*c.eurtogbp),
                    sum(p.cashout),sum(p.cashout*c.eurtogbp),
                    sum(p.void),sum(p.void*c.eurtogbp),
                    sum(p.bonuses),sum(p.bonuses*c.eurtogbp),
                    sum(p.reversechargeback),sum(p.reversechargeback*c.eurtogbp),
                    sum(p.sidegamesbets),sum(p.sidegamesbets*c.eurtogbp),
                    sum(p.sidegameswins),sum(p.sidegameswins*c.eurtogbp) 
                    from connectors_ppplayerprocessed p join userapp_currencyconverted c on c.date=CAST(p.date AS DATE) 
                    where p.date >= '%s' and p.date < '%s' and
                     p.siteid_id IN %s and p.fk_campaign_id IN %s group by p.fk_campaign_id , p.date, p.country, p.wdynamic
                """%(str(from_date).split(' ')[0],
                    str(to_date+timedelta(days=1)).split(' ')[0], euro_sites, 
                    tup_i)
        if country_id_list:
            euro_sql_query += euro_sql_query + 'and p.country IN %s'%(country_id_list)
        euro_sql_query += ';'
        euro_camp_list = []
        cursor = connection.cursor()
        euro_set = cursor.execute(euro_sql_query)
        col_names = [desc[0] for desc in cursor.description]
        while True:
            row = cursor.fetchone()
            if row is None:
                break
            row_dict = dict(zip(col_names, row))
            euro_camp_list.append(row_dict)
        for i in euro_camp_list:
            if currencyfilter == 'gbp':
                dict_i = {(j.replace('sum(p.','').replace(')','__sum__togbp') if '*' not in j else j.replace('sum(p.','').replace('*c.eurtogbp)','__sum')):i[j] for j in i}
            else:
                dict_i = {(j.replace('sum(p.','').replace(')','__sum') if '*' not in j else j.replace('sum(p.','').replace('*c.eurtogbp)','__sum__togbp')):i[j] for j in i}
            if api_player_tracker_obj.get((dict_i['fk_campaign_id'], dict_i['country'], i['wdynamic'])):
                for k in dict_i:
                    if type(dict_i[k]) in [int, float]:
                        api_player_tracker_obj[(dict_i['fk_campaign_id'], dict_i['country'], i['wdynamic'])][k] += dict_i[k]
            else:
                api_player_tracker_obj[(dict_i['fk_campaign_id'], dict_i['country'], i['wdynamic'])] = dict_i

        euro_sql_query = """
                    select p.date,p.fk_campaign_id, p.siteid_id,
                    sum(p.revenue),sum(p.revenue*c.eurtogbp),
                    sum(p.deposit),sum(p.deposit*c.eurtogbp),
                    sum(p.chargeback),sum(p.chargeback*c.eurtogbp),
                    sum(p.cashout),sum(p.cashout*c.eurtogbp),
                    sum(p.void),sum(p.void*c.eurtogbp),
                    sum(p.bonus),sum(p.bonus*c.eurtogbp)
                     from connectors_uploadadvanced p join userapp_currencyconverted c on c.date=CAST(p.date AS DATE)
                     where p.date >= '%s' and p.date < '%s' and
                     p.siteid_id IN %s and p.fk_campaign_id IN %s group by p.fk_campaign_id, p.date;
                """%(str(from_date).split(' ')[0],
                    str(to_date+timedelta(days=1)).split(' ')[0], euro_sites, 
                    tup_i)
        euro_camp_list = []
        cursor = connection.cursor()
        euro_set = cursor.execute(euro_sql_query)
        col_names = [desc[0] for desc in cursor.description]
        while True:
            row = cursor.fetchone()
            if row is None:
                break
            row_dict = dict(zip(col_names, row))
            euro_camp_list.append(row_dict)
        for i in euro_camp_list:
            if currencyfilter == 'gbp':
                currency = u'\xa3'
                dict_i = {(j.replace('sum(p.','').replace(')','__sum__togbp') if '*' not in j else j.replace('sum(p.','').replace('*c.eurtogbp)','__sum')):i[j] for j in i}
            else:
                currency = u'\u20ac'
                dict_i = {(j.replace('sum(p.','').replace(')','__sum') if '*' not in j else j.replace('sum(p.','').replace('*c.eurtogbp)','__sum__togbp')):i[j] for j in i}
            if upload_tracker_obj.get((dict_i['fk_campaign_id'], 'United Kingdom', None)):
                for k in dict_i:
                    if type(dict_i[k]) not in [int, float]:
                        continue
                    if upload_tracker_obj[(dict_i['fk_campaign_id'], 'United Kingdom', None)].get(k):
                        upload_tracker_obj[(dict_i['fk_campaign_id'], 'United Kingdom', None)][k] += dict_i[k]
                    else:
                        upload_tracker_obj[(dict_i['fk_campaign_id'], 'United Kingdom', None)][k] = dict_i[k]
            else:
                upload_tracker_obj[(dict_i['fk_campaign_id'], 'United Kingdom', None)] = dict_i

    if currency_sites[u'\xa3']:
        
        gbp_sites = '('+str(functools.reduce(lambda a,b : str(a)+','+str(b),currency_sites[u'\xa3']))+')'
        gbp_sql_query = """
                    select p.date, p.country, p.wdynamic, p.fk_campaign_id, p.siteid_id,
                    sum(p.registrations), sum(p.ftd),
                    sum(p.revenue),sum(p.revenue*c.gbptoeur),
                    sum(p.deposit),sum(p.deposit*c.gbptoeur),
                    sum(p.chargeback),sum(p.chargeback*c.gbptoeur),
                    sum(p.cashout),sum(p.cashout*c.gbptoeur),
                    sum(p.void),sum(p.void*c.gbptoeur),
                    sum(p.bonuses),sum(p.bonuses*c.gbptoeur),
                    sum(p.reversechargeback),sum(p.reversechargeback*c.gbptoeur),
                    sum(p.sidegamesbets),sum(p.sidegamesbets*c.gbptoeur),
                    sum(p.sidegameswins),sum(p.sidegameswins*c.gbptoeur)  
                    from connectors_ppplayerprocessed p join userapp_currencyconverted c on c.date=CAST(p.date AS DATE)
                     where p.date >= '%s' and p.date< '%s' and
                     p.siteid_id IN %s and p.fk_campaign_id IN %s group by p.fk_campaign_id , p.date, p.country, p.wdynamic
                    """%(str(from_date).split(' ')[0],
                        str(to_date+timedelta(days=1)).split(' ')[0], gbp_sites, 
                    tup_i)
        if country_id_list:
            gbp_sql_query += gbp_sql_query + 'and p.country IN %s'%(country_id_list)
        gbp_sql_query += ';'
        gbp_camp_list = []
        cursor = connection.cursor()
        gbp_set = cursor.execute(gbp_sql_query)
        col_names = [desc[0] for desc in cursor.description]
        while True:
            row = cursor.fetchone()
            if row is None:
                break
            row_dict = dict(zip(col_names, row))
            gbp_camp_list.append(row_dict)
        
        for i in gbp_camp_list:
            if currencyfilter == 'euro':
                dict_i = {(j.replace('sum(p.','').replace(')','__sum__toeuro') if '*' not in j else j.replace('sum(p.','').replace('*c.gbptoeur)','__sum')):i[j] for j in i}
            else:
                dict_i = {(j.replace('sum(p.','').replace(')','__sum') if '*' not in j else j.replace('sum(p.','').replace('*c.gbptoeur)','__sum__toeuro')):i[j] for j in i}
            if api_player_tracker_obj.get((dict_i['fk_campaign_id'], dict_i['country'], i['wdynamic'])):
                for k in dict_i:
                    if type(dict_i[k]) in [int, float]:
                        api_player_tracker_obj[(dict_i['fk_campaign_id'], dict_i['country'], i['wdynamic'])][k] += dict_i[k]
            else:
                api_player_tracker_obj[(dict_i['fk_campaign_id'], dict_i['country'], i['wdynamic'])] = dict_i
        gbp_sql_query = """
                    select p.date, p.fk_campaign_id, p.siteid_id,
                    sum(p.revenue),sum(p.revenue*c.gbptoeur),
                    sum(p.deposit),sum(p.deposit*c.gbptoeur),
                    sum(p.chargeback),sum(p.chargeback*c.gbptoeur),
                    sum(p.cashout),sum(p.cashout*c.gbptoeur),
                    sum(p.void),sum(p.void*c.gbptoeur),
                    sum(p.bonus),sum(p.bonus*c.gbptoeur)
                     from connectors_uploadadvanced p join userapp_currencyconverted c on c.date=CAST(p.date AS DATE)
                     where p.date >= '%s' and p.date< '%s' and
                     p.siteid_id IN %s and p.fk_campaign_id IN %s group by p.fk_campaign_id , p.date;
                    """%(str(from_date).split(' ')[0],
                        str(to_date+timedelta(days=1)).split(' ')[0], gbp_sites, 
                    tup_i)
        gbp_camp_list = []
        cursor = connection.cursor()
        gbp_set = cursor.execute(gbp_sql_query)
        col_names = [desc[0] for desc in cursor.description]
        while True:
            row = cursor.fetchone()
            if row is None:
                break
            row_dict = dict(zip(col_names, row))
            gbp_camp_list.append(row_dict)
        for i in gbp_camp_list:
            if currencyfilter == 'euro':
                dict_i = {(j.replace('sum(p.','').replace(')','__sum__toeuro') if '*' not in j else j.replace('sum(p.','').replace('*c.gbptoeur)','__sum')):i[j] for j in i}
            else:
                dict_i = {(j.replace('sum(p.','').replace(')','__sum') if '*' not in j else j.replace('sum(p.','').replace('*c.gbptoeur)','__sum__toeuro')):i[j] for j in i}
            if upload_tracker_obj.get((dict_i['fk_campaign_id'], 'United Kingdom', None)):
                for k in dict_i:
                    if type(dict_i[k]) in [int, float]:
                        if upload_tracker_obj[(dict_i['fk_campaign_id'], 'United Kingdom', None)].get(k):
                            upload_tracker_obj[(dict_i['fk_campaign_id'], 'United Kingdom', None)][k] += dict_i[k]
                        else:
                            upload_tracker_obj[(dict_i['fk_campaign_id'], 'United Kingdom', None)][k] = dict_i[k]
            else:
                upload_tracker_obj[(dict_i['fk_campaign_id'], 'United Kingdom', None)] = dict_i
        
    # api_player_tracker_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
    #                         date__gte=str(from_date).split(' ')[0],
    #                         date__lte=str(to_date).split(' ')[0],
    #                         siteid__in=site_ids).values('fk_campaign').annotate(Sum('deposit'),Sum('chargeback'),
    #                         Sum('cashout'),Sum('void'),Sum('reversechargeback'),
    #                         Sum('bonuses'),
    #                         Sum('sidegamesbets'),Sum('sidegameswins'),
    #                         Sum('jackpotcontribution'),Sum('revenue'))
    # api_player_tracker_obj = {i['fk_campaign']:i for i in api_player_tracker_obj}
    api_new_player_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links, 
                        registrationdate__gte=str(from_date),
                        registrationdate__lte=str(to_date),
                        siteid__in=site_ids,deposit__gt=0).values(
                        'fk_campaign', 'country', 'wdynamic').annotate(Sum('deposit'), Sum('revenue'))
    api_new_player_obj = {(i['fk_campaign'],i['country'], i['wdynamic']):i for i in api_new_player_obj}
    api_active_player_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
                        lastdate__gte=str(from_date),
                        lastdate__lte=str(to_date),
                        siteid__in=site_ids).values('fk_campaign', 'country', 'wdynamic').annotate(Count('playerid', 
                                 distinct=True))
    api_active_player_obj = {(i['fk_campaign'],i['country'], i['wdynamic']):i for i in api_active_player_obj}
    api_date_deposit_player_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
                        date__gte=str(from_date).split(' ')[0],
                        date__lte=str(to_date).split(' ')[0],
                        siteid__in=site_ids,deposit__gt=0).values(
                        'fk_campaign', 'country', 'wdynamic').annotate(Count('playerid', 
                                 distinct=True))
    api_date_deposit_player_obj = {(i['fk_campaign'],i['country'], i['wdynamic']):i for i in api_date_deposit_player_obj}
    api_date_side_player_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
                        date__gte=str(from_date).split(' ')[0],
                        date__lte=str(to_date).split(' ')[0],
                        siteid__in=site_ids,sidegamesbets__gt=0).values(
                        'fk_campaign', 'country', 'wdynamic').annotate(Count('playerid', 
                                 distinct=True))
    api_date_side_player_obj = {(i['fk_campaign'],i['country'], i['wdynamic']):i for i in api_date_side_player_obj}

    api_new_side_player_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links, 
                        registrationdate__gte=str(from_date),
                        registrationdate__lte=str(to_date),
                        siteid__in=site_ids,sidegamesbets__gt=0).values(
                        'fk_campaign', 'country', 'wdynamic').annotate(Sum('sidegamesbets'))
    api_new_side_player_obj = {(i['fk_campaign'],i['country'], i['wdynamic']):i for i in api_new_side_player_obj}

    
    api_avg_active_players_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
                        registrationdate__gte=str(from_date).split(' ')[0],
                        lastdate__lte=str(to_date).split(' ')[0],
                        siteid__in=site_ids).values(
                        'fk_campaign', 'country', 'wdynamic').annotate(Count('playerid', 
                                 distinct=True))
    api_avg_active_players_obj = {(i['fk_campaign'],i['country'], i['wdynamic']):i for i in api_avg_active_players_obj}
    list_country = [i['country'] for i in PPPlayerProcessed.objects.values('country').distinct()]
    list_country.append('')
    ftd_dict_deposit = {}
    tup_i = '(-1)'
    i = [int(j.id) for j in camp_links]
    if i:
        tup_i = '('+str(functools.reduce(lambda a,b : str(a)+','+str(b),i))+')'
    site_ids_list = [i.id for i in site_ids]
    euro_sites = '('+str(functools.reduce(lambda a,b : str(a)+','+str(b),site_ids_list))+')'
    euro_sql_query = """
                SELECT DISTINCT `connectors_ppplayerprocessed`.`playerid`, `connectors_ppplayerprocessed`.`deposit`,
                    `connectors_ppplayerprocessed`.`revenue`,
                 `connectors_ppplayerprocessed`.`fk_campaign_id`, `connectors_ppplayerprocessed`.`country`, 
                 `connectors_ppplayerprocessed`.`wdynamic` FROM `connectors_ppplayerprocessed` WHERE 
                 ((`connectors_ppplayerprocessed`.`fk_campaign_id`) IN %s 
                 AND (`connectors_ppplayerprocessed`.`siteid_id`) IN %s 
                 AND `connectors_ppplayerprocessed`.`ftddate` < '%s' 
                 AND `connectors_ppplayerprocessed`.`ftddate` >= '%s' 
                 AND CAST(`connectors_ppplayerprocessed`.`ftddate` As DATE) = (`connectors_ppplayerprocessed`.`date`) 
                 AND NOT (`connectors_ppplayerprocessed`.`ftddate` IS NULL))
            """%(tup_i, euro_sites, str(to_date+timedelta(days=1)).split(' ')[0],
                        str(from_date).split(' ')[0])
    euro_sql_query += ';'
    euro_camp_list = []
    cursor = connection.cursor()
    euro_set = cursor.execute(euro_sql_query)
    col_names = [desc[0] for desc in cursor.description]
    while True:
        row = cursor.fetchone()
        if row is None:
            break
        row_dict = dict(zip(col_names, row))
        euro_camp_list.append(row_dict)

    for i in euro_camp_list:
        if (i['fk_campaign_id'],i['country'], i['wdynamic']) in ftd_dict_deposit:
            ftd_dict_deposit[(i['fk_campaign_id'],i['country'], i['wdynamic'])].append(i)
        else:
            ftd_dict_deposit[(i['fk_campaign_id'],i['country'], i['wdynamic'])] = [i]
    # for i in camp_links:
    #     for country in list_country:
    if None not in w_dynamics_list:
        w_dynamics_list.append(None)
    import itertools
    a = [camp_links,list_country,w_dynamics_list]
    camp_country_list = list(itertools.product(*a))
    currency_obj = CurrencyConverted.objects.get(date=from_date.date())
    # camp_country_list = [(x,y) for x in camp_links for y in list_country]
    
    for k in camp_country_list:
        i = k[0]
        country = k[1]
        w_dynamic = k[2]
        newplayer_list = []
        new_player_count = 0
        new_player_deposit = 0.00
        active_player_count= 0
        diposit_accounts = 0
        wagering_accounts = 0
        sidegameswins = 0.00
        total_wagers = 0.00
        total_value = 0.00
        days_count = to_date - from_date
        days_count = days_count.days
        avg_active_players_days = 0
        avg_active_players = 0
        avg_net_cash = 0.00
        avg_ltv = 0.00
        revenue = 0.0
        deposit = 0.0
        cashout = 0.0
        chargeback = 0.0
        void = 0.0
        bonus = 0.0
        registrations = 0
        revenue_c = 0.0
        ftds = 0
        cpa_acc = 0
        clicks = 0
        avg_ltv_list = []
        revenue_ltv = 0.0
        cpa_value = 0.00
        cpr_value = 0.00
        rev_value = 0.00
        reg_rate = '0.00'
        ftd_rate = '0.00'
        cpa_acc = 0
        total_commission_acc = 0.00
        cpa_value_acc = 0.00
        cpr_value_acc = 0.00
        rev_value_acc = 0.00
        check_tuple = (i.id, country, w_dynamic)
        dict_l = [api_new_player_obj, 
                    api_active_player_obj, api_player_tracker_obj,
                    api_date_deposit_player_obj, api_date_side_player_obj,
                    api_avg_active_players_obj, api_registrations, api_ftds, upload_tracker_obj]
        if check_tuple not in api_new_player_obj and check_tuple not in api_active_player_obj and\
            check_tuple not in api_player_tracker_obj and check_tuple not in api_date_deposit_player_obj\
            and check_tuple not in api_date_side_player_obj and check_tuple not in api_avg_active_players_obj\
            and check_tuple not in api_registrations and check_tuple not in api_ftds \
            and check_tuple not in upload_tracker_obj and check_tuple not in api_clicks:
            continue
        #if check_tuple in api_ftds:
        #    import pdb;pdb.set_trace()
        # currency = currency_dict[i.fk_campaign.siteid]
        content_type = 'POST_BACK'
        clicks = api_clicks[check_tuple]['clicks__sum'] if api_clicks.get(check_tuple) else 0
        
        if api_player_tracker_obj.get(check_tuple):
            revenue_c = api_player_tracker_obj[check_tuple]['revenue__sum']
            deposit = api_player_tracker_obj[check_tuple]['deposit__sum']
            cashout = api_player_tracker_obj[check_tuple]['cashout__sum']
            chargeback = api_player_tracker_obj[check_tuple]['chargeback__sum']
            void = api_player_tracker_obj[check_tuple]['void__sum']
            bonus = api_player_tracker_obj[check_tuple]['bonuses__sum']
        if api_new_player_obj.get(check_tuple) and api_new_side_player_obj.get(check_tuple):
            new_player_deposit = api_new_player_obj.get(check_tuple)['deposit__sum']
            avg_ltv = (api_new_player_obj[check_tuple]['revenue__sum']/api_new_side_player_obj[check_tuple]['sidegamesbets__sum'])
        if api_active_player_obj.get(check_tuple):
            active_player_count = api_active_player_obj.get(check_tuple)['playerid__count']
            # deposit_list = [f for f in date_player_obj.values('playerid').annotate(Sum('deposit')) if f['deposit__sum']>0.0]
            # side_bets_list = [k for k in date_player_obj.values('playerid').annotate(Sum('sidegamesbets')) if k['sidegamesbets__sum']>0.0]
        if api_date_deposit_player_obj.get(check_tuple):
            diposit_accounts = api_date_deposit_player_obj[check_tuple]['playerid__count']
        if api_date_side_player_obj.get(check_tuple):
            wagering_accounts = api_date_side_player_obj[check_tuple]['playerid__count']
            avg_net_cash = round(revenue/api_date_side_player_obj[check_tuple]['playerid__count'],2)
        if api_player_tracker_obj.get(check_tuple) and api_player_tracker_obj.get(check_tuple).get('sidegamesbets__sum'):
            total_wagers = round(api_player_tracker_obj[check_tuple]['sidegamesbets__sum'], 2)
        if api_player_tracker_obj.get(check_tuple) and api_player_tracker_obj.get(check_tuple).get('sidegameswins__sum'):
            sidegameswins = round(api_player_tracker_obj[check_tuple]['sidegameswins__sum'], 2)

        if api_avg_active_players_obj.get(check_tuple):
            avg_active_players = int(api_avg_active_players_obj[check_tuple]['playerid__count'])            
        if api_registrations.get(check_tuple):
            registrations = api_registrations[check_tuple]['playerid__count']
        if api_ftds.get(check_tuple):
            ftds = api_ftds[check_tuple]['playerid__count']
        if upload_tracker_obj.get(check_tuple):
            content_type = 'UPLOAD'
            if upload_tracker_obj[check_tuple].get('revenue__sum'):
                revenue_c = upload_tracker_obj[check_tuple]['revenue__sum']
            if upload_tracker_obj[check_tuple].get('deposit__sum'):
                deposit = upload_tracker_obj[check_tuple]['deposit__sum']
            if upload_tracker_obj[check_tuple].get('cashout__sum'):
                cashout = upload_tracker_obj[check_tuple]['cashout__sum']
            if upload_tracker_obj[check_tuple].get('chargeback__sum'):
                chargeback = upload_tracker_obj[check_tuple]['chargeback__sum']
            if upload_tracker_obj[check_tuple].get('void__sum'):
                void = upload_tracker_obj[check_tuple]['void__sum']
            if upload_tracker_obj[check_tuple].get('bonus__sum'):
                bonus = upload_tracker_obj[check_tuple]['bonus__sum']
            if upload_tracker_obj[check_tuple].get('registrations__sum'):
                registrations = upload_tracker_obj[check_tuple]['registrations__sum']
            if upload_tracker_obj[check_tuple].get('ftd__sum'):
                ftds = upload_tracker_obj[check_tuple]['ftd__sum']
            if upload_tracker_obj[check_tuple].get('clicks__sum'):
                clicks = clicks + upload_tracker_obj[check_tuple]['clicks__sum']
            if upload_tracker_obj[check_tuple].get('ftd__sum'):
                cpa_acc = upload_tracker_obj[check_tuple]['ftd__sum']
        #if revenue_c > 0:
        #    import pdb;pdb.set_trace()
        tracker = i.fk_campaign.name.replace('vcommsubaff', '')
        # deals if not commission and country:
        commission = None
        if country_ids_list.get(country):
            comm_check_tuple = (i.id, country_ids_list[country])
            commission = commission_dict.get(comm_check_tuple) if commission_dict.get(comm_check_tuple) else None
        if not commission:
            commission = commission_dict.get((i.id, None)) if commission_dict.get((i.id, None)) else None
        if commission:
            cpa_rev = 0.0
            if not commission['cpa'][1]:
                commission['cpa'][1] = 0
            # if commission['pocdeduction'] and i.fk_affiliateinfo.account_manager and i.fk_affiliateinfo.account_manager.id != 91:
            #     revenue_c = (revenue_c - (revenue_c*0.21))
            if commission['cpa'] and commission['cpa'][2]:
                res_list = []
                if commission['currency'] == 'gbp':
                    convert_cpa_check_gbp_euro = currency_obj.gbptoeur*commission['cpa'][1]
                    convert_cpa_value_gbp_euro = currency_obj.gbptoeur*commission['cpa'][2]
                elif commission['currency'] == 'euro':
                    convert_cpa_check_gbp_euro = commission['cpa'][1]
                    convert_cpa_value_gbp_euro = commission['cpa'][2]
                else:
                    convert_cpa_check_gbp_euro = commission['cpa'][1]
                    convert_cpa_value_gbp_euro = commission['cpa'][2]
                if commission['cpa'][0] == 'FTD':
                    if not ftd_dict_deposit.get(check_tuple):
                        api_ftd_player_obj = []
                    else:
                        if i.fk_campaign.siteid.id in currency_sites[u'\xa3']:
                            api_ftd_player_obj = [j for j in ftd_dict_deposit[check_tuple] if (currency_obj.gbptoeur*j['deposit']) >= convert_cpa_check_gbp_euro]
                        else:
                            api_ftd_player_obj = [j for j in ftd_dict_deposit[check_tuple] if j['deposit'] >= convert_cpa_check_gbp_euro]
                    cpa_acc = cpa_acc + len(api_ftd_player_obj)
                    for j_ in api_ftd_player_obj:
                        cpa_rev += convert_cpa_check_gbp_euro 
                    cpa_value_acc = (cpa_acc) * float(convert_cpa_value_gbp_euro)
                elif commission['cpa'][0] == 'Registration' and registrations:
                    cpa_value_acc = (registrations) * float(convert_cpa_value_gbp_euro)
            if commission['revshare'] and revenue_c:
                # Maxi affiliates tiered structure calculations.
                if commission['exclude_rev']:
                    comm_r = tiered_structure_calculation(from_date, commission['default'],
                        i, (revenue_c - cpa_rev), commission['revshare'], currency_obj, ftds)
                    rev_value_acc = ((revenue_c - cpa_rev)  * (comm_r))/100
                else:
                    comm_r = tiered_structure_calculation(from_date, commission['default'], 
                        i, (revenue_c), commission['revshare'], currency_obj, ftds)
                    rev_value_acc = (revenue_c * (comm_r))/100
            elif i.fk_affiliateinfo.account_manager.id in [91, 102]:
                comm_r = tiered_structure_calculation(from_date, True, 
                            i, (revenue_c), 0.0, currency_obj, ftds)
                rev_value_acc = (revenue_c * (comm_r))/100
        elif i.fk_affiliateinfo.account_manager.id in [91, 102]:
            comm_r = tiered_structure_calculation(from_date, True, 
                            i, (revenue_c), 0.0, currency_obj, ftds)
            rev_value_acc = (revenue_c * (comm_r))/100
        revenue = revenue_c
        total_commission_acc = cpa_value_acc + cpr_value_acc + rev_value_acc
        total_value = total_commission_acc
        cpa_value = cpa_value_acc
        rev_value = rev_value_acc
        data_r_obj = CampaignDateReport.objects.filter(fk_campaign=i,
                            date=from_date.date(), country=country, wdynamic=w_dynamic,
                            siteid=i.fk_campaign.siteid,
                            fk_affiliateinfo=i.fk_affiliateinfo)
        data_report_obj, created = CampaignDateReport.objects.get_or_create(fk_campaign=i,
                            date=from_date.date(), country=country, wdynamic=w_dynamic,
                            siteid=i.fk_campaign.siteid,
                            fk_affiliateinfo=i.fk_affiliateinfo)
        data_report_obj.registrations = registrations
        data_report_obj.clicks = clicks
        data_report_obj.ftd = ftds
        data_report_obj.deposit = deposit
        data_report_obj.cashout = cashout
        data_report_obj.bonuses = bonus
        data_report_obj.revenue = revenue
        data_report_obj.chargeback = chargeback
        data_report_obj.void = void
        data_report_obj.sidegameswins = sidegameswins
        data_report_obj.newplayersdeposit = new_player_deposit
        data_report_obj.totalwagers = total_wagers
        data_report_obj.newplayers = new_player_count
        data_report_obj.activeplayers = active_player_count
        data_report_obj.depositingaccounts = diposit_accounts
        data_report_obj.wageringaccounts = wagering_accounts
        data_report_obj.avgactivedays = avg_active_players
        data_report_obj.avgplayerltv = avg_ltv
        data_report_obj.netperplayer = avg_net_cash
        data_report_obj.cpacommission = cpa_value
        data_report_obj.revsharecommission = rev_value
        data_report_obj.totalcommission = total_value
        data_report_obj.save()   
    return True


def tiered_structure_calculation(from_date, default, i, revenue, revshare_c, currency_obj, ftds):
    tiered_objs = TieredStructure.objects.filter(Q(siteid=i.fk_campaign.siteid),
                                Q(account_manager=i.fk_affiliateinfo.account_manager),
                                Q(start_date__lte=from_date)|Q(start_date__isnull=True),
                                Q(end_date__gte=from_date)|Q(end_date__isnull=True))
    #import pdb;pdb.set_trace()
    if tiered_objs:
        #import pdb;pdb.set_trace()
        tiered_obj = tiered_objs[0]
        if from_date > i.fk_affiliateinfo.registeredon and default:
            tiered_dict = tiered_obj.options
            for t, r in tiered_dict.iteritems():
                if tiered_obj.stype != 'Revenue':
                    if int(r[0]) <= ftds <= int(r[1]):
                        revshare_c = float(t)
                        break
                else:
                    if float(r[0]) <= currency_obj.eurtogbp*revenue <= float(r[1])+0.99:
                        revshare_c = float(t)
                        break
    if i.fk_affiliateinfo.account_manager.id == 91:
        if from_date > i.fk_affiliateinfo.registeredon + timedelta(days=30) and default:
            tiered_dict = {25: [-float('inf'), 499], 30: [500, 999.99], 
                35: [1000, 2499.99], 40: [2500, 4999.99], 
                45: [5000, 7499.99], 50: [7500, float('inf')]}
            for t, r in tiered_dict.iteritems():
                if r[0] <= currency_obj.eurtogbp*revenue <= r[1]:
                    revshare_c = t
                    break
    elif i.fk_affiliateinfo.account_manager.id == 102:
        if from_date > i.fk_affiliateinfo.registeredon and default:
            tiered_dict = {25: [0, 5], 35: [6, 20], 
                40: [21, 50], 50: [50, float('inf')]}
            for t, r in tiered_dict.iteritems():
                if r[0] <= ftds <= r[1]:
                    revshare_c = t
                    break
    return revshare_c

def db_process_hits(obj, camp_links, from_date, to_date, site_ids, currencyfilter, country=None):
    data_list = []
    currency_sites = {}
    country_list = []
    if country and type(country) == str:
        country_list = [country]
        country_id_list = '('+','.join(country)+')'
    else:
        # country_list = cache.get('country_list')
        if not country_list:
            country_list = [i.countryname for i in Country.objects.all()]
            # cache.set('country_list', country_list, cache_time)
    currency_objs = Currency.objects.filter(siteid__in=site_ids)
    for cur in currency_objs:
        if cur.currency in currency_sites:
            currency_sites[cur.currency].append(int(cur.siteid.id))
        else:
            currency_sites[cur.currency] = [int(cur.siteid.id)]
    if not currency_sites.get(u'\xa3'):
        currency_sites[u'\xa3'] = []
    if not currency_sites.get(u'\u20ac'):
        currency_sites[u'\u20ac'] = []
    currency_dict = get_currency_dict(site_ids)
    commission_dict = get_commission_dict(obj, camp_links, [obj], site_ids)
    i = [int(j.id) for j in camp_links]
    tup_i = '(-1)'
    if i:
        tup_i = '('+str(functools.reduce(lambda a,b : str(a)+','+str(b),i))+')'
    upload_tracker_obj = {}
    upload_tracker_obj = UploadAdvanced.objects.filter(fk_campaign__in=camp_links,
                    date__gte=str(from_date).split(' ')[0],
                    date__lte=str(to_date).split(' ')[0],
                    siteid__in=site_ids,
                    processed=True).values(
                        'fk_campaign').annotate(Sum('ftd'),
                    Sum('registrations'), Sum('clicks'))
    upload_tracker_obj = {i['fk_campaign']:i for i in upload_tracker_obj}
    upload_player_tracker_obj = {}
    upload_new_player_obj = {}
    upload_active_player_obj = {}
    upload_date_player_obj = {}
    upload_ftd_player_obj = {}
    upload_avg_active_players_obj = {}
    # tracker_obj = get_cliks_data([], from_date, to_date, site_ids, [i])
    api_clicks = CampaignClicksCount.objects.filter(fk_campaign__in=camp_links,
                            date__gte=str(from_date).split(' ')[0],
                            date__lte=str(to_date).split(' ')[0],
                            fk_campaign__fk_campaign__siteid__in=site_ids).values(
                            'fk_campaign').annotate(Sum('clicks'))
    api_clicks = {i['fk_campaign']:i for i in api_clicks}
    api_registrations = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links, 
                        registrationdate__gte=str(from_date),
                        registrationdate__lte=str(to_date),
                        country__in=country_list,
                        siteid__in=site_ids).values('fk_campaign').annotate(Count('playerid', 
                                 distinct=True))
    api_registrations = {i['fk_campaign']:i for i in api_registrations}
    api_ftds = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links, 
                        ftddate__gte=str(from_date),
                        ftddate__lte=str(to_date),
                        country__in=country_list,
                        siteid__in=site_ids,deposit__gt=0
                        ).values('fk_campaign').annotate(Count('playerid', 
                                 distinct=True))
    api_ftds = {i['fk_campaign']:i for i in api_ftds}
    api_player_tracker_obj = {}
    if currencyfilter == 'gbp':
        currency = u'\xa3'
    else:
        currency = u'\u20ac'
    if currency_sites[u'\u20ac']:
        euro_sites = '('+str(functools.reduce(lambda a,b : str(a)+','+str(b),currency_sites[u'\u20ac']))+')'
        euro_sql_query = """
                    select p.date,p.fk_campaign_id, p.siteid_id,
                    sum(p.registrations), sum(p.ftd),
                    sum(p.revenue),sum(p.revenue*c.eurtogbp),
                    sum(p.deposit),sum(p.deposit*c.eurtogbp),
                    sum(p.chargeback),sum(p.chargeback*c.eurtogbp),
                    sum(p.cashout),sum(p.cashout*c.eurtogbp),
                    sum(p.void),sum(p.void*c.eurtogbp),
                    sum(p.bonuses),sum(p.bonuses*c.eurtogbp),
                    sum(p.reversechargeback),sum(p.reversechargeback*c.eurtogbp),
                    sum(p.sidegamesbets),sum(p.sidegamesbets*c.eurtogbp),
                    sum(p.sidegameswins),sum(p.sidegameswins*c.eurtogbp) 
                    from connectors_ppplayerprocessed p join userapp_currencyconverted c on c.date=CAST(p.date AS DATE)
                     group by p.fk_campaign_id, p.date having p.date >= '%s' and p.date < '%s' and
                     p.siteid_id IN %s and p.fk_campaign_id IN %s 
                """%(str(from_date).split(' ')[0],
                    str(to_date).split(' ')[0], euro_sites, 
                    tup_i)
        if country:
            euro_sql_query += euro_sql_query + 'and p.country IN %s'%(country_id_list)
        euro_sql_query += ';'
        euro_camp_list = []
        cursor = connection.cursor()
        euro_set = cursor.execute(euro_sql_query)
        col_names = [desc[0] for desc in cursor.description]
        while True:
            row = cursor.fetchone()
            if row is None:
                break
            row_dict = dict(zip(col_names, row))
            euro_camp_list.append(row_dict)
        for i in euro_camp_list:
            if currencyfilter == 'gbp':
                currency = u'\xa3'
                dict_i = {(j.replace('sum(p.','').replace(')','__sum__togbp') if '*' not in j else j.replace('sum(p.','').replace('*c.eurtogbp)','__sum')):i[j] for j in i}
            else:
                currency = u'\u20ac'
                dict_i = {(j.replace('sum(p.','').replace(')','__sum') if '*' not in j else j.replace('sum(p.','').replace('*c.eurtogbp)','__sum__togbp')):i[j] for j in i}
            if api_player_tracker_obj.get(dict_i['fk_campaign_id']):
                for k in dict_i:
                    if type(dict_i[k]) in [int, float]:
                        api_player_tracker_obj[dict_i['fk_campaign_id']][k] += dict_i[k]
            else:
                api_player_tracker_obj[dict_i['fk_campaign_id']] = dict_i

        euro_sql_query = """
                    select p.date,p.fk_campaign_id, p.siteid_id,
                    sum(p.revenue),sum(p.revenue*c.eurtogbp),
                    sum(p.deposit),sum(p.deposit*c.eurtogbp),
                    sum(p.chargeback),sum(p.chargeback*c.eurtogbp),
                    sum(p.cashout),sum(p.cashout*c.eurtogbp),
                    sum(p.void),sum(p.void*c.eurtogbp),
                    sum(p.bonus),sum(p.bonus*c.eurtogbp)
                     from connectors_uploadadvanced p join userapp_currencyconverted c on c.date=CAST(p.date AS DATE)
                     group by p.fk_campaign_id, p.date having p.date >= '%s' and p.date < '%s' and
                     p.siteid_id IN %s and p.fk_campaign_id IN %s;
                """%(str(from_date).split(' ')[0],
                    str(to_date).split(' ')[0], euro_sites, 
                    tup_i)
        euro_camp_list = []
        cursor = connection.cursor()
        euro_set = cursor.execute(euro_sql_query)
        col_names = [desc[0] for desc in cursor.description]
        while True:
            row = cursor.fetchone()
            if row is None:
                break
            row_dict = dict(zip(col_names, row))
            euro_camp_list.append(row_dict)
        for i in euro_camp_list:
            if currencyfilter == 'gbp':
                currency = u'\xa3'
                dict_i = {(j.replace('sum(p.','').replace(')','__sum__togbp') if '*' not in j else j.replace('sum(p.','').replace('*c.eurtogbp)','__sum')):i[j] for j in i}
            else:
                currency = u'\u20ac'
                dict_i = {(j.replace('sum(p.','').replace(')','__sum') if '*' not in j else j.replace('sum(p.','').replace('*c.eurtogbp)','__sum__togbp')):i[j] for j in i}
            if upload_tracker_obj.get(dict_i['fk_campaign_id']):
                for k in dict_i:
                    if type(dict_i[k]) not in [int, float]:
                        continue
                    if upload_tracker_obj[dict_i['fk_campaign_id']].get(k):
                        upload_tracker_obj[dict_i['fk_campaign_id']][k] += dict_i[k]
                    else:
                        upload_tracker_obj[dict_i['fk_campaign_id']][k] = dict_i[k]
            else:
                upload_tracker_obj[dict_i['fk_campaign_id']] = dict_i

    if currency_sites[u'\xa3']:
        gbp_sites = '('+str(functools.reduce(lambda a,b : str(a)+','+str(b),currency_sites[u'\xa3']))+')'
        gbp_sql_query = """
                    select p.date, p.fk_campaign_id, p.siteid_id,
                    sum(p.registrations), sum(p.ftd),
                    sum(p.revenue),sum(p.revenue*c.gbptoeur),
                    sum(p.deposit),sum(p.deposit*c.gbptoeur),
                    sum(p.chargeback),sum(p.chargeback*c.gbptoeur),
                    sum(p.cashout),sum(p.cashout*c.gbptoeur),
                    sum(p.void),sum(p.void*c.gbptoeur),
                    sum(p.bonuses),sum(p.bonuses*c.gbptoeur),
                    sum(p.reversechargeback),sum(p.reversechargeback*c.gbptoeur),
                    sum(p.sidegamesbets),sum(p.sidegamesbets*c.gbptoeur),
                    sum(p.sidegameswins),sum(p.sidegameswins*c.gbptoeur)  
                    from connectors_ppplayerprocessed p join userapp_currencyconverted c on c.date=CAST(p.date AS DATE)
                     group by p.fk_campaign_id , p.date having p.date >= '%s' and p.date< '%s' and
                     p.siteid_id IN %s and p.fk_campaign_id IN %s;
                    """%(str(from_date).split(' ')[0],
                        str(to_date).split(' ')[0], gbp_sites, 
                    tup_i)
        if country:
            gbp_sql_query += gbp_sql_query + 'and p.country IN %s'%(country_id_list)
        gbp_sql_query += ';'
        gbp_camp_list = []
        cursor = connection.cursor()
        gbp_set = cursor.execute(gbp_sql_query)
        col_names = [desc[0] for desc in cursor.description]
        while True:
            row = cursor.fetchone()
            if row is None:
                break
            row_dict = dict(zip(col_names, row))
            gbp_camp_list.append(row_dict)
        for i in gbp_camp_list:
            if currencyfilter == 'euro':
                dict_i = {(j.replace('sum(p.','').replace(')','__sum__toeuro') if '*' not in j else j.replace('sum(p.','').replace('*c.gbptoeur)','__sum')):i[j] for j in i}
            else:
                dict_i = {(j.replace('sum(p.','').replace(')','__sum') if '*' not in j else j.replace('sum(p.','').replace('*c.gbptoeur)','__sum__toeuro')):i[j] for j in i}
            if api_player_tracker_obj.get(dict_i['fk_campaign_id']):
                for k in dict_i:
                    if type(dict_i[k]) in [int, float]:
                        api_player_tracker_obj[dict_i['fk_campaign_id']][k] += dict_i[k]
            else:
                api_player_tracker_obj[dict_i['fk_campaign_id']] = dict_i

        gbp_sql_query = """
                    select p.date, p.fk_campaign_id, p.siteid_id,
                    sum(p.revenue),sum(p.revenue*c.gbptoeur),
                    sum(p.deposit),sum(p.deposit*c.gbptoeur),
                    sum(p.chargeback),sum(p.chargeback*c.gbptoeur),
                    sum(p.cashout),sum(p.cashout*c.gbptoeur),
                    sum(p.void),sum(p.void*c.gbptoeur),
                    sum(p.bonus),sum(p.bonus*c.gbptoeur)
                     from connectors_uploadadvanced p join userapp_currencyconverted c on c.date=CAST(p.date AS DATE)
                     group by p.fk_campaign_id , p.date having p.date >= '%s' and p.date< '%s' and
                     p.siteid_id IN %s and p.fk_campaign_id IN %s;
                    """%(str(from_date).split(' ')[0],
                        str(to_date).split(' ')[0], gbp_sites, 
                    tup_i)
        gbp_camp_list = []
        cursor = connection.cursor()
        gbp_set = cursor.execute(gbp_sql_query)
        col_names = [desc[0] for desc in cursor.description]
        while True:
            row = cursor.fetchone()
            if row is None:
                break
            row_dict = dict(zip(col_names, row))
            gbp_camp_list.append(row_dict)
        for i in gbp_camp_list:
            if currencyfilter == 'euro':
                dict_i = {(j.replace('sum(p.','').replace(')','__sum__toeuro') if '*' not in j else j.replace('sum(p.','').replace('*c.gbptoeur)','__sum')):i[j] for j in i}
            else:
                dict_i = {(j.replace('sum(p.','').replace(')','__sum') if '*' not in j else j.replace('sum(p.','').replace('*c.gbptoeur)','__sum__toeuro')):i[j] for j in i}
            if upload_tracker_obj.get(dict_i['fk_campaign_id']):
                for k in dict_i:
                    if type(dict_i[k]) in [int, float]:
                        if upload_tracker_obj[dict_i['fk_campaign_id']].get(k):
                            upload_tracker_obj[dict_i['fk_campaign_id']][k] += dict_i[k]
                        else:
                            upload_tracker_obj[dict_i['fk_campaign_id']][k] = dict_i[k]
            else:
                upload_tracker_obj[dict_i['fk_campaign_id']] = dict_i
    
    # api_player_tracker_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
    #                         date__gte=str(from_date).split(' ')[0],
    #                         date__lte=str(to_date).split(' ')[0],
    #                         siteid__in=site_ids).values('fk_campaign').annotate(Sum('deposit'),Sum('chargeback'),
    #                         Sum('cashout'),Sum('void'),Sum('reversechargeback'),
    #                         Sum('bonuses'),
    #                         Sum('sidegamesbets'),Sum('sidegameswins'),
    #                         Sum('jackpotcontribution'),Sum('revenue'))
    # api_player_tracker_obj = {i['fk_campaign']:i for i in api_player_tracker_obj}
    api_new_player_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links, 
                        registrationdate__gte=str(from_date),
                        registrationdate__lte=str(to_date),
                        country__in=country_list,
                        siteid__in=site_ids,deposit__gt=0).values(
                        'fk_campaign').annotate(Sum('deposit'), Sum('revenue'))
    api_new_player_obj = {i['fk_campaign']:i for i in api_new_player_obj}
    api_active_player_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
                        lastdate__gte=str(from_date),
                        lastdate__lte=str(to_date),
                        country__in=country_list,
                        siteid__in=site_ids).values('fk_campaign').annotate(Count('playerid', 
                                 distinct=True))
    api_active_player_obj = {i['fk_campaign']:i for i in api_active_player_obj}
    api_date_deposit_player_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
                        date__gte=str(from_date).split(' ')[0],
                        date__lte=str(to_date).split(' ')[0],
                        country__in=country_list,
                        siteid__in=site_ids,deposit__gt=0).values(
                        'fk_campaign').annotate(Count('playerid', 
                                 distinct=True))
    api_date_deposit_player_obj = {i['fk_campaign']:i for i in api_date_deposit_player_obj}
    api_date_side_player_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
                        date__gte=str(from_date).split(' ')[0],
                        date__lte=str(to_date).split(' ')[0],
                        country__in=country_list,
                        siteid__in=site_ids,sidegamesbets__gt=0).values(
                        'fk_campaign').annotate(Count('playerid', 
                                 distinct=True))
    api_date_side_player_obj = {i['fk_campaign']:i for i in api_date_side_player_obj}

    api_new_side_player_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links, 
                        registrationdate__gte=str(from_date),
                        registrationdate__lte=str(to_date),
                        country__in=country_list,
                        siteid__in=site_ids,sidegamesbets__gt=0).values(
                        'fk_campaign').annotate(Sum('sidegamesbets'))
    api_new_side_player_obj = {i['fk_campaign']:i for i in api_new_side_player_obj}

    
    api_avg_active_players_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
                        registrationdate__gte=str(from_date).split(' ')[0],
                        lastdate__lte=str(to_date).split(' ')[0],
                        country__in=country_list,
                        siteid__in=site_ids).values(
                        'fk_campaign').annotate(Count('playerid', 
                                 distinct=True))
    api_avg_active_players_obj = {i['fk_campaign']:i for i in api_avg_active_players_obj}
    ftd_deposit_list = PPPlayerProcessed.objects.filter(fk_campaign=camp_links,
                ftddate__gte=str(from_date).split(' ')[0],
                ftddate__lte=str(to_date),
                siteid__in=site_ids,
                ftddate = F('date')
                ).values('playerid', 'deposit', 'fk_campaign').distinct()
    ftd_dict_deposit = {}
    for d in ftd_deposit_list:
        if int(d['fk_campaign']) in ftd_dict_deposit:
            ftd_dict_deposit[int(d['fk_campaign'])].append(d)
        else:
            ftd_dict_deposit[int(d['fk_campaign'])] = [d]
    for i in camp_links:
        newplayer_list = []
        new_player_count = 0
        new_player_deposit = 0.00
        active_player_count= 0
        diposit_accounts = 0
        wagering_accounts = 0
        total_wagers = 0.00
        cpa_value = 0.00
        cpr_value = 0.00
        rev_value = 0.00
        total_value = 0.00
        days_count = to_date - from_date
        days_count = days_count.days
        avg_active_players_days = 0
        avg_active_players = 0
        avg_net_cash = 0.00
        avg_ltv = 0.00
        revenue = 0.0
        deposit = 0.0
        cashout = 0.0
        chargeback = 0.0
        void = 0.0
        bonus = 0.0
        registrations = 0
        ftds = 0
        cpa_acc = 0
        clicks = 0
        avg_ltv_list = []
        revenue_ltv = 0.0
        content_type = 'POST_BACK'
        clicks = api_clicks[i.id]['clicks__sum'] if api_clicks.get(i.id) else 0
        if api_player_tracker_obj.get(i.id):
            # cpa_accounts = new_player_obj.values('playerid').annotate(Sum('deposit'))
            # for c in cpa_accounts:
            #     newplayer_list.append(c)
            
            # if newplayer_list:
            #     new_player_count = len(newplayer_list)
            #     for j in newplayer_list:
            #         new_player_deposit = new_player_deposit + j['deposit__sum']
            # new_player_count = new_player_obj.count()
            if api_new_player_obj.get(i.id):
                new_player_deposit = api_new_player_obj.get(i.id)['deposit__sum']
                avg_ltv = (api_new_player_obj[i.id]['revenue__sum']/api_new_side_player_obj[i.id]['sidegamesbets__sum'])
            if api_active_player_obj.get(i.id):
                active_player_count = api_active_player_obj.get(i.id)['playerid__count']
            revenue = api_player_tracker_obj[i.id]['revenue__sum']
            deposit = api_player_tracker_obj[i.id]['deposit__sum']
            cashout = api_player_tracker_obj[i.id]['cashout__sum']
            chargeback = api_player_tracker_obj[i.id]['chargeback__sum']
            void = api_player_tracker_obj[i.id]['void__sum']
            bonus = api_player_tracker_obj[i.id]['bonuses__sum']
            # deposit_list = [f for f in date_player_obj.values('playerid').annotate(Sum('deposit')) if f['deposit__sum']>0.0]
            # side_bets_list = [k for k in date_player_obj.values('playerid').annotate(Sum('sidegamesbets')) if k['sidegamesbets__sum']>0.0]
            if api_date_deposit_player_obj.get(i.id):
                diposit_accounts = api_date_deposit_player_obj[i.id]['playerid__count']
            if api_date_side_player_obj.get(i.id):
                wagering_accounts = api_date_side_player_obj[i.id]['playerid__count']
                avg_net_cash = round(revenue/api_date_side_player_obj[i.id]['playerid__count'],2)
            if api_player_tracker_obj.get(i.id) and api_player_tracker_obj.get(i.id).get('sidegamesbets__sum'):
                total_wagers = round(api_player_tracker_obj[i.id]['sidegamesbets__sum'], 2)
            if api_avg_active_players_obj.get(i.id):
                avg_active_players = int(api_avg_active_players_obj[i.id]['playerid__count'])            
            if api_registrations.get(i.id):
                registrations = api_registrations[i.id]['playerid__count']
            if api_ftds.get(i.id):
                ftds = api_ftds[i.id]['playerid__count']
        if upload_tracker_obj.get(i.id):
            content_type = 'UPLOAD'
            if upload_tracker_obj[i.id].get('revenue__sum'):
                revenue_c = upload_tracker_obj[i.id]['revenue__sum']
            if upload_tracker_obj[i.id].get('deposit__sum'):
                deposit += upload_tracker_obj[i.id]['deposit__sum']
            if upload_tracker_obj[i.id].get('cashout__sum'):
                cashout += upload_tracker_obj[i.id]['cashout__sum']
            if upload_tracker_obj[i.id].get('chargeback__sum'):
                chargeback += upload_tracker_obj[i.id]['chargeback__sum']
            if upload_tracker_obj[i.id].get('void__sum'):
                void += upload_tracker_obj[i.id]['void__sum']
            if upload_tracker_obj[i.id].get('bonus__sum'):
                bonus += upload_tracker_obj[i.id]['bonus__sum']
            registrations = upload_tracker_obj[i.id]['registrations__sum']
            ftds = upload_tracker_obj[i.id]['ftd__sum']
            clicks = clicks + upload_tracker_obj[i.id]['clicks__sum']
            cpa_acc = ftds

        reg_rate = '0.00'
        ftd_rate = '0.00'
        if registrations and clicks:
            reg_rate = float(registrations)/float(clicks)
            reg_rate = round(reg_rate*100, 2)
            reg_rate = str(reg_rate)
        if registrations and ftds:
            ftd_rate = float(ftds)/float(registrations)
            ftd_rate = round(ftd_rate*100, 2)
            ftd_rate = str(ftd_rate)

        tracker = i.fk_campaign.name.replace('vcommsubaff', '')

        # deals
        commission = commission_dict.get(i.id) if commission_dict.get(i.id) else 0
        if commission:
            if not commission['cpa'][1]:
                commission['cpa'][1] = 0
            # if commission['pocdeduction']:
            #     revenue = (revenue - (revenue*0.21))
            if commission['cpa'] and commission['cpa'][0] == 'FTD' and commission['cpa'][2]:
                res_list = []
                if not ftd_dict_deposit.get(int(i.id)):
                    api_ftd_player_obj = []
                else:
                    api_ftd_player_obj = [j for j in ftd_dict_deposit[int(i.id)] if j['deposit'] > commission['cpa'][1]]
                # api_ftd_player_obj = PPPlayerProcessed.objects.filter(fk_campaign=i,
                #                     ftddate__gte=str(from_date).split(' ')[0],
                #                     ftddate__lte=str(to_date),
                #                     siteid__in=site_ids,deposit__gt=commission['cpa'][1],
                #                     ftddate = F('date')
                #                     ).values('playerid').distinct()
                cpa_acc = cpa_acc + len(api_ftd_player_obj)
                cpa_value = (cpa_acc) * float(commission['cpa'][2])
                cpa_value = round(cpa_value, 2)
            # elif commission['cpa'] and commission['cpa'][0] == 'FTD' and commission['cpa'][2] and content_type != 'UPLOAD':
            #     res_list = []
            #     api_ftd_player_obj = PPPlayerProcessed.objects.filter(fk_campaign=i,
            #                         date__gte=str(from_date).split(' ')[0],
            #                         date__lte=str(to_date).split(' ')[0],
            #                         siteid__in=site_ids,deposit__gt=commission['cpa'][1]
            #                         ).values('playerid').distinct()
            #     cpa_acc = len(api_ftd_player_obj)
            #     cpa_value = (cpa_acc) * float(commission['cpa'][2])
            #     cpa_value = round(cpa_value, 2)
            # if commission.referalchoice:
            #     cpr_value = (tracker_obj['cashout']) * (commission.referalcommissionvalue)
            if commission['revshare'] and revenue:
                rev_value = (revenue * (commission['revshare']))/100
                rev_value = round(rev_value, 2)
        total_value = cpa_value + cpr_value + rev_value
        total_value = round(total_value, 2)

        aff_data = [tracker,
                    0, clicks, 0,
                    registrations, reg_rate,
                    ftds, ftd_rate, new_player_count,
                    round(new_player_deposit, 2), active_player_count, diposit_accounts, wagering_accounts, total_wagers,
                    avg_active_players, avg_net_cash,
                    round(deposit, 2) if deposit else 0.00,
                    round(cashout, 2) if cashout else 0.00,
                    round(chargeback, 2) if chargeback else 0.00, 
                    round(void, 2) if void else 0.00,
                    round(bonus, 2) if bonus else 0.00,
                    round(revenue, 2) if revenue else 0.00, round(avg_ltv, 2) if avg_ltv else 0.00,
                    cpa_value, rev_value, total_value]
        # if len(total_list) == 2:
        #     total_list = total_list + aff_data[2:]
        # else:
        #     total_list = [x + y if not isinstance(x,str) else x for x, y in zip(total_list, aff_data)]
        aff_data = [j if ('.' not in str(j) or isinstance(j,str)) else (currency + str(j)) for j in aff_data]
        aff_data = [k if k != (currency + '0.0') else (currency + '0.00') for k in aff_data]
        data_list.append(aff_data)
    return data_list


def Process_campaign_stats(obj, from_date, to_date, camp_links, site_ids, group_d, currencyfilter):
    data_list = []
    currency = ''
    currency_dict = {}
    total_list = ['Total', '']
    header_list = [
           { "title": "Grouped by", "width":"200px"},
           # { "title": "Brand", "width":"200px"},
           # { "title": "Brand", "width":"200px"},
           # { "title": "Campaign ID"},
           # { "title": "Campaign name", "width":"200px"},
           { "title": "Impressions", "width":"140px"},
           { "title": "Clicks", "width":"120px"},
           { "title": "CTR", "width":"120px"},
           { "title": "Registrations", "width":"200px"},
           { "title": "Registration rate %", "width":"180px"},
           { "title": "FTD", "width":"120px"},
           { "title": "Conversion rate %", "width":"200px"},
           { "title": "New players", "width":"160px"},
           { "title": "New player Deposits", "width":"240px"},
           { "title": "Active players", "width":"200px"},
           { "title": "Depositing accounts", "width":"240px"},
           { "title": "Wagering accounts", "width":"230px"},
           { "title": "Total Wagers", "width":"180px"},
           { "title": "Avg active days", "width":"200px"},
           { "title": "Net per player", "width":"180px"},
           { "title": "Deposits", "width":"130px"},
           # { "title": "Cashout", "width":"130px"},
           { "title": "Chargeback", "width":"150px"},
           { "title": "Void", "width":"120px"},
           { "title": "Bonus", "width":"120px"},
           { "title": "Revenue", "width":"150px"},
           { "title": "Avg. Player LTV", "width":"200px"},
           { "title": "CPA Commission", "width":"200px"},
           { "title": "Revshare Commission", "width":"220px"},
           { "title": "Total Commission", "width":"180px"},
        ]
    data_list = db_process_hits(obj, camp_links, from_date, to_date, site_ids, currencyfilter)
    return (header_list, data_list, currency)

def group_by_earnings_report(obj, camp_links, from_date, to_date, site_ids, group_d, currencyfilter, country):
    data_list = []
    currency = ''
    currency_sites = {}
    currency_objs = Currency.objects.filter(siteid__in=site_ids)
    for cur in currency_objs:
        if cur.currency in currency_sites:
            currency_sites[cur.currency].append(int(cur.siteid.id))
        else:
            currency_sites[cur.currency] = [int(cur.siteid.id)]
    if not currency_sites.get(u'\xa3'):
        currency_sites[u'\xa3'] = []
    if not currency_sites.get(u'\u20ac'):
        currency_sites[u'\u20ac'] = []
    currency_dict = {}
    total_list = ['Total', '']
    header_list = [
           { "title": "Grouped by", "width":"200px"},
           # { "title": "Brand", "width":"200px"},
           # { "title": "Brand", "width":"200px"},
           # { "title": "Campaign ID"},
           # { "title": "Campaign name", "width":"200px"},
           { "title": "Impressions", "width":"140px"},
           { "title": "Clicks", "width":"120px"},
           { "title": "CTR", "width":"120px"},
           { "title": "Registrations", "width":"200px"},
           { "title": "Registration rate %", "width":"180px"},
           { "title": "FTD", "width":"120px"},
           { "title": "Conversion rate %", "width":"200px"},
           { "title": "New players", "width":"160px"},
           { "title": "Active players", "width":"200px"},
           { "title": "Depositing accounts", "width":"240px"},
           { "title": "Wagering accounts", "width":"230px"},
           { "title": "Total Wagers", "width":"180px"},
           { "title": "Avg active days", "width":"200px"},
           { "title": "Net per player", "width":"180px"},
           { "title": "Deposits", "width":"130px"},
           # { "title": "Cashout", "width":"130px"},
           { "title": "Chargeback", "width":"150px"},
           { "title": "Void", "width":"120px"},
           { "title": "Bonus", "width":"120px"},
           { "title": "Revenue", "width":"150px"},
           { "title": "Avg. Player LTV", "width":"200px"},
           { "title": "CPA Commission", "width":"200px"},
           { "title": "Revshare Commission", "width":"220px"},
           { "title": "Referral Commission", "width":"220px"},
           { "title": "Total Commission", "width":"180px"},
        ]
    group_list = []
    data_list, currency = group_by_results(from_date, to_date, site_ids, camp_links,
                     group_d, currencyfilter, 'earnings', country)
    return (header_list, data_list, currency)


def Process_player_stats(obj, from_date, to_date, camp_links, site_ids):
    data_list = []
    total_list = ['Total', '']
    player_id_list = []
    currency = ''
    currency_dict = {}
    header_list = [
           { "title": "Brand", "width":"200px"},
           # { "title": "Campaign ID"},
           { "title": "Player Id", "width":"180px"},
           { "title": "Dynamic", "width":"180px"},
           { "title": "Campaign Name", "width":"180px"},
           { "title": "Registration Date", "width":"200px"},
           { "title": "FTD Date", "width":"160px"},
           { "title": "Total Deposits", "width":"180px"},
           { "title": "Charge Backs", "width":"180px"},
           { "title": "First Played", "width":"180px"},
           { "title": "Last Played", "width":"180px"},
           { "title": "Bets", "width":"120px"},
           { "title": "Wins", "width":"120px"},
           { "title": "Bonus", "width":"120px"},
           { "title": "Revenue", "width":"130px"},
           { "title": "Adjustments", "width":"150px"},
           # { "title": "Bingo Revenue"},
           # { "title": "Games Revenue"},
           { "title": "CPA Commission", "width":"180px"},
           { "title": "Revshare Commission", "width":"220px"},
           { "title": "Total Commission", "width":"200px"},
           ]
    currency = ''
    currency_dict = get_currency_dict(site_ids)
    newplayer_list = []

    post_active_player_objs = PostBackPlayer.objects.filter(fk_campaign__in=camp_links,
                    lastdate__gte=str(from_date),
                    lastdate__lte=str(to_date),
                    siteid__in=site_ids).select_related(
                    'fk_campaign', 'fk_campaign__fk_campaign', 'siteid').order_by('-date')

    api_active_player_objs = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
                    date__gte=str(from_date).split(' ')[0],
                    date__lte=str(to_date).split(' ')[0],
                    siteid__in=site_ids).select_related('fk_campaign', 
                    'fk_campaign__fk_affiliateinfo',
                    'fk_campaign__fk_campaign'
                    ,'siteid').order_by('-date')
    players_dict = {}
    deposit_list = [f['playerid'] for f in api_active_player_objs.values('playerid').annotate(Sum('deposit')) if f['deposit__sum']>0.0]
    for active_player_obj in api_active_player_objs:
        if active_player_obj.playerid not in player_id_list:
            players_dict[active_player_obj.playerid] = {
                                'brand': active_player_obj.siteid.name,
                                'siteid': active_player_obj.siteid,
                                'camp_id': active_player_obj.fk_campaign.id,
                                'dynamic': active_player_obj.wdynamic,
                                'country': active_player_obj.country,
                                'camp_name': active_player_obj.fk_campaign.fk_campaign.name,
                                'registrationdate': active_player_obj.registrationdate.strftime("%d-%m-%Y") if active_player_obj.registrationdate else '',
                                'ftddate':active_player_obj.ftddate.strftime("%d-%m-%Y") if active_player_obj.ftddate else '',
                                'deposit': active_player_obj.deposit,
                                'chargeback': active_player_obj.chargeback,
                                'firstplayed':active_player_obj.ftddate.strftime("%d-%m-%Y") if active_player_obj.ftddate else '',
                                'lastdate': active_player_obj.lastdate.strftime("%d-%m-%Y") if active_player_obj.lastdate else '',
                                'bets':active_player_obj.sidegamesbets,
                                'wins': active_player_obj.sidegameswins,
                                'bonus':active_player_obj.bonuses,
                                'revenue': active_player_obj.revenue,
                                }
        else:
            players_dict[active_player_obj.playerid]['deposit'] += active_player_obj.deposit
            players_dict[active_player_obj.playerid]['chargeback'] += active_player_obj.chargeback
            players_dict[active_player_obj.playerid]['bets'] += active_player_obj.sidegamesbets
            players_dict[active_player_obj.playerid]['wins'] += active_player_obj.sidegameswins
            players_dict[active_player_obj.playerid]['bonus'] += active_player_obj.bonuses
            players_dict[active_player_obj.playerid]['revenue'] += active_player_obj.revenue
        player_id_list.append(active_player_obj.playerid)
    for pid, values_obj in players_dict.iteritems():
        currency = currency_dict[values_obj['siteid']]
        aff_data = [values_obj['brand'], pid,
                values_obj['dynamic'],
                values_obj['camp_name'],
                values_obj['registrationdate'],
                values_obj['ftddate'],
                (currency + str(round(values_obj['deposit'],2)) if '-' not in str(values_obj['deposit']) else '-'+currency+str(round(values_obj['deposit'],2)).replace('-','')) if values_obj['deposit'] else currency+'0.00', 
                (currency + str(round(values_obj['chargeback'],2)) if '-' not in str(values_obj['chargeback']) else '-'+currency+str(round(values_obj['chargeback'],2)).replace('-','')) if values_obj['chargeback'] else currency+'0.00',
                values_obj['firstplayed'], values_obj['lastdate'],
                (currency + str(round(values_obj['bets'],2)) if '-' not in str(values_obj['bets']) else '-'+currency+str(round(values_obj['bets'],2)).replace('-','')) if values_obj['bets'] else currency+'0.00',
                (currency + str(round(values_obj['wins'],2)) if '-' not in str(values_obj['wins']) else '-'+currency+str(round(values_obj['wins'],2)).replace('-','')) if values_obj['wins'] else currency+'0.00',
                (currency + str(round(values_obj['bonus'],2)) if '-' not in str(values_obj['bonus']) else '-'+currency+str(round(values_obj['bonus'],2)).replace('-','')) if values_obj['bonus'] else currency+'0.00',
                (currency + str(round(values_obj['revenue'],2)) if '-' not in str(values_obj['revenue']) else '-'+currency+str(round(values_obj['revenue'],2)).replace('-','')) if values_obj['revenue'] else currency+'0.00',
                currency +'0.00', currency +'0.00', currency +'0.00', currency +'0.00']
        # if len(total_list) == 2:
        #     total_list = total_list + aff_data[2:]
        # else:
        #     total_list = [x + y if (not isinstance(x,str) and not isinstance(x,datetime.datetime)) else x for x, y in zip(total_list, aff_data)]
        # aff_data = [j if ('.' not in str(j) or isinstance(j,str) and not isinstance(j,datetime)) else (currency + str(j)) for j in aff_data]
        # aff_data = [k if k != (currency + '0.0') else (currency + '0.00') for k in aff_data]
        data_list.append(aff_data)
    # total_list = get_total(data_list)
    # if len(data_list) > 0:
    #     total_list = [j if '.' not in str(j) else (currency + str(j)) for j in total_list]
    #     total_list[6] =  round(sum([float(_i[6]) for _i in data_list])/len(data_list), 2)
    #     total_list[8] = round(sum([float(_i[8]) for _i in data_list])/len(data_list), 2)
    #     #total_list = [k if k != (currency + '0.0') else (currency + '0.00') for k in total_list]
    #     data_list.append(total_list)
    # data_list = [[j if '.' not in str(j) else (currency + str(j)) for j in i] for i in data_list]
    return (header_list, data_list, currency)
    

def Process_game_list(obj):
    data_list = []
    header_list = [
           { "title": "Game Name", "width":"170px"},
           { "title": "Game type", "width":"200px"},
           { "title": "Bonus code", "width":"120px"},
           { "title": "Player", "width":"220px"},
           { "title": "Brand", "width":"220px"},
           { "title": "Affiliate", "width":"220px"},
           { "title": "Campaign", "width":"220px"},
           { "title": "Result", "width":"220px"}
           ]
    return (header_list, data_list)



def device_report_stats(obj, from_date, to_date, camp_links, device, site_ids, country=None):
    data_list = []
    total_list = ['Total', '']
    player_id_list = []
    currency = ''
    currency_dict = {}
    header_list = [
           { "title": "Brand", "width":"200px"},
           { "title": "Device", "width":"120px"},
           { "title": "Player Id", "width":"150px"},
           { "title": "Dynamic", "width":"150px"},
           { "title": "Registration Date", "width":"220px"},
           { "title": "FTD Date", "width":"150px"},
           { "title": "Total Deposits", "width":"210px"},
           { "title": "Charge Backs", "width":"200px"},
           { "title": "Revenue", "width":"130px"},
           { "title": "CPA Commission", "width":"180px"},
           { "title": "Revshare Commission", "width":"200px"},
           { "title": "Total Commission", "width":"180px"},
           ]
    currency = ''
    if not country:
        # country_list = cache.get('country_list')
        country = [i.countryname for i in Country.objects.all()]
    currency_dict = get_currency_dict(site_ids)
    newplayer_list = []
    post_active_player_objs = PostBackPlayer.objects.filter(fk_campaign__in=camp_links,
                    lastdate__gte=str(from_date),
                    lastdate__lte=str(to_date),
                    siteid__in=site_ids).select_related(
                    'fk_campaign', 'fk_campaign__fk_campaign', 'siteid').order_by('-date')
    # api_new_player_obj = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
    #                     registrationdate__gte=str(from_date),
    #                     registrationdate__lte=str(to_date),
    #                     siteid__in=site_ids)
    if device == 'All':
        api_active_player_objs = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
                    lastdate__gte=str(from_date),
                    lastdate__lte=str(to_date),
                    country__in=country,
                    siteid__in=site_ids).exclude(cashout=0, deposit=0).select_related('fk_campaign', 'fk_campaign__fk_campaign',
                    'siteid').order_by('-date')
    else:
        api_active_player_objs = PPPlayerProcessed.objects.filter(fk_campaign__in=camp_links,
                    lastdate__gte=str(from_date),
                    lastdate__lte=str(to_date),
                    country__in=country,
                    platform=device,
                    siteid__in=site_ids).exclude(cashout=0, deposit=0).select_related('fk_campaign', 'fk_campaign__fk_campaign',
                    'siteid').order_by('-date')
    players_dict = {}
    deposit_list = [f['playerid'] for f in api_active_player_objs.values('playerid').annotate(Sum('deposit')) if f['deposit__sum']>0.0]
    for active_player_obj in api_active_player_objs:
        if active_player_obj.playerid not in player_id_list:
            players_dict[active_player_obj.playerid] = {
                                'brand': active_player_obj.siteid.name,
                                'siteid': active_player_obj.siteid,
                                'camp_id': active_player_obj.fk_campaign.id,
                                'dynamic': active_player_obj.wdynamic,
                                'country': active_player_obj.country,
                                'camp_name': active_player_obj.fk_campaign.fk_campaign.name,
                                'registrationdate': active_player_obj.registrationdate.strftime("%d-%m-%Y") if active_player_obj.registrationdate else '',
                                'ftddate':active_player_obj.ftddate.strftime("%d-%m-%Y") if active_player_obj.ftddate else '',
                                'deposit': active_player_obj.deposit,
                                'cashout': active_player_obj.cashout,
                                'chargeback': active_player_obj.chargeback,
                                'firstplayed':active_player_obj.ftddate.strftime("%d-%m-%Y") if active_player_obj.ftddate else '',
                                'lastdate': active_player_obj.lastdate.strftime("%d-%m-%Y") if active_player_obj.lastdate else '',
                                'bets':active_player_obj.sidegamesbets,
                                'wins': active_player_obj.sidegameswins,
                                'bonus':active_player_obj.bonuses,
                                'revenue': active_player_obj.revenue,
                                'device': active_player_obj.platform
                                }
        else:
            players_dict[active_player_obj.playerid]['deposit'] += active_player_obj.deposit
            players_dict[active_player_obj.playerid]['chargeback'] += active_player_obj.chargeback
            players_dict[active_player_obj.playerid]['cashout'] += active_player_obj.cashout
            players_dict[active_player_obj.playerid]['bets'] += active_player_obj.sidegamesbets
            players_dict[active_player_obj.playerid]['wins'] += active_player_obj.sidegameswins
            players_dict[active_player_obj.playerid]['bonus'] += active_player_obj.bonuses
            players_dict[active_player_obj.playerid]['revenue'] += active_player_obj.revenue
        player_id_list.append(active_player_obj.playerid)
    for pid, values_obj in players_dict.iteritems():
        currency = currency_dict[values_obj['siteid']]
        aff_data = [values_obj['brand'], values_obj['device'], pid,
                values_obj['dynamic'],
                values_obj['registrationdate'],
                values_obj['ftddate'],
                (currency + str(round(values_obj['deposit'],2)) if '-' not in str(values_obj['deposit']) else '-'+currency+str(round(values_obj['deposit'],2)).replace('-','')) if values_obj['deposit'] else currency+'0.00', 
                (currency + str(round(values_obj['chargeback'],2)) if '-' not in str(values_obj['chargeback']) else '-'+currency+str(round(values_obj['chargeback'],2)).replace('-','')) if values_obj['chargeback'] else currency+'0.00',
                (currency + str(round(values_obj['revenue'],2)) if '-' not in str(values_obj['revenue']) else '-'+currency+str(round(values_obj['revenue'],2)).replace('-','')) if values_obj['revenue'] else currency+'0.00',
                currency +'0.00', currency +'0.00', currency +'0.00']
        # if len(total_list) == 2:
        #     total_list = total_list + aff_data[2:]
        # else:
        #     total_list = [x + y if (not isinstance(x,str) and not isinstance(x,datetime.datetime)) else x for x, y in zip(total_list, aff_data)]
        # aff_data = [j if ('.' not in str(j) or isinstance(j,str) and not isinstance(j,datetime)) else (currency + str(j)) for j in aff_data]
        # aff_data = [k if k != (currency + '0.0') else (currency + '0.00') for k in aff_data]
        data_list.append(aff_data)
    return (header_list, data_list, currency)

