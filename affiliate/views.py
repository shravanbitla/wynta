"""Create your views here."""
import random
import csv
import sys
import json
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.views.generic.base import TemplateView
from django.template import RequestContext 
from django.core.urlresolvers import reverse
from django.template import Template, Context
# # # # # # # # # # from django.contrib.sites.models import get_current_site
from django.contrib.admin.views.decorators import staff_member_required
from django.utils.decorators import method_decorator
from django.db.models import Sum, Count
from django.db import connection
from django.contrib.sites.models import Site
from django.core.mail import EmailMessage
from django.shortcuts import render
from affiliate.utils import *
from affiliate.dashboard_data import *
from userapp.models import *
from affiliate.models import *
from userapp.function import *
from .forms import *
from django.db.models import Q
from userapp.mails import *
from userapp.utils import get_network_sites_list, error_log, get_site_player_first_register_timestamp
from datetime import date, datetime, time
import calendar
from userapp.utils import get_fromdate_todate_for_django_query_filter, get_hexdigest,\
    get_site_player_first_register_timestamp
from userapp.utils import monthdelta, MyEncoder,get_paginate_dimensions, get_chips_type
from affiliate.utils import enc_password
from copy import deepcopy
from affiliate.dashboard_data import get_affiliate_campaign_link
#from userapp.userapp_db_entry import get_channels_list
from django.views.decorators.csrf import csrf_exempt

from middleware import SqlProfilingMiddleware

from connectors.models import *

def profiling(request):
    return render_to_response("sqlqueries_profiling.html", {"queries": SqlProfilingMiddleware.Queries})



def custom_page_not_found_view(request):
    context_dict = {}
    try:
        ap_obj = AffiliateProgramme.objects.filter(Q(domain__icontains=str(request.META['HTTP_HOST'])) | Q(
                                    wynta_domain__icontains=str(request.META['HTTP_HOST'])))
        if ap_obj:
            ap_obj = ap_obj[0]
        else:
            if 'fozil' in request.META['HTTP_HOST']:
                ap_obj = AffiliateProgramme.objects.all()[0]
            else:
                ap_obj = AffiliateProgramme.objects.all()[1]
        context_dict['partner'] = ap_obj.name
        context_dict['partner_obj'] = ap_obj
    except:
        obj = None
    template_name = "404.html"
    
    # import django
    # csrf_token_new = django.middleware.csrf.get_token(request)
    # print "CSRFFFF:::404", csrf_token_new
    # resp["csrf_token_new"] = csrf_token_new
    return render(request, template_name, context_dict)

def custom_error_view(request):
    context_dict = {}
    try:
        ap_obj = AffiliateProgramme.objects.filter(Q(domain__icontains=str(request.META['HTTP_HOST'])) | Q(
                                    wynta_domain__icontains=str(request.META['HTTP_HOST'])))
        if ap_obj:
            ap_obj = ap_obj[0]
        else:
            if 'fozil' in request.META['HTTP_HOST']:
                ap_obj = AffiliateProgramme.objects.all()[0]
            else:
                ap_obj = AffiliateProgramme.objects.all()[1]
        context_dict['partner'] = ap_obj.name
        context_dict['partner_obj'] = ap_obj
    except:
        obj = None
    template_name = "500.html"
    # if request.is_mobile:
    #     template_name = "mobile_templates/500.html"
    # import django
    # csrf_token_new = django.middleware.csrf.get_token(request)
    # print "CSRFFFF:::500", csrf_token_new
    # resp["csrf_token_new"] = csrf_token_new
    return render(request, template_name, context_dict)


def get_brand_order(obj):
    """
    Returns query set in order according to site order table
    """
    acc_manager = obj.account_manager
    queryset = acc_manager.siteid.filter(
        siteorder__accountmanager_id=acc_manager
        ).order_by('siteorder__order_id')
    queryset = queryset & obj.siteid.all()
    return queryset

def get_site_ids_data_list(siteids, filter_siteids):
    all_siteids = Site.objects.filter(id__in=siteids)
    siteid_list = []
    for i in all_siteids:
        temp = {}
        if (filter_siteids and int(i.id) in filter_siteids) or not filter_siteids:
            temp['selected'] = True
        else:
            temp['selected'] = False
        temp['id'] = int(i.id)
        temp['name'] = str(i.name)
        siteid_list.append(temp)
    return siteid_list


def delete_affiliate_session(request):
    if hasattr(request, 'session'):
        if 'partnerid' in request.session.keys():
            del request.session['affiliate_session_id']
        else:
            for key in request.session.keys():
                del request.session[key]

def logout(request):
    """ Logout affiliate from website """
    try:
        delete_affiliate_session(request)
    except KeyError:
        pass
    return HttpResponseRedirect(reverse("affiliate_login"))

def get_affiliate_session_id(request):
    affiliate_id = None
    if hasattr(request, 'session'):
        affiliate_id = request.session.get('affiliate_session_id')
    return affiliate_id


class EditProfile(TemplateView):
    """ class edit profile """
    def get(self, request):
        context_dict = {}
        sessionid = get_affiliate_session_id(request)
        affid = sessionid
        if not affid:
            return HttpResponseRedirect(reverse("affiliate_login"))

        obj = Affiliateinfo.objects.select_related('account_manager').get(id=affid)
        request.user.username = obj.username
        request.user.email = obj.email
        # aff_com_list = Affiliatecommission.objects.filter( fk_affiliateinfo__in=[affid])
        # aff_com_dict = dict( (i.fk_affiliateinfo_id, i)  for i in aff_com_list)
        data_list = []
        context_dict["aff_obj"] = obj
        if obj.account_manager:
            context_dict["partner"] = obj.account_manager.affiliateprogramme.name
            context_dict["partner_obj"] = obj.account_manager.affiliateprogramme
        context_dict['country_list'] = Country.objects.all()
        context_dict['site_urls_list'] =[[i.id, i.info] for i in AffiliateSite.objects.filter(fk_affiliateinfo=obj)]
        # context_dict["commission_obj"] = aff_com_dict.get(obj.id)
        return render(request, 'affiliates/affiliate-edit-profile-1.html', context_dict)
        # return render_to_response('affiliates/affiliate-edit-profile-1.html',
        #                           context_dict,
        #                           context_instance=RequestContext(request))

    def post(self, request):
        context_dict = {}
        sessionid = get_affiliate_session_id(request)
        affid = sessionid
        if not affid:
            return HttpResponse("affid is manadatory parameter.")
        try:
            obj = Affiliateinfo.objects.select_related('account_manager').get(id=affid)
            context_dict['country_list'] = Country.objects.all()
            username = request.POST.get("username")
            password = request.POST.get("password")
            confirmpassword = request.POST.get("confirmpassword")
            contactname = request.POST.get("contactname")
            companyname = request.POST.get("companyname")
            email = request.POST.get("email")
            address = request.POST.get("address")
            city = request.POST.get("city")
            pincode = request.POST.get("pincode")
            refid = request.POST.get("refid")
            telephone = request.POST.get("telephone", 0)
            state = request.POST.get("state")
            skype = request.POST.get("skype")
            countryname = str(request.POST.get("countryname"))
            changes = []
            if password and password !='' and 'sha1$' not in password:
                password = enc_password(password)
            else:
                password = obj.password
            site_urls = request.POST.getlist('site_url')
            if site_urls:
                for url_d in site_urls:
                    if not url_d:
                        continue
                    aff_site, created = AffiliateSite.objects.get_or_create(fk_affiliateinfo=obj,
                                                info = url_d)
                    changes.append('URLs')
                    aff_site.save()
            AffiliateSite.objects.filter(fk_affiliateinfo=obj).exclude(info__in=site_urls).delete()
            if obj.email != email:
                mailidcount = Affiliateinfo.objects.filter(email=email, 
                    account_manager=obj.account_manager).count()
                if mailidcount > 0:
                    context_dict["aff_obj"] = obj
                    context_dict["error_message"] = 'Email is already \
                    registered with '+ obj.account_manager.affiliateprogramme.name
                    if obj.account_manager:
                        context_dict["partner"] = obj.account_manager.affiliateprogramme.name
                        context_dict["partner_obj"] = obj.account_manager.affiliateprogramme
                    return render(request, 'affiliates/affiliate-edit-profile-1.html', context_dict)
            obj.contactname = contactname if contactname else username
            if obj.companyname != companyname:
                changes.append('Company Name')
            obj.companyname = companyname
            if obj.password != password:
                changes.append('Password')
            obj.password = password
            if obj.email != email:
                changes.append('Email')
            obj.email = email
            if obj.address != address:
                changes.append('Address')
            obj.address = address
            if obj.postcode != pincode:
                changes.append('Postcode')
            obj.postcode = pincode
            if obj.telephone != int(telephone):
                changes.append('Phone Number')
            obj.telephone = int(telephone)
            if obj.city != city:
                changes.append('City')
            obj.city = city
            if obj.state != state:
                changes.append('State')
            obj.state = state
            if obj.country != countryname:
                changes.append('Country')
            obj.country=countryname
            if obj.skype != skype:
                changes.append('Skype')
            obj.skype = skype
            obj.save()
            if changes:
                fields = ', '.join(changes)
            subject = "Change of {0} - {1} ({2})".format(fields, email, obj.account_manager.affiliateprogramme.name)
            content = """Dear {0},<br><br>
                        
                        Your affiliate {1} - {2} - on {3} has successfully updated their {4}.<br><br> 
                        
                        Thanks,<br>
                        Team @ Wynta """.format(obj.account_manager.user.username, obj.id, obj.email, obj.account_manager.affiliateprogramme.name, fields)
            check_EmailClient(obj.account_manager, None, subject, content, """%s""" % 'support@wynta.com', [], [obj.account_manager.user.email], None, obj,None,None, None)
            request.user.username = obj.username
            request.user.email = obj.email
            context_dict["aff_obj"] = obj
            context_dict['site_urls_list'] =[[i.id, i.info] for i in AffiliateSite.objects.filter(fk_affiliateinfo=obj)]
            context_dict["message"] = 'Your Profile details have been successfully submitted.'
        except Exception as e:
            print str(e)
            context_dict["error_message"] = 'Error in editing profile info'
        if obj.account_manager:
            context_dict["partner"] = obj.account_manager.affiliateprogramme.name
            context_dict["partner_obj"] = obj.account_manager.affiliateprogramme
        return render(request, 'affiliates/affiliate-edit-profile-1.html', context_dict)
        # return render_to_response('affiliates/affiliate-edit-profile-1.html',
        #                           context_dict,
        #                           context_instance=RequestContext(request))

def indexpage(request):
    context_dict = {}
    ap_obj = AffiliateProgramme.objects.filter(Q(domain__icontains=str(request.META['HTTP_HOST'])) | Q(
                                    wynta_domain__icontains=str(request.META['HTTP_HOST'])))
    
    if 'fozilpartners' in request.META['HTTP_HOST'] and 'affiliates' not in request.META['HTTP_HOST']:
        return render(request, 'fp-index.html', context_dict)
    # if 'fozilpartners' in request.META['HTTP_HOST'] and 'affiliate' not in request.META['HTTP_HOST']:
    #     return render(request, 'fp-index.html', context_dict)
    # elif len(ap_obj) == 0:
    #     return render(request, 'siteinvalid.html', context_dict)
    # else:
    #     return home(request)
    return home(request)

# def indexpage(request):
#     context_dict = {}
#     return render(request, 'windex.html', context_dict)
#     # return render_to_response('fp-index.html',context_dict,
#     #                            context_instance=RequestContext(request))

def integration(request):
    context_dict = {}
    return render(request, 'wynta.html', context_dict)

def thanks(request):
    context_dict = {}
    return render(request, 'thankswynta.html', context_dict)

def thanksaff(request):
    context_dict = {}
    ap_obj = AffiliateProgramme.objects.filter(Q(domain__icontains=str(request.META['HTTP_HOST'])) | Q(
                                    wynta_domain__icontains=str(request.META['HTTP_HOST'])))
    if ap_obj:
        ap_obj = ap_obj[0]
    elif len(ap_obj) == 0:
        return render(request, 'siteinvalid.html', context_dict)
    else:
        if 'fozil' in request.META['HTTP_HOST']:
            ap_obj = AffiliateProgramme.objects.all()[0]
        else:
            ap_obj = AffiliateProgramme.objects.all()[1]
    context_dict['partner'] = ap_obj.name
    context_dict['partner_obj'] = ap_obj
    return render(request, 'affiliates/thanksaff.html', context_dict)

def fp_market(request):
    context_dict = {}
    return render(request, 'fp/fp-marketing.html', context_dict)
    # return render_to_response('fp/fp-marketing.html',context_dict,
    #                            context_instance=RequestContext(request))

def fp_brands(request):
    context_dict = {}
    return render(request, 'fp/fp-brands.html', context_dict)
    # return render_to_response('fp/fp-brands.html',context_dict,
    #                            context_instance=RequestContext(request))

def fp_deals(request):
    context_dict = {}
    return render(request, 'fp/fp-deals.html', context_dict)
    # return render_to_response('fp/fp-deals.html',context_dict,
    #                            context_instance=RequestContext(request))


def redirect_aff_program(request):
    context_dict = {}
    sessionid = get_affiliate_session_id(request)
    if sessionid:
        aid = request.GET.get('aid')
        if sessionid == aid:
            return True
        aff_obj = Affiliateinfo.objects.get(id=aid)
        url_d = aff_obj.account_manager.affiliateprogramme.domain
        if url_d and 'http' not in url_d:
            url_d = 'http://'+url_d
        request.session['affid'] = str(aff_obj.id)
        request.session['affiliate_session_id'] = str(aff_obj.id)
        return redirect(str(url_d))
    else:
        context_dict['status'] = 'Error'
        context_dict['message'] = 'Please login'
        return render(request, 'affiliates/auth/login-1.html', context_dict)
        # return HttpResponseRedirect(reverse('affiliate_home'),)
        # return HttpResponse(json.dumps(context_dict, indent=4))

def set_affiliate_accessible(request):
    context_dict = {}
    sessionid = get_affiliate_session_id(request)
    if sessionid:
        access = request.POST.get('access')
        if access == 'true':
            access = True
        else:
            access = False
        aff_obj = Affiliateinfo.objects.get(id=sessionid)
        aff_obj.accessible = access
        aff_obj.save()
        context_dict['status'] = 'Success'
        return HttpResponse(json.dumps(context_dict, indent=4))
    else:
        context_dict['status'] = 'Error'
        context_dict['message'] = str(e)
        return render(request, 'affiliates/auth/login-1.html', context_dict)

def affiliate_prog_list(request):
    context_dict = {}
    sessionid = get_affiliate_session_id(request)
    try:
        obj = Affiliateinfo.objects.select_related('account_manager').get(id=sessionid)
        aff_objs = Affiliateinfo.objects.filter(email=obj.email)
        available_aff_prog_list = [i.account_manager.affiliateprogramme.id for i in aff_objs]
        aff_prog_list = AffiliateProgramme.objects.exclude(id__in=available_aff_prog_list)
        aff_prog_list = aff_prog_list.filter(accessible=True)
        avv_aff_prog_list = AffiliateProgramme.objects.filter(id__in=available_aff_prog_list)
        avv_aff_prog = []
        total_d = {}
        # currency_earnings_dict = get_aff_earnings(total_d, aff_objs)
        for i in aff_objs:
            aff_prog_obj = i.account_manager.affiliateprogramme
            att_list = [aff_prog_obj.id, aff_prog_obj.name,
                             aff_prog_obj.logo.url if aff_prog_obj.logo else '',
                              i.status]
            network_obj = NetworkAffiliateProgrammeMapping.objects.filter(
                            programme=aff_prog_obj)
            networks_str = ', '.join(list(set([j.networkid.networkname if j.networkid else '' for j in network_obj])))
            att_list.append(networks_str)
            rev_d = 0.0
            # rev_d, total_d = get_aff_earnings(total_d, [i])
            # rev_d, total_d = aff_total_earning(i.email, currency_earnings_dict, total_d)
            att_list.append(rev_d)
            att_list.append(i.id)
            if i == obj or i.status.lower() != 'approved':
                att_list.append('self')
            else:
                att_list.append('no')
            att_list.append(obj.email)
            avv_aff_prog.append(att_list)
        context_dict['avv_aff_prog'] = avv_aff_prog
        total_er_str = ''
        for d in total_d:
            total_er_str += d + ' ' + str(total_d[d])
        context_dict['aff_total_earnings'] = total_er_str
        display_aff_prog = []
        for i in aff_prog_list:
            att_list = [i.id, i.name, i.logo.url if i.logo else '']
            network_obj = NetworkAffiliateProgrammeMapping.objects.filter(
                            programme=i)
            networks_str = ', '.join(list(set([j.networkid.networkname if j.networkid else '' for j in network_obj])))
            att_list.append(networks_str)
            display_aff_prog.append(att_list)
        context_dict['display_aff_prog'] = display_aff_prog
        context_dict['accessible'] = obj.accessible
        context_dict['status'] = 'Success'
        context_dict['message'] = 'Successfully Requested.'
        return HttpResponse(json.dumps(context_dict, indent=4))
    except Exception as e:
        context_dict['status'] = 'Error'
        context_dict['message'] = str(e)
        return HttpResponse(json.dumps(context_dict, indent=4))

def wynta_login(request):
    return render(request, 'affiliates/wyntalogin.html')

def requestap(request):
    context_dict = {}
    sessionid = get_affiliate_session_id(request)
    try:
        obj = Affiliateinfo.objects.select_related('account_manager').get(id=sessionid)
        apid = request.GET.get('apid')
        aff_prog = AffiliateProgramme.objects.get(id=apid)
        account_manager = AccountManager.objects.filter(affiliateprogramme=aff_prog)[0]
        aff_obj = Affiliateinfo.objects.create(username=obj.username,
                   password=obj.password, contactname=obj.contactname,
                   companyname=obj.companyname,
                   email=obj.email,
                   address=obj.address, city=obj.city,
                   state=obj.state, country=obj.country,
                   ipaddress=obj.ipaddress,
                   registeredon=datetime.now(),
                   postcode=obj.postcode, telephone=obj.telephone,
                   dob=obj.dob,
                   account_manager=account_manager)
        networkid_obj = Network.objects.all()
        for nt in networkid_obj:
            aff_obj.networkid.add(nt)
            aff_obj.save()
        for siteobj in account_manager.siteid.all():
            aff_obj.siteid.add(siteobj)
            aff_obj.save()
        aff_obj.save()
        context_dict['status'] = 'Success'
        context_dict['message'] = 'Successfully Requested.'
        return HttpResponse(json.dumps(context_dict, indent=4))
    except Exception as e:
        context_dict['status'] = 'Error'
        return HttpResponse(json.dumps(context_dict, indent=4))

class AffiliateRegister(TemplateView):
    """ class to password reset .
    it will send rest link to player email id """

    def get(self, request):
        """ to render templates """
        context_dict = {}
        context_dict['refid'] = request.GET.get("refid")
        ap_obj = AffiliateProgramme.objects.filter(Q(domain__icontains=str(request.META['HTTP_HOST'])) | Q(
                                    wynta_domain__icontains=str(request.META['HTTP_HOST'])))
        if ap_obj:
            ap_obj = ap_obj[0]
        elif len(ap_obj) == 0:
            return render(request, 'siteinvalid.html', context_dict)
        else:
            if 'fozil' in request.META['HTTP_HOST']:
                ap_obj = AffiliateProgramme.objects.all()[0]
            else:
                ap_obj = AffiliateProgramme.objects.all()[1]
        context_dict['options'] = self.get_reg_page_values(ap_obj)
        context_dict['partner'] = ap_obj.name
        context_dict['partner_obj'] = ap_obj
        context_dict['country_list'] = Country.objects.all()
        context_dict['currency_list'] = [[j['id__max'], j['currency']] for j in Currency.objects.values('currency').annotate(Max('id')).distinct()]
        return render(request, 'affiliates/auth/register-1.html', context_dict)
        # return render_to_response('affiliates/auth/register-1.html',
        #                           context_dict, context_instance=RequestContext(request))
    
    def add_form_fields_data(self, request):
        context_form_data = {}
        for i in request.POST:
            context_form_data[i] = request.POST.get(i, "")
        return context_form_data
    
    def get_reg_page_values(self, ap_obj):
        reg_page_edit = RegistrationPageEdit.objects.all()
        options = []
        for i in reg_page_edit:
            if ap_obj.domain == i.accmanager.affiliateprogramme.domain or ap_obj.wynta_domain == i.accmanager.affiliateprogramme.wynta_domain:
                for i in reg_page_edit[0].options:
                    options.append(i.encode())
        print('contect dict of options',options)
        return options

    def post(self, request):
        context_dict = {}
        context_dict['country_list'] = Country.objects.all()
        # username = request.POST.get("username")
        password = request.POST.get("passwd")
        confirmpassword = request.POST.get("confirmpassword")
        contactname = request.POST.get("contactname")
        lastname = request.POST.get("lastname")
        companyname = request.POST.get("companyname")
        email = request.POST.get("email")
        emailupdates = request.POST.get("emailupdates", '')
        address = request.POST.get("address")
        address2 = request.POST.get("address2")
        city = request.POST.get("city")
        pincode = request.POST.get("pincode")
        refid = request.POST.get("refid")
        mobileupdates = request.POST.get('modeofcontactphone', '')
        im = request.POST.get('modeofcontactim', '')
        skype = request.POST.get('skype', '')
        telephone = request.POST.get("telephone")
        state = request.POST.get("state")
        site_urls = request.POST.getlist("site_url")
        countryname = request.POST.get("countryname")
        dob = request.POST.get("dateofbirth")
        ipaddress = request.META['REMOTE_ADDR']
        if password and confirmpassword:
            if password != confirmpassword:
                form_data_dict = self.add_form_fields_data(request)
                context_dict.update(form_data_dict)
                context_dict["error_message"] = "Password and Confirm password are miss match."
                context_dict['country_list'] = Country.objects.all()
                # context_dict['currency_list'] = [[j['id__max'], j['currency']] for j in Currency.objects.values('currency').annotate(Max('id')).distinct()]
                return render(request, 'affiliates/auth/register-1.html', context_dict)
                # return render_to_response('affiliates/auth/register-1.html',
                #               context_dict, 
                #               context_instance=RequestContext(request))
        network = Network.objects.get(networkid=1)
        ap_obj = AffiliateProgramme.objects.filter(Q(domain__icontains=str(request.META['HTTP_HOST'])) | Q(
                                    wynta_domain__icontains=str(request.META['HTTP_HOST'])))
        if ap_obj:
            ap_obj = ap_obj[0]
            context_dict['partner'] = ap_obj.name
            context_dict['partner_obj'] = ap_obj
        else:
            ap_obj = None
        selected_sites = [] #[s.domain for s in Site.objects.all()]
        options = self.get_reg_page_values(ap_obj)
        has_error, aff_obj_or_msg = check_and_create_affiliate_details(network, selected_sites, email, password, email, contactname, companyname, address, address2, city,
                                      state, countryname, ipaddress, pincode, telephone, refid, lastname, ap_obj, options, dob)
        if has_error:
            form_data_dict = self.add_form_fields_data(request)
            context_dict.update(form_data_dict)
            context_dict["error_message"] = aff_obj_or_msg
            context_dict['country_list'] = Country.objects.all()
            context_dict['currency_list'] = [[j['id__max'], j['currency']] for j in Currency.objects.values('currency').annotate(Max('id')).distinct()]
            return render(request, 'affiliates/auth/register-1.html', context_dict)
            # return render_to_response('affiliates/auth/register-1.html',
            #                   context_dict, 
            #                   context_instance=RequestContext(request))
        else:
            if emailupdates.lower() == 'yes':
                aff_obj_or_msg.mailrequired = True
            else:
                aff_obj_or_msg.mailrequired = False
            if mobileupdates.lower() == 'yes':
                aff_obj_or_msg.mobilerequired = True
            else:
                aff_obj_or_msg.mobilerequired = False
            if im.lower() == 'yes':
                aff_obj_or_msg.im = True
            else:
                aff_obj_or_msg.im = False
            aff_obj_or_msg.skype = skype
            aff_obj_or_msg.save()
            aff_comm_obj = Affiliatecommission.objects.create(fk_affiliateinfo=aff_obj_or_msg,
                                            revshare=True,
                                            revsharepercent=25)
            aff_comm_obj.save()
            for url_d in site_urls:
                if not url_d:
                    continue
                aff_site, created = AffiliateSite.objects.get_or_create(fk_affiliateinfo=aff_obj_or_msg,
                                            info = url_d)
                aff_site.save()
            addpaymentdetails(request, aff_obj_or_msg)
        # request.session['affid'] = str(aff_obj_or_msg.id)
        # request.session['affiliate_session_id'] = str(aff_obj_or_msg.id)
        request.session['created_account'] = True
        return HttpResponseRedirect(reverse('thanksaff'),)


@affiliate_login_required
def sub_affiliate_list(request):
    pass


@affiliate_login_required
def home(request):
    sessionid = get_affiliate_session_id(request)
    try:
        obj = Affiliateinfo.objects.select_related('account_manager').get(id=sessionid)
        commission = Affiliatecommission.objects.select_related('fk_affiliateinfo').filter(fk_affiliateinfo=obj)
        if commission:
            commission = commission[0]
        else:
            commission = Affiliatecommission.objects.select_related('fk_affiliateinfo').filter(
                            account_manager = obj.account_manager)
            if commission:
                commission = commission[0]
            else:
                commission = Affiliatecommission.objects.select_related('fk_affiliateinfo').filter(
                                    siteid__in = obj.siteid.all())
                if commission:
                    commission = commission[0]
                else:
                    commission = Affiliatecommission.objects.all()[0]
        request.user.username = obj.username
        request.user.email = obj.email
    except Exception as e:
        return HttpResponse(e)
    number_of_items = 10000
    nexting_page = 0
    previous_page = 0
    starting_page = 0
    page = 0
    site_ids_list = get_brand_order(obj)
    net_ids_list = [1]
    custom = str(request.POST.get("customid"))
    fromdate = str(request.POST.get("fromdate"))
    todate = str(request.POST.get("todate"))
    domain = request.POST.get("sitename")
    tabled = request.POST.get("tabled")
    chartd = request.POST.get("chart")
    site_name = 'All Websites'
    site_ids = site_ids_list
    if domain and domain != 'All':
        site_domain_list = domain.split(',')
        site_ids = site_ids_list.filter(domain__in=site_domain_list)
        site_name = 'Multiple sites.'
        if len(domain.split(',')) <2:
            site_name = site_ids_list.get(domain=site_domain_list[0]).name
    siteids = request.POST.getlist("siteids")
    if request.method == "GET":
        to_day_date = datetime.now()
        date = to_day_date.day
        year = to_day_date.year
        month = to_day_date.month
        dates_tuple = calendar.monthrange(year, month)
        fromdate = "%s/%s/%s" %(month, 1, year)
        todate = "%s/%s/%s" %(month, date, year)
        custom = "Custom"
        siteids = None
        channel = request.GET.get("channel")
    else:
        custom = str(request.POST.get("customid"))
        fromdate = str(request.POST.get("fromdate"))
        todate = str(request.POST.get("todate"))
        channel = request.POST.get("channel", None)
        if channel and channel == "All":
            channel = None
    context_dict = {}
    currencyfilter = request.POST.get("currency", 'euro')
    date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
    from_date = date_times_dict.get('fromdate')
    to_date = date_times_dict.get('todate')
    request.session['affiliate_dashboard_dates'] = date_times_dict
    aff_objs = list(Affiliateinfo.objects.filter(superaffiliate=obj).select_related('account_manager'))
    header_list = [
           { "title": "Brand", "width":"220px"},
           { "title": "Deposits", "width":"200px"},
           # { "title": "Bonus", "width":"120px"},
           { "title": "Revenue", "width":"200px"},
           { "title": "CPA Commission", "width":"200px"},
           { "title": "CPR Commission", "width":"200px"},
           { "title": "Revshare Commission", "width":"220px"},
           { "title": "Total Commission", "width":"180px"},
           ]
    data_list = []
    currency = ''
    if obj.id == 11:
        aff_objs.append(obj)
        context_dict = get_affiliate_home_stats_changed(aff_objs, aff_objs, commission, from_date, to_date, site_ids, currencyfilter)
        context_dict['device_report'] = get_device_info_chart(aff_objs, aff_objs, commission, from_date, to_date, site_ids)

        header_list, data_list, currency = activity_report_stats(aff_objs, aff_objs, commission, from_date, to_date, site_ids)
    else:
        context_dict = get_affiliate_home_stats_changed(aff_objs, [obj], commission, from_date, to_date, site_ids, currencyfilter)
        context_dict['device_report'] = get_device_info_chart(aff_objs, [obj], commission, from_date, to_date, site_ids)

        header_list, data_list, currency = activity_report_stats(aff_objs, [obj], commission, from_date, to_date, site_ids)
    if request.POST.get("format") == "json" and tabled:
        context_dict["status"] = "success"
        context_dict['data_list'] = data_list
        context_dict["header_list"] = header_list
        return HttpResponse(json.dumps(context_dict, indent=4))
    elif request.POST.get("format") == "json" and chartd:
        context_dict["status"] = "success"
        return HttpResponse(json.dumps(context_dict, indent=4))
    context_dict['device_list'] = ['Web', 'Mobile', 'Tablet', 'Television']
    from_date = date_times_dict.get('fromdate').strftime('%B %d, %Y')
    to_date = date_times_dict.get('todate').strftime('%B %d, %Y')
    ui_from_date = datetime_or_date_to_string(date_times_dict.get('fromdate'))
    ui_to_date = datetime_or_date_to_string(date_times_dict.get('todate'))
    context_dict['stats_date'] = "%s - %s" %(from_date, to_date)
    context_dict["from_date"] = from_date
    context_dict["to_date"] = to_date
    context_dict["ui_from_date"] = ui_from_date
    context_dict["ui_to_date"] = ui_to_date
    context_dict['sites_obj_list'] = site_ids_list

    count = len(data_list)
    start_record, end_record, start_page, \
            end_page, next_page, cur_page, page_count = get_paginate_dimensions(page, starting_page, 
                                                                                nexting_page, previous_page, 
                                                                                count, number_of_items)

    pagination_div  = makePaginationDiv(start_page, page_count, cur_page, next_page, count)
    top_creatives = get_top_creatives(site_ids_list)
    context_dict['sites_net_list'] = NetworkAndSiteMap.objects.filter(siteid__in=site_ids_list)
    context_dict['top_creatives'] = top_creatives
    context_dict["data_list"] = data_list
    context_dict["header_list"] = header_list
    context_dict["currency"] = currency
    context_dict["pagination_div"] = pagination_div
    if obj.account_manager:
        context_dict["partner"] = obj.account_manager.affiliateprogramme.name
        context_dict["partner1"] = json.dumps(obj.account_manager.affiliateprogramme.name)
        context_dict["partner_obj"] = obj.account_manager.affiliateprogramme
    context_dict['domain'] = site_name
    context_dict['data_list'] = json.dumps(data_list)
    context_dict["header_list"] = json.dumps(header_list)
    created_account = request.session.get('created_account')
    if created_account:
        context_dict["message"] = "Your account has been created successfully."
        del request.session['created_account']
    return render(request, 'affiliates/dashboard-1.html', context_dict)


@affiliate_login_required
def refer(request):
    context_dict = {'status': 'Success'}
    try:
        affID = get_affiliate_session_id(request)
        obj = Affiliateinfo.objects.select_related('account_manager').get(id=affID)
        request.user.username = obj.username
        request.user.email = obj.email
        if obj.account_manager:
            context_dict["partner"] = obj.account_manager.affiliateprogramme.name
            if request.method == "GET":
                context_dict["partner_obj"] = obj.account_manager.affiliateprogramme
        if request.method != "GET":
            context_dict['refer_link'] = request.META['HTTP_HOST'] + request.path.replace('get-referral-link/', 'register') + '?refid='+ str(affID)
            return HttpResponse(json.dumps(context_dict), content_type='application/json')
    except Exception as e:
        print str(e)
        context_dict['status'] = 'Error'
    return render(request, 'affiliates/refer-1.html', context_dict)
    # return render_to_response('affiliates/refer-1.html',context_dict, 
    #                            context_instance=RequestContext(request))




# @affiliate_login_required
# def affiliate_player_registrations(request):
#     affID = get_affiliate_session_id(request)
#     obj = Affiliateinfo.objects.select_related('account_manager').get(id=affID)
#     request.user.username = obj.username
#     site_ids_list = get_brand_order(obj)
#     campaign_list = Campaign.objects.filter( fk_affiliateinfo=obj)
#     net_ids_list = []

#     number_of_items = 10000
#     nexting_page = 0
#     previous_page = 0
#     starting_page = 0
#     page = 0
    
#     if request.method == "GET":
#         custom = "Custom"
#         fromdate = request.GET.get("fromdate")
#         todate = request.GET.get("todate")
#         if not fromdate or not todate:
#             custom = None
#         siteids = None
#         chipstype = "cash"
#         campaign_id = None
#         channel = request.GET.get('channel', "All")
#         starting_page = request.GET.get('start_page')
#         nexting_page = request.GET.get('next')
#         previous_page = request.GET.get('prev')
#         page = request.GET.get('page')
#         if not starting_page  and not nexting_page and not previous_page and not page:
#             form_data = {}
#             request.session['affiliate_player_registrations'] = form_data
#         else:
#             form_data = request.session.get('affiliate_player_registrations')
#             custom = form_data.get("customid")
#             fromdate = form_data.get("fromdate")
#             todate = form_data.get("todate")
#             siteids = form_data.get("siteids")
#             campaign_id = form_data.get("campaign_id")
#             channel = form_data.get('channel')
#         if not fromdate and not todate:
#             custom = "Custom"
#             todate = datetime.now().strftime("%m/%d/%Y")
#             current_month , current_year =  datetime.now().month, datetime.now().year
#             fromdate = "%s/01/%s" % (current_month, current_year )
#     else:
#         custom = str(request.POST.get("customid"))
#         fromdate = str(request.POST.get("fromdate"))
#         todate = str(request.POST.get("todate"))
#         siteids = request.POST.getlist("siteids")
#         campaign_id = request.POST.get('campaign_id')
#         channel = request.POST.get('channel', 'All')
#         if campaign_id == 'all':
#             campaign_id = None
        
#         form_data = {'fromdate':fromdate, 'todate':todate,'customid':custom, "siteids":siteids,
                     
#                      "campaign_id":campaign_id, "channel":channel}
#         request.session['affiliate_player_registrations'] = form_data
    
#     if campaign_id:
#         campaign_id = int(campaign_id)
#     if channel and channel == "All":
#         channel = None
#     filter_networks = [1]
#     if siteids:
#         filter_sites = [int(i) for i in siteids]
#     else:
#         filter_sites = [int(i.id) for i in site_ids_list]
#     siteid_list = get_site_ids_data_list(site_ids_list, filter_sites)
    
#     date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
#     from_date = date_times_dict.get('fromdate')
#     to_date = date_times_dict.get('todate')
    
    
    
#     query_set = Playerinfo.objects.filter(registeredon__gte=from_date, 
#                               registeredon__lte=to_date,
#                               playerandsitemapping__siteid__id__in=filter_sites,
#                                playerandsitemapping__networkid__networkid__in=filter_networks,
#                                affiliate=obj)
#     if campaign_id:
#         query_set = query_set.filter(campaign=campaign_id)
#     if channel:
#         device_info_objs = get_selected_channel_devices_info_objs(channel)
#         query_set = query_set.filter(playerinfo1__device_info_id__in=device_info_objs)
#     count = query_set.count()
#     start_record, end_record, start_page, \
#             end_page, next_page, cur_page, page_count = get_paginate_dimensions(page, starting_page, 
#                                                                                 nexting_page, previous_page, 
#                                                                                 count, number_of_items)
#     players_objs_list = query_set.select_related('state','country','affiliate','campaign',
#                                                                              'playerandsitemapping__networkid',
#                                                                              'playerandsitemapping__siteid').\
#                                                                             order_by('-id')[start_record:end_record]
    
#     player_ids_list = [int(i.id) for i in players_objs_list]
    
#     games_dicts = get_players_total_games_played(player_ids_list)
#     total_cash_games_played_list = games_dicts.get("tgp_cash")
#     total_free_games_played_list = games_dicts.get("tgp_free")
    
#     gpr_dict = get_players_gpr_free_cash(player_ids_list)
    
#     player_gpr_free_dict = gpr_dict.get("gpr_free")
#     player_gpr_cash_dict = gpr_dict.get("gpr_cash")
    
#     header_list = [
#            { "title": "User ID"},
#            { "title": "Username"} ,
#            #{ "title": "Affiliate"} ,
#            { "title": "Campaign"} ,
#            { "title": "Player Type"},
#            { "title": "Date of Signup"},
#            { "title": "Verified"},
#            { "title": "GPR Cash"},
#            { "title": "GPR Free"},
#            { "title": "TGP Cash"},
#            { "title": "TGP Free"}
           
#        ]
#     data_list = []
#     sum_list = ["Total :", " "," ","","","",0,0,0,0]
#     for i in players_objs_list:
#         playerid = int(i.id)
#         verfied = "False"
#         if i.verified and i.mobileverified:
#             verfied = "Verified"
#         elif i.verified and not i.mobileverified:
#             verfied = "Mobile-False"
#         elif not i.verified and i.mobileverified:
#             verfied = "Email-False"
                
#         sum_list[6] +=player_gpr_cash_dict.get(playerid, 0)
#         sum_list[7] +=player_gpr_free_dict.get(playerid, 0)
#         sum_list[8] +=total_cash_games_played_list.get(playerid, 0)
#         sum_list[9] +=total_free_games_played_list.get(playerid, 0)
#         data_list.append([i.id, i.username, 
#                           #i.affiliate.username if i.affiliate else "NA", 
#                           i.campaign.name if i.campaign else "NA",
#                           "Deposit" if i.deposits and i.deposits > 0 else "Free",
#                           i.registeredon.strftime("%d-%m-%Y"),
#                           verfied,
#                           player_gpr_cash_dict.get(playerid, 0),
#                           player_gpr_free_dict.get(playerid, 0),
#                           total_cash_games_played_list.get(playerid, 0),
#                           total_free_games_played_list.get(playerid, 0)])
#     data_list.append(sum_list)

#     pagination_div  = makePaginationDiv(start_page, page_count, cur_page, next_page, count)
    
#     context_dict = {"data_list":data_list,"siteid_list":siteid_list,"header_list":header_list,
#                         "from_date":from_date.strftime('%B %d, %Y'),"to_date":to_date.strftime('%B %d, %Y') ,
#                         "ui_from_date":datetime_or_date_to_string(from_date),
#                         "ui_to_date":datetime_or_date_to_string(to_date),
#                        'campaign_id': campaign_id,"pagination_div":pagination_div}
#     if request.POST.get("format") == "json":
#         context_dict["status"] = "success"
#         context_dict['data_list'] = data_list
#         context_dict["header_list"] = header_list
#         return HttpResponse(json.dumps(context_dict, indent=4))
#     context_dict["campaign_list"] = campaign_list
#     context_dict['data_list'] = json.dumps(data_list)
#     context_dict["header_list"] = json.dumps(header_list)
#     context_dict["channels_list"] = get_channels_list()
#     context_dict["channel"] = channel if channel else "All" 
#     return render_to_response('affiliates/reports/player-registrations.html', context_dict,
#                                   context_instance=RequestContext(request))


# @affiliate_login_required
def affiliate_player_registrations(request):
    affID = get_affiliate_session_id(request)
    obj = Affiliateinfo.objects.select_related('account_manager').get(id=affID)
    request.user.username = obj.username
    request.user.email = obj.email
    # site_ids_list = get_brand_order(obj)
    campaign_list = Campaign.objects.filter(fk_affiliateinfo=obj)
    net_ids_list = []

    number_of_items = 10000
    nexting_page = 0
    previous_page = 0
    starting_page = 0
    page = 0
    if request.method == "GET":
        custom = "Custom"
        fromdate = request.GET.get("fromdate")
        todate = request.GET.get("todate")
        if not fromdate or not todate:
            custom = None
        campaign_id = None
        starting_page = request.GET.get('start_page')
        nexting_page = request.GET.get('next')
        previous_page = request.GET.get('prev')
        page = request.GET.get('page')
        if not starting_page  and not nexting_page and not previous_page and not page:
            form_data = {}
            request.session['affiliate_player_registrations'] = form_data
        else:
            form_data = request.session.get('affiliate_player_registrations')
            custom = form_data.get("customid")
            fromdate = form_data.get("fromdate")
            todate = form_data.get("todate")
            siteids = form_data.get("siteids")
            campaign_id = form_data.get("campaign_id")
        if not fromdate and not todate:
            custom = "Custom"
            todate = datetime.now().strftime("%m/%d/%Y")
            current_month , current_year =  datetime.now().month, datetime.now().year
            fromdate = "%s/01/%s" % (current_month, current_year )
    else:
        custom = str(request.POST.get("customid"))
        fromdate = str(request.POST.get("fromdate"))
        todate = str(request.POST.get("todate"))
        siteids = request.POST.getlist("siteids")
        campaign_id = request.POST.get('campaign_id')
        if campaign_id == 'all':
            campaign_id = None
        
        form_data = {'fromdate':fromdate, 'todate':todate,'customid':custom, "siteids":siteids,
                     
                     "campaign_id":campaign_id}
        request.session['affiliate_player_registrations'] = form_data
    
    if campaign_id:
        campaign_id = int(campaign_id)
    # filter_networks = [1]
    # if siteids:
    #     filter_sites = [int(i) for i in siteids]
    # else:
    #     filter_sites = [int(i.id) for i in site_ids_list]
    # siteid_list = get_site_ids_data_list(site_ids_list, filter_sites)
    
    date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
    from_date = date_times_dict.get('fromdate')
    to_date = date_times_dict.get('todate')
    
    
    
    query_set = PPPlayerRaw.objects.filter(registrationdate__gte=from_date, 
                              registrationdate__lte=to_date)
    if campaign_id:
        query_set = query_set.filter(tracker=campaign_id)
    # if channel:
    #     device_info_objs = get_selected_channel_devices_info_objs(channel)
    #     query_set = query_set.filter(playerinfo1__device_info_id__in=device_info_objs)
    count = query_set.count()
    start_record, end_record, start_page, \
            end_page, next_page, cur_page, page_count = get_paginate_dimensions(page, starting_page, 
                                                                                nexting_page, previous_page, 
                                                                                count, number_of_items)
    players_objs_list = query_set.order_by('-id')[start_record:end_record]
    
    player_ids_list = [int(i.id) for i in players_objs_list]
    
    header_list = [
            { "title": "Site Id"} ,
           { "title": "User ID"},
           { "title": "Affiliate"} ,
           { "title": "Campaign"} ,
           { "title": "Country"},
           { "title": "Date of Signup"},
           { "title": "Currency"},
           { "title": "Status"},
           { "title": "Ftd"},
           { "title": "Deposit"},
           { "title": "Total Deposit"},
           { "title": "Cashout"},
           { "title": "Total Cashout"},
           { "title": "Bonuses"},
           { "title": "Expired Bonuses"},
           { "title": "Revenue"},
           { "title": "Chargeback"},
           { "title": "Reverse Chargeback"},
           { "title": "SideGames Bets"},
           { "title": "SideGames Wins"},
           { "title": "Jackpot Contribution"}
       ]
    data_list = []
    for i in players_objs_list:
        playerid = int(i.id)
        data_list.append([i.whitelabelid, i.id, "Sticky Slots",
                          i.tracker, i.country,
                          i.registrationdate.strftime("%d-%m-%Y"),
                          i.currency, i.status, i.ftd,
                          i.deposit, i.totaldeposit,
                          i.cashout, i.totalcashout,
                          i.bonuses, i.expiredbonuses, i.revenue,
                          i.chargeback, i.reversechargeback,
                          i.sidegamesbets, i.sidegameswins, i.jackpotcontribution
                          ])

    pagination_div  = makePaginationDiv(start_page, page_count, cur_page, next_page, count)
    
    context_dict = {"data_list":data_list,"header_list":header_list,
                        "from_date":from_date.strftime('%B %d, %Y'),"to_date":to_date.strftime('%B %d, %Y') ,
                        "ui_from_date":datetime_or_date_to_string(from_date),
                        "ui_to_date":datetime_or_date_to_string(to_date),
                       'campaign_id': campaign_id,"pagination_div":pagination_div}
    if request.POST.get("format") == "json":
        context_dict["status"] = "success"
        context_dict['data_list'] = data_list
        context_dict["header_list"] = header_list
        return HttpResponse(json.dumps(context_dict, indent=4))
    context_dict["campaign_list"] = campaign_list
    context_dict['data_list'] = json.dumps(data_list)
    context_dict["header_list"] = json.dumps(header_list)
    # context_dict["channels_list"] = get_channels_list()
    # context_dict["channel"] = channel if channel else "All"
    return render(request, 'affiliates/reports/player-registrations.html', context_dict)
    # return render_to_response('affiliates/reports/player-registrations.html', context_dict,
    #                               context_instance=RequestContext(request))

from django.shortcuts import redirect
import uuid

def get_info_from_ip(request):
    loginip = request.META['REMOTE_ADDR']
    try:
        url = "http://pro.ip-api.com/json/%s?key=%s"% (loginip, settings.IP_API_KEY)
        resp = requests.get(url)
        if resp.status_code == 200:
            resp_details = json.loads(resp.text)
            state = resp_details['regionName'].upper()
            country = resp_details['country']
            return [country, state, loginip]
    except:
        return ['', '', loginip]


def add_click_info(camp_map_obj, request, camp_obj):
    w_dynamic = request.GET.get("track")
    tracker = request.GET.get("tracker")
    lang = request.GET.get("lang", '')
    aff_id = request.GET.get("aid")
    netw_site_obj = NetworkAndSiteMap.objects.get(siteid=camp_obj.siteid)
    content_type = netw_site_obj.networkid.connect_type
    unique_code = uuid.uuid4().hex
    _d = datetime.now().date()
    if request.user_agent.is_mobile:
        platform = 'Mobile'
    elif request.user_agent.is_tablet:
        platform = 'Tablet'
    elif request.user_agent.is_pc:
        platform = 'Web'
    else:
        platform = 'Bot'
    info_list = ['','','',''] 
    # get_info_from_ip(request)
    if info_list and info_list[0]:
        country_obj, created = Country.objects.get_or_create(countryname=info_list[0])
        country_obj.save()
    camp_click_obj, created = CampaignClicksCount.objects.get_or_create(date=_d,
                        fk_campaign = camp_map_obj,
                        platform = platform,
                        country = info_list[0]
                        )
    camp_click_obj.clicks = camp_click_obj.clicks + 1 if camp_click_obj.clicks else 1
    camp_click_obj.save()
    # postback_adv, created = PostBackAdvanced.objects.get_or_create(date=_d,
    #                     siteid = camp_obj.siteid,
    #                     fk_campaign = camp_map_obj,
    #                     networkid = netw_site_obj.networkid
    #                     )
    # postback_adv.clicks = postback_adv.clicks + 1 if postback_adv.clicks else 1
    # postback_adv.save()
    postback_clicks = PostBackClicks.objects.create(
                        siteid = camp_obj.siteid,
                        fk_campaign = camp_map_obj,
                        networkid = netw_site_obj.networkid,
                        clickid=unique_code,
                        ip_address=info_list[2],
                        country = info_list[0],
                        state = info_list[1],
                        platform = platform
                        )
    if w_dynamic:
        postback_clicks.wdynamic = w_dynamic
    postback_clicks.save()

    if content_type == 'POST_BACK' and camp_map_obj.url:
        url_d = 'https://' + camp_map_obj.url + '&clickid='+ str(unique_code)
    elif content_type == 'FTP' and camp_map_obj.url:
        url_d = camp_map_obj.url
        if netw_site_obj.networkid.networkname == 'Nektan':
            url_d = camp_map_obj.url + '&tcode='+ str(unique_code)
    elif camp_map_obj.url and netw_site_obj.networkid.networkname == 'YayaGaming':
        url_d = camp_map_obj.url + '?btag='+ str(unique_code) + '&affid=' + str(aff_id)
    elif camp_map_obj.url and netw_site_obj.networkid.networkname == 'Dragonfish':
        url_d = camp_map_obj.url + str(unique_code)
    elif camp_map_obj.url:
        if netw_site_obj.networkid.networkname == 'ProgressPlay' and tracker:
            url_d = camp_map_obj.url.split('tracker=')[0] + 'tracker='+tracker + '&btag='+ str(unique_code)
        else:
            url_d = camp_map_obj.url + '&btag='+ str(unique_code)
        if lang:
            url_d += '&lang='+lang
    else:
        camp_map_obj = create_new_url_tracker(camp_map_obj.trackerid, camp_obj, 
                        camp_map_obj.fk_affiliateinfo)
        url_d = camp_map_obj.url
        if lang:
            url_d += '&lang='+lang
    return url_d

def redirectfp(request):
    resp_dict = {}
    try:
        if request.method == "GET":
            campaignid = request.GET.get("cid")
            aff_id = request.GET.get("aid")
            unique_code = uuid.uuid4().hex
            blid = request.GET.get("blid",None)
            mlid = request.GET.get("mlid",None)
            camp_obj = Campaign.objects.get(id=campaignid)
            if aff_id:
                aff_obj = Affiliateinfo.objects.select_related('account_manager').get(id=aff_id)
            camp_map_obj = CampaignTrackerMapping.objects.filter(fk_campaign=camp_obj, 
                            fk_affiliateinfo=aff_obj,
                            )
            if not camp_map_obj:
                camp_map_obj = add_campaign_tracker(aff_obj, camp_obj)[1]
            else:
                camp_map_obj = camp_map_obj.first()
            netw_site_obj = NetworkAndSiteMap.objects.get(siteid=camp_obj.siteid)
            content_type = netw_site_obj.networkid.connect_type
            _d = datetime.now().date()
            if content_type in ['POST_BACK', 'API_REPORT', 'FTP']:
                url_d = add_click_info(camp_map_obj, request, camp_obj)
            elif content_type == 'UPLOAD':
                url_d = add_click_info(camp_map_obj, request, camp_obj)
                # process_obj, created = UploadAdvanced.objects.get_or_create(date=_d,
                #                     siteid = camp_obj.siteid,
                #                     fk_campaign = camp_map_obj,
                #                     processed = True)
                # process_obj.clicks = process_obj.clicks + 1 if process_obj.clicks else 1
                # process_obj.save()
                url_d = camp_map_obj.url
            else:
                url_d = camp_map_obj.url
            if blid:
                bannerlogobj = Bannerlogs.objects.get(id=blid)
                if '#' in bannerlogobj.link:
                    url_d = url_d + '#'+bannerlogobj.link.split('/#')[1]
                    return redirect(url_d)
                else:
                    if '?' not in bannerlogobj.link:
                        url_d = bannerlogobj.link +'?'+ url_d.split('/')[1]
                    else:
                        url_d = bannerlogobj.link +'&'+ url_d.split('?')[1]
                    return redirect(url_d)
            if mlid:
                mailerlogobj = Mailerlogs.objects.get(id=mlid)
                print('mailerlogobjc  ',mailerlogobj, url_d)
                if '#' in mailerlogobj.link:
                    url_d = url_d + '#'+mailerlogobj.link.split('/#')[1]
                    return redirect(url_d)
                else:
                    if '?' not in mailerlogobj.link:
                        url_d = mailerlogobj.link +'?'+ url_d.split('/')[1]
                    else:
                        url_d = mailerlogobj.link +'&'+ url_d.split('?')[1]
                    print('url d', url_d)
                    return redirect(url_d)
            lpagelogobj = LandingPagelogs.objects.filter(fk_campaign=camp_obj)
            if lpagelogobj and lpagelogobj[0].fk_lpage.link:
                if 'casino-pp.net' in lpagelogobj[0].fk_lpage.link:
                    url_d = lpagelogobj[0].fk_lpage.link.split('/#')[0]+'/?' + url_d.split('?')[1]
                    if '#' in lpagelogobj[0].fk_lpage.link:
                        url_d = url_d + '#'+lpagelogobj[0].fk_lpage.link.split('/#')[1]
                else:
                    if '?' not in lpagelogobj[0].fk_lpage.link:
                        url_d = lpagelogobj[0].fk_lpage.link +'?'+ url_d.split('?')[1]
                    else:
                        url_d = lpagelogobj[0].fk_lpage.link +'&'+ url_d.split('?')[1]
            else:
                bannerlogobj = Bannerlogs.objects.filter(fk_campaign=camp_obj)
                if bannerlogobj and bannerlogobj[0].fk_bannerimages.link:
                    if 'casino-pp.net' in bannerlogobj[0].fk_bannerimages.link:
                        url_d = bannerlogobj[0].fk_bannerimages.link.split('/#')[0]+'/?'+url_d.split('?')[1]
                        if '#' in bannerlogobj[0].fk_bannerimages.link:
                            url_d = url_d + '#'+bannerlogobj[0].fk_bannerimages.link.split('/#')[1]
                    else:
                        if '?' not in bannerlogobj[0].fk_bannerimages.link:
                            url_d = bannerlogobj[0].fk_bannerimages.link +'?'+ url_d.split('?')[1]
                        else:
                            url_d = bannerlogobj[0].fk_bannerimages.link +'&'+ url_d.split('?')[1]
            # if 'vcommsubaff' in camp_map_obj.fk_affiliateinfo.username.lower() and camp_obj.siteid.id==1:
            #     url_d = 'http://www.stickyslots.com/730500' +'?'+ url_d.split('?')[1]
            return redirect(url_d)
    except Exception as e:
        resp_dict['error_message'] = str(e)
        print('error in redirect fp',str(e))
        import traceback
        traceback.print_exc()
        # return HttpResponse(json.dumps(resp_dict), content_type='application/json')
        return render(request, '500.html', resp_dict)
        # return render_to_response('affiliates/createtracker-1.html',resp_dict,
        #                    context_instance=RequestContext(request))

def gamewinning_images(request):
    resp = {"status":"error", "data":[]}
    tableid = request.GET.get("tableid")
    gameid = request.GET.get("gameid")
    sessionid = request.session.get('id')
    try:
        obj = Playerinfo.objects.get(id=sessionid)
        if tableid and gameid:
            data = GameWinningImage.objects.filter(playerid=obj, tableid=tableid,
                                            gameid=gameid)
            data = [[str(i.link.url),i.subgameid] for i in data]
            print data
            if data:
                resp["data"] = data
                resp ["status"] = "success"
    except Exception as e:
        resp["message"] = e
    return HttpResponse(json.dumps(resp), content_type='application/json')


def get_category(request):
    resp = {"status":"error", "categorys":[], 'banners':[]}
    affID = get_affiliate_session_id(request)
    try:
        obj = Affiliateinfo.objects.select_related('account_manager').get(id=affID)
        request.user.username = obj.username
        request.user.email = obj.email
    except:
        return HttpResponseRedirect(reverse("affiliate_login"))
    domain = request.POST.get("sitename")
    media_type = request.POST.get("media_type")
    sites_obj = get_brand_order(obj)
    sites_obj = sites_obj.get(domain=domain)
    category_obj = [i[0] for i in MediaCategory.objects.filter(siteid=sites_obj).values_list('category_name')]
    banners = []
    if media_type:
        if media_type == 'V':
            videos = Video.objects.filter(siteid=sites_obj)
            banners = [[str(i.video.url),i.title, i.id] for i in videos if i.video]
            resp['links'] = banners
        elif media_type in ['SS', 'P', 'B']:
            banners = BannerImages.objects.filter(siteid=sites_obj, media_type=media_type)
            banners = [[str(i.banner.url),i.title, i.id, i.width, i.height] for i in banners if i.banner]
            resp['links'] = banners
        elif media_type == 'LP':
            lpages = Lpages.objects.filter(siteid=sites_obj)
            banners = [[str(i.link),i.title, i.id] for i in lpages]
            resp['links'] = banners
        elif media_type == 'M':
            mailers = Mailer.objects.filter(siteid=sites_obj)
            banners = [[str(i.subject), str(i.content), i.id] for i in mailers]
            resp['links'] = banners
        lpages = Lpages.objects.filter(siteid=sites_obj)
        lp_banners = [[str(i.link),i.title, i.id] for i in lpages]
        resp['lp_links'] = lp_banners
    # banners = BannerImages.objects.filter(siteid=sites_obj)
    # banners = [[str(i.banner.url),i.title, i.id] for i in banners]
    media_dict = dict(Campaign.MEDIA_TYPE)
    resp['media_type'] = media_dict[media_type]
    resp['categorys'] = category_obj
    resp['banners'] = banners
    resp['links'] = banners
    return HttpResponse(json.dumps(resp, indent=4))

def addpaymentdetails(request, obj):
    try:
        accountnumber = request.POST.get("accountnumber")
        sortcode = request.POST.get("sortcode")
        accountname = request.POST.get("accountname")
        iban = request.POST.get("iban")
        swift = request.POST.get("swift")
        bankname = request.POST.get("bankname")
        bankaddress = request.POST.get("bankaddress")
        beneficiaryaddress = request.POST.get("beneficiaryaddress")
        paypal_username = request.POST.get("paypal_username")
        paypal_email = request.POST.get("paypal_email")
        neteller_username = request.POST.get("neteller_username")
        neteller_accountnumber = request.POST.get("neteller_accountnumber")
        neteller_email = request.POST.get("neteller_email")
        acc_details_obj, created = AccountDetails.objects.get_or_create(
                        fk_affiliateinfo = obj)
        acc_details_obj.accountnumber = accountnumber
        acc_details_obj.sortcode = sortcode
        acc_details_obj.accountname = accountname
        acc_details_obj.iban = iban
        acc_details_obj.swift = swift
        acc_details_obj.bankname = bankname
        acc_details_obj.bankaddress = bankaddress
        acc_details_obj.beneficiaryaddress = beneficiaryaddress
        acc_details_obj.neteller_username = neteller_username
        acc_details_obj.neteller_email = neteller_email
        acc_details_obj.neteller_accountnumber = neteller_accountnumber
        acc_details_obj.paypal_email = paypal_email
        acc_details_obj.paypal_username = paypal_username
        acc_details_obj.save()
        return True
    except:
        return False


class PaymentDetails(TemplateView):
    """ class edit profile """
    def get(self, request):  
        context_dict = {}
        sessionid = get_affiliate_session_id(request)
        affid = sessionid
        if not affid:
            return HttpResponseRedirect(reverse("affiliate_login"))

        obj = Affiliateinfo.objects.select_related('account_manager').get(id=affid)
        request.user.username = obj.username
        request.user.email = obj.email
        # aff_com_list = Affiliatecommission.objects.filter( fk_affiliateinfo__in=[affid])
        # aff_com_dict = dict( (i.fk_affiliateinfo_id, i)  for i in aff_com_list)
        data_list = []
        acc_details = AccountDetails.objects.filter(fk_affiliateinfo = obj)
        if not acc_details:
            acc_obj = AccountDetails.objects.create(fk_affiliateinfo = obj)
            acc_details = [acc_obj]
        if obj.account_manager:
            context_dict["partner"] = obj.account_manager.affiliateprogramme.name
            context_dict["partner_obj"] = obj.account_manager.affiliateprogramme
        if acc_details[0].accountname == None and acc_details[0].paypal_email == None and acc_details[0].neteller_accountnumber == None:
            context_dict["noPay"] = "No payment details have been provided yet."
        context_dict["domain"] = "All Websites"
        context_dict["data"] = acc_details[0] if acc_details else {}
        currency_l = []
        context_dict['currency_list'] = [[j['id__max'], j['currency']] for j in Currency.objects.values('currency').annotate(Max('id')).distinct()]
        # context_dict["commission_obj"] = aff_com_dict.get(obj.id)
        return render(request, 'affiliates/affiliate-payment-details.html', context_dict)
        # return render_to_response('affiliates/affiliate-edit-profile-1.html',
        #                           context_dict,
        #                           context_instance=RequestContext(request))

    def post(self, request):
        context_dict = {}
        sessionid = get_affiliate_session_id(request)
        affid = sessionid
        if not affid:
            return HttpResponse("affid is manadatory parameter.")
        try:
            obj = Affiliateinfo.objects.select_related('account_manager').get(id=affid)
            accountnumber = request.POST.get("accountnumber")
            sortcode = request.POST.get("sortcode")
            accountname = request.POST.get("accountname")
            iban = request.POST.get("iban")
            swift = request.POST.get("swift")
            bankname = request.POST.get("bankname")
            bankaddress = request.POST.get("bankaddress")
            beneficiaryaddress = request.POST.get("beneficiaryaddress")
            paypal_username = request.POST.get("paypal_username")
            paypal_email = request.POST.get("paypal_email")
            neteller_username = request.POST.get("neteller_username")
            neteller_email = request.POST.get("neteller_email")
            neteller_accountnumber = request.POST.get("neteller_accountnumber")
            acc_details_obj, created = AccountDetails.objects.get_or_create(
                            fk_affiliateinfo = obj)
            acc_details_obj.accountnumber = accountnumber
            acc_details_obj.sortcode = sortcode
            acc_details_obj.accountname = accountname
            acc_details_obj.iban = iban
            acc_details_obj.swift = swift
            acc_details_obj.bankname = bankname
            acc_details_obj.bankaddress = bankaddress
            acc_details_obj.beneficiaryaddress = beneficiaryaddress
            acc_details_obj.neteller_username = neteller_username
            acc_details_obj.neteller_accountnumber = neteller_accountnumber
            acc_details_obj.neteller_email = neteller_email
            acc_details_obj.paypal_email = paypal_email
            acc_details_obj.paypal_username = paypal_username
            # checking for payment type
            if neteller_accountnumber != '':
                acc_details_obj.accounttype = "Neteller"
            elif paypal_email != ' ':
                acc_details_obj.accounttype = "Paypal"
            elif accountnumber != '': 
                acc_details_obj.accounttype = "Bank Wire Transfer"
            accounttype = acc_details_obj.accounttype
            acc_details_obj.save()
            subject = "Payment details added - {0} ({1})".format(obj.email,obj.account_manager.affiliateprogramme.name)
            content = """Dear {0},<br><br>
                        Your affiliate {1}- {2} - on {3} has successfully updated their payment details for {4} payments.<br><br> 
                        Thanks,<br>
                        Team @ Wynta""".format(obj.account_manager.user.username, obj.id, obj.email, obj.account_manager.affiliateprogramme.name, accounttype)
            check_EmailClient(obj.account_manager, None, subject, content, """%s""" % 'support@wynta.com', [], [obj.account_manager.user.email], None, obj,None,None, None)
            request.user.username = obj.username
            request.user.email = obj.email
        except Exception as e:
            print str(e)
            context_dict["error_message"] = 'Error in editing Payment info'
            context_dict["domain"] = "All Websites"
            return render(request, 'affiliates/affiliate-payment-details.html', context_dict)
        context_dict["aff_obj"] = obj
        if obj.account_manager:
            context_dict["partner"] = obj.account_manager.affiliateprogramme.name
            context_dict["partner_obj"] = obj.account_manager.affiliateprogramme
        context_dict["message"] = 'Your payment details have been successfully submitted.'
        context_dict["data"] = acc_details_obj
        context_dict["domain"] = "All Websites"
        context_dict['currency_list'] = [[j['id__max'], j['currency']] for j in Currency.objects.values('currency').annotate(Max('id')).distinct()]
        return render(request, 'affiliates/affiliate-payment-details.html', context_dict)

@affiliate_login_required
def createtracker(request):
    affID = get_affiliate_session_id(request)
    try:
        obj = Affiliateinfo.objects.select_related('account_manager').get(id=affID)
        request.user.username = obj.username
        request.user.email = obj.email
    except:
        return HttpResponseRedirect(reverse("affiliate_login"))
    resp_dict = {}
    resp_dict['obj'] = obj
    if obj.account_manager:
        resp_dict["partner"] = obj.account_manager.affiliateprogramme.name
        resp_dict["partner_obj"] = obj.account_manager.affiliateprogramme
    site_ids_list = get_brand_order(obj)
    category_obj = MediaCategory.objects.all().values('category_name').distinct('siteid')
    resp_dict['media_types'] = list(Campaign.MEDIA_TYPE)
    resp_dict['languages'] = dict(Campaign.Language)
    resp_dict['sites_obj_list'] = site_ids_list
    resp_dict['category_obj_list'] = ''
    resp_dict["message"] = None
    site_name = 'All Websites'
    if request.method =="POST":
        linkname = request.POST.get('linkname')
        domain = request.POST.get("sitename")
        media_type = request.POST.get("media_type")
        language = request.POST.get("language", 'English')
        category = request.POST.get("category")
        bannerid = request.POST.get("bannerid")
        landingpage = request.POST.get("landingpage")
        if domain and domain != 'All':
            site_ids = [site_ids_list.get(domain=domain)]
            site_name = site_ids_list.get(domain=domain).name
        has_error, camp_obj_or_msg = validate_and_create_campaign(obj, linkname,
                                 domain, [media_type, language, category])
        if has_error:
            resp_dict['error_message'] = camp_obj_or_msg
            return render(request, 'affiliates/createtracker-1.html', resp_dict)
            # return render_to_response('affiliates/createtracker-1.html',resp_dict, 
            #                    context_instance=RequestContext(request))
        resp_dict["tracker_obj"] = camp_obj_or_msg
        imagelink = ''
        if media_type and bannerid:
            #resp_dict['bannerid'] = bannerid
            #resp_dict['media_type'] = media_type
            if media_type == 'V':
                videoobj = Video.objects.get(id=bannerid)
                videologobj = Videologs.objects.create(fk_campaign=camp_obj_or_msg, fk_video=videoobj)
                videologobj.save()
                imagelink = videoobj.video.url
            elif media_type in ['SS', 'P', 'B']:
                bannerobj = BannerImages.objects.get(id=bannerid)
                bannerlogobj = Bannerlogs.objects.create(fk_campaign=camp_obj_or_msg, fk_bannerimages=bannerobj)
                bannerlogobj.save()
                imagelink = bannerobj.banner.url
                resp_dict['width'] = bannerobj.width
                resp_dict['height'] = bannerobj.height
        if landingpage:
            lpageobj = Lpages.objects.get(id=landingpage)
            lpagelogobj = LandingPagelogs.objects.create(fk_campaign=camp_obj_or_msg, fk_lpage=lpageobj)
            lpagelogobj.save()
        request.session["created_tracker"] = linkname
        resp_dict['media_type'] = media_type
        resp_dict['cid'] = camp_obj_or_msg.id
        resp_dict['affid'] = obj.id
        resp_dict['imagelink'] = imagelink
        resp_dict['global_camp_dict'] = {}
        return render(request, 'affiliates/createtracker-1.html', resp_dict)
    else:
        domain = request.GET.get("site")
        media_type = request.GET.get("media_type")
        bannerid = request.GET.get("bid")
        abs_url = request.build_absolute_uri().split('/affiliate/')[0]
        global_camp_objs = Campaign.objects.filter(Q(fk_affiliateinfo__isnull=True) &
                        Q(account_manager=obj.account_manager) &
                        Q(siteid__in=site_ids_list)).values_list('id',
                        'siteid__domain').distinct()
        resp_dict['global_camp_dict'] = {str(i[1]):get_affiliate_campaign_link(
                                Campaign.objects.get(id=i[0]), obj, {}, abs_url)+'&lang=&track=' 
                                for i in global_camp_objs}
        if media_type and bannerid:
            resp_dict['bannerid'] = bannerid
            resp_dict['media_type'] = media_type
            if media_type == 'V':
                videos = Video.objects.filter(id=bannerid)
                domain = videos[0].siteid.name
                site_obj = videos[0].siteid
                banners = [[str(i.video.url),i.title, i.id] for i in videos if i.video]
                resp_dict['links'] = banners
            elif media_type in ['SS', 'P', 'B']:
                banners = BannerImages.objects.filter(id=bannerid, media_type=media_type)
                domain = banners[0].siteid.name
                site_obj = banners[0].siteid
                banners = [[str(i.banner.url),i.title, i.id, i.width, i.height] for i in banners if i.banner]
                resp_dict['links'] = banners
            elif media_type == 'LP':
                lpages = Lpages.objects.filter(id=bannerid)
                domain = lpages[0].siteid.name
                site_obj = lpages[0].siteid
                # banners = [[str(i.link),i.title, i.id] for i in lpages]
                # resp_dict['links'] = banners
            lpages = Lpages.objects.filter(siteid=site_obj)
            # domain = lpages[0].siteid.name
            if lpages:
                lp_banners = [[str(i.link),i.title, i.id] for i in lpages]
                resp_dict['lp_links'] = lp_banners
            resp_dict['sitename'] = domain
            resp_dict['domain'] = domain
        else:
            resp_dict['media_type'] = media_type
            resp_dict['sitename'] = domain
            resp_dict['domain'] = domain
            # elif media_type == 'M':
            #     mailers = Mailer.objects.filter(id=bannerid)
            #     banners = [[str(i.subject), str(i.content), i.id] for i in mailers]
            #     resp_dict['links'] = banners
    # resp_dict["domain"] = site_name
    return render(request, 'affiliates/createtracker-1.html', resp_dict)
    # return render_to_response('affiliates/createtracker-1.html',resp_dict, 
    #                            context_instance=RequestContext(request))

def get_market_materials(request):
    resp = {"status":"error", "categorys":[], 'links':[]}
    affID = get_affiliate_session_id(request)
    try:
        obj = Affiliateinfo.objects.select_related('account_manager').get(id=affID)
        request.user.username = obj.username
        request.user.email = obj.email
    except:
        return HttpResponseRedirect(reverse("affiliate_login"))
    domain = request.POST.get("sitename")
    print 'domain', domain
    try:
        media_type = request.POST.get("media_type")
        bid = request.POST.get("bid")
        lang = request.POST.get("language")
        size = request.POST.get("size")
        site_ids_list = get_brand_order(obj)
        lpage = request.POST.get("lpage")
        site_name = 'All Websites'
        site_ids = site_ids_list
        if domain and domain != 'All':
            site_domain_list = domain.split(',')
            site_ids = site_ids_list.filter(domain__in=site_domain_list)
            site_name = 'Multiple sites.'
            if len(domain.split(',')) <2:
                site_name = site_ids_list.get(domain=site_domain_list[0]).name
        else:
            site_ids = site_ids_list
        if bid:
            site_ids = site_ids_list
        category_obj = [i[0] for i in MediaCategory.objects.filter(siteid__in=site_ids).values_list('category_name')]
        resp['categorys'] = category_obj
        media_dict = dict(Campaign.MEDIA_TYPE)
        resp['media_type'] = media_dict[media_type]
        resp['site'] = domain
        # lpages = Lpages.objects.filter(siteid=site_ids)
        # banners = [[str(i.link),i.title, i.id] for i in lpages]
        resp['lp_links'] = []
        request.session['marketing_redirect'] = resp
        if media_type:
            if media_type == 'V':
                videos = Video.objects.filter(siteid__in=site_ids,user = obj.account_manager.user)
                if bid:
                    videos = videos.filter(id=bid)
                #     if video_selected: 
                if lang:
                    videos = videos.filter(language=lang)     
                banners_list = []
                for i in videos:
                    camp_id = 0
                    videologobj = Videologs.objects.filter(fk_video=i, 
                                        fk_campaign__name=i.title,
                                        fk_campaign__account_manager=obj.account_manager,
                                        fk_campaign__fk_affiliateinfo__isnull=True) 
                    if videologobj and (i.video or i.link):
                        camp_id = videologobj[0].fk_campaign.id
                    elif i.video or i.link:
                        network_obj = NetworkAndSiteMap.objects.get(siteid=i.siteid)
                        camp_obj, created = Campaign.objects.get_or_create(
                            name=i.title, siteid=i.siteid,
                            account_manager=obj.account_manager,
                            networkid=network_obj.networkid, media_type=media_type)
                        camp_obj.save()
                        videologobj, created = Videologs.objects.get_or_create(
                            fk_campaign=camp_obj, fk_video=i)
                        videologobj.save()
                        camp_id = camp_obj.id
                    if i.video:
                        banners_list.append([str(i.video.url),i.title, i.id, camp_id, obj.id])
                    else:
                        if i.link:
                            print('inside i link>>>>>>>', i.link,camp_id )
                            banners_list.append([str(i.link),i.title, i.id, camp_id, obj.id])
                # banners = [[str(i.video.url),i.title, i.id] for i in videos if i.video]
                resp['links'] = banners_list
            elif media_type in ['SS', 'P', 'B']:
                banners = BannerImages.objects.filter(siteid__in=site_ids,
                                user = obj.account_manager.user,
                                media_type=media_type)
                if lang:
                    banners = banners.filter(language=lang)
                if size:
                    banners = banners.filter(width=size.split('x')[0], height=size.split('x')[1])
                if bid:
                    banners = banners.filter(id=bid)
                camp_dict = {}
                banners_list = []
                lp_links = []
                for i in banners:
                    camp_id = 0
                    bannerlogobj = Bannerlogs.objects.filter(fk_bannerimages=i, 
                                        fk_campaign__name=i.title,
                                        fk_campaign__account_manager=obj.account_manager,
                                        fk_campaign__fk_affiliateinfo__isnull=True)
                    if bannerlogobj and i.banner:
                        camp_id = bannerlogobj[0].fk_campaign.id
                    elif not bannerlogobj and i.banner:
                        network_obj = NetworkAndSiteMap.objects.get(siteid=i.siteid)
                        camp_obj, created = Campaign.objects.get_or_create(
                            name=i.title, siteid=i.siteid,
                            account_manager=obj.account_manager,
                            networkid=network_obj.networkid, media_type=media_type)
                        camp_obj.save()
                        bannerlogobj, created = Bannerlogs.objects.get_or_create(
                            fk_campaign=camp_obj, fk_bannerimages=i)
                        bannerlogobj.save()
                        camp_id = camp_obj.id
                    if i.banner:
                        lpages = Lpages.objects.filter(link=i.link,siteid=i.siteid)
                        if lpages:
                            for j in lpages:
                                lp_banners = [str(j.link),j.title, j.id]
                                lp_links.append(lp_banners)
                        else:
                            lp_links.append([str(domain),'Home Page', ''])
                        banners_list.append([str(i.banner.url),i.title,
                                i.id, i.width, i.height, camp_id, obj.id])
                if lp_links:
                    resp['lp_links'] = []
                    for i in lp_links:
                        if i not in resp['lp_links']:
                            resp['lp_links'].append(i)
                resp['links'] = banners_list
            elif media_type == 'LP':
                banners_list = []
                lpages = Lpages.objects.filter(siteid__in=site_ids,user = obj.account_manager.user)
                if lang:
                    lpages = lpages.filter(language=lang)
                if bid:
                    lpages = lpages.filter(id=bid)
                for i in lpages:
                    network_s_obj = NetworkAndSiteMap.objects.filter(siteid=i.siteid)
                    url_t = i.lpages.url if i.lpages else ''
                    if not url_t:
                        url_t = network_s_obj[0].logo.url if network_s_obj else ''

                    lpageslogobj = LandingPagelogs.objects.filter(fk_lpage=i, 
                                        fk_campaign__name=i.title,
                                        fk_campaign__account_manager=obj.account_manager,
                                        fk_campaign__fk_affiliateinfo__isnull=True)
                    if lpageslogobj and i.link:
                        camp_id = lpageslogobj[0].fk_campaign.id
                    elif i.link:
                        camp_obj, created = Campaign.objects.get_or_create(
                            name=i.title, siteid=i.siteid,
                            account_manager=obj.account_manager, media_type=media_type)
                        camp_obj.save()
                        lpageslogobj, created = LandingPagelogs.objects.get_or_create(
                            fk_campaign=camp_obj, fk_lpage=i)
                        lpageslogobj.save()
                        camp_id = camp_obj.id
                    if i.link:
                        banners_list.append([str(i.link),i.title, i.id, 
                            url_t, camp_id, obj.id])
                    # banners_l.append([str(i.link),i.title, i.id, url_t])
                banners = banners_list
                resp['links'] = banners
            elif media_type == 'M':
                siteid=Site.objects.get(domain=site_ids[0])
                mailers = Mailer.objects.filter(siteid=int(siteid.id),user = obj.account_manager.user)
                # banners = [[str(i.content), str(i.subject), i.id] for i in mailers]
                # if lang:
                #     mailers = mailers.filter(language=lang)
                camp_dict = {}
                banners_list = []
                lp_links = []
                print('hre',len(mailers), site_ids, siteid, obj.account_manager)
                for i in mailers:
                    camp_id = 0
                    mailerlogobj = Mailerlogs.objects.filter(mailer=i.id, 
                                        fk_campaign__name=i.subject)
                    print('hre', mailerlogobj[0].id)
                    if mailerlogobj:
                        camp_id = mailerlogobj[0].fk_campaign.id
                        print('hre', mailerlogobj[0].fk_campaign.id)
                    if mailerlogobj[0].link:
                        lpages = Lpages.objects.filter(link=mailerlogobj[0].link,siteid=i.siteid)
                        print('lne of lpages',len(lpages))
                        if lpages:
                            for j in lpages:
                                lp_banners = [str(j.link),j.title, j.id]
                                lp_links.append(lp_banners)
                        else:
                            lp_links.append([str(domain),'Home Page', ''])
                        print('lp links',lp_links)
                        banners_list.append([str(mailerlogobj[0].link),i.subject, i.content,
                                i.id, camp_id,obj.id])
                        print('banners list',banners_list)
                if lp_links:
                    resp['lp_links'] = []
                    for i in lp_links:
                        if i not in resp['lp_links']:
                            resp['lp_links'].append(i)
                resp['links'] = banners_list
            else:
                banners_list = []
                tlinks = Tlinks.objects.filter(siteid__in=site_ids,user = obj.account_manager.user)
                if lang:
                    tlinks = tlinks.filter(language=lang)
                if bid:
                    tlinks = tlinks.filter(id=bid)
                for i in tlinks:
                    network_s_obj = NetworkAndSiteMap.objects.filter(siteid=i.siteid)
                    tlinkslogsobj = Tlinkslogs.objects.filter(fk_tlink=i, 
                                        fk_campaign__name=i.title,
                                        fk_campaign__account_manager=obj.account_manager,
                                        fk_campaign__fk_affiliateinfo__isnull=True)
                    if tlinkslogsobj and i.link:
                        camp_id = tlinkslogsobj[0].fk_campaign.id
                    elif i.link:
                        camp_obj, created = Campaign.objects.get_or_create(
                            name=i.title, siteid=i.siteid,
                            account_manager=obj.account_manager, media_type=media_type)
                        camp_obj.save()
                        tlinkslogsobj, created = Tlinkslogs.objects.get_or_create(
                            fk_campaign=camp_obj, fk_tlink=i)
                        tlinkslogsobj.save()
                        camp_id = camp_obj.id
                    if i.link:
                        banners_list.append([str(i.link),i.title, i.id, 
                            str(i.link), camp_id, obj.id])
                    else:
                        banners_list.append(['',i.title, i.id, 
                            '', camp_id, obj.id])
                    # banners_l.append([str(i.link),i.title, i.id, url_t])
                banners = banners_list
                resp['links'] = banners
            resp['bid'] = bid
            resp['status'] = 'success'
    except Exception as e:
        print 'get market', str(e)
        import traceback
        traceback.print_exc()
        resp['message'] = str(e)
    return HttpResponse(json.dumps(resp, indent=4))

@staff_member_required
def marketingmaterials(request):
    """
    function to call affiliate admin default template
    template name :  affiliate/admin/index.html
    """
    return render(request, 'affiliates/admin/marketmaterial.html', locals())
    # return render_to_response('affiliates/admin/marketmaterial.html',
    #                           locals(),
    #                           context_instance=RequestContext(request))

@affiliate_login_required
def marketmaterial(request):
    affID = get_affiliate_session_id(request)
    try:
        obj = Affiliateinfo.objects.select_related('account_manager').get(id=affID)
        request.user.username = obj.username
        request.user.email = obj.email
    except:
        return HttpResponseRedirect(reverse("affiliate_login"))

    resp_dict = {}
    site_name = 'All Websites'
    resp_dict['obj'] = obj
    resp_dict["domain"] = site_name

    sites_obj = get_brand_order(obj)
    category_obj = MediaCategory.objects.all().values('category_name').distinct('siteid')
    resp_dict['media_types'] = dict(Campaign.MEDIA_TYPE)
    sizes_dict = {}
    b_images = BannerImages.objects.filter(user=obj.account_manager.user,siteid__isnull=False).select_related('siteid')
    for _b in b_images:
        image_size = "%sx%s"%(_b.width,_b.height)
        site_m_type = "%s-%s"%(str(_b.siteid.domain), _b.media_type)
        if str(site_m_type) in sizes_dict:
            if image_size not in sizes_dict[str(site_m_type)]:
                sizes_dict[str(site_m_type)].append(image_size)
        else:
            sizes_dict[str(site_m_type)] = [image_size]
    resp_dict['sizes'] = sizes_dict
    resp_dict['sites_obj_list'] = sites_obj
    resp_dict['category_obj_list'] = ''
    resp_dict["message"] = None
    if obj.account_manager:
        resp_dict["partner"] = obj.account_manager.affiliateprogramme.name
        resp_dict["partner_obj"] = obj.account_manager.affiliateprogramme
    return render(request, 'affiliates/marketingmaterial.html', resp_dict)
    # return render_to_response('affiliates/marketingmaterial.html',resp_dict, 
    #                            context_instance=RequestContext(request))


# class Bannertolink(TemplateView):

#     def get(self, request, linkid, bannerid):
#         id = request.session.get('affid')
#         obj = Affiliateinfo.objects.select_related('account_manager').get(id=id)
#         linkobj = Campaign.objects.get(id=linkid)
#         bannerobj = BannerImages.objects.get(id=bannerid)
#         bannername = bannerobj.banner.name
#         link = linkobj.link
#         currentsite = get_current_site(request)
#         currentsite = str(currentsite.domain)
#         bannerlogobj = Bannerlogs.objects.create(fk_campaign=linkobj, fk_bannerimages=bannerobj)
#         bannerlogobj.link = """<a href="%s"><img border="0" alt="" src="http://%s/%s"
#                             height="200" width='300' /></a>""" % ((str(link), str(currentsite), str(bannername)))
#         bannerlogobj.save()
#         return HttpResponseRedirect(reverse('banner', args=(linkid,)))

# class  Bannerlist(TemplateView):
#     def get(self , request):
#         id = request.session.get('affid')
#         obj = Affiliateinfo.objects.select_related('account_manager').get(id=id)
#         bannerobj = Bannerlogs.objects.filter(fk_campaign__fk_affiliateinfo=obj)
#         return render_to_response('affiliates/banner.html',locals())     
#         #return HttpResponseRedirect(reverse('banner',args=(id,)))

def affiliate_player_registrations(request):
    affID = get_affiliate_session_id(request)
    obj = Affiliateinfo.objects.select_related('account_manager').get(id=affID)
    request.user.username = obj.username
    request.user.email = obj.email
    # site_ids_list = get_brand_order(obj)
    campaign_list = Campaign.objects.filter(fk_affiliateinfo=obj)
    net_ids_list = []

    number_of_items = 10000
    nexting_page = 0
    previous_page = 0
    starting_page = 0
    page = 0
    if request.method == "GET":
        custom = "Custom"
        fromdate = request.GET.get("fromdate")
        todate = request.GET.get("todate")
        if not fromdate or not todate:
            custom = None
        campaign_id = None
        starting_page = request.GET.get('start_page')
        nexting_page = request.GET.get('next')
        previous_page = request.GET.get('prev')
        page = request.GET.get('page')
        if not starting_page  and not nexting_page and not previous_page and not page:
            form_data = {}
            request.session['affiliate_player_registrations'] = form_data
        else:
            form_data = request.session.get('affiliate_player_registrations')
            custom = form_data.get("customid")
            fromdate = form_data.get("fromdate")
            todate = form_data.get("todate")
            siteids = form_data.get("siteids")
            campaign_id = form_data.get("campaign_id")
        if not fromdate and not todate:
            custom = "Custom"
            todate = datetime.now().strftime("%m/%d/%Y")
            current_month , current_year =  datetime.now().month, datetime.now().year
            fromdate = "%s/01/%s" % (current_month, current_year )
    else:
        custom = str(request.POST.get("customid"))
        fromdate = str(request.POST.get("fromdate"))
        todate = str(request.POST.get("todate"))
        siteids = request.POST.getlist("siteids")
        campaign_id = request.POST.get('campaign_id')
        if campaign_id == 'all':
            campaign_id = None
        
        form_data = {'fromdate':fromdate, 'todate':todate,'customid':custom, "siteids":siteids,
                     
                     "campaign_id":campaign_id}
        request.session['affiliate_player_registrations'] = form_data
    
    if campaign_id:
        campaign_id = int(campaign_id)
    # filter_networks = [1]
    # if siteids:
    #     filter_sites = [int(i) for i in siteids]
    # else:
    #     filter_sites = [int(i.id) for i in site_ids_list]
    # siteid_list = get_site_ids_data_list(site_ids_list, filter_sites)
    
    date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
    from_date = date_times_dict.get('fromdate')
    to_date = date_times_dict.get('todate')
    
    
    
    query_set = PPPlayerRaw.objects.filter(registrationdate__gte=from_date, 
                              registrationdate__lte=to_date)
    if campaign_id:
        query_set = query_set.filter(tracker=campaign_id)
    # if channel:
    #     device_info_objs = get_selected_channel_devices_info_objs(channel)
    #     query_set = query_set.filter(playerinfo1__device_info_id__in=device_info_objs)
    count = query_set.count()
    start_record, end_record, start_page, \
            end_page, next_page, cur_page, page_count = get_paginate_dimensions(page, starting_page, 
                                                                                nexting_page, previous_page, 
                                                                                count, number_of_items)
    players_objs_list = query_set.order_by('-id')[start_record:end_record]
    
    player_ids_list = [int(i.id) for i in players_objs_list]
    
    header_list = [
            { "title": "Site Id"} ,
           { "title": "User ID"},
           { "title": "Affiliate"} ,
           { "title": "Campaign"} ,
           { "title": "Country"},
           { "title": "Date of Signup"},
           { "title": "Currency"},
           { "title": "Status"},
           { "title": "Ftd"},
           { "title": "Deposit"},
           { "title": "Total Deposit"},
           { "title": "Cashout"},
           { "title": "Total Cashout"},
           { "title": "Bonuses"},
           { "title": "Expired Bonuses"},
           { "title": "Revenue"},
           { "title": "Chargeback"},
           { "title": "Reverse Chargeback"},
           { "title": "SideGames Bets"},
           { "title": "SideGames Wins"},
           { "title": "Jackpot Contribution"}
       ]
    data_list = []
    for i in players_objs_list:
        playerid = int(i.id)
        data_list.append([i.whitelabelid, i.id, "Sticky Slots",
                          i.tracker, i.country,
                          i.registrationdate.strftime("%d-%m-%Y"),
                          i.currency, i.status, i.ftd,
                          i.deposit, i.totaldeposit,
                          i.cashout, i.totalcashout,
                          i.bonuses, i.expiredbonuses, i.revenue,
                          i.chargeback, i.reversechargeback,
                          i.sidegamesbets, i.sidegameswins, i.jackpotcontribution
                          ])

    pagination_div  = makePaginationDiv(start_page, page_count, cur_page, next_page, count)
    
    context_dict = {"data_list":data_list,"header_list":header_list,
                        "from_date":from_date.strftime('%B %d, %Y'),"to_date":to_date.strftime('%B %d, %Y') ,
                        "ui_from_date":datetime_or_date_to_string(from_date),
                        "ui_to_date":datetime_or_date_to_string(to_date),
                       'campaign_id': campaign_id,"pagination_div":pagination_div}
    if request.POST.get("format") == "json":
        context_dict["status"] = "success"
        context_dict['data_list'] = data_list
        context_dict["header_list"] = header_list
        return HttpResponse(json.dumps(context_dict, indent=4))
    context_dict["campaign_list"] = campaign_list
    context_dict['data_list'] = json.dumps(data_list)
    context_dict["header_list"] = json.dumps(header_list)
    # context_dict["channels_list"] = get_channels_list()
    # context_dict["channel"] = channel if channel else "All" 
    return render(request, 'affiliates/reports/player-registrations.html', context_dict)
    # return render_to_response('affiliates/reports/player-registrations.html', context_dict,
    #                               context_instance=RequestContext(request))


def get_tracker_code(request):
    resp_dict = {'status': 'Success'}
    camp_id = request.GET.get("camp_id")
    aff_id = request.GET.get("aff_id")
    lang = request.GET.get("lang", None)
    lpage = request.GET.get("lpage", None)
    imageId = request.GET.get("imageId", None)
    media_type = request.GET.get("media_type")
    dlang = request.GET.get("dlang", '')
    aff_obj = Affiliateinfo.objects.select_related('account_manager').get(id=aff_id)
    linkobj = Campaign.objects.get(id=camp_id)
    got_error, camp_track_map = add_campaign_tracker(aff_obj, linkobj)
    abs_url = request.build_absolute_uri().split('/affiliate/')[0]
    print abs_url, request.build_absolute_uri(), 'absolute url'
    linkurl = get_affiliate_campaign_link(linkobj, aff_obj, {}, abs_url)
    netw_site_obj = NetworkAndSiteMap.objects.get(siteid=linkobj.siteid)
    if not lang:
        lang = dlang
    linkurl += '&lang=' + lang 
    if lpage and imageId:
        lpage = Lpages.objects.get(id=lpage)
        link =  lpage.link
        if media_type !="M":
            bannerobj = BannerImages.objects.get(id=imageId)
            blid = Bannerlogs.objects.get_or_create(link=link,fk_bannerimages=bannerobj, fk_campaign=linkobj)
            if isinstance(blid,tuple):
                linkurl += '&blid=' + str(int(blid[0].id))
        else:
            mailerobj = Mailer.objects.get(id=imageId)
            mlid = Mailerlogs.objects.get_or_create(link=link,mailer=mailerobj, fk_campaign=linkobj)
            if isinstance(mlid,tuple):
                linkurl += '&mlid=' + str(int(mlid[0].id))
    if netw_site_obj.networkid.networkname == 'ProgressPlay':
        linkurl += '&tracker='
    resp_dict['linkurl'] = linkurl + '&track='
    if got_error:
        resp_dict['error_message'] = camp_track_map
        resp_dict['status'] = 'Error'
        return HttpResponse(json.dumps(resp_dict, indent=4))
    return HttpResponse(json.dumps(resp_dict, indent=4))


@affiliate_login_required
def displaytrackers(request):
    context_dict = {}
    sessionid = get_affiliate_session_id(request)
    affID = get_affiliate_session_id(request)
    try:
        obj = Affiliateinfo.objects.select_related('account_manager').get(id=affID)
        request.user.username = obj.username
        request.user.email = obj.email
    except:
        return HttpResponseRedirect(reverse("affiliate_login"))
    campaign_list = Campaign.objects.filter(fk_affiliateinfo=obj)
    net_ids_list = []

    number_of_items = 10000
    nexting_page = 0
    previous_page = 0
    starting_page = 0
    page = 0
    site_ids_list = get_brand_order(obj)
    site_ids = site_ids_list
    format_d = request.POST.get("format")
    domain = None
    site_name = 'All Websites'
    if request.method == "GET":
        custom = "Custom"
        fromdate = request.GET.get("fromdate")
        todate = request.GET.get("todate")
        if not fromdate or not todate:
            custom = None
        campaign_id = None
        starting_page = request.GET.get('start_page')
        nexting_page = request.GET.get('next')
        previous_page = request.GET.get('prev')
        page = request.GET.get('page')
        if not starting_page  and not nexting_page and not previous_page and not page:
            form_data = {}
            request.session['displaytrackers'] = form_data
        else:
            form_data = request.session.get('displaytrackers')
            custom = form_data.get("customid")
            fromdate = form_data.get("fromdate")
            todate = form_data.get("todate")
            siteids = form_data.get("siteids")
        if not fromdate and not todate:
            custom = "Custom"
            todate = datetime.now().strftime("%m/%d/%Y")
            current_month , current_year =  datetime.now().month, datetime.now().year
            fromdate = "%s/01/%s" % (current_month, current_year )
    else:
        custom = str(request.POST.get("customid"))
        fromdate = str(request.POST.get("fromdate"))
        todate = str(request.POST.get("todate"))
        siteids = request.POST.getlist("siteids")
        domain = request.POST.get("sitename")
        
        if domain:
            format_d = 'reload'
        site_ids = site_ids_list
        if domain and domain != 'All':
            site_domain_list = domain.split(',')
            site_ids = site_ids_list.filter(domain__in=site_domain_list)
            site_name = 'Multiple sites.'
            if len(domain.split(',')) <2:
                site_name = site_ids_list.get(domain=site_domain_list[0]).name
        form_data = {'fromdate':fromdate, 'todate':todate,'customid':custom}
        request.session['displaytrackers'] = form_data

    # filter_networks = [1]
    # if siteids:
    #     filter_sites = [int(i) for i in siteids]
    # else:
    #     filter_sites = [int(i.id) for i in site_ids_list]
    # siteid_list = get_site_ids_data_list(site_ids_list, filter_sites)
    
    date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
    from_date = date_times_dict.get('fromdate')
    to_date = date_times_dict.get('todate')
    if obj.account_manager.id == 91:
        from_d_date = datetime.strptime('2019-05-08', '%Y-%m-%d')
        if from_date < from_d_date:
            from_date = from_d_date
        camp_links = Campaign.objects.filter(
                        Q(Q(fk_affiliateinfo=obj) | Q(account_manager=obj.account_manager) | 
                        Q(siteid__in=site_ids)) &
                        Q(created_timestamp__gte=from_date) &
                        Q(created_timestamp__lte=to_date)
                        ).select_related('siteid').order_by('-created_timestamp')
    else:
        camp_links = Campaign.objects.filter(
                            Q(Q(fk_affiliateinfo=obj) | Q(account_manager=obj.account_manager)) & 
                            Q(siteid__in=site_ids) &
                            Q(created_timestamp__gte=from_date) &
                            Q(created_timestamp__lte=to_date)
                            ).select_related('siteid').order_by('-created_timestamp')
    count = camp_links.count()
    start_record, end_record, start_page, \
            end_page, next_page, cur_page, page_count = get_paginate_dimensions(page, starting_page, 
                                                                                nexting_page, previous_page, 
                                                                                count, number_of_items)
    #from_date = date(2012, 01,01)
    #to_date = datetime.now()+timedelta(days=1)
    header_list, data_list = process_campaign_list(obj, from_date, to_date, camp_links, site_ids)


    pagination_div  = makePaginationDiv(start_page, page_count, cur_page, next_page, count)

    context_dict = {"data_list":data_list,"header_list":header_list,
                        "from_date":from_date.strftime('%B %d, %Y'),"to_date":to_date.strftime('%B %d, %Y') ,
                        "ui_from_date":datetime_or_date_to_string(from_date),
                        "ui_to_date":datetime_or_date_to_string(to_date),
                        "pagination_div":pagination_div, 'domain': site_name}
    if format_d == "json":
        context_dict["status"] = "success"
        context_dict['data_list'] = data_list
        context_dict["header_list"] = header_list
        return HttpResponse(json.dumps(context_dict, indent=4))
    context_dict["campaign_list"] = campaign_list
    context_dict['data_list'] = json.dumps(data_list)
    context_dict["header_list"] = json.dumps(header_list)
    if obj.account_manager:
        context_dict["partner"] = obj.account_manager.affiliateprogramme.name
        context_dict["partner_obj"] = obj.account_manager.affiliateprogramme
    # context_dict["channels_list"] = get_channels_list()
    # context_dict["channel"] = channel if channel else "All"
    return render(request, 'affiliates/displaytrackers.html', context_dict)
    # return render_to_response('affiliates/displaytrackers.html', context_dict,
    #                               context_instance=RequestContext(request))


@affiliate_login_required
def get_paymentrpts(request):
    context_dict = {}
    sessionid = get_affiliate_session_id(request)
    affID = get_affiliate_session_id(request)
    try:
        obj = Affiliateinfo.objects.select_related('account_manager').get(id=affID)
        request.user.username = obj.username
        request.user.email = obj.email
    except:
        return HttpResponseRedirect(reverse("affiliate_login"))
    campaign_list = Campaign.objects.filter(fk_affiliateinfo=obj)
    net_ids_list = []

    number_of_items = 10000
    nexting_page = 0
    previous_page = 0
    starting_page = 0
    page = 0
    site_ids_list = get_brand_order(obj)
    site_ids = site_ids_list
    format_d = request.POST.get("format")
    domain = None
    site_name = 'All Websites'
    if request.method == "GET":
        custom = "Custom"
        fromdate = request.GET.get("fromdate")
        todate = request.GET.get("todate")
        if not fromdate or not todate:
            custom = None
        campaign_id = None
        starting_page = request.GET.get('start_page')
        nexting_page = request.GET.get('next')
        previous_page = request.GET.get('prev')
        page = request.GET.get('page')
        device = request.POST.get("device", 'All')
        if not starting_page  and not nexting_page and not previous_page and not page:
            form_data = {}
            request.session['affiliate_statementrpt'] = form_data
        else:
            form_data = request.session.get('affiliate_statementrpt')
            custom = form_data.get("customid")
            fromdate = form_data.get("fromdate")
            todate = form_data.get("todate")
            siteids = form_data.get("siteids")
            campaign_id = form_data.get("campaign_id")
        if not fromdate and not todate:
            custom = "Custom"
            todate = datetime.now().strftime("%m/%d/%Y")
            current_month , current_year =  datetime.now().month, datetime.now().year
            fromdate = "%s/01/%s" % (current_month, current_year )
    else:
        custom = str(request.POST.get("customid"))
        fromdate = str(request.POST.get("fromdate"))
        todate = str(request.POST.get("todate"))
        siteids = request.POST.getlist("siteids")
        campaign_id = request.POST.get('campaign_id')

        domain = request.POST.get("sitename")

        if domain:
            format_d = 'reload'
        site_ids = site_ids_list
        if domain and domain != 'All':
            site_domain_list = domain.split(',')
            site_ids = site_ids_list.filter(domain__in=site_domain_list)
            site_name = 'Multiple sites.'
            if len(domain.split(',')) <2:
                site_name = site_ids_list.get(domain=site_domain_list[0]).name
        
        device = request.POST.get("device", 'All')
        if campaign_id == 'all':
            campaign_id = None
        form_data = {'fromdate':fromdate, 'todate':todate,'customid':custom, "siteids":siteids,
                     "campaign_id":campaign_id}
        request.session['affiliate_statementrpt'] = form_data

    # filter_networks = [1]
    # if siteids:
    #     filter_sites = [int(i) for i in siteids]
    # else:
    #     filter_sites = [int(i.id) for i in site_ids_list]
    # siteid_list = get_site_ids_data_list(site_ids_list, filter_sites)

    date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
    from_date = date_times_dict.get('fromdate')
    to_date = date_times_dict.get('todate')
    count = 0
    start_record, end_record, start_page, \
            end_page, next_page, cur_page, page_count = get_paginate_dimensions(page, starting_page,
                                                                                nexting_page, previous_page,
                                                                                count, number_of_items)
    header_list, data_list = Process_payment_list(obj)


    pagination_div  = makePaginationDiv(start_page, page_count, cur_page, next_page, count)

    context_dict = {"data_list":data_list,"header_list":header_list,
                        "from_date":from_date.strftime('%B %d, %Y'),"to_date":to_date.strftime('%B %d, %Y') ,
                        "ui_from_date":datetime_or_date_to_string(from_date),
                        "ui_to_date":datetime_or_date_to_string(to_date),"pagination_div":pagination_div,
                        'domain': site_name}

    if format_d == "json":
        context_dict["status"] = "success"
        context_dict['data_list'] = data_list
        context_dict["header_list"] = header_list
        return HttpResponse(json.dumps(context_dict, indent=4))
    context_dict['sites_obj_list'] = site_ids_list
    context_dict["campaign_list"] = campaign_list
    context_dict['data_list'] = json.dumps(data_list)
    context_dict["header_list"] = json.dumps(header_list)
    if obj.account_manager:
        context_dict["partner"] = obj.account_manager.affiliateprogramme.name
        context_dict["partner_obj"] = obj.account_manager.affiliateprogramme
    # context_dict = {}
    # sessionid = get_affiliate_session_id(request)
    # affID = get_affiliate_session_id(request)
    # try:
    #     obj = Affiliateinfo.objects.select_related('account_manager').get(id=affID)
    #     request.user.username = obj.username
    #     request.user.email = obj.email
    # except:
    #     return HttpResponseRedirect(reverse("affiliate_login"))
    # campaign_list = Campaign.objects.filter(fk_affiliateinfo=obj)
    # number_of_items = 10000
    # nexting_page = 0
    # previous_page = 0
    # starting_page = 0
    # page = 0
    # search_param = None
    # domain = None
    # site_name = 'All Websites'
    # if request.method == "GET":
    #     custom = "Custom"
    #     starting_page = request.GET.get('start_page')
    #     nexting_page = request.GET.get('next')
    #     previous_page = request.GET.get('prev')
    #     page = request.GET.get('page')
    # else:
    #     search_param = str(request.POST.get("search"))
    #     domain = request.POST.get("sitename")
    # sub_aff_list = Affiliateinfo.objects.filter(superaffiliate=obj)
    # site_ids_list = get_brand_order(obj)
    # site_ids = site_ids_list
    # if domain and domain != 'All':
    #     site_ids = [site_ids_list.get(domain=domain)]
    #     site_name = site_ids_list.get(domain=domain).name
    # # if search_param:
    # #     sub_aff_list = sub_aff_list.filter(username__icontains=search_param)
    # count = 0
    # start_record, end_record, start_page, \
    #         end_page, next_page, cur_page, page_count = get_paginate_dimensions(page, starting_page, 
    #                                                                             nexting_page, previous_page, 
    #                                                                             count, number_of_items)
    # #from_date = date(2012, 01,01)
    # #to_date = datetime.now()+timedelta(days=1)
    # header_list, data_list = Process_payment_list(obj)

    # pagination_div  = makePaginationDiv(start_page, page_count, cur_page, next_page, count)
    
    # context_dict = {"data_list":data_list,"header_list":header_list,
    #                     "pagination_div":pagination_div, 'domain': site_name}
    # if request.POST.get("format") == "json":
    #     context_dict["status"] = "success"
    #     context_dict['data_list'] = data_list
    #     context_dict["header_list"] = header_list
    #     return HttpResponse(json.dumps(context_dict, indent=4))
    # context_dict['data_list'] = json.dumps(data_list)
    # context_dict["header_list"] = json.dumps(header_list)
    # context_dict["channels_list"] = get_channels_list()
    # context_dict["channel"] = channel if channel else "All"
    return render(request, 'affiliates/paymenthistory.html', context_dict)
    # return render_to_response('affiliates/paymenthistory.html', context_dict,
    #                               context_instance=RequestContext(request))


@affiliate_login_required
def get_statementrpt(request):
    context_dict = {}
    sessionid = get_affiliate_session_id(request)
    affID = get_affiliate_session_id(request)
    try:
        obj = Affiliateinfo.objects.select_related('account_manager').get(id=affID)
        request.user.username = obj.username
        request.user.email = obj.email
    except:
        return HttpResponseRedirect(reverse("affiliate_login"))
    campaign_list = Campaign.objects.filter(fk_affiliateinfo=obj)
    net_ids_list = []

    number_of_items = 10000
    nexting_page = 0
    previous_page = 0
    starting_page = 0
    page = 0
    site_ids_list = get_brand_order(obj)
    site_ids = site_ids_list
    format_d = request.POST.get("format")
    domain = None
    site_name = 'All Websites'
    if request.method == "GET":
        custom = "Custom"
        fromdate = request.GET.get("fromdate")
        todate = request.GET.get("todate")
        if not fromdate or not todate:
            custom = None
        campaign_id = None
        starting_page = request.GET.get('start_page')
        nexting_page = request.GET.get('next')
        previous_page = request.GET.get('prev')
        page = request.GET.get('page')
        device = request.POST.get("device", 'All')
        if not starting_page  and not nexting_page and not previous_page and not page:
            form_data = {}
            request.session['affiliate_statementrpt'] = form_data
        else:
            form_data = request.session.get('affiliate_statementrpt')
            custom = form_data.get("customid")
            fromdate = form_data.get("fromdate")
            todate = form_data.get("todate")
            siteids = form_data.get("siteids")
            campaign_id = form_data.get("campaign_id")
        if not fromdate and not todate:
            custom = "Custom"
            todate = datetime.now().strftime("%m/%d/%Y")
            current_month , current_year =  datetime.now().month, datetime.now().year
            fromdate = "%s/01/%s" % (current_month, current_year )
    else:
        custom = str(request.POST.get("customid"))
        fromdate = str(request.POST.get("fromdate"))
        todate = str(request.POST.get("todate"))
        siteids = request.POST.getlist("siteids")
        campaign_id = request.POST.get('campaign_id')

        domain = request.POST.get("sitename")

        if domain:
            format_d = 'reload'
        site_ids = site_ids_list
        if domain and domain != 'All':
            site_domain_list = domain.split(',')
            site_ids = site_ids_list.filter(domain__in=site_domain_list)
            site_name = 'Multiple sites.'
            if len(domain.split(',')) <2:
                site_name = site_ids_list.get(domain=site_domain_list[0]).name
        
        device = request.POST.get("device", 'All')
        if campaign_id == 'all':
            campaign_id = None
        form_data = {'fromdate':fromdate, 'todate':todate,'customid':custom, "siteids":siteids,
                     "campaign_id":campaign_id}
        request.session['affiliate_statementrpt'] = form_data

    # filter_networks = [1]
    # if siteids:
    #     filter_sites = [int(i) for i in siteids]
    # else:
    #     filter_sites = [int(i.id) for i in site_ids_list]
    # siteid_list = get_site_ids_data_list(site_ids_list, filter_sites)

    date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
    from_date = date_times_dict.get('fromdate')
    count = 0
    start_record, end_record, start_page, \
            end_page, next_page, cur_page, page_count = get_paginate_dimensions(page, starting_page,
                                                                                nexting_page, previous_page,
                                                                                count, number_of_items)
    from_date = date(2012, 01,01)
    to_date = datetime.now()+timedelta(days=1)
    header_list, data_list = Process_statement_list(obj)


    pagination_div  = makePaginationDiv(start_page, page_count, cur_page, next_page, count)

    context_dict = {"data_list":data_list,"header_list":header_list,
                        "from_date":from_date.strftime('%B %d, %Y'),"to_date":to_date.strftime('%B %d, %Y') ,
                        "ui_from_date":datetime_or_date_to_string(from_date),
                        "ui_to_date":datetime_or_date_to_string(to_date),"pagination_div":pagination_div,
                        'domain': site_name}

    if format_d == "json":
        context_dict["status"] = "success"
        context_dict['data_list'] = data_list
        context_dict["header_list"] = header_list
        return HttpResponse(json.dumps(context_dict, indent=4))
    context_dict['sites_obj_list'] = site_ids_list
    context_dict["campaign_list"] = campaign_list
    context_dict['data_list'] = json.dumps(data_list)
    context_dict["header_list"] = json.dumps(header_list)
    if obj.account_manager:
        context_dict["partner"] = obj.account_manager.affiliateprogramme.name
        context_dict["partner_obj"] = obj.account_manager.affiliateprogramme
    # context_dict["channels_list"] = get_channels_list()
    # context_dict["channel"] = channel if channel else "All"

    # context_dict = {}
    # sessionid = get_affiliate_session_id(request)
    # affID = get_affiliate_session_id(request)
    # try:
    #     obj = Affiliateinfo.objects.select_related('account_manager').get(id=affID)
    #     request.user.username = obj.username
    #     request.user.email = obj.email
    # except:
    #     return HttpResponseRedirect(reverse("affiliate_login"))
    # campaign_list = Campaign.objects.filter(fk_affiliateinfo=obj)
    # number_of_items = 10000
    # nexting_page = 0
    # previous_page = 0
    # starting_page = 0
    # page = 0
    # search_param = None
    # domain = None
    # if request.method == "GET":
    #     custom = "Custom"
    #     starting_page = request.GET.get('start_page')
    #     nexting_page = request.GET.get('next')
    #     previous_page = request.GET.get('prev')
    #     page = request.GET.get('page')
    # else:
    #     search_param = str(request.POST.get("search"))
    #     domain = request.POST.get("sitename")
    # site_ids_list = get_brand_order(obj)
    # site_ids = site_ids_list
    # site_name = 'All Websites'
    # if domain and domain != 'All':
    #     site_ids = [site_ids_list.get(domain=domain)]
    #     site_name = site_ids_list.get(domain=domain).name
    # count = 0
    # start_record, end_record, start_page, \
    #         end_page, next_page, cur_page, page_count = get_paginate_dimensions(page, starting_page, 
    #                                                                             nexting_page, previous_page, 
    #                                                                             count, number_of_items)
    # #from_date = date(2012, 01,01)
    # #to_date = datetime.now()+timedelta(days=1)
    # header_list, data_list = Process_statement_list(obj)

    # pagination_div  = makePaginationDiv(start_page, page_count, cur_page, next_page, count)
    
    # context_dict = {"data_list":data_list,"header_list":header_list,
    #                     "pagination_div":pagination_div, 'domain': site_name}
    # if request.POST.get("format") == "json":
    #     context_dict["status"] = "success"
    #     context_dict['data_list'] = data_list
    #     context_dict["header_list"] = header_list
    #     return HttpResponse(json.dumps(context_dict, indent=4))
    # context_dict['data_list'] = json.dumps(data_list)
    # context_dict["header_list"] = json.dumps(header_list)
    # context_dict["channels_list"] = get_channels_list()
    # context_dict["channel"] = channel if channel else "All"
    return render(request, 'affiliates/statementlist.html', context_dict)
    # return render_to_response('affiliates/statementlist.html', context_dict,
    #                               context_instance=RequestContext(request))


@affiliate_login_required
def get_sub_affiliate_list(request):
    context_dict = {}
    sessionid = get_affiliate_session_id(request)
    affID = get_affiliate_session_id(request)
    try:
        obj = Affiliateinfo.objects.select_related('account_manager').get(id=affID)
        request.user.username = obj.username
        request.user.email = obj.email
    except:
        return HttpResponseRedirect(reverse("affiliate_login"))
    campaign_list = Campaign.objects.filter(fk_affiliateinfo=obj)
    number_of_items = 10000
    nexting_page = 0
    previous_page = 0
    starting_page = 0
    page = 0
    search_param = None
    domain = None
    if request.method == "GET":
        custom = "Custom"
        starting_page = request.GET.get('start_page')
        nexting_page = request.GET.get('next')
        previous_page = request.GET.get('prev')
        page = request.GET.get('page')
    else:
        search_param = str(request.POST.get("search"))
        domain = request.POST.get("sitename")
    sub_aff_list = Affiliateinfo.objects.filter(superaffiliate=obj)
    site_ids_list = get_brand_order(obj)
    site_ids = site_ids_list
    site_name = 'All Websites'
    if domain and domain != 'All':
        site_domain_list = domain.split(',')
        site_ids = site_ids_list.filter(domain__in=site_domain_list)
        site_name = 'Multiple sites.'
        if len(domain.split(',')) <2:
            site_name = site_ids_list.get(domain=site_domain_list[0]).name
    if search_param:
        sub_aff_list = sub_aff_list.filter(username__icontains=search_param)
    count = sub_aff_list.count()
    start_record, end_record, start_page, \
            end_page, next_page, cur_page, page_count = get_paginate_dimensions(page, starting_page, 
                                                                                nexting_page, previous_page, 
                                                                                count, number_of_items)
    #from_date = date(2012, 01,01)
    #to_date = datetime.now()+timedelta(days=1)
    header_list, data_list = Process_affiliate_list(obj, sub_aff_list)

    pagination_div  = makePaginationDiv(start_page, page_count, cur_page, next_page, count)
    
    context_dict = {"data_list":data_list,"header_list":header_list,
                        "pagination_div":pagination_div, 'domain': site_name}
    if request.POST.get("format") == "json":
        context_dict["status"] = "success"
        context_dict['data_list'] = data_list
        context_dict["header_list"] = header_list
        return HttpResponse(json.dumps(context_dict, indent=4))
    context_dict['data_list'] = json.dumps(data_list)
    context_dict["header_list"] = json.dumps(header_list)
    if obj.account_manager:
        context_dict["partner"] = obj.account_manager.affiliateprogramme.name
        context_dict["partner_obj"] = obj.account_manager.affiliateprogramme
    # context_dict["channels_list"] = get_channels_list()
    # context_dict["channel"] = channel if channel else "All"
    return render(request, 'affiliates/subafflist.html', context_dict)
    # return render_to_response('affiliates/subafflist.html', context_dict,
    #                               context_instance=RequestContext(request))


def get_sub_affiliates(request):
    affID = get_affiliate_session_id(request)
    try:
        obj = Affiliateinfo.objects.select_related('account_manager').get(id=affID)
        request.user.username = obj.username
        request.user.email = obj.email
    except:
        return HttpResponseRedirect(reverse("affiliate_login"))
    search_param = str(request.POST.get("search"))
    sub_aff_list = Affiliateinfo.objects.filter(superaffiliate=obj)
    if search_param:
        sub_aff_list = sub_aff_list.filter(username__icontains=search_param)
    return HttpResponse(json.dumps({'sub_aff_list': sub_aff_list}, indent=4))


@affiliate_login_required
def sub_affiliate_report(request):
    context_dict = {}
    sessionid = get_affiliate_session_id(request)
    affID = get_affiliate_session_id(request)
    try:
        obj = Affiliateinfo.objects.select_related('account_manager').get(id=affID)
        request.user.username = obj.username
        request.user.email = obj.email
    except:
        return HttpResponseRedirect(reverse("affiliate_login"))
    campaign_list = Campaign.objects.filter(fk_affiliateinfo=obj)
    net_ids_list = []

    number_of_items = 10000
    nexting_page = 0
    previous_page = 0
    starting_page = 0
    page = 0
    subaff_id = None
    site_ids_list = get_brand_order(obj)
    site_ids = site_ids_list
    format_d = request.POST.get("format")
    domain = None
    site_name = 'All Websites'
    if request.method == "GET":
        custom = "Custom"
        fromdate = request.GET.get("fromdate")
        todate = request.GET.get("todate")
        if not fromdate or not todate:
            custom = None
        campaign_id = None
        starting_page = request.GET.get('start_page')
        nexting_page = request.GET.get('next')
        previous_page = request.GET.get('prev')
        page = request.GET.get('page')
        if not starting_page  and not nexting_page and not previous_page and not page:
            form_data = {}
            request.session['affiliate_subaffiliateinfo'] = form_data
        else:
            form_data = request.session.get('affiliate_subaffiliateinfo')
            custom = form_data.get("customid")
            fromdate = form_data.get("fromdate")
            todate = form_data.get("todate")
            siteids = form_data.get("siteids")
            campaign_id = form_data.get("campaign_id")
        if not fromdate and not todate:
            custom = "Custom"
            todate = datetime.now().strftime("%m/%d/%Y")
            current_month , current_year =  datetime.now().month, datetime.now().year
            fromdate = "%s/01/%s" % (current_month, current_year )
    else:
        custom = str(request.POST.get("customid"))
        fromdate = str(request.POST.get("fromdate"))
        todate = str(request.POST.get("todate"))
        siteids = request.POST.getlist("siteids")
        campaign_id = request.POST.get('campaign_id')
        subaff_id = request.POST.get('subaff_id')
        if campaign_id == 'all':
            campaign_id = None
        domain = request.POST.get("sitename")

        site_ids_list = get_brand_order(obj)
        
        if domain:
            format_d = 'reload'
        site_ids = site_ids_list
        if domain and domain != 'All':
            site_domain_list = domain.split(',')
            site_ids = site_ids_list.filter(domain__in=site_domain_list)
            site_name = 'Multiple sites.'
            if len(domain.split(',')) <2:
                site_name = site_ids_list.get(domain=site_domain_list[0]).name

        form_data = {'fromdate':fromdate, 'todate':todate,'customid':custom, "siteids":siteids,
                     "campaign_id":campaign_id}
        request.session['affiliate_subaffiliateinfo'] = form_data
    sitefilter = request.POST.getlist("siteid")
    campfilter = request.POST.get("campfilter")
    currencyfilter = request.POST.get("currency", 'euro')
    group_d = str(request.POST.get("groupby", 'brand'))
    if sitefilter:
        site_ids = site_ids.filter(id__in=sitefilter)
    else:
        pass
    country_list = Country.objects.all().values_list('countryname', flat=True).distinct()
    country = request.POST.getlist('country')
    # if country:
    #     country = [country]
    # else:
    #     country = country_list


    date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
    from_date = date_times_dict.get('fromdate')
    to_date = date_times_dict.get('todate')
    print 'domain', domain, site_ids
    sub_aff_list = Affiliateinfo.objects.filter(superaffiliate=obj)
    if subaff_id:
        sub_aff_list = sub_aff_list.filter(id=subaff_id)
    camp_track_links = CampaignTrackerMapping.objects.filter(fk_affiliateinfo__in=sub_aff_list,
            fk_campaign__siteid__in=site_ids).select_related('fk_campaign', 'fk_campaign__siteid')
    count = sub_aff_list.count()
    start_record, end_record, start_page, \
            end_page, next_page, cur_page, page_count = get_paginate_dimensions(page, starting_page,
                                                                                nexting_page, previous_page, 
                                                                                count, number_of_items)
    #from_date = date(2012, 01,01)
    #to_date = datetime.now()+timedelta(days=1)
    header_list, data_list, currency = Process_sub_affiliate_stats(sub_aff_list, camp_track_links, from_date, 
        to_date, site_ids, obj, currencyfilter, group_d, country)


    pagination_div  = makePaginationDiv(start_page, page_count, cur_page, next_page, count)

    context_dict = {"data_list":data_list,"header_list":header_list,
                        "from_date":from_date.strftime('%B %d, %Y'),"to_date":to_date.strftime('%B %d, %Y') ,
                        "ui_from_date":datetime_or_date_to_string(from_date),
                        "ui_to_date":datetime_or_date_to_string(to_date),
                       'campaign_id': campaign_id,"pagination_div":pagination_div, 'domain': site_name,
                        'currency': currency}
    if format_d == "json":
        context_dict["status"] = "success"
        context_dict['data_list'] = data_list
        context_dict["header_list"] = header_list
        return HttpResponse(json.dumps(context_dict, indent=4))
    context_dict["campaign_list"] = campaign_list
    context_dict['siteid'] = sitefilter if sitefilter else []
    context_dict['sites_list'] = [[i.id, i.name] for i in get_brand_order(obj)]
    context_dict['data_list'] = json.dumps(data_list)
    context_dict["header_list"] = json.dumps(header_list)
    context_dict["subaff_list"] = [{'id':i.id, 'username': i.username.replace('vcommsubaff', '')} for i in Affiliateinfo.objects.filter(superaffiliate=obj)]
    context_dict['sites_obj_list'] = site_ids_list
    if obj.account_manager:
        context_dict["partner"] = obj.account_manager.affiliateprogramme.name
        context_dict["partner_obj"] = obj.account_manager.affiliateprogramme
    # context_dict["channel"] = channel if channel else "All"
    return render(request, 'affiliates/subaffiliatereport.html', context_dict)
    # return render_to_response('affiliates/subaffiliatereport.html', context_dict,
    #                               context_instance=RequestContext(request))

@affiliate_login_required
def sub_affiliate_data_report(request):
    context_dict = {}
    sessionid = get_affiliate_session_id(request)
    affID = get_affiliate_session_id(request)
    try:
        obj = Affiliateinfo.objects.select_related('account_manager').get(id=affID)
        request.user.username = obj.username
        request.user.email = obj.email
    except:
        return HttpResponseRedirect(reverse("affiliate_login"))
    campaign_list = Campaign.objects.filter(fk_affiliateinfo=obj)
    net_ids_list = []

    number_of_items = 10000
    nexting_page = 0
    previous_page = 0
    starting_page = 0
    page = 0
    subaff_id = None
    site_ids_list = get_brand_order(obj)
    site_ids = site_ids_list
    format_d = request.POST.get("format")
    domain = None
    site_name = 'All Websites'
    if request.method == "GET":
        custom = "Custom"
        fromdate = request.GET.get("fromdate")
        todate = request.GET.get("todate")
        if not fromdate or not todate:
            custom = None
        campaign_id = None
        starting_page = request.GET.get('start_page')
        nexting_page = request.GET.get('next')
        previous_page = request.GET.get('prev')
        page = request.GET.get('page')
        if not starting_page  and not nexting_page and not previous_page and not page:
            form_data = {}
            request.session['affiliate_subaffiliateinfo'] = form_data
        else:
            form_data = request.session.get('affiliate_subaffiliateinfo')
            custom = form_data.get("customid")
            fromdate = form_data.get("fromdate")
            todate = form_data.get("todate")
            siteids = form_data.get("siteids")
            campaign_id = form_data.get("campaign_id")
        if not fromdate and not todate:
            custom = "Custom"
            todate = datetime.now().strftime("%m/%d/%Y")
            current_month , current_year =  datetime.now().month, datetime.now().year
            fromdate = "%s/01/%s" % (current_month, current_year )
    else:
        custom = str(request.POST.get("customid"))
        fromdate = str(request.POST.get("fromdate"))
        todate = str(request.POST.get("todate"))
        siteids = request.POST.getlist("siteids")
        campaign_id = request.POST.get('campaign_id')
        subaff_id = request.POST.get('subaff_id')
        if campaign_id == 'all':
            campaign_id = None
        domain = request.POST.get("sitename")

        site_ids_list = get_brand_order(obj)
        
        if domain:
            format_d = 'reload'
        site_ids = site_ids_list
        if domain and domain != 'All':
            site_domain_list = domain.split(',')
            site_ids = site_ids_list.filter(domain__in=site_domain_list)
            site_name = 'Multiple sites.'
            if len(domain.split(',')) <2:
                site_name = site_ids_list.get(domain=site_domain_list[0]).name

        form_data = {'fromdate':fromdate, 'todate':todate,'customid':custom, "siteids":siteids,
                     "campaign_id":campaign_id}
        request.session['affiliate_subaffiliateinfo'] = form_data
    if campaign_id:
        campaign_id = int(campaign_id)
    # filter_networks = [1]
    # if siteids:
    #     filter_sites = [int(i) for i in siteids]
    # else:
    #     filter_sites = [int(i.id) for i in site_ids_list]
    # siteid_list = get_site_ids_data_list(site_ids_list, filter_sites)
    sitefilter = request.POST.getlist("siteid")
    if sitefilter:
        site_ids = site_ids.filter(id__in=sitefilter)
    else:
        pass
    date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
    from_date = date_times_dict.get('fromdate')
    to_date = date_times_dict.get('todate')
    sub_aff_list = Affiliateinfo.objects.filter(superaffiliate=obj)
    if subaff_id:
        sub_aff_list = sub_aff_list.filter(id=subaff_id)
    count = sub_aff_list.count()
    start_record, end_record, start_page, \
            end_page, next_page, cur_page, page_count = get_paginate_dimensions(page, starting_page,
                                                                                nexting_page, previous_page, 
                                                                                count, number_of_items)
    #from_date = date(2012, 01,01)
    #to_date = datetime.now()+timedelta(days=1)
    header_list, data_list, currency = Process_sub_affiliate_data_stats(sub_aff_list, from_date, to_date, site_ids)


    pagination_div  = makePaginationDiv(start_page, page_count, cur_page, next_page, count)

    context_dict = {"data_list":data_list,"header_list":header_list,
                        "from_date":from_date.strftime('%B %d, %Y'),"to_date":to_date.strftime('%B %d, %Y') ,
                        "ui_from_date":datetime_or_date_to_string(from_date),
                        "ui_to_date":datetime_or_date_to_string(to_date),
                       'campaign_id': campaign_id,"pagination_div":pagination_div, 'domain': site_name,
                        'currency': currency}
    if format_d == "json":
        context_dict["status"] = "success"
        context_dict['data_list'] = data_list
        context_dict["header_list"] = header_list
        return HttpResponse(json.dumps(context_dict, indent=4))
    context_dict["campaign_list"] = campaign_list
    context_dict['data_list'] = json.dumps(data_list)
    context_dict["header_list"] = json.dumps(header_list)
    context_dict["subaff_list"] = [{'id':i.id, 'username': i.username.replace('vcommsubaff', '')} for i in Affiliateinfo.objects.filter(superaffiliate=obj)]
    context_dict['sites_obj_list'] = site_ids_list
    if obj.account_manager:
        context_dict["partner"] = obj.account_manager.affiliateprogramme.name
        context_dict["partner_obj"] = obj.account_manager.affiliateprogramme
    # context_dict["channel"] = channel if channel else "All"
    return render(request, 'affiliates/subaffiliatedatareport.html', context_dict)


@affiliate_login_required
def viewtrackersinfo(request):
    context_dict = {}
    sessionid = get_affiliate_session_id(request)
    affID = get_affiliate_session_id(request)
    try:
        obj = Affiliateinfo.objects.select_related('account_manager').get(id=affID)
        request.user.username = obj.username
        request.user.email = obj.email
    except:
        return HttpResponseRedirect(reverse("affiliate_login"))
    campaign_list = Campaign.objects.filter(fk_affiliateinfo=obj)
    net_ids_list = []

    number_of_items = 10000
    nexting_page = 0
    previous_page = 0
    starting_page = 0
    page = 0
    site_ids_list = get_brand_order(obj)
    site_ids = site_ids_list
    format_d = request.POST.get("format")
    domain = None
    site_name = 'All Websites'
    if request.method == "GET":
        custom = "Custom"
        fromdate = request.GET.get("fromdate")
        todate = request.GET.get("todate")
        if not fromdate or not todate:
            custom = None
        campaign_id = None
        starting_page = request.GET.get('start_page')
        nexting_page = request.GET.get('next')
        previous_page = request.GET.get('prev')
        page = request.GET.get('page')
        campfilter = None
        sitefilter = None
        group_d = 'campaign'
        if not starting_page  and not nexting_page and not previous_page and not page:
            form_data = {}
            request.session['affiliate_viewtrackersinfo'] = form_data
        else:
            form_data = request.session.get('affiliate_viewtrackersinfo')
            custom = form_data.get("customid")
            fromdate = form_data.get("fromdate")
            todate = form_data.get("todate")
            siteids = form_data.get("siteids")
            campaign_id = form_data.get("campaign_id")
        if not fromdate and not todate:
            custom = "Custom"
            todate = datetime.now().strftime("%m/%d/%Y")
            current_month , current_year =  datetime.now().month, datetime.now().year
            fromdate = "%s/01/%s" % (current_month, current_year )
    else:
        custom = str(request.POST.get("customid"))
        fromdate = str(request.POST.get("fromdate"))
        todate = str(request.POST.get("todate"))
        siteids = request.POST.getlist("siteids")
        sitefilter = request.POST.getlist("siteid")
        campfilter = request.POST.get("campfilter")
        group_d = str(request.POST.get("groupby", 'brand'))
        domain = request.POST.get("sitename")

        if domain:
            format_d = 'reload'
        site_ids = site_ids_list
        if domain and domain != 'All':
            site_domain_list = domain.split(',')
            site_ids = site_ids_list.filter(domain__in=site_domain_list)
            site_name = 'Multiple sites.'
            if len(domain.split(',')) <2:
                site_name = site_ids_list.get(domain=site_domain_list[0]).name
        campaign_id = request.POST.get('campaign_id')
        if campaign_id == 'all':
            campaign_id = None
        
        form_data = {'fromdate':fromdate, 'todate':todate,'customid':custom, "siteids":siteids,
                     
                     "campaign_id":campaign_id}
        request.session['affiliate_viewtrackersinfo'] = form_data
    if campaign_id:
        campaign_id = int(campaign_id)
    # filter_networks = [1]
    # if siteids:
    #     filter_sites = [int(i) for i in siteids]
    # else:
    #     filter_sites = [int(i.id) for i in site_ids_list]
    # siteid_list = get_site_ids_data_list(site_ids_list, filter_sites)
    sitefilter = request.POST.getlist("siteid")
    campfilter = request.POST.get("campfilter")
    currencyfilter = request.POST.get("currency", 'euro')
    group_d = str(request.POST.get("groupby", 'brand'))
    country_list = Country.objects.all().values_list('countryname', flat=True).distinct()
    country = request.POST.getlist('country')
    # if country:
    #     country = [country]
    # else:
    #     country = country_list
    camp_links = Campaign.objects.filter(fk_affiliateinfo=obj).select_related('siteid').\
                        order_by('-id')

    if campfilter:
        camp_links = camp_links.filter(id=campfilter)
        if campfilter:
            campaign_id = int(campfilter)
    if sitefilter:
        site_ids = site_ids.filter(id__in=sitefilter)
    else:
        pass

    date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
    from_date = date_times_dict.get('fromdate')
    to_date = date_times_dict.get('todate')
    print 'domain', domain, site_ids
    aff_objs = list(Affiliateinfo.objects.filter(superaffiliate=obj))
    
    if obj.id == 11:
        aff_objs.append(obj)
        camp_track_links = CampaignTrackerMapping.objects.filter(fk_affiliateinfo__in=aff_objs,
            fk_campaign__siteid__in=site_ids).select_related('fk_campaign', 'fk_campaign__siteid')
    else:
        camp_track_links = CampaignTrackerMapping.objects.filter(fk_affiliateinfo__in=[obj],
            fk_campaign__siteid__in=site_ids).select_related('fk_campaign', 'fk_campaign__siteid')
    count = camp_links.count()
    start_record, end_record, start_page, \
            end_page, next_page, cur_page, page_count = get_paginate_dimensions(page, starting_page,
                                                                                nexting_page, previous_page,
                                                                                count, number_of_items)
    #from_date = date(2012, 01,01)
    #to_date = datetime.now()+timedelta(days=1)
    # if group_d != 'campaign':
    header_list, data_list, currency = group_by_earnings_report(obj, camp_track_links, from_date,
     to_date, site_ids, group_d, currencyfilter, country)
    # else:
    #     header_list, data_list, currency = Process_campaign_stats(obj, from_date, to_date, 
    #         camp_track_links, site_ids, group_d, currencyfilter)


    pagination_div  = makePaginationDiv(start_page, page_count, cur_page, next_page, count)

    context_dict = {"data_list":data_list,"header_list":header_list,
                        "from_date":from_date.strftime('%B %d, %Y'),"to_date":to_date.strftime('%B %d, %Y') ,
                        "ui_from_date":datetime_or_date_to_string(from_date),
                        "ui_to_date":datetime_or_date_to_string(to_date),
                       'campaign_id': campaign_id,"pagination_div":pagination_div, 'domain': site_name,
                       'currency': currency}
    if format_d == "json":
        context_dict["status"] = "success"
        context_dict['data_list'] = data_list
        context_dict["header_list"] = header_list
        return HttpResponse(json.dumps(context_dict, indent=4))
    context_dict['sites_obj_list'] = site_ids_list
    context_dict['siteid'] = sitefilter if sitefilter else []
    context_dict['sites_list'] = [[i.id, i.name] for i in get_brand_order(obj)]
    context_dict["campaign_list"] = campaign_list
    context_dict["country_list"] = country_list
    context_dict['data_list'] = json.dumps(data_list)
    context_dict["header_list"] = json.dumps(header_list)
    if obj.account_manager:
        context_dict["partner"] = obj.account_manager.affiliateprogramme.name
        context_dict["partner_obj"] = obj.account_manager.affiliateprogramme
    # context_dict["channels_list"] = get_channels_list()
    # context_dict["channel"] = channel if channel else "All"
    return render(request, 'affiliates/viewtrackers.html', context_dict)
    # return render_to_response('affiliates/viewtrackers.html', context_dict,
    #                               context_instance=RequestContext(request))


@affiliate_login_required
def playerstatsinfo(request):
    context_dict = {}
    sessionid = get_affiliate_session_id(request)
    affID = get_affiliate_session_id(request)
    try:
        obj = Affiliateinfo.objects.select_related('account_manager').get(id=affID)
        request.user.username = obj.username
        request.user.email = obj.email
    except:
        return HttpResponseRedirect(reverse("affiliate_login"))
    campaign_list = Campaign.objects.filter(fk_affiliateinfo=obj)
    net_ids_list = []

    number_of_items = 10000
    nexting_page = 0
    previous_page = 0
    starting_page = 0
    page = 0
    site_ids_list = get_brand_order(obj)
    site_ids = site_ids_list
    format_d = request.POST.get("format")
    domain = None
    site_name = 'All Websites'
    if request.method == "GET":
        custom = "Custom"
        fromdate = request.GET.get("fromdate")
        todate = request.GET.get("todate")
        if not fromdate or not todate:
            custom = None
        campaign_id = None
        starting_page = request.GET.get('start_page')
        nexting_page = request.GET.get('next')
        previous_page = request.GET.get('prev')
        page = request.GET.get('page')
        if not starting_page  and not nexting_page and not previous_page and not page:
            form_data = {}
            request.session['affiliate_playersinfo'] = form_data
        else:
            form_data = request.session.get('affiliate_playersinfo')
            custom = form_data.get("customid")
            fromdate = form_data.get("fromdate")
            todate = form_data.get("todate")
            siteids = form_data.get("siteids")
            campaign_id = form_data.get("campaign_id")
        if not fromdate and not todate:
            custom = "Custom"
            todate = datetime.now().strftime("%m/%d/%Y")
            current_month , current_year =  datetime.now().month, datetime.now().year
            fromdate = "%s/01/%s" % (current_month, current_year )
    else:
        custom = str(request.POST.get("customid"))
        fromdate = str(request.POST.get("fromdate"))
        todate = str(request.POST.get("todate"))
        siteids = request.POST.getlist("siteids")
        campaign_id = request.POST.get('campaign_id')
        domain = request.POST.get("sitename")

        if domain:
            format_d = 'reload'
        site_ids = site_ids_list
        if domain and domain != 'All':
            site_domain_list = domain.split(',')
            site_ids = site_ids_list.filter(domain__in=site_domain_list)
            site_name = 'Multiple sites.'
            if len(domain.split(',')) <2:
                site_name = site_ids_list.get(domain=site_domain_list[0]).name
        if campaign_id == 'all':
            campaign_id = None

        form_data = {'fromdate':fromdate, 'todate':todate,'customid':custom, "siteids":siteids,
                     "campaign_id":campaign_id}
        request.session['affiliate_playersinfo'] = form_data

    if campaign_id:
        campaign_id = int(campaign_id)
    # filter_networks = [1]
    # if siteids:
    #     filter_sites = [int(i) for i in siteids]
    # else:
    #     filter_sites = [int(i.id) for i in site_ids_list]
    # siteid_list = get_site_ids_data_list(site_ids_list, filter_sites)
    sitefilter = request.POST.getlist("siteid")
    campfilter = request.POST.get("campfilter")
    camp_links = Campaign.objects.filter(fk_affiliateinfo=obj).select_related('siteid').\
                        order_by('-id')
    if campfilter:
        camp_links = camp_links.filter(id=campfilter)
        if campfilter:
            campaign_id = int(campfilter)
    if sitefilter:
        site_ids = site_ids.filter(id__in=sitefilter)
    else:
        pass
    date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
    from_date = date_times_dict.get('fromdate')
    to_date = date_times_dict.get('todate')
    camp_links = Campaign.objects.filter(fk_affiliateinfo=obj, siteid__in=site_ids).select_related('siteid').\
                        order_by('-id')
    aff_objs = list(Affiliateinfo.objects.filter(superaffiliate=obj))
    if obj.id == 11:
        aff_objs.append(obj)
        camp_track_links = CampaignTrackerMapping.objects.filter(fk_affiliateinfo__in=aff_objs,
            fk_campaign__siteid__in=site_ids).select_related('fk_campaign', 'fk_campaign__siteid')
    else:
        camp_track_links = CampaignTrackerMapping.objects.filter(fk_affiliateinfo__in=[obj],
            fk_campaign__siteid__in=site_ids).select_related('fk_campaign', 'fk_campaign__siteid')
    count = camp_links.count()
    start_record, end_record, start_page, \
            end_page, next_page, cur_page, page_count = get_paginate_dimensions(page, starting_page,
                                                                                nexting_page, previous_page,
                                                                                count, number_of_items)
    #from_date = date(2012, 01,01)
    #to_date = datetime.now()+timedelta(days=1)
    header_list, data_list, currency = Process_player_stats(obj, from_date, to_date, camp_track_links, site_ids)


    pagination_div  = makePaginationDiv(start_page, page_count, cur_page, next_page, count)

    context_dict = {"data_list":data_list,"header_list":header_list,
                        "from_date":from_date.strftime('%B %d, %Y'),"to_date":to_date.strftime('%B %d, %Y') ,
                        "ui_from_date":datetime_or_date_to_string(from_date),
                        "ui_to_date":datetime_or_date_to_string(to_date),
                       'campaign_id': campaign_id,"pagination_div":pagination_div, 'domain': site_name,
                       'currency': currency, 'count': len(data_list)}
    if format_d == "json":
        context_dict["status"] = "success"
        context_dict['data_list'] = data_list
        context_dict["header_list"] = header_list
        return HttpResponse(json.dumps(context_dict, indent=4))
    context_dict['sites_obj_list'] = site_ids_list
    context_dict['siteid'] = sitefilter if sitefilter else []
    context_dict['sites_list'] = [[i.id, i.name] for i in get_brand_order(obj)]
    context_dict["campaign_list"] = campaign_list
    context_dict['data_list'] = json.dumps(data_list)
    context_dict["header_list"] = json.dumps(header_list)
    if obj.account_manager:
        context_dict["partner"] = obj.account_manager.affiliateprogramme.name
        context_dict["partner_obj"] = obj.account_manager.affiliateprogramme
    # context_dict["channels_list"] = get_channels_list()
    # context_dict["channel"] = channel if channel else "All"
    return render(request, 'affiliates/playerreportinfo.html', context_dict)
    # return render_to_response('affiliates/playerreportinfo.html', context_dict,
    #                               context_instance=RequestContext(request))


@affiliate_login_required
def devicestatsinfo(request):
    context_dict = {}
    sessionid = get_affiliate_session_id(request)
    affID = get_affiliate_session_id(request)
    try:
        obj = Affiliateinfo.objects.select_related('account_manager').get(id=affID)
        request.user.username = obj.username
        request.user.email = obj.email
    except:
        return HttpResponseRedirect(reverse("affiliate_login"))
    campaign_list = Campaign.objects.filter(fk_affiliateinfo=obj)
    net_ids_list = []

    number_of_items = 10000
    nexting_page = 0
    previous_page = 0
    starting_page = 0
    page = 0
    site_ids_list = get_brand_order(obj)
    site_ids = site_ids_list
    format_d = request.POST.get("format")
    domain = None
    site_name = 'All Websites'
    if request.method == "GET":
        custom = "Custom"
        fromdate = request.GET.get("fromdate")
        todate = request.GET.get("todate")
        if not fromdate or not todate:
            custom = None
        campaign_id = None
        starting_page = request.GET.get('start_page')
        nexting_page = request.GET.get('next')
        previous_page = request.GET.get('prev')
        page = request.GET.get('page')
        device = request.POST.get("device", 'All')
        if not starting_page  and not nexting_page and not previous_page and not page:
            form_data = {}
            request.session['affiliate_deviceinfo'] = form_data
        else:
            form_data = request.session.get('affiliate_deviceinfo')
            custom = form_data.get("customid")
            fromdate = form_data.get("fromdate")
            todate = form_data.get("todate")
            siteids = form_data.get("siteids")
            campaign_id = form_data.get("campaign_id")
        if not fromdate and not todate:
            custom = "Custom"
            todate = datetime.now().strftime("%m/%d/%Y")
            current_month , current_year =  datetime.now().month, datetime.now().year
            fromdate = "%s/01/%s" % (current_month, current_year )
    else:
        custom = str(request.POST.get("customid"))
        fromdate = str(request.POST.get("fromdate"))
        todate = str(request.POST.get("todate"))
        siteids = request.POST.getlist("siteids")
        campaign_id = request.POST.get('campaign_id')

        domain = request.POST.get("sitename")

        if domain:
            format_d = 'reload'
        site_ids = site_ids_list
        if domain and domain != 'All':
            site_domain_list = domain.split(',')
            site_ids = site_ids_list.filter(domain__in=site_domain_list)
            site_name = 'Multiple sites.'
            if len(domain.split(',')) <2:
                site_name = site_ids_list.get(domain=site_domain_list[0]).name
        
        device = request.POST.get("device", 'All')
        if campaign_id == 'all':
            campaign_id = None
        form_data = {'fromdate':fromdate, 'todate':todate,'customid':custom, "siteids":siteids,
                     "campaign_id":campaign_id}
        request.session['affiliate_deviceinfo'] = form_data

    if campaign_id:
        campaign_id = int(campaign_id)
    # filter_networks = [1]
    # if siteids:
    #     filter_sites = [int(i) for i in siteids]
    # else:
    #     filter_sites = [int(i.id) for i in site_ids_list]
    # siteid_list = get_site_ids_data_list(site_ids_list, filter_sites)
    sitefilter = request.POST.getlist("siteid")
    campfilter = request.POST.get("campfilter")
    camp_links = Campaign.objects.filter(fk_affiliateinfo=obj).select_related('siteid').\
                        order_by('-id')
    if campfilter:
        camp_links = camp_links.filter(id=campfilter)
        if campfilter:
            campaign_id = int(campfilter)
    if sitefilter:
        site_ids = site_ids.filter(id__in=sitefilter)
    else:
        pass
    date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
    from_date = date_times_dict.get('fromdate')
    to_date = date_times_dict.get('todate')
    camp_links = Campaign.objects.filter(fk_affiliateinfo=obj, siteid__in=site_ids).select_related('siteid').\
                        order_by('-id')
    aff_objs = list(Affiliateinfo.objects.filter(superaffiliate=obj))
    if obj.id == 11:
        aff_objs.append(obj)
        camp_track_links = CampaignTrackerMapping.objects.filter(fk_affiliateinfo__in=aff_objs,
            fk_campaign__siteid__in=site_ids).select_related('fk_campaign', 'fk_campaign__siteid')
    else:
        camp_track_links = CampaignTrackerMapping.objects.filter(fk_affiliateinfo__in=[obj],
            fk_campaign__siteid__in=site_ids).select_related('fk_campaign', 'fk_campaign__siteid')
    count = camp_links.count()
    start_record, end_record, start_page, \
            end_page, next_page, cur_page, page_count = get_paginate_dimensions(page, starting_page,
                                                                                nexting_page, previous_page,
                                                                                count, number_of_items)
    #from_date = date(2012, 01,01)
    #to_date = datetime.now()+timedelta(days=1)
    header_list, data_list, currency = device_report_stats(obj, from_date, to_date, camp_track_links, device, site_ids)


    pagination_div  = makePaginationDiv(start_page, page_count, cur_page, next_page, count)

    context_dict = {"data_list":data_list,"header_list":header_list,
                        "from_date":from_date.strftime('%B %d, %Y'),"to_date":to_date.strftime('%B %d, %Y') ,
                        "ui_from_date":datetime_or_date_to_string(from_date),
                        "ui_to_date":datetime_or_date_to_string(to_date),
                       'campaign_id': campaign_id,"pagination_div":pagination_div,
                       'devices': ["Mobile", "Web", "All"], 'd':device, 'domain': site_name, 'currency': currency}

    if format_d == "json":
        context_dict["status"] = "success"
        context_dict['data_list'] = data_list
        context_dict["header_list"] = header_list
        return HttpResponse(json.dumps(context_dict, indent=4))
    context_dict['sites_obj_list'] = site_ids_list
    context_dict['siteid'] = sitefilter if sitefilter else []
    context_dict['sites_list'] = [[i.id, i.name] for i in get_brand_order(obj)]
    context_dict["campaign_list"] = campaign_list
    context_dict['data_list'] = json.dumps(data_list)
    context_dict["header_list"] = json.dumps(header_list)
    if obj.account_manager:
        context_dict["partner"] = obj.account_manager.affiliateprogramme.name
        context_dict["partner_obj"] = obj.account_manager.affiliateprogramme
    # context_dict["channels_list"] = get_channels_list()
    # context_dict["channel"] = channel if channel else "All"
    return render(request, 'affiliates/devicereportinfo.html', context_dict)
    # return render_to_response('affiliates/devicereportinfo.html', context_dict,
    #                               context_instance=RequestContext(request))


@affiliate_login_required
def get_gamerpt(request):
    context_dict = {}
    sessionid = get_affiliate_session_id(request)
    affID = get_affiliate_session_id(request)
    try:
        obj = Affiliateinfo.objects.select_related('account_manager').get(id=affID)
        request.user.username = obj.username
        request.user.email = obj.email
    except:
        return HttpResponseRedirect(reverse("affiliate_login"))
    campaign_list = Campaign.objects.filter(fk_affiliateinfo=obj)
    net_ids_list = []

    number_of_items = 10000
    nexting_page = 0
    previous_page = 0
    starting_page = 0
    page = 0
    site_ids_list = get_brand_order(obj)
    site_ids = site_ids_list
    format_d = request.POST.get("format")
    domain = None
    site_name = 'All Websites'
    if request.method == "GET":
        custom = "Custom"
        fromdate = request.GET.get("fromdate")
        todate = request.GET.get("todate")
        if not fromdate or not todate:
            custom = None
        campaign_id = None
        starting_page = request.GET.get('start_page')
        nexting_page = request.GET.get('next')
        previous_page = request.GET.get('prev')
        page = request.GET.get('page')
        device = request.POST.get("device", 'All')
        if not starting_page  and not nexting_page and not previous_page and not page:
            form_data = {}
            request.session['affiliate_gamerpt'] = form_data
        else:
            form_data = request.session.get('affiliate_gamerpt')
            custom = form_data.get("customid")
            fromdate = form_data.get("fromdate")
            todate = form_data.get("todate")
            siteids = form_data.get("siteids")
            campaign_id = form_data.get("campaign_id")
        if not fromdate and not todate:
            custom = "Custom"
            todate = datetime.now().strftime("%m/%d/%Y")
            current_month , current_year =  datetime.now().month, datetime.now().year
            fromdate = "%s/01/%s" % (current_month, current_year )
    else:
        custom = str(request.POST.get("customid"))
        fromdate = str(request.POST.get("fromdate"))
        todate = str(request.POST.get("todate"))
        siteids = request.POST.getlist("siteids")
        campaign_id = request.POST.get('campaign_id')

        domain = request.POST.get("sitename")

        if domain:
            format_d = 'reload'
        site_ids = site_ids_list
        if domain and domain != 'All':
            site_domain_list = domain.split(',')
            site_ids = site_ids_list.filter(domain__in=site_domain_list)
            site_name = 'Multiple sites.'
            if len(domain.split(',')) <2:
                site_name = site_ids_list.get(domain=site_domain_list[0]).name
        
        device = request.POST.get("device", 'All')
        if campaign_id == 'all':
            campaign_id = None
        form_data = {'fromdate':fromdate, 'todate':todate,'customid':custom, "siteids":siteids,
                     "campaign_id":campaign_id}
        request.session['affiliate_gamerpt'] = form_data

    # filter_networks = [1]
    # if siteids:
    #     filter_sites = [int(i) for i in siteids]
    # else:
    #     filter_sites = [int(i.id) for i in site_ids_list]
    # siteid_list = get_site_ids_data_list(site_ids_list, filter_sites)

    date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
    from_date = date_times_dict.get('fromdate')
    to_date = date_times_dict.get('todate')
    count = 0
    start_record, end_record, start_page, \
            end_page, next_page, cur_page, page_count = get_paginate_dimensions(page, starting_page,
                                                                                nexting_page, previous_page,
                                                                                count, number_of_items)
    header_list, data_list = Process_game_list(obj)


    pagination_div  = makePaginationDiv(start_page, page_count, cur_page, next_page, count)

    context_dict = {"data_list":data_list,"header_list":header_list,
                        "from_date":from_date.strftime('%B %d, %Y'),"to_date":to_date.strftime('%B %d, %Y') ,
                        "ui_from_date":datetime_or_date_to_string(from_date),
                        "ui_to_date":datetime_or_date_to_string(to_date),"pagination_div":pagination_div,
                        'domain': site_name}

    if format_d == "json":
        context_dict["status"] = "success"
        context_dict['data_list'] = data_list
        context_dict["header_list"] = header_list
        return HttpResponse(json.dumps(context_dict, indent=4))
    context_dict['sites_obj_list'] = site_ids_list
    context_dict["campaign_list"] = campaign_list
    context_dict['data_list'] = json.dumps(data_list)
    context_dict["header_list"] = json.dumps(header_list)
    if obj.account_manager:
        context_dict["partner"] = obj.account_manager.affiliateprogramme.name
        context_dict["partner_obj"] = obj.account_manager.affiliateprogramme
    # context_dict["channels_list"] = get_channels_list()
    # context_dict["channel"] = channel if channel else "All"




    # context_dict = {}
    # sessionid = get_affiliate_session_id(request)
    # affID = get_affiliate_session_id(request)
    # try:
    #     obj = Affiliateinfo.objects.select_related('account_manager').get(id=affID)
    #     request.user.username = obj.username
    #     request.user.email = obj.email
    # except:
    #     return HttpResponseRedirect(reverse("affiliate_login"))
    # campaign_list = Campaign.objects.filter(fk_affiliateinfo=obj)
    # number_of_items = 10000
    # nexting_page = 0
    # previous_page = 0
    # starting_page = 0
    # page = 0
    # search_param = None
    # domain = None
    # site_name = 'All Websites'
    # if request.method == "GET":
    #     custom = "Custom"
    #     starting_page = request.GET.get('start_page')
    #     nexting_page = request.GET.get('next')
    #     previous_page = request.GET.get('prev')
    #     page = request.GET.get('page')
    # else:
    #     search_param = str(request.POST.get("search"))
    #     domain = request.POST.get("sitename")

    #     if domain:
    #         format_d = 'reload'
    #     if domain and domain != 'All':
    #         site_ids = [site_ids_list.get(domain=domain)]
    #         site_name = site_ids_list.get(domain=domain).name
    # sub_aff_list = Affiliateinfo.objects.filter(superaffiliate=obj)
    # # if search_param:
    # #     sub_aff_list = sub_aff_list.filter(username__icontains=search_param)
    # count = 0
    # start_record, end_record, start_page, \
    #         end_page, next_page, cur_page, page_count = get_paginate_dimensions(page, starting_page, 
    #                                                                             nexting_page, previous_page, 
    #                                                                             count, number_of_items)
    # #from_date = date(2012, 01,01)
    # #to_date = datetime.now()+timedelta(days=1)
    # header_list, data_list = Process_game_list(obj)

    # pagination_div  = makePaginationDiv(start_page, page_count, cur_page, next_page, count)
    
    # context_dict = {"data_list":data_list,"header_list":header_list,
    #                     "pagination_div":pagination_div, 'domain': site_name}
    # if request.POST.get("format") == "json":
    #     context_dict["status"] = "success"
    #     context_dict['data_list'] = data_list
    #     context_dict["header_list"] = header_list
    #     return HttpResponse(json.dumps(context_dict, indent=4))
    # context_dict['data_list'] = json.dumps(data_list)
    # context_dict["header_list"] = json.dumps(header_list)
    # # context_dict["channels_list"] = get_channels_list()
    # # context_dict["channel"] = channel if channel else "All"
    return render(request, 'affiliates/gamereport.html', context_dict)
    # return render_to_response('affiliates/gamereport.html', context_dict,
    #                               context_instance=RequestContext(request))

@affiliate_login_required
def dynamicreportinfo(request):
    context_dict = {}
    sessionid = get_affiliate_session_id(request)
    affID = get_affiliate_session_id(request)
    try:
        obj = Affiliateinfo.objects.select_related('account_manager').get(id=affID)
        request.user.username = obj.username
        request.user.email = obj.email
    except:
        return HttpResponseRedirect(reverse("affiliate_login"))
    campaign_list = Campaign.objects.filter(fk_affiliateinfo=obj)
    net_ids_list = []

    number_of_items = 10000
    nexting_page = 0
    previous_page = 0
    starting_page = 0
    page = 0
    site_ids_list = get_brand_order(obj)
    site_ids = site_ids_list
    format_d = request.POST.get("format")
    domain = None
    site_name = 'All Websites'
    if request.method == "GET":
        custom = "Custom"
        fromdate = request.GET.get("fromdate")
        todate = request.GET.get("todate")
        if not fromdate or not todate:
            custom = None
        campaign_id = None
        starting_page = request.GET.get('start_page')
        nexting_page = request.GET.get('next')
        previous_page = request.GET.get('prev')
        page = request.GET.get('page')
        # device = request.POST.get("device", 'Desktop')
        if not starting_page  and not nexting_page and not previous_page and not page:
            form_data = {}
            request.session['affiliate_campaignsinfo'] = form_data
        else:
            form_data = request.session.get('affiliate_campaignsinfo')
            custom = form_data.get("customid")
            fromdate = form_data.get("fromdate")
            todate = form_data.get("todate")
            siteids = form_data.get("siteids")
            campaign_id = form_data.get("campaign_id")
        if not fromdate and not todate:
            custom = "Custom"
            todate = datetime.now().strftime("%m/%d/%Y")
            current_month , current_year =  datetime.now().month, datetime.now().year
            fromdate = "%s/01/%s" % (current_month, current_year )
    else:
        custom = str(request.POST.get("customid"))
        fromdate = str(request.POST.get("fromdate"))
        todate = str(request.POST.get("todate"))
        siteids = request.POST.getlist("siteids")
        campaign_id = request.POST.get('campaign_id')
        
        domain = request.POST.get("sitename")
        if domain:
            format_d = 'reload'
        site_ids = site_ids_list
        print 'site_ids', site_ids_list
        if domain and domain != 'All':
            site_domain_list = domain.split(',')
            print 'site_ids', site_domain_list
            site_ids = site_ids_list.filter(domain__in=site_domain_list)
            print 'site_ids_l', site_ids
            site_name = 'Multiple sites.'
            if len(domain.split(',')) <2:
                site_name = site_ids_list.get(domain=site_domain_list[0]).name
        
        # device = request.POST.get("device", 'Desktop')
        if campaign_id == 'all':
            campaign_id = None
        form_data = {'fromdate':fromdate, 'todate':todate,'customid':custom, "siteids":siteids,
                     "campaign_id":campaign_id}
        request.session['affiliate_campaignsinfo'] = form_data

    if campaign_id:
        campaign_id = int(campaign_id)
    # filter_networks = [1]
    # if siteids:
    #     filter_sites = [int(i) for i in siteids]
    # else:
    #     filter_sites = [int(i.id) for i in site_ids_list]
    # siteid_list = get_site_ids_data_list(site_ids_list, filter_sites)
    sitefilter = request.POST.getlist("siteid")
    campfilter = request.POST.get("campfilter")
    currencyfilter = request.POST.get("currency", 'euro')
    group_d = str(request.POST.get("groupby", 'dynamic'))
    if sitefilter:
        site_ids = site_ids.filter(id__in=sitefilter)
    else:
        pass

    country_list = Country.objects.all().values_list('countryname', flat=True).distinct()
    country = request.POST.getlist('country')
    # if country:
    #     country = [country]
    # else:
    #     country = country_list

    camp_links = Campaign.objects.filter(fk_affiliateinfo=obj, siteid__in=site_ids).select_related('siteid').\
                        order_by('-id')
    if campfilter:
        camp_links = camp_links.filter(id=campfilter)
        if campfilter:
            campaign_id = int(campfilte0r)
    date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
    from_date = date_times_dict.get('fromdate')
    to_date = date_times_dict.get('todate')
    aff_objs = list(Affiliateinfo.objects.filter(superaffiliate=obj))
    if obj.id == 11:
        aff_objs.append(obj)
        camp_track_links = CampaignTrackerMapping.objects.filter(fk_affiliateinfo__in=aff_objs,
            fk_campaign__siteid__in=site_ids).select_related('fk_campaign', 'fk_campaign__siteid')
    else:
        camp_track_links = CampaignTrackerMapping.objects.filter(fk_affiliateinfo__in=[obj],
            fk_campaign__siteid__in=site_ids).select_related('fk_campaign', 'fk_campaign__siteid')
    count = camp_links.count()
    start_record, end_record, start_page, \
            end_page, next_page, cur_page, page_count = get_paginate_dimensions(page, starting_page,
                                                                                nexting_page, previous_page,
                                                                                count, number_of_items)
    #from_date = date(2012, 01,01)
    #to_date = datetime.now()+timedelta(days=1)
    header_list, data_list, currency = dynamic_reports_stats(obj, from_date, to_date, 
        camp_track_links, site_ids, currencyfilter, group_d, country)


    pagination_div  = makePaginationDiv(start_page, page_count, cur_page, next_page, count)

    context_dict = {"data_list":data_list,"header_list":header_list,
                        "from_date":from_date.strftime('%B %d, %Y'),"to_date":to_date.strftime('%B %d, %Y') ,
                        "ui_from_date":datetime_or_date_to_string(from_date),
                        "ui_to_date":datetime_or_date_to_string(to_date),
                       'campaign_id': campaign_id,"pagination_div":pagination_div,
                       'currency': currency, 'domain': site_name
                       }

    if format_d == "json":
        context_dict["status"] = "success"
        context_dict['data_list'] = data_list
        context_dict["header_list"] = header_list
        return HttpResponse(json.dumps(context_dict, indent=4))
    context_dict['siteid'] = sitefilter if sitefilter else []
    context_dict['sites_list'] = [[i.id, i.name] for i in get_brand_order(obj)]
    context_dict["campaign_list"] = campaign_list
    context_dict["country_list"] = country_list
    context_dict['data_list'] = json.dumps(data_list)
    context_dict["header_list"] = json.dumps(header_list)
    if obj.account_manager:
        context_dict["partner"] = obj.account_manager.affiliateprogramme.name
        context_dict["partner_obj"] = obj.account_manager.affiliateprogramme
    # context_dict["channels_list"] = get_channels_list()
    # context_dict["channel"] = channel if channel else "All"
    return render(request, 'affiliates/dynamic_report.html', context_dict)

@affiliate_login_required
def campaignreportinfo(request):
    context_dict = {}
    sessionid = get_affiliate_session_id(request)
    affID = get_affiliate_session_id(request)
    try:
        obj = Affiliateinfo.objects.select_related('account_manager').get(id=affID)
        request.user.username = obj.username
        request.user.email = obj.email
    except:
        return HttpResponseRedirect(reverse("affiliate_login"))
    campaign_list = Campaign.objects.filter(fk_affiliateinfo=obj)
    net_ids_list = []

    number_of_items = 10000
    nexting_page = 0
    previous_page = 0
    starting_page = 0
    page = 0
    site_ids_list = get_brand_order(obj)
    site_ids = site_ids_list
    format_d = request.POST.get("format")
    domain = None
    site_name = 'All Websites'
    if request.method == "GET":
        custom = "Custom"
        fromdate = request.GET.get("fromdate")
        todate = request.GET.get("todate")
        if not fromdate or not todate:
            custom = None
        campaign_id = None
        starting_page = request.GET.get('start_page')
        nexting_page = request.GET.get('next')
        previous_page = request.GET.get('prev')
        page = request.GET.get('page')
        # device = request.POST.get("device", 'Desktop')
        if not starting_page  and not nexting_page and not previous_page and not page:
            form_data = {}
            request.session['affiliate_campaignsinfo'] = form_data
        else:
            form_data = request.session.get('affiliate_campaignsinfo')
            custom = form_data.get("customid")
            fromdate = form_data.get("fromdate")
            todate = form_data.get("todate")
            siteids = form_data.get("siteids")
            campaign_id = form_data.get("campaign_id")
        if not fromdate and not todate:
            custom = "Custom"
            todate = datetime.now().strftime("%m/%d/%Y")
            current_month , current_year =  datetime.now().month, datetime.now().year
            fromdate = "%s/01/%s" % (current_month, current_year )
    else:
        custom = str(request.POST.get("customid"))
        fromdate = str(request.POST.get("fromdate"))
        todate = str(request.POST.get("todate"))
        siteids = request.POST.getlist("siteids")
        campaign_id = request.POST.get('campaign_id')
        
        domain = request.POST.get("sitename")
        if domain:
            format_d = 'reload'
        site_ids = site_ids_list
        print 'site_ids', site_ids_list
        if domain and domain != 'All':
            site_domain_list = domain.split(',')
            print 'site_ids', site_domain_list
            site_ids = site_ids_list.filter(domain__in=site_domain_list)
            print 'site_ids_l', site_ids
            site_name = 'Multiple sites.'
            if len(domain.split(',')) <2:
                site_name = site_ids_list.get(domain=site_domain_list[0]).name
        
        # device = request.POST.get("device", 'Desktop')
        if campaign_id == 'all':
            campaign_id = None
        form_data = {'fromdate':fromdate, 'todate':todate,'customid':custom, "siteids":siteids,
                     "campaign_id":campaign_id}
        request.session['affiliate_campaignsinfo'] = form_data

    if campaign_id:
        campaign_id = int(campaign_id)
    # filter_networks = [1]
    # if siteids:
    #     filter_sites = [int(i) for i in siteids]
    # else:
    #     filter_sites = [int(i.id) for i in site_ids_list]
    # siteid_list = get_site_ids_data_list(site_ids_list, filter_sites)
    sitefilter = request.POST.getlist("siteid")
    campfilter = request.POST.get("campfilter")
    currencyfilter = request.POST.get("currency", 'euro')
    group_d = str(request.POST.get("groupby", 'campaign'))
    if sitefilter:
        site_ids = site_ids.filter(id__in=sitefilter)
    else:
        pass

    country_list = Country.objects.all().values_list('countryname', flat=True).distinct()
    country = request.POST.getlist('country')
    # if country:
    #     country = [country]
    # else:
    #     country = country_list

    camp_links = Campaign.objects.filter(fk_affiliateinfo=obj, siteid__in=site_ids).select_related('siteid').\
                        order_by('-id')
    if campfilter:
        camp_links = camp_links.filter(id=campfilter)
        if campfilter:
            campaign_id = int(campfilte0r)
    date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
    from_date = date_times_dict.get('fromdate')
    to_date = date_times_dict.get('todate')
    aff_objs = list(Affiliateinfo.objects.filter(superaffiliate=obj))
    if obj.id == 11:
        aff_objs.append(obj)
        camp_track_links = CampaignTrackerMapping.objects.filter(fk_affiliateinfo__in=aff_objs,
            fk_campaign__siteid__in=site_ids).select_related('fk_campaign', 'fk_campaign__siteid')
    else:
        camp_track_links = CampaignTrackerMapping.objects.filter(fk_affiliateinfo__in=[obj],
            fk_campaign__siteid__in=site_ids).select_related('fk_campaign', 'fk_campaign__siteid')
    count = camp_links.count()
    start_record, end_record, start_page, \
            end_page, next_page, cur_page, page_count = get_paginate_dimensions(page, starting_page,
                                                                                nexting_page, previous_page,
                                                                                count, number_of_items)
    #from_date = date(2012, 01,01)
    #to_date = datetime.now()+timedelta(days=1)
    header_list, data_list, currency = campaign_reports_stats(obj, from_date, to_date, 
        camp_track_links, site_ids, currencyfilter, group_d, country)


    pagination_div  = makePaginationDiv(start_page, page_count, cur_page, next_page, count)

    context_dict = {"data_list":data_list,"header_list":header_list,
                        "from_date":from_date.strftime('%B %d, %Y'),"to_date":to_date.strftime('%B %d, %Y') ,
                        "ui_from_date":datetime_or_date_to_string(from_date),
                        "ui_to_date":datetime_or_date_to_string(to_date),
                       'campaign_id': campaign_id,"pagination_div":pagination_div,
                       'currency': currency, 'domain': site_name
                       }

    if format_d == "json":
        context_dict["status"] = "success"
        context_dict['data_list'] = data_list
        context_dict["header_list"] = header_list
        return HttpResponse(json.dumps(context_dict, indent=4))
    context_dict['siteid'] = sitefilter if sitefilter else []
    context_dict['sites_list'] = [[i.id, i.name] for i in get_brand_order(obj)]
    context_dict["campaign_list"] = campaign_list
    context_dict["country_list"] = country_list
    context_dict['data_list'] = json.dumps(data_list)
    context_dict["header_list"] = json.dumps(header_list)
    if obj.account_manager:
        context_dict["partner"] = obj.account_manager.affiliateprogramme.name
        context_dict["partner_obj"] = obj.account_manager.affiliateprogramme
    # context_dict["channels_list"] = get_channels_list()
    # context_dict["channel"] = channel if channel else "All"
    return render(request, 'affiliates/campaignreportinfo.html', context_dict)
    # return render_to_response('affiliates/campaignreportinfo.html', context_dict,
    #                               context_instance=RequestContext(request))


@affiliate_login_required
def viewtrackers(request):
    context_dict = {}
    sessionid = get_affiliate_session_id(request)
    affID = get_affiliate_session_id(request)
    try:
        obj = Affiliateinfo.objects.select_related('account_manager').get(id=affID)
        request.user.username = obj.username
        request.user.email = obj.email
    except:
        return HttpResponseRedirect(reverse("affiliate_login"))
    if sessionid:
        sites_list = get_brand_order(obj)
        request.user.username = obj.username
        request.user.email = obj.email
    
    if request.method == "GET":
        created_tracker = request.session.get("created_tracker")
        if created_tracker:
            context_dict["message"] = "Campaign Created Successfully."
            default_site_id = Campaign.objects.get(name=created_tracker).siteid_id
            del request.session["created_tracker"]
        else:
            default_site_id = sites_list[0].id
        
    else:
        site_name = request.POST.get("sitename")
        site_obj = Site.objects.get(domain=site_name)
        default_site_id = site_obj.id

    header_list, data_list = process_campaing_statics_response(obj,[default_site_id], None, None)


    if request.POST.get("format") == "json":
        context_dict["status"] = "success"
        context_dict['data_list'] = data_list
        context_dict["header_list"] = header_list
        return HttpResponse(json.dumps(context_dict, cls=MyEncoder, indent=4))
    context_dict['data_list'] = json.dumps(data_list, cls=MyEncoder)
    context_dict["header_list"] = json.dumps(header_list,cls=MyEncoder)
    context_dict["sites_obj_list"] = sites_list
    if obj.account_manager:
        context_dict["partner"] = obj.account_manager.affiliateprogramme.name
        context_dict["partner_obj"] = obj.account_manager.affiliateprogramme
    #context_dict["message"] = "Tracker Created Successfully."
    return render(request, 'affiliates/viewtrackers.html', context_dict)
    # return render_to_response('affiliates/viewtrackers.html',context_dict, 
    #                            context_instance=RequestContext(request))


def check_affiliate_name_exist(request):
    result = {"status":"success", "exist":True}
    if request.method == "GET":
        affname = request.GET.get("username")
        try:
            aff_obj = Affiliateinfo.objects.select_related('account_manager').get(username=affname)
        except Affiliateinfo.DoesNotExist:
            result["exist"] = False
    return HttpResponse(json.dumps(result))

def check_affiliate_email_exist(request):
    result = {"status":"success", "exist":True}
    if request.method == "GET":
        email = request.GET.get("email")
        ap_obj = AffiliateProgramme.objects.filter(Q(domain__icontains=str(request.META['HTTP_HOST'])) | Q(
                                    wynta_domain__icontains=str(request.META['HTTP_HOST'])))
        if ap_obj:
            ap_obj = ap_obj[0]
        else:
            ap_obj = None
        if ap_obj:
            acmanager = AccountManager.objects.filter(affiliateprogramme=ap_obj)[0]
        else:
            acmanager = AccountManager.objects.all()[0]
        mailidcount = Affiliateinfo.objects.filter(email=email, account_manager=acmanager).count()
        if mailidcount > 0:
            result["exist"] = True
        else:
            result["exist"] = False
        # try:
        #     aff_obj = Affiliateinfo.objects.select_related('account_manager').get(email=email)
        # except Affiliateinfo.DoesNotExist:
        #     result["exist"] = False
    return HttpResponse(json.dumps(result))

def check_campaign_name_exist(request):
    result = {"status":"success", "exist":True}
    if request.method == "GET":
        linkname = request.GET.get("linkname")
        try:
            camp_obj = Campaign.objects.get(name=linkname)
        except Campaign.DoesNotExist:
            result["exist"] = False
    return HttpResponse(json.dumps(result))


def affiliatepasswordresetemail(obj, tokenobj, from_email):
    """function to send password reset mail to affiliate's email address """
    from userapp.models import MessageTrigger, MessageTemplate
    messagetrigger = MessageTrigger.objects.filter(triggercondition="PASSWORD_RESET", status=True)
    if not messagetrigger:
        logger.critical("Please activate a Template in Egcs to send password reset link")
    if messagetrigger:
        i = messagetrigger[0]
        templateid = str(i.templates_id)
        messagetemplateobject = MessageTemplate.objects.get(id=templateid)
        #subject = messagetemplateobject.subject
        subject = obj.account_manager.affiliateprogramme.name + ": Password reset for your affiliate account"
        content = messagetemplateobject.content
        a = Template(content)
        tokenid = tokenobj.id
        username = obj.id
        domain = obj.account_manager.affiliateprogramme.domain
        aff_prog = obj.account_manager.affiliateprogramme.name
        token = tokenobj.token
        b = Context(locals())
        d = a.render(b)
        # if aff_prog == 'Maxi Affiliates':
        #     maxi_emails([obj.email], subject, d)
        # else:
        #     msg = EmailMessage(subject, d, from_email, [obj.email])
        #     msg.content_subtype = "html"
        #     msg.send()
        check_EmailClient(obj.account_manager, None, subject, d, """%s""" % 'support@wynta.com', [], [obj.email], None, obj,i,None, None)


class AffiliatePasswordReset(TemplateView):
    """ class to password reset .
    it will send rest link to player email id """

    def get(self, request):
        """ to render templates """
        response = ""
        context_dict = {}
        ap_obj = AffiliateProgramme.objects.filter(Q(domain__icontains=str(request.META['HTTP_HOST'])) | Q(
                                    wynta_domain__icontains=str(request.META['HTTP_HOST'])))
        if ap_obj:
            ap_obj = ap_obj[0]
        else:
            if 'fozil' in request.META['HTTP_HOST']:
                ap_obj = AffiliateProgramme.objects.all()[0]
            else:
                ap_obj = AffiliateProgramme.objects.all()[1]
        context_dict['partner'] = ap_obj.name
        context_dict['partner_obj'] = ap_obj
        return render(request, 'affiliates/auth/password_reset_page.html', context_dict)
        # return render_to_response('affiliates/auth/password_reset_page.html',
        #                           locals(), context_instance=RequestContext(request))

    def post(self, request):
        """ to process form data and sending emails
        passwordresetemail(obj, tokenobj)
        GenToken
        link will expire after 3 hrs. minutes = 180
        """
        context_dict = {'status': 'Error'}
        ap_obj = AffiliateProgramme.objects.filter(Q(domain__icontains=str(request.META['HTTP_HOST'])) | Q(
                                    wynta_domain__icontains=str(request.META['HTTP_HOST'])))
        if ap_obj:
            ap_obj = ap_obj[0]
        else:
            if 'fozil' in request.META['HTTP_HOST']:
                ap_obj = AffiliateProgramme.objects.all()[0]
            else:
                ap_obj = AffiliateProgramme.objects.all()[1]
        context_dict['partner'] = ap_obj.name
        context_dict['partner_obj'] = ap_obj
        try:
            mailid = str(request.POST.get('email'))
            mailvalidation = is_valid_email(mailid)
            reset_status = False
            if mailvalidation is False:                        # to check email validation
                context_dict['errors'] = "Please enter valid Email address."
                return render(request, 'affiliates/auth/password_reset_page.html', context_dict)
                # return render_to_response('affiliates/auth/password_reset_page.html',
                #                           json.dumps(context_dict), context_instance=RequestContext(request))
            try:
                obj = Affiliateinfo.objects.select_related('account_manager').get(email=mailid,
                            account_manager__affiliateprogramme=ap_obj)
            except Exception as e:
                print e
                context_dict['errors'] = """This email address is not registered with us. Please check and enter the correct one."""
                return render(request, 'affiliates/auth/password_reset_page.html', context_dict)
                # return render_to_response('affiliates/auth/password_reset_page.html',
                #                           json.dumps(context_dict), context_instance=RequestContext(request))
            def GenToken(length=10, chars=string.letters + string.digits):
                """token code for mail verification"""
                return ''.join([choice(chars) for i in range(length)])
            token = GenToken(length=20, chars=string.letters + string.digits)    # generate token
            partner_obj = AffiliatePasswordResetlogs.objects.filter(affiliateid=obj)
            if partner_obj:
                partner_obj.delete()
            context_dict['acc_manager'] = obj.account_manager.user.email
            tokenobj = AffiliatePasswordResetlogs.objects.create(affiliateid=obj, token=token,
                                                        expiretimestamp=datetime.now() + timedelta(minutes=180))
            affiliatepasswordresetemail(obj, tokenobj, obj.account_manager.user.email)    # email sending
        except Exception as e:
            print 'resend password: ', str(e)
            context_dict['errors'] = """There is some error while sending email. Will send the reset email once we resolve the issue."""
            return render(request, 'affiliates/auth/password_reset_page.html', context_dict)
            # return render_to_response('affiliates/auth/password_reset_page.html',
            #                           json.dumps(context_dict), context_instance=RequestContext(request))
        context_dict['status'] = 'Success'
        return render(request, 'affiliates/auth/password_reset_mail_sent.html', context_dict)
        # return render_to_response('affiliates/auth/password_reset_mail_sent-1.html',
        #                           json.dumps(context_dict), context_instance=RequestContext(request))

def password_reset_mail_sent(request):
    response = ""
    context_dict = {}
    ap_obj = AffiliateProgramme.objects.filter(Q(domain__icontains=str(request.META['HTTP_HOST'])) | Q(
                                    wynta_domain__icontains=str(request.META['HTTP_HOST'])))
    if ap_obj:
        ap_obj = ap_obj[0]
    else:
        if 'fozil' in request.META['HTTP_HOST']:
            ap_obj = AffiliateProgramme.objects.all()[0]
        else:
            ap_obj = AffiliateProgramme.objects.all()[1]
    context_dict['partner'] = ap_obj.name
    context_dict['partner_obj'] = ap_obj
    return render(request, 'affiliates/auth/password_reset_mail_sent-1.html', locals())
    # return render_to_response('affiliates/auth/password_reset_mail_sent-1.',
    #                               locals(), context_instance=RequestContext(request))


class AffiliatePasswordResetAction(TemplateView):
    """
    This class will come in action when
    player will hit password reset link
    """
    def get(self, request, username, token, expireid):
        """
        process link url and check validation of url based on
        PasswordResetlogs model
        """
        context_dict = {}
        ap_obj = AffiliateProgramme.objects.filter(Q(domain__icontains=str(request.META['HTTP_HOST'])) | Q(
                                    wynta_domain__icontains=str(request.META['HTTP_HOST'])))
        if ap_obj:
            ap_obj = ap_obj[0]
        else:
            if 'fozil' in request.META['HTTP_HOST']:
                ap_obj = AffiliateProgramme.objects.all()[0]
            else:
                ap_obj = AffiliateProgramme.objects.all()[1]
        context_dict['partner'] = ap_obj.name
        context_dict['partner_obj'] = ap_obj
        try:
            obj = Affiliateinfo.objects.select_related('account_manager').get(id=username)
        except:
            return HttpResponse("Invalid link")
        try:
            tokenobj = AffiliatePasswordResetlogs.objects.get(id=expireid,
                                                     affiliateid=obj, token=token
                                                     )
        except:
            context_dict['error'] = True
            context_dict['message'] = "Invalid link."
            context_dict['status'] = 'Error'
            return render(request, 'affiliates/auth/login-1.html', context_dict)
            # return render_to_response('affiliates/auth/login-1.html',
            #                       locals(),
            #                       context_instance=RequestContext(request))
        expiretime = tokenobj.expiretimestamp
        context_dict['email'] = obj.email
        if expiretime < datetime.now():
            context_dict['error'] = True
            context_dict['message'] = "Link is being expired."
            context_dict['status'] = 'Error'
            return render(request, 'affiliates/auth/login-1.html', context_dict)
            # return render_to_response('affiliates/auth/login-1.html',
            #                       locals(),
            #                       context_instance=RequestContext(request))
        return render(request, 'affiliates/auth/password_reset_action-1.html', context_dict)
        # return render_to_response('affiliates/auth/password_reset_action.html',
        #                           locals(),
        #                           context_instance=RequestContext(request))

    def post(self, request, username, token, expireid):
        """ player can submit and process password change form """
        context_dict = {}
        ap_obj = AffiliateProgramme.objects.filter(Q(domain__icontains=str(request.META['HTTP_HOST'])) | Q(
                                    wynta_domain__icontains=str(request.META['HTTP_HOST'])))
        if ap_obj:
            ap_obj = ap_obj[0]
        else:
            if 'fozil' in request.META['HTTP_HOST']:
                ap_obj = AffiliateProgramme.objects.all()[0]
            else:
                ap_obj = AffiliateProgramme.objects.all()[1]
        context_dict['partner'] = ap_obj.name
        context_dict['partner_obj'] = ap_obj
        try:
            obj = Affiliateinfo.objects.select_related('account_manager').get(id=username)
        except:
            return HttpResponse("Invalid link")
        try:
            tokenobj = AffiliatePasswordResetlogs.objects.get(id=expireid,
                                                     affiliateid=obj, token=token
                                                     )
        except:
            errors = "Link is not valid now.you have already used this link"
            return render(request, 'affiliates/auth/login-1.html', context_dict)
            # return render_to_response('affiliates/auth/login-1.html',
            #                       locals(),
            #                       context_instance=RequestContext(request))
        password = request.POST.get('password')
        cpassword = request.POST.get('cpassword')
        if password == "" or cpassword == "":
            context_dict['errors'] = "Password fields can not be blank"
            return render(request, 'affiliates/auth/password_reset_action-1.html', context_dict)
            # return render_to_response('affiliates/auth/password_reset_action-1.html',
            #                           locals(),
            #                           context_instance=RequestContext(request))
        if password != cpassword:
            context_dict['errors'] = "Password fields don't match."
            return render(request, 'affiliates/auth/password_reset_action-1.html', context_dict)
            # return render_to_response('affiliates/auth/password_reset_action-1.html',
            #                           locals(),
            #                           context_instance=RequestContext(request))

        if len(cpassword) < 5 or len(cpassword) > 15:
            context_dict['errors'] = "Password length should be between 5 characters to 15 characters."
            return render(request, 'affiliates/auth/password_reset_action-1.html', locals())
            # return render_to_response('affiliates/auth/password_reset_action-1.html',
            #                           locals(),
            #                           context_instance=RequestContext(request))
        password = enc_password(cpassword)   # convert password into hash code
        obj.password = password
        obj.save()
        tokenobj.delete()
        context_dict['errors'] = "Your Password Changed successfully.Please Login to continue."
        request.session["aff_pwd_reset_done"] = True
        return HttpResponseRedirect(reverse("affiliate_login"))

"""
def createaffiliatelink(request):
    ##To create affiliate links at website
    sessionid = request.session.get('affid')
    currentsite = get_current_site(request)
    currentsite = str(currentsite.domain)
    linkname = str(request.POST.get('linkname'))
    try:
        obj = Affiliateinfo.objects.select_related('account_manager').get(id=sessionid)
    except:
        pass
    linkobj = Campaign.objects.create(fk_affiliateinfo=obj)
    linkid = str(linkobj.id)
    affiliateid = str(linkobj.fk_affiliateinfo.id)
    try:
        linkurl = 'http://'+currentsite+'/affiliateregistration/' + affiliateid+'/' + linkid
    except:
        print sys.exc_info()
    linkobj.link = linkurl
    linkobj.name = linkname
    linkobj.save()
    promotion = 'link'
    links = Campaign.objects.filter(fk_affiliateinfo=obj).order_by('-id')
    return render_to_response('affiliates/index.html', locals(), context_instance=RequestContext(request))
"""

def authenticate_user(username, password):
    context_dict = {}
    try:
        #siteid = get_current_site(request)
        obj = Affiliateinfo.objects.select_related('account_manager').get(username=username)
    except Affiliateinfo.DoesNotExist:
        context_dict['Error'] = "The username or password entered is incorrect."
        context_dict['status'] = 'Error'
        return context_dict
    if obj.status != 'Approved':
        context_dict['Error'] = "Your Account is Not yet Approved or Deactivated, please contact to admin"
        context_dict['status'] = 'Error'
        return context_dict
    dbpassword = obj.password
    '''hash function password hash function generation'''
    a = dbpassword.split('$')
    hashdb = str(a[2])
    salt = str(a[1])
    usrhash = get_hexdigest(a[0], a[1], password)
    if hashdb == usrhash:
        if obj.status != 'Approved':
            context_dict['Error'] = "Your account is not yet approved."
            context_dict['status'] = 'Error'
            return context_dict
        context_dict['affid'] = str(obj.id)
        #request.session['affiliate_session_id'] = str(obj.id)
        context_dict['status'] = 'Success'
    else:
        context_dict['Error'] = "The password entered is incorrect."
        context_dict['status'] = 'Error'
    return context_dict


class Signin(TemplateView):

    def get(self, request):
        context_dict = {}
        id = request.session.get('affid')
        if id:
            return HttpResponseRedirect(reverse('affiliate'),)
        ap_obj = AffiliateProgramme.objects.filter(Q(domain__icontains=str(request.META['HTTP_HOST'])) | Q(
                                    wynta_domain__icontains=str(request.META['HTTP_HOST'])))
        if ap_obj:
            ap_obj = ap_obj[0]
        elif len(ap_obj) == 0:
            return render(request, 'siteinvalid.html', context_dict)
        else:
            if 'fozil' in request.META['HTTP_HOST']:
                ap_obj = AffiliateProgramme.objects.all()[0]
            else:
                ap_obj = AffiliateProgramme.objects.all()[1]
        context_dict['partner'] = ap_obj.name
        context_dict['partner_obj'] = ap_obj
        pwd_reset_done = request.session.get("aff_pwd_reset_done")
        aborted = request.session.get("Aborted")
        if aborted:
            context_dict['error'] = True
            context_dict['message'] = "Your affiliate programme is aborted. Please contact your account manager for updates."
            del request.session["Aborted"]
        if pwd_reset_done:
            context_dict['message'] = "Your password has been changed successfully. Please log in to continue."
            del request.session["aff_pwd_reset_done"]
        return render(request, 'affiliates/auth/login-1.html', context_dict)
        # return render_to_response('affiliates/auth/login-1.html', context_dict, 
        #                           context_instance=RequestContext(request))

    def save_account(self,request,username, aff_id, programme):
        ipaddress = request.META['REMOTE_ADDR']
        domain  = str(request.META['HTTP_HOST'])
        account_obj = Onelogin.objects.filter(ipaddress=ipaddress)
        account = []
        if account_obj:
            if account_obj[0].account:
                account = account_obj[0].account
                account.append([username, domain, aff_id, programme])  
                if account:
                    account_obj = Onelogin.objects.filter(id=account_obj[0].id).update(account = account, timestamp=datetime.now())
            else:
                account_obj = Onelogin.objects.filter(id=account_obj[0].id).update(account = [[username, domain, aff_id, programme]], timestamp=datetime.now())
        else:
            account = [[username, domain, aff_id, programme]]
            print('account created', account)
            account_obj = Onelogin.objects.create(account = account, ipaddress = ipaddress,timestamp=datetime.now())
            account_obj.save()


    def post(self, request):
        context_dict = {}
        # ap_obj = AffiliateProgramme.objects.filter(Q(domain__icontains=str(request.META['HTTP_HOST'])) | Q(
        # if ap_obj:
        #     ap_obj = ap_obj[0]
        # else:
        #     return render(request, 'siteinvalid.html', context_dict)
        # context_dict['partner'] = ap_obj.name
        # context_dict['partner_obj'] = ap_obj
        ap_obj = AffiliateProgramme.objects.filter(Q(domain__icontains=str(request.META['HTTP_HOST'])) | Q(
                                    wynta_domain__icontains=str(request.META['HTTP_HOST'])))
        if ap_obj:
            ap_obj = ap_obj[0]
            context_dict['partner'] = ap_obj.name
            context_dict['partner_obj'] = ap_obj
        username = str(request.POST.get('username'))
        password = request.POST.get('passwd')
        try:
            #siteid = get_current_site(request)
            obj = Affiliateinfo.objects.select_related(
                            'account_manager').get(email=username
                            ,account_manager__affiliateprogramme=ap_obj)
        except Affiliateinfo.DoesNotExist:
            error_log()
            context_dict['error'] = True
            context_dict['message'] = "The email entered is incorrect."
            context_dict['status'] = 'Error'
            return render(request, 'affiliates/auth/login-1.html', context_dict) 
            # return HttpResponse(json.dumps(context_dict), content_type='application/json')
        if obj.status != 'Approved':
            context_dict['error'] = True
            context_dict['message'] = "Your account has either not been approved yet or has been deactivated; please contact admin!"
            context_dict['status'] = 'Error'
            return render(request, 'affiliates/auth/login-1.html', context_dict) 
            # return HttpResponse(json.dumps(context_dict), content_type='application/json')
        dbpassword = obj.password
        '''hash function password hash function generation'''
        signinpass = enc_password(password)
        a = dbpassword.split('$')
        hashdb = str(a[2])
        salt = str(a[1])
        usrhash = get_hexdigest(a[0], a[1], password)
        print 'dbpassword', hashdb
        print 'ourpassword', password, usrhash
        if usrhash == hashdb:
            if obj.status != 'Approved':
                context_dict['error'] = True
                context_dict['status'] = 'Error'
                context_dict['message'] = "Your account has either not been approved yet or has been deactivated; please contact admin!"
                return render(request, 'affiliates/auth/login-1.html', context_dict) 
                # return HttpResponse(json.dumps(context_dict), content_type='application/json')
            request.session['affid'] = str(obj.id)
            # used for affiliate referral choice
            request.session['affRefChoice'] = obj.getAffRefChoice
            request.session['affiliate_session_id'] = str(obj.id)
            context_dict['status'] = 'Success'
            obj.lastlogin = datetime.now()
            obj.save()
            self.save_account(request, username, str(obj.id), obj.account_manager.affiliateprogramme.name)
            return HttpResponseRedirect(reverse('affiliate_home'),)
            # return render(request, 'affiliates/auth/login-1.html', context_dict) 
            # return HttpResponse(json.dumps(context_dict), content_type='application/json')
        else:
            context_dict['error'] = True
            context_dict['message'] = "The password entered is incorrect."
            context_dict['status'] = 'Error'
            return render(request, 'affiliates/auth/login-1.html', context_dict) 
            # return HttpResponse(json.dumps(context_dict), content_type='application/json')

@csrf_exempt
def one_login(request):
    if request.method == 'POST':
        try:
            data = json.loads(request.body)
            request.session['affid'] = data.get('affid', None)
            # used for affiliate referral choice
            request.session['affiliate_session_id'] = data.get('affiliate_session_id', None)
            if request.session['affid']:
                obj = Affiliateinfo.objects.get(id=request.session['affid'])
                obj.lastlogin = datetime.now()
                request.session['affRefChoice'] = obj.getAffRefChoice
                obj.save()
            return HttpResponse(json.dumps({'status': 'success'}))
        except Exception as e:
            print('error in one login',e)
            return HttpResponse(json.dumps({'status': 'error'}))

@csrf_exempt
def get_account_ip(request):
    if request.method == 'GET':
        try:
           ipaddress = request.META['REMOTE_ADDR']
           accounts = Onelogin.objects.filter(ipaddress=ipaddress)
           accounts_list = []
           if accounts:
               exist = None
               if accounts[0].account:
                    accounts_list = accounts[0].account
                    for account in accounts[0].account:
                        if account[1] == str(request.META['HTTP_HOST']):
                            exist = True
                    if not exist:
                        accounts_list = []
           return HttpResponse(json.dumps({'account': accounts_list}))     
        except Exception as e:
            print('error in get account',e)
            return HttpResponse(json.dumps({'status': 'error'}))

@csrf_exempt
def remove_account(request):
    if request.method == 'POST':
        try:
            data = json.loads(request.body)
            ipaddress = request.META['REMOTE_ADDR']
            accounts = Onelogin.objects.filter(ipaddress=ipaddress)
            if accounts:
                accounts_list = accounts[0].account
                if data in accounts_list:
                    accounts_list.remove(data)
                    updatedAccount = Onelogin.objects.filter(id=accounts[0].id).update(account=accounts_list)
                    if updatedAccount:
                        return HttpResponse(json.dumps({'account': accounts_list}))
        except Exception as e:
            print('error in get account',e)
            return HttpResponse(json.dumps({'status': 'error'}))
                     
from django.views.decorators.csrf import csrf_exempt
import base64

def create_sub_affiliate(subname, subid, admin, site, siteid):
    try:
        obj_list = VcommFPAffiliateMapping.objects.filter(vcommsubid=subid)
        if site:
            siteobj = Site.objects.get(domain=site)
        else:
            siteobj = Site.objects.all()[0]
        aff_list = Affiliateinfo.objects.filter(username=subname + str(subid), siteid=siteobj)
        if len(obj_list) == 0 and len(aff_list) == 0:
            aff_obj = Affiliateinfo.objects.create(username=subname + str(subid), password=admin.password, contactname=subname,
                                                       companyname=admin.companyname, email=subname + str(subid) + '@' + site,
                                                       address=admin.address, city=admin.city,
                                                       state=admin.state, country=admin.country, ipaddress=admin.ipaddress,
                                                       registeredon=datetime.now(),
                                                       postcode=admin.postcode, telephone=admin.telephone,
                                                       dob=admin.dob)
            if site:
                siteobj = Site.objects.get(domain=site)
            else:
                siteobj = Site.objects.all()[0]
            networkid_obj = Network.objects.all()
            for nt in networkid_obj:
                aff_obj.networkid.add(nt)
            aff_obj.siteid.add(siteobj)
            aff_obj.superaffiliate = admin
            aff_obj.save()
            vcomm_aff_obj = VcommFPAffiliateMapping.objects.create(vcommsubid=subid,
                                                    vcommsubname=subname,
                                                    affiliateid=aff_obj)
            vcomm_aff_obj.save()
            aff_comm_obj = Affiliatecommission.objects.create(fk_affiliateinfo=aff_obj,
                                            revshare=True,
                                            revsharepercent=35)
            aff_comm_obj.save()
        elif len(obj_list) == 0 and len(aff_list) > 0:
            aff_obj = aff_list[0]
            vcomm_aff_obj = VcommFPAffiliateMapping.objects.create(vcommsubid=subid,
                                                    vcommsubname=subname,
                                                    affiliateid=aff_obj)
            vcomm_aff_obj.save()
            if site:
                if Site.objects.get(domain=site) not in aff_obj.siteid.all():
                    siteobj = Site.objects.get(domain=site)
                    aff_obj.siteid.add(siteobj)
            else:
                aff_obj.superaffiliate = admin
                aff_obj.save()
            networkid_obj = Network.objects.all()
            for nt in networkid_obj:
                aff_obj.networkid.add(nt)
        elif len(obj_list) > 0 and len(aff_list) == 0:
            aff_obj = Affiliateinfo.objects.filter(username=subname + str(subid))
            if not aff_obj:
                aff_obj = Affiliateinfo.objects.create(username=subname + str(subid), password=admin.password, contactname=subname,
                                                           companyname=admin.companyname, email=subname + str(subid) + '@' + site,
                                                           address=admin.address, city=admin.city,
                                                           state=admin.state, country=admin.country, ipaddress=admin.ipaddress,
                                                           registeredon=datetime.now(),
                                                           postcode=admin.postcode, telephone=admin.telephone,
                                                           dob=admin.dob, status='Approved')
                networkid_obj = Network.objects.all()
                for nt in networkid_obj:
                    aff_obj.networkid.add(nt)
                aff_obj.superaffiliate = admin
            else:
                aff_obj = aff_obj[0]
            if site:
                siteobj = Site.objects.get(domain=site)
            else:
                siteobj = Site.objects.all()[0]
            aff_obj.siteid.add(siteobj)
            aff_obj.save()
            aff_comm_obj = Affiliatecommission.objects.create(fk_affiliateinfo=aff_obj,
                                            revshare=True,
                                            revsharepercent=35)
            aff_comm_obj.save()
        elif len(obj_list) > 0 and len(aff_list) > 0:
            aff_obj = obj_list[0].affiliateid
            if site:
                if Site.objects.get(domain=site) not in aff_obj.siteid.all():
                    siteobj = Site.objects.get(domain=site)
                    aff_obj.siteid.add(siteobj)
            else:
                aff_obj.superaffiliate = admin
                aff_obj.save()
            networkid_obj = Network.objects.all()
            for nt in networkid_obj:
                aff_obj.networkid.add(nt)
    except Exception as e:
        print 'track link', e
        return {'status':'Error', 'Error': 'Error in creating tracking link'}
    return {'status':'Success', 'aff_obj': aff_obj}

def postback_click_store(request, camp_obj, camp_map_obj, netw_site_obj):
    unique_code = uuid.uuid4().hex
    _d = datetime.now().date()
    if request.user_agent.is_mobile:
        platform = 'Mobile'
    elif request.user_agent.is_tablet:
        platform = 'Tablet'
    elif request.user_agent.is_pc:
        platform = 'Web'
    else:
        platform = 'Bot'
    info_list = get_info_from_ip(request)
    postback_adv, created = PostBackAdvanced.objects.get_or_create(date=_d,
                        siteid = camp_obj.siteid,
                        fk_campaign = camp_map_obj,
                        networkid = netw_site_obj.networkid
                        )
    postback_adv.clicks = postback_adv.clicks + 1 if postback_adv.clicks else 1
    postback_adv.save()
    camp_click_obj, created = CampaignClicksCount.objects.get_or_create(date=_d,
                        fk_campaign = camp_map_obj,
                        platform = platform,
                        country = info_list[0]
                        )
    camp_click_obj.clicks = camp_click_obj.clicks + 1 if camp_click_obj.clicks else 1
    camp_click_obj.save()
    postback_clicks = PostBackClicks.objects.create(
                        siteid = camp_obj.siteid,
                        fk_campaign = camp_map_obj,
                        networkid = netw_site_obj.networkid,
                        clickid=unique_code,
                        ip_address=info_list[2],
                        country = info_list[0],
                        state = info_list[1],
                        platform = platform
                        )
    postback_clicks.save()
    url_d = 'https://' + camp_map_obj.url + '&clickid='+ str(unique_code)
    return url_d


@csrf_exempt
def vcommregister(request):
    resp_dict = {}
    try:
        #encoded = request.META['HTTP_AUTHORIZATION']
        #data = base64.b64decode(encoded.split(' ')[1]).split(':')
        #username = data[0]
        #password = data[1]
        #auth_resp = authenticate_user(username, password)
        #if auth_resp['status'] == 'Error':
        #    return HttpResponse(json.dumps(auth_resp), content_type='application/json')
        affid = int(request.POST.get('affid')) if request.POST.get('affid') else None
        subaffid = int(request.POST.get('subaffid')) if request.POST.get('subaffid') else None
        subaffname = request.POST.get('subaffname')
        site = request.POST.get('site')
        siteid = request.POST.get('siteid')
        if not subaffid and not subaffname:
            affid = int(request.GET.get('affid')) if request.GET.get('affid') else None
            subaffid = request.GET.get('subaffid') if request.GET.get('subaffid') else None
            subaffname = request.GET.get('subaffname')
            site = request.GET.get('site')
            siteid = request.GET.get('siteid')
        if siteid:
            try:
                site = Site.objects.get(id=int(siteid)).domain
                site_obj = Site.objects.get(id=int(siteid))
            except:
                return HttpResponse(json.dumps({'status':'Error',
                                'message': 'siteid is not valid.'}),
                                content_type='application/json')
        else:
            siteid = '1'
        if not site:
            site = 'www.stickyslots.com'
            site_obj = Site.objects.get(domain='www.stickyslots.com')
        if not subaffname or len(subaffname) < 5:
            subaffname = 'vcommsubaff'
        if not affid or not subaffid:
            return HttpResponse(json.dumps({'status':'Error',
                            'message': 'affid and subaffid are mandatory parameters.'}),
                            content_type='application/json')

        admin_aff_obj = Affiliateinfo.objects.select_related('account_manager').get(id=affid)
        sub_aff_obj = create_sub_affiliate(subaffname, subaffid, admin_aff_obj, site, str(siteid))

        if sub_aff_obj['status'] == 'Error':
            return HttpResponse(json.dumps(sub_aff_obj), content_type='application/json')
        linkobj = Campaign.objects.filter(fk_affiliateinfo=sub_aff_obj['aff_obj'], name=subaffname+str(subaffid) + str(siteid))
        _d = datetime.now().date()
        if len(linkobj)>0:
            netw_site_obj = NetworkAndSiteMap.objects.get(siteid=linkobj[0].siteid)
            content_type = netw_site_obj.networkid.connect_type
            resp_dict["tracking-link"] = linkobj[0].link
            resp_dict["subaffid"] = subaffid
            camp_track_map = CampaignTrackerMapping.objects.get(fk_campaign=linkobj[0])
            if content_type == 'POST_BACK':
                url_d = postback_click_store(request, linkobj[0], camp_track_map, netw_site_obj)
                return redirect(url_d)
            elif content_type == 'UPLOAD':
                process_obj, created = UploadAdvanced.objects.get_or_create(date=_d,
                                    siteid = netw_site_obj.siteid,
                                    fk_campaign = camp_track_map,
                                    processed = True)
                process_obj.clicks = process_obj.clicks + 1 if process_obj.clicks else 1
                process_obj.save()
                return redirect(camp_track_map.url)
            else:
                url_d = add_click_info(camp_track_map, request, linkobj[0])
                # process_obj, created = PPAdvancedProcessed.objects.get_or_create(date=_d,
                #                         siteid = netw_site_obj.siteid,
                #                         fk_campaign = camp_track_map)
                # process_obj.clicks = process_obj.clicks + 1 if process_obj.clicks else 1
                # process_obj.save()
                return redirect(url_d)
        else:
            netw_site_obj = NetworkAndSiteMap.objects.get(siteid=site_obj)
            content_type = netw_site_obj.networkid.connect_type
            has_error, camp_obj_or_msg = validate_and_create_campaign(sub_aff_obj['aff_obj'], subaffname+str(subaffid)+str(siteid), site, [], True)
            if has_error:
                print 'track link campaign creation', camp_obj_or_msg
                resp_dict['Error'] = 'Error with redirect to destination site.'
                resp_dict['status'] == 'Error'
            else:
                got_error, camp_track_map = add_campaign_tracker(sub_aff_obj['aff_obj'], camp_obj_or_msg)
                resp_dict["tracking-link"] = camp_obj_or_msg.link
                resp_dict["subaffid"] = subaffid
                if not got_error:
                    if content_type == 'POST_BACK':
                        url_d = postback_click_store(request, camp_obj_or_msg, camp_track_map, netw_site_obj)
                        return redirect(url_d)
                    elif content_type == 'UPLOAD':
                        process_obj, created = UploadAdvanced.objects.get_or_create(date=_d,
                                            siteid = netw_site_obj.siteid,
                                            fk_campaign = camp_track_map,
                                            processed = True)
                        process_obj.clicks = process_obj.clicks + 1 if process_obj.clicks else 1
                        process_obj.save()
                        return redirect(camp_track_map.url)
                    else:
                        url_d = add_click_info(camp_track_map, request, camp_obj_or_msg)
                        return redirect(url_d)
                return redirect(camp_track_map.url)
    except Exception as e:
        resp_dict['Error'] = 'Error with redirect to destination site.'
        resp_dict['status'] = 'Error'
        print 'track link', str(e)
    return HttpResponse(json.dumps(resp_dict), content_type='application/json')



@csrf_exempt
def vcommdata(request):
    resp_dict = {'status':'Error'}
    try:
        # encoded = request.META['HTTP_AUTHORIZATION']
        # data = base64.b64decode(encoded.split(' ')[1]).split(':')
        # username = data[0]
        # password = data[1]
        # auth_resp = authenticate_user(username, password)
        # if auth_resp['status'] == 'Error':
        #     return HttpResponse(json.dumps(auth_resp), content_type='application/json')
        affid = int(request.POST.get('affid')) if request.POST.get('affid') else None
        subaffid = int(request.POST.get('subaffid')) if request.POST.get('subaffid') else None
        from_date = request.POST.get('startdate')
        todate = request.POST.get('enddate')
        if not subaffid:
            affid = int(request.GET.get('affid')) if request.GET.get('affid') else None
            subaffid = int(request.GET.get('subaffid')) if request.GET.get('subaffid') else None
            from_date = request.GET.get('startdate')
            todate = request.GET.get('enddate')
        if not subaffid:
            return HttpResponse(json.dumps({'status':'Error',
                            'message': 'subaffid is mandatory parameters.'}),
                            content_type='application/json')
        if from_date and todate:
            from_date = datetime.strptime(from_date, "%d-%m-%Y").date()
            todate = datetime.strptime(todate, "%d-%m-%Y").date()
        fp_sub_add_obj = VcommFPAffiliateMapping.objects.get(vcommsubid=subaffid)
        subaffobj = fp_sub_add_obj.affiliateid
        # camp_links = Campaign.objects.filter(fk_affiliateinfo=subaffobj).select_related('siteid').\
        #                 order_by('-id')
        parent_aff_obj = subaffobj.superaffiliate
        camp_track_links = CampaignTrackerMapping.objects.filter(fk_affiliateinfo=subaffobj)
        resp = get_subaffiliate_data_set(subaffid, camp_track_links, subaffobj, from_date, todate)
        resp_dict['status'] = 'Success'
        resp_dict['data'] = resp
    except Exception as e:
        resp_dict['message'] = 'Error in sending data for the sub affiliate '+str(subaffid)+' .'
        resp_dict['status'] = 'Error'
        print 'sub affiliate report', str(e)
    return HttpResponse(json.dumps(resp_dict), content_type='application/json')


@csrf_exempt
def vcommadmin(request):
    resp_dict = {'status':'Error'}
    try:
        # encoded = request.META['HTTP_AUTHORIZATION']
        # data = base64.b64decode(encoded.split(' ')[1]).split(':')
        # username = data[0]
        # password = data[1]
        # auth_resp = authenticate_user(username, password)
        # if auth_resp['status'] == 'Error':
        #     return HttpResponse(json.dumps(auth_resp), content_type='application/json')
        # admin_aff_obj = Affiliateinfo.objects.select_related('account_manager').get(id=auth_resp['affid'])
        affid = int(request.POST.get('affid')) if request.POST.get('affid') else None
        subaffid = int(request.POST.get('subaffid')) if request.POST.get('subaffid') else None
        from_date = request.POST.get('startdate')
        todate = request.POST.get('enddate')
        if not subaffid:
            affid = int(request.GET.get('affid')) if request.GET.get('affid') else None
            subaffid = int(request.GET.get('subaffid')) if request.GET.get('subaffid') else None
            from_date = request.GET.get('startdate')
            todate = request.GET.get('enddate')
        if not affid:
            return HttpResponse(json.dumps({'status':'Error',
                            'message': 'affid is mandatory parameters.'}),
                            content_type='application/json')
        if from_date and todate:
            from_date = datetime.strptime(from_date, "%d-%m-%Y").date()
            todate = datetime.strptime(todate, "%d-%m-%Y").date()
        admin_aff_obj = Affiliateinfo.objects.select_related('account_manager').get(id=affid)
        # camp_links = Campaign.objects.filter(fk_affiliateinfo=subaffobj).select_related('siteid').\
        #                 order_by('-id')
        # camp_track_links = CampaignTrackerMapping.objects.filter(fk_campaign__fk_affiliateinfo=admin_aff_obj)
        resp = get_admin_affiliate_data_set(admin_aff_obj, from_date, todate)
        resp_dict['status'] = 'Success'
        resp_dict['data'] = resp
    except Exception as e:
        resp_dict['message'] = 'Error in sending data for the affiliate.'
        resp_dict['status'] = 'Error'
        print 'affiliate report', str(e)
    return HttpResponse(json.dumps(resp_dict), content_type='application/json')


# class Account(TemplateView):


#     def post(self, request):
#         contactname = str(request.POST.get('contactname'))
#         companyname = str(request.POST.get('companyname'))
#         email = str(request.POST.get('email'))
#         address = str(request.POST.get('address'))
#         city = str(request.POST.get('city'))
#         country = str(request.POST.get('country'))
#         state = str(request.POST.get('state'))
#         postcode = str(request.POST.get('postcode'))
#         telephone = str(request.POST.get('telephone'))
#         status = str(request.POST.get('active'))
#         id = request.session.get('affid')
#         obj = Affiliateinfo.objects.select_related('account_manager').get(id=id)
#         if obj.email != email:
#             mailidcount = Affiliateinfo.objects.filter(email=email).count()
#             if mailidcount == 0:
#                 pass
#             else:
#                 return HttpResponse('Mail Id is Already Registered')
#                 return render_to_response('affiliates/login.html', locals(), context_instance=RequestContext(request))
#         obj.contactname = contactname
#         obj.companyname = companyname
#         obj.email = email
#         obj.address = address
#         obj.city = city
#         obj.state = state
#         obj.country = country
#         obj.postcode = postcode
#         obj.telephone = telephone
#         obj.active = status
#         obj.save()
#         return HttpResponseRedirect(reverse('affiliate'),)

#     def get(self, request):
#         return HttpResponseRedirect(reverse('affiliate'),)



class Linkaffiliate(TemplateView):
    def get(self, request):
        id = request.session.get('affid')
        obj = Affiliateinfo.objects.select_related('account_manager').get(id=id)
        links = Campaign.objects.filter(fk_affiliateinfo=obj).order_by('-id')
        return render(request, 'affiliates/linkcreator.html', locals())
        # return render_to_response('affiliates/linkcreator.html', locals(), context_instance=RequestContext(request))


class Banner(TemplateView):

    def get(self, request, linkid):
        id = request.session.get('affid')
        obj = Affiliateinfo.objects.select_related('account_manager').get(id=id)
        links = Campaign.objects.get(id=linkid)
        linkid = links.id
        bannerlogs = Bannerlogs.objects.filter(fk_campaign=links)
        bannerlogslist =[]
        for i in bannerlogs:
            bannerlogslist.append(int(str(i.fk_bannerimages.id)))
        banners = BannerImages.objects.all()
        for i in bannerlogslist:
            banners = banners.exclude(id=i)
        return render(request, 'affiliates/selectbanner.html', locals())
        # return render_to_response('affiliates/selectbanner.html', locals(), context_instance=RequestContext(request))

    def post(self, request, linkid):
        fk_campaign = Campaign.objects.get(id=linkid)
        return render(request, 'affiliates/selectbanner.html', locals())
        # return render_to_response('affiliates/selectbanner.html', locals(), context_instance=RequestContext(request))


# class Bannertolink(TemplateView):

#     def get(self, request, linkid, bannerid):
#         id = request.session.get('affid')
#         obj = Affiliateinfo.objects.select_related('account_manager').get(id=id)
#         linkobj = Campaign.objects.get(id=linkid)
#         bannerobj = BannerImages.objects.get(id=bannerid)
#         bannername = bannerobj.banner.name
#         link = linkobj.link
#         currentsite = get_current_site(request)
#         currentsite = str(currentsite.domain)
#         bannerlogobj = Bannerlogs.objects.create(fk_campaign=linkobj, fk_bannerimages=bannerobj)
#         bannerlogobj.link = """<a href="%s"><img border="0" alt="" src="http://%s/%s"
#                             height="200" width='300' /></a>""" % ((str(link), str(currentsite), str(bannername)))
#         bannerlogobj.save()
#         return HttpResponseRedirect(reverse('banner', args=(linkid,)))

class  Bannerlist(TemplateView):
    def get(self , request):
        id = request.session.get('affid')
        obj = Affiliateinfo.objects.select_related('account_manager').get(id=id)
        bannerobj = Bannerlogs.objects.filter(fk_campaign__fk_affiliateinfo=obj)
        return render(request, 'affiliates/banner.html', locals())
        # return render_to_response('affiliates/banner.html',locals())     
        #return HttpResponseRedirect(reverse('banner',args=(id,)))


@check_ip_required
@staff_member_required
def adminhome(request):
    """
    function to call affiliate admin default template
    template name :  affiliate/admin/index.html
    """
    return render(request, 'affiliates/admin/index.html', locals())
    # return render_to_response('affiliates/admin/index.html',
    #                           locals(),
    #                           context_instance=RequestContext(request))


class Searchadmin(TemplateView):

    @method_decorator(check_ip_required)
    @method_decorator(staff_member_required)
    def get(self, request):
        networks, ntw_sites_list = get_network_sites_list()
        ntw_sites_list = json.dumps(ntw_sites_list)
        return render(request, 'affiliates/admin/search.html', locals())
        # return render_to_response('affiliates/admin/search.html', locals(), 
        #                           context_instance=RequestContext(request))

    @method_decorator(check_ip_required)
    @method_decorator(staff_member_required)
    def post(self, request):
        networks, ntw_sites_list = get_network_sites_list()
        ntw_sites_list = json.dumps(ntw_sites_list)
        network_name = request.POST.get('network', None)
        sites = request.POST.getlist('sites')
        affid = request.POST.get('id')
        username = str(request.POST.get('username'))
        mail = str(request.POST.get('mail'))
        obj = Affiliateinfo.objects.filter(networkid__networkname__startswith=network_name)
        if affid != '':
            try:
                obj = obj.filter(id=affid)
            except:
                response = 'Please Enter Valid ID'
                return render(request, 'affiliates/admin/search.html', {'response':response})
                # return render_to_response('affiliates/admin/search.html',{'response':response},context_instance=RequestContext(request))
        if username is not '':
            obj = obj.filter(username__startswith=username)
        if mail is not '':
            obj = obj.filter(email__startswith=mail)
        if sites:
            obj = Affiliateinfo.objects.filter(networkid__networkname__startswith=network_name,
                                               siteid__domain__in=sites)
        newlist = list(obj)
        if newlist == []:
            response = 'No matching Record Found'
            return render(request, 'affiliates/admin/search.html', locals())
            # return render_to_response('affiliates/admin/search.html', locals(), context_instance=RequestContext(request))
        request.session['newlist'] = newlist
        return render(request, 'affiliates/admin/search.html', locals())
        # return render_to_response('affiliates/admin/search.html', locals(), context_instance=RequestContext(request))


@check_ip_required
@staff_member_required
def affiliatesearchimportcsv(request):
    newlist = request.session.get('newlist', None)
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="affiliate_search.csv"'
    writer = csv.writer(response, delimiter=',')
    writer.writerow(['Affiliate ID', 'User Name', 'Contact Name', 'company name', 'Email', 'Address', 'City',
                     'State', 'Country', 'Telephone', 'Post Code', 'ipaddress', 'Registeredon', 'Account Active'])
    for i in newlist:
        writer.writerow([i.id, i.username, i.contactname, i.companyname, i.email, i.address, i.city, i.state, i.country,
                         i.telephone, i.postcode, i.ipaddress, i.registeredon, i.active, i.status])
    return response


@check_ip_required
@staff_member_required
def affiliatedeactivate(request, username):
    """ it will deactivate affiliate account"""
    obj = Affiliateinfo.objects.select_related('account_manager').get(id=username)
    obj.active = False   # active boolean field
    obj.save()
    return HttpResponseRedirect(reverse('affiliateaccount', args=(username,)))


@check_ip_required
@staff_member_required
def affiliateactivate(request, username):
    """ it will active affiliate account"""
    obj = Affiliateinfo.objects.select_related('account_manager').get(id=username)
    obj.active = True
    obj.save()
    return HttpResponseRedirect(reverse('affiliateaccount', args=(username,)))



@check_ip_required
@staff_member_required
def affiliatereportimportcsv(request):
    """  """
    newlist = request.session.get('csvdata', None)
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="Financial_report_affiliate.csv"'
    writer = csv.writer(response)
    l = len(newlist)
    for i in newlist:
        writer.writerow(i)
    return response


class Bannerallseeadmin(TemplateView):
    """ """
    @method_decorator(check_ip_required)
    @method_decorator(staff_member_required)
    def get(self, request):
        from settings import MEDIA_URL
        banners = BannerImages.objects.all()
        return render(request, 'affiliates/admin/seebanners.html', locals())
        # return render_to_response('affiliates/admin/seebanners.html', locals(), context_instance=RequestContext(request))


# class Banneraddadmin(TemplateView):
#     """ Upload Images """
#     @method_decorator(check_ip_required)
#     @method_decorator(staff_member_required)
#     def get(self, request):
#         obj = Affiliateinfo.objects.all()
#         return render_to_response('affiliates/admin/addbanner.html', locals(), context_instance=RequestContext(request))

#     @method_decorator(check_ip_required)
#     @method_decorator(staff_member_required)
#     def post(self, request):
#         bannertitle = request.POST.get('bannername')
#         try:
#             banner = request.FILES['banner']
#         except:
#             response = "Please Upload Banner"
#             return render_to_response('affiliates/admin/addbanner.html', locals(), context_instance=RequestContext(request))
#         if bannertitle == "":
#             return HttpResponse("Please Enter Banner Name")
            
#         bannerobj = BannerImages.objects.create(title=bannertitle, user=request.user, banner=banner)
#         bannername =  bannerobj.banner.name
#         currentsite = get_current_site(request)
#         currentsite = str(currentsite.domain)
#         bannerobj.link = """<a href="http://%s/media/%s"><img border="0" alt="" src="http://%s/media/%s" height="200" width='300' /></a>""" % (str(currentsite) ,str(bannername), str(currentsite)  , str(bannername))
#         im = PImage.open(os.path.join(MEDIA_ROOT, bannerobj.banner.name))
#         bannerobj.width, bannerobj.height = im.size
#         bannerobj.save()
#         response = "Banner is added successfully"
#         image = "<img src='/media/%s'  class='preview'>" % (bannername,)
#         return  HttpResponse(image)

# class  Banneradmin(TemplateView):
    
#     @method_decorator(check_ip_required)
#     @method_decorator(staff_member_required)
#     def get(self, request, id, linkid):
#         obj = Affiliateinfo.objects.select_related('account_manager').get(id=id)
#         id = obj.id
#         links = Campaign.objects.get(id=linkid)
#         linkid = links.id
#         bannerlogs = Bannerlogs.objects.filter(fk_campaign=links)
#         bannerlogslist =[]
#         for i in bannerlogs:
#             bannerlogslist.append(int(str(i.fk_bannerimages.id)))
#         banners = BannerImages.objects.all()
#         for i in bannerlogslist:
#             banners = banners.exclude(id=i)
#         addmessage = request.session.get('addmessage')
#         try:
#             del request.session['addmessage']
#         except KeyError:
#             pass
#         if addmessage != None:
#             response = addmessage
#         return render_to_response('affiliates/admin/selectbanner.html', locals(), context_instance=RequestContext(request))

#     @method_decorator(check_ip_required)
#     @method_decorator(staff_member_required)
#     def post(self, request, linkid):
#         fk_campaign = Campaign.objects.get(id=linkid)
#         return render_to_response('affiliates/admin/selectbanner.html', locals(), context_instance=RequestContext(request))


# class Bannerseeadmin(TemplateView):

#     @method_decorator(check_ip_required)
#     @method_decorator(staff_member_required)
#     def get(self, request, id, linkid):
#         obj = Affiliateinfo.objects.select_related('account_manager').get(id=id)
#         banners = Bannerlogs.objects.filter(fk_campaign=linkid)
#         deletemessage = request.session.get('deletemessage')
#         try:
#             del request.session['deletemessage']
#         except KeyError:
#             pass
#         if deletemessage != None:
#             response = deletemessage
#         return render_to_response('affiliates/admin/allbanners-link.html', locals(), context_instance=RequestContext(request))


# class Bannersee(TemplateView):
#     def get(self, request, linkid):
#         sessionid = request.session.get('affid')
#         try:
#             obj = Affiliateinfo.objects.select_related('account_manager').get(id=id)
#         except:
#             pass
#         banners = Bannerlogs.objects.filter(fk_campaign=linkid)
#         return render_to_response('affiliates/allbanners-link.html', locals(), context_instance=RequestContext(request))


# class Bannertolinkadmin(TemplateView):

#     @method_decorator(check_ip_required)
#     @method_decorator(staff_member_required)
#     def get(self , request ,id, linkid , bannerid):
#         obj = Affiliateinfo.objects.select_related('account_manager').get(id=id)
#         linkobj = Campaign.objects.get(id=linkid)
#         bannerobj = BannerImages.objects.get(id=bannerid)
#         bannername = bannerobj.banner.name
#         link = linkobj.link
#         currentsite = get_current_site(request)
#         currentsite = str(currentsite.domain)
#         try:
#             bannerlogobj = Bannerlogs.objects.create(fk_campaign=linkobj, fk_bannerimages=bannerobj)
#         except:
#             return HttpResponse('This Image is already added for this link')
#         bannername = "media/"+bannername
#         bannerlogobj.link = """<a href="%s"><img border="0" alt="" src="http://%s/%s" height="200" width='300' /></a>""" % ((str(link) ,str(currentsite), str(bannername)))
#         bannerlogobj.save()
#         linkid = linkobj.id
#         id = obj.id
#         addmessage = "Banner is added successfully to campaign"
#         request.session['addmessage'] = addmessage
#         return HttpResponseRedirect(reverse('banneradmin', args=(id, linkid,)))


# class Bannertolink(TemplateView):

#     #@method_decorator(check_ip_required)
#     #@method_decorator(staff_member_required)
#     def get(self, request, linkid, bannerid):
#         sessionid = request.session.get('affid')
#         try:
#             obj = Affiliateinfo.objects.select_related('account_manager').get(id=sessionid)
#         except:
#             pass
#         linkobj = Campaign.objects.get(id=linkid)
#         bannerobj = BannerImages.objects.get(id=bannerid)
#         bannername = bannerobj.banner.name
#         link = linkobj.link
#         currentsite = get_current_site(request)
#         currentsite = str(currentsite.domain)
#         try:
#             bannerlogobj = Bannerlogs.objects.create(fk_campaign = linkobj , fk_bannerimages = bannerobj)
#         except:
#             return HttpResponse('This Image is already added for this link')
#         bannerlogobj.link = """<a href="%s"><img border="0" alt="" src="http://%s/%s" height="200" width='300' /></a>""" % ((str(link) ,str(currentsite), str(bannername)))
#         bannerlogobj.save()
#         linkid = linkobj.id
#         id = obj.id
#         return HttpResponseRedirect(reverse('banner', args=(linkid,)))


# class Bannerdeleteadmin(TemplateView):

#     @method_decorator(check_ip_required)
#     @method_decorator(staff_member_required)
#     def get(self, request, id, linkid, bannerid):
#         obj = Affiliateinfo.objects.select_related('account_manager').get(id=id)
#         linkobj = Campaign.objects.get(id=linkid)
#         bannerlogobj = Bannerlogs.objects.get(id = bannerid)
#         bannerlogobj.delete()
#         linkid = linkobj.id
#         id = obj.id
#         deletemessage = "Banner has been removed succcessfully"
#         request.session['deletemessage'] = deletemessage
#         return HttpResponseRedirect(reverse('seebanneradmin', args=(id,linkid,)))


# class Bannerdelete(TemplateView):
#     def get(self, request, linkid, bannerid):
#         linkobj = Campaign.objects.get(id=linkid)
#         bannerlogobj = Bannerlogs.objects.get(id=bannerid)
#         bannerlogobj.delete()
#         linkid = linkobj.id
#         return HttpResponseRedirect(reverse('seebanner', args=(linkid,)))


# class Link(TemplateView):

#     @method_decorator(check_ip_required)
#     @method_decorator(staff_member_required)
#     def get(self, request, username):
#         obj = Affiliateinfo.objects.select_related('account_manager').get(id=str(username))
#         links = Campaign.objects.filter(fk_affiliateinfo = obj).order_by('-id')
#         return render_to_response('affiliates/admin/linkcreator.html',locals(),context_instance=RequestContext(request))


# @check_ip_required
# @staff_member_required
# def createaffiliatelinkadmin(request, id):
#     if request.method == 'POST':
#         #currentsite = get_current_site(request)
#         #currentsite = str(currentsite.domain)
#         linkname = str(request.POST.get('linkname'))
#         currentsite = request.POST.get('sitename')
#         try:
#             obj = Affiliateinfo.objects.select_related('account_manager').get(id=id)
#         except:
#             obj = None
#         sites_obj = None
#         links = None
#         if obj:
#             sites_obj = get_brand_order(obj)
#             links = Campaign.objects.filter(fk_affiliateinfo=obj, siteid__in=sites_obj).order_by('-id')
#         if linkname == '':
#             error = 'Please Select other unique  Campaign Name.'
#             return render_to_response('affiliates/admin/linkcreator.html', locals(),
#                                       context_instance=RequestContext(request))
#         c_list = Campaign.objects.filter(name=linkname, fk_affiliateinfo=obj).count()
#         if c_list != 0:
#             error = 'This campaign name is already registered.'
#             return render_to_response('affiliates/admin/linkcreator.html', locals(),
#                                       context_instance=RequestContext(request))
#         try:
#             selected_site_obj = sites_obj.get(domain=currentsite)
#             linkobj = Campaign.objects.create(fk_affiliateinfo=obj, name=linkname, siteid=selected_site_obj,
#                                               networkid=obj.networkid)
#         except Exception as e:
#             error = 'This campaign name is already registered.'
#             return render_to_response('affiliates/admin/linkcreator.html', locals(),
#                                       context_instance=RequestContext(request))
#         linkurl = get_affiliate_campaign_link(selected_site_obj.domain, linkobj)
#         linkobj.link = linkurl
#         linkobj.name = linkname
#         linkobj.save()
#         promotion = 'link'
#         links = Campaign.objects.filter(fk_affiliateinfo=obj, siteid__in=sites_obj).order_by('-id')
#         return render_to_response('affiliates/admin/linkcreator.html', locals(),
#                                   context_instance=RequestContext(request))

#     if request.method == 'GET':
#         """ """
#         try:
#             obj = Affiliateinfo.objects.select_related('account_manager').get(id=id)
#         except:
#             obj = None
#         sites_obj = None
#         if obj:
#             sites_obj = get_brand_order(obj)
#         links = Campaign.objects.filter(fk_affiliateinfo=obj, siteid__in=sites_obj).order_by('-id')
#         return render_to_response('affiliates/admin/linkcreator.html',
#                                   locals(),
#                                   context_instance=RequestContext(request))


def signup(request, affiliate, campaign):
    """
    THIS FUCNTION WILL CALL WHEN PLAYER WILL HIT ON UNIQUE CAMPAIGN
    GIVEN BY AFFILIATE
    IT WILL STORE CAMPAIGN , AFFILIATE INTO SESSION AND IT WILL REDIRECT TO
    SIGNUP AT PLAYER WEBSITE.
    """
    if request.method == 'GET':
        country = Country.objects.filter(countryname='india')
        state = State.objects.filter(country=country)
        obj = Affiliateinfo.objects.select_related('account_manager').get(id=affiliate)
        if obj.status not in ['Pending', 'Rejected']:
            obj1 = Campaign.objects.get(id=campaign)
            Hitcount.objects.create(fk_affiliateinfo=obj, fk_campaign=obj1)
            request.session['affiliate'] = affiliate      # putting affiliateid , campaign into session 
            request.session['campaign'] = campaign
        return HttpResponseRedirect(reverse('signup'))

def get_affiliate_logins_or_uniquelogins(request, affid, uniquelogins=False):
    obj = Affiliateinfo.objects.select_related('account_manager').get(id=affid)
    resp_dict = {}
    if request.method == "GET":
        campaignID = request.GET.get('campaignid')
        date_type = request.GET.get('date')
        from_date = request.GET.get('datepicker1')
        todate = request.GET.get('datepicker2')
        if not date_type:
            date_type = "Today"
            from_date, todate = None,None
        dates_dict = get_fromdate_todate_for_django_query_filter(date_type, from_date, todate)
        to_date_time = dates_dict.get('todate')
        from_date_time = dates_dict.get('fromdate')
        
    else:
        campaignID = request.POST.get('campaignid')
        date_type = request.POST.get('date')
        if date_type and date_type.lower() != "all":
            fromdate = request.POST.get('datepicker1')
            todate = request.POST.get('datepicker2')
            dates_dict = get_fromdate_todate_for_django_query_filter(date_type, fromdate, todate)
            to_date_time = dates_dict.get('todate')
            from_date_time = dates_dict.get('fromdate')
        else:
            dates_dict = get_site_player_first_register_timestamp()
            to_date_time = dates_dict.get('todate')
            from_date_time = dates_dict.get('fromdate')
    campaignID = int(campaignID.split('-')[0]) if campaignID else None
    if uniquelogins:
        if campaignID:
            resp_dict['campaignID'] = campaignID
            campaign_obj = Campaign.objects.get(id=campaignID)
            resp_dict['campaign_name'] = campaign_obj.name
            l = Loginon.objects.filter(loginok=True, uniquelogin=True, logintime__gte=from_date_time.date(),
                                  logintime__lte=to_date_time.date(), campaignid=campaign_obj)
        else:
            l = Loginon.objects.filter(loginok=True, uniquelogin=True, logintime__gte=from_date_time.date(),
                                  logintime__lte=to_date_time.date(), affiliateid=obj)
    else:
        if campaignID:
            resp_dict['campaignID'] = campaignID
            campaign_obj = Campaign.objects.get(id=campaignID)
            resp_dict['campaign_name'] = campaign_obj.name
            l = Loginon.objects.filter(loginok=True,  logintime__gte=from_date_time.date(),
                                  logintime__lte=to_date_time.date(), campaignid=campaign_obj)
        else:
            l = Loginon.objects.filter(loginok=True, logintime__gte=from_date_time.date(),
                                  logintime__lte=to_date_time.date(), affiliateid=obj)
    l_list = []
    for i in l:
        temp = {}
        temp['playerid'] = i.playerid_id
        temp['playername'] = i.playerid.username
        temp['realchips'] = i.playerid.realchips
        temp['freechips'] = i.playerid.funchips
        if uniquelogins:
            temp['cashgames'] = Reconcilelogs.objects.filter(timestamp__lte = to_date_time,
                             timestamp__gte=from_date_time,transaction_type="WAGERING",
                             amounttype="CASH",playerid=i.playerid).count()
            temp['freegames'] = Reconcilelogs.objects.filter(playerid=i.playerid,
                              timestamp__lte = to_date_time,
                             timestamp__gte=from_date_time,transaction_type="WAGERING",
                             amounttype="FUNCHIPS").count()
            temp['deposits'] = Order.objects.filter(direction="DEPOSIT",orderstate="FINISHED",playerid=i.playerid, 
                                                    timestamp__gte = from_date_time, 
                                                    timestamp__lte=to_date_time).\
                                                    exclude(ordertype='REAL_ADJUSTMENT').\
                                                    aggregate(Sum('ordermoney')).values()[0]
        l_list.append(temp)
    resp_dict['players_info'] = l_list
    resp_dict['result_count'] = len(l_list)
    resp_dict['obj'] = obj
    resp_dict['campaign_list'] = Campaign.objects.filter(fk_affiliateinfo=obj)
    resp_dict['custom'] = date_type
    if date_type.lower() == 'custom':
        resp_dict['from_ui_date'] = from_date_time
        resp_dict['to_ui_date'] = from_date_time
    resp_dict['from_date_time'] = from_date_time
    resp_dict['to_date_time'] = to_date_time
    return resp_dict
# @check_ip_required
# @staff_member_required
# def admin_affiliate_user_logins(request, id):
#     resp_dict = get_affiliate_logins_or_uniquelogins(request, id, False)
#     return render_to_response('affiliates/admin/affiliate_logins.html',
#                                  resp_dict, context_instance=RequestContext(request)) 
# @check_ip_required
# @staff_member_required
# def admin_affiliate_user_uniquelogins(request, id):
#     resp_dict = get_affiliate_logins_or_uniquelogins(request, id, True)
#     return render_to_response('affiliates/admin/affiliate_uniquelogins.html',
#                                  resp_dict, context_instance=RequestContext(request))




# def get_users_aff_camp_financials(aff_id, campaign_id, from_date_time, to_date_time, extra_params_dict={}):
#     resp_dict = {}
#     temp = {}
#     obj = Affiliateinfo.objects.select_related('account_manager').get(id=aff_id)
#     commission = Affiliatecommission.objects.get(fk_affiliateinfo=obj)
#     resp_dict['obj'] = obj
#     result_list = []
#     total_sum_deposit = 0
#     total_sum_withdrawal = 0
#     total_rake_sum = 0
#     total_invested_sum = 0
#     total_adustments_sum = 0
#     registertype = extra_params_dict.get('registertype')
#     bet_type = extra_params_dict.get('bet_type')
#     if registertype and registertype == 'new users':
#         if not campaign_id:
#             player_objs_list = Playerinfo.objects.filter(affiliate=obj,
#                                                      registeredon__gte=from_date_time,
#                                                      registeredon__lte=to_date_time).order_by('-id')
#         else:
#             player_objs_list = Playerinfo.objects.filter(affiliate=obj,campaign__id=campaign_id,
#                                                      registeredon__gte=from_date_time,
#                                                      registeredon__lte=to_date_time).order_by('-id')
#     elif registertype and registertype == 'old users':
#         if not campaign_id:
#             player_objs_list = Playerinfo.objects.filter(affiliate=obj,
#                                                      registeredon__lte=from_date_time).order_by('-id')
#         else:
#             player_objs_list = Playerinfo.objects.filter(affiliate=obj,campaign__id=campaign_id,
#                                                      registeredon__lte=from_date_time).order_by('-id')
#     else:
#         if not campaign_id:
#             player_objs_list = Playerinfo.objects.filter(affiliate=obj).order_by('-id')
#         else:
#             player_objs_list = Playerinfo.objects.filter(affiliate=obj,
#                                                          campaign__id=campaign_id).order_by('-id')
#     add_total_list = False
#     for player in player_objs_list:
#         add_total_list = True
#         temp = []
#         sum_deposits = Order.objects.filter(Q(direction='DEPOSIT') & Q(ordertype='BANKING') 
#                                     & Q(orderstate='FINISHED') & Q(playerid__affiliate=obj) &
#                                     Q(timestamp__gte=from_date_time) &
#                                     Q(timestamp__lte=to_date_time) & Q(playerid=player)).\
#                                     aggregate(Sum('ordermoney')).values()[0]
#         if sum_deposits in [None, 'None', 0]:
#             sum_deposits = 0
#         sum_withdrawal = Order.objects.filter(Q(direction='WITHDRAWL') & Q(ordertype='BANKING') 
#                                     & Q(orderstate='FINISHED') & Q(playerid__affiliate=obj) &
#                                     Q(timestamp__gte=from_date_time) &
#                                     Q(timestamp__lte=to_date_time) & Q(playerid=player)).\
#                                     aggregate(Sum('ordermoney')).values()[0]
#         if sum_withdrawal in [None, 'None', 0]:
#             sum_withdrawal = 0
#         realadjustmentdeposit = Order.objects.filter(Q(playerid__affiliate=obj) & Q(direction='DEPOSIT') & 
#                                                  Q(ordertype='REAL_ADJUSTMENT')&
#                                                  Q(timestamp__gte=from_date_time) &
#                                                  Q(timestamp__lte=to_date_time)& Q(playerid=player)).aggregate(Sum('ordermoney'))
    
#         if realadjustmentdeposit and realadjustmentdeposit.get('ordermoney__sum'):
#             radeposits = realadjustmentdeposit.get('ordermoney__sum')
#         else:
#             radeposits = 0
    
#         realadjustmentwithdrawl = Order.objects.filter(Q(playerid__affiliate=obj) & Q(direction='WITHDRAWL') &
#                                                         Q(ordertype='REAL_ADJUSTMENT')&
#                                                      Q(timestamp__gte=from_date_time) &
#                                                      Q(timestamp__lte=to_date_time)& Q(playerid=player)).aggregate(Sum('ordermoney'))
#         if realadjustmentwithdrawl and realadjustmentwithdrawl.get('ordermoney__sum'):
#             rawithdrawl = realadjustmentwithdrawl.get('ordermoney__sum')
#         else:
#             rawithdrawl = 0
#         adjustments = radeposits - rawithdrawl
#         u_total = sum_deposits - sum_withdrawal + adjustments
#         accurals = ((u_total * commission.revsharepercent) / 100)#accurals
        
#         cursor = connection.cursor()
#         cursor.execute("""SELECT SUM( pin.investedamount), SUM(r.rakemoney) FROM userapp_playersinvolved pin
#             INNER JOIN  userapp_game g ON g.id= pin.fkgame_id
#             INNER JOIN userapp_rakelogs r ON g.gameid=r.fkgame_id
#             INNER JOIN userapp_gamesetting gs ON g.gamesettingid = gs.id
#             INNER JOIN userapp_stream s ON s.id=g.streamid
#             WHERE pin.playerid_id=%s AND pin.investedamount IS NOT NULL
#             AND s.fromchips=%s AND s.tochips=%s AND pin.timestamp >= %s AND
#             pin.timestamp <= %s;""",(player.id, bet_type, bet_type,from_date_time, to_date_time,))
    
#         results = cursor.fetchall()
#         rake_sum = results[0][0] if results[0][0] else 0
#         invested_sum = results[0][1] if results[0][1] else 0
#         p_type = "None"
#         if player.registeredon <= from_date_time:
#             p_type = "OF" if sum_deposits == 0 else "OC"
#         elif from_date_time <= player.registeredon <= to_date_time:
#             p_type = "NF" if sum_deposits == 0 else "NC"
#         temp = [player.username, player.id, p_type, sum_deposits, sum_withdrawal, rake_sum, invested_sum, accurals]
        
#         total_sum_deposit += sum_deposits
#         total_sum_withdrawal += sum_withdrawal
#         total_rake_sum += rake_sum
#         total_invested_sum += invested_sum
#         total_adustments_sum += accurals
#         result_list.append(temp)
    
#     if add_total_list:
#     #adding totals list
#         result_list.append(["TOTAL","","",total_sum_deposit,
#                         total_sum_withdrawal,
#                        total_rake_sum,
#                         total_invested_sum, 
#                         total_adustments_sum])
#     resp_dict['users_data_list'] = result_list
#     resp_dict['revsharepercent'] = commission.revsharepercent
#     return resp_dict

# def get_affiliate_data_for_give_dates(obj, commision, from_date_time, to_date_time):
#     temp = {}
#     temp['username'] = obj.username
#     temp['registration']= Playerinfo.objects.filter(affiliate=obj,
#                                                     registeredon__gte=from_date_time,
#                                                     registeredon__lte=to_date_time).count()
                                                    
#     temp['affiliate_logins'] = Loginon.objects.filter(affiliateid=obj, loginok=True,
#                                                                    logintime__gte=from_date_time.date(),
#                                                                    logintime__lte=to_date_time.date()).count()
#     temp['affiliate_uniquelogins'] = Loginon.objects.filter(affiliateid=obj, loginok=True,uniquelogin=True,
#                                                                    logintime__gte=from_date_time.date(),
#                                                                    logintime__lte=to_date_time.date()).count()
#     affhit = Hitcount.objects.filter(fk_affiliateinfo=obj,
#                                      timestamp__gte=from_date_time,
#                                       timestamp__lte=to_date_time).aggregate(Sum('hits'))
#     if affhit and affhit.get('hits__sum'):
#         temp['hitcount'] = affhit.get('hits__sum')
#     else:
#         temp['hitcount'] = 0
#     temp['firsttimeobj'] = Order.objects.filter(Q(direction='DEPOSIT') & Q(firstdeposit=True) & 
#                                         Q(orderstate='FINISHED')& Q(playerid__affiliate=obj) &
#                                         Q(timestamp__gte=from_date_time) &
#                                            Q(timestamp__lte=to_date_time)).count()
#     deposits = Order.objects.filter(Q(direction='DEPOSIT') & Q(ordertype='BANKING') 
#                                     & Q(orderstate='FINISHED') & Q(playerid__affiliate=obj) &
#                                     Q(timestamp__gte=from_date_time) &
#                                     Q(timestamp__lte=to_date_time)).aggregate(Sum('ordermoney'))
#     if deposits and deposits.get('ordermoney__sum'):
#         temp['sumdeposit'] = deposits.get('ordermoney__sum')
#     else:
#         temp['sumdeposit'] = 0
#     withdrawl = Order.objects.filter(Q(direction='WITHDRAWL') & Q(orderstate='FINISHED') & Q(playerid__affiliate=obj)&
#                                     Q(timestamp__gte=from_date_time) &
#                                     Q(timestamp__lte=to_date_time)).aggregate(Sum('ordermoney'))
#     if withdrawl and withdrawl.get('ordermoney__sum'):
#         temp['sumwithdrawl'] = withdrawl.get('ordermoney__sum')
#     else:
#         temp['sumwithdrawl'] = 0
#     commission = Affiliatecommission.objects.get(fk_affiliateinfo=obj)
#     temp['cpa'] = (temp['firsttimeobj']) * (commission.cpacommissionvalue)
#     temp['cpr'] = (temp['registration']) * (commission.referalcommissionvalue)
#     realadjustmentdeposit = Order.objects.filter(Q(playerid__affiliate=obj) & Q(direction='DEPOSIT') & 
#                                                  Q(ordertype='REAL_ADJUSTMENT')&
#                                                  Q(timestamp__gte=from_date_time) &
#                                                  Q(timestamp__lte=to_date_time)).aggregate(Sum('ordermoney'))
    
#     if realadjustmentdeposit and realadjustmentdeposit.get('ordermoney__sum'):
#         radeposits = realadjustmentdeposit.get('ordermoney__sum')
#     else:
#         radeposits = 0

#     realadjustmentwithdrawl = Order.objects.filter(Q(playerid__affiliate=obj) & Q(direction='WITHDRAWL') &
#                                                     Q(ordertype='REAL_ADJUSTMENT')&
#                                                  Q(timestamp__gte=from_date_time) &
#                                                  Q(timestamp__lte=to_date_time)).aggregate(Sum('ordermoney'))
#     if realadjustmentwithdrawl and realadjustmentwithdrawl.get('ordermoney__sum'):
#         rawithdrawl = realadjustmentwithdrawl.get('ordermoney__sum')
#     else:
#         rawithdrawl = 0
#     temp['adjustments'] = radeposits - rawithdrawl
#     temp['total'] = temp['sumdeposit'] - temp['sumwithdrawl'] + temp['adjustments']
#     #temp['net'] = (temp['total'] * commission.revsharepercent)/100
#     temp['net'] = ((temp['total'] * commission.revsharepercent)/100)+temp['cpa']+temp['cpr']
#     #temp['revsharepercent'] = commission.revsharepercent
#     return temp
    
# def get_campaign_data_for_given_dates(obj, commission, from_date_time, to_date_time, date_wise=False):
#     resp_list = []
#     links = Campaign.objects.filter(fk_affiliateinfo=obj)
#     #commission = Affiliatecommission.objects.get(fk_affiliateinfo=obj)
#     for i in links:
#         camp_dict = {}
#         if date_wise:
#             camp_dict['DateTime'] = from_date_time
#         camp_dict['datepicker1'] = from_date_time.strftime("%m/%d/%Y")
#         camp_dict['datepicker2'] = to_date_time.strftime("%m/%d/%Y")
#         camp_dict['id'] = "%s-%s" %(i.id,i.name)
        
#         camp_dict['campaignregistration'] = Playerinfo.objects.filter(campaign=i,
#                                                                        registeredon__gte=from_date_time,
#                                                                        registeredon__lte=to_date_time).count()
        
#         camp_dict['campaign_logins'] = Loginon.objects.filter(campaignid=i, loginok=True,
#                                                                    logintime__gte=from_date_time.date(),
#                                                                    logintime__lte=to_date_time.date()).count()
#         camp_dict['campaign_uniquelogins'] = Loginon.objects.filter(campaignid=i, loginok=True,uniquelogin=True,
#                                                                    logintime__gte=from_date_time.date(),
#                                                                    logintime__lte=to_date_time.date()).count()
        
                                                                   
#         hit_count_obj =  Hitcount.objects.filter(fk_campaign=i,
#                                                  timestamp__gte=from_date_time,
#                                                  timestamp__lte=to_date_time).aggregate(Sum('hits'))
#         if hit_count_obj and hit_count_obj.get('hits__sum'):
#             camp_dict['campaignhitcount'] = hit_count_obj['hits__sum']
#         else:
#             camp_dict['campaignhitcount'] = 0

#         camp_dict['campaignsingletime'] = Order.objects.filter(Q(direction='DEPOSIT') & Q(firstdeposit=True) & 
#                                                     Q(orderstate='FINISHED')& Q(playerid__campaign=i)&
#                                                     Q(timestamp__gte=from_date_time) &
#                                                  Q(timestamp__lte=to_date_time)).count()
        
        
#         camp_dict['campaigncpalist'] = camp_dict['campaignsingletime'] * (commission.cpacommissionvalue)
#         camp_dict['campaigncprlist'] = camp_dict['campaignregistration'] * (commission.referalcommissionvalue)
        
        
#         campaigndeposits_obj = Order.objects.filter(Q(direction='DEPOSIT') & Q(ordertype='BANKING') & 
#                                                 Q(orderstate='FINISHED') &
#                                                  Q(playerid__campaign=i) & Q(timestamp__gte=from_date_time) &
#                                                  Q(timestamp__lte=to_date_time)).aggregate(Sum('ordermoney'))
#         if campaigndeposits_obj and campaigndeposits_obj.get('ordermoney__sum'):
#             camp_dict['campaignsumdeposit'] = campaigndeposits_obj['ordermoney__sum']
#         else:
#             camp_dict['campaignsumdeposit'] = 0
#         campaignwithdrawl_obj = Order.objects.filter(Q(direction='WITHDRAWL') & 
#                                                  Q(orderstate='FINISHED') & 
#                                                  Q(playerid__campaign=i) & Q(timestamp__gte=from_date_time) &
#                                                  Q(timestamp__lte=to_date_time)).aggregate(Sum('ordermoney'))
#         if campaignwithdrawl_obj and campaignwithdrawl_obj.get('ordermoney__sum'):
#             camp_dict['campaignsumwithdrawl'] = campaignwithdrawl_obj['ordermoney__sum']
#         else:
#             camp_dict['campaignsumwithdrawl'] = 0
        
#         '''real adjustment for camapaign '''
#         campaignradeposits_obj = Order.objects.filter(Q(playerid__campaign=i) & Q(direction='DEPOSIT')
#                                                 & Q(ordertype='REAL_ADJUSTMENT') & Q(timestamp__gte=from_date_time) &
#                                                  Q(timestamp__lte=to_date_time)).aggregate(Sum('ordermoney'))

#         if campaignradeposits_obj and campaignradeposits_obj.get('ordermoney__sum'):
#             campaignradeposits = campaignradeposits_obj['ordermoney__sum']
#         else:
#             campaignradeposits = 0
        
#         campaignrawithdrawls_obj = Order.objects.filter(Q(playerid__campaign=i) & Q(direction='WITHDRAWL') 
#                                                     & Q(ordertype='REAL_ADJUSTMENT') & Q(timestamp__gte=from_date_time) &
#                                                  Q(timestamp__lte=to_date_time)).aggregate(Sum('ordermoney'))
#         if campaignrawithdrawls_obj and campaignrawithdrawls_obj.get('ordermoney__sum'):
#             campaignrawithdrawls = campaignrawithdrawls_obj['ordermoney__sum']
#         else:
#             campaignrawithdrawls = 0
#         camp_dict['campaignrealadjustment'] = campaignradeposits - campaignrawithdrawls
        
#         camp_dict['campaigntotal'] = camp_dict['campaignsumdeposit']-camp_dict['campaignsumwithdrawl'] +\
#                          camp_dict['campaignrealadjustment']#revenue
#         #camp_dict['campaignnet'] = camp_dict['campaigntotal'] * (commission.revsharepercent / 100)
#         camp_dict['campaignnet'] = ((camp_dict['campaigntotal'] * commission.revsharepercent) / 100)+\
#                                     camp_dict['campaigncpalist']+camp_dict['campaigncprlist'] #accurals
#         #camp_dict['revsharepercent'] = commission.revsharepercent
#         total_flag = camp_dict.get("campaignregistration")+camp_dict.get("campaignhitcount")+ camp_dict.get("campaignsingletime")+\
#                     camp_dict.get("campaignsumwithdrawl")+camp_dict.get("campaignsumwithdrawl")+camp_dict.get("campaignrealadjustment")+\
#                     camp_dict.get("campaigntotal")+camp_dict.get("campaignnet")
        
#         if total_flag != 0 and date_wise:
#             resp_list.append(camp_dict)
#         elif not date_wise:
#             resp_list.append(camp_dict) 
#     return resp_list

# def get_affiliate_financial_data(aff_id, from_date_time, to_date_time, date_wise=False):
#     resp_dict = {}
#     obj = Affiliateinfo.objects.select_related('account_manager').get(id=aff_id)
#     commission = Affiliatecommission.objects.get(fk_affiliateinfo=obj)
#     resp_dict['obj'] = obj
#     if date_wise:
#         date_wise_resp_list = []
#         total_reg_count = 0
#         total_clicks_count = 0
#         total_acquitions_count = 0
#         total_deposits_amount = 0
#         total_withdrawl_amount = 0
#         total_adjustments_amount = 0
#         total_revenue_amount = 0
#         total_accurals_amount = 0
#         total_campaign_logins = 0
#         total_campaign_uniquelogins = 0
#         delta = to_date_time.date() - from_date_time.date()
#         for i in range(delta.days + 1):
#             fromdate =  datetime.combine(from_date_time.date() + timedelta (days=i), time(0,0,0))
#             todate = datetime.combine(fromdate, time(23,59,59))
#             g = get_campaign_data_for_given_dates(obj,commission, fromdate, todate, date_wise)
#             for j in g:
#                 total_reg_count += j.get("campaignregistration")
#                 total_clicks_count += j.get("campaignhitcount")
#                 total_acquitions_count += j.get("campaignsingletime")
#                 total_deposits_amount += j.get("campaignsumdeposit")
#                 total_withdrawl_amount += j.get("campaignsumwithdrawl")
#                 total_adjustments_amount += j.get("campaignrealadjustment")
#                 total_revenue_amount += j.get("campaigntotal")
#                 total_accurals_amount += j.get("campaignnet")
#                 total_campaign_logins += j.get("campaign_logins")
#                 total_campaign_uniquelogins += j.get("campaign_uniquelogins")
#             date_wise_resp_list.extend(g)
#         totals_dict = {'campaignregistration':total_reg_count,'campaignhitcount':total_clicks_count,
#                        'campaignsingletime':total_acquitions_count,'campaignsumdeposit':total_deposits_amount,
#                        'campaignsumwithdrawl':total_withdrawl_amount,'campaignrealadjustment':total_adjustments_amount,
#                        'campaigntotal':total_revenue_amount,'campaignnet':total_accurals_amount,'DateTime':"TOTAL",
#                        'campaign_logins':total_campaign_logins,'campaign_uniquelogins':total_campaign_uniquelogins}
#         date_wise_resp_list.append(totals_dict)
#         resp_dict['date_wise_data'] = date_wise_resp_list
#     resp_dict['revsharepercent'] = commission.revsharepercent
#     return resp_dict

# def affiliate_user_reports(request):
#     affID = request.session.get('affid')
#     try:
#         obj = Affiliateinfo.objects.select_related('account_manager').get(id=affID)
#     except:
#         return HttpResponseRedirect('/affiliate/')
#     resp_dict = {}
#     """
#     if not hasattr(request, 'session'):
#         return HttpResponseRedirect(reverse("affiliate"))
#     affID = request.session.get("affid")
#     if not affID:
#         return HttpResponseRedirect(reverse("affiliate"))
#     """
#     campaign_list = Campaign.objects.filter(fk_affiliateinfo__id=affID)
#     if request.method == "GET":
#         dates_dict = get_fromdate_todate_for_django_query_filter("Last 30 Days", None, None)
#         to_date_time = dates_dict.get('todate')
#         from_date_time = dates_dict.get('fromdate')
#         registertype = "new users"
#         campaign_id = None
#         date_type = "Last 30 Days"
#         bet_type = "CASH"
#     else:
#         date_type = request.POST.get('date')
#         bet_type = request.POST.get("bet_type")
#         newusers = request.POST.get('newusers')
#         oldusers = request.POST.get('oldusers')
#         registertype = None
#         if newusers == "on" and oldusers == "on":
#             registertype = "all users"
#         if not registertype and newusers == "on":
#             registertype = "new users"
#         if not registertype and oldusers == "on":
#             registertype = "old users"
#         campaign_id = request.POST.get('campaign_id')
#         if date_type and date_type.lower() != "all":
#             fromdate = request.POST.get('datepicker1')
#             todate = request.POST.get('datepicker2')
#             dates_dict = get_fromdate_todate_for_django_query_filter(date_type, fromdate, todate)
#             to_date_time = dates_dict.get('todate')
#             from_date_time = dates_dict.get('fromdate')
#         else:
#             dates_dict = get_site_player_first_register_timestamp()
#             to_date_time = dates_dict.get('todate')
#             from_date_time = dates_dict.get('fromdate')
            
#     registertype = registertype.lower() if registertype else None
#     campaign_id = campaign_id if campaign_id else campaign_list[0].id
#     users_data_dict = get_users_aff_camp_financials(affID, campaign_id, from_date_time, to_date_time,
#                                                     {'registertype':registertype,'bet_type':bet_type})
#     resp_dict['users_data_list'] = users_data_dict.get("users_data_list")
#     resp_dict['affiliate_id'] = affID
#     resp_dict['obj'] = users_data_dict.get("obj")
#     resp_dict['registertype'] = registertype.upper()
#     resp_dict['from_date'] = from_date_time
#     resp_dict['to_date'] = to_date_time
#     resp_dict['campaignID'] = campaign_id
#     resp_dict['campaign_list'] = campaign_list
#     resp_dict['custom'] = date_type
#     resp_dict['bet_type'] = bet_type
#     if date_type.lower() == "custom":
#         resp_dict['from_ui_date'] = fromdate
#         resp_dict['to_ui_date'] = todate
#     resp_dict['revsharepercent'] = users_data_dict.get("revsharepercent")
#     return render_to_response('affiliates/affiliate_user_financials.html',
#                               resp_dict, context_instance=RequestContext(request))

# def affiliate_campaign_financial(request):
#     """ player site affiliate financial display """
    
#     aff_id = request.session.get('affid')
#     try:
#         obj = Affiliateinfo.objects.select_related('account_manager').get(id=aff_id)
#     except:
#         return HttpResponseRedirect('/affiliate/')
#     """
#     if not hasattr(request, 'session'):
#         return HttpResponseRedirect(reverse("affiliate"))
#     aff_id = request.session.get("affid")
#     if not aff_id:
#         return HttpResponseRedirect(reverse("affiliate"))
#     """
#     obj = Affiliateinfo.objects.select_related('account_manager').get(id=aff_id)
#     if request.method == "GET":
#         dates_dict = get_fromdate_todate_for_django_query_filter(None, None, None)
#         to_date_time = dates_dict.get('todate')
#         from_date_time = dates_dict.get('fromdate')
#         date_type = "Today"
#     else:
#         date_type = request.POST.get('date')
#         if date_type and date_type.lower() != "all":
#             fromdate = request.POST.get('datepicker1')
#             todate = request.POST.get('datepicker2')
#             dates_dict = get_fromdate_todate_for_django_query_filter(date_type, fromdate, todate)
#             to_date_time = dates_dict.get('todate')
#             from_date_time = dates_dict.get('fromdate')
#         else:
#             dates_dict = get_site_player_first_register_timestamp()
#             to_date_time = dates_dict.get('todate')
#             from_date_time = dates_dict.get('fromdate')
#     player_objs_list = Playerinfo.objects.filter(affiliate=obj,
#                                                  registeredon__gte=from_date_time,
#                                                  registeredon__lte=to_date_time).order_by('-id').\
#                                                  values('id','username','registeredon','verified','creationip','state__statename')
#     firsttime_depositors_list = Order.objects.filter(Q(direction='DEPOSIT') & Q(firstdeposit=True) & Q(orderstate= 'FINISHED')&
#                                          Q(playerid__affiliate=obj) &
#                                            Q(timestamp__gte=from_date_time) &
#                                            Q(timestamp__lte=to_date_time)).order_by('-id').values('playerid_id','playerid__username',
#                                                                            'playerid__registeredon','playerid__state__statename',
#                                                                            'playerid__deposits')
#     resp_dict = {}
#     commission = Affiliatecommission.objects.get(fk_affiliateinfo=obj)
#     resp_dict['affiliate_info'] = [get_affiliate_data_for_give_dates(obj,commission, from_date_time,to_date_time)]
#     resp_dict['campaign_info'] = get_campaign_data_for_given_dates(obj, commission, from_date_time, to_date_time)
#     resp_dict['affiliate_id'] = id
#     resp_dict['player_objs_list'] = player_objs_list
#     resp_dict['firsttime_depositors_list'] = firsttime_depositors_list
#     resp_dict['obj'] = obj
#     #resp_dict['date_wise_data'] = affiliate_data_dict.get("date_wise_data")
#     resp_dict['custom'] = date_type
#     resp_dict['from_date'] = from_date_time
#     resp_dict['to_date'] = to_date_time
#     resp_dict['revsharepercent'] = commission.revsharepercent
#     return render_to_response('affiliates/affiliate_campaign_financial.html',
#                               resp_dict, context_instance=RequestContext(request))

# def financialhome(request, id):
#     aff_id = request.session.get('affid')
#     try:
#         obj = Affiliateinfo.objects.select_related('account_manager').get(id=aff_id)
#     except:
#         return HttpResponseRedirect('/affiliate/')
#     if request.method == "GET":
#         dates_dict = get_fromdate_todate_for_django_query_filter(None, None, None)
#         to_date_time = dates_dict.get('todate')
#         from_date_time = dates_dict.get('fromdate')
#         date_type = "Today"
#     else:
#         date_type = request.POST.get('date')
#         if date_type and date_type.lower() != "all":
#             fromdate = request.POST.get('datepicker1')
#             todate = request.POST.get('datepicker2')
#             dates_dict = get_fromdate_todate_for_django_query_filter(date_type, fromdate, todate)
#             to_date_time = dates_dict.get('todate')
#             from_date_time = dates_dict.get('fromdate')
#         else:
#             dates_dict = get_site_player_first_register_timestamp()
#             to_date_time = dates_dict.get('todate')
#             from_date_time = dates_dict.get('fromdate')
#     resp_dict = {}
#     affiliate_data_dict = get_affiliate_financial_data(obj.id, from_date_time, to_date_time, True)
#     #resp_dict['affiliate_info'] = affiliate_data_dict.get("affiliate_info")
#     #resp_dict['campaign_info'] = affiliate_data_dict.get("campaign_info")
#     resp_dict['affiliate_id'] = id
#     #resp_dict['player_objs_list'] = player_objs_list
#     #resp_dict['firsttime_depositors_list'] = firsttime_depositors_list
#     resp_dict['obj'] = obj
#     resp_dict['date_wise_data'] = affiliate_data_dict.get("date_wise_data")
#     resp_dict['custom'] = date_type
#     resp_dict['from_date'] = from_date_time
#     resp_dict['to_date'] = to_date_time
#     resp_dict['revsharepercent'] = affiliate_data_dict.get('revsharepercent')
#     return render_to_response('affiliates/affiliate-financials.html',
#                               resp_dict, context_instance=RequestContext(request))

# def affiliatefinancial(request, id):
#     obj = Affiliateinfo.objects.select_related('account_manager').get(id=id)
#     sites_list = get_brand_order(obj)
#     if request.method == "GET":
#         dates_dict = get_fromdate_todate_for_django_query_filter(None, None, None)
#         to_date_time = dates_dict.get('todate')
#         from_date_time = dates_dict.get('fromdate')
#         date_type = "Today"
#         siteid = sites_list[0].id
#     else:
#         date_type = request.POST.get('date')
#         if date_type and date_type.lower() != "all":
#             fromdate = request.POST.get('datepicker1')
#             todate = request.POST.get('datepicker2')
#             dates_dict = get_fromdate_todate_for_django_query_filter(date_type, fromdate, todate)
#             to_date_time = dates_dict.get('todate')
#             from_date_time = dates_dict.get('fromdate')
#         else:
#             #dates_dict = get_site_player_first_register_timestamp()
#             to_date_time = None
#             from_date_time = None
#         siteid = request.POST.get("siteid")
    
#     """
#     player_objs_list = Playerinfo.objects.filter(affiliate__id=id,
#                                                  registeredon__gte=from_date_time,
#                                                  registeredon__lte=to_date_time).order_by('-id').\
#                                                  values('id','username','registeredon','verified','creationip','state__statename')
#     firsttime_depositors_list = Order.objects.filter(Q(direction='DEPOSIT') & Q(firstdeposit=True) & Q(orderstate= 'FINISHED')&
#                                          Q(playerid__affiliate__id=id) &
#                                            Q(timestamp__gte=from_date_time) &
#                                            Q(timestamp__lte=to_date_time)).order_by('-id').values('playerid_id','playerid__username',
#                                                                            'playerid__registeredon','playerid__state__statename',
#                                                                            'playerid__deposits')
#     """
#     resp_dict = {}
#     #affiliate_data_dict = get_affiliate_financial_data(id, from_date_time, to_date_time, True)
#     header_list, data_list = process_campaing_statics_response(obj,[siteid], from_date_time, to_date_time)
#     commission = Affiliatecommission.objects.get( fk_affiliateinfo=obj)
#     #resp_dict['affiliate_info'] = [get_affiliate_data_for_give_dates(obj,commission, from_date_time,to_date_time)]
#     #resp_dict['campaign_info'] = get_campaign_data_for_given_dates(obj, commission, from_date_time, to_date_time)
#     #resp_dict['affiliate_info'] = affiliate_data_dict.get("affiliate_info")
#     #resp_dict['campaign_info'] = affiliate_data_dict.get("campaign_info")
#     resp_dict['affiliate_id'] = id
#     resp_dict['obj'] = obj
#     #resp_dict['player_objs_list'] = player_objs_list
#     #resp_dict['firsttime_depositors_list'] = firsttime_depositors_list
#     #resp_dict['date_wise_data'] = affiliate_data_dict.get("date_wise_data")
#     resp_dict['custom'] = date_type
#     resp_dict["sites_obj_list"] = sites_list
#     resp_dict['header_list'] = json.dumps(header_list, cls=MyEncoder, indent=4)
#     resp_dict['data_list'] = json.dumps(data_list, cls=MyEncoder, indent=4)
#     if date_type.lower() == 'Custom':
#         resp_dict['from_ui_date'] = fromdate
#         resp_dict['to_ui_date'] = todate
#     return render_to_response('affiliates/admin/affiliate-financials.html', resp_dict,
#                               context_instance=RequestContext(request))

# def get_user_activities_response(request, aff_obj):
#     resp_dict = {}
#     resp_dict['obj'] = aff_obj
#     resp_dict["affiliate_id"] = aff_obj.id
#     resp_dict["campaign_list"] = Campaign.objects.filter(fk_affiliateinfo=aff_obj)
#     if request.method == "GET": 
#         username = request.GET.get('username')
#         custom = request.GET.get('custom')
#         from_ui_date, to_ui_date = None, None
#         if custom and custom.lower()=="custom":
#             from_ui_date = request.GET.get('from_ui_date')
#             to_ui_date = request.GET.get('to_ui_date')
#             date_type = custom
#         else:
#             date_type = "Today"
#         dates_dict = get_fromdate_todate_for_django_query_filter(date_type, from_ui_date, to_ui_date)
#         to_date_time = dates_dict.get('todate')
#         from_date_time = dates_dict.get('fromdate')
#         all = "off"
#         deposits, withdrawals, adjustments, bonuscode, deductions, rake = "on","on","off", None,"on","on"
#         all_flag = False
#     else:
#         username = request.POST.get('username')
#         deposits = request.POST.get('deposits')
#         withdrawals = request.POST.get('withdrawals')
#         deductions = request.POST.get('deductions')
#         rake = request.POST.get('rakes')
#         adjustments = request.POST.get('adjustments')
#         accurals = request.POST.get('accurals')
#         bonuscode = request.POST.get('bonuscode')
#         #campaign_id = request.POST.get('campaign_id')
#         all = request.POST.get('all')
#         date_type = request.POST.get('date')
#         if date_type and date_type.lower() != "all":
#             fromdate = request.POST.get('datepicker1')
#             todate = request.POST.get('datepicker2')
#             dates_dict = get_fromdate_todate_for_django_query_filter(date_type, fromdate, todate)
#             to_date_time = dates_dict.get('todate')
#             from_date_time = dates_dict.get('fromdate')
#         else:
#             dates_dict = get_site_player_first_register_timestamp()
#             to_date_time = dates_dict.get('todate')
#             from_date_time = dates_dict.get('fromdate')

#     direction_list = []
#     if not username or username == "":
#         userID = ""
#         today_date = datetime.now()
#         player_ids_list = []
#         player_objs_list = []
#         ff = Order.objects.filter(timestamp__day = today_date.day,playerid__affiliate=aff_obj,
#                              timestamp__month = datetime.now().month, 
#                              timestamp__year=datetime.now().year,orderstate="FINISHED").\
#                              exclude(modeofdeposit='MANUAL_ADMIN').order_by('-id')
#         for l in ff:
#             if l.playerid_id not in  player_ids_list:
#                 player_ids_list.append(l.playerid_id)
#                 player_objs_list.append(l.playerid)        
#         re = Reconcilelogs.objects.filter(timestamp__day = today_date.day,playerid__affiliate=aff_obj,
#                              timestamp__month = datetime.now().month, 
#                              timestamp__year=datetime.now().year,transaction_type="WAGERING",
#                              amounttype="CASH").order_by('-id')
#         for k in re:
#             if k.playerid_id not in  player_ids_list:
#                 player_ids_list.append(k.playerid_id)
#                 player_objs_list.append(k.playerid)
#     else:
#         player_objs_list = Playerinfo.objects.filter(username=username)
#         if len(player_objs_list) == 0:
#             resp_dict["error_message"] = "Player Does not Exist in database."
#             return resp_dict
#         userID = player_objs_list[0].id
#     try:
#         commission = Affiliatecommission.objects.get(fk_affiliateinfo=aff_obj)
#     except Affiliatecommission.DoesNotExist:
#         commission = Affiliatecommission()
#         commission.revsharepercent = 0
#     resp_dict['revsharepercent'] = commission.revsharepercent
    
#     all_flag, deposits_flag, withdrawals_flag, adjustments_flag, deductions_flag, rakes_flag, accurals_flag = False,\
#                     False, False,False,True,False,True
#     if all and str(all) == "on":
#         deposits, withdrawals, adjustments, bonuscode, deductions, rake = "on","on","on", None,"on","on"
#         all_flag = True
#     if deposits and str(deposits) == 'on':
#         direction_list.append("DEPOSIT")
#         deposits_flag = True
#     if withdrawals and str(withdrawals) == "on":
#         direction_list.append("WITHDRAWL")
#         withdrawals_flag = True
        
#     ordertype = ""
#     if adjustments and str(adjustments) == "on":
#         if not direction_list:
#             direction_list = ["DEPOSIT", "WITHDRAWL"]
#         ordertype = "REAL_ADJUSTMENT"
#         adjustments_flag = True
#     resp_list = []
#     for player_obj in player_objs_list:
#         order_objs = []
#         if direction_list: 
#             order_objs = Order.objects.filter(direction__in=direction_list, playerid=player_obj,orderstate="FINISHED",
#                                               timestamp__lte=to_date_time,timestamp__gte=from_date_time).order_by('-id')
#         if bonuscode and str(bonuscode)=="None":
#             if order_objs:
#                 order_objs = order_objs.filter(bonuscode=bonuscode)
#             else:
#                 order_objs = Order.objects.filter(bonuscode=bonuscode, playerid=player_obj,orderstate="FINISHED",
#                                                   timestamp__lte=to_date_time,timestamp__gte=from_date_time).order_by('-id')
        
#         if ordertype:
#             order_objs = order_objs.filter(ordertype=ordertype)
        
#         if deductions and str(deductions) == "on":
#             if not direction_list:
#                 order_objs = Order.objects.filter(fee__isnull=False, 
#                                                   playerid=player_obj,orderstate="FINISHED",
#                                                   timestamp__lte=to_date_time,timestamp__gte=from_date_time).order_by('-id')
#         games_rake_list = []
#         if rake and str(rake)=="on":
#             rakes_flag = True
#             cursor = connection.cursor()
#             to_date_time_str = to_date_time.strftime("%Y-%m-%d %H:%M:%S")
#             from_date_time_str = from_date_time.strftime("%Y-%m-%d %H:%M:%S")
#             cursor.execute("""SELECT  g.tableid, g.gameid, pin.timestamp, pin.investedamount, 
#                 r.rakemoney, gs.rake, s.id, gs.id, gs.bet
#                 FROM userapp_playersinvolved pin
#                 INNER JOIN  userapp_game g ON g.id= pin.fkgame_id
#                 INNER JOIN userapp_rakelogs r ON g.gameid=r.fkgame_id
#                 INNER JOIN userapp_gamesetting gs ON g.gamesettingid = gs.id
#                 INNER JOIN userapp_stream s ON s.id=g.streamid
#                 WHERE pin.playerid_id=%s AND pin.investedamount IS NOT NULL
#                 AND s.fromchips=%s AND s.tochips=%s AND
#                 pin.timestamp BETWEEN %s AND %s ORDER BY pin.timestamp DESC;""",(player_obj.id, "CASH", "CASH", from_date_time_str, to_date_time_str))
#             results = cursor.fetchall()
#             for j in results:
#                 temp = []
#                 temp.append("TableID - %s"%(j[0]))
#                 temp.append(j[2])
#                 temp.append("Gameplay")
#                 temp.append(j[3])
#                 temp.append("%s"%j[5]+"%")
#                 temp.append(j[3] * commission.revsharepercent / 100)
#                 temp.append("StreamID : %s  GamesettingID : %s  Bet : %s"%(j[6], j[7], j[8]))
#                 games_rake_list.append(temp)
#         for i in order_objs:
#             temp = []
#             temp.append("%s-%s"%("orderID",i.id))
#             temp.append(i.timestamp)
#             if ordertype:
#                 temp.append("%s - %s"%(i.direction, ordertype))
#             else:
#                 temp.append(i.direction)
#             temp.append(i.ordermoney)
#             temp.append(i.fee)
#             if i.direction == "DEPOSIT":
#                 temp.append(i.ordermoney * commission.revsharepercent / 100)
#             else:
#                 temp.append("NA")
#             temp.append("")
#             resp_list.append(temp)
#         if games_rake_list:
#             resp_list.extend(games_rake_list)
#     timestamps_list = [i[1] for i in resp_list]
#     sorted_timestamps_list = sorted(range(len(timestamps_list)), key=lambda k: timestamps_list[k], reverse=True)
#     sorted_resp_list = [resp_list[k] for k in sorted_timestamps_list]
#     resp_dict["acitivites_list"] = sorted_resp_list
#     resp_dict['campaignID'] = player_obj.campaign.name if username else "N.A"
#     resp_dict['username'] = username if username else "N.A"
#     resp_dict['userid'] = userID
#     resp_dict['from_date'] = from_date_time
#     resp_dict['to_date'] = to_date_time
#     resp_dict['all_flag'] = all_flag
#     resp_dict['deposits_flag'] = deposits_flag
#     resp_dict['withdrawals_flag'] = withdrawals_flag
#     resp_dict['adjustments_flag'] = adjustments_flag
#     resp_dict['deductions_flag'] = deductions_flag
#     resp_dict['rakes_flag'] = rakes_flag
#     resp_dict['accurals_flag'] = accurals_flag
#     resp_dict['custom'] = date_type
#     return resp_dict


# @check_ip_required
# @staff_member_required
# def admin_affiliate_user_activities(request, id):
#     aff_obj = Affiliateinfo.objects.select_related('account_manager').get(id=id)
#     response_dict = get_user_activities_response(request, aff_obj)
#     return render_to_response('affiliates/admin/affiliate_users_activities.html', response_dict,
#                               context_instance=RequestContext(request))

# def affiliate_user_activities(request):
#     aff_id = request.session.get("affid") 
#     try:
#         aff_obj = Affiliateinfo.objects.select_related('account_manager').get(id=aff_id)
#     except Affiliateinfo.DoesNotExist:
#         return HttpResponseRedirect("/affiliate/")
#     response_dict = get_user_activities_response(request, aff_obj)
#     response_dict["obj"] = aff_obj
#     return render_to_response('affiliates/affiliate_users_activities.html', response_dict,
#                               context_instance=RequestContext(request))

# @check_ip_required
# @staff_member_required
# def admin_affiliate_user_reports(request, id):
#     resp_dict = {}
#     campaign_list = Campaign.objects.filter(fk_affiliateinfo__id=id)
    
#     if request.method == "GET":
#         dates_dict = get_fromdate_todate_for_django_query_filter(None, None, None)
#         to_date_time = dates_dict.get('todate')
#         from_date_time = dates_dict.get('fromdate')
#         registertype = "new users"
#         campaign_id = None
#         date_type = "Today"
#     else:
#         date_type = request.POST.get('date')
#         registertype = request.POST.get('registertype')
#         newusers = request.POST.get('newusers')
#         oldusers = request.POST.get('oldusers')
#         registertype = None
#         if newusers == "on" and oldusers == "on":
#             registertype = "all users"
#         if not registertype and newusers == "on":
#             registertype = "new users"
#         if not registertype and oldusers == "on":
#             registertype = "old users"
#         campaign_id = request.POST.get('campaign_id')
#         if date_type and date_type.lower() != "all":
#             fromdate = request.POST.get('datepicker1')
#             todate = request.POST.get('datepicker2')
#             dates_dict = get_fromdate_todate_for_django_query_filter(date_type, fromdate, todate)
#             to_date_time = dates_dict.get('todate')
#             from_date_time = dates_dict.get('fromdate')
#         else:
#             dates_dict = get_site_player_first_register_timestamp()
#             to_date_time = dates_dict.get('todate')
#             from_date_time = dates_dict.get('fromdate')

#     registertype = registertype.lower() if registertype else None
#     campaign_id = campaign_id if campaign_id else campaign_list[0].id
#     users_data_dict = get_users_aff_camp_financials(id, campaign_id, from_date_time, to_date_time,
#                                                     {'registertype': registertype})
#     resp_dict['users_data_list'] = users_data_dict.get("users_data_list")
#     resp_dict['affiliate_id'] = id
#     resp_dict['obj'] = users_data_dict.get("obj")
#     resp_dict['registertype'] = registertype
#     resp_dict['from_date'] = from_date_time
#     resp_dict['to_date'] = to_date_time
#     resp_dict['campaignID'] = campaign_id
#     resp_dict['campaign_list'] = campaign_list
#     resp_dict['custom'] = date_type
#     resp_dict['revsharepercent'] = users_data_dict.get("revsharepercent")
#     return render_to_response('affiliates/admin/affiliate_users_financial.html', resp_dict,
#                               context_instance=RequestContext(request))


def generate_custom_url(request, template):
    resp_dict = {}
    resp_dict["status"] = False
    if request.method == "GET":
        camp_id = request.GET.get("camp_id")
        if not camp_id:
            return HttpResponse("campaign id is a mandatory parameter.")
        
        try:
            camp_obj = Campaign.objects.select_related('networkid',
                                                      'siteid', 'fk_affiliateinfo').get(id=camp_id)
        except:
            return HttpResponse("Given cam_id does not exit.")
        resp_dict["camp_obj"] = camp_obj
        resp_dict["obj"] = camp_obj.fk_affiliateinfo
        return render(request, template, resp_dict)
        # return render_to_response(template, resp_dict,
        #                       context_instance=RequestContext(request))
    
    if request.method == "POST":
        camp_id = request.POST.get("camp_id")
        if not camp_id:
            return HttpResponse("campaign id is a mandatory parameter.")
        
        try:
            camp_obj = Campaign.objects.select_related('networkid',
                                                      'siteid', 'fk_affiliateinfo').get(id=camp_id)
        except:
            return HttpResponse("Given cam_id does not exit.")
        
        campaign_name = camp_obj.name
        campaign_source = camp_obj.fk_affiliateinfo.username
        campaign_term = request.POST.get("campaign_term", "keyword")
        campaign_medium = request.POST.get("campaign_medium", "banner")
        campaign_content = request.POST.get("campaign_content", "signup")
        
        campaign_term = "keyword" if not campaign_term else campaign_term
        campaign_medium = "banner" if not campaign_medium else campaign_medium
        campaign_content = "signup" if not campaign_content else campaign_content
            
            
        params = { "camp_id":camp_obj.id,
                  "utm_source":campaign_source,
                  "utm_medium":campaign_medium,
                  "utm_campaign":campaign_name,
                  "utm_term":campaign_term,
                  "utm_content":campaign_content}
        link = get_affiliate_campaign_link(camp_obj.siteid.domain, camp_obj, params)
        linkurl = link
        camp_obj.link = linkurl
        camp_obj.save()
        resp_dict["camp_obj"] = camp_obj
        resp_dict["obj"] = camp_obj.fk_affiliateinfo
        resp_dict["message"] = "Link updated successfully."
        resp_dict["status"] = True
        if request.POST.get('type') == "json":
            resp = {"status":"success","url":camp_obj.link}
            return HttpResponse(json.dumps(resp,cls=MyEncoder))
    return render(request, template, resp_dict)
    # return render_to_response(template, resp_dict,
    #                           context_instance=RequestContext(request))


@affiliate_login_required
def custom_url_builder(request):
    template = 'affiliates/custom-campaign-url-build.html'
    return generate_custom_url(request, template)

@check_ip_required
@staff_member_required
def admin_custom_campaign_url(request):
    template = 'affiliates/admin/custom-campaign-url.html'
    return generate_custom_url(request, template)

@csrf_exempt
def wyntacsave(request):
    resp_dict = {}
    resp_dict["status"] = False
    if request.method == "POST":
        # form = ContactUsForm(request.POST)
        # resp_dict['form'] = form
        try:
            # if form.is_valid():
            email = request.POST.get("email")
            company = request.POST.get("companyname")
            contact_obj = WyntaSupport.objects.create(
                            email=email,
                            companyname=company)
            contact_obj.save()
            resp_dict["status"] = True
            resp_dict["message"] = 'Information is logged. We will get back to you soon.'
        except Exception as e:
            resp_dict["message"] = 'Information could not logged. We will get back to you soon.'
    return HttpResponse(json.dumps(resp_dict))


def contact_us(request):
    resp_dict = {}
    resp_dict["status"] = False
    if request.method == "GET":
        # form = ContactUsForm()
        resp_dict["status"] = True
        # resp_dict['form'] = form
        return render(request, 'affiliates/contact-us1.html', resp_dict)
        # return render_to_response('affiliates/contact-us1.html', resp_dict,
        #                       context_instance=RequestContext(request))

    if request.method == "POST":
        # form = ContactUsForm(request.POST)
        # resp_dict['form'] = form
        try:
            # if form.is_valid():
            name = request.POST.get("name")
            email = request.POST.get("email")
            username = request.POST.get("username")
            subject = request.POST.get("subject")
            message = request.POST.get("message")
            contact_obj = AffiliateContactUs(name=name,
                            email=email,
                            username=username,
                            subject=subject,
                            message=message)
            contact_obj.save()
            resp_dict["status"] = True
            resp_dict["message"] = 'we will get back to you shortly!'
        except Exception as e:
            resp_dict["message"] = 'we will get back to you shortly!'
            resp_dict["Error"] = str(e)
    return HttpResponse(json.dumps(resp_dict, indent=4))


@affiliate_login_required
def get_messages(request):
    context_dict = {}
    sessionid = get_affiliate_session_id(request)
    affid = sessionid
    if not affid:
        return HttpResponse("affid is manadatory parameter.")

    obj = Affiliateinfo.objects.select_related('account_manager').get(id=affid)
    request.user.username = obj.username
    request.user.email = obj.email
    messages_list = []
    site_name = 'All Websites'
    msg_logs = Messagelogs.objects.filter(affiliate=obj, to_email=obj.email)
    if msg_logs:
        for msg_l in msg_logs:
            messages_list.append({'id': str(msg_l.id),
                'date': str(msg_l.timestamp),
                'from': msg_l.from_email,
                'status': msg_l.get_status_display(),
                'subject': msg_l.messagetrigger.templates.subject,
                'content': msg_l.messagetrigger.triggercondition.replace('_', ' ')
                # 'content': msg_l.messagetrigger.templates.content,
                })
    context_dict['domain'] = site_name
    context_dict["messages_list"] = messages_list
    context_dict["aff_obj"] = obj
    if obj.account_manager:
        context_dict["partner"] = obj.account_manager.affiliateprogramme.name
        context_dict["partner_obj"] = obj.account_manager.affiliateprogramme
    return render(request, 'affiliates/affiliate-message.html', context_dict)
    # return render_to_response('affiliates/affiliate-message.html',
    #                           context_dict,
    #                           context_instance=RequestContext(request))


@affiliate_login_required
def delete_message(request):
    context_dict = {}
    msgid = request.GET.get("id")
    print 'delete start', msgid
    msg_logs = Messagelogs.objects.get(id=msgid)
    msg_logs.status = 'D'
    msg_logs.save()
    print 'msg status', msg_logs.status
    print 'sucess deleted message'
    return HttpResponseRedirect(reverse("affiliate_messages"))

@affiliate_login_required
def message_view(request):
    context_dict = {}
    sessionid = get_affiliate_session_id(request)
    affID = get_affiliate_session_id(request)
    try:
        obj = Affiliateinfo.objects.select_related('account_manager').get(id=affID)
        request.user.username = obj.username
        request.user.email = obj.email
    except:
        return HttpResponseRedirect(reverse("affiliate_login"))
    m_id = request.GET.get('m_id')
    view_id = request.GET.get('view', 0)
    if request.method == 'POST':
        to_email = request.POST.getlist('to_email')
        from_email = request.POST.get('from_email')
        subject = request.POST.get('subject')
        content = request.POST.get('content')
        try:
            emails_list = to_email
            custom_affiliate_email(obj,None, obj.account_manager, emails_list, from_email, content, subject)
            resp = 'Success'
        except Exception as e:
            print str(e)
            resp = 'Error'
        context_dict = {'from': obj.email, 'sent': resp, 'to': obj.account_manager.user.email}
        context_dict['status'] = 'Success'
        return HttpResponse(json.dumps(context_dict, indent=4))
    else:
        if int(view_id) == 1:
            message_obj = Messagelogs.objects.get(id=m_id)
            message_obj.affstatus = 'R'
            message_obj.save()
            s_r = 'Received'
            if message_obj.from_email == obj.email:
                s_r = 'Sent'
            data_dict = {'from': message_obj.from_email, 'to': message_obj.to_email,
                            'subject': message_obj.subject,
                            'content': message_obj.content,
                            'timestamp': str(message_obj.timestamp),
                            's_r': s_r}
        elif int(view_id) == 0:
            if m_id:
                message_obj = Messagelogs.objects.get(id=m_id)
                message_obj.affstatus = 'D'
                message_obj.save()
                context_dict['status'] = 'Success'
                return HttpResponse(json.dumps(context_dict, indent=4))
            data_dict = {'from': obj.email, 'to': obj.account_manager.user.email}
    context_dict['data'] = data_dict
    if obj.account_manager:
        context_dict["partner"] = obj.account_manager.affiliateprogramme.name
        context_dict["partner_obj"] = obj.account_manager.affiliateprogramme
    # context_dict["channels_list"] = get_channels_list()
    # context_dict["channel"] = channel if channel else "All"
    return render(request, 'affiliates/messages.html', context_dict)

@affiliate_login_required
def messages_list(request):
    number_of_items = 10000
    nexting_page = 0
    previous_page = 0
    starting_page = 0
    page = 0
    status_dict = dict(Messagelogs.STATUS_TYPE)
    context_dict = {}
    sessionid = get_affiliate_session_id(request)
    affID = get_affiliate_session_id(request)
    try:
        obj = Affiliateinfo.objects.select_related('account_manager').get(id=affID)
        request.user.username = obj.username
        request.user.email = obj.email
    except:
        return HttpResponseRedirect(reverse("affiliate_login"))
    try:
        header_list = [
                   { "title": "From"},
                   { "title": "To"},
                   { "title": "Sent/Received"},
                   { "title": "Status"},
                   { "title": "Time"},
                   { "title": "View"},
                   { "title": "Action"}
               ]
        data_list = []
        messages_objs = Messagelogs.objects.filter(Q(affiliate=obj)
                            ).exclude(affstatus='D').order_by('-timestamp')
        for message_obj in messages_objs:
            del_ele = '<button class="btn btn-danger" onclick="savestatus(this)" hrefs="/affiliate/message-status/?m_id='+str(message_obj.id)+'">Delete</button>' 
            views = '<a href="/affiliate/message-status/?m_id='+str(message_obj.id)+'&view=1"><button class="btn btn-purple" >View</button></a>'
            s_r = 'Received'
            if message_obj.from_email == obj.email:
                s_r = 'Sent'
            data_list.append([message_obj.from_email, 
                            message_obj.to_email, s_r, status_dict[message_obj.affstatus],
                            str(message_obj.timestamp), views, del_ele])
    except Exception as e:
        print 'error with dump files ', str(e)
        context_dict['error'] = str(e)
    context_dict['data_list'] = json.dumps(data_list)
    context_dict["header_list"] = json.dumps(header_list)
    if obj.account_manager:
        context_dict["partner"] = obj.account_manager.affiliateprogramme.name
        context_dict["partner_obj"] = obj.account_manager.affiliateprogramme
    # context_dict["channels_list"] = get_channels_list()
    # context_dict["channel"] = channel if channel else "All"
    return render(request, 'affiliates/messages_list.html', context_dict)


@affiliate_login_required
def get_faq(request):
    context_dict = {}
    sessionid = get_affiliate_session_id(request)
    affid = sessionid
    if not affid:
        return HttpResponse("affid is manadatory parameter.")

    obj = Affiliateinfo.objects.select_related('account_manager').get(id=affid)
    request.user.username = obj.username
    request.user.email = obj.email
    if obj.account_manager:
        context_dict["partner"] = obj.account_manager.affiliateprogramme.name
        context_dict["partner_obj"] = obj.account_manager.affiliateprogramme
    context_dict["aff_obj"] = obj
    return render(request, 'affiliates/affiliate-faq.html', context_dict)
    # return render_to_response('affiliates/affiliate-faq.html',
    #                           context_dict,
    #                           context_instance=RequestContext(request))


@affiliate_login_required
def affiliate_profile(request):
    context_dict = {}
    sessionid = get_affiliate_session_id(request)
    affid = sessionid
    if not affid:
        return HttpResponse("affid is manadatory parameter.")

    obj = Affiliateinfo.objects.select_related('account_manager').get(id=affid)
    request.user.username = obj.username
    request.user.email = obj.email
    aff_com_list = Affiliatecommission.objects.filter( fk_affiliateinfo__in=[affid])
    aff_com_dict = dict( (i.fk_affiliateinfo_id, i)  for i in aff_com_list)
    data_list = []
    if obj.account_manager:
        context_dict["partner"] = obj.account_manager.affiliateprogramme.name
        context_dict["partner_obj"] = obj.account_manager.affiliateprogramme
    context_dict["aff_obj"] = obj
    context_dict["commission_obj"] = aff_com_dict.get(obj.id)
    return render(request, 'affiliates/affiliate-profile-1.html', context_dict)
    # return render_to_response('affiliates/affiliate-profile-1.html',
    #                           context_dict,
    #                           context_instance=RequestContext(request))

@affiliate_login_required
def affiliate_profile_update(request):
    cprpercent = request.POST.get("cpr")
    cpapercent = request.POST.get("cpa")
    rspercent = request.POST.get("rsshare")

    rscheck = request.POST.get("rscheck")
    cpacheck = request.POST.get("cpacheck")
    cprcheck = request.POST.get("cprcheck")
    sessionid = get_affiliate_session_id(request)
    affid = sessionid
    if not affid:
        return HttpResponse("affiliateid is a mandatory parameter.")
    try:
        aff_obj = Affiliateinfo.objects.select_related('account_manager').get(id=affid)
        affcomm = Affiliatecommission.objects.get(fk_affiliateinfo=aff_obj)

        if rscheck == "on":
            affcomm.revshare = True
        else:
            affcomm.revshare = False

        if cpacheck == "on":
            affcomm.cpachoice = True
        else:
            affcomm.cpachoice = False

        if cprcheck == "on":
            affcomm.referalchoice = True
        else:
            affcomm.referalchoice = False

        if rspercent:
            affcomm.revsharepercent = rspercent if rspercent else 0
        if cpapercent:
            affcomm.cpacommissionvalue = cpapercent if cpapercent else 0
        if cprpercent:
            affcomm.referalcommissionvalue = cprpercent if cprpercent else 0
        affcomm.save()
    except:
        pass
    return HttpResponseRedirect(reverse("affiliate_profile"))


def comingsoonpostlogin(request):
    return render(request, 'comingsoonpostlogin.html', {})
    # return render_to_response('comingsoonpostlogin.html',
    #                           {}, context_instance=RequestContext(request))


import smtplib

def send_support_mail(Email, Subject, Message):
    try:
        Subject = "%s - %s" % (Subject, get_mail_subject())
        # toemail = Limit.objects.filter(id=1)
        To = 'info@fozilmedia.com'
        server = smtplib.SMTP('localhost')
        Message = string.join(("From: %s" % Email,
                               "To: %s" % To,
                               "Subject: %s" % Subject,
                               # "",
                               "\r\n",
                               Message
                               ), "\r\n")
        print Email, To, Message
        server.sendmail(Email, [To], Message)
        server.quit()
    except Exception as e:
        print e


def fp_contact(request):
    try:
        name = request.POST.get("name", None)
        email = request.POST.get("email" , None)
        mobile = request.POST.get("mobile", None)
        message = request.POST.get("message", None)

        if not name and not email and not mobile and not message:
            return render(request, 'fp-index.html', locals())
            # return render_to_response("fp-index.html", locals(),
            #                           context_instance=RequestContext(request))
        Subject = "FP Page - Support"
        if mobile:
            Subject = "%s - Mobile : %s" % (Subject, mobile)
        # Support.objects.create(name=name, email=email, subject=Subject, message=message)
        send_support_mail(email, Subject, message)
    except Exception as e:
        print "Error in support Mail", e
    message = True
    return render(request, 'fp-index.html', locals())
    # return render_to_response("fp-index.html", locals(),
    #                           context_instance=RequestContext(request))


@affiliate_login_required
def affchangepassword(request):
    context_dict = {}
    sessionid = get_affiliate_session_id(request)
    affid = sessionid
    if not affid:
        return HttpResponse("affid is manadatory parameter.")
    obj = Affiliateinfo.objects.select_related('account_manager').get(id=affid)
    request.user.username = obj.username
    request.user.email = obj.email
    if request.method == 'POST':
        currentpass = request.POST.get('currentpass')
        newpass = request.POST.get('newpass')
        confirmpass = request.POST.get('confirmpass')
        try:
            if newpass != confirmpass:
                context_dict['status'] = 'Error'
                context_dict['message'] = 'New Password and confirm Password does not match.'
            else:
                # dbpassword = obj.password
                # signinpass = enc_password(currentpass)
                # a = dbpassword.split('$')
                # hashdb = str(a[2])
                # salt = str(a[1])
                # usrhash = get_hexdigest(a[0], a[1], currentpass)
                # if usrhash != hashdb:
                #     context_dict['status'] = 'Error'
                #     context_dict['message'] = 'The password entered is incorrect.'
                # else:
                passwd = enc_password(newpass)   # convert password into hash code
                obj.password = passwd
                obj.save()
                context_dict['status'] = 'Success' 
                context_dict['message'] = 'Password Changed Successfully'
        except Exception as e:
            context_dict['status'] = 'Error'
            context_dict['message'] = 'Issue in reseting your password.'
    if obj.account_manager:
        context_dict["partner"] = obj.account_manager.affiliateprogramme.name
        context_dict["partner_obj"] = obj.account_manager.affiliateprogramme
    context_dict["aff_obj"] = obj
    return render(request, 'affiliates/passwordchange.html', context_dict)



@affiliate_login_required
def whats_new(request):
    context_dict = {}
    sessionid = get_affiliate_session_id(request)
    affID = get_affiliate_session_id(request)
    try:
        obj = Affiliateinfo.objects.select_related('account_manager').get(id=affID)
        request.user.username = obj.username
        request.user.email = obj.email
    except:
        return HttpResponseRedirect(reverse("affiliate_login"))
    header_list = [
                { "title": "Notification Type"},
                { "title": "Title"},
                { "title": "Content"},
                { "title": "Redirect URL"}
            ]
    data_list = []
    notifications_obj =  Notifications.objects.filter(accmanager=AccountManager.objects.get(id=7)).filter((Q(category='Affiliate')) | (Q(category='Both'))).order_by('-id')
    if notifications_obj:    
        for i in notifications_obj:
            if i.notificationtype and i.content and i.redirecturl:    
                if i.notificationtype == 'F':
                    notificationtype = 'New Feature'
                elif i.notificationtype == 'A':
                    notificationtype = 'Announcement'
                else:
                    notificationtype = 'New Release'
                redirectURL = '<a href="'+ i.redirecturl +'" class="btn btn-success pr-2 pl-2 shadow-none" target="_blank">Click Here</a>'
                data_list.append([notificationtype,i.title,i.content,redirectURL])
    context_dict['data_list'] = json.dumps(data_list)
    context_dict["header_list"] = json.dumps(header_list)
    context_dict['acc_manager'] = json.dumps('affiliate')
    context_dict["page_view"] = json.dumps('affiliate')
    if obj.account_manager:
        context_dict["partner"] = obj.account_manager.affiliateprogramme.name
        context_dict["partner_obj"] = obj.account_manager.affiliateprogramme
    print('acc manager id', context_dict['acc_manager'])
    return render(request,'affiliates/whats-new.html', context_dict)


def get_notifications(request):
    context_dict = {}
    sessionid = get_affiliate_session_id(request)
    affID = get_affiliate_session_id(request)
    try:
        obj = Affiliateinfo.objects.select_related('account_manager').get(id=affID)
        request.user.username = obj.username
        request.user.email = obj.email
    except:
        return HttpResponseRedirect(reverse("affiliate_login"))
    if request.method == 'GET':
        lastseen = datetime.now()
        noti_acc_obj = Notifications.objects.filter(affinfo=obj)
        if noti_acc_obj:
            before_last_seen = noti_acc_obj[0].lastseen
        else:
            before_last_seen = None
            noti_acc_obj = Notifications.objects.create(affinfo=obj)
        if before_last_seen:    
            notifications_obj = Notifications.objects.filter(accmanager=AccountManager.objects.get(id=7),
                                                            lastupdated__lte=lastseen, lastupdated__gte=before_last_seen).filter((Q(category='Affiliate')) | (Q(category='Both'))).order_by('-id')
        else:
            notifications_obj = Notifications.objects.filter(accmanager=AccountManager.objects.get(id=7),
                                                            lastupdated__lte=lastseen).filter((Q(category='Affiliate')) | (Q(category='Both'))).order_by('-id')
        
        print('notifications',notifications_obj)
    
        notifications = []
        if notifications_obj:
            for i in notifications_obj:
                notification = {}
                notification['id'] = i.id
                notification['notificationtype'] = i.notificationtype
                notification['title'] = i.title
                notification['content'] = i.content
                notification['redirecturl'] = i.redirecturl
                notifications.append(notification)
    if request.method == 'POST':
        lastseen = datetime.now()
        noti_acc_obj = Notifications.objects.filter(affinfo=obj)
        noti_acc_obj.update(lastseen=lastseen)
        notifications = []
    return HttpResponse(json.dumps({'notifications' : notifications, 'status' : 'Success'}))
