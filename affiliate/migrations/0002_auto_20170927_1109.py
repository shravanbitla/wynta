# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
        ('userapp', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('affiliate', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='offer',
            name='networkid',
            field=models.ForeignKey(db_column=b'networkid', blank=True, to='userapp.Network', null=True),
        ),
        migrations.AddField(
            model_name='offer',
            name='siteid',
            field=models.ForeignKey(db_column=b'siteid', blank=True, to='sites.Site', null=True),
        ),
        migrations.AddField(
            model_name='notes',
            name='fk_affiliateinfo',
            field=models.ForeignKey(to='affiliate.Affiliateinfo', null=True),
        ),
        migrations.AddField(
            model_name='notes',
            name='siteid',
            field=models.ForeignKey(db_column=b'siteid', blank=True, to='sites.Site', null=True),
        ),
        migrations.AddField(
            model_name='mediacategory',
            name='networkid',
            field=models.ForeignKey(db_column=b'networkid', blank=True, to='userapp.Network', null=True),
        ),
        migrations.AddField(
            model_name='mediacategory',
            name='siteid',
            field=models.ForeignKey(db_column=b'siteid', blank=True, to='sites.Site', null=True),
        ),
        migrations.AddField(
            model_name='mappingparameters',
            name='client',
            field=models.ForeignKey(to='affiliate.Advertiser'),
        ),
        migrations.AddField(
            model_name='hitcount',
            name='device_info_id',
            field=models.ForeignKey(db_column=b'device_info_id', blank=True, to='userapp.Device_info', null=True),
        ),
        migrations.AddField(
            model_name='hitcount',
            name='fk_affiliateinfo',
            field=models.ForeignKey(blank=True, to='affiliate.Affiliateinfo', null=True),
        ),
        migrations.AddField(
            model_name='hitcount',
            name='fk_campaign',
            field=models.ForeignKey(blank=True, to='affiliate.Campaign', null=True),
        ),
        migrations.AddField(
            model_name='currency',
            name='siteid',
            field=models.ForeignKey(blank=True, to='sites.Site', null=True),
        ),
        migrations.AddField(
            model_name='chequedetails',
            name='fk_affiliateinfo',
            field=models.ForeignKey(blank=True, to='affiliate.Affiliateinfo', null=True),
        ),
        migrations.AddField(
            model_name='campaigntrackermapping',
            name='fk_affiliateinfo',
            field=models.ForeignKey(db_column=b'fk_affiliateinfo_id', blank=True, to='affiliate.Affiliateinfo', null=True),
        ),
        migrations.AddField(
            model_name='campaigntrackermapping',
            name='fk_campaign',
            field=models.ForeignKey(db_column=b'fk_campaign_id', blank=True, to='affiliate.Campaign', null=True),
        ),
        migrations.AddField(
            model_name='campaign',
            name='fk_affiliateinfo',
            field=models.ForeignKey(to='affiliate.Affiliateinfo', null=True),
        ),
        migrations.AddField(
            model_name='campaign',
            name='media_category',
            field=models.ForeignKey(db_column=b'media_category', to='affiliate.MediaCategory', null=True),
        ),
        migrations.AddField(
            model_name='campaign',
            name='networkid',
            field=models.ForeignKey(db_column=b'networkid', blank=True, to='userapp.Network', null=True),
        ),
        migrations.AddField(
            model_name='campaign',
            name='siteid',
            field=models.ForeignKey(db_column=b'siteid', blank=True, to='sites.Site', null=True),
        ),
        migrations.AddField(
            model_name='bannerlogs',
            name='fk_bannerimages',
            field=models.ForeignKey(blank=True, to='affiliate.BannerImages', null=True),
        ),
        migrations.AddField(
            model_name='bannerlogs',
            name='fk_campaign',
            field=models.ForeignKey(blank=True, to='affiliate.Campaign', null=True),
        ),
        migrations.AddField(
            model_name='bannerimages',
            name='user',
            field=models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='affiliatepasswordresetlogs',
            name='affiliateid',
            field=models.ForeignKey(to='affiliate.Affiliateinfo'),
        ),
        migrations.AddField(
            model_name='affiliateinfo',
            name='networkid',
            field=models.ForeignKey(db_column=b'networkid', blank=True, to='userapp.Network', null=True),
        ),
        migrations.AddField(
            model_name='affiliateinfo',
            name='siteid',
            field=models.ManyToManyField(to='sites.Site', null=True, blank=True),
        ),
        migrations.AddField(
            model_name='affiliateinfo',
            name='superaffiliate',
            field=models.ForeignKey(db_column=b'superaffiliateid', blank=True, to='affiliate.Affiliateinfo', null=True),
        ),
        migrations.AddField(
            model_name='affiliatecommission',
            name='fk_affiliateinfo',
            field=models.ForeignKey(blank=True, to='affiliate.Affiliateinfo', null=True),
        ),
        migrations.AddField(
            model_name='advertiser',
            name='account_manager',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
