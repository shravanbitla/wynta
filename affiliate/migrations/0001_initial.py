# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import affiliate.models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Advertiser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('link', models.TextField()),
                ('description', models.TextField()),
                ('name', models.CharField(unique=True, max_length=30)),
                ('created_timestamp', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.CreateModel(
            name='Affiliatecommission',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('revshare', models.BooleanField(default=False, verbose_name=b'RevShare Flag')),
                ('revsharepercent', models.IntegerField(null=True, verbose_name=b'Revshare Percent', blank=True)),
                ('cpachoice', models.BooleanField(default=False, verbose_name=b'CPA Flag')),
                ('criteriaacquisition', affiliate.models.EnumField(blank=True, max_length=104, null=True, verbose_name=b'Criteria', choices=[(b'FTD', b'FTD'), (b'Register', b'Register'), (b'Deposit', b'Deposit')])),
                ('criteriavalue', models.FloatField(default=0.0, verbose_name=b'Criteria Value')),
                ('cpacommissionvalue', models.IntegerField(null=True, verbose_name=b'CPA Commision Value', blank=True)),
                ('referalchoice', models.BooleanField(default=False, verbose_name=b'Referal Flag', db_column=b'cprchoice')),
                ('referalcommissionvalue', models.IntegerField(null=True, verbose_name=b'Referal commission value', db_column=b'cprcommissionvalue', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='AffiliateContactUs',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('username', models.CharField(max_length=20, null=True, blank=True)),
                ('email', models.CharField(max_length=20, null=True, blank=True)),
                ('subject', models.CharField(max_length=20, null=True, blank=True)),
                ('message', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Affiliateinfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('username', models.CharField(unique=True, max_length=30)),
                ('password', models.CharField(max_length=128)),
                ('contactname', models.CharField(max_length=30)),
                ('companyname', models.CharField(max_length=30)),
                ('email', models.EmailField(unique=True, max_length=254)),
                ('address', models.CharField(max_length=100)),
                ('city', models.CharField(max_length=100)),
                ('state', models.CharField(max_length=30)),
                ('country', models.CharField(max_length=30)),
                ('postcode', models.IntegerField()),
                ('telephone', models.BigIntegerField()),
                ('ipaddress', models.GenericIPAddressField()),
                ('registeredon', models.DateTimeField()),
                ('active', models.BooleanField(default=False)),
                ('status', models.CharField(default=b'Pending', max_length=30, verbose_name=b'Status', choices=[(b'Pending', b'Pending'), (b'Approved', b'Approved'), (b'Rejected', b'Rejected')])),
                ('mailrequired', models.BooleanField(default=True)),
                ('mobilerequired', models.BooleanField(default=True)),
                ('dob', models.DateField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.CreateModel(
            name='AffiliatePasswordResetlogs',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('token', models.CharField(max_length=20)),
                ('expiretimestamp', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
        migrations.CreateModel(
            name='BannerImages',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=60, null=True, blank=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('width', models.IntegerField(null=True, blank=True)),
                ('height', models.IntegerField(null=True, blank=True)),
                ('link', models.TextField(max_length=500, null=True, blank=True)),
                ('banner', models.ImageField(upload_to=b'bannerimages/')),
            ],
        ),
        migrations.CreateModel(
            name='Bannerlogs',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('link', models.TextField(max_length=100, null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Campaign',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('link', models.TextField()),
                ('name', models.CharField(unique=True, max_length=30)),
                ('created_timestamp', models.DateTimeField(default=django.utils.timezone.now)),
                ('media_type', affiliate.models.EnumField(max_length=104, verbose_name=b'Media Type', choices=[(b'TEXT', b'TEXT'), (b'BANNER', b'BANNER'), (b'HTML5', b'HTML5')])),
                ('language', affiliate.models.EnumField(max_length=104, verbose_name=b'Language', choices=[(b'English', b'English'), (b'Spanish', b'Spanish'), (b'German', b'German')])),
            ],
        ),
        migrations.CreateModel(
            name='CampaignTrackerMapping',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('trackerid', models.IntegerField(default=0, unique=True)),
                ('url', models.TextField(null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Chequedetails',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('chequepayableto', models.CharField(max_length=30, unique=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Currency',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('currency', models.CharField(unique=True, max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Hitcount',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('hits', models.IntegerField(default=1)),
            ],
        ),
        migrations.CreateModel(
            name='MappingParameters',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('generic_parameter', models.CharField(max_length=100)),
                ('client_parameter', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='MediaCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cateogry_name', models.CharField(unique=True, max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Notes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('notes', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Offer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('link', models.TextField()),
                ('name', models.CharField(unique=True, max_length=30)),
                ('created_timestamp', models.DateTimeField(default=django.utils.timezone.now)),
                ('advertiser', models.ForeignKey(to='affiliate.Advertiser')),
                ('fk_affiliateinfo', models.ForeignKey(to='affiliate.Affiliateinfo', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Wiretransfer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('accountnumber', models.CharField(max_length=30)),
                ('ifsccode', models.CharField(max_length=30)),
                ('accounttype', models.CharField(max_length=30)),
                ('accountname', models.CharField(max_length=30)),
                ('bankname', models.CharField(max_length=30)),
                ('bankaddress', models.CharField(max_length=30)),
                ('fk_affiliateinfo', models.ForeignKey(to='affiliate.Affiliateinfo')),
            ],
        ),
    ]
