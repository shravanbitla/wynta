from django.http import HttpResponseRedirect
from django.contrib.auth.hashers import make_password, check_password
from django.core.urlresolvers import reverse
from datetime import datetime, date
from models import Affiliateinfo
from django.template import Template, Context
from userapp.utils import get_fromdate_todate_for_django_query_filter, get_hexdigest
import random

RUMMY_WEBSITE_PROTOCOL = {1:"https", 2:"https", 3:"https", 4:"https"}

def affiliate_login_required(func1):
    """ decorator for checking player is logged into website or not """
    def wrap(request, *args, **kwargs):
        """check the session if id key not exist,redirect to home """
        if 'affiliate_session_id' not in request.session.keys():
            return HttpResponseRedirect(reverse('affiliate_login'))
            #return HttpResponseRedirect(reverse("presignin"))
        else:
            aff_id = request.session.get('affiliate_session_id')
            # aff_obj = Affiliateinfo.objects.get(id=aff_id)
            # if aff_obj.account_manager.status == 'Aborted':
            #     request.session['Aborted'] = True
            #     request.session['affid'] = False
            #     return HttpResponseRedirect(reverse('affiliate_login'))
        return func1(request, *args, **kwargs)
    wrap.__doc__ = func1.__doc__
    wrap.__name__ = func1.__name__
    return wrap

def enc_password(password):
    """ convert string into sha1 for affiliate password"""
    algo = 'sha1'
    salt = get_hexdigest(algo, str(random.random()), str(random.random()))[:5]
    hsh = get_hexdigest(algo, salt, password)
    password = '%s$%s$%s' % (algo, salt, hsh)
    return password

def datetime_or_date_to_string(datetime_obj, is_time=False):
    resp = "GivenDate"
    try:
        if isinstance(datetime_obj, datetime):
            if not is_time:
                resp = datetime_obj.strftime("%m/%d/%Y")
            else:
                resp = datetime_obj.strftime("%Y-%m-%d %H:%M:%S")
        elif isinstance(datetime_obj, date):
            resp = datetime_obj.strftime("%m/%d/%Y")
    except:
        return ''
    return resp

def makePaginationDiv(start_page, page_count_list, cur_page, next_page, count):
    divstring = ''
    if start_page == 0:
        divstring += '<span class="disabled previous_page">&#8592 Previous</span>'
    else:
        divstring += '<a class=" previous_page" href="?start_page=%s&prev=1">&#8592 Previous</a>' %start_page
    for i in page_count_list:
        if i == cur_page:
            divstring +='<em class="current">%s</em>' % i
        else:
            divstring += '<a rel="next" href="?page=%s">%s</a>' %(i, i)
    if next_page:
        divstring += '<a class="next_page" rel="next" href="?start_page=%s&next=1">Next &#8594</a>'%start_page
    else:
        divstring += '<span class="disabled previous_page">Next &#8594</span>'
    divstring += '&ensp; &ensp;Total Count : %s' %count
    return divstring

def paginationdiv(data, cpage):
    page_div = ''
    fields = data['next'].split('?')[1].split('&')[0] if (data['next'] and '&' in data['next']) else ''
    if data['previous']:
        page_div = '<a class=" previous_page" href="'+data['previous']+'&'+fields+'">&#8592 Previous</a>' #'<li><a href="'+data['previous']+'" aria-label="Previous"><span aria-hidden="true">Previous</span></a></li>'
    else:
        page_div = '<span class="disabled previous_page">&#8592 Previous</span>' #'<li class="disabled"><a href="." aria-label="Previous"><span aria-hidden="true">Previous</span></a></li>'
    for i in range(1,int(math.ceil(data['count']/20.0))+1):
        if i == 1 and cpage == i:
            page_div += '<em class="current">%s</em>' % str(i) #'<li><a href=".">'+str(i)+'</a></li>'
        elif cpage == i:
            page_div += '<a rel="next" class="active" href="?page='+str(i)+'&'+fields+'">'+str(i)+'</a>' #'<li class="active"><a href="?page='+str(i)+'&'+fields+'">'+str(i)+'</a></li>'
        else:
            page_div += '<a rel="next" href="?page='+str(i)+'&'+fields+'">'+str(i)+'</a>' #'<li><a href="?page='+str(i)+'&'+fields+'">'+str(i)+'</a></li>'
    if data['next']:
        page_div += '<a class="next_page" rel="next" href="'+data['next']+'&'+fields+'">Next &#8594</a>' #'<li><a href="'+data['next']+'" aria-label="Next"><span aria-hidden="true">Next</span></a></li>'
    else:
        page_div += '<span class="disabled previous_page">Next &#8594</span>' #'<li class="disabled"><a href="." aria-label="Next"><span aria-hidden="true">Next</span></a></li>'
    return page_div
