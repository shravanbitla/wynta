""" url related to affiliate"""
from django.contrib import admin
from django.conf.urls import include, url
from affiliate.views import *
admin.autodiscover()

urlpatterns = [
    url(r'^fpcontact/$', fp_contact),
                    # url(r'^$',home, name='affiliate'),
                    url(r'^affiliate-deals/$', fp_deals),
                    url(r'^our-brands/$', fp_brands),
                    url(r'^affiliate-marketing/$', fp_market),
                    url(r'^contact-us/$', fp_contact),
                    url(r'^affiliate/dashboard/$',home, name='affiliate'),
                    url(r'^affiliate/$',home, name='affiliate_home'),
                    url(r'^affiliate/requestap/$',requestap, name='affiliate_requestap'),
                    url(r'^affiliate/affp-list/$',affiliate_prog_list, name='affiliate_affp-list'),
                    url(r'^affiliate/set-accessible/$',set_affiliate_accessible, name='affiliate_affp-set'),
                    url(r'^affiliate/login/$', Signin.as_view(), name='affiliate_login' ),
                    url(r'^affiliate/register/$', AffiliateRegister.as_view(), name='affiliate_register' ),
                    url(r'^affiliate/edit-profile/$', EditProfile.as_view(), name='affiliate_editprofile' ),
                    url(r'^affiliate/logout/$',logout, name='logout' ),
                    url(r'^affiliate/payment-details/$',PaymentDetails.as_view(), name='affiliate_paymentdetails' ),
                    url(r'^affiliate/get-referral-link/$',refer, name='affiliate_referlink' ),
                    url(r'^affiliate/contact/$',contact_us, name='affiliate_contactus' ),
                    url(r'^affiliate/redirect/$',redirectfp, name='affiliate_redirect' ),
                    url(r'^affiliate/messages/$',messages_list, name='aff_messages'),
                    url(r'^affiliate/message-status/$',message_view, name='aff_message_view'),
                    url(r'^affiliate/redirect-aff-prog/$',redirect_aff_program, name='affiliate_redirect_ap' ),
                    # url(r'^affiliate/redirectj/$',redirectjm, name='affiliate_redirect_jm' ),
                    url(r'^affiliate/comingsoonpost/$',comingsoonpostlogin, name='affiliate_comingsoonpostlogin' ),
                    url(r'^affiliate/campaign-setup/$',createtracker, name='affiliate_createtracker' ),
                    url(r'^affiliate/viewtrackers/$',viewtrackers, name='affiliate_viewtrackers' ),
                    url(r'^affiliate/campaigns-admin/$',displaytrackers, name='affiliate_displaytrackers' ),
                    url(r'^affiliate/gettrackerlink/$',get_tracker_code, name='affiliate_trakercode' ),
                    url(r'^affiliate/sub-affiliate-list/$',get_sub_affiliate_list, name='affiliate_get_sub_affiliate_list' ),

                    url(r'^affiliate/marketingmaterials/$',marketingmaterials, name='affiliate_marketingmaterials' ),

                    url(r'^affiliate/sub-affiliate-report/$',sub_affiliate_report, name='affiliate_subaffrpt' ),
                    url(r'^affiliate/subaffdownload/$',sub_affiliate_data_report, name='affiliate_subaffdatarpt' ),
                    url(r'^affiliate/subaffs/$',get_sub_affiliates, name='affiliate_subafflist' ),
                    url(r'^affiliate/earnings-report/$',viewtrackersinfo, name='affiliate_viewtrackersinfo' ),
                    url(r'^affiliate/player-report/$',playerstatsinfo, name='affiliate_playerstatsinfo' ),
                    url(r'^affiliate/device-report/$',devicestatsinfo, name='affiliate_devicestatsinfo' ),
                    url(r'^affiliate/campaign-report/$',campaignreportinfo, name='affiliate_campaignreportinfo' ),
                    url(r'^affiliate/dynamic-report/$',dynamicreportinfo, name='affiliate_dynamicreportinfo' ),

                    url(r'^affiliate/custom-url-builder/$', custom_url_builder, name="affiliate_custom_url_builder"),
                    url(r'^affiliate/check_username/$',check_affiliate_name_exist, name='affiliate_check_affiliate_name_exist' ),
                    url(r'^affiliate/check_email/$',check_affiliate_email_exist, name='affiliate_check_affiliate_email_exist' ),
                    url(r'^affiliate/check_campaign_name/$',check_campaign_name_exist, name='affiliate_check_campaign_name_exist' ),

                    url(r'^affiliate/change-password/$',affchangepassword, name='affcpcsoon'),
                    url(r'affiliate/passwordreset/$', AffiliatePasswordReset.as_view(), name="affiliate_affilaitepasswordreset"),
                    url(r'affiliate/passwordresetaction/(?P<username>\w+)/(?P<token>\w+)/(?P<expireid>\w+)/$', 
                           AffiliatePasswordResetAction.as_view(), name="affiliate_affiliatepasswordresetaction"),
                    url(r'affiliate/passwordresetsent/$', password_reset_mail_sent, name="affiliate_affilaitepasswordresetsent"),

                    url(r'^affiliate/statementrpt/$',get_statementrpt, name='affiliate_statementrpt' ),
                    url(r'^affiliate/payment-history/$',get_paymentrpts, name='affiliate_paymentrpts' ),
                    url(r'^affiliate/gaming-report/$',get_gamerpt, name='affiliate_gamerpt' ),

                    url(r'^affiliate/gmarketdiv/$',get_market_materials, name='affiliate_get_market_materials' ),


                    url(r'^affiliate/marketing-material/$',marketmaterial, name='affiliate_marketmaterial' ),

                    # url(r'^affiliate/messages/$',get_messages, name='affiliate_messages' ),
                    # url(r'^affiliate/delmessages/$',delete_message, name='affiliate_delete_message' ),
                    url(r'^affiliate/faq/$',get_faq, name='affiliate_faq' ),

                    #need to remove
                    url(r'^affiliate-signin/$',Signin.as_view(), name='signin' ),
                    url(r'^affiliate-logout/$',logout, name='logout' ),
                    #need to remove
                    
                    #Vcomm Integration
                    url(r'^affiliate/subaffregister/$', vcommregister, name="affiliate_vcommregister"),
                    url(r'^affiliate/vcommregister/$', vcommregister, name="affiliate_vcommregister"),
                    url(r'^affiliate/vcommdata/$',vcommdata, name='affiliate_vcommdata' ),
                    url(r'^affiliate/vcommadmin/$',vcommadmin, name='affiliate_vcommadmin' ),

                    url(r'^affiliate/getcategory/$',get_category, name='affiliate_get_category' ),
                    url(r'^logout/$', logout, name='logout'),
                    url(r'^affiliate/thanksaff/$', thanksaff, name='thanksaff'),
                    url(r'^$', indexpage, name='index'),
                    # url(r'^admin/', include(admin.site.urls)),
                    url(r'^affiliate/whats-new/$', whats_new, name='whats_new'),
                    url(r'^affiliate/get-notifications/$', get_notifications, name='get_notifications'),
                    url(r'^affiliate/wynta-login/$', wynta_login, name='wynta_login'),
                    url(r'^affiliate/one-login/$', one_login, name='one_login'),
                    url(r'^affiliate/remove-account/$', remove_account, name='remove_account'),
                    url(r'^affiliate/get-account-ip/$', get_account_ip, name='get_account_ip')

 ]
# urlpatterns = patterns('affiliate.views',
#                     url(r'^fpcontact/$', 'fp_contact'),
#                     url(r'^affiliate-deals/$', 'fp_deals'),
#                     url(r'^our-brands/$', 'fp_brands'),
#                     url(r'^affiliate-marketing/$', 'fp_market'),
#                     url(r'^contact-us/$', 'fp_contact'),
#                     url(r'^affiliate/$','home', name='affiliate'),
#                     url(r'^affiliate/home/$','home', name='affiliate_home'),
#                     url(r'^affiliate/login/$', Signin.as_view(), name='affiliate_login' ),
#                     url(r'^affiliate/register/$', AffiliateRegister.as_view(), name='affiliate_register' ),
#                     url(r'^affiliate/edit-profile/$', EditProfile.as_view(), name='affiliate_editprofile' ),
#                     url(r'^affiliate/logout/$','logout', name='logout' ),
#                     # url(r'^affiliate/statistics/$',"statistics", name='affiliate_statistics' ),
#                     url(r'^affiliate/refer/$',"refer", name='affiliate_referlink' ),
#                     url(r'^affiliate/contact/$',"contact_us", name='affiliate_contactus' ),
#                     url(r'^affiliate/redirect/$',"redirectfp", name='affiliate_redirect' ),
#                     url(r'^affiliate/comingsoonpost/$',"comingsoonpostlogin", name='affiliate_comingsoonpostlogin' ),
#                     url(r'^affiliate/createtracker/$',"createtracker", name='affiliate_createtracker' ),
#                     url(r'^affiliate/viewtrackers/$',"viewtrackers", name='affiliate_viewtrackers' ),
#                     url(r'^affiliate/displaytrackers/$',"displaytrackers", name='affiliate_displaytrackers' ),
#                     url(r'^affiliate/gettrackerlink/$',"get_tracker_code", name='affiliate_trakercode' ),
#                     url(r'^affiliate/subaffiliatelist/$',"get_sub_affiliate_list", name='affiliate_get_sub_affiliate_list' ),

#                     url(r'^affiliate/marketingmaterials/$',"marketingmaterials", name='affiliate_marketingmaterials' ),

#                     url(r'^affiliate/subaffrpt/$',"sub_affiliate_report", name='affiliate_subaffrpt' ),
#                     url(r'^affiliate/subaffs/$',"get_sub_affiliates", name='affiliate_subafflist' ),
#                     url(r'^affiliate/viewtrackersinfo/$',"viewtrackersinfo", name='affiliate_viewtrackersinfo' ),
#                     url(r'^affiliate/playerstatsinfo/$',"playerstatsinfo", name='affiliate_playerstatsinfo' ),
#                     url(r'^affiliate/devicestatsinfo/$',"devicestatsinfo", name='affiliate_devicestatsinfo' ),
#                     url(r'^affiliate/campaignreportinfo/$',"campaignreportinfo", name='affiliate_campaignreportinfo' ),

#                     url(r'^affiliate/custom-url-builder/$', 'custom_url_builder', name="affiliate_custom_url_builder"),
#                     url(r'^affiliate/check_username/$',"check_affiliate_name_exist", name='affiliate_check_affiliate_name_exist' ),
#                     url(r'^affiliate/check_email/$',"check_affiliate_email_exist", name='affiliate_check_affiliate_email_exist' ),
#                     url(r'^affiliate/check_campaign_name/$',"check_campaign_name_exist", name='affiliate_check_campaign_name_exist' ),
                    
#                     url(r'affiliate/passwordreset/$', AffiliatePasswordReset.as_view(), name="affiliate_affilaitepasswordreset"),
#                     url(r'affiliate/passwordresetaction/(?P<username>\w+)/(?P<token>\w+)/(?P<expireid>\w+)/$', 
#                            AffiliatePasswordResetAction.as_view(), name="affiliate_affiliatepasswordresetaction"),
#                     url(r'affiliate/passwordresetsent/$', 'password_reset_mail_sent', name="affiliate_affilaitepasswordresetsent"),

#                     url(r'^affiliate/statementrpt/$',"get_statementrpt", name='affiliate_statementrpt' ),
#                     url(r'^affiliate/paymentrpt/$',"get_paymentrpts", name='affiliate_paymentrpts' ),
#                     url(r'^affiliate/gamerpt/$',"get_gamerpt", name='affiliate_gamerpt' ),

#                     url(r'^affiliate/gmarketdiv/$',"get_market_materials", name='affiliate_get_market_materials' ),


#                     url(r'^affiliate/marketing-material/$',"marketmaterial", name='affiliate_marketmaterial' ),

#                     url(r'^affiliate/messages/$',"get_messages", name='affiliate_messages' ),
#                     url(r'^affiliate/delmessages/$',"delete_message", name='affiliate_delete_message' ),
#                     url(r'^affiliate/faq/$',"get_faq", name='affiliate_faq' ),

#                     #need to remove
#                     url(r'^affiliate-signin/$',Signin.as_view(), name='signin' ),
#                     url(r'^affiliate-logout/$','logout', name='logout' ),
#                     #need to remove
                    
#                     #Vcomm Integration
#                     url(r'^affiliate/vcommregister/$', 'vcommregister', name="affiliate_vcommregister"),
#                     url(r'^affiliate/vcommdata/$',"vcommdata", name='affiliate_vcommdata' ),
#                     url(r'^affiliate/vcommadmin/$',"vcommadmin", name='affiliate_vcommadmin' ),

#                     url(r'^affiliate/getcategory/$',"get_category", name='affiliate_get_category' ),
                   
#                     url(r'^logout/$','logout', name='logout' ),
#                     url(r'^admin/', include(admin.site.urls)),
#                     url(r'^$','indexpage'),
#                 ) 
