import os
from env_settings.wynta_conf import *
DEBUG = False
#DEBUG = False

CAPTCHA_CHALLENGE_FUNCT = 'middleware.random_digit_challenge'
CAPTCHA_OUTPUT_FORMAT = '%(image)s %(hidden_field)s %(text_field)s'
CAPTCHA_NOISE_FUNCTIONS = ('captcha.helpers.noise_dots',)

#TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
     ('Shravan Kumar', 'shravan.bitla@gridlogic.in'),
     ('Saheb', 'saheb.pacha@gridlogic.in'),
)

PROJECT_PATH = os.path.dirname(os.path.dirname(__file__))
DIRNAME = os.path.abspath(os.path.dirname(__file__))

AUTO_LOGOUT_DELAY = 30 #in minutes
MANAGERS = ADMINS

DATABASE_ROUTERS = ['gamelogs.dbrouter.LogRouter',]
SESSION_COOKIE_NAME = "WYNTA"

TIME_ZONE = 'Asia/Kolkata'

LOGIN_REDIRECT_URL = "/home/"

LANGUAGE_CODE = 'en-us'

SITE_ID = 1
NETWORK_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"

MEDIA_ROOT = '/var/www/wynta/media/'

MEDIA_URL = '/media/'

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
#MEDIA_URL = 'http://115.248.252.52/media/'
ADMIN_MEDIA_PREFIX = '/media/'
# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = '/static_files/'

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static_files/'

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
ADMIN_MEDIA_PREFIX = '/static_files/admin/'


# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(DIRNAME, 'static_files'),
)

# List of finder classes that know how to find static files in
# various locations.

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '4p92y=+lh!6vi+*i*1!hhu$f+wja(ptzu3&b5n3tqh2^5(^4s2'

# List of callables that know how to import templates from various sources.
#TEMPLATE_LOADERS = (
#    'django.template.loaders.filesystem.Loader',
#    'django.template.loaders.app_directories.Loader',
#)

MIDDLEWARE_CLASSES = (
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    #'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
    #'django.middleware.cache.UpdateCacheMiddleware',
    #'django.middleware.common.CommonMiddleware',
    #'django.middleware.cache.FetchFromCacheMiddleware'
    'pagination.middleware.PaginationMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    # 'debug_toolbar.middleware.DebugToolbarMiddleware',
    'django_user_agents.middleware.UserAgentMiddleware',
    'middleware.AutoLogout',
    #'middleware.MobileTemplatesMiddleware',
    #'middleware.LogHttpResponse',
    #'middleware.memoryMiddleware',
    'middleware.SqlProfilingMiddleware',
    'middleware.AuthorizerMiddleware',
    'threadlocals.middleware.ThreadLocalMiddleware',
)


INTERNAL_IPS = ('127.0.0.1',)
DEBUG_TOOLBAR_PANELS = (
    'debug_toolbar.panels.version.VersionDebugPanel',
    'debug_toolbar.panels.timer.TimerDebugPanel',
    'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
    'debug_toolbar.panels.headers.HeaderDebugPanel',
    'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
    'debug_toolbar.panels.template.TemplateDebugPanel',
    'debug_toolbar.panels.sql.SQLDebugPanel',
    'debug_toolbar.panels.signals.SignalDebugPanel',
    'debug_toolbar.panels.logger.LoggingPanel',
)


DEBUG_TOOLBAR_CONFIG = {
        'INTERCEPT_REDIRECTS': False,
    }
"""
TEMPLATE_CONTEXT_PROCESSORS =(
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.request",
)
""" 



#TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
#    os.path.join(DIRNAME, 'templates'),
    # Don't forget to use absolute paths, not relative paths.
#)

#MOBILE_TEMPLATE_DIRS = (
#    os.path.join(DIRNAME, 'templates', 'mobile_templates'),
#)

AUTH_PROFILE_MODULE = "signal"

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'corsheaders',
    'affiliate',
    'connectors',
    'pagination',
    'admins',
    'wyntaadmin',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'endless_pagination',

    'rest_framework',
    # 'rest_framework_xml',
    # 'rest_framework.authtoken',
    
    'django_extensions',
    # 'rest_framework',
    'gamelogs',
    'django_user_agents',
    'apiapp',
    'userapp',
)

#import djcelery
#djcelery.setup_loader()




TINYMCE_DEFAULT_CONFIG = {
    'plugins': "table,spellchecker,paste,searchreplace",
    'theme': "advanced",
}
TINYMCE_SPELLCHECKER = True
TINYMCE_COMPRESSOR = True


LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.AdminEmailHandler'
        }
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
        'error_log': {
            'format':'[%(levelname)s %(asctime)s] \n\t%(message)s'
        },
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'error_log_handler': {
            'level': 'ERROR',
            'class': 'logging.FileHandler',
            'formatter':'error_log'
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
        },
        'error_log':{
            'handlers': ['error_log_handler'],
            'level': 'ERROR',
        },
    }
}




REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
    ),
    'DEFAULT_FILTER_BACKENDS': ('django_filters.rest_framework.DjangoFilterBackend',),
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 10
}

EMAIL_BACKEND = EMAIL_BACKEND
EMAIL_HOST = EMAIL_HOST
EMAIL_HOST_USER = EMAIL_HOST_USER
EMAIL_HOST_PASSWORD = EMAIL_HOST_PASSWORD
EMAIL_PORT = EMAIL_PORT
EMAIL_USE_TLS = EMAIL_USE_TLS
EMAIL_SMS_TOKEN_EXPIRY = EMAIL_SMS_TOKEN_EXPIRY
ENCODE_SALT = ENCODE_SALT

SESSION_EXPIRE_AT_BROWSER_CLOSE = True


GLS_ADMINS = ('srikanth', 'azeem','parikshit','partha', 'anupama')
GLS_MIX_ADMINS = ('srikanth', 'azeem','parikshit','partha', 'anupama')

ROOT_URLCONF = 'urls'
SESSION_SERIALIZER = 'django.contrib.sessions.serializers.PickleSerializer'

PP_USER_NAME = 'adminsticky'
PP_PASSWORD = 'SSwlm^49'

IP_API_KEY = "pKhnby1J1pAgIxN"
