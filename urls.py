from django.conf.urls import include, url
from django.contrib import admin
admin.autodiscover()
from django.views.generic import TemplateView
from django.contrib.auth.views import login
from django.contrib.auth.views import *
from affiliate import views
import os
import affiliate

handler404 = "affiliate.views.custom_page_not_found_view"
handler500 = "affiliate.views.custom_error_view"

adminurls = include(admin.site.urls)
# urlpatterns = patterns('',
#     #url(r'^$', 'userapp.views.comingsoon'),
#     url(r'^', include('affiliate.urls')),
#     url(r'^', include('admins.urls')),
#     url(r'^', include('wyntaadmin.urls')),
#     # #azeem
#     # url(r'^sms-home','userapp.views.smshome'),
#     )
# 
urlpatterns = [
    # url(r'^admin/', include(admin.site.urls)),
    url(r'^profiling/$', 'affiliate.views.profiling', name="mysql_queries_profiling"),
    # url(r'^api/v1/', include('apiapp.urls')),
    url(r'^', include('affiliate.urls')),
    url(r'^', include('admins.urls')),
    url(r'^', include('wyntaadmin.urls')),
    url(r'^', include('connectors.urls')),
    url(r'^pmc/$', TemplateView.as_view(template_name="PMC/auth/login.html"), name="pmc_a_login"),
    # url(r'^pmc/', include('pmc.urls')),
    url(r'^pmc/index$', TemplateView.as_view(template_name="PMC/index.html")),
    url(r'^pmc/thanks$', TemplateView.as_view(template_name="PMC/thanks.html")),
    url(r'^pmc/signin/$', TemplateView.as_view(template_name="PMC/auth/login.html"), name="pmc_login"),

    #PLAYER MANAGEMENT URL'S
    url(r'^pmc/bsearch/$', TemplateView.as_view(template_name="PMC/bsearch.html")),
    url(r'^pmc/search/$', TemplateView.as_view(template_name="PMC/search.html")),
    url(r'^pmc/profile/$', TemplateView.as_view(template_name="PMC/profile.html")),
    url(r'^pmc/player-registration/$', TemplateView.as_view(template_name="PMC/player-registration.html")),
    url(r'^pmc/player-financials/$', TemplateView.as_view(template_name="PMC/player-financials.html")),
    url(r'^pmc/player-games/$', TemplateView.as_view(template_name="PMC/player-games.html")),
    
    
    #PLAYER REPORTS URL'S
    url(r'^pmc/blocked-logs/$', TemplateView.as_view(template_name="PMC/blocked-logs.html")),
    url(r'^pmc/device-report-h/$', TemplateView.as_view(template_name="PMC/device-report.html")),
    url(r'^pmc/reactivation-report/$', TemplateView.as_view(template_name="PMC/reactivation-report.html")),
    url(r'^pmc/player-birthday-report/$', TemplateView.as_view(template_name="PMC/player-birthday-report.html")),
    url(r'^pmc/player-attrition-report/$', TemplateView.as_view(template_name="PMC/player-attrition-report.html")),
    url(r'^pmc/player-report-h/$', TemplateView.as_view(template_name="PMC/player-report.html")),
    url(r'^pmc/player-daily-report-h/$', TemplateView.as_view(template_name="PMC/player-daily-report.html")),
    
    #FINANCIALS URL'S
    url(r'^pmc/daily-summary/$', TemplateView.as_view(template_name="PMC/daily-summary.html")),
    url(r'^pmc/daily-depositors/$', TemplateView.as_view(template_name="PMC/daily-depositors.html")),
    url(r'^pmc/financial-report/$', TemplateView.as_view(template_name="PMC/financial-report.html")),
    url(r'^pmc/country-summary/$', TemplateView.as_view(template_name="PMC/country-summary.html")),
    url(r'^pmc/liability-report/$', TemplateView.as_view(template_name="PMC/liability-report.html")),
    
    
    #REPORTS URL'S
    url(r'^pmc/last-logins/$', TemplateView.as_view(template_name="PMC/last-logins.html")),
    url(r'^pmc/revenue/$', TemplateView.as_view(template_name="PMC/revenue.html")),
    url(r'^pmc/email-reports/$', TemplateView.as_view(template_name="PMC/email-reports.html")),
    url(r'^pmc/sms-reports/$', TemplateView.as_view(template_name="PMC/sms-reports.html")),
    url(r'^pmc/games-stats/$', TemplateView.as_view(template_name="PMC/games-stats.html")),
    url(r'^pmc/bonus-stats/$', TemplateView.as_view(template_name="PMC/bonus-stats.html")),
    
    
    #CRM URL'S
    url(r'^pmc/email-lists/$', TemplateView.as_view(template_name="PMC/email-lists.html")),
    url(r'^pmc/reports-summary/$', TemplateView.as_view(template_name="PMC/reports-summary.html")),
    
    
    #OPERATOR CONSOLE URL'S
    url(r'^pmc/add-user-role/', TemplateView.as_view(template_name="PMC/edit-role.html")),
    url(r'^pmc/role-management/$', TemplateView.as_view(template_name="PMC/role-management.html")),
    url(r'^pmc/notification-alerts/$', TemplateView.as_view(template_name="PMC/notification-alerts.html")),
    
    
    #AFFILIATE MANAGEMENT URL'S
    url(r'^pmc/affiliate-search/$', TemplateView.as_view(template_name="PMC/affiliate-search.html")),
    url(r'^pmc/create-affiliate/$', TemplateView.as_view(template_name="PMC/create-affiliate.html")),
    url(r'^pmc/affiliate-report/$', TemplateView.as_view(template_name="PMC/affiliate-report.html")),
    
    
    #COMPLAINCE URL'S
    url(r'^pmc/settlement-reports/$', TemplateView.as_view(template_name="PMC/settlement-reports.html")),
    url(r'^pmc/contracts/$', TemplateView.as_view(template_name="PMC/contracts.html")),
    
    #ACCOUNT MANAGEMENT URL'S
    url(r'^pmc/overview/$', TemplateView.as_view(template_name="PMC/overview.html")),
    url(r'^pmc/active-users/$', TemplateView.as_view(template_name="PMC/active-users.html")),
    url(r'^pmc/account-managers/$', TemplateView.as_view(template_name="account-managers.html")),
    url(r'^pmc/account-activities/$', TemplateView.as_view(template_name="PMC/account-activities.html"))
    
    
    # url(r'^$',views.indexpage),
    # url(r'^integration/$',views.integration),
    # url(r'^thankswynta/$',views.thanks),
    # url(r'^wyntacsave/$',views.wyntacsave, name='wyntacsave'),
]
