"""
Provides various authentication policies.
"""
from __future__ import unicode_literals

import base64
import binascii

import random
import datetime

from django.contrib.auth import authenticate, get_user_model
from django.contrib.sites.models import get_current_site
from django.middleware.csrf import CsrfViewMiddleware
from django.utils.six import text_type
from django.utils.translation import ugettext_lazy as _
from .models import CustomToken, CustomTokenAffiliate
from userapp.models import *
from userapp.utils import *
from affiliate.utils import *
from affiliate.models import *
from affiliate.dashboard_data import *
from connectors.models import *

from rest_framework.renderers import JSONRenderer
from rest_framework.views import APIView
from rest_framework import HTTP_HEADER_ENCODING, exceptions, status as st, viewsets
from rest_framework.response import Response


def get_authorization_header(request):
    """
    Return request's 'Authorization:' header, as a bytestring.
    Hide some test client ickyness where the header can be unicode.
    """
    auth = request.META.get('HTTP_AUTHORIZATION', b'')
    if isinstance(auth, text_type):
        # Work around django test client oddness
        auth = auth.encode(HTTP_HEADER_ENCODING)
    return auth


class CSRFCheck(CsrfViewMiddleware):
    def _reject(self, request, reason):
        # Return the failure reason instead of an HttpResponse
        return reason


class BaseAuthentication(object):
    """
    All authentication classes should extend BaseAuthentication.
    """

    def authenticate(self, request):
        """
        Authenticate the request and return a two-tuple of (user, token).
        """
        raise NotImplementedError(".authenticate() must be overridden.")

    def authenticate_header(self, request):
        """
        Return a string to be used as the value of the `WWW-Authenticate`
        header in a `401 Unauthenticated` response, or `None` if the
        authentication scheme should return `403 Permission Denied` responses.
        """
        pass


class AffiliateLogin(object):
    def post_login(self, request):
        context_dict = {}
        context_dict['obj'] = None
        username = str(request.POST.get('email'))
        obj = Affiliateinfo.objects.filter(email=username)
        if not obj:
            context_dict['error'] = True
            context_dict['message'] = "The Email entered is not a valid one."
            context_dict['status'] = 'Error'
            return context_dict
        obj = obj[0]
        password = str(request.POST.get('passwd'))
        # user = authenticate(username=user_obj[0].username, password=password)
        dbpassword = obj.password
        '''hash function password hash function generation'''
        signinpass = enc_password(password)
        a = dbpassword.split('$')
        hashdb = str(a[2])
        salt = str(a[1])
        usrhash = get_hexdigest(a[0], a[1], password)
        print 'dbpassword', hashdb
        print 'ourpassword', password, usrhash
        if usrhash == hashdb:
            if obj.status != 'Approved':
                context_dict['message'] = "Your account is not yet approved."
                context_dict['error'] = True
                context_dict['status'] = 'Error'
            else:
                context_dict['obj'] = obj
        else:
            context_dict['error'] = True
            context_dict['message'] = "The Password entered is not a valid one."
            context_dict['status'] = 'Error'
        return context_dict

class UserLogin(object):
    def post_login(self, request):
        context_dict = {}
        context_dict['obj'] = None
        username = str(request.POST.get('email'))
        user_obj = User.objects.filter(email=username)
        if not user_obj:
            context_dict['error'] = True
            context_dict['message'] = "The Email entered is not a valid one."
            context_dict['status'] = 'Error'
            return context_dict
        password = str(request.POST.get('passwd'))
        user = authenticate(username=user_obj[0].username, password=password)
        if user is not None:
            if user.is_active:
                acc_manager_obj = AccountManager.objects.filter(user=user)
                if acc_manager_obj:
                    acc_manager = acc_manager_obj[0]
                else:
                    partuser_obj = PartnerUser.objects.filter(user=user)
                if len(acc_manager_obj) < 1 and len(partuser_obj) < 1 :
                    context_dict['error'] = True
                    context_dict['message'] = "The email is not an account manager."
                    context_dict['status'] = 'Error'
                else:
                    context_dict['obj'] = user
        else:
            context_dict['error'] = True
            context_dict['message'] = "The Password entered is not a valid one."
            context_dict['status'] = 'Error'
        return context_dict


def generatePass(psw, alog='sha1', salt=None):
    '''hash function password generation code '''
    "this method returns the encrypted password in sha"
    try:
        algorithm = getattr(hashlib, alog)
    except AttributeError, e:
        logger.error('attribure error in generatePass method , error info %s', e)
    hobj = algorithm(alog)
    hobj.update(psw)
    if not salt:
        "generating the salt"
        salt = ''.join(random.sample(string.printable, 5))
        hobj.update(salt)
        psw = hobj.hexdigest()
        return (psw, salt)
    else:
        hobj.update(salt)
        psw = hobj.hexdigest()
        return psw


def enc_password(password):
    '''
    Encrypt password
    '''
    import random
    algo = 'sha1'
    salt = get_hexdigest(algo, str(random.random()), str(random.random()))[:5]
    hsh = get_hexdigest(algo, salt, password)
    password = '%s$%s$%s' % (algo, salt, hsh)
    return password

#Custome auth handler to override the message being returned.
from rest_framework.authtoken.views import ObtainAuthToken
class CustomObtainAuthToken(ObtainAuthToken):
    def post(self, request):
        login_obj = UserLogin()
        user = login_obj.post_login(request)
        if not user.get('error',False):
            token, created = CustomToken.objects.get_or_create(user=user['obj'])
            return Response({'token': token.key, 'unique_id': user['obj'].id})
        else:
            return Response({"error": user['message'] }, status=st.HTTP_400_BAD_REQUEST)


class AffiliateObtainAuthToken(ObtainAuthToken):
    def post(self, request):
        login_obj = AffiliateLogin()
        affiliate = login_obj.post_login(request)
        if not affiliate.get('error',False):
            token, created = CustomTokenAffiliate.objects.get_or_create(affiliate=affiliate['obj'])
            return Response({'token': token.key, 'unique_id': affiliate['obj'].id})
        else:
            return Response({"error": affiliate['message'] }, status=st.HTTP_400_BAD_REQUEST)

class TokenAuthentication(BaseAuthentication):
    """
    Simple token based authentication.
    Clients should authenticate by passing the token key in the "Authorization"
    HTTP header, prepended with the string "Token ".  For example:
        Authorization: Token 401f7ac837da42b97f613d789819ff93537bee6a
    """

    keyword = 'Token'
    model = CustomToken

    def get_model(self):
        if self.model is not None:
            return self.model
        return CustomToken

    """
    A custom token model may be used, but must have the following properties.
    * key -- The string identifying the token
    * user -- The user to which the token belongs
    """

    def authenticate(self, request):
        auth = get_authorization_header(request).split()

        if not auth or auth[0].lower() != self.keyword.lower().encode():
            return None

        if len(auth) == 1:
            msg = _('Invalid token header. No credentials provided.')
            raise exceptions.AuthenticationFailed(msg)
        elif len(auth) > 2:
            msg = _('Invalid token header. Token string should not contain spaces.')
            raise exceptions.AuthenticationFailed(msg)

        try:
            token = auth[1].decode()
        except UnicodeError:
            msg = _('Invalid token header. Token string should not contain invalid characters.')
            raise exceptions.AuthenticationFailed(msg)

        return self.authenticate_credentials(token)

    def authenticate_credentials(self, key):
        model = self.get_model()
        try:
            token = model.objects.select_related('user').get(key=key)
        except model.DoesNotExist:
            raise exceptions.AuthenticationFailed(_('Invalid token.'))

        # if not token.playerid.is_active:
        #     raise exceptions.AuthenticationFailed(_('User inactive or deleted.'))

        return (token.user, token)

    def authenticate_header(self, request):
        return self.keyword


class AffiliateTokenAuthentication(BaseAuthentication):
    """
    Simple token based authentication.
    Clients should authenticate by passing the token key in the "Authorization"
    HTTP header, prepended with the string "Token ".  For example:
        Authorization: Token 401f7ac837da42b97f613d789819ff93537bee6a
    """

    keyword = 'Token'
    model = CustomTokenAffiliate

    def get_model(self):
        if self.model is not None:
            return self.model
        return CustomTokenAffiliate

    """
    A custom token model may be used, but must have the following properties.
    * key -- The string identifying the token
    * user -- The user to which the token belongs
    """

    def authenticate(self, request):
        auth = get_authorization_header(request).split()

        if not auth or auth[0].lower() != self.keyword.lower().encode():
            return None

        if len(auth) == 1:
            msg = _('Invalid token header. No credentials provided.')
            raise exceptions.AuthenticationFailed(msg)
        elif len(auth) > 2:
            msg = _('Invalid token header. Token string should not contain spaces.')
            raise exceptions.AuthenticationFailed(msg)

        try:
            token = auth[1].decode()
        except UnicodeError:
            msg = _('Invalid token header. Token string should not contain invalid characters.')
            raise exceptions.AuthenticationFailed(msg)

        return self.authenticate_credentials(token)

    def authenticate_credentials(self, key):
        model = self.get_model()
        try:
            token = model.objects.select_related('affiliate').get(key=key)
        except model.DoesNotExist:
            raise exceptions.AuthenticationFailed(_('Invalid token.'))

        # if not token.playerid.is_active:
        #     raise exceptions.AuthenticationFailed(_('User inactive or deleted.'))

        return (token.affiliate, token)

    def authenticate_header(self, request):
        return self.keyword