from django.conf.urls import patterns, include, url
from .authenticate import *
from .views import *

from rest_framework.routers import DefaultRouter
router = DefaultRouter()

router.register(r'dashboard', DashboardView, base_name='dashboard_overview')
router.register(r'chartdata', BarChart, base_name='dashboard_barchart')
router.register(r'affiliateperformance', AffiliatePerformance, base_name='dashboard_affperformance')
router.register(r'brandreport', BrandReport, base_name='dashboard_brandreport')


router.register(r'earningsreport', EarningsReport, base_name='amca_earningsreport')
router.register(r'campaignreport', CampaignReport, base_name='amca_campaignreport')
router.register(r'conversionreport', ConversionReport, base_name='amca_conversionreport')
router.register(r'dynamicreport', DynamicReport, base_name='amca_dynamicreport')
router.register(r'playerreport', PlayerReport, base_name='amca_playerreport')
router.register(r'affiliateactivity', AffiliateReport, base_name='amca_affiliateactivity')


routeraff = DefaultRouter()
routeraff.register(r'dashboard', AffDashboardView, base_name='dashboard_aff')
routeraff.register(r'chartdata', AffBarChart, base_name='dashboard_chart')
routeraff.register(r'brandreport', AffBrandReport, base_name='dashboard_aff_report')

urlpatterns = patterns('',
                       url(r'^login',
                           CustomObtainAuthToken.as_view()),
                       url(r'^logout', logout_admin, name='amc_logout' ),
                       url(r'^affiliate/login',
                           AffiliateObtainAuthToken.as_view()),
                       url(r'^affiliate/logout', logout_aff, name='aff_logout' ),
                       url(r'^', include(router.urls)),
                       url(r'^savestatus', savestatus, name='amca_savestatus'),
                       url(r'^affiliate/', include(routeraff.urls)),
                       )