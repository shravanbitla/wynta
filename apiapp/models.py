import binascii
import os

from django.conf import settings
from django.db import models
from django.contrib.auth.models import User
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from affiliate.models import *


class CustomToken(models.Model):
    """
    The default authorization token model.
    """
    key = models.CharField(_("Key"), max_length=40, primary_key=True)
    user = models.OneToOneField(
        User
    )
    created = models.DateTimeField(_("Created"), auto_now_add=True)


    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super(CustomToken, self).save(*args, **kwargs)

    def generate_key(self):
        return binascii.hexlify(os.urandom(20)).decode()

    def __str__(self):
        return self.key


class CustomTokenAffiliate(models.Model):
    """
    The default authorization token model.
    """
    try:
        key = models.CharField(_("Key"), max_length=40, primary_key=True)
        affiliate = models.OneToOneField(
            Affiliateinfo
        )
        created = models.DateTimeField(_("Created"), auto_now_add=True)
    except Exception as e:
        print('inside apiapp/models.py',e)

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super(CustomTokenAffiliate, self).save(*args, **kwargs)

    def generate_key(self):
        return binascii.hexlify(os.urandom(20)).decode()

    def __str__(self):
        return self.key