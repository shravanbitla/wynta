from django.shortcuts import render

# Create your views here.
from rest_framework.views import APIView
from rest_framework import exceptions, filters, generics, mixins, status as st, viewsets
from rest_framework.renderers import JSONRenderer, BrowsableAPIRenderer
from django.utils import timezone
from rest_framework.response import Response
from django.core import serializers
from affiliate.utils import *
from django.contrib.auth.models import User
from affiliate.dashboard_data import *
from userapp.models import *
from affiliate.models import *
from affiliate.dashboard_data import *
from connectors.models import *
from connectors.retrievers import *
from userapp.utils import *
from userapp.function import *
from userapp.mails import *
import smtplib
import string
from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from apiapp.authenticate import TokenAuthentication, AffiliateTokenAuthentication
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from admin.views import get_brand_order


def logout_aff(request):
    """ Logout partner from website """
    try:
        logout(request)
        status_code = st.HTTP_200_OK
    except KeyError:
        status_code = st.HTTP_200_OK
    return Response({'status': 'Success', 'message': 'Successfully Logout'}, status=status_code)

def logout_admin(request):
    """ Logout partner from website """
    try:
        logout(request)
        status_code = st.HTTP_200_OK
    except KeyError:
        status_code = st.HTTP_200_OK
    return Response({'status': 'Success', 'message': 'Successfully Logout'}, status=status_code)


def get_aff_comm(obj):
    commission = Affiliatecommission.objects.select_related('fk_affiliateinfo').filter(fk_affiliateinfo=obj)
    if commission:
        commission = commission[0]
    else:
        commission = Affiliatecommission.objects.select_related('fk_affiliateinfo').filter(
                        account_manager = obj.account_manager)
        if commission:
            commission = commission[0]
        else:
            commission = Affiliatecommission.objects.select_related('fk_affiliateinfo').filter(
                                siteid__in = obj.siteid.all())
            if commission:
                commission = commission[0]
            else:
                commission = Affiliatecommission.objects.all()[0]
    return commission

def is_datediff_valid(fromdate, todate):
    date_between = todate - fromdate
    diff_dates = date_between.days
    status = True
    if diff_dates > 90:
        status = False
    return status

class DashboardView(viewsets.ViewSet):
    """
    url: /api/v1/dashboard/
    Headers:{
        'Authorization': 'Token db975411fdad07e4c41c1639aecf20b2e87asdhsd5' (User login token)
    }
    data: {
        phone_preference: True,
        sms_preference: False,
        email_preference: True
        }
    responce:{status: "Success", message: "Preference Edited successfully"}
    """
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    # filter_backends = (filters.DjangoFilterBackend,filters.SearchFilter,)
    http_method_names = ['get', 'post']

    def list(self, request):
        context_dict = {}
        try:
            status = 'Success'
            message = ''
            acc_manager = None
            partnerid = request.custom_data.get('partnerid')
            try:
                user = User.objects.get(id=partnerid)
                acc_manager_obj = AccountManager.objects.filter(user=user)
                partuser_obj = []
                if acc_manager_obj:
                    acc_manager = acc_manager_obj[0]
                else:
                    partuser_obj = PartnerUser.objects.filter(user=user)
                    acc_manager = partuser_obj[0].accmanager
                if not acc_manager:
                    status_code = st.HTTP_400_BAD_REQUEST
                    message = partnerid + " is not in database in respect to any User."
                    return Response({'status': status, 'message': message}, status=status_code)
            except:
                logger.error(partnerid + " is not in database in respect to any User.")
                status = 'Error'
                status_code = st.HTTP_400_BAD_REQUEST
                message = partnerid + " is not in database in respect to any User."
                return Response({'status': status, 'message': message}, status=status_code)
            network_site_obj = NetworkAndSiteMap.objects.filter(networkid__in=acc_manager.affiliateprogramme.networkid.all())
            affiliates_obj = Affiliateinfo.objects.filter(account_manager=acc_manager)
            site_ids_list = get_brand_order(acc_manager)
            net_ids_list = [1]
            custom = str(request.GET.get("customid", 'Custom'))
            fromdate = str(request.GET.get("fromdate"))
            todate = str(request.GET.get("todate"))
            domain = request.GET.get("sitename")
            affiliate_id = request.GET.get("affiliateid")
            currency = request.GET.get("currency", 'euro')
            site_name = 'All Websites'
            site_ids = site_ids_list
            if domain and domain != 'All':
                site_domain_list = domain.split(',')
                site_ids = site_ids_list.filter(domain__in=site_domain_list)
                site_name = 'Multiple sites.'
                if len(domain.split(',')) <2:
                    site_name = site_ids_list.get(domain=site_domain_list[0]).name
            siteids = request.GET.getlist("siteids")
            if request.method == "GET" and fromdate == 'None':
                to_day_date = datetime.now()
                date = to_day_date.day
                year = to_day_date.year
                month = to_day_date.month
                dates_tuple = calendar.monthrange(year, month)
                fromdate = "%s/%s/%s" %(month, 1, year)
                todate = "%s/%s/%s" %(month, date, year)
                custom = "Custom"
                siteids = None
            if affiliate_id:
                affiliate_obj = Affiliateinfo.objects.get(id=affiliate_id)
                commission = Affiliatecommission.objects.filter(fk_affiliateinfo=affiliate_obj)
            elif affiliates_obj:
                affiliate_obj = affiliates_obj[0]
                commission = Affiliatecommission.objects.filter(fk_affiliateinfo=affiliate_obj)
            else:
                affiliate_obj = ''
                commission = Affiliatecommission.objects.all()
            if not commission:
                commission = Affiliatecommission.objects.all()
            commission = commission[0]
            date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
            from_date = date_times_dict.get('fromdate')
            to_date = date_times_dict.get('todate')
            request.session['affiliate_dashboard_dates'] = date_times_dict
            context_dict = get_affiliate_admin_home_stats(affiliates_obj, commission, from_date, to_date, site_ids, currency)
            context_dict['device_list'] = ['Web', 'Mobile', 'Tablet', 'Television']
            context_dict['device_report'] = get_device_info_chart(affiliates_obj, affiliates_obj, commission, from_date, to_date, site_ids)
            
            context_dict['affiliates_count'] = len(affiliates_obj)
            context_dict['sites_count'] = len(site_ids_list)
            context_dict['affiliate_list'] = [[j.id, j.email] for j in affiliates_obj]
            context_dict['campaign_count'] = Campaign.objects.filter(fk_affiliateinfo__in=affiliates_obj).count()
            pending_aff = affiliates_obj.filter(status='Pending').count()
            rejected_aff = affiliates_obj.filter(status='Rejected').count()
            approved_aff = context_dict['affiliates_count'] - (pending_aff + rejected_aff)
            context_dict['pending_affiliates'] = [{'Id': aff.id, 'Email': str(aff.email),'Company': str(aff.companyname)} for aff in affiliates_obj.filter(status="Pending")[:5]]
            context_dict['affiliate_status'] = {'Pending': pending_aff, 
                            'Approved': approved_aff, 'Rejected': rejected_aff}

            affiliate_objs = affiliates_obj.exclude(username=acc_manager.user.username)
            header_list, data_list, currency_d = activity_admin_report_stats(affiliate_objs, from_date, to_date, site_ids, currency)
            data_dict_list = []
            for d in data_list:
                data_dict_list.append({v['title']: d[i] for i, v in enumerate(header_list)})
            data_b_dict_list = []
            header_b_list, data_b_list, currency_d = advertiser_reports_stats(affiliates_obj, from_date, to_date, site_ids, currency, None)
            for d in data_b_list:
                data_b_dict_list.append({v['title']: d[i] for i, v in enumerate(header_b_list)})
            from_date = date_times_dict.get('fromdate').strftime('%B %d, %Y')
            to_date = date_times_dict.get('todate').strftime('%B %d, %Y')
            ui_from_date = datetime_or_date_to_string(date_times_dict.get('fromdate'))
            ui_to_date = datetime_or_date_to_string(date_times_dict.get('todate'))
            context_dict['stats_date'] = "%s - %s" %(from_date, to_date)
            context_dict["ui_from_date"] = ui_from_date
            context_dict["ui_to_date"] = ui_to_date
            context_dict['sites_obj_list'] = [{'id':i.id, 'name': i.name } for i in site_ids_list]

            
            context_dict["data_b_list"] = data_b_dict_list
            context_dict["header_b_list"] = header_b_list
            context_dict["currency"] = request.GET.get("currency", 'euro')
            # context_dict["pagination_div"] = pagination_div
            # if request.GET.get("format") == "json":
            #     context_dict["status"] = "success"
            #     context_dict['data_list'] = data_list
            #     context_dict["header_list"] = header_list
            #     return HttpResponse(json.dumps(context_dict, indent=4))
            context_dict['domain'] = site_name
            context_dict['data_list'] = data_dict_list
            context_dict['partner'] = acc_manager.affiliateprogramme.name
            context_dict["header_list"] = header_list
            status_code = st.HTTP_200_OK
        except Exception as e:
            error_log()
            message = "Internal server error, %s" % e
            status = 'Error'
            status_code = st.HTTP_400_BAD_REQUEST
        return Response({'status': status, 'message': message, 'data': context_dict}, status=status_code)


class BarChart(viewsets.ViewSet):
    """
    url: /api/v1/barchart/
    Headers:{
        'Authorization': 'Token db975411fdad07e4c41c1639aecf20b2e87asdhsd5' (User login token)
    }
    data: {
        phone_preference: True,
        sms_preference: False,
        email_preference: True
        }
    responce:{status: "Success", message: "Preference Edited successfully"}
    """
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    # filter_backends = (filters.DjangoFilterBackend,filters.SearchFilter,)
    http_method_names = ['get', 'post']

    def list(self, request):
        context_dict = {}
        try:
            status = 'Success'
            message = ''
            acc_manager = None
            partnerid = request.custom_data.get('partnerid')
            try:
                user = User.objects.get(id=partnerid)
                acc_manager_obj = AccountManager.objects.filter(user=user)
                partuser_obj = []
                if acc_manager_obj:
                    acc_manager = acc_manager_obj[0]
                else:
                    partuser_obj = PartnerUser.objects.filter(user=user)
                    acc_manager = partuser_obj[0].accmanager
                if not acc_manager:
                    status_code = st.HTTP_400_BAD_REQUEST
                    message = partnerid + " is not in database in respect to any User."
                    return Response({'status': status, 'message': message}, status=status_code)
            except:
                logger.error(partnerid + " is not in database in respect to any User.")
                status = 'Error'
                status_code = st.HTTP_400_BAD_REQUEST
                message = partnerid + " is not in database in respect to any User."
                return Response({'status': status, 'message': message}, status=status_code)
            network_site_obj = NetworkAndSiteMap.objects.filter(networkid__in=acc_manager.affiliateprogramme.networkid.all())
            affiliates_obj = Affiliateinfo.objects.filter(account_manager=acc_manager)
            site_ids_list = get_brand_order(acc_manager)
            net_ids_list = [1]
            custom = str(request.GET.get("customid", 'Custom'))
            fromdate = str(request.GET.get("fromdate"))
            todate = str(request.GET.get("todate"))
            domain = request.GET.get("sitename")
            affiliate_id = request.GET.get("affiliateid")
            currency = request.GET.get("currency", 'euro')
            site_name = 'All Websites'
            site_ids = site_ids_list
            if domain and domain != 'All':
                site_domain_list = domain.split(',')
                site_ids = site_ids_list.filter(domain__in=site_domain_list)
                site_name = 'Multiple sites.'
                if len(domain.split(',')) <2:
                    site_name = site_ids_list.get(domain=site_domain_list[0]).name
            siteids = request.GET.getlist("siteids")
            if request.method == "GET" and fromdate == 'None':
                to_day_date = datetime.now()
                date = to_day_date.day
                year = to_day_date.year
                month = to_day_date.month
                dates_tuple = calendar.monthrange(year, month)
                fromdate = "%s/%s/%s" %(month, 1, year)
                todate = "%s/%s/%s" %(month, date, year)
                custom = "Custom"
                siteids = None
            if affiliate_id:
                affiliate_obj = Affiliateinfo.objects.get(id=affiliate_id)
                commission = Affiliatecommission.objects.filter(fk_affiliateinfo=affiliate_obj)
            elif affiliates_obj:
                affiliate_obj = affiliates_obj[0]
                commission = Affiliatecommission.objects.filter(fk_affiliateinfo=affiliate_obj)
            else:
                affiliate_obj = ''
                commission = Affiliatecommission.objects.all()
            if not commission:
                commission = Affiliatecommission.objects.all()
            commission = commission[0]
            date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
            from_date = date_times_dict.get('fromdate')
            to_date = date_times_dict.get('todate')
            request.session['affiliate_dashboard_dates'] = date_times_dict
            context_dict = get_affiliate_admin_home_stats(affiliates_obj, commission, from_date, to_date, site_ids, currency)
            del context_dict['date_data']
            # from_date = date_times_dict.get('fromdate').strftime('%B %d, %Y')
            # to_date = date_times_dict.get('todate').strftime('%B %d, %Y')
            # ui_from_date = datetime_or_date_to_string(date_times_dict.get('fromdate'))
            # ui_to_date = datetime_or_date_to_string(date_times_dict.get('todate'))
            # context_dict['stats_date'] = "%s - %s" %(from_date, to_date)
            # context_dict["ui_from_date"] = ui_from_date
            # context_dict["ui_to_date"] = ui_to_date
            context_dict['sites_obj_list'] = [{'id':i.id, 'name': i.name } for i in site_ids_list]
            # context_dict["pagination_div"] = pagination_div
            # if request.GET.get("format") == "json":
            #     context_dict["status"] = "success"
            #     context_dict['data_list'] = data_list
            #     context_dict["header_list"] = header_list
            #     return HttpResponse(json.dumps(context_dict, indent=4))
            context_dict['domain'] = site_name
            context_dict['part_user'] = ast.literal_eval(partuser_obj[0].options) if partuser_obj else []
            context_dict['partner'] = acc_manager.affiliateprogramme.name
            
            status_code = st.HTTP_200_OK
        except Exception as e:
            error_log()
            message = "Internal server error, %s" % e
            status = 'Error'
            status_code = st.HTTP_400_BAD_REQUEST
        return Response({'status': status, 'message': message, 'data': context_dict}, status=status_code)


class AffiliatePerformance(viewsets.ViewSet):
    """
    url: /api/v1/barchart/
    Headers:{
        'Authorization': 'Token db975411fdad07e4c41c1639aecf20b2e87asdhsd5' (User login token)
    }
    data: {
        phone_preference: True,
        sms_preference: False,
        email_preference: True
        }
    responce:{status: "Success", message: "Preference Edited successfully"}
    """
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    # filter_backends = (filters.DjangoFilterBackend,filters.SearchFilter,)
    http_method_names = ['get', 'post']

    def list(self, request):
        context_dict = {}
        try:
            status = 'Success'
            message = ''
            acc_manager = None
            partnerid = request.custom_data.get('partnerid')
            try:
                user = User.objects.get(id=partnerid)
                acc_manager_obj = AccountManager.objects.filter(user=user)
                partuser_obj = []
                if acc_manager_obj:
                    acc_manager = acc_manager_obj[0]
                else:
                    partuser_obj = PartnerUser.objects.filter(user=user)
                    acc_manager = partuser_obj[0].accmanager
                if not acc_manager:
                    status_code = st.HTTP_400_BAD_REQUEST
                    message = partnerid + " is not in database in respect to any User."
                    return Response({'status': status, 'message': message}, status=status_code)
            except:
                logger.error(partnerid + " is not in database in respect to any User.")
                status = 'Error'
                status_code = st.HTTP_400_BAD_REQUEST
                message = partnerid + " is not in database in respect to any User."
                return Response({'status': status, 'message': message}, status=status_code)
            network_site_obj = NetworkAndSiteMap.objects.filter(networkid__in=acc_manager.affiliateprogramme.networkid.all())
            affiliates_obj = Affiliateinfo.objects.filter(account_manager=acc_manager)
            site_ids_list = get_brand_order(acc_manager)
            net_ids_list = [1]
            custom = str(request.GET.get("customid", 'Custom'))
            fromdate = str(request.GET.get("fromdate"))
            todate = str(request.GET.get("todate"))
            domain = request.GET.get("sitename")
            affiliate_id = request.GET.get("affiliateid")
            currency = request.GET.get("currency", 'euro')
            site_name = 'All Websites'
            site_ids = site_ids_list
            if domain and domain != 'All':
                site_domain_list = domain.split(',')
                site_ids = site_ids_list.filter(domain__in=site_domain_list)
                site_name = 'Multiple sites.'
                if len(domain.split(',')) <2:
                    site_name = site_ids_list.get(domain=site_domain_list[0]).name
            siteids = request.GET.getlist("siteids")
            if request.method == "GET" and fromdate == 'None':
                to_day_date = datetime.now()
                date = to_day_date.day
                year = to_day_date.year
                month = to_day_date.month
                dates_tuple = calendar.monthrange(year, month)
                fromdate = "%s/%s/%s" %(month, 1, year)
                todate = "%s/%s/%s" %(month, date, year)
                custom = "Custom"
                siteids = None
            date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
            from_date = date_times_dict.get('fromdate')
            to_date = date_times_dict.get('todate')
            affiliate_objs = affiliates_obj.exclude(username=acc_manager.user.username)
            header_list, data_list, currency_d = activity_admin_report_stats(affiliate_objs, from_date, to_date, site_ids, currency)
            context_dict['data_list'] = data_list
            context_dict['partner'] = acc_manager.affiliateprogramme.name
            context_dict["header_list"] = header_list

            context_dict['sites_obj_list'] = [{'id':i.id, 'name': i.name } for i in site_ids_list]
            # context_dict["pagination_div"] = pagination_div
            # if request.GET.get("format") == "json":
            #     context_dict["status"] = "success"
            #     context_dict['data_list'] = data_list
            #     context_dict["header_list"] = header_list
            #     return HttpResponse(json.dumps(context_dict, indent=4))
            context_dict['domain'] = site_name
            
            context_dict['partner'] = acc_manager.affiliateprogramme.name
            
            status_code = st.HTTP_200_OK
        except Exception as e:
            error_log()
            message = "Internal server error, %s" % e
            status = 'Error'
            status_code = st.HTTP_400_BAD_REQUEST
        return Response({'status': status, 'message': message, 'data': context_dict}, status=status_code)


class PlayerReport(viewsets.ViewSet):
    """
    url: /api/v1/playerreport/
    Headers:{
        'Authorization': 'Token db975411fdad07e4c41c1639aecf20b2e87asdhsd5' (User login token)
    }
    data: {
        }
    responce:{status: "Success", message: "Preference Edited successfully"}
    """
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    # filter_backends = (filters.DjangoFilterBackend,filters.SearchFilter,)
    http_method_names = ['get', 'post']

    def list(self, request):
        context_dict = {}
        try:
            status = 'Success'
            message = ''
            acc_manager = None
            partnerid = request.custom_data.get('partnerid')
            try:
                user = User.objects.get(id=partnerid)
                acc_manager_obj = AccountManager.objects.filter(user=user)
                partuser_obj = []
                if acc_manager_obj:
                    acc_manager = acc_manager_obj[0]
                else:
                    partuser_obj = PartnerUser.objects.filter(user=user)
                    acc_manager = partuser_obj[0].accmanager
                if not acc_manager:
                    status_code = st.HTTP_400_BAD_REQUEST
                    message = partnerid + " is not in database in respect to any User."
                    return Response({'status': status, 'message': message}, status=status_code)
            except:
                logger.error(partnerid + " is not in database in respect to any User.")
                status = 'Error'
                status_code = st.HTTP_400_BAD_REQUEST
                message = partnerid + " is not in database in respect to any User."
                return Response({'status': status, 'message': message}, status=status_code)
            network_site_obj = NetworkAndSiteMap.objects.filter(networkid__in=acc_manager.affiliateprogramme.networkid.all())
            site_ids_list = get_brand_order(acc_manager)
            net_ids_list = [1]
            custom = str(request.GET.get("customid", 'Custom'))
            fromdate = str(request.GET.get("fromdate"))
            todate = str(request.GET.get("todate"))
            domain = request.GET.get("sitename")
            affiliate_id = request.GET.get("affiliateid")
            site_name = 'All Websites'
            site_ids = site_ids_list
            if domain and domain != 'All':
                site_domain_list = domain.split(',')
                site_ids = site_ids_list.filter(domain__in=site_domain_list)
                site_name = 'Multiple sites.'
                if len(domain.split(',')) <2:
                    site_name = site_ids_list.get(domain=site_domain_list[0]).name
            siteids = request.GET.getlist("siteids")
            if request.method == "GET" and fromdate == 'None':
                to_day_date = datetime.now()
                date = to_day_date.day
                year = to_day_date.year
                month = to_day_date.month
                dates_tuple = calendar.monthrange(year, month)
                fromdate = "%s/%s/%s" %(month, 1, year)
                todate = "%s/%s/%s" %(month, date, year)
                custom = "Custom"
                siteids = None
            date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
            from_date = date_times_dict.get('fromdate')
            to_date = date_times_dict.get('todate')
            country_list = Country.objects.all().values_list('countryname', flat=True).distinct()
            country = request.POST.getlist('country')
            # if country:
            #     country = [country]
            if not country:
                country = list(country_list)
                country.append('')
            aff_id = request.GET.get('aff_id')
            currencyfilter = request.GET.get("currency", 'euro')
            group_d = str(request.GET.get("groupby", 'brand'))
            sitefilter = request.GET.getlist("siteid")
            if sitefilter:
                site_ids = site_ids.filter(id__in=sitefilter)
            else:
                sitefilter = [str(i.id) for i in site_ids]
            aff_list = Affiliateinfo.objects.filter(account_manager=acc_manager).order_by('-id')
            if aff_id:
                aff_m_list = aff_list.filter(id=aff_id)
            else:
                aff_m_list = aff_list
            camp_track_links = CampaignTrackerMapping.objects.filter(
                    fk_affiliateinfo__in=aff_m_list,
                    fk_campaign__siteid__in=site_ids).select_related('fk_campaign', 'fk_affiliateinfo')
            header_list, data_list, currency = Process_activedepositor_stats(acc_manager, from_date, 
                to_date, camp_track_links, site_ids, country)

            data_dict_list = []
            for d in data_list:
                data_dict_list.append({v['title']: d[i] for i, v in enumerate(header_list)})
            context_dict['data_list'] = data_dict_list
            context_dict["header_list"] = header_list

            context_dict['sites_obj_list'] = [{'id':i.id, 'name': i.name } for i in site_ids_list]
            context_dict["currency"] = currency
            context_dict["aff_list"] = [{'id':i.id, 'username': i.email.replace('vcommsubaff', '')} for i in aff_list]
            context_dict["country_list"] = country_list
            # context_dict["pagination_div"] = pagination_div
            # if request.GET.get("format") == "json":
            #     context_dict["status"] = "success"
            #     context_dict['data_list'] = data_list
            #     context_dict["header_list"] = header_list
            #     return HttpResponse(json.dumps(context_dict, indent=4))
            context_dict['domain'] = site_name
            context_dict['partner'] = acc_manager.affiliateprogramme.name
            status_code = st.HTTP_200_OK
        except Exception as e:
            error_log()
            message = "Internal server error, %s" % e
            status = 'Error'
            status_code = st.HTTP_400_BAD_REQUEST
        return Response({'status': status, 'message': message, 'data': context_dict}, status=status_code)


class AffiliateReport(viewsets.ViewSet):
    """
    url: /api/v1/affiliateactivity/
    Headers:{
        'Authorization': 'Token db975411fdad07e4c41c1639aecf20b2e87asdhsd5' (User login token)
    }
    data: {
        }
    responce:{status: "Success", message: "Preference Edited successfully"}
    """
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    # filter_backends = (filters.DjangoFilterBackend,filters.SearchFilter,)
    http_method_names = ['get', 'post']

    def list(self, request):
        context_dict = {}
        try:
            status = 'Success'
            message = ''
            acc_manager = None
            partnerid = request.custom_data.get('partnerid')
            try:
                user = User.objects.get(id=partnerid)
                acc_manager_obj = AccountManager.objects.filter(user=user)
                partuser_obj = []
                if acc_manager_obj:
                    acc_manager = acc_manager_obj[0]
                else:
                    partuser_obj = PartnerUser.objects.filter(user=user)
                    acc_manager = partuser_obj[0].accmanager
                if not acc_manager:
                    status_code = st.HTTP_400_BAD_REQUEST
                    message = partnerid + " is not in database in respect to any User."
                    return Response({'status': status, 'message': message}, status=status_code)
            except:
                logger.error(partnerid + " is not in database in respect to any User.")
                status = 'Error'
                status_code = st.HTTP_400_BAD_REQUEST
                message = partnerid + " is not in database in respect to any User."
                return Response({'status': status, 'message': message}, status=status_code)
            network_site_obj = NetworkAndSiteMap.objects.filter(networkid__in=acc_manager.affiliateprogramme.networkid.all())
            site_ids_list = get_brand_order(acc_manager)
            net_ids_list = [1]
            custom = str(request.GET.get("customid", 'Custom'))
            fromdate = str(request.GET.get("fromdate"))
            todate = str(request.GET.get("todate"))
            domain = request.GET.get("sitename")
            affiliate_id = request.GET.get("affiliateid")
            site_name = 'All Websites'
            site_ids = site_ids_list
            if domain and domain != 'All':
                site_domain_list = domain.split(',')
                site_ids = site_ids_list.filter(domain__in=site_domain_list)
                site_name = 'Multiple sites.'
                if len(domain.split(',')) <2:
                    site_name = site_ids_list.get(domain=site_domain_list[0]).name
            siteids = request.GET.getlist("siteids")
            if request.method == "GET" and fromdate == 'None':
                to_day_date = datetime.now()
                date = to_day_date.day
                year = to_day_date.year
                month = to_day_date.month
                dates_tuple = calendar.monthrange(year, month)
                fromdate = "%s/%s/%s" %(month, 1, year)
                todate = "%s/%s/%s" %(month, date, year)
                custom = "Custom"
                siteids = None
            date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
            from_date = date_times_dict.get('fromdate')
            to_date = date_times_dict.get('todate')
            country = request.GET.getlist('country')
            aff_id = request.GET.get('aff_id')
            currencyfilter = request.GET.get("currency", 'euro')
            group_d = str(request.GET.get("groupby", 'brand'))
            country_list = Country.objects.all().values_list('countryname', flat=True).distinct()
            sitefilter = request.GET.getlist("siteid")
            if sitefilter:
                site_ids = site_ids.filter(id__in=sitefilter)
            else:
                sitefilter = [str(i.id) for i in site_ids]
            aff_list = Affiliateinfo.objects.filter(account_manager=acc_manager).order_by('-id')
            if aff_id:
                aff_m_list = aff_list.filter(id=aff_id)
            else:
                aff_m_list = aff_list
            camp_track_links = CampaignTrackerMapping.objects.filter(
                    fk_affiliateinfo__in=aff_m_list,
                    fk_campaign__siteid__in=site_ids).select_related('fk_campaign', 'fk_affiliateinfo')
            header_list, data_list, currency = affiliate_group_report_stats(acc_manager, from_date, to_date, camp_track_links,
                    site_ids, aff_m_list, country, group_d, currencyfilter)
            data_dict_list = []
            for d in data_list:
                data_dict_list.append({v['title']: d[i] for i, v in enumerate(header_list)})
            context_dict['data_list'] = data_dict_list
            context_dict["header_list"] = header_list

            context_dict['sites_obj_list'] = [{'id':i.id, 'name': i.name } for i in site_ids_list]
            context_dict["aff_list"] = [{'id':i.id, 'username': i.email.replace('vcommsubaff', '')} for i in aff_list]
            context_dict["country_list"] = country_list
            context_dict["currency"] = currency
            # context_dict["pagination_div"] = pagination_div
            # if request.GET.get("format") == "json":
            #     context_dict["status"] = "success"
            #     context_dict['data_list'] = data_list
            #     context_dict["header_list"] = header_list
            #     return HttpResponse(json.dumps(context_dict, indent=4))
            context_dict['domain'] = site_name
            context_dict['partner'] = acc_manager.affiliateprogramme.name
            status_code = st.HTTP_200_OK
        except Exception as e:
            error_log()
            message = "Internal server error, %s" % e
            status = 'Error'
            status_code = st.HTTP_400_BAD_REQUEST
        return Response({'status': status, 'message': message, 'data': context_dict}, status=status_code)

class DynamicReport(viewsets.ViewSet):
    """
    url: /api/v1/dynamicreport/
    Headers:{
        'Authorization': 'Token db975411fdad07e4c41c1639aecf20b2e87asdhsd5' (User login token)
    }
    data: {
        }
    responce:{status: "Success", message: "Preference Edited successfully"}
    """
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    # filter_backends = (filters.DjangoFilterBackend,filters.SearchFilter,)
    http_method_names = ['get', 'post']

    def list(self, request):
        context_dict = {}
        try:
            status = 'Success'
            message = ''
            acc_manager = None
            partnerid = request.custom_data.get('partnerid')
            try:
                user = User.objects.get(id=partnerid)
                acc_manager_obj = AccountManager.objects.filter(user=user)
                partuser_obj = []
                if acc_manager_obj:
                    acc_manager = acc_manager_obj[0]
                else:
                    partuser_obj = PartnerUser.objects.filter(user=user)
                    acc_manager = partuser_obj[0].accmanager
                if not acc_manager:
                    status_code = st.HTTP_400_BAD_REQUEST
                    message = partnerid + " is not in database in respect to any User."
                    return Response({'status': status, 'message': message}, status=status_code)
            except:
                logger.error(partnerid + " is not in database in respect to any User.")
                status = 'Error'
                status_code = st.HTTP_400_BAD_REQUEST
                message = partnerid + " is not in database in respect to any User."
                return Response({'status': status, 'message': message}, status=status_code)
            network_site_obj = NetworkAndSiteMap.objects.filter(networkid__in=acc_manager.affiliateprogramme.networkid.all())
            site_ids_list = get_brand_order(acc_manager)
            net_ids_list = [1]
            custom = str(request.GET.get("customid", 'Custom'))
            fromdate = str(request.GET.get("fromdate"))
            todate = str(request.GET.get("todate"))
            domain = request.GET.get("sitename")
            affiliate_id = request.GET.get("affiliateid")
            site_name = 'All Websites'
            site_ids = site_ids_list
            if domain and domain != 'All':
                site_domain_list = domain.split(',')
                site_ids = site_ids_list.filter(domain__in=site_domain_list)
                site_name = 'Multiple sites.'
                if len(domain.split(',')) <2:
                    site_name = site_ids_list.get(domain=site_domain_list[0]).name
            siteids = request.GET.getlist("siteids")
            if request.method == "GET" and fromdate == 'None':
                to_day_date = datetime.now()
                date = to_day_date.day
                year = to_day_date.year
                month = to_day_date.month
                dates_tuple = calendar.monthrange(year, month)
                fromdate = "%s/%s/%s" %(month, 1, year)
                todate = "%s/%s/%s" %(month, date, year)
                custom = "Custom"
                siteids = None
            date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
            from_date = date_times_dict.get('fromdate')
            to_date = date_times_dict.get('todate')
            country = request.GET.getlist('country')
            aff_id = request.GET.get('aff_id')
            currencyfilter = request.GET.get("currency", 'euro')
            group_d = str(request.GET.get("groupby", 'brand'))
            country_list = Country.objects.all().values_list('countryname', flat=True).distinct()
            sitefilter = request.GET.getlist("siteid")
            if sitefilter:
                site_ids = site_ids.filter(id__in=sitefilter)
            else:
                sitefilter = [str(i.id) for i in site_ids]
            aff_list = Affiliateinfo.objects.filter(account_manager=acc_manager).order_by('-id')
            if aff_id:
                aff_m_list = aff_list.filter(id=aff_id)
            else:
                aff_m_list = aff_list
            camp_track_links = CampaignTrackerMapping.objects.filter(
                    fk_affiliateinfo__in=aff_m_list,
                    fk_campaign__siteid__in=site_ids).select_related('fk_campaign', 'fk_affiliateinfo')
            header_list, data_list, currency = dynamic_reports_admin_stats(acc_manager, from_date, to_date,
                    camp_track_links, site_ids, aff_m_list, country, currencyfilter, group_d)
            data_dict_list = []
            for d in data_list:
                data_dict_list.append({v['title']: d[i] for i, v in enumerate(header_list)})
            context_dict['data_list'] = data_dict_list
            context_dict["header_list"] = header_list

            context_dict['sites_obj_list'] = [{'id':i.id, 'name': i.name } for i in site_ids_list]
            context_dict["aff_list"] = [{'id':i.id, 'username': i.email.replace('vcommsubaff', '')} for i in aff_list]
            context_dict["country_list"] = country_list
            context_dict["currency"] = currency
            # context_dict["pagination_div"] = pagination_div
            # if request.GET.get("format") == "json":
            #     context_dict["status"] = "success"
            #     context_dict['data_list'] = data_list
            #     context_dict["header_list"] = header_list
            #     return HttpResponse(json.dumps(context_dict, indent=4))
            context_dict['domain'] = site_name
            context_dict['partner'] = acc_manager.affiliateprogramme.name
            status_code = st.HTTP_200_OK
        except Exception as e:
            error_log()
            message = "Internal server error, %s" % e
            status = 'Error'
            status_code = st.HTTP_400_BAD_REQUEST
        return Response({'status': status, 'message': message, 'data': context_dict}, status=status_code)


class ConversionReport(viewsets.ViewSet):
    """
    url: /api/v1/conversionreport/
    Headers:{
        'Authorization': 'Token db975411fdad07e4c41c1639aecf20b2e87asdhsd5' (User login token)
    }
    data: {
        }
    responce:{status: "Success", message: "Preference Edited successfully"}
    """
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    # filter_backends = (filters.DjangoFilterBackend,filters.SearchFilter,)
    http_method_names = ['get', 'post']

    def list(self, request):
        context_dict = {}
        try:
            status = 'Success'
            message = ''
            acc_manager = None
            partnerid = request.custom_data.get('partnerid')
            try:
                user = User.objects.get(id=partnerid)
                acc_manager_obj = AccountManager.objects.filter(user=user)
                partuser_obj = []
                if acc_manager_obj:
                    acc_manager = acc_manager_obj[0]
                else:
                    partuser_obj = PartnerUser.objects.filter(user=user)
                    acc_manager = partuser_obj[0].accmanager
                if not acc_manager:
                    status_code = st.HTTP_400_BAD_REQUEST
                    message = partnerid + " is not in database in respect to any User."
                    return Response({'status': status, 'message': message}, status=status_code)
            except:
                logger.error(partnerid + " is not in database in respect to any User.")
                status = 'Error'
                status_code = st.HTTP_400_BAD_REQUEST
                message = partnerid + " is not in database in respect to any User."
                return Response({'status': status, 'message': message}, status=status_code)
            network_site_obj = NetworkAndSiteMap.objects.filter(networkid__in=acc_manager.affiliateprogramme.networkid.all())
            site_ids_list = get_brand_order(acc_manager)
            net_ids_list = [1]
            custom = str(request.GET.get("customid", 'Custom'))
            fromdate = str(request.GET.get("fromdate"))
            todate = str(request.GET.get("todate"))
            domain = request.GET.get("sitename")
            affiliate_id = request.GET.get("affiliateid")
            site_name = 'All Websites'
            site_ids = site_ids_list
            if domain and domain != 'All':
                site_domain_list = domain.split(',')
                site_ids = site_ids_list.filter(domain__in=site_domain_list)
                site_name = 'Multiple sites.'
                if len(domain.split(',')) <2:
                    site_name = site_ids_list.get(domain=site_domain_list[0]).name
            siteids = request.GET.getlist("siteids")
            if request.method == "GET" and fromdate == 'None':
                to_day_date = datetime.now()
                date = to_day_date.day
                year = to_day_date.year
                month = to_day_date.month
                dates_tuple = calendar.monthrange(year, month)
                fromdate = "%s/%s/%s" %(month, 1, year)
                todate = "%s/%s/%s" %(month, date, year)
                custom = "Custom"
                siteids = None
            date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
            from_date = date_times_dict.get('fromdate')
            to_date = date_times_dict.get('todate')
            country = request.GET.getlist('country')
            aff_id = request.GET.get('aff_id')
            currencyfilter = request.GET.get("currency", 'euro')
            group_d = str(request.GET.get("groupby", 'brand'))
            country_list = Country.objects.all().values_list('countryname', flat=True).distinct()
            sitefilter = request.GET.getlist("siteid")
            if sitefilter:
                site_ids = site_ids.filter(id__in=sitefilter)
            else:
                sitefilter = [str(i.id) for i in site_ids]
            aff_list = Affiliateinfo.objects.filter(account_manager=acc_manager).order_by('-id')
            if aff_id:
                aff_m_list = aff_list.filter(id=aff_id)
            else:
                aff_m_list = aff_list
            camp_track_links = CampaignTrackerMapping.objects.filter(
                    fk_affiliateinfo__in=aff_m_list,
                    fk_campaign__siteid__in=site_ids).select_related('fk_campaign', 'fk_affiliateinfo')
            header_list, data_list, currency = Process_conversion_stats(acc_manager, from_date, to_date, 
                    camp_track_links, site_ids, country, currencyfilter, group_d)
            data_dict_list = []
            for d in data_list:
                data_dict_list.append({v['title']: d[i] for i, v in enumerate(header_list)})
            context_dict['data_list'] = data_dict_list
            context_dict["header_list"] = header_list

            context_dict['sites_obj_list'] = [{'id':i.id, 'name': i.name } for i in site_ids_list]
            context_dict["aff_list"] = [{'id':i.id, 'username': i.email.replace('vcommsubaff', '')} for i in aff_list]
            context_dict["country_list"] = country_list
            context_dict["currency"] = currency
            # context_dict["pagination_div"] = pagination_div
            # if request.GET.get("format") == "json":
            #     context_dict["status"] = "success"
            #     context_dict['data_list'] = data_list
            #     context_dict["header_list"] = header_list
            #     return HttpResponse(json.dumps(context_dict, indent=4))
            context_dict['domain'] = site_name
            context_dict['partner'] = acc_manager.affiliateprogramme.name
            status_code = st.HTTP_200_OK
        except Exception as e:
            error_log()
            message = "Internal server error, %s" % e
            status = 'Error'
            status_code = st.HTTP_400_BAD_REQUEST
        return Response({'status': status, 'message': message, 'data': context_dict}, status=status_code)


class CampaignReport(viewsets.ViewSet):
    """
    url: /api/v1/campaignreport/
    Headers:{
        'Authorization': 'Token db975411fdad07e4c41c1639aecf20b2e87asdhsd5' (User login token)
    }
    data: {
        }
    responce:{status: "Success", message: "Preference Edited successfully"}
    """
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    # filter_backends = (filters.DjangoFilterBackend,filters.SearchFilter,)
    http_method_names = ['get', 'post']

    def list(self, request):
        context_dict = {}
        try:
            status = 'Success'
            message = ''
            acc_manager = None
            partnerid = request.custom_data.get('partnerid')
            try:
                user = User.objects.get(id=partnerid)
                acc_manager_obj = AccountManager.objects.filter(user=user)
                partuser_obj = []
                if acc_manager_obj:
                    acc_manager = acc_manager_obj[0]
                else:
                    partuser_obj = PartnerUser.objects.filter(user=user)
                    acc_manager = partuser_obj[0].accmanager
                if not acc_manager:
                    status_code = st.HTTP_400_BAD_REQUEST
                    message = partnerid + " is not in database in respect to any User."
                    return Response({'status': status, 'message': message}, status=status_code)
            except:
                logger.error(partnerid + " is not in database in respect to any User.")
                status = 'Error'
                status_code = st.HTTP_400_BAD_REQUEST
                message = partnerid + " is not in database in respect to any User."
                return Response({'status': status, 'message': message}, status=status_code)
            network_site_obj = NetworkAndSiteMap.objects.filter(networkid__in=acc_manager.affiliateprogramme.networkid.all())
            site_ids_list = get_brand_order(acc_manager)
            net_ids_list = [1]
            custom = str(request.GET.get("customid", 'Custom'))
            fromdate = str(request.GET.get("fromdate"))
            todate = str(request.GET.get("todate"))
            domain = request.GET.get("sitename")
            affiliate_id = request.GET.get("affiliateid")
            site_name = 'All Websites'
            site_ids = site_ids_list
            if domain and domain != 'All':
                site_domain_list = domain.split(',')
                site_ids = site_ids_list.filter(domain__in=site_domain_list)
                site_name = 'Multiple sites.'
                if len(domain.split(',')) <2:
                    site_name = site_ids_list.get(domain=site_domain_list[0]).name
            siteids = request.GET.getlist("siteids")
            if request.method == "GET" and fromdate == 'None':
                to_day_date = datetime.now()
                date = to_day_date.day
                year = to_day_date.year
                month = to_day_date.month
                dates_tuple = calendar.monthrange(year, month)
                fromdate = "%s/%s/%s" %(month, 1, year)
                todate = "%s/%s/%s" %(month, date, year)
                custom = "Custom"
                siteids = None
            date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
            from_date = date_times_dict.get('fromdate')
            to_date = date_times_dict.get('todate')
            country = request.GET.getlist('country')
            aff_id = request.GET.get('aff_id')
            currencyfilter = request.GET.get("currency", 'euro')
            group_d = str(request.GET.get("groupby", 'brand'))
            country_list = Country.objects.all().values_list('countryname', flat=True).distinct()
            sitefilter = request.GET.getlist("siteid")
            if sitefilter:
                site_ids = site_ids.filter(id__in=sitefilter)
            else:
                sitefilter = [str(i.id) for i in site_ids]
            aff_list = Affiliateinfo.objects.filter(account_manager=acc_manager).order_by('-id')
            if aff_id:
                aff_m_list = aff_list.filter(id=aff_id)
            else:
                aff_m_list = aff_list
            camp_track_links = CampaignTrackerMapping.objects.filter(
                    fk_affiliateinfo__in=aff_m_list,
                    fk_campaign__siteid__in=site_ids).select_related('fk_campaign', 'fk_affiliateinfo')
            header_list, data_list, currency = campaign_reports_admin_stats(acc_manager, from_date, to_date,
                    camp_track_links, site_ids, aff_m_list, country, currencyfilter, group_d)
            data_dict_list = []
            for d in data_list:
                data_dict_list.append({v['title']: d[i] for i, v in enumerate(header_list)})
            context_dict['data_list'] = data_dict_list
            context_dict["header_list"] = header_list

            context_dict['sites_obj_list'] = [{'id':i.id, 'name': i.name } for i in site_ids_list]
            context_dict["aff_list"] = [{'id':i.id, 'username': i.email.replace('vcommsubaff', '')} for i in aff_list]
            context_dict["country_list"] = country_list
            context_dict["currency"] = currency
            # context_dict["pagination_div"] = pagination_div
            # if request.GET.get("format") == "json":
            #     context_dict["status"] = "success"
            #     context_dict['data_list'] = data_list
            #     context_dict["header_list"] = header_list
            #     return HttpResponse(json.dumps(context_dict, indent=4))
            context_dict['domain'] = site_name
            context_dict['partner'] = acc_manager.affiliateprogramme.name
            status_code = st.HTTP_200_OK
        except Exception as e:
            error_log()
            message = "Internal server error, %s" % e
            status = 'Error'
            status_code = st.HTTP_400_BAD_REQUEST
        return Response({'status': status, 'message': message, 'data': context_dict}, status=status_code)


class EarningsReport(viewsets.ViewSet):
    """
    url: /api/v1/earningsreport/
    Headers:{
        'Authorization': 'Token db975411fdad07e4c41c1639aecf20b2e87asdhsd5' (User login token)
    }
    data: {
        }
    responce:{status: "Success", message: "Preference Edited successfully"}
    """
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    # filter_backends = (filters.DjangoFilterBackend,filters.SearchFilter,)
    http_method_names = ['get', 'post']

    def list(self, request):
        context_dict = {}
        try:
            status = 'Success'
            message = ''
            acc_manager = None
            partnerid = request.custom_data.get('partnerid')
            try:
                user = User.objects.get(id=partnerid)
                acc_manager_obj = AccountManager.objects.filter(user=user)
                partuser_obj = []
                if acc_manager_obj:
                    acc_manager = acc_manager_obj[0]
                else:
                    partuser_obj = PartnerUser.objects.filter(user=user)
                    acc_manager = partuser_obj[0].accmanager
                if not acc_manager:
                    status_code = st.HTTP_400_BAD_REQUEST
                    message = partnerid + " is not in database in respect to any User."
                    return Response({'status': status, 'message': message}, status=status_code)
            except:
                logger.error(partnerid + " is not in database in respect to any User.")
                status = 'Error'
                status_code = st.HTTP_400_BAD_REQUEST
                message = partnerid + " is not in database in respect to any User."
                return Response({'status': status, 'message': message}, status=status_code)
            network_site_obj = NetworkAndSiteMap.objects.filter(networkid__in=acc_manager.affiliateprogramme.networkid.all())
            site_ids_list = get_brand_order(acc_manager)
            net_ids_list = [1]
            custom = str(request.GET.get("customid", 'Custom'))
            fromdate = str(request.GET.get("fromdate"))
            todate = str(request.GET.get("todate"))
            domain = request.GET.get("sitename")
            affiliate_id = request.GET.get("affiliateid")
            site_name = 'All Websites'
            site_ids = site_ids_list
            if domain and domain != 'All':
                site_domain_list = domain.split(',')
                site_ids = site_ids_list.filter(domain__in=site_domain_list)
                site_name = 'Multiple sites.'
                if len(domain.split(',')) <2:
                    site_name = site_ids_list.get(domain=site_domain_list[0]).name
            siteids = request.GET.getlist("siteids")
            if request.method == "GET" and fromdate == 'None':
                to_day_date = datetime.now()
                date = to_day_date.day
                year = to_day_date.year
                month = to_day_date.month
                dates_tuple = calendar.monthrange(year, month)
                fromdate = "%s/%s/%s" %(month, 1, year)
                todate = "%s/%s/%s" %(month, date, year)
                custom = "Custom"
                siteids = None
            date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
            from_date = date_times_dict.get('fromdate')
            to_date = date_times_dict.get('todate')
            country = request.GET.getlist('country')
            aff_id = request.GET.get('aff_id')
            currencyfilter = request.GET.get("currency", 'euro')
            group_d = str(request.GET.get("groupby", 'brand'))
            country_list = Country.objects.all().values_list('countryname', flat=True).distinct()
            sitefilter = request.GET.getlist("siteid")
            if sitefilter:
                site_ids = site_ids.filter(id__in=sitefilter)
            else:
                sitefilter = [str(i.id) for i in site_ids]
            aff_list = Affiliateinfo.objects.filter(account_manager=acc_manager).order_by('-id')
            if aff_id:
                aff_m_list = aff_list.filter(id=aff_id)
            else:
                aff_m_list = aff_list
            camp_track_links = CampaignTrackerMapping.objects.filter(
                    fk_affiliateinfo__in=aff_m_list,
                    fk_campaign__siteid__in=site_ids).select_related('fk_campaign', 'fk_affiliateinfo')
            header_list, data_list, currency = Process_campaign_admin_stats(acc_manager, from_date, to_date,
                        camp_track_links, site_ids, aff_m_list, country, group_d, currencyfilter)
            data_dict_list = []
            for d in data_list:
                data_dict_list.append({v['title']: d[i] for i, v in enumerate(header_list)})
            context_dict['data_list'] = data_dict_list
            context_dict["header_list"] = header_list

            context_dict['sites_obj_list'] = [{'id':i.id, 'name': i.name } for i in site_ids_list]
            context_dict["aff_list"] = [{'id':i.id, 'username': i.email.replace('vcommsubaff', '')} for i in aff_list]
            context_dict["country_list"] = country_list
            context_dict["currency"] = currency
            # context_dict["pagination_div"] = pagination_div
            # if request.GET.get("format") == "json":
            #     context_dict["status"] = "success"
            #     context_dict['data_list'] = data_list
            #     context_dict["header_list"] = header_list
            #     return HttpResponse(json.dumps(context_dict, indent=4))
            context_dict['domain'] = site_name
            context_dict['partner'] = acc_manager.affiliateprogramme.name
            status_code = st.HTTP_200_OK
        except Exception as e:
            error_log()
            message = "Internal server error, %s" % e
            status = 'Error'
            status_code = st.HTTP_400_BAD_REQUEST
        return Response({'status': status, 'message': message, 'data': context_dict}, status=status_code)


class BrandReport(viewsets.ViewSet):
    """
    url: /api/v1/barchart/
    Headers:{
        'Authorization': 'Token db975411fdad07e4c41c1639aecf20b2e87asdhsd5' (User login token)
    }
    data: {
        phone_preference: True,
        sms_preference: False,
        email_preference: True
        }
    responce:{status: "Success", message: "Preference Edited successfully"}
    """
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    # filter_backends = (filters.DjangoFilterBackend,filters.SearchFilter,)
    http_method_names = ['get', 'post']

    def list(self, request):
        context_dict = {}
        try:
            status = 'Success'
            message = ''
            acc_manager = None
            partnerid = request.custom_data.get('partnerid')
            try:
                user = User.objects.get(id=partnerid)
                acc_manager_obj = AccountManager.objects.filter(user=user)
                partuser_obj = []
                if acc_manager_obj:
                    acc_manager = acc_manager_obj[0]
                else:
                    partuser_obj = PartnerUser.objects.filter(user=user)
                    acc_manager = partuser_obj[0].accmanager
                if not acc_manager:
                    status_code = st.HTTP_400_BAD_REQUEST
                    message = partnerid + " is not in database in respect to any User."
                    return Response({'status': status, 'message': message}, status=status_code)
            except:
                logger.error(partnerid + " is not in database in respect to any User.")
                status = 'Error'
                status_code = st.HTTP_400_BAD_REQUEST
                message = partnerid + " is not in database in respect to any User."
                return Response({'status': status, 'message': message}, status=status_code)
            network_site_obj = NetworkAndSiteMap.objects.filter(networkid__in=acc_manager.affiliateprogramme.networkid.all())
            site_ids_list = get_brand_order(acc_manager)
            net_ids_list = [1]
            custom = str(request.GET.get("customid", 'Custom'))
            fromdate = str(request.GET.get("fromdate"))
            todate = str(request.GET.get("todate"))
            domain = request.GET.get("sitename")
            affiliate_id = request.GET.get("affiliateid")
            site_name = 'All Websites'
            site_ids = site_ids_list
            if domain and domain != 'All':
                site_domain_list = domain.split(',')
                site_ids = site_ids_list.filter(domain__in=site_domain_list)
                site_name = 'Multiple sites.'
                if len(domain.split(',')) <2:
                    site_name = site_ids_list.get(domain=site_domain_list[0]).name
            siteids = request.GET.getlist("siteids")
            if request.method == "GET" and fromdate == 'None':
                to_day_date = datetime.now()
                date = to_day_date.day
                year = to_day_date.year
                month = to_day_date.month
                dates_tuple = calendar.monthrange(year, month)
                fromdate = "%s/%s/%s" %(month, 1, year)
                todate = "%s/%s/%s" %(month, date, year)
                custom = "Custom"
                siteids = None
            date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
            from_date = date_times_dict.get('fromdate')
            to_date = date_times_dict.get('todate')
            country = request.GET.getlist('country')
            aff_id = request.GET.get('aff_id')
            currencyfilter = request.GET.get("currency", 'euro')
            group_d = str(request.GET.get("groupby", 'brand'))
            country_list = Country.objects.all().values_list('countryname', flat=True).distinct()
            sitefilter = request.GET.getlist("siteid")
            if sitefilter:
                site_ids = site_ids.filter(id__in=sitefilter)
            else:
                sitefilter = [str(i.id) for i in site_ids]
            aff_list = Affiliateinfo.objects.filter(account_manager=acc_manager).order_by('-id')
            if aff_id:
                aff_m_list = aff_list.filter(id=aff_id)
            else:
                aff_m_list = aff_list
            header_list, data_list, currency = advertiser_reports_stats(aff_m_list, from_date, 
                to_date, site_ids, currencyfilter, country)
            data_dict_list = []
            for d in data_list:
                data_dict_list.append({v['title']: d[i] for i, v in enumerate(header_list)})
            context_dict['data_list'] = data_dict_list
            context_dict["header_list"] = header_list

            context_dict['sites_obj_list'] = [{'id':i.id, 'name': i.name } for i in site_ids_list]
            context_dict["aff_list"] = [{'id':i.id, 'username': i.email.replace('vcommsubaff', '')} for i in aff_list]
            context_dict["country_list"] = country_list
            context_dict["currency"] = currency
            # context_dict["pagination_div"] = pagination_div
            # if request.GET.get("format") == "json":
            #     context_dict["status"] = "success"
            #     context_dict['data_list'] = data_list
            #     context_dict["header_list"] = header_list
            #     return HttpResponse(json.dumps(context_dict, indent=4))
            context_dict['domain'] = site_name
            context_dict['partner'] = acc_manager.affiliateprogramme.name
            status_code = st.HTTP_200_OK
        except Exception as e:
            error_log()
            message = "Internal server error, %s" % e
            status = 'Error'
            status_code = st.HTTP_400_BAD_REQUEST
        return Response({'status': status, 'message': message, 'data': context_dict}, status=status_code)



class AffDashboardView(viewsets.ViewSet):
    """
    url: /api/v1/affiliate/brandreport/
    Headers:{
        'Authorization': 'Token db975411fdad07e4c41c1639aecf20b2e87asdhsd5' (User login token)
    }
    data: {
        'bar_data':[[1,2,3],[4,5,6]]
        }
    responce:{status: "Success", message: "Preference Edited successfully"}
    """
    authentication_classes = (SessionAuthentication, AffiliateTokenAuthentication)
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    # filter_backends = (filters.DjangoFilterBackend,filters.SearchFilter,)
    http_method_names = ['get', 'post']

    def list(self, request):
        context_dict = {}
        try:
            status = 'Success'
            message = ''
            acc_manager = None
            affid = request.custom_data.get('affid')
            try:
                obj = Affiliateinfo.objects.filter(id=affid)
                if not obj:
                    status_code = st.HTTP_400_BAD_REQUEST
                    message = affid + " is not in database in respect to any Affiliate."
                    return Response({'status': status, 'message': message}, status=status_code)
                obj = obj[0]
            except:
                logger.error(affid + " is not in database in respect to any Affiliate.")
                status = 'Error'
                status_code = st.HTTP_400_BAD_REQUEST
                message = affid + " is not in database in respect to any Affiliate."
                return Response({'status': status, 'message': message}, status=status_code)
            commission = get_aff_comm(obj)
            site_ids_list = obj.siteid.all().order_by('name')
            net_ids_list = [1]
            custom = str(request.GET.get("customid", 'Custom'))
            fromdate = str(request.GET.get("fromdate"))
            todate = str(request.GET.get("todate"))
            domain = request.GET.get("sitename")
            site_name = 'All Websites'
            site_ids = site_ids_list
            if domain and domain != 'All':
                site_domain_list = domain.split(',')
                site_ids = site_ids_list.filter(domain__in=site_domain_list)
                site_name = 'Multiple sites.'
                if len(domain.split(',')) <2:
                    site_name = site_ids_list.get(domain=site_domain_list[0]).name
            siteids = request.GET.getlist("siteids")
            if request.method == "GET" and fromdate == 'None':
                to_day_date = datetime.now()
                date = to_day_date.day
                year = to_day_date.year
                month = to_day_date.month
                dates_tuple = calendar.monthrange(year, month)
                fromdate = "%s/%s/%s" %(month, 1, year)
                todate = "%s/%s/%s" %(month, date, year)
                custom = "Custom"
                siteids = None
                
            context_dict = {}

            date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
            from_date = date_times_dict.get('fromdate')
            to_date = date_times_dict.get('todate')
            aff_objs = list(Affiliateinfo.objects.filter(superaffiliate=obj))
            aff_objs.append(obj)
            context_dict = get_affiliate_dashboard_stats(aff_objs, commission, from_date, to_date, site_ids)

            context_dict['sites_obj_list'] = [{'id':i.id, 'name': i.name } for i in site_ids_list]

            top_creatives = get_top_creatives(site_ids_list)
            context_dict['top_creatives'] = top_creatives
            header_list = [{'title':'Brand'}, {'title':'Type'}, {'title':'Size'}, 
                        {'title':'Campaign'}, {'title':'BannerID'}, 
                        {'title':'Banner Type'}, {'title':'Link'}]
            data_dict_list = []
            for d in top_creatives:
                data_dict_list.append({v['title']: d[i] for i, v in enumerate(header_list)})
            context_dict['data_list'] = data_dict_list
            context_dict["header_list"] = header_list

            context_dict["partner"] = obj.account_manager.affiliateprogramme.name
            # context_dict["partner_obj"] = obj.account_manager.affiliateprogramme
            
            context_dict['domain'] = site_name
            
            status_code = st.HTTP_200_OK
        except Exception as e:
            error_log()
            message = "Internal server error, %s" % e
            status = 'Error'
            status_code = st.HTTP_400_BAD_REQUEST
        return Response({'status': status, 'message': message, 'data': context_dict}, status=status_code)


class AffBarChart(viewsets.ViewSet):
    """
    url: /api/v1/affiliate/brandreport/
    Headers:{
        'Authorization': 'Token db975411fdad07e4c41c1639aecf20b2e87asdhsd5' (User login token)
    }
    data: {
        'bar_data':[[1,2,3],[4,5,6]]
        }
    responce:{status: "Success", message: "Preference Edited successfully"}
    """
    authentication_classes = (SessionAuthentication, AffiliateTokenAuthentication)
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    # filter_backends = (filters.DjangoFilterBackend,filters.SearchFilter,)
    http_method_names = ['get', 'post']

    def list(self, request):
        context_dict = {}
        try:
            status = 'Success'
            message = ''
            acc_manager = None
            affid = request.custom_data.get('affid')
            try:
                obj = Affiliateinfo.objects.filter(id=affid)
                if not obj:
                    status_code = st.HTTP_400_BAD_REQUEST
                    message = affid + " is not in database in respect to any Affiliate."
                    return Response({'status': status, 'message': message}, status=status_code)
                obj = obj[0]
            except:
                logger.error(affid + " is not in database in respect to any Affiliate.")
                status = 'Error'
                status_code = st.HTTP_400_BAD_REQUEST
                message = affid + " is not in database in respect to any Affiliate."
                return Response({'status': status, 'message': message}, status=status_code)
            commission = get_aff_comm(obj)
            site_ids_list = obj.siteid.all().order_by('name')
            net_ids_list = [1]
            custom = str(request.GET.get("customid", 'Custom'))
            fromdate = str(request.GET.get("fromdate"))
            todate = str(request.GET.get("todate"))
            domain = request.GET.get("sitename")
            currencyfilter = request.GET.get('currency', 'euro')
            site_name = 'All Websites'
            site_ids = site_ids_list
            if domain and domain != 'All':
                site_domain_list = domain.split(',')
                site_ids = site_ids_list.filter(domain__in=site_domain_list)
                site_name = 'Multiple sites.'
                if len(domain.split(',')) <2:
                    site_name = site_ids_list.get(domain=site_domain_list[0]).name
            siteids = request.GET.getlist("siteids")
            if request.method == "GET" and fromdate == 'None':
                to_day_date = datetime.now()
                date = to_day_date.day
                year = to_day_date.year
                month = to_day_date.month
                dates_tuple = calendar.monthrange(year, month)
                fromdate = "%s/%s/%s" %(month, 1, year)
                todate = "%s/%s/%s" %(month, date, year)
                custom = "Custom"
                siteids = None
                
            context_dict = {}

            date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
            from_date = date_times_dict.get('fromdate')
            to_date = date_times_dict.get('todate')
            aff_objs = list(Affiliateinfo.objects.filter(superaffiliate=obj))
            aff_objs.append(obj)
            context_dict_data = get_affiliate_home_stats_changed(aff_objs, [obj], commission, from_date, to_date, site_ids, currencyfilter)
            # get_affiliate_home_stats(aff_objs, [obj], commission, from_date, to_date, site_ids)
            context_dict['device_list'] = ['Web', 'Mobile', 'Tablet', 'Television']
            context_dict['device_report'] = get_device_info_chart(aff_objs, [obj],commission, from_date, to_date, site_ids)
            context_dict['bar_chart'] = {'dates_list': context_dict_data['bar_dates_list'], 
                        'data':context_dict_data['barchart']}
            context_dict['area_chart'] = {'dates_list': context_dict_data['dates_list'],
                                    'data': context_dict_data['area_chart']}
            context_dict['sites_obj_list'] = [{'id':i.id, 'name': i.name } for i in site_ids_list]

            context_dict["partner"] = obj.account_manager.affiliateprogramme.name
            # context_dict["partner_obj"] = obj.account_manager.affiliateprogramme
            
            context_dict['domain'] = site_name
            
            status_code = st.HTTP_200_OK
        except Exception as e:
            error_log()
            message = "Internal server error, %s" % e
            status = 'Error'
            status_code = st.HTTP_400_BAD_REQUEST
        return Response({'status': status, 'message': message, 'data': context_dict}, status=status_code)


class AffBrandReport(viewsets.ViewSet):
    """
    url: /api/v1/affiliate/brandreport/
    Headers:{
        'Authorization': 'Token db975411fdad07e4c41c1639aecf20b2e87asdhsd5' (User login token)
    }
    data: {
        'bar_data':[[1,2,3],[4,5,6]]
        }
    responce:{status: "Success", message: "Preference Edited successfully"}
    """
    authentication_classes = (SessionAuthentication, AffiliateTokenAuthentication)
    renderer_classes = (JSONRenderer, BrowsableAPIRenderer)
    # filter_backends = (filters.DjangoFilterBackend,filters.SearchFilter,)
    http_method_names = ['get', 'post', 'put']

    def list(self, request):
        context_dict = {}
        try:
            status = 'Success'
            message = ''
            acc_manager = None
            affid = request.custom_data.get('affid')
            try:
                obj = Affiliateinfo.objects.filter(id=affid)
                if not obj:
                    status_code = st.HTTP_400_BAD_REQUEST
                    message = affid + " is not in database in respect to any Affiliate."
                    return Response({'status': status, 'message': message}, status=status_code)
                obj = obj[0]
            except:
                logger.error(affid + " is not in database in respect to any Affiliate.")
                status = 'Error'
                status_code = st.HTTP_400_BAD_REQUEST
                message = affid + " is not in database in respect to any Affiliate."
                return Response({'status': status, 'message': message}, status=status_code)
            commission = get_aff_comm(obj)
            site_ids_list = obj.siteid.all().order_by('name')
            net_ids_list = [1]
            custom = str(request.GET.get("customid", 'Custom'))
            fromdate = str(request.GET.get("fromdate"))
            todate = str(request.GET.get("todate"))
            domain = request.GET.get("sitename")
            site_name = 'All Websites'
            site_ids = site_ids_list
            if domain and domain != 'All':
                site_domain_list = domain.split(',')
                site_ids = site_ids_list.filter(domain__in=site_domain_list)
                site_name = 'Multiple sites.'
                if len(domain.split(',')) <2:
                    site_name = site_ids_list.get(domain=site_domain_list[0]).name
            siteids = request.GET.getlist("siteids")
            if request.method == "GET" and fromdate == 'None':
                to_day_date = datetime.now()
                date = to_day_date.day
                year = to_day_date.year
                month = to_day_date.month
                dates_tuple = calendar.monthrange(year, month)
                fromdate = "%s/%s/%s" %(month, 1, year)
                todate = "%s/%s/%s" %(month, date, year)
                custom = "Custom"
                siteids = None
                
            context_dict = {}
            date_times_dict = get_fromdate_todate_for_django_query_filter(custom, fromdate, todate)
            from_date = date_times_dict.get('fromdate')
            to_date = date_times_dict.get('todate')
            aff_objs = list(Affiliateinfo.objects.filter(superaffiliate=obj))
            aff_objs.append(obj)
            # context_dict = get_affiliate_home_stats(aff_objs, commission, from_date, to_date, site_ids)
            # context_dict['device_list'] = ['Web', 'Mobile', 'Tablet', 'Television']
            # context_dict['device_report'] = get_device_info_chart(aff_objs, commission, from_date, to_date, site_ids)

            header_list, data_list, currency_d = activity_report_stats(aff_objs, [obj], commission, from_date, to_date, site_ids)
            data_dict_list = []
            for d in data_list:
                data_dict_list.append({v['title']: d[i] for i, v in enumerate(header_list)})
            context_dict['data_list'] = data_dict_list
            context_dict["header_list"] = header_list
            status_code = st.HTTP_200_OK
        except Exception as e:
            error_log()
            message = "Internal server error, %s" % e
            status = 'Error'
            status_code = st.HTTP_400_BAD_REQUEST
        return Response({'status': status, 'message': message, 'data': context_dict}, status=status_code)

def savestatus(request):
    resp_dict = {}
    try:
        acc_manager = None
        partnerid = request.custom_data.get('partnerid')
        try:
            user = User.objects.get(id=partnerid)
            acc_manager_obj = AccountManager.objects.filter(user=user)
            partuser_obj = []
            if acc_manager_obj:
                acc_manager = acc_manager_obj[0]
            else:
                partuser_obj = PartnerUser.objects.filter(user=user)
                acc_manager = partuser_obj[0].accmanager
            if not acc_manager:
                status_code = st.HTTP_400_BAD_REQUEST
                message = partnerid + " is not in database in respect to any User."
                return Response({'status': status, 'message': message}, status=status_code)
        except:
            logger.error(partnerid + " is not in database in respect to any User.")
            status = 'Error'
            status_code = st.HTTP_400_BAD_REQUEST
            message = partnerid + " is not in database in respect to any User."
            return Response({'status': status, 'message': message}, status=status_code)  
        aff_id = request.GET.get('aff_id')
        status = request.GET.get('status')
        s_aff = Affiliateinfo.objects.get(id=int(aff_id))
        s_aff.status = status
        s_aff.save()
        if status == 'Approved':
            statuschangemail(s_aff, s_aff, 'AFFILIATE_SIGN_UP_APPROVAL')
        else:
            statuschangemail(s_aff, s_aff, 'AFFILIATE_SIGN_UP_REJECTED')
        status = 'Success'
        resp_dict['pending_count'] = Affiliateinfo.objects.filter(
                        account_manager=s_aff.account_manager,
                        status="Pending").count()
        status_code = st.HTTP_400_BAD_REQUEST
        message = 'Successfully Updated.'
    except Exception as e:
        message = str(e)
        status = 'Error'
        status_code = st.HTTP_400_BAD_REQUEST
        return Response({'status': status, 'message': message}, status=status_code) 
    return Response({'status': status, 'message': message, 'data': resp_dict}, status=status_code)