        
from datetime import datetime, timedelta
from django.conf import settings
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse



class AutoLogout:
    def process_request(self, request):
        path = request.path.split("/")
        if path and path[1].lower() in ['static_files','flash','flashbonuschunksinfo',
                                        'favicon.ico']:
            return None
        if hasattr(request, 'session'):
            try:
                if request.session.get('PLAYER_SESSION_FLAG') != "SESSION_REMOVED" and \
                        (datetime.now() - request.session['PLAYER_SESSION_FLAG'] > timedelta( 0, 60 * settings.AUTO_LOGOUT_DELAY, 0)):
                    player_id = request.session.get('id')
                    if player_id:
                        request.session['PLAYER_SESSION_FLAG'] = "SESSION_REMOVED"
                        return HttpResponseRedirect('/playerlogout/')
                elif request.session.get('PLAYER_SESSION_FLAG') != "SESSION_REMOVED":
                    request.session['PLAYER_SESSION_FLAG'] = datetime.now()
            except Exception as e:
                pass
            

    def process_response(self, request, response):
        return response

from django.conf import settings

class MobileTemplatesMiddleware(object):
    """Determines which set of templates to use for a mobile site"""

    def process_request(self, request):
        # sets are used here, you can use other logic if you have an older version of Python
        #MOBILE_SUBDOMAINS = set(['m', 'mobile'])
        #domain = set(request.META.get('HTTP_HOST', '').split('.'))
        MOBILE_SUBDOMAINS = set(['8008'])
        domain = set(request.META.get('HTTP_HOST', '').split(':'))

        if len(MOBILE_SUBDOMAINS & domain):
            settings.TEMPLATE_DIRS = settings.MOBILE_TEMPLATE_DIRS

"""
class LogHttpResponse(object):
    def process_request(self, request):
        from userapp.models import RequestUrlLogs
        rl = RequestUrlLogs.objects.create(starttime=datetime.now())
        if hasattr(request, 'session'):
            request.session['RequestLogID'] = rl.id
        
    
    def process_response(self, request, response):

        from userapp.models import RequestUrlLogs
        rlID = None
        if hasattr(request, 'session'):
            rlID = request.session.get('RequestLogID')
        if rlID:
            path = request.get_full_path()
            size = len(response.content) #(len(response.content)/1024)/1024 #inMBS
            rl_obj = RequestUrlLogs.objects.get(id=rlID)
            rl_obj.contentsize = size
            rl_obj.path = path
            rl_obj.ipaddress = request.META['REMOTE_ADDR']
            rl_obj.playerid = request.session.get('id')
            rl_obj.endtime = datetime.now()
            rl_obj.save()
        return response
"""

import random
def random_digit_challenge():
    ret = u''
    for i in range(6):
        ret += str(random.randint(0,9))
    if len(ret) == 0:
        ret = '872148'
    return ret, ret

from django.db import connections
import datetime
import time
import os

class SqlProfilingMiddleware(object):
    Queries = []

    def process_request(self, request):
        return None
    def process_view(self, request, view_func, view_args, view_kwargs):
        return None
    def process_template_response(self, request, response):
        self._add_sql_queries(request)
        return response
    def process_response(self, request, response):
        self._add_sql_queries(request)
        return response
    def process_exception(self, request, exception):
        return None

    def _add_sql_queries(self, request):
        for connection_name in connections:
            connection = connections[connection_name]
            count = len(connection.queries)
            for q in connection.queries:
                q["count"] = count
                count = count -1
                SqlProfilingMiddleware.Queries.insert(0, q)
            # add request info as a separator
            SqlProfilingMiddleware.Queries.insert(0, {"time": datetime.datetime.now(), "path" : request.get_full_path() ,"total":len(connection.queries),
                                                      "database":connection_name})

from apiapp.models import *
class AuthorizerMiddleware:

    def process_request(self, request):
        ddata = {}
        # Check for authentication
        if 'HTTP_AUTHORIZATION' in request.META:
            #key = request.META.get('HTTP_AUTHORIZATION').split(' ')[1]
            try:
                key = request.META.get('HTTP_AUTHORIZATION').split(' ')[1]
                user = CustomToken.objects.filter(key=key)
                if user:
                    ddata['partnerid'] = user[0].user.id
                else:
                    affiliate = CustomTokenAffiliate.objects.get(key=key).affiliate
                    ddata['affid'] = affiliate.id
            except Exception as e:
                print "WERRRRR", e
        request.custom_data = ddata
        print "JAJAJAJAJ",request.custom_data
        return None


import threading
import uuid
class CurrentUserMiddleware:
    thread_local = threading.local()
    #def process_request(self, get_response):
    #    # import pdb;pdb.set_trace()
    #    self.get_response = get_response
    #    self.thread_local = threading.local()
    #    # One-time configuration and initialization.

    def process_response(self, request, response):
        try:
            # import pdb;pdb.set_trace()
            # Code to be executed for each request before
            # the view (and later middleware) are called.
            self.thread_local.current_user = request.user.username
            self.thread_local.aff_id = request.session.get('affid')
            self.thread_local.url = request.path
            self.thread_local.x_trace_token = request.META.get('X-Trace-Token', str(uuid.uuid4()))
            # Code to be executed for each request/response after
            # the view is called.
            return response
        except Exception as e:
            print('excpetion in middleware.py ',e)
            return response