ALTER TABLE userapp_playerbalancesupdatesstack ADD COLUMN depositamount DECIMAL(10,2) default null, 
ADD COLUMN withdrawableamount DECIMAL(10,2)  default null;

ALTER TABLE userapp_playerinfo ADD COLUMN depositamount DECIMAL(10,2) DEFAULT 0, 
ADD COLUMN withdrawableamount DECIMAL(10,2) default 0;

UPDATE userapp_playerinfo SET depositamount=0, withdrawableamount=realchips WHERE deposits>0;


ALTER TABLE userapp_withdrawalinfo ADD COLUMN is_change TINYINT;
ALTER TABLE `userapp_playerwithdrawalinfo` MODIFY COLUMN country INT(11);
ALTER TABLE userapp_playerwithdrawalinfo ADD FOREIGN KEY playerwithdrawalinfo_countryid(country) REFERENCES userapp_country(id);
ALTER TABLE userapp_limit ADD COLUMN no_of_free_withdrawals_per_month INT(11), ADD COLUMN withdrawal_fee DECIMAL(10,2); 

ALTER TABLE userapp_limit ADD COLUMN withdrawal_percent INT(11) default 100; 

ALTER TABLE userapp_stream ADD COLUMN aienable TINYINT;

CREATE TABLE `userapp_eventtype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eventname` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `eventname` (`eventname`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `userapp_smscount` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `timestamp` DATETIME NOT NULL,
  `eventtype_id` INT(11) DEFAULT NULL,
  `playerid_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userapp__playerid_id_2904da65_fk_userapp_playerandsitemapping_id` (`playerid_id`),
  KEY `userapp_smscount_eventtype_id_5851471a_fk_userapp_eventtype_id` (`eventtype_id`),
  CONSTRAINT `userapp_smscount_eventtype_id_5851471a_fk_userapp_eventtype_id` FOREIGN KEY (`eventtype_id`) REFERENCES `userapp_eventtype` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `userapp__playerid_id_2904da65_fk_userapp_playerandsitemapping_id` FOREIGN KEY (`playerid_id`) REFERENCES `userapp_playerandsitemapping` (`id`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `userapp_mailcount` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `timestamp` DATETIME NOT NULL,
  `eventtype_id` INT(11) DEFAULT NULL,
  `playerid_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userapp_m_playerid_id_3350a12_fk_userapp_playerandsitemapping_id` (`playerid_id`),
  KEY `userapp_mailcount_eventtype_id_1e4a0451_fk_userapp_eventtype_id` (`eventtype_id`),
  CONSTRAINT `userapp_mailcount_eventtype_id_1e4a0451_fk_userapp_eventtype_id` FOREIGN KEY (`eventtype_id`) REFERENCES `userapp_eventtype` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `userapp_m_playerid_id_3350a12_fk_userapp_playerandsitemapping_id` FOREIGN KEY (`playerid_id`) REFERENCES `userapp_playerandsitemapping` (`id`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

INSERT INTO userapp_eventtype(id, eventname) VALUES(1,"REGISTRATION");
INSERT INTO userapp_eventtype(id, eventname) VALUES(2,"VERIFICATION");
INSERT INTO userapp_eventtype(id, eventname) VALUES(3,"SIGNUP_BONUS_CREDITS");
INSERT INTO userapp_eventtype(id, eventname) VALUES(4,"ON_DEPOSIT_BONUS");
INSERT INTO userapp_eventtype(id, eventname) VALUES(5,"REFER_A_FRIEND_BONUS");
INSERT INTO userapp_eventtype(id, eventname) VALUES(6,"MANUAL_BONUS");
INSERT INTO userapp_eventtype(id, eventname) VALUES(7,"FOR_RELOAD");
INSERT INTO userapp_eventtype(id, eventname) VALUES(8,"FIRST_DEPOSIT");
INSERT INTO userapp_eventtype(id, eventname) VALUES(9,"SUCCESSFUL_DEPOSIT");
INSERT INTO userapp_eventtype(id, eventname) VALUES(10,"FAILED_DEPOSIT");
INSERT INTO userapp_eventtype(id, eventname) VALUES(11,"SUCCESSFUL_WITHDRAWLS");
INSERT INTO userapp_eventtype(id, eventname) VALUES(12,"UNSUCCESSFUL_WITHDRAWLS");
INSERT INTO userapp_eventtype(id, eventname) VALUES(13,"REVERSAL_WITHDRAWLS");
INSERT INTO userapp_eventtype(id, eventname) VALUES(14,"EMAIL_CHANGE");
INSERT INTO userapp_eventtype(id, eventname) VALUES(15,"PASSWORD_RESET");
INSERT INTO userapp_eventtype(id, eventname) VALUES(16,"BONUS_PLAYER_POPUP");
INSERT INTO userapp_eventtype(id, eventname) VALUES(17,"SEND_OTP");
INSERT INTO userapp_eventtype(id, eventname) VALUES(18,"AFFILIATE_PASSWORD_RESET");

ALTER TABLE userapp_order MODIFY  COLUMN orderstate ENUM('CREATED','PROGRESS','FINISHED','FAILED','REVERSED','FLOWBACK', 'INPROCESS') 
DEFAULT NULL;

ALTER TABLE userapp_playerrakelog ADD COLUMN amouttype VARCHAR(3) DEFAULT NULL, ADD COLUMN gametype VARCHAR(25) DEFAULT NULL,
ADD COLUMN tournament_id INT(11) DEFAULT NULL;
ALTER TABLE tourney_tournament ENGINE = INNODB;
ALTER TABLE userapp_playerrakelog ADD FOREIGN KEY tourney_tournament_id(tournament_id) REFERENCES `tourney_tournament`(id);

ALTER TABLE userapp_rakelogs ADD COLUMN  gametype VARCHAR(25) DEFAULT NULL, ADD COLUMN tournament_id INT(11) DEFAULT NULL;
ALTER TABLE userapp_rakelogs ADD FOREIGN KEY userapp_rakelogs_tournament_id(tournament_id) REFERENCES `tourney_tournament`(id);


ALTER TABLE `tourney_tournament` MODIFY COLUMN tournament_chips_type ENUM('CASH_TOURNEY',
'FREE_TOURNEY','FUNCHIPS_TOURNEY', 'LOYALTYPOINTS_TOURNEY','SOCIALPOINTS_TOURNEY','VIP_CODES_TOURNEY');

UPDATE tourney_tournament SET tournament_chips_type="FUNCHIPS_TOURNEY" WHERE (fromchips="FUNCHIPS" AND tochips="FUNCHIPS");
update tourney_tournament set tournament_chips_type="FREE_TOURNEY" where (fromchips="CASH" and tochips="CASH" and tournament_chips=0);
update tourney_tournament set tournament_chips_type="CASH_TOURNEY" where (fromchips="CASH" and tochips="CASH" and tournament_chips > 0);
update tourney_tournament set tournament_chips_type="LOYALTYPOINTS_TOURNEY" where (fromchips="LOYALTYPOINTS" and tochips="CASH" and tournament_chips>0);


ALTER TABLE userapp_withdrawalinfo ADD COLUMN is_change TINYINT;
ALTER TABLE userapp_playerrakelog ADD INDEX (TIMESTAMP);



ALTER TABLE userapp_order MODIFY  COLUMN modeofdeposit ENUM("CREDITCARD","DEBITCARD",
    "NETBANKING",
    "MANUAL_ADMIN",
    "GHARPAY",
    "AIRTEL_MONEY",
    "CHEQUE",
    "NEFT","PAYTM_CASH","I-CASHCARD","IMPS","ITZ-CASHCARD","MOBIKWIK",
    "PAYMATE","OXICASH","PAYCASHCARD","PAYUBIZ", "OLAMONEY") DEFAULT NULL;

ALTER TABLE userapp_walletgateway MODIFY  COLUMN wallettype ENUM('PAYTM_CASH','MOBIKWIK','PAYUBIZ',
'OLAMONEY') DEFAULT NULL;

========================

CREATE TABLE userapp_favorite_game_settings(
  id INT PRIMARY KEY AUTO_INCREMENT,
  bet DECIMAL(10,2),
  table_type VARCHAR(12),
  chip_type VARCHAR(30),
  max_players INT,
  UNIQUE KEY key_1 (bet, table_type, chip_type, max_players)
);

CREATE TABLE `userapp_favorite_game` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playerid_id` int(11) DEFAULT NULL,
  `gs_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userapp_favorite_game_user_id` (`playerid_id`),
  KEY `userapp_favorite_game_gs_id` (`gs_id`),
  CONSTRAINT `userapp_favorite_game_gs_id` FOREIGN KEY (`gs_id`) REFERENCES `userapp_favorite_game_settings` (`id`),
  CONSTRAINT `userapp_favorite_game_user_id` FOREIGN KEY (`playerid_id`) REFERENCES `userapp_playerinfo` (`id`),
  UNIQUE KEY key_1 (playerid_id, gs_id)
);


CREATE TABLE `userapp_engine_management` (
  `static_player_count` int(11) DEFAULT NULL,
  `multiply_player_count` int(11) DEFAULT NULL,
  `static_table_count` int(11) DEFAULT NULL,
  `static_tourney_count` int(11) DEFAULT NULL,
  `static_tourney_table_count` int(11) DEFAULT NULL
)

########################################################################

user device information related changes

CREATE TABLE `userapp_device_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_type` varchar(10) DEFAULT NULL,
  `operating_system` varchar(15) DEFAULT NULL,
  `client_type` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `userapp_login_device_info_uniquekey` (`device_type`,`operating_system`,`client_type`)
);

INSERT INTO `userapp_device_info` (`id`, `device_type`, `operating_system`, `client_type`) VALUES('1','Web','OS','WebUI');
INSERT INTO `userapp_device_info` (`id`, `device_type`, `operating_system`, `client_type`) VALUES('2','Mobile','IOS','IOS');
INSERT INTO `userapp_device_info` (`id`, `device_type`, `operating_system`, `client_type`) VALUES('3','Mobile','Android','Android');
INSERT INTO `userapp_device_info` (`id`, `device_type`, `operating_system`, `client_type`) VALUES('4','Mobile','OS','WebUI');
INSERT INTO `userapp_device_info` (`id`, `device_type`, `operating_system`, `client_type`) VALUES('5','Tablet','OS','WebUI');
INSERT INTO `userapp_device_info` (`id`, `device_type`, `operating_system`, `client_type`) VALUES('6','Tablet','Android','Android');
INSERT INTO `userapp_device_info` (`id`, `device_type`, `operating_system`, `client_type`) VALUES('7','Tablet','IOS','IOS');
INSERT INTO `userapp_device_info` (`id`, `device_type`, `operating_system`, `client_type`) VALUES('8','TV','OS','TV');


ALTER TABLE userapp_reconcilelogs add column bet decimal(10,2) default null, ADD COLUMN device_info_id INT(11) DEFAULT NULL,
ADD FOREIGN KEY fk_device_info_id(device_info_id) REFERENCES userapp_device_info(id);

ALTER TABLE userapp_orderdevicedetails ADD COLUMN device_info_id INT(11) DEFAULT NULL, 
ADD FOREIGN KEY fk_device_info_id(device_info_id) REFERENCES userapp_device_info(id);


ALTER TABLE userapp_uniquelogindevicedetails ADD COLUMN device_info_id INT(11) DEFAULT NULL;
ADD FOREIGN KEY fk_device_info_id(device_info_id) REFERENCES userapp_device_info(id);

ALTER TABLE userapp_loginon ADD COLUMN device_info_id INT(11) DEFAULT NULL;
ADD FOREIGN KEY fk_device_info_id(device_info_id) REFERENCES userapp_device_info(id);

ALTER TABLE userapp_playerinfo1 ADD COLUMN device_info_id INT(11) DEFAULT NULL;
ADD FOREIGN KEY fk_device_info_id(device_info_id) REFERENCES userapp_device_info(id);

UPDATE userapp_playerinfo1 SET device_info_id=1 WHERE channel="Web" AND subchannel="WebUI";
UPDATE userapp_playerinfo1 SET device_info_id=2 WHERE channel="Mobile" AND subchannel="IOS";
UPDATE userapp_playerinfo1 SET device_info_id=3 WHERE channel="Mobile" AND subchannel="Android";
UPDATE userapp_playerinfo1 SET device_info_id=4 WHERE channel="Mobile" AND subchannel="WebUI";
UPDATE userapp_playerinfo1 SET device_info_id=5 WHERE channel="Tablet" AND subchannel="WebUI";
UPDATE userapp_playerinfo1 SET device_info_id=6 WHERE channel="Tablet" AND subchannel="Android";
UPDATE userapp_playerinfo1 SET device_info_id=7 WHERE channel="Tablet" AND subchannel="IOS";

UPDATE userapp_loginon SET device_info_id=1 WHERE channel="Web" AND subchannel="WebUI";
UPDATE userapp_loginon SET device_info_id=2 WHERE channel="Mobile" AND subchannel="IOS";
UPDATE userapp_loginon SET device_info_id=3 WHERE channel="Mobile" AND subchannel="Android";
UPDATE userapp_loginon SET device_info_id=4 WHERE channel="Mobile" AND subchannel="WebUI";
UPDATE userapp_loginon SET device_info_id=5 WHERE channel="Tablet" AND subchannel="WebUI";
UPDATE userapp_loginon SET device_info_id=6 WHERE channel="Tablet" AND subchannel="Android";
UPDATE userapp_loginon SET device_info_id=7 WHERE channel="Tablet" AND subchannel="IOS";


UPDATE userapp_orderdevicedetails SET device_info_id=1 WHERE channel="Web" AND subchannel="WebUI";
UPDATE userapp_orderdevicedetails SET device_info_id=2 WHERE channel="Mobile" AND subchannel="IOS";
UPDATE userapp_orderdevicedetails SET device_info_id=3 WHERE channel="Mobile" AND subchannel="Android";
UPDATE userapp_orderdevicedetails SET device_info_id=4 WHERE channel="Mobile" AND subchannel="WebUI";
UPDATE userapp_orderdevicedetails SET device_info_id=5 WHERE channel="Tablet" AND subchannel="WebUI";
UPDATE userapp_orderdevicedetails SET device_info_id=6 WHERE channel="Tablet" AND subchannel="Android";
UPDATE userapp_orderdevicedetails SET device_info_id=7 WHERE channel="Tablet" AND subchannel="IOS";


alter table userapp_playerrakelog
add column device_info_id int;

alter table userapp_playerrakelog
add CONSTRAINT `device_info_id_to_deviceinfo` FOREIGN KEY (`device_info_id`) REFERENCES `userapp_device_info` (`id`),


##########################################################################

AI and Smart correction log realted changes

CREATE TABLE `userapp_ai_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playerid_id` int(11) NOT NULL,
  `username` varchar(30) DEFAULT NULL,
  `table_id` int(11) DEFAULT NULL,
  `table_type` varchar(15) DEFAULT NULL,
  `ai_type` varchar(5) DEFAULT NULL,
  `agree` varchar(5) DEFAULT NULL,
  `chip_type` varchar(10) DEFAULT NULL,
  `time_stamp` datetime DEFAULT NULL,
  `game_id` int(11) DEFAULT NULL,
  `device_info_id` int(11) default NULL,
  `bet` Decimal(10, 2) default NULL,
   PRIMARY KEY (`id`),
   KEY `refer_table_tableid` (`table_id`),
   KEY `refer_playerinfo_table` (`playerid_id`),
   KEY `refer_game_gameid` (`game_id`),
   KEY `refer_device_info_table` (`device_info_id`),
   CONSTRAINT `refer_playerinfo_table` FOREIGN KEY (`playerid_id`) REFERENCES `userapp_playerinfo` (`id`),
   CONSTRAINT `refer_device_info_table` FOREIGN KEY (`device_info_id`) REFERENCES `userapp_device_info` (`id`) 
)
;

ALTER TABLE userapp_reconcilelogs ADD COLUMN bet DECIMAL(10,2) DEFAULT null, add column device_info_id int(11) default 1;
ALTER TABLE userapp_playerrakelog DROP FOREIGN KEY fkgame_id_refs_id_f111a356; 
 ALTER TABLE userapp_playerrakelog ADD INDEX playerrake_log_mcindex (TIMESTAMP, amouttype, gametype);
 ALTER TABLE userapp_playerrakelog ADD INDEX gameid_playerrake_log_index (fkgame_id);
 ALTER TABLE userapp_game ADD INDEX userapp_game_ts_at_gt (TIMESTAMP, tableid, gameid);
 
 ALTER TABLE userapp_loginon ADD COLUMN brand VARCHAR(30) DEFAULT NULL, ADD COLUMN device_family VARCHAR(30) DEFAULT NULL,
ADD COLUMN os_family VARCHAR(30) DEFAULT NULL, ADD COLUMN fresh_login TINYINT DEFAULT 0;

ALTER TABLE userapp_loginon DROP COLUMN channel, DROP COLUMN subchannel;


CREATE TABLE `userapp_loyaltystats` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `playerid_id` INT(11) NOT NULL,
  `tableid` INT(11) DEFAULT NULL,
  `gameid` INT(11) DEFAULT NULL,
  `tournament_id` INT(11) DEFAULT  NULL,
  `gametype` VARCHAR(15) DEFAULT NULL,
  `update_amount` DECIMAL(10,2) DEFAULT 0,
  `updatetype` VARCHAR(2) DEFAULT NULL,
  `timestamp` DATETIME DEFAULT NULL,
  `device_info_id` int(11) default NULL,
  PRIMARY KEY (`id`),
   KEY `refer_table_tableid` (`table_id`),
    KEY `refer_game_gameid` (`game_id`),
  KEY `refer_playerinfo_table` (`playerid_id`),
  CONSTRAINT `refer_playerinfo_table` FOREIGN KEY (`playerid_id`) REFERENCES `userapp_playerinfo` (`id`),
  KEY `refer_tournament_table` (`tournament_id`),
  CONSTRAINT `refer_tournament_table` FOREIGN KEY (`tournament_id`) REFERENCES `tourney_tournament` (`id`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

 ALTER TABLE userapp_loyaltystats ADD INDEX userapp_loyaltystats_ts_at_gt (TIMESTAMP, tableid, gameid);
  ALTER TABLE userapp_ai_logs ADD INDEX userapp_ai_logs_ts_at_gt (time_stamp,
  tableid, gameid);