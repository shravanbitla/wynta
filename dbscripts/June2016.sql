

ALTER TABLE userapp_order MODIFY  COLUMN orderstate 
ENUM('CREATED','PROGRESS','FINISHED','FAILED','REVERSED','FLOWBACK', 'INPROCESS', 'DECLINED') ;

ALTER TABLE userapp_order MODIFY  COLUMN modeofdeposit ENUM("CREDITCARD","DEBITCARD",
    "NETBANKING",
    "MANUAL_ADMIN",
    "GHARPAY",
    "AIRTEL_MONEY",
    "CHEQUE",
    "NEFT","PAYTM_CASH","I-CASHCARD","IMPS","ITZ-CASHCARD","MOBIKWIK",
    "PAYMATE","OXICASH","PAYCASHCARD","PAYUBIZ", "OLAMONEY", "COD") DEFAULT NULL;
    
UPDATE userapp_order SET orderstate="DECLINED" WHERE operator_id IS NOT NULL AND orderstate="REVERSED";



CREATE TABLE `userapp_smssenderid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) DEFAULT NULL,
  `sendername` varchar(15) DEFAULT NULL,
  `accounttype` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `refer_django_site_table` FOREIGN KEY (`site_id`) REFERENCES `django_site` (`id`) 
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

ALTER TABLE userapp_smscount ADD COLUMN smssenderid_id INT(11) DEFAULT NULL,
ADD FOREIGN KEY kf_userapp_smssenderid(smssenderid_id) REFERENCES userapp_smssenderid(id);

ALTER TABLE userapp_mailcount ADD COLUMN accounttype varchar(2) DEFAULT null;

===================================================================================
CREATE TABLE `userapp_promotionsapilogs` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `siteid_id` INT(11) DEFAULT NULL,
  `timestamp` DATETIME NOT NULL,
  `response` LONGTEXT,
  `status` VARCHAR(3) DEFAULT NULL,
  `groupid` INT(11) DEFAULT NULL,
  `groupname` VARCHAR(35) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `refer_django_site_table` (`siteid_id`),
  CONSTRAINT `refer_django_site_table` FOREIGN KEY (`siteid_id`) REFERENCES `django_site` (`id`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


