use rummy;
ALTER TABLE userapp_playerinfo ADD is_in_house tinyint(1) DEFAULT 0,
Add column is_home_player tinyint(1) default 0, Add column is_test_player tinyint(1) default 0,
Add column is_ tinyint(1) default 0, Add column is_test tinyint(1) default 0;


ALTER TABLE `userapp_playerinfo` ADD kycverified varchar(1) DEFAULT 'N';

ALTER TABLE `userapp_playerinfo` ADD first_deposit_date DEFAULT NULL;
ALTER TABLE `userapp_limit` ADD kyc_check tinyint(1) default 0;
ALTER TABLE `userapp_playerinfo1` ADD preference_updated datetime DEFAULT NULL;


update userapp_playerinfo p
inner join userapp_order o on o.playerid_id=p.id
set p.first_deposit_date=o.timestamp
where o.ordertype='BANKING' AND o.direction='DEPOSIT'
AND o.orderstate='FINISHED' AND o.ordertype != 'REAL_ADJUSTMENT';
