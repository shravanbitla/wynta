ALTER TABLE userapp_cashcardgateway
ADD COLUMN siteid_id INT(11),
ADD FOREIGN KEY cashcard_siteid_id(siteid_id) REFERENCES django_site(id) ON DELETE CASCADE;

ALTER TABLE userapp_creditcardgateway
ADD COLUMN siteid_id INT(11),
ADD FOREIGN KEY cashcard_siteid_id(siteid_id) REFERENCES django_site(id) ON DELETE CASCADE;

ALTER TABLE userapp_debitcardgateway
ADD COLUMN siteid_id INT(11),
ADD FOREIGN KEY cashcard_siteid_id(siteid_id) REFERENCES django_site(id) ON DELETE CASCADE;

ALTER TABLE userapp_mobilepaymentgateway
ADD COLUMN siteid_id INT(11),
ADD FOREIGN KEY cashcard_siteid_id(siteid_id) REFERENCES django_site(id) ON DELETE CASCADE;

ALTER TABLE userapp_walletgateway
ADD COLUMN siteid_id INT(11),
ADD FOREIGN KEY cashcard_siteid_id(siteid_id) REFERENCES django_site(id) ON DELETE CASCADE;

ALTER TABLE userapp_gatewayforbank
ADD COLUMN siteid_id INT(11),
ADD FOREIGN KEY cashcard_siteid_id(siteid_id) REFERENCES django_site(id) ON DELETE CASCADE;

UPDATE userapp_cashcardgateway SET siteid_id=1;
UPDATE userapp_creditcardgateway SET siteid_id=1;
UPDATE userapp_debitcardgateway SET siteid_id=1;
UPDATE userapp_mobilepaymentgateway SET siteid_id=1;
UPDATE userapp_walletgateway SET siteid_id=1;
UPDATE userapp_gatewayforbank SET siteid_id=1;


ALTER TABLE userapp_order
ADD COLUMN gateway_id INT(11),
ADD FOREIGN KEY order_gateway_id_xcvbgert989(gateway_id) REFERENCES userapp_gateways(id) ON DELETE CASCADE;


UPDATE userapp_order SET gateway_id=1  WHERE COMMENT LIKE "%Transaction is Successful (Tech Process)%";
UPDATE userapp_order SET gateway_id=3  WHERE COMMENT LIKE "%Transaction is Successful (CCAvenue)%";
UPDATE userapp_order SET gateway_id=4  WHERE COMMENT LIKE "%Transaction is Successful (Paytm)%";
UPDATE userapp_order SET gateway_id=5  WHERE COMMENT LIKE "%Transaction is Successful (PayUbiz)%";


ALTER TABLE userapp_debitcardgateway MODIFY  COLUMN cardtype ENUM("VISA","MASTERCARD","MAESTRO","RUPAY") DEFAULT NULL;

ALTER TABLE userapp_creditcardgateway MODIFY  COLUMN cardtype ENUM("VISA","MASTERCARD","MAESTRO","RUPAY") DEFAULT NULL;


INSERT INTO userapp_creditcardgateway(cardtype, gateway_id, , priority, opertator_id, STATUS, siteid_id) 
				     VALUES('VISA', 1, 10, 40, 1, 2);

INSERT INTO userapp_debitcardgateway(cardtype, gateway_id, , priority, opertator_id, STATUS, siteid_id)
				     VALUES('VISA', 1, 10, 40,1, 2);
				     
INSERT INTO userapp_debitcardgateway(cardtype, gateway_id, , priority, opertator_id, STATUS, siteid_id)
				     VALUES('MASTERO', 1, 10, 40,1,2);
				     
INSERT INTO userapp_debitcardgateway(cardtype, gateway_id, , priority, opertator_id, STATUS, siteid_id)
				    VALUES('RUPAY', 3, 10, 40, 1, 2);
				    
INSERT INTO userapp_walletgateway(wallettype, gateway_id, priority, STATUS, operator_id, siteid_id) 
					VALUES('PAYUBIZ', 5,10,1,40, 2);

INSERT INTO userapp_walletgateway(wallettype, gateway_id, priority, STATUS, operator_id, siteid_id) 
					VALUES('PAYTM_CASH',4,10,1,40,2);

INSERT INTO userapp_walletgateway(wallettype, gateway_id, priority, STATUS, operator_id, siteid_id) 
					VALUES('MOBIKWIK', 3,10,1,40,2);

CREATE TABLE `userapp_uniquelogindevicedetails` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `loginonid_id` INT(11) NOT NULL,
  `channel` VARCHAR(10) NOT NULL,
  `subchannel` VARCHAR(20) DEFAULT NULL,
  `brand` VARCHAR(30) DEFAULT NULL,
  `device_family` VARCHAR(30) DEFAULT NULL,
  `os_family` VARCHAR(30) DEFAULT NULL,
  `browser_family` VARCHAR(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userapp_uniquelogind_loginonid_id_274f1787_fk_userapp_loginon_id` (`loginonid_id`),
  KEY `userapp_uniquelogindevicedetails_c485d2ed` (`channel`),
  KEY `userapp_uniquelogindevicedetails_77bb01ba` (`subchannel`),
  CONSTRAINT `userapp_uniquelogind_loginonid_id_274f1787_fk_userapp_loginon_id` FOREIGN KEY (`loginonid_id`) REFERENCES `userapp_loginon` (`id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

CREATE TABLE `userapp_orderdevicedetails` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `orderid_id` INT(11) NOT NULL,
  `channel` VARCHAR(10) NOT NULL,
  `subchannel` VARCHAR(20) DEFAULT NULL,
  `brand` VARCHAR(30) DEFAULT NULL,
  `device_family` VARCHAR(30) DEFAULT NULL,
  `os_family` VARCHAR(30) DEFAULT NULL,
  `browser_family` VARCHAR(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userapp_orderdevicedet_orderid_id_386ffbd3_fk_userapp_loginon_id` (`orderid_id`),
  KEY `userapp_orderdevicedetails_c485d2ed` (`channel`),
  KEY `userapp_orderdevicedetails_77bb01ba` (`subchannel`),
  CONSTRAINT `userapp_orderdevicedet_orderid_id_386ffbd3_fk_userapp_loginon_id` FOREIGN KEY (`orderid_id`) REFERENCES `userapp_loginon` (`id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

			
ALTER TABLE userapp_loginon ADD COLUMN channel VARCHAR(10), ADD INDEX (channel);
ALTER TABLE userapp_loginon ADD COLUMN subchannel VARCHAR(20);

ALTER TABLE userapp_playerinfo1 ADD COLUMN channel VARCHAR(10), ADD INDEX (channel);
ALTER TABLE userapp_playerinfo1 ADD COLUMN subchannel VARCHAR(20);

INSERT INTO userapp_uniquelogindevicedetails(loginonid_id, channel, subchannel)
SELECT id, "Web","WebUI" FROM userapp_loginon WHERE loginok=1 AND uniquelogin=1 AND device_type IS NULL order by id asc;

INSERT INTO userapp_uniquelogindevicedetails(loginonid_id, channel, subchannel)
SELECT id, "Web","WebUI" FROM userapp_loginon WHERE loginok=1 AND uniquelogin=1 AND device_type="WEB-PC" order by id asc;


INSERT INTO userapp_uniquelogindevicedetails(loginonid_id, channel, subchannel)
SELECT id, "Mobile","WebUI" FROM userapp_loginon WHERE loginok=1 AND uniquelogin=1 AND device_type="MOBILE" order by id asc;

INSERT INTO userapp_uniquelogindevicedetails(loginonid_id, channel, subchannel)
SELECT id, "Tablet","WebUI" FROM userapp_loginon WHERE loginok=1 AND uniquelogin=1 AND device_type="TABLET" order by id asc;


INSERT INTO userapp_playerinfo1(playerid_id, preference_email, preference_sms,preference_phone,channel,subchannel)
SELECT id, 1,1,1,"Web", "WebUI" FROM userapp_playerinfo WHERE app_type="WEBSITE" order by id asc;

INSERT INTO userapp_playerinfo1(playerid_id, preference_email, preference_sms,preference_phone,channel,subchannel)
SELECT id, 1,1,1,"Tablet","IOS" FROM userapp_playerinfo WHERE app_type="MOBILE_APP" order by id asc;

INSERT INTO userapp_orderdevicedetails(orderid_id, channel, subchannel)
SELECT id, "Web","WebUI" FROM userapp_order;

UPDATE userapp_loginon SET channel="Web",subchannel="WebUI" WHERE device_type is NULL ;
UPDATE userapp_loginon SET channel="Web",subchannel="WebUI" WHERE device_type="WEB-PC";
UPDATE userapp_loginon SET channel="Mobile",subchannel="WebUI" WHERE device_type="MOBILE";
UPDATE userapp_loginon SET channel="Tablet",subchannel="WebUI" WHERE device_type="TABLET";
UPDATE userapp_loginon SET device_type="WEB" WHERE device_type="WEB-PC";

INSERT INTO userapp_gatewayforbank(bank_id, gateway_id, bankcode, priority, active , siteid_id)
SELECT bank_id, gateway_id, bankcode, priority, active , 2 FROM userapp_gatewayforbank; ORDER BY bank_id ASC;


UPDATE userapp_gatewayforbank SET gateway_id=3 WHERE bank_id IN ("12","30","29","28","27","26","25","24", "40","17","45","44",
"8","36","7","1","3","2") AND siteid_id=2;

-- ADD ICICI BANK 102 HDFC -103-12


