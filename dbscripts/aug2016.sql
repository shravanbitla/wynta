ALTER TABLE userapp_eventtype ADD COLUMN displayname VARCHAR(30) DEFAULT NULL;
ALTER TABLE userapp_playerinfo ADD COLUMN otp_to_mail TINYINT DEFAULT 0;
UPDATE userapp_eventtype SET displayname='Registration' WHERE id=1;
UPDATE userapp_eventtype SET displayname='Verification' WHERE id=2;
UPDATE userapp_eventtype SET displayname='Signup Bonus Credits' WHERE id=3;
UPDATE userapp_eventtype SET displayname='Deposit Bonus' WHERE id=4;
UPDATE userapp_eventtype SET displayname='Refer A Friend Bonus' WHERE id=5;
UPDATE userapp_eventtype SET displayname='Manual Bonus' WHERE id=6;
UPDATE userapp_eventtype SET displayname='For Reload' WHERE id=7;
UPDATE userapp_eventtype SET displayname='First Deposit' WHERE id=8;
UPDATE userapp_eventtype SET displayname='Successfull Deposit' WHERE id=9;
UPDATE userapp_eventtype SET displayname='Failed Deposit' WHERE id=10;
UPDATE userapp_eventtype SET displayname='Successful Withdrawal' WHERE id=11;
UPDATE userapp_eventtype SET displayname='Unsuccessful Withdrawal' WHERE id=12;
UPDATE userapp_eventtype SET displayname='Reversal Withdrawal' WHERE id=13;
UPDATE userapp_eventtype SET displayname='Email Change' WHERE id=14;
UPDATE userapp_eventtype SET displayname='Password Reset' WHERE id=15;
UPDATE userapp_eventtype SET displayname='Bonus Player Popup' WHERE id=16;
UPDATE userapp_eventtype SET displayname='One Time Password' WHERE id=17;
UPDATE userapp_eventtype SET displayname='Affiliate Password Reset' WHERE id=18;
UPDATE userapp_eventtype SET displayname='eGCS Admin' WHERE id=19;
UPDATE userapp_eventtype SET displayname='Promotions' WHERE id=20;


ALTER TABLE userapp_engine_management ADD COLUMN withdrawal_percent int(11) DEFAULT 0;
###########################################################

ALTER TABLE userapp_playerinfo ADD COLUMN otp_to_mail TINYINT DEFAULT 0;

ALTER TABLE userapp_playerrakelog ADD COLUMN depositable TINYINT DEFAULT 0;

CREATE TABLE `userapp_promotionsapilogs` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `siteid_id` INT(11) DEFAULT NULL,
  `timestamp` DATETIME NOT NULL,
  `response` LONGTEXT,
  `status` VARCHAR(3) DEFAULT NULL,
  `groupid` INT(11) DEFAULT NULL,
  `groupname` VARCHAR(35) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `refer_django_site_table` (`siteid_id`),
  CONSTRAINT `refer_django_site_table` FOREIGN KEY (`siteid_id`) REFERENCES `django_site` (`id`)
) AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


ALTER TABLE userapp_playerinfo1 ADD COLUMN campaign_reg_code varchar(50) DEFAULT NULL;

ALTER TABLE userapp_order ADD INDEX order_ts_key(timestamp);
ALTER TABLE userapp_mailcount ADD INDEX mailcount_ts_key(timestamp);
ALTER TABLE userapp_smscount ADD INDEX smscount_ts_key(timestamp);
ALTER TABLE userapp_bonuslogs ADD INDEX bonuslogs_ts_key(timestamp);
ALTER TABLE userapp_bonuschunks ADD INDEX bchunks_ts_key(activation_date_time);
ALTER TABLE userapp_playerinfo ADD INDEX player_mobile_key(mobile);
ALTER TABLE userapp_playerinfo ADD INDEX player_regdate_key(registeredon);
ALTER TABLE userapp_playerrakelog ADD INDEX playerrake_amount_type(amonttype);

##TGP,FGP,GPRF,GPRC
 


