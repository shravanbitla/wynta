ALTER TABLE ADD total_autoextratime int(11) default 90 ,
add autoextratime_chunks int(11) default 3;

ALTER TABLE `userapp_playerinfo` ADD kycverified varchar(1) DEFAULT 'N';
ALTER TABLE `userapp_limit` ADD kyc_check tinyint(1) default 0;
ALTER TABLE `userapp_limit` ADD preference_check tinyint(1) default 0;
ALTER TABLE `userapp_playerinfo1` ADD preference_updated datetime DEFAULT NULL;

ALTER TABLE `userapp_playerinfo1` ADD kyc_created datetime DEFAULT NULL ,
add kyc_updated datetime DEFAULT NULL;

ALTER TABLE 'userapp_playerinfo1'
  ADD id_proof_type char(3) DEFAULT NULL,
  ADD id_proof_no varchar(50) DEFAULT NULL,
  ADD id_proof_status varchar(1) DEFAULT 'N',
  ADD id_proof_image1 varchar(200) DEFAULT NULL,
  ADD id_proof_image2 varchar(200) DEFAULT NULL,
  ADD aadhar_no varchar(50) DEFAULT NULL,
  ADD aadhar_status varchar(1) DEFAULT 'N',
  ADD aadhar_image1 varchar(200) DEFAULT NULL,
  ADD aadhar_image2 varchar(200) DEFAULT NULL,
  ADD pan_card_no varchar(50) DEFAULT NULL,
  ADD pan_card_status varchar(1) DEFAULT 'N',
  ADD pan_card_image varchar(200) DEFAULT NULL,
  ADD preference_newsletter tinyint(1) DEFAULT 0,
  ADD preference_special_offers tinyint(1) DEFAULT 0,
  ADD preference_tournaments tinyint(1) DEFAULT 0;

CREATE TABLE `userapp_kyclogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playerinfoid_id` int(11) NOT NULL,
  `pan_card_comment` longtext DEFAULT NULL,
  `id_proof_comment` longtext DEFAULT NULL,
  `bank_comment` longtext DEFAULT NULL,
  `operator_id` int(11) NOT NULL,
  `timestamp` datetime DEFAULT NULL,
  `aadhar_comment` longtext DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `authuser_dynamic_hgs7623` FOREIGN KEY (`operator_id`) REFERENCES `auth_user` (`id`)
);

CREATE TABLE `userapp_dynamicurl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(50) DEFAULT NULL,
  `status` tinyint(1) DEFAULT 1,
  `operator_id` int(11) NOT NULL,
  `timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `authuser_dynamic_hgsd56` FOREIGN KEY (`operator_id`) REFERENCES `auth_user` (`id`)
);

CREATE TABLE `affiliate_affiliatecontactus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `subject` varchar(50) DEFAULT NULL,
  `message` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
);


CREATE TABLE `apiapp_customtoken` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(100) DEFAULT NULL,
  `playerid_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `apiapp_player_id` FOREIGN KEY (`playerid_id`) REFERENCES `userapp_playerinfo` (`id`)
);
