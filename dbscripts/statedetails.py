CREATE TABLE `userapp_statedetails` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `district` varchar(50) NULL,
  `city` varchar(50) NULL,
  `state_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userapp_state_id_333450a_state` (`state_id`),
  CONSTRAINT `userapp_state_id_333450a_state` FOREIGN KEY (`state_id`) REFERENCES `userapp_state` (`id`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

import csv
from userapp.models import *

with open('/var/www/list1.csv', 'r') as f:
    reader = csv.reader(f)
    for row in reader:
        print row[1].upper()
        st_obj = State.objects.get(statename=row[1].upper())
        std_obj = StateDetails.objects.create(state=st_obj,
                                    city=row[0])
        std_obj.save()