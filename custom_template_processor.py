from django.conf import settings
def get_meta_flags(request):
	if settings.ROBOT_META_FOLLOW_INDEX == True:
		return {'robot_content_value':"INDEX,FOLLOW"}
	return {'robot_content_value':'NOINDEX,NOFOLLOW'}