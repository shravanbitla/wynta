from django.contrib import admin
from .models import *
from .retrievers import *

# Register your models here.

class AuditTableAdmin(admin.ModelAdmin):
    date_hierarchy = 'processtime_stamp'
    list_display = ('datasource', 'job', 'status',  'processtime_stamp')
    list_filter = ('status', 'job', 'datasource')
    search_fields = ('error_msg', 'status')

    def get_readonly_fields(self, request, obj=None):
        return [field.name for field in obj._meta.fields]

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

admin.site.register(AuditTable, AuditTableAdmin)

class SiteWhitelabelMappingAdmin(admin.ModelAdmin):
    """ SiteWhitelabelMappingAdmin ui display """
    list_display = ['id', 'siteid', 'whitelabelid']
admin.site.register(SiteWhitelabelMapping, SiteWhitelabelMappingAdmin)

class PPAdvancedProcessedAdmin(admin.ModelAdmin):
    """ PPAdvancedProcessedAdmin ui display """
    list_display = ['fk_campaign', 'date', 'siteid', 'fk_campaign']
admin.site.register(PPAdvancedProcessed, PPAdvancedProcessedAdmin)

class PPPlayerProcessedAdmin(admin.ModelAdmin):
    """ PPPlayerProcessedAdmin ui display """
    list_display = ['playerid', 'date', 'siteid', 'fk_campaign']
admin.site.register(PPPlayerProcessed, PPPlayerProcessedAdmin)

class PostBackPlayerAdmin(admin.ModelAdmin):
    """ PostBackPlayerAdmin ui display """
    list_display = ['id', 'siteid', 'fk_campaign']
admin.site.register(PostBackPlayer, PostBackPlayerAdmin)

class PostBackAdvancedAdmin(admin.ModelAdmin):
    """ PostBackAdvancedAdmin ui display """
    list_display = ['id', 'siteid', 'fk_campaign']
admin.site.register(PostBackAdvanced, PostBackAdvancedAdmin)

class PostBackClicksAdmin(admin.ModelAdmin):
    """ PostBackClicksAdmin ui display """
    list_display = ['id', 'siteid', 'fk_campaign']
admin.site.register(PostBackClicks, PostBackClicksAdmin)

class PlayerCheckAdmin(admin.ModelAdmin):
    """ Player check display """
    list_display = ['playerid', 'date' ,'siteid', 'fk_campaign', 'ck_campaign']
admin.site.register(PlayerCheck, PlayerCheckAdmin)

class FileUploadAdmin(admin.ModelAdmin):
    """ affiliate info admin display"""
    list_display = ['date', 'excel', 'status']

    def save_model(self, request, obj, form, change):
        """ to check that admin of egcs can not have 5 account """
        fileupload = form.save(commit = False)
        password = form.cleaned_data.get("password")
        status = form.cleaned_data.get("status")
        # if form.instance.id:
        #     status = retrive_from_excel(request, fileupload)
        # else:
        status = retrive_from_excel(request)
        fileupload.status = status
        fileupload.save()

admin.site.register(FileUpload, FileUploadAdmin)

class UploadAdvancedAdmin(admin.ModelAdmin):
    """ PPAdvancedProcessedAdmin ui display """
    list_display = ['date', 'siteid', 'fk_campaign', 'processed']
admin.site.register(UploadAdvanced, UploadAdvancedAdmin)

class CampaignDateReportAdmin(admin.ModelAdmin):
    """ PPAdvancedProcessedAdmin ui display """
    search_fields = ['date', 'siteid', 'fk_campaign', ]
    list_display = ['date', 'siteid', 'fk_campaign', ]
admin.site.register(CampaignDateReport, CampaignDateReportAdmin)