"""Create your views here."""
#!/usr/bin/python
# -*- coding: latin-1 -*-
import random
import csv
import os, sys
import math
import json
import uuid
from django.contrib.auth import authenticate, login
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.views.generic.base import TemplateView
from django.template import RequestContext 
from django.core.urlresolvers import reverse
from django.contrib.sites.models import Site
# # # # # # # # # # from django.contrib.sites.models import get_current_site
from django.contrib.admin.views.decorators import staff_member_required
from django.utils.decorators import method_decorator
from django.db.models import Sum, Count
from django.db import connection
from django.contrib.sites.models import Site
from django.shortcuts import render
from affiliate.utils import *
from django.contrib.auth.models import User
from django.conf import settings
from affiliate.dashboard_data import *
from userapp.models import *
from affiliate.models import *
from userapp.function import *
from userapp.utils import get_network_sites_list, error_log, get_site_player_first_register_timestamp
from datetime import date, datetime, time
import calendar
# from django.contrib.admin.views.decorators import staff_member_required
from userapp.utils import get_fromdate_todate_for_django_query_filter, get_hexdigest,\
    get_site_player_first_register_timestamp
from userapp.utils import monthdelta, MyEncoder,get_paginate_dimensions, get_chips_type
from affiliate.utils import enc_password
from copy import deepcopy
from affiliate.dashboard_data import get_affiliate_campaign_link
#from userapp.userapp_db_entry import get_channels_list

from connectors.models import *
from affiliate.models import *
PB_Key = 'YWRtaW5KdW1wTWFuOkpNYW5BQDY1Nw'


def postback(request):
    """
    function to call postback
    """
    context_dict = {}
    secure_k = request.GET.get('secure')
    if secure_k != PB_Key:
        HttpResponse(json.dumps({'Status': 'Error', 'Message': 'Secure key is not valid'}, indent=4))
    goal_id = int(request.GET.get('goal_id')) if request.GET.get('goal_id') else 1
    site_id = int(request.GET.get('site_id')) if request.GET.get('site_id') else 0
    click_id = request.GET.get('clickid')
    offer_id = request.GET.get('offerid')
    player_id = int(request.GET.get('adv_sub')) if request.GET.get('adv_sub') else 0
    amount = float(request.GET.get('amount')) if request.GET.get('amount') else 0.0
    gender = request.GET.get('gender')
    sub_dict = {}
    for param in request.GET:
        if 'sub' in param and 'v' not in param:
            sub_dict[param] = request.GET.get(param + 'v', '')
    _d = datetime.now().date()
    try:
        if click_id:
            postback_clicks = PostBackClicks.objects.filter(
                                            clickid = click_id
                                            )
            postback_click_obj = postback_clicks[0]
            camp_map_obj = postback_click_obj.fk_campaign
            camp_obj = postback_click_obj.fk_campaign.fk_campaign
            netw_site_obj = NetworkAndSiteMap.objects.get(siteid=camp_obj.siteid)
            rev_percent = camp_map_obj.fk_affiliateinfo.account_manager.rev_percent_cut
        else:
            HttpResponse(json.dumps({'Status': 'Error', 'Message': 'click id is mandetory'}, indent=4))
        if goal_id == 1:
            postback_player, created = PPPlayerProcessed.objects.get_or_create(playerid=player_id,
                                        siteid=postback_click_obj.siteid,
                                        networkid=postback_click_obj.networkid,
                                        fk_campaign=postback_click_obj.fk_campaign,
                                        country=postback_click_obj.country,
                                        city=postback_click_obj.state,
                                        platform=postback_click_obj.platform,
                                        gender=gender,
                                        clickid = click_id
                                        )
            if created:
                postback_player.date = _d
                postback_player.registrationdate = datetime.now()
                postback_player.registrations = 1
                postback_player.save()
                # postback_adv, created = PostBackAdvanced.objects.get_or_create(date=_d,
                #                             siteid = camp_obj.siteid,
                #                             fk_campaign = camp_map_obj,
                #                             networkid = netw_site_obj.networkid
                #                             )
                # postback_adv.fk_campaign.trackerid = offer_id
                # postback_adv.registrations = postback_adv.registrations + 1 if postback_adv.registrations else 1
                # postback_adv.save()
            else:
                return HttpResponse(json.dumps({'Status': 'Error', 'Message': 'Player with the id is already registered.'}, indent=4))
        elif goal_id == 2:
            postback_player_obj = PPPlayerProcessed.objects.filter(playerid=player_id,
                                        siteid=postback_click_obj.siteid,
                                        networkid=postback_click_obj.networkid,
                                        fk_campaign=postback_click_obj.fk_campaign
                                        ).order_by('-date')
            if len(postback_player_obj) == 1 and not postback_player_obj[0].ftd:
                postback_player_obj = postback_player_obj[0]
                postback_player, created = PPPlayerProcessed.objects.get_or_create(playerid=player_id,
                                        siteid=postback_click_obj.siteid,
                                        networkid=postback_click_obj.networkid,
                                        fk_campaign=postback_click_obj.fk_campaign,
                                        date=_d,
                                        clickid = click_id
                                        )
                postback_player.lastdate = datetime.now()
                postback_player.ftddate = datetime.now()
                postback_player.ftd = amount
                postback_player.country = postback_player_obj.country
                postback_player.city = postback_player_obj.city
                postback_player.platform = postback_player_obj.platform
                postback_player.gender = postback_player_objgender
                postback_player.clickid = postback_player_obj.clickid
                postback_player.registrationdate = postback_player_obj.registrationdate
                postback_player.deposit = postback_player.deposit + amount
                rev_amount = (amount - abs(amount*rev_percent)) if amount else 0.0
                postback_player.revenue = postback_player.revenue + rev_amount
                postback_player.save()
            else:
                return HttpResponse(json.dumps({'Status': 'Error', 'Message': 'Player already completed FTD.'}, indent=4))
        elif goal_id == 3:
            postback_player_obj = PPPlayerProcessed.objects.filter(playerid=player_id,
                                        siteid=postback_click_obj.siteid,
                                        networkid=postback_click_obj.networkid,
                                        fk_campaign=postback_click_obj.fk_campaign
                                        ).order_by('-date')
            if len(postback_player_obj) == 1:
                postback_player_obj = postback_player_obj[0]
                postback_player, created = PPPlayerProcessed.objects.get_or_create(playerid=player_id,
                                        siteid=postback_click_obj.siteid,
                                        networkid=postback_click_obj.networkid,
                                        fk_campaign=postback_click_obj.fk_campaign,
                                        date=_d,
                                        clickid = click_id
                                        )
                postback_player.lastdate = datetime.now()
                postback_player.country = postback_player_obj.country
                postback_player.city = postback_player_obj.city
                postback_player.platform = postback_player_obj.platform
                postback_player.gender = postback_player_objgender
                postback_player.clickid = postback_player_obj.clickid
                postback_player.ftddate = postback_player_obj.ftddate
                postback_player.registrationdate = postback_player_obj.registrationdate
                postback_player.deposit = postback_player.deposit + amount
                rev_amount = (amount - abs(amount*rev_percent)) if amount else 0.0
                postback_player.revenue = postback_player.revenue + rev_amount
                postback_player.save()
            else:
                return HttpResponse(json.dumps({'Status': 'Error', 'Message': 'Player not registered or not done FTD.'}, indent=4))
        elif goal_id == 4:
            postback_player_obj = PPPlayerProcessed.objects.filter(playerid=player_id,
                                        siteid=postback_click_obj.siteid,
                                        networkid=postback_click_obj.networkid,
                                        fk_campaign=postback_click_obj.fk_campaign
                                        ).order_by('-date')
            if len(postback_player_obj) == 1:
                postback_player_obj = postback_player_obj[0]
                postback_player, created = PPPlayerProcessed.objects.get_or_create(playerid=player_id,
                                        siteid=postback_click_obj.siteid,
                                        networkid=postback_click_obj.networkid,
                                        fk_campaign=postback_click_obj.fk_campaign,
                                        date=_d,
                                        clickid = click_id
                                        )
                postback_player.lastdate = datetime.now()
                postback_player.country = postback_player_obj.country
                postback_player.city = postback_player_obj.city
                postback_player.platform = postback_player_obj.platform
                postback_player.gender = postback_player_objgender
                postback_player.clickid = postback_player_obj.clickid
                postback_player.ftddate = postback_player_obj.ftddate
                postback_player.registrationdate = postback_player_obj.registrationdate
                postback_player.cashout = postback_player.cashout + amount
                postback_player.revenue = postback_player.revenue - amount
                postback_player.save()
            else:
                return HttpResponse(json.dumps({'Status': 'Error', 'Message': 'Player not registered or does not have balance.'}, indent=4))
        elif goal_id == 5:
            postback_player_obj = PPPlayerProcessed.objects.filter(playerid=player_id,
                                        siteid=postback_click_obj.siteid,
                                        networkid=postback_click_obj.networkid,
                                        fk_campaign=postback_click_obj.fk_campaign
                                        ).order_by('-date')
            if len(postback_player_obj) == 1:
                postback_player_obj = postback_player_obj[0]
                postback_player, created = PPPlayerProcessed.objects.get_or_create(playerid=player_id,
                                        siteid=postback_click_obj.siteid,
                                        networkid=postback_click_obj.networkid,
                                        fk_campaign=postback_click_obj.fk_campaign,
                                        date=_d,
                                        clickid = click_id
                                        )
                postback_player.lastdate = datetime.now()
                postback_player.country = postback_player_obj.country
                postback_player.city = postback_player_obj.city
                postback_player.platform = postback_player_obj.platform
                postback_player.gender = postback_player_objgender
                postback_player.clickid = postback_player_obj.clickid
                postback_player.ftddate = postback_player_obj.ftddate
                postback_player.registrationdate = postback_player_obj.registrationdate
                postback_player.reversal = postback_player.reversal + amount
                rev_amount = (amount - abs(amount*rev_percent)) if amount else 0.0
                postback_player.revenue = postback_player.revenue + rev_amount
                postback_player.save()
        elif goal_id == 6:
            postback_player_obj = PPPlayerProcessed.objects.filter(playerid=player_id,
                                        siteid=postback_click_obj.siteid,
                                        networkid=postback_click_obj.networkid,
                                        fk_campaign=postback_click_obj.fk_campaign
                                        ).order_by('-date')
            if len(postback_player_obj) == 1:
                postback_player_obj = postback_player_obj[0]
                postback_player, created = PPPlayerProcessed.objects.get_or_create(playerid=player_id,
                                        siteid=postback_click_obj.siteid,
                                        networkid=postback_click_obj.networkid,
                                        fk_campaign=postback_click_obj.fk_campaign,
                                        date=_d,
                                        clickid = click_id
                                        )
                postback_player.lastdate = datetime.now()
                postback_player.country = postback_player_obj.country
                postback_player.city = postback_player_obj.city
                postback_player.platform = postback_player_obj.platform
                postback_player.gender = postback_player_objgender
                postback_player.clickid = postback_player_obj.clickid
                postback_player.ftddate = postback_player_obj.ftddate
                postback_player.registrationdate = postback_player_obj.registrationdate
                postback_player.bonuses = postback_player.bonuses + amount
                # postback_player.revenue = postback_player_obj.revenue + amount
                postback_player.save()
        elif goal_id == 7:
            postback_player_obj = PPPlayerProcessed.objects.filter(playerid=player_id,
                                        siteid=postback_click_obj.siteid,
                                        networkid=postback_click_obj.networkid,
                                        fk_campaign=postback_click_obj.fk_campaign
                                        ).order_by('-date')
            if len(postback_player_obj) == 1:
                postback_player_obj = postback_player_obj[0]
                postback_player, created = PPPlayerProcessed.objects.get_or_create(playerid=player_id,
                                        siteid=postback_click_obj.siteid,
                                        networkid=postback_click_obj.networkid,
                                        fk_campaign=postback_click_obj.fk_campaign,
                                        date=_d,
                                        clickid = click_id
                                        )
                postback_player.lastdate = datetime.now()
                postback_player.country = postback_player_obj.country
                postback_player.city = postback_player_obj.city
                postback_player.platform = postback_player_obj.platform
                postback_player.gender = postback_player_objgender
                postback_player.clickid = postback_player_obj.clickid
                postback_player.ftddate = postback_player_obj.ftddate
                postback_player.registrationdate = postback_player_obj.registrationdate
                postback_player.chargeback = postback_player.chargeback + amount
                postback_player.revenue = postback_player.revenue - amount
                postback_player.save()
        if sub_dict:
            postback_player, created = PPPlayerProcessed.objects.get_or_create(playerid=player_id,
                                        siteid=postback_click_obj.siteid,
                                        networkid=postback_click_obj.networkid,
                                        fk_campaign=postback_click_obj.fk_campaign,
                                        date=_d,
                                        clickid = click_id
                                        )
            for sub in sub_dict:
                sub_param_obj, created = SubParameters.objects.get_or_create(name=sub,
                                        siteid=postback_click_obj.siteid)
                param_mapping, created = SubParametersPlayerMapping.objects.get_or_create(player=postback_player,
                                        subid=sub_param_obj)
                param_mapping.subvalue = str(sub_dict[sub])
    except:
        return HttpResponse(json.dumps({'Status': 'Error', 'Message': 
                    'Error with Post back. Please contact support team.'}, indent=4))  
    # return render(request, 'admin/index.html', context_dict) 
    return HttpResponse(json.dumps({'Status': 'Success', 'Message': 'Successfully stored'}, indent=4))
