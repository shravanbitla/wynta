from rest_framework import serializers

from .models import AuditTable


class AuditTableSerializer(serializers.ModelSerializer):
    start = serializers.SerializerMethodField()
    end = serializers.SerializerMethodField()

    def get_start(self, obj):
        return str(obj.start)

    def get_end(self, obj):
        return str(obj.end)

    class Meta:
        model = AuditTable
