# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AuditTable',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('job', models.CharField(default=b'RET', max_length=4, choices=[(b'STG', b'Staging'), (b'PRO', b'Processing'), (b'RET', b'Retrieving')])),
                ('datasource', models.CharField(default=b'RET', max_length=25, choices=[(b'PP', b'ProgressPlay')])),
                ('status', models.CharField(default=b'Start', max_length=25)),
                ('processtime_stamp', models.DateTimeField(auto_now=True)),
                ('error_msg', models.TextField(default=b'No error')),
            ],
        ),
        migrations.CreateModel(
            name='Date',
            fields=[
                ('id', models.IntegerField(default=1, serialize=False, primary_key=True)),
                ('Day', models.CharField(max_length=256, null=True)),
                ('Month', models.CharField(max_length=200, null=True)),
                ('day_of_week', models.IntegerField(default=0)),
                ('day_of_month', models.IntegerField(default=0)),
                ('day_of_year', models.IntegerField(default=0)),
                ('week_of_year', models.IntegerField(default=0)),
                ('month_of_year', models.IntegerField(default=0)),
                ('quarter_of_year', models.IntegerField(default=0)),
                ('year', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='PPAdvancedRaw',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('whitelabelid', models.IntegerField(default=0)),
                ('registrations', models.IntegerField(default=0)),
                ('ftd', models.IntegerField(default=0)),
                ('rate', models.IntegerField(default=0)),
                ('deposit', models.FloatField(default=0.0)),
                ('cashout', models.FloatField(default=0.0)),
                ('chargeback', models.FloatField(default=0.0)),
                ('void', models.FloatField(default=0.0)),
                ('reversechargeback', models.FloatField(default=0.0)),
                ('bonus', models.FloatField(default=0.0)),
                ('expiredBonus', models.FloatField(default=0.0)),
                ('sidegamesbets', models.FloatField(default=0.0)),
                ('sidegameswins', models.FloatField(default=0.0)),
                ('jackpotcontribution', models.FloatField(default=0.0)),
                ('revenue', models.FloatField(default=0.0)),
                ('date', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='PPPlayerRaw',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('whitelabelid', models.IntegerField(default=0)),
                ('tracker', models.IntegerField(default=0)),
                ('dynamic', models.CharField(max_length=256)),
                ('country', models.CharField(max_length=256)),
                ('city', models.CharField(max_length=256)),
                ('zipcode', models.CharField(max_length=256)),
                ('currency', models.CharField(max_length=256)),
                ('gender', models.CharField(max_length=256)),
                ('birthdate', models.DateTimeField()),
                ('status', models.CharField(max_length=256)),
                ('platform', models.CharField(max_length=256)),
                ('registrationdate', models.DateTimeField()),
                ('ftddate', models.DateTimeField()),
                ('lastdate', models.DateTimeField()),
                ('registrations', models.IntegerField(default=0)),
                ('ftd', models.IntegerField(default=0)),
                ('deposit', models.FloatField(default=0.0)),
                ('totaldeposit', models.FloatField(default=0.0)),
                ('cashout', models.FloatField(default=0.0)),
                ('totalcashout', models.FloatField(default=0.0)),
                ('bonuses', models.FloatField(default=0.0)),
                ('expiredbonuses', models.FloatField(default=0.0)),
                ('revenue', models.FloatField(default=0.0)),
                ('clickid', models.CharField(max_length=256)),
                ('chargeback', models.FloatField(default=0.0)),
                ('void', models.FloatField(default=0.0)),
                ('reversechargeback', models.FloatField(default=0.0)),
                ('sidegamesbets', models.FloatField(default=0.0)),
                ('sidegameswins', models.FloatField(default=0.0)),
                ('jackpotcontribution', models.FloatField(default=0.0)),
            ],
        ),
        migrations.AddField(
            model_name='audittable',
            name='end',
            field=models.ForeignKey(related_name='end_date', to='connectors.Date'),
        ),
        migrations.AddField(
            model_name='audittable',
            name='start',
            field=models.ForeignKey(related_name='start_date', to='connectors.Date'),
        ),
    ]
