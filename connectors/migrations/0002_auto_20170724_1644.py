# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('connectors', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='audittable',
            name='end',
            field=models.DateTimeField(),
        ),
        migrations.AlterField(
            model_name='audittable',
            name='start',
            field=models.DateTimeField(),
        ),
    ]
