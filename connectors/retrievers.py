import datetime
import csv
import math
import os
import re
import requests
import time
import json

from abc import ABCMeta, abstractmethod, abstractproperty
from django.views.generic import View
from django.conf import settings
from django.db import transaction, IntegrityError
import requests
import base64
from django.contrib.sites.models import Site
from .models import *
from .views import *
from affiliate.views import *
from userapp.models import *
from affiliate.models import *
from django.core.mail import EmailMessage
import xlrd
from django.db.models import Sum, Count, Max
# import pycountry
pycountry = {}
def get_maxi_deductions(siteid, site_ids, country, revenue, deposit, cashout):
    deductions_dict = {'Jazzy Spins': {'United Kingdom': 0.66, 'Portugal': 0.30, 'Ireland':0.45, 'R': 0.35},
        'ChitChat Bingo': {'United Kingdom': 0.53, 'Portugal': 0.30, 'Ireland':0.45, 'R': 0.35},
        'BidBingo': {'United Kingdom': 0.53, 'Portugal': 0.30, 'Ireland':0.45, 'R': 0.35},
        'BingoZino': {'United Kingdom': 0.53, 'Portugal': 0.30, 'Ireland':0.45, 'R': 0.35},
        'Mr Jack Vegas': {'United Kingdom': 0.66, 'R': 0.60},
        'Mr Superplay': {'United Kingdom': 0.66, 'R': 0.60},
        'Maxiplay': {'United Kingdom': 0.66, 'R': 0.60},
        'Hippozino': {'United Kingdom': 0.66, 'R': 0.60},
        'Charming Slots': {'United Kingdom': 0.66, 'R': 0.60}
    }
    #import pdb;pdb.set_trace()
    if siteid in site_ids:
        # import pdb;pdb.set_trace()
        deduct_countries = deductions_dict[siteid.name]
        if country in deduct_countries.keys():
            deduct_percent = deduct_countries[country]
        else:
            deduct_percent = deduct_countries['R']
        revenue = (revenue - (revenue*deduct_percent))
        # cashout = deposit - revenue
        return True, revenue, cashout
    return False, revenue, cashout

class BaseConnector(ABCMeta):

    @abstractmethod
    def get_credentials(self, *args, **kwargs): pass

    @abstractmethod
    def retrieve_source_data(self, *args, **kwargs): pass

    @abstractmethod
    def store_source_data(self, *args, **kwargs): pass

    @property
    @abstractmethod
    def channel(self): pass


class ProgressPlayConnector(View):

    call_dict = {
        'advanced_data_url':"https://reports.api.progressplay.com/api/data/advanced",
        # 'player_data_url':"https://reports.api.progressplay.com/api/data/players",
        'player_data_url': "https://reports.api.progressplay.com/v20/api/data/v20/dailyactions",
        'player_personal_data_url': "https://reports.api.progressplay.com/v20/api/data/v20/playerdetails"
        }

    def channel(self):
        return 'progressplay'

    def __init__(self, *args, **kwargs):
        self.username = kwargs['username']
        self.password = kwargs['password']


    def dtobj_to_datestring(self, dt):
        return dt.strftime('%Y-%m-%d')

    def str2date(self, str):
        return datetime.datetime.strptime(str, '%Y-%m-%d')

    def get_per_chunks(self, **kwargs):
        import datetime as dttt
        day_stop = kwargs['day_stop']
        day_start = kwargs['day_start']
        chunk_size = kwargs['chunk_size']
        end_date = day_stop
        start_date = day_start
        period = (end_date-start_date).days
        print(period)
        until = day_stop
        if period % chunk_size == 0:
            chunks = []
            for i in range(period/chunk_size):
                chunks.append(until)
                until = until - dttt.timedelta(1)
        else:
            chunks = []
            for i in range(period/chunk_size):
                chunks.append(until)
                until = until - dttt.timedelta(1)
            until = until-dttt.timedelta(period % chunk_size)
            chunks.append(until)
        return chunks


    def save_audit(self, job, status, day_stop, day_start, e):
        nt_obj = Network.objects.get(networkname='ProgressPlay')
        if day_start:
            audit = AuditTable.objects.create(job=job,
                datasource=nt_obj,
                status=status,
                end=day_stop, start=day_start,
                error_msg=str(e)
            )

        else:
            audit = AuditTable.objects.create(job=job, datasource=nt_obj,
                status=status,
                error_msg=str(e)
            )
        audit.save()
        if status == 'Fail':
            msg = EmailMessage(nt_obj.networkname + ': '+ job, str(e), 
                "support@wynta.com", ['shravan.bitla@gridlogic.in'])
            msg.send()

    def convert_keys_to_string(self, dictionary):
        """Recursively converts dictionary keys to strings."""
        if not isinstance(dictionary, dict):
            if not isinstance(dictionary, int) and not isinstance(dictionary, float):
                try:
                    return str(dictionary)
                except:
                    return str(dictionary.encode('utf-8'))
            else:
                return dictionary
        return dict((str(k).lower(), self.convert_keys_to_string(v)) 
            for k, v in dictionary.items())

    @transaction.atomic
    def store_source_data(self, *args, **kwargs):
        """
        transaction.atomic() is used with for every create to catch the exception.
        It does not roll back only that row but all the rows that are inserted.
        As we have mentioned decorator on top it take cares of all the transactions
        not just one which is throwing exception.
        """
        source_data = kwargs['source_data']
        day_stop = kwargs['day_stop']
        day_start = kwargs['day_start']
        call_type = kwargs['call_type']
        ftd_data = kwargs['ftd_data']
        site_obj = kwargs['site_obj']
        raw_f = [i.name for i in PPPlayerRaw._meta.fields]
        # TODO: Make it more efficient to use bulk_create.
        # for objective in source_data:
        # import pdb;pdb.set_trace()
        source_data = json.loads(source_data.text)
        minus_30_file = day_start - timedelta(days=6)
        json_dump = 'media/json_dump/%s-%s.json'%(site_obj.id,str(day_start).split(' ')[0])
        if os.path.isfile(json_dump):
            os.remove(json_dump)
        with open(json_dump, 'w+') as outfile:
            json.dump(source_data, outfile, sort_keys = True, indent = 4,
                   ensure_ascii = False)
        minus_30_file = day_start - timedelta(days=30)
        json_minus_30_dump = 'media/json_dump/%s-%s.json'%(site_obj.id,str(minus_30_file).split(' ')[0])
        if os.path.isfile(json_minus_30_dump):
            os.remove(json_minus_30_dump)
        # d_source_data = {i['Playerid']:i for i in source_data}
        if call_type == 'player_data_url':
            date_fields = ['ftddate', 'birthday', 'ftddate',
                            'lastdate', 'registrationdate']
            list_rows_dict = []
            for raw_data_row in source_data:
                try:
                    row = self.convert_keys_to_string(raw_data_row)
                except Exception as e:
                    print str(e)
                if ftd_data.get(row['playerid']):
                    # import pdb;pdb.set_trace()
                    row['lastdate'] = ftd_data[row['playerid']]['LastUpdated']
                    row['registrationdate'] = ftd_data[row['playerid']]['RegisteredDate']
                    row['ftddate'] = ftd_data[row['playerid']]['FirstDepositDate']
                    row['birthday'] = ftd_data[row['playerid']]['DateOfBirth']
                    row['tracker'] = ftd_data[row['playerid']]['AffiliateID']
                    row['dynamic'] = ftd_data[row['playerid']]['DynamicParameter']
                    row['clickid'] = ftd_data[row['playerid']]['ClickID']
                    row['platform'] = ftd_data[row['playerid']]['RegisteredPlatform']
                    row['country'] = ftd_data[row['playerid']]['Country']
                    row['city'] = ftd_data[row['playerid']]['City']
                    row['gender'] = ftd_data[row['playerid']]['Gender']
                    row['currency'] = ftd_data[row['playerid']]['Currency']
                else:
                    pp_objs = PPPlayerProcessed.objects.filter(
                                        playerid=row['playerid'], siteid = site_obj)
                    if pp_objs:
                        pp_obj = pp_objs[0]
                        row['lastdate'] = row['date'].split('T')[0]
                        row['registrationdate'] = pp_obj.registrationdate
                        row['ftddate'] = pp_obj.ftddate
                        row['birthday'] = pp_obj.birthday
                        row['tracker'] = pp_obj.fk_campaign.trackerid
                        row['dynamic'] = pp_obj.dynamic
                        row['clickid'] = pp_obj.clickid
                        row['platform'] = pp_obj.platform
                        row['country'] = pp_obj.country
                        row['city'] = pp_obj.city
                        row['gender'] = pp_obj.gender
                        row['currency'] = pp_obj.currency
                    else:
                        continue
                row['date'] = row['date'].split('T')[0]
                row['void'] = row['voids']
                row['deposit'] = row['deposits']
                row['cashout'] = row['paidcashouts']
                row['chargeback'] = row['chargebacks']
                row['reversechargeback'] = row['reversechargebacks']
                del row['id']
                for j in row.keys():
                    if j not in raw_f:
                        del row[j]
                    elif row[j] == None:
                        row[j] = 'none'
                # del row['voids']
                # del row['deposits']
                # del row['chargebacks']
                # del row['reversechargebacks']
                for i in date_fields:
                    if isinstance(row[i], str) and row[i].lower() == 'none':
                        row[i] = row['date']
                # list_rows_dict.append(PPPlayerRaw(**row))
                try:
                    with transaction.atomic():
                        PPPlayerRaw.objects.create(**row)
                    # with transaction.atomic():
                    #     # import pdb;pdb.set_trace()
                    #     PPPlayerRaw.objects.bulk_create(list_rows_dict, batch_size=1000)
                except Exception as e:
                    # import pdb;pdb.set_trace()
                    self.save_audit('STG', 'Fail', day_stop, day_start, e)
                    return False
        else:
            for raw_data_row in source_data:
                row = self.convert_keys_to_string(raw_data_row)
                row['date'] = day_start.split(' ')[0]
                row['tracker'] = row['id']
                del row['id']
                try:
                    with transaction.atomic():
                        PPAdvancedRaw.objects.create(**row)
                except Exception as e:
                    self.save_audit('STG', 'Fail', day_stop, day_start, e)
                    return False
        return True

    @transaction.atomic
    def retrieve_source_data(self, *args, **kwargs):
        """
        transaction.atomic() is used with for every create to catch the exception.
        It does not roll back only that row but all the rows that are inserted.
        As we have mentioned decorator on top it take cares of all the transactions
        not just one which is throwing exception.
        """
        day_stop = kwargs['day_stop']
        day_start = kwargs['day_start']
        call_type = kwargs['call_type']
        site_obj = kwargs['site_obj']
        source_data = {}
        # import pdb;pdb.set_trace()
        network_obj = Network.objects.get(networkname='ProgressPlay')
        network_ap_mapping = NetworkAffiliateProgrammeMapping.objects.filter(
                            networkid=network_obj,
                            siteid=site_obj.siteid)
        if not network_ap_mapping or len(network_ap_mapping)>1:
            return False
        self.username = str(network_ap_mapping[0].username)
        self.password = str(network_ap_mapping[0].password)
        # self.username = 'adminsticky'
        # self.password = 'St!c&2018'
        authrize_code = base64.b64encode(str.encode('%s:%s'%(self.username,self.password)))
        #data = base64.b64decode(encoded)
        # import pdb;pdb.set_trace()
        Whitelabel_id = site_obj.whitelabelid
        headers = {
            'authorization': "Basic "+authrize_code,
            'content-type': "application/x-www-form-urlencoded",
            'cache-control': "no-cache"
            }
        import datetime as dtt
        now = dtt.datetime.now()
        p_day_stop = now
        p_day_start = now - dtt.timedelta(days=4)
        c_type = 'player_personal_data_url'
        p_payload = 'Whitelabels='+str(Whitelabel_id)+'&LastUpdatedDateStart='+str(p_day_start).split(' ')[0]+'&LastUpdatedDateEnd='+str(p_day_stop).split(' ')[0]
        data_report = requests.request("POST", self.call_dict[c_type], data=p_payload, headers=headers)
        if data_report:
            source_data = json.loads(data_report.text)
            source_data = {i['PlayerID']:i for i in source_data}
        else:
            #pass
            # import pdb;pdb.set_trace()
            print 'Exit from player personal data. ', data_report.text
            #return False
        try:
            # import pdb;pdb.set_trace()
            with transaction.atomic():
                if call_type == 'player_data_url':
                    chunks = self.get_per_chunks(day_stop=day_stop, day_start=day_start,chunk_size=1)
                    for chunk in chunks:
                        #if day_stop:
                        payload = 'Whitelabels='+str(Whitelabel_id)+'&DateStart='+chunk.strftime('%Y/%m/%d')+'&DateEnd='+chunk.strftime('%Y/%m/%d')
                        report = requests.request("POST", self.call_dict[call_type], data=payload, headers=headers)
                        if report:
                            self.save_audit(
                                'RET', 'Success', None,
                                None, 'Retrieving Done'
                            )
                        else:
                            report = requests.request("POST", self.call_dict[call_type], data=payload, headers=headers)
                            if not report:
                                self.save_audit(
                                    'RET', 'Fail', None,
                                    None, str(report.text)+ ' Error in Retrieving '+ str(site_obj.siteid.domain)
                                )
                                return False
                                # import pdb;pdb.set_trace()
                                # continue
                        stored = self.store_source_data(source_data=report, site_obj = site_obj.siteid,
                                 day_stop=chunk, day_start=chunk, call_type=call_type, ftd_data=source_data)
                        if not stored:
                            return False
                else:
                    chunks = self.get_per_chunks(day_stop=day_stop, day_start=day_start,chunk_size=1)
                    for chunk in chunks:
                        payload = 'WhitelabelId='+str(Whitelabel_id)+'&StartDate='+str(chunk)+'&EndDate='+str(chunk)+'&Grouping=Tracker'
                        report = requests.request("POST", self.call_dict[call_type], data=payload, headers=headers)
                        if report:
                            self.save_audit(
                                'RET', 'Success', None,
                                None, 'Retrieving Done'
                            )
                        else:
                            self.save_audit(
                                'RET', 'Fail', None,
                                None, str(report.text)+ ' Error in Retrieving'
                            )
                            return False
                        stored = self.store_source_data(source_data=report,
                                 day_stop=chunk, day_start=chunk, call_type=call_type)
                        if not stored:
                            return False
                return [day_start, day_stop]
        except Exception as e:
            # import pdb;pdb.set_trace()
            self.save_audit(
                'STG', 'Fail', day_stop,
                day_start, e
            )
            return False

    def affise_post_back(self):
        track_objs = CampaignTrackerMapping.objects.all()
        for track in track_objs:
            if track.campaign_reg_code:
                tracker_obj = PPAdvancedProcessed.objects.filter(fk_campaign=track).aggregate(
                        revenue=Sum('revenue'))
                if tracker_obj['revenue']:
                    track.goalid = track.goalid + 1
                    track.save()
                    netcash = round(tracker_obj['revenue'],3)
                    send_campaign_postback_99(track.campaign_reg_code, 'NTR', netcash, track.goalid)
        return True

    def get_map_details(self, obj, siteobj, raw):
        # import pdb;pdb.set_trace()
        aff_id = None
        if 'ga' in raw.dynamic:
            mid = raw.dynamic.split('ga')[0]
            s_id = raw.dynamic.split('ga')[1].split('cid')[0]
            if 'affid' in raw.dynamic:
                aff_id = raw.dynamic.split('ga')[1].split('cid')[1].split('affid')[1]
                try:
                    aff_id = int(aff_id)
                except:
                    aff_id = None
        else:
            mid = raw.dynamic.split('b_')[0].replace('a_','')
            s_id = raw.dynamic.split('b_')[1].split('c_')[0]
        try:
            mid = int(mid)
            s_id = int(s_id)
        except:
            return False
        if aff_id:
            aff_obj = Affiliateinfo.objects.filter(mappingid=aff_id,
                                            account_manager=obj.account_manager)
            # print 'affid got ', aff_id, aff_obj
            aff_obj = aff_obj[0] if aff_obj else None
        else:
            as_ids = AffiliateSite.objects.filter(site_id=s_id,
                                fk_affiliateinfo__account_manager=obj.account_manager)
            if not as_ids:
                print 'sid not ', s_id
                #msg = EmailMessage(str(s_id) + ' : for brand '+ siteobj.name, str(mid),
                #"support@wynta.com", ['shravan.bitla@gridlogic.in'])
                #msg.send()
            aff_obj = as_ids[0].fk_affiliateinfo if as_ids else None
        
        camp_obj, created = Campaign.objects.get_or_create(
                            account_manager=obj.account_manager, name=str(mid), 
                            siteid=siteobj)
        camp_obj.save()
        if aff_obj:
            camp_track_list_obj = CampaignTrackerMapping.objects.filter(fk_affiliateinfo=aff_obj, 
                            fk_campaign=camp_obj, goalid=1)
            if not camp_track_list_obj:
                camp_track_obj = CampaignTrackerMapping.objects.create(fk_affiliateinfo=aff_obj, 
                                fk_campaign=camp_obj, trackerid=raw.tracker, goalid=1)
            else:
                camp_track_obj = camp_track_list_obj[0]
        else:
            #msg = EmailMessage(str(s_id) + ' : for brand '+ siteobj.name, str(mid),
            #    "support@wynta.com", ['shravan.bitla@gridlogic.in'])
            #msg.send()
            camp_track_list_obj = CampaignTrackerMapping.objects.filter(fk_affiliateinfo=obj, 
                            fk_campaign=camp_obj, goalid=1)
            if not camp_track_list_obj:
                camp_track_obj = CampaignTrackerMapping.objects.create(fk_affiliateinfo=obj, 
                                fk_campaign=camp_obj, trackerid=raw.tracker, goalid=1)
            else:
                camp_track_obj = camp_track_list_obj[0]
        return camp_track_obj


    def process_data(self, **kwargs):
        day_stop = kwargs['day_stop']
        day_start = kwargs['day_start']
        call_type = kwargs['call_type']
        site_obj = kwargs['site_obj']
        siteobj = site_obj.siteid
        acmanager = AccountManager.objects.get(affiliateprogramme__name='Maxi Affiliates')
        maxi_sites = acmanager.siteid.all()
        ia_players_list = []
        if call_type != 'player_data_url':
            try:
                if Affiliateinfo.objects.filter(id=5, siteid=site_obj.siteid):
                    obj = Affiliateinfo.objects.get(id=5)
                else:
                    obj = Affiliateinfo.objects.filter(siteid=site_obj.siteid)[0]
                rev_percent = obj.account_manager.rev_percent_cut
                trackers = PPAdvancedRaw.objects.exclude(tracker=0).values_list('tracker').distinct()
                for tracker in trackers:
                    tracker = tracker[0]
                    camp_track_obj, created = CampaignTrackerMapping.objects.get_or_create(trackerid=tracker,
                                                            goalid=1)
                    if created:
                        linkobj, created = Campaign.objects.get_or_create(
                                                fk_affiliateinfo=obj, name=str(tracker), siteid=siteobj)
                        linkobj.save()
                        camp_track_obj.fk_campaign = linkobj
                        camp_track_obj.save()
                raw_obj = PPAdvancedRaw.objects.filter(date__gte=day_start, date__lte=day_stop)
                for raw in raw_obj:
                    if raw.tracker != 0:
                        camp_track_obj = CampaignTrackerMapping.objects.get(trackerid=raw.tracker,
                                                            goalid=1)
                    else:
                        camp_track_obj, created = CampaignTrackerMapping.objects.get_or_create(trackerid=0,
                                                            goalid=1)
                        linkobj, created = Campaign.objects.get_or_create(fk_affiliateinfo=obj, name='None', siteid=siteobj)
                        if created:
                            linkobj.save()
                            camp_track_obj.fk_campaign = linkobj
                            camp_track_obj.save()
                    process_obj, created = PPAdvancedProcessed.objects.get_or_create(date=raw.date,
                                    fk_campaign = camp_track_obj)
                    process_obj.siteid = siteobj
                    process_obj.registrations = raw.registrations
                    process_obj.ftd = raw.ftd
                    process_obj.rate = raw.rate
                    process_obj.deposit = raw.deposit
                    process_obj.cashout = raw.cashout
                    process_obj.chargeback = raw.chargeback
                    process_obj.void = raw.void
                    process_obj.reversechargeback = raw.reversechargeback
                    process_obj.bonus = raw.bonus
                    process_obj.expiredbonus = raw.expiredbonus
                    process_obj.sidegamesbets = raw.sidegamesbets
                    process_obj.sidegameswins = raw.sidegameswins
                    process_obj.jackpotcontribution = raw.jackpotcontribution
                    process_obj.revenue = (raw.revenue - (raw.revenue*rev_percent)) if raw.revenue else 0.0
                    process_obj.save()
                # self.affise_post_back()
                return True, 'Success'
            except Exception as e:
                print 'process advanced data error:'+str(e)
                return False, str(e)
        else:
            try:
                # import pdb;pdb.set_trace()
                currency = ''
                currency_obj = Currency.objects.filter(siteid=site_obj.siteid)
                if currency_obj:
                    currency = currency_obj[0].currency
                if Affiliateinfo.objects.filter(id=5, siteid=site_obj.siteid):
                    obj = Affiliateinfo.objects.get(id=5)
                else:
                    obj = Affiliateinfo.objects.filter(siteid=site_obj.siteid).order_by('id')[0]
                rev_percent = obj.account_manager.rev_percent_cut
                if site_obj.siteid.id== 112:
                    rev_percent = 0.39
                    print 'Slots miller for FML brand'
                raw_obj = PPPlayerRaw.objects.all()
                w_dynamic = None
                for raw in raw_obj:
                    if raw.clickid and raw.clickid != '' and raw.clickid.lower() != 'none':
                        # import pdb;pdb.set_trace()
                        click_obj = PostBackClicks.objects.filter(clickid=raw.clickid)
                        if click_obj:
                            camp_track_obj = click_obj[0].fk_campaign
                            w_dynamic = click_obj[0].wdynamic if click_obj[0].wdynamic else None
                        else:
                            continue
                    elif raw.dynamic and raw.dynamic != '' and raw.dynamic.lower() != 'none':
                        # import pdb;pdb.set_trace()
                        aff_obj = []
                        if 'aid' in raw.dynamic:
                            aid, cid = raw.dynamic.split('aid')[1].split('cid')
                            camp_track_obj = CampaignTrackerMapping.objects.filter(fk_affiliateinfo__id=aid,
                                                            fk_campaign__id=cid)[0]
                        elif siteobj.id==5:
                            dynamic_c = raw.dynamic.replace('/', '')
                            aff_obj = Affiliateinfo.objects.filter(Q(username=dynamic_c) | Q(email=dynamic_c))
                            if aff_obj:
                                camp_obj, created = Campaign.objects.get_or_create(fk_affiliateinfo=aff_obj[0], 
                                                name=str(raw.tracker), siteid=siteobj)
                                camp_track_list_obj = CampaignTrackerMapping.objects.filter(fk_affiliateinfo=aff_obj[0], 
                                                fk_campaign=camp_obj, goalid=1)
                                if not camp_track_list_obj:
                                    camp_track_obj = CampaignTrackerMapping.objects.create(fk_affiliateinfo=aff_obj[0], 
                                                    fk_campaign=camp_obj, trackerid=raw.tracker, goalid=1)
                                else:
                                    camp_track_obj = camp_track_list_obj[0]
                        if siteobj.id != 5 or not aff_obj:
                            # import pdb;pdb.set_trace()
                            if ('cid' in raw.dynamic and 'ga' in raw.dynamic) and siteobj.id != 122: # or ('a_' in raw.dynamic and 'b_' in raw.dynamic):
                                camp_track_obj = self.get_map_details(obj, siteobj, raw)
                                if not camp_track_obj:
                                    continue
                            elif raw.tracker != 0:
                                if ('a_' in raw.dynamic and 'b_' in raw.dynamic):
                                    ia_players_list.append(raw.dynamic)
                                camp_track_list_obj = CampaignTrackerMapping.objects.filter(trackerid=raw.tracker,
                                                                    fk_affiliateinfo=obj,
                                                            goalid=1, fk_campaign__siteid=site_obj.siteid)
                                if not camp_track_list_obj:
                                    linkobj, created = Campaign.objects.get_or_create(
                                                            fk_affiliateinfo=obj, name=str(raw.tracker), siteid=siteobj)
                                    linkobj.save()
                                    camp_track_obj, created = CampaignTrackerMapping.objects.get_or_create(trackerid=raw.tracker,
                                                                    goalid=1, fk_affiliateinfo=obj,
                                                                    fk_campaign__siteid=site_obj.siteid)
                                    camp_track_obj.fk_campaign = linkobj
                                    camp_track_obj.save()
                                else:
                                    camp_track_obj = camp_track_list_obj[0]
                                    created = False
                                if created or not camp_track_obj.fk_campaign:
                                    linkobj, created = Campaign.objects.get_or_create(
                                                            fk_affiliateinfo=obj, name=str(raw.tracker), siteid=siteobj)
                                    linkobj.save()
                                    camp_track_obj.fk_campaign = linkobj
                                    camp_track_obj.save()
                            else:
                                camp_track_list_obj = CampaignTrackerMapping.objects.filter(trackerid=raw.tracker,
                                                        goalid=1, fk_affiliateinfo=obj,
                                                        fk_campaign__siteid=siteobj)
                                if not camp_track_list_obj:
                                    camp_track_obj, created = CampaignTrackerMapping.objects.get_or_create(trackerid=0,
                                                                        goalid=1, fk_affiliateinfo=obj)
                                else:
                                    camp_track_obj = camp_track_list_obj[0]
                                linkobj, created = Campaign.objects.get_or_create(fk_affiliateinfo=obj, name='0', siteid=siteobj)
                                linkobj.save()
                                camp_track_obj.fk_campaign = linkobj
                                camp_track_obj.save()
                    elif raw.tracker != 0:
                        camp_track_list_obj = CampaignTrackerMapping.objects.filter(trackerid=raw.tracker,
                                                            goalid=1, fk_affiliateinfo=obj,
                                                            fk_campaign__siteid=site_obj.siteid)
                        # camp_track_list_obj = CampaignTrackerMapping.objects.filter(trackerid=raw.tracker,
                        #                                     goalid=1, fk_affiliateinfo=obj)
                        if not camp_track_list_obj:
                            linkobj, created = Campaign.objects.get_or_create(
                                                                fk_affiliateinfo=obj, name=str(raw.tracker), siteid=siteobj)
                            linkobj.save()
                            camp_track_obj, created = CampaignTrackerMapping.objects.get_or_create(trackerid=raw.tracker,
                                                            goalid=1, fk_affiliateinfo=obj,
                                                            fk_campaign__siteid=site_obj.siteid)
                            camp_track_obj.fk_campaign = linkobj
                            camp_track_obj.save()
                        else:
                            camp_track_obj = camp_track_list_obj[0]
                            created = False
                        if created or not camp_track_obj.fk_campaign:
                            linkobj, created = Campaign.objects.get_or_create(
                                                    fk_affiliateinfo=obj, name=str(raw.tracker), 
                                                    siteid=siteobj)
                            linkobj.save()
                            camp_track_obj.fk_campaign = linkobj
                            camp_track_obj.save()
                    else:
                        # import pdb;pdb.set_trace()
                        camp_track_list_obj = CampaignTrackerMapping.objects.filter(trackerid=raw.tracker,
                                                            goalid=1, fk_affiliateinfo=obj,
                                                            fk_campaign__siteid=siteobj)
                        if not camp_track_list_obj:
                            linkobj, created = Campaign.objects.get_or_create(fk_affiliateinfo=obj, name='0', siteid=siteobj)
                            linkobj.save()
                            camp_track_obj, created = CampaignTrackerMapping.objects.get_or_create(trackerid=raw.tracker,
                                                            goalid=1, fk_affiliateinfo=obj,
                                                            fk_campaign__siteid=site_obj.siteid)
                            camp_track_obj.fk_campaign = linkobj
                            camp_track_obj.save()
                        else:
                            camp_track_obj = camp_track_list_obj[0]
                            linkobj, created = Campaign.objects.get_or_create(fk_affiliateinfo=obj, name='0', siteid=siteobj)
                            linkobj.save()
                            camp_track_obj.fk_campaign = linkobj
                            camp_track_obj.save()
                    playercheck = PlayerCheck.objects.filter(playerid=raw.playerid,
                                                            siteid=siteobj)
                    player_check = False
                    if playercheck:
                        if playercheck[0].date < raw.date:
                            camp_track_obj = playercheck[0].ck_campaign
                            process_obj = PPPlayerProcessed.objects.filter(playerid=raw.playerid,
                                            siteid = siteobj,
                                            date=raw.date
                                            )
                            player_check = True
                            if process_obj:
                                process_obj.delete()
                    if camp_track_obj.fk_campaign.siteid != siteobj:
                        msg = EmailMessage(' Brand not matching : for brand '+ siteobj.name, str(siteobj.id),
                                 "support@wynta.com", ['shravan.bitla@gridlogic.in'])
                        msg.send()
                        continue
                    if PPPlayerProcessed.objects.filter(playerid=raw.playerid,date=raw.date).exclude(siteid=siteobj).count()>0:
                        if camp_track_obj.fk_campaign.siteid == siteobj:
                            # import pdb;pdb.set_trace()
                            PPPlayerProcessed.objects.filter(playerid=raw.playerid,date=raw.date).exclude(siteid=siteobj).delete()
                    process_obj = PPPlayerProcessed.objects.filter(playerid=raw.playerid,
                                            siteid = siteobj,
                                            date=raw.date
                                            )
                    country_obj = Country.objects.filter(countryname=raw.country.strip())
                    if not country_obj:
                        country_obj = Country.objects.create(countryname=raw.country.strip())
                        country_obj.save()
                    poct, raw.revenue, raw.cashout = get_maxi_deductions(siteobj, maxi_sites,
                                  raw.country, raw.revenue, raw.deposit, raw.cashout)
                    if len(process_obj) == 0:
                        # import pdb;pdb.set_trace()
                        process_obj = PPPlayerProcessed.objects.create(playerid=raw.playerid,
                                                fk_campaign = camp_track_obj,
                                                siteid = siteobj,
                                                birthday = raw.birthday,
                                                lastdate=raw.lastdate,
                                                registrations = raw.registrations,
                                                dynamic = raw.dynamic,
                                                country = raw.country,
                                                currency = currency,
                                                city = raw.city,
                                                zipcode = raw.zipcode,
                                                gender = raw.gender,
                                                status = raw.status,
                                                platform = raw.platform,
                                                deposit = raw.deposit,
                                                clickid = raw.clickid,
                                                totaldeposit = raw.totaldeposit,
                                                totalcashout = raw.totalcashout,
                                                cashout = raw.cashout,
                                                bonuses = raw.bonuses,
                                                expiredbonuses = raw.expiredbonuses,
                                                revenue = (raw.revenue - (raw.revenue*rev_percent)) if raw.revenue else 0.0,
                                                chargeback = raw.chargeback,
                                                void = raw.void,
                                                reversechargeback = raw.reversechargeback,
                                                sidegamesbets = raw.sidegamesbets,
                                                sidegameswins = raw.sidegameswins,
                                                jackpotcontribution = raw.jackpotcontribution,
                                                date=raw.date
                                                )
                        if not player_check:
                            process_obj.registrationdate = raw.registrationdate
                            process_obj.ftddate = raw.ftddate
                            process_obj.save()
                    else:
                        # import pdb;pdb.set_trace()
                        process_obj = process_obj[0]
                        if process_obj.processed:
                            continue
                        process_obj.fk_campaign = camp_track_obj
                        process_obj.siteid = siteobj
                        if not player_check:
                            process_obj.registrationdate = raw.registrationdate
                            process_obj.ftddate = raw.ftddate
                        process_obj.birthday = raw.birthday
                        process_obj.lastdate=raw.lastdate
                        process_obj.registrations = raw.registrations
                        process_obj.dynamic = raw.dynamic
                        process_obj.country = raw.country
                        process_obj.city = raw.city
                        process_obj.zipcode = raw.zipcode
                        process_obj.gender = raw.gender
                        process_obj.status = raw.status
                        process_obj.platform = raw.platform
                        process_obj.deposit = raw.deposit
                        process_obj.totaldeposit = raw.totaldeposit
                        process_obj.totalcashout = raw.totalcashout
                        process_obj.cashout = raw.cashout
                        process_obj.bonuses = raw.bonuses
                        process_obj.expiredbonuses = raw.expiredbonuses
                        process_obj.revenue = (raw.revenue - (raw.revenue*rev_percent)) if raw.revenue else 0.0
                        process_obj.chargeback = raw.chargeback
                        process_obj.void = raw.void
                        process_obj.reversechargeback = raw.reversechargeback
                        process_obj.sidegamesbets = raw.sidegamesbets
                        process_obj.sidegameswins = raw.sidegameswins
                        process_obj.jackpotcontribution = raw.jackpotcontribution
                    process_obj.wdynamic = w_dynamic
                    process_obj.save()
                #if ia_players_list:
                #    msg = EmailMessage(' Income Access dynamic : for brand '+ siteobj.name, str(set(ia_players_list)),
                #                 "support@wynta.com", ['shravan.bitla@gridlogic.in'])
                #    msg.send()
                return True, 'Success'
            except Exception as e:
                # import pdb;pdb.set_trace()
                ele_error =  'process player data error: '+str(e) + ' brand: '+ str(siteobj.name)
                return False, ele_error


    #@transaction.atomic
    def processing_data(self, **kwargs):
        """
        transaction.atomic() is used with for every create to catch the exception.
        It does not roll back only that row but all the rows that are inserted.
        As we have mentioned decorator on top it take cares of all the transactions
        not just one which is throwing exception.
        """
        day_stop = kwargs['day_stop']
        day_start = kwargs['day_start']
        call_type = kwargs['call_type']
        site_obj = kwargs['site_obj']
        try:
            # with transaction.atomic():
            processed, st_e = self.process_data(day_stop=day_stop, day_start=day_start, site_obj=site_obj, call_type=call_type)
            if processed:
                net_obj = NetworkAffiliateProgrammeMapping.objects.filter(siteid=site_obj.siteid)
                if net_obj:
                    net_obj = net_obj[0]
                    net_obj.retrive = True
                    net_obj.save()
                self.save_audit(
                    'PRO', 'Success', None,
                    None, 'Processing Done'
                )
                return True
            else:
                self.save_audit(
                    'PRO', 'Fail', None,
                    None, st_e + ' Error in Processing'
                )
                return False
        except Exception as e:
            print 'processing error:'+str(e)
            self.save_audit(
                'PRO', 'Fail', day_stop,
                day_start, e
            )
            return False

import sys
import datetime as dtt
def on_demand_pp(request, siteid):
    try:
        call = 'player_data_url'
        now = dtt.datetime.now()
        day_stop = now
        day_start = now - dtt.timedelta(days=30)
        pp_connector = ProgressPlayConnector(
            username='',
            password=''
        )
        network_obj = Network.objects.get(networkname='ProgressPlay')
        pp_connector.network = network_obj
        site_white_obj = SiteWhitelabelMapping.objects.filter(networkid=network_obj, siteid_id=siteid)
        if not site_white_obj:
            return False
        site_obj = site_white_obj[0]
        stored = pp_connector.retrieve_source_data(
                day_stop=day_stop, day_start=day_start, 
                site_obj=site_obj, call_type=call)
        if stored:
            pp_connector.save_audit(
                'STG', 'Success', day_stop,
                day_start, 'Stagging Success'
            )
            pp_connector.processing_data(day_stop=day_stop, 
                day_start=day_start, site_obj=site_obj, call_type=call)
        else:
            return False
        return True
    except Exception as e:
        print str(e)
        msg = EmailMessage('On demand ProgressPlay script', str(e), 
                "support@wynta.com", ['shravan.bitla@gridlogic.in'])
        msg.send()
        return False

def retrive_from_excel(request):
    if request.method in ['POST', 'PUT']:
        # form = UploadFileForm(request.POST, request.FILES)
        # if form.is_valid():
        resp = handle_uploaded_file(request.FILES['excel'], request)
        return resp

def handle_uploaded_file(f, request):
    # networkid = request.POST.get('networkid')
    try:
        affid = int(request.POST.get('affiliate')) if request.POST.get('affiliate') else 0
        if not affid:
            affid = int(request.POST.get('fk_affiliateinfo')) if request.POST.get('fk_affiliateinfo') else 0
        date = str(request.POST.get('date'))
        ext = os.path.splitext(f.name)[1]  # [0] returns path+filename
        valid_extensions = ['.xlsx', '.xls']
        site_dict = {'FTB': 'Funtastic Bingo', 'TGIB': 'TGIBingo', 'SPT':'Slots Pocket'}
        if not ext.lower() in valid_extensions:
            raise ValidationError(u'Unsupported file extension.')
        book = xlrd.open_workbook(file_contents=f.read())
        sheets = book.sheets()
        headers_list = []
        for col in range(0, sheets[0].ncols):
            headers_list.append(sheets[0].cell_value(0, col).replace(' ','').lower())
        for row in range(1, sheets[0].nrows):
            row_list = [sheets[0].cell_value(row, i) for i in range(0, sheets[0].ncols)]
            row_dict = dict(zip(headers_list, row_list))
            # if row_dict['siteid'] in site_dict:
            #     site_name = site_dict[row_dict['brandname']]
            # else:
            #     site_name = row_dict['brandname']
            if not row_dict['siteid']:
                return 'ERR'
            site_obj = Site.objects.get(id=int(row_dict['siteid']))
            site = site_obj.domain
            admin = Affiliateinfo.objects.get(id=affid)
            subname = 'fileUpload'
            subid = str(int(row_dict['affiliateid']))
            camp_track_map = None
            if row_dict['campaignid']:
                camp_id = str(int(row_dict['campaignid']))
                camp_track_map = CampaignTrackerMapping.objects.filter(trackerid=int(camp_id),
                                                fk_campaign__siteid=site_obj, goalid=3)
            if not camp_track_map:
                aff_list = Affiliateinfo.objects.filter(
                        username=subid,siteid=site_obj)
                if len(aff_list) == 0:
                    aff_obj = Affiliateinfo.objects.create(username=subid,
                                               password=admin.password, contactname=subname,
                                               companyname=admin.companyname, 
                                               email=subname + str(subid) + '@' + site,
                                               address=admin.address, city=admin.city,
                                               state=admin.state, country=admin.country, 
                                               ipaddress=admin.ipaddress,
                                               registeredon=datetime.now(),
                                               postcode=admin.postcode, telephone=admin.telephone,
                                               dob=admin.dob,
                                               account_manager=admin.account_manager)
                    networkid_obj = Network.objects.all()
                    for nt in networkid_obj:
                        aff_obj.networkid.add(nt)
                    aff_obj.siteid.add(site_obj)
                    aff_obj.superaffiliate = admin
                    aff_obj.save()
                    # aff_comm_obj = Affiliatecommission.objects.create(fk_affiliateinfo=aff_obj,
                    #                                 revshare=True,
                    #                                 revsharepercent=100)
                    # aff_comm_obj.save()
                else:
                    aff_obj = aff_list[0]
                if row_dict['campaignid']:
                    camp_id = str(int(row_dict['campaignid']))
                else:
                    camp_id = subid

                camp_track_map, created = CampaignTrackerMapping.objects.get_or_create(trackerid=int(camp_id),
                                                fk_affiliateinfo=aff_obj, goalid=3)
                if created:
                    linkobj, created = Campaign.objects.get_or_create(
                                            fk_affiliateinfo=aff_obj, name=str(camp_id), siteid=site_obj)
                    linkobj.save()
                    camp_track_map.fk_campaign = linkobj
                    camp_track_map.save()
            else:
                camp_track_map = camp_track_map[0]
            process_obj, created = UploadAdvanced.objects.get_or_create(date=date,
                                        fk_campaign = camp_track_map)
            ftd_r = int(int(row_dict['ftd'])/int(row_dict['registrations'])) if int(row_dict['registrations']) else 0
            process_obj.siteid = site_obj
            process_obj.clicks = int(row_dict['clicks'])
            process_obj.registrations = int(row_dict['registrations'])
            process_obj.ftd = int(row_dict['ftd'])
            process_obj.rate = ftd_r
            process_obj.deposit = float(row_dict['deposits'])
            process_obj.cashout = float(row_dict['withdrawal'])
            process_obj.chargeback = 0.00
            process_obj.void = 0.00
            process_obj.reversechargeback = 0.00
            process_obj.bonus = float(row_dict['adjustments'])
            process_obj.expiredbonus = 0.00
            process_obj.sidegamesbets = 0.00
            process_obj.sidegameswins = 0.00
            process_obj.jackpotcontribution = 0.00
            process_obj.processed = True
            process_obj.revenue = float(float(row_dict['deposits']) - float(row_dict['withdrawal']))
            process_obj.save()
    except Exception as e:
        print 'Exception in file upload ', str(e)
        msg = EmailMessage('Proprietary', str(e), 
                "support@wynta.com", ['shravan.bitla@gridlogic.in'])
        msg.send()
        return 'ERR'
    return 'PRO'

    # for chunk in f.chunks():
    #     destination.write(chunk)

import csv

def handle_jumpmansftp_file(f, date):
    # networkid = request.POST.get('networkid')
    try:
        ext = os.path.splitext(f)[1]  # [0] returns path+filename
        site_name = f.split('_')[0]
        valid_extensions = ['.csv', '.xls']
        site_dict = {i.name.replace(' ', '').lower(): i.name for i in Site.objects.all()}
        if not ext.lower() in valid_extensions:
            raise ValidationError(u'Unsupported file extension.')
        dict_list = []
        with open('/sftp/shared/'+site_name+'/'+f, 'rb') as csvfile:
            # creating a csv reader object
            reader = csv.DictReader(csvfile)
            for line in reader:
                dict_list.append(line)

            # csvreader = csv.reader(csvfile)
     
            # # extracting field names through first row
            # fields = csvreader.next()
         
            # # extracting each data row one by one
            # for row in csvreader:
            #     rows.append(row)
        site_name = site_dict[site_name]
        currency = ''
        site_obj = Site.objects.get(name=site_name)
        currency_obj = Currency.objects.filter(siteid=site_obj)
        if currency_obj:
            currency = currency_obj[0].currency
        site = site_obj.domain
        from_d = datetime.strptime(date, "%Y-%m-%d")
        minus_30_file = from_d - timedelta(days=30)
        json_dump = 'media/json_dump/%s-%s-%s.json'%(site_obj.id,str(from_d).split(' ')[0], f)
        if os.path.isfile(json_dump):
            os.remove(json_dump)
        with open(json_dump, 'w+') as outfile:
            json.dump(dict_list, outfile, sort_keys = True, indent = 4,
                   ensure_ascii = False)
        minus_30_file = from_d - timedelta(days=30)
        json_minus_30_dump = 'media/json_dump/%s-%s-%s.json'%(site_obj.id,str(minus_30_file).split(' ')[0], f)
        if os.path.isfile(json_minus_30_dump):
            os.remove(json_minus_30_dump)
        if 'REG' in f:
            # import pdb;pdb.set_trace()
            for row_dict in dict_list:
                # if row_dict['siteid'] in site_dict:
                camp_track_map = None
                playerid = row_dict['PLAYER_ID'].strip()
                name = row_dict['USERNAME'].strip()
                country = row_dict['PLAYER_COUNTRY'].strip()
                try:
                    country = pycountry.countries.get(alpha_2=country).name
                except:
                    country = row_dict['PLAYER_COUNTRY']
                btag = str(row_dict['BTAG'].strip())
                d_aff_obj = None
                if not btag or str(btag)=='0':
                    d_aff_obj = Affiliateinfo.objects.filter(siteid=site_obj).order_by('id')[0]
                    btag = 0
                clickid = str(btag)
                w_dynamic = None
                platform = 'Web'
                click_obj = PostBackClicks.objects.filter(clickid=clickid)
                if click_obj:
                    camp_track_obj = click_obj[0].fk_campaign
                    platform = click_obj[0].platform
                    w_dynamic = click_obj[0].wdynamic if click_obj[0].wdynamic else None
                elif clickid.isdigit():
                    camp_id = int(btag)
                    camp_track_map = CampaignTrackerMapping.objects.filter(trackerid=camp_id,
                                                fk_campaign__siteid=site_obj, goalid=3)
                    if d_aff_obj:
                        camp_track_map = CampaignTrackerMapping.objects.filter(
                                                fk_affiliateinfo=d_aff_obj,
                                                fk_campaign__siteid=site_obj, goalid=3)
                    if not camp_track_map:
                        continue
                    else:
                        camp_track_obj = camp_track_map[0]
                else:
                    continue

                process_obj = PPPlayerProcessed.objects.filter(playerid=playerid,
                                        siteid = site_obj,
                                        date=date
                                        )
                if len(process_obj) == 0:
                    process_obj = PPPlayerProcessed.objects.create(playerid=playerid,
                                            fk_campaign = camp_track_obj,
                                            siteid = site_obj,
                                            registrationdate = date,
                                            registrations = 1,
                                            platform = platform,
                                            currency =currency,
                                            dynamic = btag,
                                            country = country,
                                            clickid = btag,
                                            date=date
                                            )
                else:
                    process_obj = process_obj[0]
                    process_obj.registrationdate = date
                    # process_obj.lastdate=date
                    process_obj.registrations = 1
                    process_obj.dynamic = btag
                    process_obj.country = country
                process_obj.wdynamic = w_dynamic
                process_obj.save()
        else:
            for row_dict in dict_list:
                # if row_dict['siteid'] in site_dict:
                # import pdb;pdb.set_trace()
                camp_track_map = None
                playerid = row_dict['PLAYER_ID'].strip()
                btag = str(row_dict['BTAG'].strip())
                deposit = float(row_dict['DEPOSITS'].strip())
                revenue = float(row_dict['NGR'].strip())
                cashout = (revenue - deposit)*-1
                # sidegamesbets = row_dict['SIDE_GAMES_#_OF_BETS']
                sidegamesbets = float(row_dict['SIDE_GAMES_STAKE'].strip())
                bonuses = float(row_dict['SIDE_GAMES_BONUS'].strip())
                d_aff_obj = None
                if not btag or str(btag)=='0':
                    d_aff_obj = Affiliateinfo.objects.filter(siteid=site_obj).order_by('id')[0]
                    btag = 0
                clickid = str(btag)
                w_dynamic = None
                platform = 'Web'
                click_obj = PostBackClicks.objects.filter(clickid=clickid)
                if click_obj:
                    camp_track_obj = click_obj[0].fk_campaign
                    platform = click_obj[0].platform
                    w_dynamic = click_obj[0].wdynamic if click_obj[0].wdynamic else None
                elif clickid.isdigit():
                    camp_id = int(btag)
                    camp_track_map = CampaignTrackerMapping.objects.filter(trackerid=camp_id,
                                                fk_campaign__siteid=site_obj, goalid=3)
                    if d_aff_obj:
                        camp_track_map = CampaignTrackerMapping.objects.filter(
                                                fk_affiliateinfo=d_aff_obj,
                                                fk_campaign__siteid=site_obj, goalid=3)
                    if not camp_track_map:
                        continue
                    else:
                        camp_track_obj = camp_track_map[0]
                else:
                    continue
                # import pdb;pdb.set_trace()
                rev_percent = camp_track_obj.fk_affiliateinfo.account_manager.rev_percent_cut
                playercheck = PlayerCheck.objects.filter(playerid=playerid,
                                                            siteid=site_obj)
                if playercheck:
                    if str(playercheck[0].date) < date:
                        camp_track_obj = playercheck[0].ck_campaign
                        process_obj = PPPlayerProcessed.objects.filter(playerid=playerid,
                                        siteid = site_obj,
                                        date=date
                                        )
                        if process_obj:
                            process_obj.delete()
                process_objs = PPPlayerProcessed.objects.filter(playerid=playerid,
                                        siteid = site_obj,
                                        registrationdate__isnull=False
                                        )
                ftd_objs = PPPlayerProcessed.objects.filter(playerid=playerid,
                                        siteid = site_obj,
                                        ftddate__isnull=False
                                        ).order_by('ftddate')
                # if len(process_obj) == 0:
                #     process_obj = PPPlayerProcessed.objects.create(playerid=playerid,
                #                             fk_campaign = camp_track_obj,
                #                             siteid = site_obj,
                #                             lastdate=date,
                #                             deposit = deposit,
                #                             cashout = cashout,
                #                             platform = 'Desktop',
                #                             bonuses = bonuses,
                #                             revenue = (revenue - (revenue*0.10)) if revenue else 0.0,
                #                             sidegamesbets = sidegamesbets,
                #                             date=date
                #                             )
                #     if deposit > 0:
                #         process_obj.ftddate = date
                if len(process_objs) > 0:
                    process_obj = PPPlayerProcessed.objects.filter(playerid=playerid,
                                        siteid = site_obj,
                                        date=date
                                        )
                    if len(process_obj) == 0:
                        # import pdb;pdb.set_trace()
                        process_obj = PPPlayerProcessed.objects.create(playerid=playerid,
                                            fk_campaign = camp_track_obj,
                                            siteid = site_obj,
                                            lastdate=date,
                                            deposit = deposit,
                                            cashout = cashout,
                                            platform = platform,
                                            bonuses = bonuses,
                                            revenue = (revenue - (revenue*rev_percent)) if revenue else 0.0,
                                            sidegamesbets = sidegamesbets,
                                            date=date
                                            )
                        if deposit > 0 and not ftd_objs:
                            process_obj.ftddate = date
                        elif ftd_objs:
                            process_obj.ftddate = ftd_objs[0].ftddate
                        if process_objs[0].registrationdate:
                            process_obj.registrationdate = process_objs[0].registrationdate
                        if process_objs[0].country:
                            process_obj.country = process_objs[0].country
                    else:
                        # import pdb;pdb.set_trace()
                        process_obj = process_obj[0]
                        if deposit > 0 and not ftd_objs:
                            # import pdb;pdb.set_trace()
                            process_obj.ftddate = date
                        elif ftd_objs:
                            # import pdb;pdb.set_trace()
                            process_obj.ftddate = ftd_objs[0].ftddate
                        process_obj.lastdate=date
                        process_obj.deposit = deposit
                        process_obj.cashout = cashout
                        process_obj.bonuses = bonuses
                        process_obj.revenue = (revenue - (revenue*rev_percent)) if revenue else 0.0
                        process_obj.sidegamesbets = sidegamesbets
                        if process_objs[0].country:
                            process_obj.country = process_objs[0].country
                    process_obj.wdynamic = w_dynamic
                    process_obj.save()
                else:
                    if ftd_objs:
                        # import pdb;pdb.set_trace()
                        ftd_objs.update(ftddate=None)
                    process_obj = PPPlayerProcessed.objects.filter(playerid=playerid,
                                        siteid = site_obj,
                                        date=date
                                        )
                    if len(process_obj) == 0:
                        process_obj = PPPlayerProcessed.objects.create(playerid=playerid,
                                            fk_campaign = camp_track_obj,
                                            siteid = site_obj,
                                            lastdate=date,
                                            deposit = deposit,
                                            cashout = cashout,
                                            platform = platform,
                                            bonuses = bonuses,
                                            revenue = (revenue - (revenue*rev_percent)) if revenue else 0.0,
                                            sidegamesbets = sidegamesbets,
                                            date=date
                                            )
                    else:
                        process_obj = process_obj[0]
                        process_obj.lastdate=date
                        process_obj.deposit = deposit
                        process_obj.cashout = cashout
                        process_obj.bonuses = bonuses
                        process_obj.revenue = (revenue - (revenue*rev_percent)) if revenue else 0.0
                        process_obj.sidegamesbets = sidegamesbets
                    process_obj.wdynamic = w_dynamic
                    process_obj.save()

    except Exception as e:
        print 'Exception in file upload ', str(e)
        msg = EmailMessage('Jumpman', str(e), 
                "support@wynta.com", ['shravan.bitla@gridlogic.in'])
        msg.send()
        return 'ERR'
    return 'PRO'

def handle_nektansftp_file(f, site_name, date):
    # networkid = request.POST.get('networkid')
    try:
        ext = os.path.splitext(f)[1]  # [0] returns path+filename
        # site_name = f.split('_')[0]
        valid_extensions = ['.tsv', '.csv']
        site_dict = {i.name.replace(' ', '').lower(): i.name for i in Site.objects.all()}
        if not ext.lower() in valid_extensions:
            raise ValidationError(u'Unsupported file extension.')
        dict_list = []
        with open('/sftp_necktan/shared/'+site_name+'/'+f, 'rb') as csvfile:
        # with open('files/Fortune Cat Casino/'+f, 'rb') as csvfile:
            # creating a csv reader object
            reader = csv.DictReader(csvfile, delimiter='\t')
            for line in reader:
                dict_list.append(line)

            # csvreader = csv.reader(csvfile)
     
            # # extracting field names through first row
            # fields = csvreader.next()
         
            # # extracting each data row one by one
            # for row in csvreader:
            #     rows.append(row)
        site_name = site_dict[site_name]
        currency = ''
        site_obj = Site.objects.get(name=site_name)
        currency_obj = Currency.objects.filter(siteid=site_obj)
        if currency_obj:
            currency = currency_obj[0].currency
        site = site_obj.domain
        if 'REG' in f:
            for row_dict in dict_list:
                # if row_dict['siteid'] in site_dict:
                camp_track_map = None
                playerid = row_dict['PLAYER_ID']
                name = row_dict['USERNAME']
                country = row_dict['PLAYER_COUNTRY']
                try:
                    country = pycountry.countries.get(alpha_2=country).name
                except:
                    country = row_dict['PLAYER_COUNTRY']
                btag = str(row_dict['BTAG'])
                if not btag:
                    btag = 1
                clickid = str(btag)
                w_dynamic = None
                click_obj = PostBackClicks.objects.filter(clickid=clickid)
                if click_obj:
                    camp_track_obj = click_obj[0].fk_campaign
                    w_dynamic = click_obj[0].wdynamic if click_obj[0].wdynamic else None
                elif clickid.isdigit():
                    camp_id = int(btag)
                    camp_track_map = CampaignTrackerMapping.objects.filter(trackerid=camp_id,
                                                fk_campaign__siteid=site_obj, goalid=3)
                    if not camp_track_map:
                        continue
                    else:
                        camp_track_obj = camp_track_map[0]
                else:
                    continue
                process_obj = PPPlayerProcessed.objects.filter(playerid=playerid,
                                        siteid = site_obj,
                                        date=date
                                        )
                if len(process_obj) == 0:
                    process_obj = PPPlayerProcessed.objects.create(playerid=playerid,
                                            fk_campaign = camp_track_obj,
                                            siteid = site_obj,
                                            registrationdate = date,
                                            registrations = 1,
                                            platform = 'Desktop',
                                            currency =currency,
                                            dynamic = btag,
                                            country = country,
                                            clickid = btag,
                                            date=date
                                            )
                else:
                    process_obj = process_obj[0]
                    process_obj.registrationdate = date
                    process_obj.lastdate=date
                    process_obj.registrations = 1
                    process_obj.dynamic = btag
                    process_obj.country = country
                process_obj.wdynamic = w_dynamic
                process_obj.save()
        else:
            for row_dict in dict_list:
                # if row_dict['siteid'] in site_dict:
                camp_track_map = None
                playerid = row_dict['PLAYER_ID']
                btag = str(row_dict['BTAG'])
                deposit = float(row_dict['DEPOSITS'])
                revenue = float(row_dict['CASINO_REVENUE'])
                cashout = float(row_dict['WITHDRAWALS'])
                chargeback = row_dict['CHARGEBACKS']
                sidegameswins = float(row_dict['CASINO_RETURNS'])
                sidegamesbets = float(row_dict['CASINO_WAGERED'])
                bonuses = float(row_dict.get('BONUSES', 0))
                if not btag:
                    btag = 1
                clickid = str(btag)
                w_dynamic = None
                click_obj = PostBackClicks.objects.filter(clickid=clickid)
                if click_obj:
                    camp_track_obj = click_obj[0].fk_campaign
                    w_dynamic = click_obj[0].wdynamic if click_obj[0].wdynamic else None
                elif clickid.isdigit():
                    camp_id = int(btag)
                    camp_track_map = CampaignTrackerMapping.objects.filter(trackerid=camp_id,
                                                fk_campaign__siteid=site_obj, goalid=3)
                    if not camp_track_map:
                        continue
                    else:
                        camp_track_obj = camp_track_map[0]
                else:
                    continue
                playercheck = PlayerCheck.objects.filter(playerid=playerid,
                                                            siteid=site_obj)
                if playercheck:
                    if playercheck[0].date < date:
                        camp_track_obj = playercheck[0].ck_campaign
                        process_obj = PPPlayerProcessed.objects.filter(playerid=playerid,
                                        siteid = site_obj,
                                        date=date
                                        )
                        if process_obj:
                            process_obj.delete()
                process_objs = PPPlayerProcessed.objects.filter(playerid=playerid,
                                        siteid = site_obj,
                                        )
                ftd_objs = PPPlayerProcessed.objects.filter(playerid=playerid,
                                        siteid = site_obj,
                                        ftddate__isnull=False
                                        ).order_by('ftddate')
                if len(process_objs) > 0:
                    process_obj = PPPlayerProcessed.objects.filter(playerid=playerid,
                                        siteid = site_obj,
                                        date=date
                                        )
                    if len(process_obj) == 0:
                        process_obj = PPPlayerProcessed.objects.create(playerid=playerid,
                                            fk_campaign = camp_track_obj,
                                            siteid = site_obj,
                                            lastdate=date,
                                            deposit = deposit,
                                            chargeback = chargeback,
                                            cashout = cashout,
                                            platform = 'Desktop',
                                            bonuses = bonuses,
                                            revenue = (revenue - abs(revenue*0.10)) if revenue else 0.0,
                                            sidegamesbets = sidegamesbets,
                                            sidegameswins = sidegameswins,
                                            date=date
                                            )
                        if deposit > 0 and not ftd_objs:
                            process_obj.ftddate = date
                        elif ftd_objs:
                            process_obj.ftddate = ftd_objs[0].ftddate
                        if process_objs[0].registrationdate:
                            process_obj.registrationdate = process_objs[0].registrationdate
                        if process_objs[0].country:
                            process_obj.country = process_objs[0].country
                    else:
                        process_obj = process_obj[0]
                        if deposit > 0 and not ftd_objs:
                            process_obj.ftddate = date
                        elif ftd_objs:
                            process_obj.ftddate = ftd_objs[0].ftddate
                        process_obj.chargeback = chargeback
                        process_obj.lastdate = date
                        process_obj.deposit = deposit
                        process_obj.cashout = cashout
                        process_obj.bonuses = bonuses
                        process_obj.revenue = (revenue - abs(revenue*0.10)) if revenue else 0.0
                        process_obj.sidegamesbets = sidegamesbets
                        process_obj.sidegameswins = sidegameswins
                        if process_objs[0].country:
                            process_obj.country = process_objs[0].country
                    process_obj.wdynamic = w_dynamic
                    process_obj.save()
                else:
                    process_obj = PPPlayerProcessed.objects.filter(playerid=playerid,
                                        siteid = site_obj,
                                        date=date
                                        )
                    if len(process_obj) == 0:
                        process_obj = PPPlayerProcessed.objects.create(playerid=playerid,
                                            fk_campaign = camp_track_obj,
                                            siteid = site_obj,
                                            lastdate=date,
                                            deposit = deposit,
                                            chargeback = chargeback,
                                            cashout = cashout,
                                            platform = 'Desktop',
                                            bonuses = bonuses,
                                            revenue = (revenue - abs(revenue*0.10)) if revenue else 0.0,
                                            sidegamesbets = sidegamesbets,
                                            sidegameswins = sidegameswins,
                                            date=date
                                            )
                    else:
                        process_obj = process_obj[0]
                        process_obj.lastdate=date
                        process_obj.chargeback = chargeback
                        process_obj.deposit = deposit
                        process_obj.cashout = cashout
                        process_obj.bonuses = bonuses
                        process_obj.revenue = (revenue - abs(revenue*0.10)) if revenue else 0.0
                        process_obj.sidegamesbets = sidegamesbets
                        process_obj.sidegameswins = sidegameswins
                    process_obj.wdynamic = w_dynamic
                    process_obj.save()

    except Exception as e:
        print 'Exception in file upload ', str(e)
        msg = EmailMessage('Nektan', str(e), 
                "support@wynta.com", ['shravan.bitla@gridlogic.in'])
        msg.send()
        return 'ERR'
    return 'PRO'


class YayaConnector(View):
    call_dict = {
        # 'player_games': "https://reports.api.progressplay.com/v20/api/data/v20/playergames",
        'player_data_url': "/affiliates/get-transactions-data",
        'player_personal_data_url': "/affiliates/get-players-data"
        }

    def store_player_personal_data(self, **kwargs):
        site_obj = kwargs['site_obj']
        site_obj = site_obj.siteid
        source_data = kwargs['source_data']
        day_stop = kwargs['day_stop']
        day_start = kwargs['day_start']
        call_type = kwargs['call_type']
        currency_obj = Currency.objects.filter(siteid=site_obj)
        if currency_obj:
            currency = currency_obj[0].currency
        # TODO: Make it more efficient to use bulk_create.
        # for objective in source_data:
        #import pdb;pdb.set_trace()
        print source_data.text
        source_data = json.loads(source_data.text)
        minus_30_file = day_start - timedelta(days=30)
        json_dump = 'media/json_dump/%s-%s.json'%(site_obj.id,str(day_start).split(' ')[0])
        if os.path.isfile(json_dump):
            os.remove(json_dump)
        with open(json_dump, 'w+') as outfile:
            json.dump(source_data, outfile, sort_keys = True, indent = 4,
                   ensure_ascii = False)
        minus_30_file = day_start - timedelta(days=30)
        json_minus_30_dump = 'media/json_dump/%s-%s.json'%(site_obj.id,str(minus_30_file).split(' ')[0])
        if os.path.isfile(json_minus_30_dump):
            os.remove(json_minus_30_dump)
        if call_type == 'player_personal_data_url':
            for raw_data_row in source_data:
                row = raw_data_row
                btag = row['btag']
                del row['btag']
                clickid = str(btag)
                w_dynamic = None
                click_obj = PostBackClicks.objects.filter(clickid=clickid)
                if click_obj:
                    camp_track_obj = click_obj[0].fk_campaign
                    w_dynamic = click_obj[0].wdynamic if click_obj[0].wdynamic else None
                else:
                    continue
                row['playerid'] = row['id']
                playerid = row['playerid']
                del row['id']
                date_d = day_start.date()
                row['date'] = str(date_d)
                row['registereddate'] = str(date_d)
                row['siteid'] = site_obj
                row['registeredplatform'] = click_obj[0].platform
                row['country'] = click_obj[0].country
                row['alias'] = row['username']
                del row['name']
                del row['username']
                transactions = row['transactions']
                deposit = float(transactions.get('deposited', 0))
                cashout = float(transactions.get('withdrawals', 0))
                chargeback = float(transactions.get('refunded', 0))
                rev_percent = camp_track_obj.fk_affiliateinfo.account_manager.rev_percent_cut
                revenue = deposit - cashout
                revenue = (revenue - abs(revenue*rev_percent)) if revenue else 0.0
                del row['transactions']
                try:
                    with transaction.atomic():
                        pmc_p = PMCPlayerPersonalDetails.objects.filter(
                                    playerid=row['playerid'])
                        if pmc_p:
                            for attr, value in row.iteritems(): 
                                setattr(pmc_p[0], attr, value)
                                pmc_p[0].save()
                        else:
                            pmc_p = PMCPlayerPersonalDetails.objects.create(**row)
                        process_obj = PPPlayerProcessed.objects.filter(playerid=playerid,
                                        siteid = site_obj,
                                        date=date_d
                                                )
                        if len(process_obj) == 0:
                            process_obj = PPPlayerProcessed.objects.create(playerid=playerid,
                                                    fk_campaign = camp_track_obj,
                                                    siteid = site_obj,
                                                    registrationdate = date_d,
                                                    registrations = 1,
                                                    platform = click_obj[0].platform,
                                                    currency =currency,
                                                    dynamic = btag,
                                                    country = click_obj[0].country,
                                                    clickid = btag,
                                                    date=date_d
                                                    )
                        else:
                            process_obj = process_obj[0]
                            process_obj.registrationdate = date_d
                            process_obj.lastdate=date_d
                            process_obj.registrations = 1
                            process_obj.dynamic = btag
                            process_obj.country = click_obj[0].country
                        if deposit > 0 and not process_obj.ftddate:
                            #process_obj.deposit = deposit
                            process_obj.ftddate = date_d
                            pmc_p.firstdepositdate = date_d
                            pmc_p.lastdepositdate = date_d
                        if deposit > 0:
                            process_obj.deposit = deposit
                        if cashout > 0:
                            process_obj.cashout = cashout
                        if revenue > 0:
                            process_obj.revenue = revenue
                        process_obj.wdynamic = w_dynamic
                        process_obj.save()
                except Exception as e:
                    self.save_audit('STG', 'Fail', day_stop, day_start, e)
                    return False
        else:
            for raw_data_row in source_data:
                # import pdb;pdb.set_trace()
                row = raw_data_row #self.convert_keys_to_string(raw_data_row)
                btag = row['btag']
                clickid = str(btag)
                w_dynamic = None
                click_obj = PostBackClicks.objects.filter(clickid=clickid)
                if click_obj:
                    camp_track_obj = click_obj[0].fk_campaign
                    w_dynamic = click_obj[0].wdynamic if click_obj[0].wdynamic else None
                else:
                    continue
                rev_percent = camp_track_obj.fk_affiliateinfo.account_manager.rev_percent_cut
                row['playerid'] = row['player_id']
                playerid = row['playerid']
                del row['player_id']
                date_d = day_start.date()
                row['date'] = str(date_d)
                row['siteid'] = site_obj
                deposit = float(row['deposited'])
                cashout = float(row['withdrawals'])
                revenue = row['deposited'] - row['withdrawals']
                revenue = (revenue - (revenue*rev_percent)) if revenue else 0.0
                chargeback = float(row['refunded'])
                bonus = float(row['bonus'])
                wins = float(row['win'])
                bets = row['win'] + row['lost']
                try:
                    with transaction.atomic():
                        pmc_p = PMCPlayerPersonalDetails.objects.filter(
                                    playerid=row['playerid'])
                        if not pmc_p:
                            continue
                        pmc_p = pmc_p[0]
                        if deposit > 0:
                            if not pmc_p.firstdepositdate:
                                pmc_p.firstdepositdate = date_d
                            pmc_p.lastdepositdate = date_d
                        pmc_p.lastlogindate = date_d
                        pmc_p.lastupdated = date_d
                        process_total_obj = PPPlayerProcessed.objects.filter(playerid=playerid,
                                        siteid = site_obj
                                        )
                        process_obj = PPPlayerProcessed.objects.filter(playerid=playerid,
                                        siteid = site_obj,
                                        date=date_d
                                                )
                        if len(process_obj) == 0 and process_total_obj:
                            process_obj = PPPlayerProcessed.objects.create(playerid=playerid,
                                                    fk_campaign = camp_track_obj,
                                                    siteid = site_obj,
                                                    registrationdate = process_total_obj[0].registrationdate,
                                                    registrations = 1,
                                                    platform = process_total_obj[0].platform,
                                                    currency = process_total_obj[0].currency,
                                                    dynamic = process_total_obj[0].clickid,
                                                    country = process_total_obj[0].country,
                                                    clickid = process_total_obj[0].clickid,
                                                    date=date_d
                                                    )
                        else:
                            process_obj = process_obj[0]
                        process_obj.lastdate=date_d
                        if deposit > 0:
                            if not process_total_obj[0].ftddate:
                                process_obj.ftddate = date_d
                                process_total_obj.update(ftddate=date_d)
                            process_obj.deposit = deposit
                        if cashout > 0:
                            process_obj.cashout = cashout
                        if revenue > 0:
                            process_obj.revenue = revenue
                        if chargeback > 0:
                            process_obj.chargeback = chargeback
                        if bonus > 0:
                            process_obj.bonuses = bonus
                        if bets > 0:
                            process_obj.sidegamesbets = bets
                        if wins > 0:
                            process_obj.sidegameswins = wins
                        process_obj.wdynamic = w_dynamic
                        process_obj.save()
                except Exception as e:
                    self.save_audit('STG', 'Fail', day_stop, day_start, e)
                    return False
        return True


    def get_per_chunks(self, **kwargs):
        import datetime as dttt
        day_stop = kwargs['day_stop']
        day_start = kwargs['day_start']
        chunk_size = kwargs['chunk_size']
        end_date = day_stop
        start_date = day_start
        period = (end_date-start_date).days
        print(period)
        until = day_stop
        if period % chunk_size == 0:
            chunks = []
            for i in range(period/chunk_size):
                chunks.append(until)
                until = until - dttt.timedelta(1)
        else:
            chunks = []
            for i in range(period/chunk_size):
                chunks.append(until)
                until = until - dttt.timedelta(1)
            until = until-dttt.timedelta(period % chunk_size)
            chunks.append(until)
        return chunks

    def save_audit(self, job, status, day_stop, day_start, e):
        nt_obj = Network.objects.get(networkname='YayaGaming')
        if day_start:
            audit = AuditTable.objects.create(job=job,
                datasource=nt_obj,
                status=status,
                end=day_stop, start=day_start,
                error_msg=str(e)
            )

        else:
            audit = AuditTable.objects.create(job=job, datasource=nt_obj,
                status=status,
                error_msg=str(e)
            )
        audit.save()
        if status == 'Fail':
            # msg = EmailMessage(nt_obj.networkname + ': '+ job, str(e), 
            #     "support@wynta.com", ['shravan.bitla@gridlogic.in'])
            # msg.send()
            pass

    @transaction.atomic
    def retrieve_players_data(self, *args, **kwargs):
        """
        transaction.atomic() is used with for every create to catch the exception.
        It does not roll back only that row but all the rows that are inserted.
        As we have mentioned decorator on top it take cares of all the transactions
        not just one which is throwing exception.
        """
        day_stop = kwargs['day_stop']
        day_start = kwargs['day_start']
        call_type = kwargs['call_type']
        site_obj = kwargs['site_obj']
        network_obj = Network.objects.get(networkname='YayaGaming')
        network_ap_mapping = NetworkAffiliateProgrammeMapping.objects.filter(
                            networkid=network_obj,
                            siteid=site_obj.siteid)
        if not network_ap_mapping or len(network_ap_mapping)>1:
            return False
        self.username = str(network_ap_mapping[0].username)
        self.password = str(network_ap_mapping[0].password)
        # import pdb;pdb.set_trace()
        #data = base64.b64decode(encoded) 
        Whitelabel_id = site_obj.whitelabelid
        try:
            with transaction.atomic():
                url = 'https://'+ site_obj.siteid.domain +self.call_dict[call_type]
                chunks = self.get_per_chunks(day_stop=day_stop, day_start=day_start,chunk_size=1)
                for chunk in chunks:
                    p_payload = { 'login': self.username,
                                  'password': self.password,
                                'date_from': chunk.strftime('%Y-%m-%d'), 
                                'date_to': chunk.strftime('%Y-%m-%d'),
                                'currency': 'EUR' }
                    report = requests.post(url, params=p_payload)
                    if report:
                        self.save_audit(
                            'RET', 'Success', None,
                            None, 'Retrieving Done'
                        )
                    else:
                        self.save_audit(
                            'RET', 'Fail', None,
                            None, str(report.text)+ ' Error in Retrieving'
                        )
                        return False
                    stored = self.store_player_personal_data(site_obj=site_obj, 
                            source_data=report,
                             day_stop=chunk, day_start=chunk, call_type=call_type)
                    if not stored:
                        pass
                        # return False
                return [day_start, day_stop]
        except Exception as e:
            self.save_audit(
                'STG', 'Fail', day_stop,
                day_start, e
            )
            return False


import urllib2, httplib, socket
from suds.client import Client
from suds.transport.http import HttpTransport, Reply, TransportError
import ast

class HTTPSClientAuthHandler(urllib2.HTTPSHandler):
    def __init__(self, key, cert):
        urllib2.HTTPSHandler.__init__(self)
        self.key = key
        self.cert = cert

    def https_open(self, req):
        #Rather than pass in a reference to a connection class, we pass in
        # a reference to a function which, for all intents and purposes,
        # will behave as a constructor
        return self.do_open(self.getConnection, req)

    def getConnection(self, host, timeout=300):
        return httplib.HTTPSConnection(host,
                                       key_file=self.key,
                                       cert_file=self.cert)

class HTTPSClientCertTransport(HttpTransport):
    def __init__(self, key, cert, *args, **kwargs):
        HttpTransport.__init__(self, *args, **kwargs)
        self.key = key
        self.cert = cert

    def u2open(self, u2request):
        """
        Open a connection.
        @param u2request: A urllib2 request.
        @type u2request: urllib2.Requet.
        @return: The opened file-like urllib2 object.
        @rtype: fp
        """
        tm = self.options.timeout
        url = urllib2.build_opener(HTTPSClientAuthHandler(self.key, self.cert))
        if self.u2ver() < 2.6:
            socket.setdefaulttimeout(tm)
            return url.open(u2request)
        else:
            return url.open(u2request, timeout=tm)

class DragonFishConnector(View):

    url =  "https://xgw.888.com:444/AffiliateService/AffiliateService.svc?wsdl"

    device_dict = {1:'Mobile', 2:'Tablet', 3:'Web'}

    active_link = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/" xmlns:rl="http://schemas.datacontract.org/2004/07/RL.BingoIntegrationServer.AffiliateService.DataContract" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays">

               <soapenv:Header/>

               <soapenv:Body>

                  <tem:GetRegistrationsSummary>

                     <!--Optional:-->

                     <tem:request>

                        <rl:AffiliatePlatformID>11</rl:AffiliatePlatformID>

                        <rl:NetworkID>1</rl:NetworkID>

                        <rl:RegistrationsDate>2018-08-25</rl:RegistrationsDate>

                        <rl:RequestDate>2019-04-18T00:00:00.000+03:00</rl:RequestDate>

                        <rl:ServerID>1</rl:ServerID>

                        <!--Optional:-->

                        <rl:SkinIDList>

                           <!--Zero or more repetitions:-->

                           <arr:int>2767</arr:int>

                        </rl:SkinIDList>

                     </tem:request>

                  </tem:GetRegistrationsSummary>

               </soapenv:Body>

            </soapenv:Envelope>"""
    content = {'registration_data_url': """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/" xmlns:rl="http://schemas.datacontract.org/2004/07/RL.BingoIntegrationServer.AffiliateService.DataContract" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays">

               <soapenv:Header/>

               <soapenv:Body>

                  <tem:GetRegistrationsSummary>

                     <!--Optional:-->

                     <tem:request>

                        <rl:AffiliatePlatformID>%s</rl:AffiliatePlatformID>

                        <rl:NetworkID>1</rl:NetworkID>

                        <rl:RegistrationsDate>%s</rl:RegistrationsDate>

                        <rl:RequestDate>%sT00:00:00.000+03:00</rl:RequestDate>

                        <rl:ServerID>1</rl:ServerID>

                        <!--Optional:-->

                        <rl:SkinIDList>

                           <!--Zero or more repetitions:-->

                           <arr:int>%s</arr:int>

                        </rl:SkinIDList>

                     </tem:request>

                  </tem:GetRegistrationsSummary>

               </soapenv:Body>

            </soapenv:Envelope>""",
            'player_data_url':"""<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/" xmlns:rl="http://schemas.datacontract.org/2004/07/RL.BingoIntegrationServer.AffiliateService.DataContract" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays">
                   <soapenv:Header/>
                   <soapenv:Body>
                      <tem:GetActivitySummary>
                         <!--Optional:-->
                         <tem:request>
                            <rl:ActivityDate>%s</rl:ActivityDate>
                            <rl:AffiliatePlatformID>%s</rl:AffiliatePlatformID>
                            
                            <rl:NetworkID>1</rl:NetworkID>
                            <rl:RequestDate>%sT00:00:00.000+03:00</rl:RequestDate>
                            <rl:ServerID>1</rl:ServerID>
                            <!--Optional:-->
                            <rl:SkinIDList>
                               <!--Zero or more repetitions:-->
                               <arr:int>%s</arr:int>
                            </rl:SkinIDList>
                         </tem:request>
                      </tem:GetActivitySummary>
                   </soapenv:Body>
                </soapenv:Envelope>"""
            }
    #'player_data_url': '<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/" xmlns:rl="http://schemas.datacontract.org/2004/07/RL.BingoIntegrationServer.AffiliateService.DataContract" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays"><soapenv:Header/><soapenv:Body><tem:GetActivitySummary>   <!--Optional:-->   <tem:request>      <rl:ActivityDate>%s</rl:ActivityDate>      <rl:AffiliatePlatformID>11</rl:AffiliatePlatformID>      <rl:NetworkID>1</rl:NetworkID>      <rl:RequestDate>%sT00:00:00.000+03:00</rl:RequestDate>      <rl:ServerID>1</rl:ServerID>      <!--Optional:-->      <rl:SkinIDList>         <!--Zero or more repetitions:-->        <arr:int>2802</arr:int><arr:int>2803</arr:int> <arr:int>%s</arr:int>     </rl:SkinIDList>   </tem:request></tem:GetActivitySummary></soapenv:Body></soapenv:Envelope>'

    def channel(self):
        return 'Dragonfish'

    def __init__(self, *args, **kwargs):
        pass

    def dtobj_to_datestring(self, dt):
        return dt.strftime('%Y-%m-%d')

    def str2date(self, str):
        return datetime.datetime.strptime(str, '%Y-%m-%d')

    def get_per_chunks(self, **kwargs):
        import datetime as dttt
        # import pdb;pdb.set_trace()
        day_stop = kwargs['day_stop']
        day_start = kwargs['day_start']
        chunk_size = kwargs['chunk_size']
        end_date = day_stop
        start_date = day_start
        period = (end_date-start_date).days
        print(period)
        until = day_stop
        if period % chunk_size == 0:
            chunks = []
            if chunk_size == -1:
                for i in range(period/chunk_size, 1):
                    chunks.append(start_date)
                    start_date = start_date + dttt.timedelta(1)
            else:
                for i in range(period/chunk_size):
                    chunks.append(until)
                    until = until - dttt.timedelta(chunk_size)
        else:
            chunks = []
            for i in range(period/chunk_size):
                chunks.append(until)
                until = until - dttt.timedelta(1)
            until = until-dttt.timedelta(period % chunk_size)
            chunks.append(until)
        return chunks


    def save_audit(self, job, status, day_stop, day_start, e):
        nt_obj = Network.objects.get(networkname='Dragonfish')
        if day_start:
            audit = AuditTable.objects.create(job=job,
                datasource=nt_obj,
                status=status,
                end=day_stop, start=day_start,
                error_msg=str(e)
            )

        else:
            audit = AuditTable.objects.create(job=job, datasource=nt_obj,
                status=status,
                error_msg=str(e)
            )
        audit.save()
        if status == 'Fail':
            msg = EmailMessage(nt_obj.networkname + ': '+ job, str(e), 
                "support@wynta.com", ['shravan.bitla@gridlogic.in'])
            msg.send()

    def convert_keys_to_string(self, dictionary):
        """Recursively converts dictionary keys to strings."""
        if not isinstance(dictionary, dict):
            if not isinstance(dictionary, int) and not isinstance(dictionary, float):
                try:
                    return str(dictionary).replace('b:','')
                except:
                    return str(dictionary.encode('utf-8')).replace('b:','')
            else:
                return dictionary
        return dict((str(k).lower().replace('b:',''), self.convert_keys_to_string(v)) 
            for k, v in dictionary.items())

    def get_map_details(self, obj, siteobj, btag):
        aff_id = None
        # import pdb;pdb.set_trace()
        if 'ga' in btag:
            mid = btag.split('ga')[0]
            s_id = btag.split('ga')[1].split('cid')[0]
            if 'affid' in btag:
                aff_id = btag.split('ga')[1].split('cid')[1].split('affid')[1]
                try:
                    aff_id = int(aff_id)
                except:
                    aff_id = None
        else:
            mid = btag.split('b_')[0].replace('a_','')
            s_id = btag.split('b_')[1].split('c_')[0]
        try:
            mid = int(mid)
            s_id = int(s_id)
        except:
            return False
        if aff_id:
            aff_obj = Affiliateinfo.objects.filter(mappingid=aff_id,
                                            account_manager=obj.account_manager)
            # print 'affid got ', aff_id, aff_obj
            aff_obj = aff_obj[0] if aff_obj else None
        else:
            as_ids = AffiliateSite.objects.filter(site_id=s_id,
                                fk_affiliateinfo__account_manager=obj.account_manager)
            if not as_ids:
                print 'sid not ', s_id
                #msg = EmailMessage(str(s_id) + ' : for brand '+ siteobj.name, str(mid),
                #"support@wynta.com", ['shravan.bitla@gridlogic.in'])
                #msg.send()
            aff_obj = as_ids[0].fk_affiliateinfo if as_ids else None
        
        camp_obj, created = Campaign.objects.get_or_create(
                            account_manager=obj.account_manager, name=str(mid), 
                            siteid=siteobj)
        camp_obj.save()
        if aff_obj:
            camp_track_list_obj = CampaignTrackerMapping.objects.filter(fk_affiliateinfo=aff_obj, fk_campaign=camp_obj, goalid=4)
            if not camp_track_list_obj:
                camp_track_obj = CampaignTrackerMapping.objects.create(fk_affiliateinfo=aff_obj, 
                                fk_campaign=camp_obj, trackerid=1, goalid=4)
            else:
                camp_track_obj = camp_track_list_obj[0]
        else:
            camp_track_list_obj = CampaignTrackerMapping.objects.filter(fk_affiliateinfo=obj, 
                            fk_campaign=camp_obj, goalid=4)
            if not camp_track_list_obj:
                camp_track_obj = CampaignTrackerMapping.objects.create(fk_affiliateinfo=obj, 
                                fk_campaign=camp_obj, trackerid=1, goalid=4)
            else:
                camp_track_obj = camp_track_list_obj[0]
        return camp_track_obj


    @transaction.atomic
    def store_source_data(self, *args, **kwargs):
        """
        transaction.atomic() is used with for every create to catch the exception.
        It does not roll back only that row but all the rows that are inserted.
        As we have mentioned decorator on top it take cares of all the transactions
        not just one which is throwing exception.
        """
        source_data = kwargs['source_data']
        day_stop = kwargs['day_stop']
        day_start = kwargs['day_start']
        date = day_start.split(' ')[0]
        call_type = kwargs['call_type']
        site_obj = kwargs['site_obj']
        currency = ''
        # import pdb;pdb.set_trace()
        acmanager = AccountManager.objects.get(affiliateprogramme__name='Maxi Affiliates')
        maxi_sites = acmanager.siteid.all()
        currency_obj = Currency.objects.filter(siteid=site_obj)
        if currency_obj:
            currency = currency_obj[0].currency
        date_d = datetime.strptime(str(day_start).split(' ')[0], "%Y-%m-%d")
        json_dump = 'media/json_dump/%s-%s-%s.txt'%(site_obj.id,str(date_d).split(' ')[0], call_type)
        if os.path.isfile(json_dump):
            os.remove(json_dump)
        with open(json_dump, 'w+') as outfile:
            json.dump(str(source_data), outfile, sort_keys = True, indent = 4,
                   ensure_ascii = False)
        minus_30_file = date_d - timedelta(days=30)
        json_minus_30_dump = 'media/json_dump/%s-%s-%s.txt'%(site_obj.id,str(minus_30_file).split(' ')[0], call_type)
        if os.path.isfile(json_minus_30_dump):
            os.remove(json_minus_30_dump)
        if call_type == 'registration_data_url':
            # print source_data, day_start
            source_data = source_data['PlayersRegistrationData']
            if not source_data:
                print day_start
                return True
            for row in source_data['RegistrationData']:
                platform = self.device_dict[int(row['DeviceTypeID'])]
                bonus = float(row['RegistrationBonusAmount'])
                ftd = row['IsRegisterAndFTDAtSameDay']
                registrationdate = row['RegistrationDate']
                try:
                    country = pycountry.countries.get(numeric=str(row['CountryIsoCode'])).name
                except:
                    country = ''
                name = row['Alias']
                btag = row['Tag']
                playerid = int(row['PlayerID'])
                clickid = btag
                w_dynamic = btag
                if btag and btag != '' and btag.lower() != 'none':
                    clickid = btag[17:]
                    click_obj = PostBackClicks.objects.filter(clickid=clickid)
                    if click_obj:
                        camp_track_obj = click_obj[0].fk_campaign
                        w_dynamic = click_obj[0].wdynamic if click_obj[0].wdynamic else None
                    elif 'ga' in btag:
                        if Affiliateinfo.objects.filter(id=5, siteid=site_obj):
                            obj = Affiliateinfo.objects.get(id=5)
                        else:
                            obj = Affiliateinfo.objects.filter(siteid=site_obj).order_by('id')[0]
                        camp_track_obj = self.get_map_details(obj, site_obj, clickid)
                        if not camp_track_obj:
                            continue
                        cid = clickid.split('ga')[1].split('cid')[1].split('pid')[0]
                        if cid:
                            w_dynamic = cid
                    else:
                        # import pdb;pdb.set_trace()
                        if Affiliateinfo.objects.filter(id=5, siteid=site_obj):
                            obj = Affiliateinfo.objects.get(id=5)
                        else:
                            if '_003_' in btag and site_obj.id==150:
                                if day_start > '2019-10-24':
                                    continue
                                obj = Affiliateinfo.objects.get(id=1371)
                            else:
                                obj = Affiliateinfo.objects.filter(siteid=site_obj).order_by('id')[0]
                        camp_obj, created = Campaign.objects.get_or_create(
                                       fk_affiliateinfo=obj, name=btag,
                                       siteid=site_obj)
                        camp_obj.save()
                        camp_track_list_obj = CampaignTrackerMapping.objects.filter(fk_affiliateinfo=obj,
                                                  fk_campaign=camp_obj, goalid=4)
                        if not camp_track_list_obj:
                            camp_track_obj = CampaignTrackerMapping.objects.create(fk_affiliateinfo=obj,
                                                 fk_campaign=camp_obj, trackerid=1, goalid=4)
                        else:
                            camp_track_obj = camp_track_list_obj[0]
                else:
                    continue
                process_obj = PPPlayerProcessed.objects.filter(playerid=playerid,
                                        siteid = site_obj,
                                        date=date
                                        )
                if len(process_obj) == 0:
                    process_obj = PPPlayerProcessed.objects.create(playerid=playerid,
                                            alias = name,
                                            fk_campaign = camp_track_obj,
                                            siteid = site_obj,
                                            registrationdate = registrationdate,
                                            registrations = 1,
                                            platform = platform,
                                            currency =currency,
                                            dynamic = btag,
                                            country = country,
                                            clickid = clickid,
                                            date=date
                                            )
                else:
                    process_obj = process_obj[0]
                    process_obj.fk_campaign = camp_track_obj
                    process_obj.registrationdate = registrationdate
                    process_obj.lastdate=date
                    process_obj.registrations = 1
                    process_obj.dynamic = btag
                    process_obj.country = country
                if ftd:
                    process_obj.ftddate = date
                process_obj.wdynamic = w_dynamic
                process_obj.save()
        else:
            print source_data, day_start
            source_data = source_data['PlayersActivityData']
            if not source_data:
                return True
            print 'job went well on', day_start
            for row in source_data['ActivityData']:
                btag = row['Tag']
                void = (row.VoidDepositAmount/100) if row.VoidDepositAmount else 0.0
                rev_cash_out = (row.ReversalCashoutAmount/100) if row.ReversalCashoutAmount else 0.0
                playerid = row['PlayerID']
                bonuses = (row.BonusesAmount/100) if row.BonusesAmount else 0.0
                cashout = (row.CashoutAmount/100) if row.CashoutAmount else 0.0
                chargeback = (row.ChargebackAmount/100) if row.ChargebackAmount else 0.0
                deposit = (row.DepositAmount/100) if row.DepositAmount else 0.0
                sidegamesbets = (row.BingoWagersAmount/100) if row.BingoWagersAmount else 0.0
                sidegameswins = (row.BingoWinningsAmount/100) if row.BingoWinningsAmount else 0.0
                revenue = deposit - cashout + rev_cash_out
                clickid = btag
                w_dynamic = btag
                if btag and btag != '' and btag.lower() != 'none':
                    clickid = btag[17:]
                    click_obj = PostBackClicks.objects.filter(clickid=clickid)
                    if click_obj:
                        camp_track_obj = click_obj[0].fk_campaign
                        w_dynamic = click_obj[0].wdynamic if click_obj[0].wdynamic else None
                    elif 'ga' in btag:
                        if Affiliateinfo.objects.filter(id=5, siteid=site_obj):
                            obj = Affiliateinfo.objects.get(id=5)
                        else:
                            obj = Affiliateinfo.objects.filter(siteid=site_obj).order_by('id')[0]
                        camp_track_obj = self.get_map_details(obj, site_obj, clickid)
                        if not camp_track_obj:
                            continue
                        cid = clickid.split('ga')[1].split('cid')[1].split('pid')[0]
                        if cid:
                            w_dynamic = cid
                    else:
                        # import pdb;pdb.set_trace()
                        if Affiliateinfo.objects.filter(id=5, siteid=site_obj):
                            obj = Affiliateinfo.objects.get(id=5)
                        else:
                            if '_003_' in btag and site_obj.id==150:
                                if day_start > '2019-10-24':
                                    continue
                                obj = Affiliateinfo.objects.get(id=1371)
                            else:
                                obj = Affiliateinfo.objects.filter(siteid=site_obj).order_by('id')[0]
                        camp_obj, created = Campaign.objects.get_or_create(
                                       fk_affiliateinfo=obj, name=btag,
                                       siteid=site_obj)
                        camp_obj.save()
                        camp_track_list_obj = CampaignTrackerMapping.objects.filter(fk_affiliateinfo=obj,
                                                  fk_campaign=camp_obj, goalid=4)
                        if not camp_track_list_obj:
                            camp_track_obj = CampaignTrackerMapping.objects.create(fk_affiliateinfo=obj,
                                                 fk_campaign=camp_obj, trackerid=1, goalid=4)
                        else:
                            camp_track_obj = camp_track_list_obj[0]
                else:
                    continue
                rev_percent = camp_track_obj.fk_affiliateinfo.account_manager.rev_percent_cut
                playercheck = PlayerCheck.objects.filter(playerid=playerid,
                                                            siteid=site_obj)
                if playercheck:
                    if str(playercheck[0].date) < date:
                        camp_track_obj = playercheck[0].ck_campaign
                        process_obj = PPPlayerProcessed.objects.filter(playerid=playerid,
                                        siteid = site_obj,
                                        date=date
                                        )
                        if process_obj:
                            process_obj.delete()
                process_objs = PPPlayerProcessed.objects.filter(playerid=playerid,
                                        siteid = site_obj,
                                        )
                if len(process_objs) > 0:
                    poct, revenue, cashout = get_maxi_deductions(site_obj, maxi_sites,
                                  process_objs[0].country, revenue, deposit, cashout)
                    #import pdb;pdb.set_trace()
                    process_obj = PPPlayerProcessed.objects.filter(playerid=playerid,
                                        siteid = site_obj,
                                        date=date
                                        )
                    if len(process_obj) == 0:
                        process_obj = PPPlayerProcessed.objects.create(playerid=playerid,
                                            fk_campaign = camp_track_obj,
                                            siteid = site_obj,
                                            lastdate=date,
                                            deposit = deposit,
                                            cashout = cashout,
                                            platform = process_objs[0].platform,
                                            bonuses = bonuses,
                                            revenue = (revenue - (revenue*rev_percent)) if revenue else 0.0,
                                            sidegamesbets = sidegamesbets,
                                            sidegameswins = sidegameswins,
                                            date=date
                                            )
                        if deposit > 0 and not process_objs[0].ftddate and process_objs[0].registrationdate:
                            if process_objs[0].registrationdate > datetime.strptime('2018-12-01', '%Y-%m-%d'):
                                process_obj.ftddate = date
                        elif process_objs[0].ftddate:
                            process_obj.ftddate = process_objs[0].ftddate
                        if process_objs[0].registrationdate:
                            process_obj.registrationdate = process_objs[0].registrationdate
                        if process_objs[0].country:
                            process_obj.country = process_objs[0].country
                    else:
                        process_obj = process_obj[0]
                        if deposit > 0 and not process_obj.ftddate and process_objs[0].registrationdate:
                            if process_objs[0].registrationdate > datetime.strptime('2018-12-01', '%Y-%m-%d'):
                                process_obj.ftddate = date
                        elif process_obj.ftddate:
                            process_obj.ftddate = process_obj.ftddate
                        process_obj.lastdate=date
                        process_obj.fk_campaign = camp_track_obj
                        process_obj.deposit = deposit
                        process_obj.cashout = cashout
                        process_obj.bonuses = bonuses
                        process_obj.revenue = (revenue - (revenue*rev_percent)) if revenue else 0.0
                        process_obj.sidegamesbets = sidegamesbets
                        if process_objs[0].country:
                            process_obj.country = process_objs[0].country
                    process_obj.wdynamic = w_dynamic
                    process_obj.save()
                else:
                    print playerid
                    process_obj = PPPlayerProcessed.objects.filter(playerid=playerid,
                                        siteid = site_obj,
                                        date=date
                                        )
                    if len(process_obj) == 0:
                        process_obj = PPPlayerProcessed.objects.create(playerid=playerid,
                                            fk_campaign = camp_track_obj,
                                            siteid = site_obj,
                                            lastdate=date,
                                            deposit = deposit,
                                            cashout = cashout,
                                            platform = '',
                                            bonuses = bonuses,
                                            revenue = (revenue - (revenue*rev_percent)) if revenue else 0.0,
                                            sidegamesbets = sidegamesbets,
                                            date=date
                                            )
                    else:
                        process_obj = process_obj[0]
                        process_obj.lastdate=date
                        process_obj.fk_campaign = camp_track_obj
                        process_obj.deposit = deposit
                        process_obj.cashout = cashout
                        process_obj.bonuses = bonuses
                        process_obj.revenue = (revenue - (revenue*rev_percent)) if revenue else 0.0
                        process_obj.sidegamesbets = sidegamesbets
                    process_obj.wdynamic = w_dynamic
                    process_obj.save()
        return True

    @transaction.atomic
    def retrieve_source_data(self, *args, **kwargs):
        """
        transaction.atomic() is used with for every create to catch the exception.
        It does not roll back only that row but all the rows that are inserted.
        As we have mentioned decorator on top it take cares of all the transactions
        not just one which is throwing exception.
        """
        day_stop = kwargs['day_stop']
        day_start = kwargs['day_start']
        call_type = kwargs['call_type']
        site_obj = kwargs['site_obj']
        source_data = {}
        network_obj = Network.objects.get(networkname='Dragonfish')
        # network_ap_mapping = NetworkAffiliateProgrammeMapping.objects.filter(
        #                     networkid=network_obj,
        #                     siteid=site_obj.siteid)
        # authrize_code = base64.b64encode(str.encode('%s:%s'%(self.username,self.password)))
        #data = base64.b64decode(encoded)
        # 2802, 2803, 2804, 2863
        Whitelabel_id = site_obj.whitelabelid
        headers = '{}'
        import datetime as dtt
        now = dtt.datetime.now()
        # import logging
        # logging.basicConfig(level=logging.INFO)
        # logging.getLogger('suds.client').setLevel(logging.DEBUG)
        # logging.getLogger('suds.transport').setLevel(logging.DEBUG)
        p_day_stop = now
        p_day_start = day_start
        try:
            #import pdb;pdb.set_trace()
            with transaction.atomic():
                if call_type == 'registration_data_url':
                    chunks = self.get_per_chunks(day_stop=day_stop, day_start=day_start,chunk_size=-1)
                    cd = Client(self.url,
                       transport = HTTPSClientCertTransport('bf.key', 'bf.pem'))
                    cd.set_options(headers=ast.literal_eval(headers))
                    for chunk in chunks:
                        for i in [11]:
                            p_payload = self.content[call_type]%(i, 
                                str(chunk).split(' ')[0],
                                str(p_day_stop).split(' ')[0],str(Whitelabel_id))
                            #print p_payload
                            data_report = cd.service.GetRegistrationsSummary(__inject={'msg':p_payload})
                            # data_report = requests.get(df_url, data=p_payload,headers=headers, verify=False, cert=("dfcert.pem", "dfkey.pem"))
                            if data_report:
                                # print data_report
                                self.save_audit(
                                    'RET', 'Success', None,
                                    None, 'Retrieving Done'
                                )
                            else:
                                report = cd.service.GetRegistrationsSummary(__inject={'msg':p_payload})
                                if not report:
                                    self.save_audit(
                                        'RET', 'Fail', None,
                                        None, str(report.text) + ' Error in Retrieving '+ str(site_obj.siteid.domain)+ ' '+ str(chunk)
                                    )
                            print data_report, site_obj.siteid
                            stored = self.store_source_data(source_data=data_report, site_obj= site_obj.siteid,
                                     day_stop=str(chunk), day_start=str(chunk), call_type=call_type, ftd_data=source_data)
                            # if not stored:
                            #     return False
                else:
                    chunks = self.get_per_chunks(day_stop=day_stop, day_start=day_start,chunk_size=-1)
                    cd = Client(self.url,
                       transport = HTTPSClientCertTransport('bf.key', 'bf.pem'))
                    cd.set_options(headers=ast.literal_eval(headers))
                    for chunk in chunks:
                        for i in [11, 8]:
                            p_payload = self.content[call_type]%(str(chunk).split(' ')[0],i,
                                str(p_day_stop).split(' ')[0],str(Whitelabel_id))
                            
                            data_report = cd.service.GetActivitySummary(__inject={'msg':p_payload})
                            if data_report:
                                self.save_audit(
                                    'RET', 'Success', None,
                                    None, 'Retrieving Done'
                                )
                            else:
                                report = cd.service.GetActivitySummary(__inject={'msg':p_payload})
                                if not report:
                                    self.save_audit(
                                        'RET', 'Fail', None,
                                        None, str(report.text) + ' Error in Retrieving '+ str(site_obj.siteid.domain)+ ' '+ str(chunk)
                                    )
                                    # continue
                            stored = self.store_source_data(source_data=data_report, site_obj= site_obj.siteid,
                                     day_stop=str(chunk), day_start=str(chunk), call_type=call_type, ftd_data=source_data)
                            # if not stored:
                            #     return False
        except Exception as e:
            self.save_audit(
                'STG', 'Fail', day_stop,
                day_start, e
            )
            return False
