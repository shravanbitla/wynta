from django.core.management.base import BaseCommand, CommandError
from userapp.models import *
#from userapp.models import Network
import sys
import datetime as dtt
from connectors.models import *
from affiliate.models import *
import smtplib
import string
import random
import csv
from django.template import Template, Context
from django.core.mail import EmailMessage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

class Command(BaseCommand):
    def handle(self, *args, **options):
        email = 'manager@maxiaffiliates.com'
        passwd = '4_MA_only_!'
        server = smtplib.SMTP('smtp.gmail.com:587')
        server.ehlo()
        server.starttls()
        server.ehlo()
        server.login(email, passwd)
        acmanager = AccountManager.objects.get(affiliateprogramme__name='Maxi Affiliates')
        aff_objs = Affiliateinfo.objects.filter(account_manager=acmanager, status='Approved', id__gt=1272)
        for obj in aff_objs:
            def GenToken(length=10, chars=string.letters + string.digits):
                """token code for mail verification"""
                return ''.join([random.choice(chars) for i in range(length)])
            token = GenToken(length=20, chars=string.letters + string.digits)    # generate token
            tokenobj = AffiliatePasswordResetlogs.objects.create(affiliateid=obj, token=token,
                        expiretimestamp=datetime.now() + timedelta(minutes=180000))
            try:
                subject = 'Welcome to the new Maxi Affiliates platform.'
                tokenid = tokenobj.id
                username = obj.id
                domain = obj.account_manager.affiliateprogramme.domain
                aff_prog = obj.account_manager.affiliateprogramme.name
                token = tokenobj.token
                content = """<p>Dear %s,</p>
                    <p>We're happy to introduce you to the new and improved Maxi Affiliates platform. </p>
                    <p>The data migration has been completed. All that is left to do is use your new login details.</p>
                    <p>Here are your new login details:</p>
                    <p>URL: https://partner.maxiaffiliates.com/<br />Username: %s<br />Password: <span style="text-decoration: underline;"><a style="text-decoration: underline;" href="http://%s/affiliate/passwordresetaction/%s/%s/%s">Click to change password</a></span></p>
                    <p>Please click on the above link to reset your password.</p>
                    <p>Looking forward to generating revenue with you!</p>
                    <p>Best Regards,<br />The MaxiAffiliates Team<br />Skype: Maxi Affiliates Manager<br />Web: https://www.maxiaffiliates.com</p>
                    """%(obj.contactname, 
                        obj.email, domain, username, token, tokenid)
                to_email = obj.email
                msg = MIMEMultipart('alternative')
                msg['Subject'] = subject
                msg['From'] = email
                msg['To'] = to_email
                content = MIMEText(content, 'html')
                msg.attach(content)
                dd = server.sendmail(email, to_email, msg.as_string())
                # a = Template(content)
                # b = Context(locals())
                # d = a.render(b)
                # msg = EmailMessage(subject, d,'manager@maxiaffiliates.com', 
                #            ['shrvn.chandra@gmail.com'])
                # msg.content_subtype = "html"
                # msg.send()
            except Exception as e:
                print str(e)
                import pdb;pdb.set_trace()
        server.quit()
