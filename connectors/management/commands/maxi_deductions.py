from django.core.management.base import BaseCommand, CommandError
from connectors.models import AuditTable, Date, PPAdvancedRaw, PPPlayerRaw, PPPlayerProcessed
from userapp.models import *
#from userapp.models import Network
import sys
import datetime as dtt
from connectors.models import *
import pycountry
from userapp.models import Country

def get_maxi_deductions(siteid, site_ids, country, revenue, deposit, cashout):
    deductions_dict = {'Jazzy Spins': {'United Kingdom': 0.66, 'Portugal': 0.30, 'Ireland':0.45, 'R': 0.35},
        'ChitChat Bingo': {'United Kingdom': 0.53, 'Portugal': 0.30, 'Ireland':0.45, 'R': 0.35},
        'BidBingo': {'United Kingdom': 0.53, 'Portugal': 0.30, 'Ireland':0.45, 'R': 0.35},
        'BingoZino': {'United Kingdom': 0.53, 'Portugal': 0.30, 'Ireland':0.45, 'R': 0.35},
        'Mr Jack Vegas': {'United Kingdom': 0.66, 'R': 0.60},
        'Mr Superplay': {'United Kingdom': 0.66, 'R': 0.60},
        'Maxiplay': {'United Kingdom': 0.66, 'R': 0.60},
        'Hippozino': {'United Kingdom': 0.66, 'R': 0.60}
    }
    if siteid in site_ids:
        deduct_countries = deductions_dict[siteid.name]
        if country in deduct_countries.keys():
            deduct_percent = deduct_countries[country]
        else:
            deduct_percent = deduct_countries['R']
        if revenue <0:
            revenue = (revenue/(1+deduct_percent))
        else:
            return True, revenue, cashout
        revenue = (revenue - (revenue*deduct_percent))
        # cashout = deposit - revenue
        return True, revenue, cashout
    return False, revenue, cashout

class Command(BaseCommand):
    def handle(self, *args, **options):
        import pdb;pdb.set_trace()
        acmanager = AccountManager.objects.get(affiliateprogramme__name='Maxi Affiliates')
        maxi_sites = acmanager.siteid.all()
        for i in PPPlayerProcessed.objects.filter(siteid_id__in=maxi_sites):
            poct, revenue, cashout = get_maxi_deductions(i.siteid, maxi_sites,
                                  i.country, i.revenue, i.deposit, i.cashout)
            if i.revenue != revenue:
                i.revenue = revenue
                i.save()