from django.core.management.base import BaseCommand, CommandError
from userapp.models import *
from connectors.models import *
from affiliate.models import *
from affiliate.dashboard_data import *
import sys
from datetime import datetime, time, timedelta
from dateutil.relativedelta import relativedelta
import os
from dateutil.rrule import rrule, MONTHLY, YEARLY
from django.core.mail import EmailMessage

class Command(BaseCommand):

    def handle(self, *args, **options):
        dates_d = []
        to_date = datetime.now().date() - timedelta(days=1)
        from_date = to_date.replace(day=1)
        # import pdb;pdb.set_trace()
        # date = datetime.now().date() - timedelta(days=15)
        # while date.year > 2018:
        #     if date.day == 1:
        #         to_date = date - timedelta(days=1)
        #         from_date = to_date.replace(day=1)
        #         dates_d.append((from_date, to_date))
        #     date = from_date
        # dates_d = dates_d[::-1]
        # import pdb;pdb.set_trace()
        dates_d = [(from_date, to_date)]
        for d in dates_d:
            to_date = d[1]
            from_date = d[0]
            # to_date = datetime.now().date() - timedelta(days=1)
            # from_date = to_date.replace(day=1)
            try:
                aff_objs = Affiliateinfo.objects.all()
                camp_links = CampaignTrackerMapping.objects.filter(
                                fk_affiliateinfo__in=aff_objs
                                ).select_related('fk_campaign', 'fk_affiliateinfo')
                sites = Site.objects.all()
                data_list, currency = group_by_results(from_date, to_date, sites, camp_links, 'affiliate_report',
                             'gbp', 'affiliate_report')
                for aff_data in data_list:
                    aff_obj = Affiliateinfo.objects.get(id=aff_data[0])
                    account = AccountDetails.objects.filter(fk_affiliateinfo=aff_obj).order_by('id').first()
                    account_type = 'bankwire'
                    if account:
                        if account.paypal_username:
                            account_type = 'paypal'
                        elif account.neteller_username:
                            account_type = 'neteller'
                    limit = Threshold.objects.filter(
                                account_manager=aff_obj.account_manager).order_by('id').first()
                    status = 'carried'
                    threshold_amount = None
                    last_carried_balance = 0.0
                    paid_status = False
                    total_comm = aff_data[-1]
                    history_ph = PaymentHistory.objects.filter(fk_affiliateinfo=aff_obj,
                                                status='carried', paid_status=False)
                    history_earnings = history_ph.aggregate(Sum('amount'))
                    if history_earnings:
                        history_earnings = history_earnings['amount__sum']
                    if history_ph:
                        last_balance = history_ph.order_by('-timestamp').first()
                        if last_balance:
                            last_carried_balance = last_balance.balance
                        # if last_carried and history_earnings != last_carried:
                        #     raise Exception('Error with mismatch of carried.')
                    if limit:
                        threshold_amount = getattr(limit, account_type)
                    if not threshold_amount:
                        threshold_amount = 0
                    # import pdb;pdb.set_trace()
                    balance = total_comm + last_carried_balance
                    if balance >= threshold_amount:
                        status = 'pay'
                        history_ph.update(paid_status=True)
                        paid_status = True
                    # if last_carried < 0:
                    #     last_carried == 0
                    # import pdb;pdb.set_trace()
                    payment_h = PaymentHistory.objects.get_or_create(
                                    fk_affiliateinfo=aff_obj,
                                    status=status,
                                    payment_type=account_type,
                                    month=from_date.month,
                                    year=from_date.year,
                                    amount=total_comm,
                                    paid_status=paid_status,
                                    carried_amount=last_carried_balance,
                                    threshold_amount=threshold_amount,
                                    balance=balance
                                    )
            except Exception as e:
                import pdb;pdb.set_trace()
                msg = EmailMessage('Player Report Exception in Player data report ', str(e), 
                        "support@wynta.com", ['shravan.bitla@gridlogic.in'])
                msg.send()

