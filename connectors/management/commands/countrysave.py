from django.core.management.base import BaseCommand, CommandError
from connectors.models import AuditTable, Date, PPAdvancedRaw, PPPlayerRaw, PPPlayerProcessed
from userapp.models import *
#from userapp.models import Network
import sys
import datetime as dtt

class Command(BaseCommand):
    def handle(self, *args, **options):
        with open('countrylist.csv') as f:
            content = f.readlines()
            for line in content:
                if '"' in line:
                    name = line.split('",')[0].replace('"','')
                else:
                    name = line.split(',')[0]
                country_obj, created = Country.objects.get_or_create(countryname=name)
                country_obj.save()