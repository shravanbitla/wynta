from django.core.management.base import BaseCommand, CommandError
from connectors.models import AuditTable, Date, PPAdvancedRaw, PPPlayerRaw, PPPlayerProcessed
from userapp.models import Network
from connectors.retrievers import *
#from userapp.models import Network
import sys
import datetime as dtt

class Command(BaseCommand):
    def handle(self, *args, **options):
        calls = ['player_data_url']
        # calls = ['advanced_data_url', 'player_data_url']
        self.username = settings.PP_USER_NAME
        self.password = settings.PP_PASSWORD
        now = dtt.datetime.now()
        day_stop = now
        day_start = now - dtt.timedelta(days=2)
        #if day_start.month < day_stop.month or day_start.year < day_stop.year:
        #    day_start = day_stop.replace(day=1)
        pp_connector = ProgressPlayConnector(
            username=self.username,
            password=self.password
        )
        pp_connector.save_audit(
            'RET', 'Start', day_stop,
            day_start, 'Retriving Started'
        )

        network_obj = Network.objects.get(networkname='ProgressPlay')
        pp_connector.network = network_obj
        # import pdb;pdb.set_trace()
        acmanager = AccountManager.objects.get(affiliateprogramme__name='Maxi Affiliates')
        site_white_obj = SiteWhitelabelMapping.objects.filter(networkid=network_obj)
        for site_obj in site_white_obj:
            for call in calls:
                print site_obj.siteid.name + 'site start'
                # import pdb;pdb.set_trace()
                if call == 'advanced_data_url':
                    query_set = [ad.id for ad in PPAdvancedRaw.objects.all()]
                else:
                    # query_set = [pp.id for pp in PPPlayerRaw.objects.all()]
                    PPPlayerRaw.objects.all().delete()
                stored = pp_connector.retrieve_source_data(
                    day_stop=day_stop, day_start=day_start, site_obj=site_obj, call_type=call)
                if stored and call == 'advanced_data_url':
                    # Deleting previous Record
                    PPAdvancedRaw.objects.filter(id__in=query_set).delete()
                    pp_connector.save_audit(
                        'STG', 'Success', day_stop,
                        day_start, 'Stagging Success'
                    )
                    pp_connector.processing_data(day_stop=day_stop, day_start=day_start, site_obj=site_obj, call_type=call)
                elif stored and call == 'player_data_url':
                    pp_connector.save_audit(
                        'STG', 'Success', day_stop,
                        day_start, 'Stagging Success'
                    )
                    pp_connector.processing_data(day_stop=day_stop, day_start=day_start, site_obj=site_obj, call_type=call)
                    # import pdb;pdb.set_trace()
                elif not stored:
                    return False

