from django.core.management.base import BaseCommand, CommandError
from connectors.models import AuditTable, Date, PPAdvancedRaw, PPPlayerRaw, PPPlayerProcessed
from userapp.models import Network
from connectors.retrievers import *
#from userapp.models import Network
import sys
import datetime as dtt

class Command(BaseCommand):
    def handle(self, *args, **options):
        calls = ['player_personal_data_url', 'player_data_url']
        now = dtt.datetime.now()
        day_stop = now
        day_start = now - dtt.timedelta(days=9)
        if day_start.month < day_stop.month or day_start.year < day_stop.year:
            day_start = day_stop.replace(day=1)
        yy_connector = YayaConnector(
            username='',
            password=''
        )
        yy_connector.save_audit(
            'RET', 'Start', day_stop,
            day_start, 'Retriving Started'
        )
        # import pdb;pdb.set_trace()
        network_obj = Network.objects.get(networkname='YayaGaming')
        yy_connector.network = network_obj
        site_white_obj = SiteWhitelabelMapping.objects.filter(networkid=network_obj)
        for site_obj in site_white_obj:
            for call in calls:
                stored = yy_connector.retrieve_players_data(
                    day_stop=day_stop, day_start=day_start, site_obj=site_obj, call_type=call)
                yy_connector.save_audit(
                    'STG', 'Success', day_stop,
                    day_start, 'Stagging Success'
                )
                # pp_connector.processing_data(day_stop=day_stop, day_start=day_start, site_obj=site_obj, call_type=call)


