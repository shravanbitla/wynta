from django.db.models import F
from django.core.management.base import BaseCommand, CommandError
from userapp.models import *
from admins.views import *
#from userapp.models import Network
import sys
import datetime as dtt

class Command(BaseCommand):
    def handle(self, *args, **options):
    	# import pdb;pdb.set_trace()
        af_prog = AffiliateProgramme.objects.filter(ssl_state='STARTED')
        for aff_p in af_prog:
        	add_lets_encrypt_files(aff_p.domain, aff_p.id)
