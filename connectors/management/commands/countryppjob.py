from django.core.management.base import BaseCommand, CommandError
from connectors.models import AuditTable, Date, PPAdvancedRaw, PPPlayerRaw, PPPlayerProcessed
from userapp.models import *
#from userapp.models import Network
import sys
import datetime as dtt
from connectors.models import *
import pycountry
from userapp.models import Country

class Command(BaseCommand):
    def handle(self, *args, **options):
        for i in PPPlayerProcessed.objects.all().values('country').distinct():
            country = i['country']
            try:
                country = pycountry.countries.get(alpha_2=country).name
                pp_obj = PPPlayerProcessed.objects.filter(country=i['country'])
                for pp in pp_obj:
                    pp.country = country
                    pp.save()
            except:
                country = i['country']
            country_obj, created = Country.objects.get_or_create(countryname=country)
            if created:
                country_obj.save()