from django.core.management.base import BaseCommand, CommandError
from userapp.models import *
from affiliate.models import *
import sys
from datetime import datetime
import csv
import codecs
import pandas as pd
from affiliate.utils import enc_password
from django.core.files import File
import os
import urllib

def add_affiliates_data(dict_list, acmanager):
    import pdb;pdb.set_trace()
    dict_list = []
    csvfile = codecs.open('maxi_data/AffiliateList.csv', 'rU', 'utf-16')
    reader = csv.reader(codecs.open('maxi_data/AffiliateList.csv', 'rU', 'utf-16'))
    # dictreader = csv.DictReader(csvfile)
    t = 0
    header_d = []
    try:
        for line in reader:
            if t == 0:
                header_d = line[0].split(u'\t')
            else:
                d_c = dict(zip(header_d, line[0].split(u'\t')))
                dict_list.append(d_c)
            t += 1
    except Exception as e:
        print 'error ', str(e)
    import pdb;pdb.set_trace()
    for row_dict in dict_list:
        try:
            affid = row_dict['Affiliate ID']
            name = row_dict['Username']
            website = row_dict['Website']
            contactname = row_dict['Affiliate Name']
            email = row_dict['Email']
            ipaddress = row_dict['Sign Up IP']
            email_verified_ip = row_dict['Email Verified IP']
            e_status = row_dict['Email Verified Status']
            country = row_dict['Country']
            r_date = row_dict['Sign Up Date']
            app_status = row_dict['Approval status']
            companyname = row_dict['Company Name']
            c_number = row_dict['Company Contact number']
            telephone = row_dict['Contact number']
            state = row_dict['State']
            address = row_dict['Address']
            country = row_dict['Country']
            city = row_dict['City']
            postcode = row_dict['Zipcode']
            skypeid = row_dict['Skype ID']
        except:
            import pdb;pdb.set_trace()
        password = enc_password(name)
        if r_date:
            try:
                r_date = datetime.strptime(r_date, '%d/%m/%Y %H:%M %p')
            except:
                r_date = datetime.strptime(r_date, '%d/%m/%Y %H:%M')
        else:
            r_date = None
        try:
            telephone = int(telephone)
        except:
            telephone = 0
        active = False
        status = 'Rejected'
        if app_status.strip().lower() == 'active':
            active = True
            status = 'Approved'
        try:
            aff_obj, created = Affiliateinfo.objects.get_or_create(mappingid=affid,username=name, password=password, 
                                        contactname=contactname,
                                       companyname=companyname, email=email, address=address, city=city,
                                       website=website,
                                       state=state, country=country, ipaddress=ipaddress,
                                       registeredon=r_date,
                                       skype=skypeid,active=active,status=status,
                                       mailrequired=True if 'not' not in e_status.lower() else False,
                                       postcode=postcode, telephone=telephone,
                                        account_manager=acmanager)
            for siteobj in acmanager.siteid.all():
                aff_obj.siteid.add(siteobj)
                aff_obj.save()
            networkid_obj = Network.objects.all()
            for nt in networkid_obj:
                aff_obj.networkid.add(nt)
            aff_obj.save()
        except Exception as e:
            print str(e)
            import pdb;pdb.set_trace()

def upload_marketing_materials(dict_list, acc_manager):
    import pdb;pdb.set_trace()
    for row_dict in dict_list:
        try:
            affid = row_dict['Affiliate ID']
            mediaid = row_dict['MediaID']
            height = row_dict.get('MediaHeight')
            affname = row_dict['Affiliate Name']
            width = row_dict.get('MediaWidth')
            s_id = row_dict['Site ID']
            direct_link = row_dict['Direct Link']
            java_script = row_dict.get('Javascript')
            html_track = row_dict['Html Track']
            destination_url = row_dict['Destination URL']
            brand = row_dict['Brand']
            media_category = row_dict['Media Category']
            language = row_dict['Language']
        except:
            import pdb;pdb.set_trace()
        site_obj = Site.objects.filter(name=brand)
        if site_obj:
            site_obj = site_obj[0]
        else:
            site_obj, created = Site.objects.get_or_create(name=brand, 
                                domain=destination_url)
        camp_obj = Campaign.objects.filter(name=str(mediaid), siteid=site_obj)
        if camp_obj:
            if not java_script and camp_obj[0].media_type=='B':
                camp_obj[0].media_type = 'T'
                camp_obj[0].save()
            continue
        if java_script:
            if 'https://brand' in html_track:
                b_link = 'https://brand'+html_track.split('https://brand')[1].split('"')[0]
            elif 'http://brand' in html_track:
                b_link = 'http://brand'+html_track.split('http://brand')[1].split('"')[0]
            bannerobj, created = BannerImages.objects.get_or_create(title=media_category,
                            mappingid=mediaid,
                            siteid=site_obj, media_type='B')
            if created:
                bannerobj.width=width
                bannerobj.height=height
                bannerobj.user = acc_manager.user
                result = urllib.urlretrieve(b_link, "media/bannerimages/"+b_link.split('/')[-1])
                bannerobj.banner.save(
                        b_link.split('/')[-1],
                        File(open(result[0]))
                        )
                bannerobj.save()
        camp_obj, created = Campaign.objects.get_or_create(
                            name=str(mediaid) , siteid=site_obj,
                            account_manager=acc_manager,
                            language=language)
        camp_obj.save()
        if java_script:
            camp_obj.media_type='B'
            bannerlogobj, created = Bannerlogs.objects.get_or_create(
                fk_campaign=camp_obj, fk_bannerimages=bannerobj)
            bannerlogobj.save()
        else:
            camp_obj.media_type='T'
        camp_obj.save()

def mapping_affiliate_siteid(acc_manager):
    import pdb;pdb.set_trace()
    dict_list = []
    with open('maxi_data/MediaTrackerLinks Chitchat.csv', 'rb') as csvfile:
        # creating a csv reader object
        # csvfile = codecs.open('maxi_data/'+file, 'rU', 'utf-16')
        # reader = csv.reader(codecs.open('maxi_data/'+file, 'rU', 'utf-16'))
        reader = csv.DictReader(csvfile)
        t = 0
        header_d = []
        try:
            for line in reader:
                d_c = {i.strip():v for i, v in line.iteritems()}
                dict_list.append(d_c) 
        except Exception as e:
            print 'error ', str(e)
            import pdb;pdb.set_trace()
    import pdb;pdb.set_trace()
    for row_dict in dict_list:
        try:
            affid = row_dict['Affiliate ID']
            mediaid = row_dict['MediaID']
            affname = row_dict['Affiliate Name']
            s_id = row_dict['Site ID']
        except Exception as e:
            print str(e)
        aff_obj = Affiliateinfo.objects.filter(mappingid=affid, account_manager=acc_manager)
        if len(aff_obj) == 1:
            aff_obj = aff_obj[0]
            as_ids = AffiliateSite.objects.filter(site_id=s_id)
            if as_ids and as_ids[0].fk_affiliateinfo != aff_obj:
                print 'multiple site Ids'
                import pdb;pdb.set_trace()
            else:
                as_ids = AffiliateSite.objects.filter(fk_affiliateinfo=aff_obj,
                                                site_id=s_id)
                if not as_ids:
                    import pdb;pdb.set_trace()
                    as_ids, created = AffiliateSite.objects.get_or_create(fk_affiliateinfo=aff_obj,
                                                site_id=s_id)
                    as_ids.save()
                    print as_ids
        else:
            print 'Multiple affids'
            import pdb;pdb.set_trace()

def add_commissions(acmanager):
    dict_list = []
    aff_list = []
    import pdb;pdb.set_trace()
    with open('maxi_data/BespokeConfigHistory-june19.csv', 'rb') as csvfile:
        # creating a csv reader object
        # csvfile = codecs.open('maxi_data/'+file, 'rU', 'utf-16')
        # reader = csv.reader(codecs.open('maxi_data/'+file, 'rU', 'utf-16'))
        reader = csv.DictReader(csvfile)
        t = 0
        header_d = []
        try:
            for line in reader:
                d_c = {i.strip():v for i, v in line.iteritems()}
                dict_list.append(d_c) 
        except Exception as e:
            print 'error ', str(e)
    import pdb;pdb.set_trace()
    for row_dict in dict_list:
        try:
            test = row_dict['']
            affname = row_dict['Affiliate']
            comm = row_dict['Bespoke Name']
            brand = row_dict['Brand']
            if 'permanently' not in test.lower():
                continue
            if 'month' in comm.lower() or 'cpa' in comm.lower():
                comm = comm[:-8]
            if '% revenue share' in comm.lower():
                comm = int(comm.lower().replace('% revenue share', ''))
            elif '% rev share' in comm.lower():
                comm = int(comm.lower().replace('% rev share', ''))
            elif ' percent' in comm.lower():
                comm = int(comm.lower().replace(' percent', ''))
            else:
                print 'invalid commission ', comm
                aff_list.append(affname)
                continue
        except:
            print 'error in parsing'
            aff_list.append(affname)
            continue
        site_obj = Site.objects.filter(name=brand)
        if site_obj:
            site_obj = site_obj[0]
        else:
            print brand
        try:
            aff_obj = Affiliateinfo.objects.filter(username=affname,
                account_manager=acmanager)
            if not aff_obj:
                print 'no aff obj ', affname
                aff_list.append(affname)
                continue
            aff_obj = aff_obj[0]
            aff_comm_objs = Affiliatecommission.objects.filter(fk_affiliateinfo=aff_obj,
                                            siteid=site_obj)
            if len(aff_comm_objs) == 1:
                continue
            elif len(aff_comm_objs) > 1:
                aff_comm_objs.delete()
            aff_comm_obj = Affiliatecommission.objects.create(fk_affiliateinfo=aff_obj,
                                            siteid=site_obj,
                                            revshare=True,
                                            revsharepercent=comm,
                                            pocdeduction=False)
            aff_comm_obj.save()
        except Exception as e:
            print str(e)
            import pdb;pdb.set_trace()
    print list(set(aff_list))

def write_payment(acmanager):
    import pdb;pdb.set_trace()
    row = ['Aff_id', 'Aff_email', 'Aff_username', 'Aff_company', 'accountnumber', 'sortcode',
               'accounttype', 'accountname', 'iban', 'swift', 'bankname', 'bankaddress',
               'paypal_username', 'paypal_email', 'neteller_email', 'neteller_username',
               'neteller_accountnumber' ]
    with open('payment_details.csv', 'w+') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerow(row)
    csvFile.close()
    aff_objs = Affiliateinfo.objects.filter(
                account_manager=acmanager)
    for aff_obj in aff_objs:
        row = [aff_obj.id, aff_obj.email, aff_obj.username, aff_obj.companyname]
        account_details = AccountDetails.objects.filter(fk_affiliateinfo=aff_obj)
        if account_details:
            acc_d = account_details[0]
            row = row + [acc_d.accountnumber,
                            acc_d.sortcode,
                            acc_d.accounttype, 
                            acc_d.accountname,
                            acc_d.iban,
                            acc_d.swift,
                            acc_d.bankname,
                            acc_d.bankaddress, 
                            acc_d.paypal_username,
                            acc_d.paypal_email,
                            acc_d.neteller_email,
                            acc_d.neteller_username,
                            acc_d.neteller_accountnumber]
        with open('payment_details.csv', 'a') as csvFile:
            try:
                writer = csv.writer(csvFile)
                writer.writerow(row)
            except:
                writer = csv.writer(csvFile)
                writer.writerow([unicode(s).encode("utf-8") for s in row])
        csvFile.close()



class Command(BaseCommand):
    def handle(self, *args, **options):
        try:
            acmanager = AccountManager.objects.get(affiliateprogramme__name='Maxi Affiliates')
            # add_affiliates_data([], acmanager)
            # add_commissions(acmanager)
            write_payment(acmanager)
            import pdb;pdb.set_trace()
            # aff_objs = Affiliateinfo.objects.filter(account_manager=acmanager)
            # country_obj = {i['countryname']:i['id'] for i in Country.objects.all().values('id','countryname').distinct()}
            # for aff in aff_objs:
            #     aff.country = country_obj[aff.country] if country_obj.get(aff.country) else aff.country
            #     aff.save()
            # import pdb;pdb.set_trace()
            #add_commissions(acmanager)
            # mapping_affiliate_siteid(acmanager)
            return True
            for file in os.listdir('maxi_data' + '/'):
                import pdb;pdb.set_trace()
                if file.endswith(".csv"):
                    ext = os.path.splitext(file)[1] 
                    valid_extensions = ['.csv', '.xls']
                    if not ext.lower() in valid_extensions:
                        print 'Unsupported file extension.'
                    dict_list = []
                    # with open('maxi_data/'+file, 'rb') as csvfile:
                    # creating a csv reader object
                    if 'affiliate' in file.lower():
                        csvfile = codecs.open('maxi_data/'+file, 'rU', 'utf-16')
                        reader = csv.reader(codecs.open('maxi_data/'+file, 'rU', 'utf-16'))
                        # dictreader = csv.DictReader(csvfile)
                        t = 0
                        header_d = []
                        try:
                            for line in reader:
                                if t == 0:
                                    header_d = line[0].split(u'\t')
                                else:
                                    d_c = dict(zip(header_d, line[0].split(u'\t')))
                                    dict_list.append(d_c)
                                t += 1
                        except Exception as e:
                            print 'error ', str(e)
                            import pdb;pdb.set_trace()
                        # add_affiliates_data(dict_list, acmanager)
                    else:
                        with open('maxi_data/'+file, 'rb') as csvfile:
                            # creating a csv reader object
                            # csvfile = codecs.open('maxi_data/'+file, 'rU', 'utf-16')
                            # reader = csv.reader(codecs.open('maxi_data/'+file, 'rU', 'utf-16'))
                            reader = csv.DictReader(csvfile)
                            t = 0
                            header_d = []
                            try:
                                for line in reader:
                                    d_c = {i.strip():v for i, v in line.iteritems()}
                                    dict_list.append(d_c) 
                            except Exception as e:
                                print 'error ', str(e)
                                import pdb;pdb.set_trace()
                        upload_marketing_materials(dict_list, acmanager)
        except Exception as e:
            print str(e)
