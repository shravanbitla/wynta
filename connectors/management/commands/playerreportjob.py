from django.core.management.base import BaseCommand, CommandError
from userapp.models import *
from connectors.models import *
from affiliate.models import *
from affiliate.dashboard_data import *
import sys
from datetime import datetime, time
import os
from dateutil.rrule import rrule, MONTHLY, YEARLY
from django.core.mail import EmailMessage

class Command(BaseCommand):

    def handle(self, *args, **options):
        date_now = datetime.now()
        date_check = datetime.now() -timedelta(days=7)
        if date_check.month < date_now.month or date_check.year < date_now.year:
            date_check = date_now.replace(day=1)
        # import pdb;pdb.set_trace()
        print date_check, date_now
        try:
            #date_check = datetime.combine(date_check, time(0,0,0))
            #date_now = datetime.combine(date_now, time(23,59,59))
            #db_campaign_entry_add(date_check, date_now, campd_list)
            dates_days_list = [dt for dt in rrule(DAILY, dtstart=date_check, until=date_now)]
            for ind, d in enumerate(dates_days_list):
                if len(dates_days_list) == ind:
                    continue
                # from_date = d
                # to_date = dates_days_list[ind+1]
                from_date_time = datetime.combine(d, time(0,0,0))
                to_date_time = datetime.combine(d, time(23,59,59))
                #site_ids = Site.objects.filter(id=158)
                site_ids = Site.objects.all()
                #acmanager = AccountManager.objects.get(affiliateprogramme__name='Maxi Affiliates')
                #site_ids = acmanager.siteid.all()
                # import pdb;pdb.set_trace()
                for site_id in site_ids:
                    campd_list = get_site_aff_camp([site_id])
                    db_campaign_entry_add(from_date_time, to_date_time, campd_list)
                # date_check = date_check + timedelta(days=1)
        except Exception as e:
            # import pdb;pdb.set_trace()
            msg = EmailMessage('Player Report Exception in Player data report ', str(e), 
                    "support@wynta.com", ['shravan.bitla@gridlogic.in'])
            msg.send()
