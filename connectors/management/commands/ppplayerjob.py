from django.core.management.base import BaseCommand, CommandError
from connectors.models import AuditTable, Date, PPAdvancedRaw, PPPlayerRaw, PPPlayerProcessed
from userapp.models import Network
from connectors.retrievers import *
#from userapp.models import Network
import sys
import datetime as dtt

class Command(BaseCommand):
    def handle(self, *args, **options):
        calls = ['player_personal_data_url']
        now = dtt.datetime.now()
        day_stop = now
        day_start = now - dtt.timedelta(days=6)
        if day_start.month < day_stop.month or day_start.year < day_stop.year:
            day_start = day_stop.replace(day=1)
        pp_connector = ProgressPlayConnector(
            username='',
            password=''
        )
        pp_connector.save_audit(
            'RET', 'Start', day_stop,
            day_start, 'Retriving Started'
        )

        network_obj = Network.objects.get(networkname='Progress Play')
        pp_connector.network = network_obj
        site_white_obj = SiteWhitelabelMapping.objects.filter(networkid=network_obj,siteid_id=102)
        for site_obj in site_white_obj:
            for call in calls:
                # if call == 'advanced_data_url':
                #     query_set = [ad.id for ad in PPAdvancedRaw.objects.all()]
                # else:
                #     query_set = [pp.id for pp in PPPlayerRaw.objects.all()]
                #     PPPlayerRaw.objects.filter(id__in=query_set).delete()
                stored = pp_connector.retrieve_player_data(
                    day_stop=day_stop, day_start=day_start, site_obj=site_obj, call_type=call)
                pp_connector.save_audit(
                    'STG', 'Success', day_stop,
                    day_start, 'Stagging Success'
                )
                # pp_connector.processing_data(day_stop=day_stop, day_start=day_start, site_obj=site_obj, call_type=call)


