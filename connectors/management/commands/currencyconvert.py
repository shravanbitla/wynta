from django.core.management.base import BaseCommand, CommandError
from userapp.models import *
from forex_python.converter import CurrencyRates
from currency_converter import CurrencyConverter
import sys
import datetime as dtt

class Command(BaseCommand):
    def handle(self, *args, **options):
        # c = CurrencyRates()
        c = CurrencyConverter('http://www.ecb.int/stats/eurofxref/eurofxref-hist.zip')
        # import pdb;pdb.set_trace()
        date_now = datetime.now()
        date_check = datetime.now() - timedelta(days=8)
        # location = "files"
        while date_check.date() <= date_now.date():
            try:
                if date_now.date() == date_check.date():
                    date_c = datetime.now() - timedelta(days=1)
                    eur_gbp = c.convert(1, 'EUR', 'GBP', date=date_c.date())
                    gbp_eur = c.convert(1, 'GBP', 'EUR', date=date_c.date())
                else:
                    eur_gbp = c.convert(1, 'EUR', 'GBP', date=date_check.date())
                    gbp_eur = c.convert(1, 'GBP', 'EUR', date=date_check.date())
            except:
                pass
            #eur_gbp = c.get_rate('EUR', 'GBP', date_check)
            #gbp_eur = c.get_rate('GBP', 'EUR', date_check)
            currency_obj, created = CurrencyConverted.objects.get_or_create(
                            date=date_check.date())
            currency_obj.eurtogbp=eur_gbp
            currency_obj.gbptoeur=gbp_eur
            currency_obj.save()
            date_check = date_check + timedelta(days=1)
