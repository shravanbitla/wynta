from django.core.management.base import BaseCommand, CommandError
from connectors.models import AuditTable, Date, PPAdvancedRaw, PPPlayerRaw, PPPlayerProcessed
from userapp.models import Network
from connectors.retrievers import *
#from userapp.models import Network
import sys
import datetime as dtt

class Command(BaseCommand):
    def handle(self, *args, **options):
        # calls = ['registration_data_url', 'player_data_url']
        calls = ['player_data_url']
        now = dtt.datetime.now()
        day_stop = now
        import pdb;pdb.set_trace()
        day_start = now - dtt.timedelta(days=9)
        df_connector = DragonFishConnector(
        )
        df_connector.save_audit(
            'RET', 'Start', day_stop,
            day_start, 'Retriving Started'
        )

        network_obj = Network.objects.get(networkname='Dragonfish')
        df_connector.network = network_obj
        site_white_obj = SiteWhitelabelMapping.objects.filter(networkid=network_obj)
        for site_obj in site_white_obj:
            for call in calls:
                stored = df_connector.retrieve_source_data(
                    day_stop=day_stop, day_start=day_start, site_obj=site_obj, call_type=call)
                # if stored and call == 'player_data_url':
                #     df_connector.save_audit(
                #         'STG', 'Success', day_stop,
                #         day_start, 'Stagging Success'
                #     )
                #     # df_connector.processing_data(day_stop=day_stop, day_start=day_start, site_obj=site_obj, call_type=call)
                # elif not stored:
                #     return False

