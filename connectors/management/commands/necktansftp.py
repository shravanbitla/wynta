from django.core.management.base import BaseCommand, CommandError
from connectors.models import AuditTable, Date, PPAdvancedRaw, PPPlayerRaw, PPPlayerProcessed
from userapp.models import *
from connectors.retrievers import *
import sys
import datetime
import os

class Command(BaseCommand):
    def handle(self, *args, **options):
        date_now = datetime.datetime.now()
        date_check = datetime.datetime.now()  - timedelta(days=9)
        # location = "files/Fortune Cat Casino"
        # import pdb;pdb.set_trace()
        location = "/sftp_necktan/shared/"
        # import pdb;pdb.set_trace()
        while date_check.date() != date_now.date():
            date_check_d = date_check.strftime("%Y%m%d")
            # folder = ''
            for folder in os.listdir(location):
                for file in os.listdir(location + folder + '/'):
                    if file.endswith(".tsv") and date_check_d in file:
                        handle_nektansftp_file(file, folder, str(date_check).split(' ')[0])
            date_check = date_check + timedelta(days=1)
