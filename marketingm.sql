CREATE TABLE `affiliate_videologs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link` longtext,
  `fk_campaign_id` int(11) DEFAULT NULL,
  `fk_video_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `affiliate_videologs_b8897f94` (`fk_campaign_id`),
  KEY `affiliate_videologs_75e887a8` (`fk_video_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;

CREATE TABLE `affiliate_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(60) DEFAULT NULL,
  `created` datetime NOT NULL,
  `link` longtext,
  `video` varchar(100) NOT NULL,
  `siteid_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `affiliate_video_55091235` (`siteid_id`),
  KEY `affiliate_video_e8701ad4` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

CREATE TABLE `affiliate_screenshotslogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link` longtext,
  `fk_campaign_id` int(11) DEFAULT NULL,
  `screenimages_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `affiliate_pagepeellogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link` longtext,
  `fk_campaign_id` int(11) DEFAULT NULL,
  `pagepeelimage_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


CREATE TABLE `affiliate_mailerlogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link` longtext,
  `fk_campaign_id` int(11) DEFAULT NULL,
  `mailer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `affiliate_mailer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(100) NOT NULL,
  `content` longtext NOT NULL,
  `siteid_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

CREATE TABLE `affiliate_lpages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(60) DEFAULT NULL,
  `created` datetime NOT NULL,
  `link` longtext,
  `lpages` varchar(100) NOT NULL,
  `siteid_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `affiliate_landingpagelogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link` longtext,
  `fk_campaign_id` int(11) DEFAULT NULL,
  `fk_lpage_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ;

CREATE TABLE `affiliate_bannerlogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link` longtext,
  `fk_bannerimages_id` int(11) DEFAULT NULL,
  `fk_campaign_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `affiliate_bannerlogs_81d37e9a` (`fk_bannerimages_id`),
  KEY `affiliate_bannerlogs_b8897f94` (`fk_campaign_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;


CREATE TABLE `affiliate_bannerimages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(60) DEFAULT NULL,
  `created` datetime NOT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `link` longtext,
  `banner` varchar(100) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `siteid_id` int(11) DEFAULT '1',
  `media_type` enum('B','P','SS') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `affiliate_bannerimages_e8701ad4` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;


CREATE TABLE `affiliate_affiliateinfo_networkid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `affiliateinfo_id` int(11) NOT NULL,
  `network_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `affiliateinfo_id` (`affiliateinfo_id`,`network_id`),
  KEY `affiliate_affiliatein_site_id_4b788628dd0_fk_django_network_id` (`network_id`)
) ENGINE=MyISAM AUTO_INCREMENT=171 DEFAULT CHARSET=latin1;


CREATE TABLE `connectors_postbackplayer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playerid` int(11) NOT NULL,
  `dynamic` varchar(256) DEFAULT NULL,
  `country` varchar(256) DEFAULT NULL,
  `city` varchar(256) DEFAULT NULL,
  `zipcode` varchar(256) DEFAULT NULL,
  `currency` varchar(256) DEFAULT NULL,
  `gender` varchar(256) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `status` varchar(256) DEFAULT NULL,
  `platform` varchar(256) DEFAULT NULL,
  `registrationdate` datetime DEFAULT NULL,
  `ftddate` datetime DEFAULT NULL,
  `lastdate` datetime DEFAULT NULL,
  `registrations` int(11) DEFAULT NULL,
  `ftd` int(11) DEFAULT NULL,
  `deposit` double DEFAULT NULL,
  `totaldeposit` double DEFAULT NULL,
  `cashout` double DEFAULT NULL,
  `totalcashout` double DEFAULT NULL,
  `bonuses` double DEFAULT NULL,
  `expiredbonuses` double DEFAULT NULL,
  `revenue` double DEFAULT NULL,
  `clickid` varchar(256) DEFAULT NULL,
  `chargeback` double DEFAULT NULL,
  `void` double DEFAULT NULL,
  `reversal` double DEFAULT NULL,
  `reversechargeback` double DEFAULT NULL,
  `sidegamesbets` double DEFAULT NULL,
  `sidegameswins` double DEFAULT NULL,
  `jackpotcontribution` double DEFAULT NULL,
  `date` date DEFAULT NULL,
  `fk_campaign_id` int(11) DEFAULT NULL,
  `siteid_id` int(11) DEFAULT NULL,
  `networkid_id` int(11) DEFAULT NULL,
  `postback_click_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `connectors_ppplayerprocessed_b8897f94` (`fk_campaign_id`),
  KEY `connectors_ppplayerprocessed_55091235` (`siteid_id`),
  KEY `connectors_ppplayerprocessed_5509676235` (`networkid_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4554 DEFAULT CHARSET=latin1;

CREATE TABLE `connectors_subparameters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `siteid_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `connectors_ppplayerprocessed_550976635` (`siteid_id`)
) ENGINE=MyISAM AUTO_INCREMENT=420 DEFAULT CHARSET=latin1;

CREATE TABLE `connectors_subparametersplayermapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subvalue` varchar(600) DEFAULT NULL,
  `player_id` int(11) DEFAULT NULL,
  `subid_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `connectors_ppplayerprocessed_557236821235` (`subid_id`),
  KEY `connectors_ppplayerprocessed_5509623576235` (`player_id`)
) ENGINE=MyISAM AUTO_INCREMENT=420 DEFAULT CHARSET=latin1;

CREATE TABLE `connectors_postbackadvanced` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `registrations` int(11) DEFAULT 0,
  `clicks` int(11) DEFAULT 0,
  `ftd` int(11) DEFAULT 0,
  `rate` int(11) DEFAULT 0,
  `deposit` double DEFAULT NULL,
  `cashout` double DEFAULT NULL,
  `chargeback` double DEFAULT NULL,
  `bonus` double DEFAULT NULL,
  `expiredbonus` double DEFAULT NULL,
  `reversal` double DEFAULT NULL,
  `revenue` double DEFAULT NULL,
  `void` double DEFAULT NULL,
  `reversechargeback` double DEFAULT NULL,
  `sidegamesbets` double DEFAULT NULL,
  `sidegameswins` double DEFAULT NULL,
  `jackpotcontribution` double DEFAULT NULL,
  `date` date NOT NULL,
  `fk_campaign_id` int(11) DEFAULT NULL,
  `siteid_id` int(11) DEFAULT NULL,
  `networkid_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `connectors_ppplayerprocessed_b8897f94` (`fk_campaign_id`),
  KEY `connectors_ppplayerprocessed_55091235` (`siteid_id`),
  KEY `connectors_ppplayerprocessed_5509676235` (`networkid_id`)
) ENGINE=MyISAM AUTO_INCREMENT=420 DEFAULT CHARSET=latin1;



 CREATE TABLE `connectors_postbackclicks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` int(11) NOT NULL,
  `platform` varchar(256) DEFAULT NULL,
  `country` varchar(256) DEFAULT NULL,
  `state` varchar(256) DEFAULT NULL,
  `registrations` int(11) NOT NULL,
  `clickid` varchar(256) DEFAULT NULL,
  `date` date NOT NULL,
  `fk_campaign_id` int(11) DEFAULT NULL,
  `siteid_id` int(11) DEFAULT NULL,
  `networkid_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `connectors_ppplayerprocessed_b8897f94` (`fk_campaign_id`),
  KEY `connectors_ppplayerprocessed_55091235` (`siteid_id`),
  KEY `connectors_ppplayerprocessed_5509676235` (`networkid_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4554 DEFAULT CHARSET=latin1;


CREATE TABLE `affiliate_accountdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accountnumber` varchar(30) NOT NULL,
  `sortcode` varchar(30) NOT NULL,
  `accounttype` varchar(30) DEFAULT NULL,
  `accountname` varchar(30) NOT NULL,
  `bankname` varchar(30) NOT NULL,
  `bankaddress` varchar(30) NOT NULL,
  `iban` varchar(30) NOT NULL,
  `swift` varchar(30) NOT NULL,
  `beneficiaryaddress` varchar(30) NOT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `fk_affiliateinfo_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `f91e09ce46_affiliate_cb310f8408` (`fk_affiliateinfo_id`),
  KEY `connectors_currency_id_5509676235` (`currency_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `connectors_playercheck` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playerid` int(11) NOT NULL,
  `date` date NOT NULL,
  `ck_campaign_id` int(11) DEFAULT NULL,
  `fk_campaign_id` int(11) DEFAULT NULL,
  `siteid_id` int(11) DEFAULT NULL,
  `networkid_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `connectors_ppplayerprocessed_b8897fg7` (`fk_campaign_id`),
  KEY `connectors_ppplayerprocessed_b8897dshd6` (`ck_campaign_id`),
  KEY `connectors_ppplayerprocessed_550878` (`siteid_id`),
  KEY `connectors_ppplayerprocessed_55096ghgh` (`networkid_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4554 DEFAULT CHARSET=latin1;

CREATE TABLE `userapp_networkaffiliateprogrammemapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `networkid` int(11) DEFAULT NULL,
  `programmeid` int(11) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(30) DEFAULT NULL,
  `retrive` tinyint(1) NOT NULL,
  `siteid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `connectors_ppplaynetworkssed_550878` (`siteid`),
  KEY `userapp__networkid_7bdb6acfb2ac5c89_fk_userapp_network_netwo2id` (`networkid`),
  KEY `userapp_networkandsite_siteid_13350d64164aec8d_fkjango_promme_id` (`programmeid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

Alter table userapp_affiliateprogramme add column `domain` varchar(50) DEFAULT NULL;
Alter table userapp_affiliateprogramme add column `logo` varchar(50) DEFAULT NULL;
Alter table userapp_affiliateprogramme add column `color` varchar(50) DEFAULT NULL;
Alter table userapp_affiliateprogramme add column `bgcolor` varchar(1000) DEFAULT NULL;
Alter table userapp_affiliateprogramme add column `buttoncolor` varchar(1000) DEFAULT NULL;
Alter table userapp_affiliateprogramme add column `tablecolor` varchar(1000) DEFAULT NULL;

Alter table userapp_affiliateprogramme add column `bgtext` varchar(50) DEFAULT NULL;
Alter table userapp_affiliateprogramme add column `buttontext` varchar(50) DEFAULT NULL;
Alter table userapp_affiliateprogramme add column `tabletext` varchar(50) DEFAULT NULL;
Alter table userapp_affiliateprogramme add column `buttonimage` varchar(1000) DEFAULT NULL;

Alter table userapp_network add column `connect_type` varchar(50) DEFAULT NULL;
Alter table connectors_postbackplayer add column `postback_click_id` int(11) DEFAULT NULL;
Alter table affiliate_affiliateinfo add column `accountmanager` int(11) DEFAULT NULL;
alter table affiliate_campaign change `language` `language` enum('English','Swedish','German') DEFAULT NULL;

CREATE TABLE `userapp_accountmanager_siteid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accountmanager_id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `affiliateinfo_id` (`accountmanager_id`,`site_id`),
  KEY `affiliate_affiliatein_hgh788628dd0ccfff_fk_django_site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=236 DEFAULT CHARSET=latin1 ;

CREATE TABLE `connectors_fileupload` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(4) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `excel` varchar(200) NOT NULL,
  `date` datetime DEFAULT NULL,
  `error_msg` longtext DEFAULT NULL,
  `siteid_id` int(11) DEFAULT NULL,
  `uploadtime` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `affiliate_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=106395 DEFAULT CHARSET=latin1;

Alter table userapp_networkandsitemap add column `logo` varchar(200) DEFAULT NULL;

CREATE TABLE `connectors_uploadadvanced` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `registrations` int(11) NOT NULL,
  `clicks` int(11) NOT NULL,
  `ftd` int(11) NOT NULL,
  `rate` int(11) NOT NULL,
  `deposit` double NOT NULL,
  `cashout` double NOT NULL,
  `chargeback` double NOT NULL,
  `void` double NOT NULL,
  `reversechargeback` double NOT NULL,
  `bonus` double NOT NULL,
  `expiredbonus` double NOT NULL,
  `sidegamesbets` double NOT NULL,
  `sidegameswins` double NOT NULL,
  `jackpotcontribution` double NOT NULL,
  `revenue` double NOT NULL,
  `date` date NOT NULL,
  `fk_campaign_id` int(11) DEFAULT NULL,
  `siteid_id` int(11) DEFAULT NULL,
  `fileid_id` int(11) DEFAULT NULL,
  `processed` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `connectors_ppadvancedprocessed_b897f94` (`fk_campaign_id`),
  KEY `connectors_ppadvancedprocessed_5591235` (`siteid_id`)
) ENGINE=MyISAM AUTO_INCREMENT=424 DEFAULT CHARSET=latin1;

ALTER TABLE userapp_network MODIFY COLUMN id int(11) auto_increment;
ALTER TABLE userapp_networkandsitemap MODIFY COLUMN id int(11) auto_increment;

alter table `connectors_postbackclicks` MODIFY COLUMN `date` datetime DEFAULT NULL;
ALTER TABLE `affiliate_currency` drop index currency;
ALTER TABLE `affiliate_affiliateinfo` drop index username;

CREATE TABLE `affiliate_affiliateidmapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `masteraffiliate` int(11) DEFAULT NULL,
  `fk_affiliateinfo_id` int(11) DEFAULT NULL,
  `siteid_id` int(11) DEFAULT NULL,
  `goalid` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `affiliate_campaigntrackermapping_e0433c420` (`fk_affiliateinfo_id`),
  KEY `affiliate_campaigntrackermapping_b89797f94` (`siteid_id`)
) ENGINE=MyISAM AUTO_INCREMENT=98 DEFAULT CHARSET=latin1;

ALTER TABLE userapp_networkaffiliateprogrammemapping add COLUMN siteid int(11) DEFAULT Null;
ALTER TABLE affiliate_mailer add COLUMN user_id int(11) DEFAULT Null;

ALTER TABLE connectors_sitewhitelabelmapping add COLUMN `programmeid` int(11) DEFAULT NULL;
ALTER TABLE userapp_networkaffiliateprogrammemapping add COLUMN accmanager int(11) DEFAULT NULL;
ALTER TABLE connectors_sitewhitelabelmapping add COLUMN accmanager int(11) DEFAULT NULL;

CREATE TABLE `affiliate_sitecommission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `commission_id` int(11) DEFAULT NULL,
  `siteid_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `affiliate_currency_55091235` (`siteid_id`),
  KEY `affiliate_commission-uuj1235` (`commission_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

CREATE TABLE `affiliate_fozilsupport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ipaddress` varchar(250) NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `data` longtext DEFAULT NULL,
  `mobile` int(50) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=106395 DEFAULT CHARSET=latin1;

ALTER TABLE affiliate_wyntasupport add COLUMN message longtext DEFAULT NULL;
ALTER TABLE affiliate_wyntasupport add COLUMN form_type varchar(100) DEFAULT NULL;


 CREATE TABLE `userapp_superadmin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `email` varchar(254) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

 CREATE TABLE `userapp_networkaffiliateprogrammemapping_siteid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `networkaffiliateprogrammemapping_id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `affiliateinfo_id` (`networkaffiliateprogrammemapping_id`,`site_id`),
  KEY `affiliate_affiliadjkh8628dd0ccfff_fk_django_site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=245 DEFAULT CHARSET=latin1;


CREATE TABLE `userapp_partneruser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` datetime NOT NULL,
  `programmeid` int(11) DEFAULT NULL,
  `accmanager` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `options` longtext DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `role` varchar(254) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1 ;

CREATE TABLE `apiapp_customtoken` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(40) NOT NULL,
  `created` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=latin1;

CREATE TABLE `pmc_customtokenpmc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(40) NOT NULL,
  `created` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=latin1;

CREATE TABLE `apiapp_customtokenaffiliate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(40) NOT NULL,
  `created` datetime NOT NULL,
  `affiliate_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `affiliate_id` (`affiliate_id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=latin1;

Alter table connectors_ppplayerraw add column `alias` varchar(250) DEFAULT NULL;
Alter table connectors_ppplayerraw add column `ggr` double DEFAULT NULL;
Alter table connectors_ppplayerraw add column `clubpointsconversion` double DEFAULT NULL;
Alter table connectors_ppplayerraw add column `revenueadjustments` double DEFAULT NULL;
Alter table connectors_ppplayerraw add column `age` int(11) DEFAULT NULL;
Alter table connectors_ppplayerraw add column `blockreason` varchar(1000) DEFAULT NULL;

Alter table connectors_ppplayerprocessed add column `alias` varchar(250) DEFAULT NULL;
Alter table connectors_ppplayerprocessed add column `ggr` double DEFAULT NULL;
Alter table connectors_ppplayerprocessed add column `clubpointsconversion` double DEFAULT NULL;
Alter table connectors_ppplayerprocessed add column `revenueadjustments` double DEFAULT NULL;
Alter table connectors_ppplayerprocessed add column `age` int(11) DEFAULT NULL;
Alter table connectors_ppplayerprocessed add column `blockreason` varchar(1000) DEFAULT NULL;


Alter table connectors_pmcplayerpersonaldetails add column `lastupdated` datetime DEFAULT NULL;
Alter table connectors_pmcplayerpersonaldetails add column `language` varchar(256) DEFAULT NULL;
CREATE TABLE `connectors_pmcplayerpersonaldetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playerid` int(11) NOT NULL,
  `email` varchar(256) DEFAULT NULL,
  `dynamicparameter` varchar(256) DEFAULT NULL,
  `promotioncode` varchar(256) DEFAULT NULL,
  `blocktype` varchar(256) DEFAULT NULL,
  `registeredplatform` varchar(256) DEFAULT NULL,
  `country` varchar(256) DEFAULT NULL,
  `city` varchar(256) DEFAULT NULL,
  `zipcode` varchar(256) DEFAULT NULL,
  `currency` varchar(256) DEFAULT NULL,
  `gender` varchar(256) DEFAULT NULL,
  `status` varchar(256) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `registrationdate` datetime DEFAULT NULL,
  `firstdepositdate` datetime DEFAULT NULL,
  `lastdepositdate` datetime DEFAULT NULL,
  `lastlogindate` datetime DEFAULT NULL,
  `blockdate` datetime DEFAULT NULL,
  `blockreleasedate` datetime DEFAULT NULL,
  `clickid` varchar(256) DEFAULT NULL,
  `totalbonuses` double NOT NULL,
  `expiredbonuses` double NOT NULL,
  `balance` double NOT NULL,
  `maxbalance` double NOT NULL,
  `totalcustomerclubpoints` double NOT NULL,
  `revenue` double NOT NULL,
  `void` double NOT NULL,
  `chargeback` double NOT NULL,
  `date` date NOT NULL,
  `affiliateid` int(11) DEFAULT NULL,
  `depositscount` int(11) DEFAULT NULL,
  `totaldeposits` int(11) DEFAULT NULL,
  `totalwithdrawals` int(11) DEFAULT NULL,
  `totalchargebacks` int(11) DEFAULT NULL,
  `totalchargebackreverses` int(11) DEFAULT NULL,
  `totalvoids` int(11) DEFAULT NULL,
  `siteid_id` int(11) DEFAULT NULL,
  `alias` varchar(250) DEFAULT NULL,
  `mailenabled` tinyint(1) NOT NULL,
  `smsenabled` tinyint(1) NOT NULL,
  `promotionsenabled` tinyint(1) NOT NULL,
  `bonusesenabled` tinyint(1) NOT NULL,
  `isblocked` tinyint(1) NOT NULL,
  `ggr` double DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `blockreason` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `connectors_ppplayerprocessed_55091235` (`siteid_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14204 DEFAULT CHARSET=latin1;


CREATE TABLE `affiliate_sitecommission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `revshare` tinyint(1) NOT NULL,
  `revsharepercent` int(11) DEFAULT NULL,
  `cpachoice` tinyint(1) NOT NULL,
  `criteriaacquisition` enum('FTD','Register','Deposit') DEFAULT NULL,
  `criteriavalue` double NOT NULL,
  `cpacommissionvalue` int(11) DEFAULT NULL,
  `cprchoice` tinyint(1) NOT NULL,
  `cprcommissionvalue` int(11) DEFAULT NULL,
  `siteid_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `connectors_ppplayerprocessed_55235` (`siteid_id`)
) ENGINE=MyISAM AUTO_INCREMENT=114 DEFAULT CHARSET=latin1;

CREATE TABLE `affiliate_campaignclickscount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_campaign_id` int(11) DEFAULT NULL,
  `platform` varchar(50) DEFAULT NULL,
  `clicks` int(11) DEFAULT '1',
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `affiliate_campaignclickcount_b8897f94` (`fk_campaign_id`)
) ENGINE=MyISAM AUTO_INCREMENT=520 DEFAULT CHARSET=latin1;

alter table affiliate_affiliateinfo add column `im` tinyint(1) DEFAULT NULL;
alter table affiliate_affiliatecommission add column `active` tinyint(1) DEFAULT 1;
alter table affiliate_affiliatecommission add column `start_date` date DEFAULT NULL;
alter table affiliate_affiliatecommission add column `end_date` date DEFAULT NULL;
alter table affiliate_affiliatecommission add column `pocdeduction` tinyint(1) DEFAULT 1;
alter table affiliate_affiliatecommission add column `siteid_id` int(11) DEFAULT NULL;
alter table affiliate_affiliatecommission add column `accountmanager` int(11) DEFAULT NULL;
alter table affiliate_affiliatecommission add column `country` int(11) DEFAULT NULL;


alter table affiliate_campaign add column `accountmanager` int(11) DEFAULT NULL;
alter table affiliate_campaign modify column `fk_affiliateinfo_id` int(11) DEFAULT NULL;

alter table `affiliate_affiliatecommission` modify column `criteriavalue` double DEFAULT NULL;

alter table `affiliate_affiliateinfo` add column `skype` varchar(50) DEFAULT NULL;

alter table `userapp_affiliateprogramme` add column `order` int(11) DEFAULT NULL;


CREATE TABLE `affiliate_affiliatecommission_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `affiliatecommission_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `affiliatecommission_id` (`affiliatecommission_id`,`country_id`),
  KEY `affiliate_affiliatein_site_id_4b7886sdsdfk_django_country_id` (`country_id`)
) ENGINE=MyISAM AUTO_INCREMENT=474 DEFAULT CHARSET=latin1;


CREATE TABLE `userapp_currencyconverted` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eurtogbp` double DEFAULT NULL,
  `gbptoeur` double DEFAULT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=420 DEFAULT CHARSET=latin1;


ALTER TABLE userapp_messagetrigger MODIFY COLUMN triggercondition enum('ADVERTISER_SIGN_UP_APPROVAL','NEW_ADVERTISER','CAMPAIGN_PAYOUT_CHANGE','CAMPAIGN_STATUS_CHANGE','REGISTRATION','AFFILIATE_SIGN_UP_APPROVAL','EMAIL_CHANGE','PASSWORD_RESET',
  'NEW_AFFILIATE','REFER_SUB_AFFILIATE','AFFILIATE_SIGN_UP_REJECTED','ADDUSER', 'INVITE_AFFILIATE', 'Jumpman_Brand_Setup', 'Jumpman_Client_Email') DEFAULT NULL;

alter table userapp_affiliateprogramme add column `accessible` tinyint(1) DEFAULT 1;

alter table affiliate_affiliateinfo add column `accessible` tinyint(1) DEFAULT 1;

ALTER TABLE affiliate_campaign MODIFY COLUMN name varchar(200) NOT NULL;


CREATE TABLE `affiliate_inviteaffiliate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_affiliateinfo_id` int(11) DEFAULT NULL,
  `accountmanager` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `affiliate_invite_e093c4dfsdfs20` (`fk_affiliateinfo_id`)
) ENGINE=MyISAM AUTO_INCREMENT=843 DEFAULT CHARSET=latin1;


CREATE TABLE `connectors_campaigndatereport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_affiliateinfo_id` int(11) DEFAULT NULL,
  `country` varchar(256) DEFAULT NULL,
  `currency` varchar(256) DEFAULT NULL,
  `gender` varchar(256) DEFAULT NULL,
  `platform` varchar(256) DEFAULT NULL,
  `registrations` int(11) DEFAULT NULL,
  `ftd` int(11) DEFAULT NULL,
  `deposit` double DEFAULT NULL,
  `totaldeposit` double DEFAULT NULL,
  `cashout` double DEFAULT NULL,
  `totalcashout` double DEFAULT NULL,
  `bonuses` double DEFAULT NULL,
  `expiredbonuses` double DEFAULT NULL,
  `revenue` double DEFAULT NULL,
  `chargeback` double DEFAULT NULL,
  `void` double DEFAULT NULL,
  `reversechargeback` double DEFAULT NULL,
  `sidegamesbets` double DEFAULT NULL,
  `sidegameswins` double DEFAULT NULL,
  `jackpotcontribution` double DEFAULT NULL,
  `newplayersdeposit` double DEFAULT NULL,
  `totalwagers` double DEFAULT NULL,
  `netperplayer` double DEFAULT NULL,
  `newplayers` int(11) DEFAULT NULL,
  `activeplayers` int(11) DEFAULT NULL,
  `depositingaccounts` int(11) DEFAULT NULL,
  `wageringaccounts` int(11) DEFAULT NULL,
  `avgactivedays` int(11) DEFAULT NULL,
  `avgplayerltv` double DEFAULT NULL,
  `cpacommission` double DEFAULT NULL,
  `revsharecommission` double DEFAULT NULL,
  `totalcommission` double DEFAULT NULL,
  `date` date NOT NULL,
  `fk_campaign_id` int(11),
  `siteid_id` int(11),
  PRIMARY KEY (`id`),
  KEY `affiliate_invite_edfsdfsdfs20` (`fk_affiliateinfo_id`),
  KEY `connectors_ppplaerprocesdsed_b8897f94` (`fk_campaign_id`),
  KEY `connectors_ppplaerprosdsssed_55091235` (`siteid_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14589 DEFAULT CHARSET=latin1;

Alter table userapp_affiliateprogramme add column `wynta_domain` varchar(200) DEFAULT NULL;
Alter table userapp_affiliateprogramme modify column `domain` varchar(200) DEFAULT NULL;

Alter table connectors_campaigndatereport add column `clicks` int(11) DEFAULT NULL;
Alter table connectors_ppplayerprocessed add column `processed` tinyint(1) DEFAULT 0;


Alter table affiliate_campaignclickscount add column `country` varchar(200) DEFAULT NULL;

Alter table userapp_accountmanager add column `status` varchar(50) DEFAULT 'Trial';
Alter table userapp_accountmanager add column `rev_percent_cut` double DEFAULT 0.0;


alter table affiliate_affiliateinfo add column `lastlogin` datetime DEFAULT NULL;

Alter table userapp_affiliateprogramme add column `ssl_state` varchar(200) DEFAULT 'NO';


Alter table userapp_accountmanager add column `amcactive` tinyint(1) DEFAULT 1;

CREATE TABLE `connectors_pmcplayergamedetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playerid` int(11) NOT NULL,
  `gameid` int(11) DEFAULT 0,
  `totalrecords` int(11) DEFAULT 0,
  `gameprovider` varchar(256) DEFAULT NULL,
  `currency` varchar(256) DEFAULT NULL,
  `gamename` varchar(256) DEFAULT NULL,
  `gamecategory` varchar(256) DEFAULT NULL,
  `date` date NOT NULL,
  `bets` double NOT NULL,
  `wins` double NOT NULL,
  `ggr` double NOT NULL,
  `siteid_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `connectors_ppplayergames_6534` (`siteid_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14204 DEFAULT CHARSET=latin1;

alter table `affiliate_affiliatecommission` modify column `cprcommissionvalue` int(11) DEFAULT 0;


CREATE TABLE `pmc_pmcaccountmanager` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` datetime NOT NULL,
  `affiliateprogramme` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `email` varchar(254) NOT NULL,
  `status` varchar(50) DEFAULT 'Trial',
  `rev_percent_cut` double DEFAULT '0',
  `pmcactive` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=latin1;

CREATE TABLE `pmc_pmcaccountmanager_siteid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pmcaccountmanager_id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pmcaccountmanager_id` (`pmcaccountmanager_id`,`site_id`),
  KEY `affiliate_affiliatein_hghdsfsdcfff_fk_django_site_id` (`site_id`)
) ENGINE=MyISAM AUTO_INCREMENT=236 DEFAULT CHARSET=latin1 ;

CREATE TABLE `pmc_pmcroleuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` datetime NOT NULL,
  `programmeid` int(11) DEFAULT NULL,
  `pmcmanager` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `options` longtext DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `role` varchar(254) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1 ;

Alter table affiliate_affiliateinfo add column `pmcmanager` int(11) DEFAULT NULL;


Alter table pmc_pmcaccountmanager modify column `pmcactive` tinyint(1) DEFAULT '1';

Alter table userapp_networkaffiliateprogrammemapping add column `pmcmanager` int(11) DEFAULT NULL;

Alter table connectors_postbackclicks add column `wdynamic` varchar(256) DEFAULT NULL;

Alter table connectors_ppplayerprocessed add column `wdynamic` varchar(256) DEFAULT NULL;

Alter table connectors_pmcplayerpersonaldetails add column `wdynamic` varchar(256) DEFAULT NULL;

Alter table connectors_campaigndatereport add column `wdynamic` varchar(256) DEFAULT NULL;

Alter table affiliate_affiliatecommission add column `currency` varchar(256) DEFAULT 'gbp';

Alter table affiliate_affiliateinfo add column `mappingid` int(11) DEFAULT NULL;

Alter table affiliate_affiliateinfo add column `website` varchar(500) DEFAULT NULL;

alter table affiliate_affiliateinfo modify column `address` varchar(256) DEFAULT NULL;
alter table affiliate_affiliateinfo modify column `companyname` varchar(256) DEFAULT NULL;

alter table affiliate_affiliateinfo modify column `contactname` varchar(256) DEFAULT NULL;
alter table affiliate_affiliateinfo modify column `city` varchar(256) DEFAULT NULL;

alter table affiliate_affiliateinfo modify column `state` varchar(256) DEFAULT NULL;
alter table affiliate_affiliateinfo modify column `country` varchar(256) DEFAULT NULL;

alter table affiliate_affiliateinfo modify column `postcode` varchar(256) DEFAULT NULL;
alter table affiliate_affiliateinfo modify column `skype` varchar(256) DEFAULT NULL;

Alter table affiliate_bannerimages add column `mappingid` int(11) DEFAULT NULL;


CREATE TABLE `affiliate_affiliatesite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_affiliateinfo_id` int(11) DEFAULT NULL,
  `site_id` int(11) DEFAULT NULL,
  `info` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `affiliate_campaigndgsdgsd5676420` (`fk_affiliateinfo_id`)
) ENGINE=MyISAM AUTO_INCREMENT=98 DEFAULT CHARSET=latin1;

alter table affiliate_affiliatecommission add column `ringfence` tinyint(1) DEFAULT 0;
alter table affiliate_affiliatecommission add column `exclude_rev` tinyint(1) DEFAULT 1;


alter table affiliate_accountdetails add column `paypal_username` varchar(250) DEFAULT '';
alter table affiliate_accountdetails add column `paypal_email` varchar(250) DEFAULT '';
alter table affiliate_accountdetails add column `neteller_email` varchar(250) DEFAULT '';
alter table affiliate_accountdetails add column `neteller_username` varchar(250) DEFAULT '';
alter table affiliate_accountdetails add column `neteller_accountnumber` varchar(250) DEFAULT '';

ALTER TABLE affiliate_affiliateinfo add COLUMN address2 varchar(250) DEFAULT NULL;

ALTER TABLE affiliate_messagelogs add COLUMN content longtext;
ALTER TABLE affiliate_messagelogs modify COLUMN to_email longtext;
ALTER TABLE affiliate_messagelogs add COLUMN subject longtext;
ALTER TABLE affiliate_messagelogs add COLUMN account_manager_id int(11) DEFAULT null;
ALTER TABLE affiliate_messagelogs add COLUMN `affstatus` enum('R','U','D') DEFAULT 'U';

Alter table affiliate_bannerimages add COLUMN `language` enum('English','Swedish','German') DEFAULT NULL;
Alter table affiliate_video add COLUMN `language` enum('English','Swedish','German') DEFAULT NULL;
Alter table affiliate_lpages add COLUMN `language` enum('English','Swedish','German') DEFAULT NULL;
Alter table affiliate_mailer add COLUMN `language` enum('English','Swedish','German') DEFAULT NULL;

alter table affiliate_accountdetails modify column `accountnumber` varchar(250) DEFAULT '';
alter table affiliate_accountdetails modify column `sortcode` varchar(250) DEFAULT '';
alter table affiliate_accountdetails modify column `accounttype` varchar(250) DEFAULT '';
alter table affiliate_accountdetails modify column `accountname` varchar(250) DEFAULT '';
alter table affiliate_accountdetails modify column `bankname` varchar(950) DEFAULT '';
alter table affiliate_accountdetails modify column `bankaddress` varchar(950) DEFAULT '';
alter table affiliate_accountdetails modify column `iban` varchar(250) DEFAULT '';
alter table affiliate_accountdetails modify column `swift` varchar(950) DEFAULT '';
alter table affiliate_accountdetails modify column `beneficiaryaddress` varchar(950) DEFAULT '';


CREATE TABLE `affiliate_tlinks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(60) DEFAULT NULL,
  `created` datetime NOT NULL,
  `link` longtext,
  `siteid_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `language` enum('English','Swedish','German') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ;

CREATE TABLE `affiliate_tlinkslogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link` longtext,
  `fk_campaign_id` int(11) DEFAULT NULL,
  `fk_tlink_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ;


CREATE TABLE `affiliate_deduction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pocdeduction` tinyint(1) NOT NULL,
  `adminfee` tinyint(1) NOT NULL,
  `siteid_id` int(11) DEFAULT NULL,
  `poctvalue` double DEFAULT 0.00,
  `adminfeevalue` double DEFAULT 0.00,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `affiliate_site_gjhg235` (`siteid_id`)
);

CREATE TABLE `affiliate_deduction_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `deduction_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `deduction_id` (`deduction_id`,`country_id`),
  KEY `affiliate_affiliatein_site_id_4bsdfsdfs_django_country_id` (`country_id`)
);

CREATE TABLE `affiliate_tieredstructure` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accountmanager` int(11) DEFAULT NULL,
  `siteid_id` int(11) DEFAULT NULL,
  `options` longtext,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `stype` enum('FTD','Revenue') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;


CREATE TABLE userapp_siteorder (
id int(11) NOT NULL AUTO_INCREMENT,
accountmanager_id int(11) NOT NULL,
site_id int(11) NOT NULL,
order_id int(11) default null,
PRIMARY KEY (id),
UNIQUE KEY affiliateinfo_id (accountmanager_id,site_id),
KEY affiliate_affiliatein_hgh788628dd0ccfff_fk_django_site_id (site_id)
) ENGINE=MyISAM AUTO_INCREMENT=862 DEFAULT CHARSET=latin1;

insert into userapp_siteorder(accountmanager_id, site_id) select accountmanager_id, site_id from userapp_accountmanager_siteid;

update affiliate_bannerimages set language = 'English' where language is null;
update affiliate_video set language = 'English' where language is null;
update affiliate_tlinks set language = 'English' where language is null;
update affiliate_lpages set language = 'English' where language is null;

Alter table affiliate_bannerimages modify  `language` varchar(30) DEFAULT NULL;
Alter table affiliate_video modify  `language` varchar(30) DEFAULT NULL;
Alter table affiliate_tlinks modify  `language` varchar(30) DEFAULT NULL;
Alter table affiliate_lpages modify  `language` varchar(30) DEFAULT NULL;

Alter table affiliate_campaign modify  `language` varchar(30) DEFAULT NULL;

CREATE TABLE `userapp_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accmanager` int(11) DEFAULT NULL,
  `affinfo` int(11) DEFAULT NULL,
  `notificationtype` varchar(254) DEFAULT NULL,
  `title` varchar(254) DEFAULT NULL,
  `content` varchar(5000) DEFAULT NULL,
  `redirecturl` varchar(254) DEFAULT NULL,
  `lastseen` varchar(254) DEFAULT NULL,
  `lastupdated` varchar(254) DEFAULT NULL,
	`category` varchar(254) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
ALTER TABLE connectors_campaigndatereport ADD COLUMN `revsharecommission` double DEFAULT NULL,

CREATE TABLE `affiliate_paymenthistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(250) DEFAULT NULL,
  `payment_type` varchar(250) DEFAULT NULL,
  `accountmanager` int(250) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `month` int(50) DEFAULT NULL,
  `year` int(50) DEFAULT NULL,
  `content_type_id` int(50) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `fk_affiliateinfo_id` int(11) DEFAULT NULL,
  `info` varchar(250) DEFAULT NULL,
  `amount` double DEFAULT '0',
  `adjusted_amount` double DEFAULT '0',
  `carried_amount` double DEFAULT '0',
  `paid_status` tinyint(1) DEFAULT NULL,
  `threshold_amount` double DEFAULT '0',
  `balance` double DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `affiliate_campaigndgsdgsd5676420` (`fk_affiliateinfo_id`)
) ENGINE=MyISAM AUTO_INCREMENT=17209 DEFAULT CHARSET=latin1 ;

CREATE TABLE `affiliate_threshold` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accountmanager_id` int(11) NOT NULL,
  `bankwire` double DEFAULT '0',
  `neteller` double DEFAULT '0',
  `paypal` double DEFAULT '0',
  `accountmanager` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `accountmanager_id` (`accountmanager_id`)
) ENGINE=MyISAM AUTO_INCREMENT=863 DEFAULT CHARSET=latin1;

Alter table affiliate_paymenthistory add `balance` double DEFAULT '0';


alter table affiliate_messagelogs add column `user_id` int(11) DEFAULT NULL;


CREATE TABLE `userapp_registrationpageedit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accmanager` int(11) DEFAULT NULL,
  `options` longtext,
  PRIMARY KEY (`id`),
  KEY `id_idx` (`accmanager`),
  CONSTRAINT `id` FOREIGN KEY (`accmanager`) REFERENCES `userapp_accountmanager` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;


insert into userapp_registrationpageedit(accmanager, options) select id, null from userapp_accountmanager;

update userapp_registrationpageedit set options='["Account Login Details", "Payment Details", "Contact", "Account Profile Details", "First Name", "Last Name", "Email Address", "Password", "Confirm Password", "Bank Wire Transfer", "PayPal", "Neteller", "Account Number", "Beneficiary Name", "Beneficiary Address", "Bank Name", "Bank Address", "Sort Code", "IBAN", "SWIFT", "Beneficiary Name P", "Email Address P", "Beneficiary Name N", "Email Address N", "Account Number N", "Email", "Phone", "IM", "URL", "Company Name", "Phone Number", "Address 1", "Address 2", "City", "State", "Postcode", "Country", "DOB", "Skype Id"]';

CREATE TABLE `userapp_emailclient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accmanager` int(11) DEFAULT NULL,
  `username` varchar(254) DEFAULT NULL,
  `password` varchar(254) DEFAULT NULL,
`emailclient` varchar(254) DEFAULT NULL,
  `domain` varchar(254) DEFAULT NULL,
  `apikey` varchar(254) DEFAULT NULL,
  `partuser` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

Alter table affiliate_video modify  `video` varchar(100) DEFAULT NULL;

CREATE TABLE `userapp_onelogin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` longtext,
  `ipaddress` varchar(250) NOT NULL,
  `timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

CREATE TABLE `affiliate_invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logopath` varchar(1000) DEFAULT NULL,
  `pay_history_id` int(250) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

ALTER TABLE affiliate_affiliatecommission ADD COLUMN created_at datetime DEFAULT NULL,ADD COLUMN comm_type varchar(200) DEFAULT NULL,ADD COLUMN notes varchar(2000) DEFAULT NULL;

ALTER TABLE affiliate_affiliatecommission ADD COLUMN monthToDate tinyint(1) DEFAULT NULL;