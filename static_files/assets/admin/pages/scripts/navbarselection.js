var NavBarSelection = function () {
	
	return {
		init:function(){
			
			var link_active = false;
			$(".page-sidebar-menu li").each(function(){
				var main_li = $(this)
				var sub_menu = $(this).find(".sub-menu");
				if (sub_menu.length == 0){
		        	if($(main_li).find('a').attr("href") == window.location.pathname){
		            	$(main_li).addClass("active open");
		            	$(main_li).find('.varrow').addClass("selected");
		        		link_active = true;
		        	}
		        }
				else{
					$(sub_menu).find('li').each(function(){
						if($(this).find('a').attr('href') == window.location.pathname){
							$(main_li).addClass("active open");
							$(main_li).find('.varrow').addClass("selected");
							$(this).addClass('open');
							link_active = true;
						}
					});
					
				}
		        
		    });
		if (!link_active){
			$(".page-sidebar-menu li").eq(0).addClass("active open");
		}
			
		}
	}
	
}();