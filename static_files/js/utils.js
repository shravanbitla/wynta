function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

$.ajaxSetup({ 
    beforeSend: function(xhr, settings) {
        if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
            // Only send the token to relative URLs i.e. locally.
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
        }
    } 
});

function ajaxBlockUI(ele){
	$(ele).block({ 
        message: '<h5><img src="/static_files/images/Preloader_21.gif"/></h6>', 
        css: {left: '506px',  border:"0px" } 
    }); 
}

function ajaxUnBlockUI(ele){
	$(ele).unblock(); 
}

function show_sites(){
	//$("#siteFilter").toggle(850)
	
	if ($("#siteFilter").is(':visible')) {
		$("#siteFilter").slideUp('slow');
	}
	else{
		$("#siteFilter").slideDown('slow');
	}
}

$('.click-plus-minus').click(function(){
    $(this).find('i').toggleClass('fa-plus fa-minus')
});
$('.click-plus-minus').blur(function(){
    $(this).find('i').toggleClass('fa-plus fa-minus')
});
