
  function addPlayerslistDataTable(headersList, dataList){
    $('#player_data:has(tbody)').dataTable().fnDestroy();
    $("#player_data").dataTable( {"iDisplayLength": 30,
       "data": dataList,
       "columns":headersList,
       // "scrollX": true,
       "ordering": false,
       paging: false,
       searching: false,
       info: false,
       "pageLength": 10,
       "autoWidth": false,
       "language": {
            "emptyTable":     "Nothing to see here... yet!"
        }
          });
   
    
}

function barchartdiv(){
    var dates_list = {{bar_dates_list|safe}} ;
    var barchart_data = {{barchart|safe}} ;
    $('#barcontainer').highcharts({
    chart: {
        type: 'column',
        spacingTop: 10,
        spacingBottom: 15,
        spacingLeft: 20,
        spacingRight: 30
    },
    title: {
        text: ''
    },
 xAxis: {
        categories: {{bar_dates_list|safe}},
        crosshair: false
    },
    yAxis: {
        min: 0,
        
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: {{barchart|safe}}
});
}
  function circlecharts(){
    var device_data = {{affiliate_status|safe}};
    $('#devicecircle').highcharts({
            chart: {
                renderTo: 'container',
                type: 'pie'
            },
            title: {
                text: ''
            },
            yAxis: {
                title: {
                    text: 'Affiliates Status.'
                }
            },
            plotOptions: {
                pie: {
                    shadow: false,
                    size: '100%'
                }
            },
            tooltip: {
                formatter: function() {
                    return '<b>'+ this.point.name +'</b>: '+ this.y +'';
                }
            },
            series: [{
                name: 'Status',
                data: [
                        {name:"Pending",y:device_data[0], color: '#0b99d7'},
                        {name:"Approved",y:device_data[1], color: '#93c83d'},
                        {name:"Rejected",y:device_data[2], color: '#7cb5ec'},
                    ],
                size: '100%',
                innerSize: '70%',
                showInLegend:true,
                dataLabels: {
                    enabled: false
                }
            }]
        } );
    
    
}


jQuery(document).ready(function() {
    circlecharts();
    barchartdiv();

    Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   QuickSidebar.init(); // init quick sidebar
Demo.init(); // init demo features
   Index.init();
   egcsTools.initDemoDaterange('{{from_date}}', '{{to_date}}');
   Index.initJQVMAP(); // init index page's custom scripts

   Index.initMiniCharts();

   egcsTools.initNavigation();
   $(".page-sidebar-menu li").eq(3).addClass("active");
   var data_list = {{data_list|safe}};
   var header_list = {{header_list|safe}};
   var domain = '{{domain|safe}}'
   var pagination_str = '{{pagination_div}}';
   var decoded = $('<div />').html(pagination_str).text();
       $('.digg_pagination').html(decoded);
  

   addPlayerslistDataTable(header_list, data_list);
  });

